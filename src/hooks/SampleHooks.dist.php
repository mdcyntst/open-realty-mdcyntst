<?php

//DO NOT MODIFY THIS EXAMPLE HOOK FILE. Copy any required functions to your own custom hook file and class per the documentation.

// Name of our hook. Must be named the same as the PHP file it is contained within, e.g.: sample_hook = sample_hook.php.

namespace OpenRealty\Hooks\SampleHooks;

use OpenRealty\BaseClass;

class SampleHooks extends BaseClass
{
    // afterListingCreate($listingID) - called after a listing is added to the db
    public function afterListingCreate(int $listingID): void
    {
        /*global $ORconn, $lang, $jscript;
        $sent = $misc->sendEmail($this->config['admin_name'], $this->config['admin_email'], $this->config['admin_email'], "New Listing Hook Triggered - ".$listingID, "New Listing Hook Triggered - ".$listingID);
        */
    }

    // afterListingUpdate($listingID) - called after a listing is modified
    public function afterListingUpdate(int $listingID): void
    {
    }

    // afterListingDelete($listingID) - called after a listing is deleted
    public function afterListingDelete(int $listingID): void
    {
    }

    // beforeListingDelete($listingID) - called before a listing is deleted
    public function beforeListingDelete(int $listingID): void
    {
    }

    // function afterListingActivated($listingID) - called after a listing is marked as active.
    public function afterListingActivated(int $listingID): void
    {
    }

    //afterUserSignup($userID) - called after a successful registration
    public function afterUserSignup(int $userID): void
    {
    }

    //beforeUserUpdate($userID,$postvars) - called after a user change is verified and before it is actually executed.
    //This allows you to get teh current users email address, etc before it is changed.
    public function beforeUserUpdate(int $userID, array $postvars): void
    {
    }

    // afterUserUpdate($userID) - called after a user changes their profile
    public function afterUserUpdate(int $userID): void
    {
    }

    // afterUserDelete($userID) - called after a user is deleted
    public function afterUserDelete(int $userID): void
    {
    }

    //afterUserLogin($userID) - called after a user logs in
    public function afterUserLogin(int $userID): void
    {
    }

    //  afterUserLogout($userID) - called after a user logs out
    public function afterUserLogout(int $userID): void
    {
    }

    /**
     * Used to Releplace the Meta Description, Keywords, and/or title for a page.
     * This function must return an array with one or more of the following keys. keywords, description, title
     *
     *  eg.
     *  return array('title'=>'This is My Custom Title','keywords' =>'custom keywords rock');
     */
    public function replaceMetaTemplateTags(string $action): void
    {
    }

    // afterLeadCreate($lead_id) - called after a lead is added to the db
    public function afterLeadCreate(int $lead_id): void
    {
    }

    /**
     * afterMediaCreate($filename, $media_type)
     * runs after a new media item is uploaded and added to OR
     * $mediainfo (array) contains 3 items
     * ['file_name'] (string) Name of media file being deleted
     * ['media_type'] (string) type of media file deleted. Possible types:
     *   listingsimages
     *   userimages
     *   listingsfiles
     *   usersfiles
     *   listingsvtours
     * ['media_parent_id'] (int) numeric listing or user ID (whicherver is applicable)
     * be aware: linked (remote) listing photos will contain http:// or https:// URLs
     * Do not echo or print any results from your script, this can break the json return codes Return NULL when
     * completed
     */
    public function afterMediaCreate(array $mediainfo): void
    {
        //return NULL;
    }

    /**
     * beforeMediaCreate($file_name, $media_type, $media_parent_id)
     * runs BEFORE a media item is deleted
     * $mediainfo (array) contains 3 items
     * ['file_name'] (string) Name of media file being deleted
     * ['media_type'] (string) type of media file deleted. Possible types:
     *   listingsimages
     *   userimages
     *   listingsfiles
     *   usersfiles
     *   listingsvtours
     * ['media_parent_id'] (int) numeric listing or user ID (whicherver is applicable)
     * be aware: linked (remote) listing photos will contain http:// or https:// URLs
     * Do not echo or print any results from your script, this can break the json return codes Return NULL when
     * completed
     */
    public function beforeMediaCreate(array $mediainfo): void
    {
        //return NULL;
    }

    /**
     * afterMediaDelete($file_name, $media_type, $media_parent_id)
     * runs AFTER  a media item is deleted
     * $mediainfo (array) contains 3 items
     * ['file_name'] (string) Name of media file being deleted
     * ['media_type'] (string) type of media file deleted. Possible types:
     *   listingsimages
     *   userimages
     *   listingsfiles
     *   usersfiles
     *   listingsvtours
     * ['media_parent_id'] (int) numeric listing or user ID (whicherver is applicable)
     * be aware: linked (remote) listing photos will contain http:// or https:// URLs
     * Do not echo or print any results from your script, this can break the json return codes Return NULL when
     * completed
     */
    public function afterMediaDelete(array $mediainfo): void
    {
        //return NULL;
    }
}
