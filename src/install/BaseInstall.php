<?php

declare(strict_types=1);

namespace OpenRealty\Install;

use ADOConnection;

/**
 * This is our base installer class, all install, upgrade, move classes extend this.
 */
class BaseInstall
{
    /**
     * Current Open-Realty Version Number
     *
     * @var string
     */
    public string $version_number;

    /** @var string[] */
    public array $lang;

    /**
     * Sets the Open-Realty version number for the installer.
     */
    public function __construct()
    {
        $this->version_number = '3.7.0-beta.2';
        $this->lang = [];
    }

    /**
     * Generates a Random Password
     *
     * @param integer $len Length of password to generate. Default 15 chars.
     *
     * @return string Generated Password
     */
    public function passwordGenerator(int $len = 15): string
    {
        $chars = '0123456789aeiouyAEIOUYbdghjmnpqrstvzBDGHJLMNPQRSTVWXZ!@#$%^&*()_+`-_[]{}\<>,./?';
        $password = '';
        for ($i = 0; $i < $len; $i++) {
            $password .= $chars[(rand() % strlen($chars))];
        }
        return $password;
    }

    /**
     * Shows input form for getting db configuration. Can accept prior values to pre-populate form for upgrades.
     *
     * @param string $old_db_server    Database Server Name.
     * @param string $old_db_name      Database Name.
     * @param string $old_db_user      Database User Name.
     * @param string $old_db_password  Database Password.
     * @param string $old_table_prefix Database Table Prefix.
     */
    public function getNewSettings(string $old_db_server = 'localhost', string $old_db_name = '', string $old_db_user = '', string $old_db_password = '', string $old_table_prefix = 'default_'): void
    {
        $www = $this->getBaseUrl();
        $physical = $this->getBasePath();
        echo '<p>' . $this->lang['install_setup__database_settings'] . '</p>
      <form name="db_connection" method="post" action="index.php?step=5">
      <p>' . $this->lang['install_Database_Type'] . '
        <select name="db_type">
        <option value="mysqli" selected="selected">' . $this->lang['install_mySQL'] . '</option>
    
      </select>
      </p>
      <p> ' . $this->lang['install_Database_Server'] . '
        <input name="db_server" type="text" value="' . $old_db_server . '" />
      </p>
      <p> ' . $this->lang['install_Database_Name'] . '
        <input type="text" name="db_database" value="' . $old_db_name . '" />
      </p>
      <p> ' . $this->lang['install_Database_User'] . '
        <input type="text" name="db_user" value="' . $old_db_user . '" />
      </p>
      <p> ' . $this->lang['install_Database_Password'] . '
        <input type="text" name="db_password" value="' . $old_db_password . '" />
      </p>
      <p> ' . $this->lang['install_Table Prefix'] . '
        <input type="text" name="table_prefix" value="' . $old_table_prefix . '" />
      </p>
      <p> ' . $this->lang['install_Base_URL'] . '
        <input type="text" name="baseurl" size="60" value="' . $www . '" />
      </p>
      <p>' . $this->lang['install_Base_Path'] . '
        <input type="text" name="basepath" size="60" value="' . $physical . '" />
      </p>
      <p>' . $this->lang['install_devel_mode'] . '
        <select name="devel_mode"><option value="no">' . $this->lang['no'] . '</option><option value="yes">' . $this->lang['yes'] . '</option></select>
      </p>
      <p>
        <input type="submit" name="Submit" value="Next" />
      </p>
      </form>';
    }

    /**
     * Build a Connection To the Database From SESSION vars
     *
     * @return ADOConnection
     */
    public function buildConn(): ADOConnection
    {
        include dirname(__FILE__) . '/../vendor/adodb/adodb-php/adodb.inc.php';
        $ORconn = ADONewConnection($_SESSION['db_type']);
        if (is_bool($ORconn)) {
            die('<strong>' . $this->lang['install_connection_fail'] . '</strong><br>');
        }
        /** @psalm-suppress InvalidArgument */
        $ORconn->setConnectionParameter(strval(MYSQLI_SET_CHARSET_NAME), 'utf8');
        $ORconn->connect((string)$_SESSION['db_server'], (string)$_SESSION['db_user'], (string)$_SESSION['db_password'], (string)$_SESSION['db_database']) or die('<strong>' . $this->lang['install_connection_fail'] . '</strong><br>');
        return $ORconn;
    }

    /**
     * Create the common.php file with correct database settings, etc.
     */
    public function writeConfig(): void
    {
        // Database connection made.
        if (!isset($_SESSION['autoinstall'])) {
            echo '' . $this->lang['install_connection_ok'] . '<br />';
        }
        $sqlversion = true;
        if ($_SESSION['db_type'] == 'mysqli') {
            $sqlversion = $this->checkMySQLVersion();
        }
        if ($sqlversion === false) {
            echo '<span style="color: red"><strong>' . $this->lang['install_sqlversion_warn'] . '</strong></span><br />';
            echo '<strong>' . $this->lang['install_sql_required'] . '5.0</strong>';
        } else {
            if (!isset($_SESSION['autoinstall'])) {
                echo '' . $this->lang['install_save_settings'] . '<br />';
            }
            $filecontent = '<?php
        
        declare(strict_types=1);
        
        use \OpenRealty\Misc;
        use \OpenRealty\Api\Commands\SettingsApi;
        use \OpenRealty\Api\Database;
        
        $config = array();
        /** @psalm-suppress InvalidGlobal */
        global $ORconn, $db_type, $db_server, $db_database, $options, $ADODB_SESS_CONN, $ADODB_FETCH_MODE, $ADODB_SESS_DEBUG, $ADODB_SESSION_TBL, $ADODB_SESS_CONN;
        
        $db_type = "' . $_SESSION['db_type'] . '";
        $db_user = "' . $_SESSION['db_user'] . '";
        $db_password = "' . $_SESSION['db_password'] . '";
        $db_database = "' . $_SESSION['db_database'] . '";
        $db_server = "' . $_SESSION['db_server'] . '";
        $config["table_prefix_no_lang"] = "' . $_SESSION['table_prefix'] . '";
        
        if(session_id()){
          session_unset();
          session_write_close();
        }

        // this is the setup for the ADODB library
        require_once "' . $_SESSION['basepath'] . '/autoloader.php";
        include_once("' . $_SESSION['basepath'] . '/vendor/adodb/adodb-php/adodb.inc.php");
        require_once "' . $_SESSION['basepath'] . '/vendor/adodb/adodb-php/session/adodb-cryptsession2.php";
        
        //establish Legacy DB connection
        $ORconn = ADONewConnection($db_type);
        if (is_bool($ORconn)) {
            header("Location: http://localhost:8100/500.shtml");
            die;
        }
        /** @psalm-suppress InvalidArgument */
        $ORconn->setConnectionParameter(MYSQLI_SET_CHARSET_NAME, "utf8");
        $ORconn->connect($db_server, $db_user, $db_password, $db_database);
        
        // Set Session Table and Connection
        $ADODB_SESSION_TBL = $config["table_prefix_no_lang"] . "sessions";
        $ADODB_SESS_CONN = $ORconn;
        $sql = "SET SESSION sql_mode = \'\'"; // To prevent errors from servers running sql_mode = ANSI
        $recordSet = $ORconn->Execute($sql);
        ';

            // End of File Content now write it
            $common = '../include/common.php';
            $out = fopen($_SESSION['basepath'] . '/include/common.php', 'w');
            fwrite($out, $filecontent, strlen($filecontent));
            fclose($out);
            @chmod($_SESSION['basepath'] . 'include/common.php', 0644);

            $commonIniContents = '
;<?php die(); ?>
[database]
db_type      =  mysql
db_server    = "' . $_SESSION['db_server'] . '";
db_name      = "' . $_SESSION['db_database'] . '";
db_user      = "' . $_SESSION['db_user'] . '";
db_password  = "' . $_SESSION['db_password'] . '";
table_prefix = "' . $_SESSION['table_prefix'] . '";
';
            $commonIni = dirname(__FILE__, 2) . '/api/common.ini.php';

            $out = fopen($commonIni, 'w');
            fwrite($out, $commonIniContents, strlen($commonIniContents));
            fclose($out);
            @chmod($commonIni, 0644);

            // File has been writen
            //Rename plugin files if they don't exist
            $userplg = '../plugins/user_plg.inc.php';
            $userplgdist = '../plugins/user_plg.dist.php';
            if (!file_exists($userplg)) {
                @rename($userplgdist, $userplg);
            }
            $loginplg = '../plugins/login_plg.inc.php';
            $loginplgdist = '../plugins/login_plg.dist.php';
            if (!file_exists($loginplg)) {
                @rename($loginplgdist, $loginplg);
            }
            $listingplg = '../plugins/listing_plg.inc.php';
            $listingplgdist = '../plugins/listing_plg.dist.php';
            if (!file_exists($listingplg)) {
                @rename($listingplgdist, $listingplg);
            }

            if (!isset($_SESSION['autoinstall'])) {
                $message = '<strong>' . $this->lang['install_settings_saved'] . ' <a href="index.php?step=6">' . $this->lang['install_continue_db_setup'] . '</a></strong>';
                echo $message;
            }
        }
    }

    /**
     * Gets the correct basepath for this Open-Realty install
     *
     * @return string
     */
    public function getBasePath(): string
    {
        $physical = substr(dirname(__FILE__), 0, -8);
        return str_replace('\\', '/', $physical);
    }

    /**
     * Reads the current Open-Realty version from the controlpanel_version field in the database.
     *
     * @return string
     */
    public function getPreviousVersion(): string
    {
        global $lang;
        $ORconn = $this->buildConn();

        // Database connection made.
        echo $this->lang['install_get_old_version'] . '<br />';
        $sql = 'SELECT controlpanel_version 
            FROM ' . $_SESSION['table_prefix'] . 'controlpanel';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            die('ERROR: ' . $sql);
        }
        // Loop throught Control Panel and save to Array
        $old_version = strval($recordSet->fields('controlpanel_version'));
        if ($old_version == '') {
            echo $lang['install_get_old_version_error'];
            exit();
        }
        return $old_version;
    }

    /**
     * Determines current baseurl from $_SERVER urls.
     *
     * @return string
     */
    public function getBaseUrl(): string
    {
        if (isset($_SERVER['PHP_SELF']) && isset($_SERVER['HTTP_HOST'])) {
            $me = $_SERVER['PHP_SELF'];


            $Apathweb = explode('/', $me);
            $myFileName = array_pop($Apathweb);
            $pathweb = implode('/', $Apathweb);
            if (
                (isset($_SERVER['HTTPS']) &&
                    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1)) ||
                (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
                    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
            ) {
                $http_proto = 'https://';
            } else {
                $http_proto = 'http://';
            }
            $myURL = $http_proto . $_SERVER['HTTP_HOST'] . $pathweb . '/' . $myFileName;
            // this is so you can verify the results:
            return substr($myURL, 0, -18);
        }
        return '';
    }

    /**
     * Check list of files to see if they are writeable
     *
     * @param string[] $files A List of file paths to check.
     *
     * @return boolean
     */
    public function checkFilePermissions(array $files): bool
    {
        $permission_pass = true;
        foreach ($files as $file) {
            if (is_writeable($file)) {
                echo '' . $this->lang['install_Permission_on'] . ' ' . $file . ' ' . $this->lang['install_are_correct'] . '<br>';
            } else {
                echo '' . $this->lang['install_Permission_on'] . ' ' . $file . ' ' . $this->lang['install_are_incorrect'] . ' (' . substr(sprintf('%o', fileperms($file)), -3) . ')<br>';
                $permission_pass = false;
            }
        }
        return $permission_pass;
    }

    /**
     * Get OS Type from SERVER_SOFTWARE
     *
     * @return string
     */
    public function osType(): string
    {
        $OS = 'Linux';
        if (isset($_SERVER['SERVER_SOFTWARE'])) {
            // Get OS
            $test1 = explode('Win32', $_SERVER['SERVER_SOFTWARE']);
            $test2 = explode('Microsoft', $_SERVER['SERVER_SOFTWARE']);
            $test3 = explode('BadBlue', $_SERVER['SERVER_SOFTWARE']);
            // REMOVED EMPTY $OS = ""; else will always catch if the if fails so no point in defining it here.
            if (count($test1) > 1 || count($test2) > 1 || count($test3) > 1) {
                $OS = 'Windows';
            }
        }
        return $OS;
    }

    /**
     * Checks if current PHP version is greater then string specified.
     *
     * @param string $requiredPHPVersion A PHP Version String to compare against.
     *
     * @return boolean
     */
    public function checkPHPVersion(string $requiredPHPVersion): bool
    {
        // Check PHP Version
        if (version_compare(PHP_VERSION, $requiredPHPVersion, '<')) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if MySQL is version 5 or higher
     *
     * @return boolean
     */
    public function checkMySQLVersion(): bool
    {
        $ORconn = $this->buildConn();
        $appmysql = '5.0';
        $sql = 'SELECT VERSION()';
        $rs = $ORconn->Execute($sql);
        if (is_bool($rs)) {
            die("<strong><span style=\"red\">ERROR - $sql</span></strong><br />");
        }
        $currentSQLversion = $rs->fields('VERSION()');
        return version_compare($currentSQLversion, $appmysql, '>=');
    }

    /**
     * Echos out the install page header.
     */
    public function showHeader(): void
    {
        echo '<html>
      <head>
      <title>Open-Realty® ' . $this->version_number . 'Install</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <style type="text/css">
      .warnings_message {color:#FF0000; font-weight:bold; padding:5px 0 5px 0;}
      </style>
      </head>
      <body style="text-align:center;width:100%;">
      <div style="width:600px;margin-left:auto;margin-right:auto;text-align:left;">
      <div style="width:600px;height:100px;background-repeat:no-repeat;background-image:url(\'logo.png\');display:block;background-color:#88909B;background-position:25px center;"></div>
      <div style="border:1px solid #88909B;padding:5px;">';
    }

    /**
     * Echos out the install page footer.
     */
    public function showFooter(): void
    {
        echo '</div></div></body></html>';
    }

    /**
     * Loads the language file for specified lang.
     *
     * @param string $lang Language to load. Eg. "en".
     */
    public function loadLang(string $lang): void
    {
        if (ctype_alpha($lang) && is_file(dirname(__FILE__) . '/language/' . $lang . '/lang.inc.php')) {
            /**
             * @psalm-suppress UnresolvableInclude
             */
            include dirname(__FILE__) . '/language/' . $lang . '/lang.inc.php';
            /** @var string[] $lang */
            $this->lang = $lang;
        }
    }

    /**
     * Drops all open-realty tables from the database.
     * This function is ony used for integration testing
     *
     * @param string $type     Database Type.
     * @param string $server   Server Type.
     * @param string $user     Database User.
     * @param string $pass     Database Password.
     * @param string $database Database Name.
     */
    public function dropAllTables(string $type, string $server, string $user, string $pass, string $database): void
    {
        $_SESSION['db_type'] = trim($type);
        $_SESSION['db_user'] = trim($user);
        $_SESSION['db_password'] = trim($pass);
        $_SESSION['db_database'] = trim($database);
        $_SESSION['db_server'] = trim($server);
        $ORconn = $this->buildConn();
        $sql = 'DROP TABLE IF EXISTS `default_addons`, `default_auth_tokens`, `default_blogcategory_relation`, `default_blogpingbacks`, `default_blogtag_relation`, `default_classformelements`, `default_controlpanel`, `default_controlpanel_seo`, `default_controlpanel_rss`, `default_controlpanel_template`, `default_en_activitylog`, `default_en_agentformelements`, `default_en_blogcategory`, `default_en_blogcomments`, `default_en_blogmain`, `default_en_blogtags`, `default_en_class`, `default_en_feedbackdb`, `default_en_feedbackdbelements`, `default_en_feedbackformelements`, `default_en_listingsdb`, `default_en_listingsdbelements`, `default_en_listingsfiles`, `default_en_listingsformelements`, `default_en_listingsimages`, `default_en_listingsvtours`, `default_en_memberformelements`, `default_en_menu`, `default_en_menu_items`, `default_en_pagesmain`, `default_en_userdb`, `default_en_userdbelements`, `default_en_userfavoritelistings`, `default_en_userimages`, `default_en_usersavedsearches`, `default_en_usersfiles`, `default_forgot`, `default_open-realty_license`, `default_seouri`, `default_sessions`, `default_tracking`, `default_zipdist`;';

        $recordSet = $ORconn->Execute($sql);
        if ($recordSet === false) {
            if ($_SESSION['devel_mode'] == 'no') {
                die("<strong><span style=\"red\">ERROR - $sql</span></strong><br />");
            } else {
                echo "<strong><span style=\"red\">ERROR - $sql</span></strong><br />";
            }
        }
    }

    /**
     * Primary install function that determines correct step and calls the correct funtions.
     */
    public function runInstaller(): void
    {
        if (isset($_GET['or_install_type'])) {
            $_SESSION['or_install_type'] = $_GET['or_install_type'];
        }
        if (isset($_GET['or_install_lang']) && is_string($_GET['or_install_lang'])) {
            $_SESSION['or_install_lang'] = $_GET['or_install_lang'];
        }

        if (isset($_GET['step']) && isset($_SESSION['or_install_type'])) {
            switch ($_SESSION['or_install_type']) {
                case 'Installer':
                    $version = new Installer();
                    break;
                case 'Upgrader':
                    $version = new Upgrader();
                    break;
                case 'Mover':
                    $version = new Mover();
                    break;
                default:
                    die('<p>Unexpected Install Type</p>');
            }
            if ($_GET['step'] == 'autoupdate') {
                $this->loadLang($_SESSION['or_install_lang']);
                $version->loadVersion();
            } elseif ($_GET['step'] == 'autoinstall') {
                $this->loadLang($_SESSION['or_install_lang']);
                $version->loadVersion();
            } elseif ($_GET['step'] > 4) {
                $this->loadLang($_SESSION['or_install_lang']);
                $version->loadVersion();
            } else {
                if (is_string($_GET['step'])) {
                    $runme = 'runInstaller' . $_GET['step'];
                    $this->$runme();
                }
            }
        } elseif (isset($_GET['step']) && is_string($_GET['step'])) {
            $runme = 'runInstaller' . $_GET['step'];
            $this->$runme();
        } else {
            $this->runInstaller0();
        }
    }

    /**
     * Install Step 0 that prompts for license agreement.
     */
    public function runInstaller0(): void
    {
        echo '<p><strong>Before you may install Open-Realty®, You must first read and agree to the following license agreement!</strong></p>';
        echo str_replace("\n", '<br>', file_get_contents(dirname(__FILE__) . '/../license.txt'));
        echo '<form name="form1" method="post" action="index.php?step=1">
            <p class="align-center"><input type="submit" name="Submit" value="I Agree" />
              <input type="Reset" name="Reset" value="I don\'t Agree" />
            </p>
          </form>';
    }

    /**
     * Install step 2, prompts for language selection
     */
    public function runInstaller1(): void
    {
        $dirs = array_filter(glob('language/*'), 'is_dir');
        $dirs = str_replace('language/', '', $dirs);
        echo '<form name="form1" method="post" action="index.php?step=2">
          Language:
          <select name="lang">';
        $langs = [];
        foreach ($dirs as $lang) {
            $langs[$lang] = $this->returnLangs($lang);
        }
        asort($langs);
        foreach ($langs as $key => $value) {
            echo '<option value="' . $key . '">' . $value . '</option>';
        }
        echo '</select>
          <input type="submit" name="Submit" value="Submit" />
    </form>';
    }

    /**
     * Returns a locale name for the selected language.
     *
     * @param string $lang The 2 char language. Eg. "en".
     *
     * @return string
     */
    private function returnLangs(string $lang): string
    {
        $lang_text = 'UnKnown';
        switch ($lang) {
            case 'en':
                $lang_text = 'English';
                break;
            case 'es':
                $lang_text = 'Español';
                break;
            case 'br':
                $lang_text = 'Português de Brasil';
                break;
            case 'pt':
                $lang_text = 'Português';
                break;
        }
        return $lang_text;
    }

    /**
     * Installer step 2. Shows any PHP version or configuration errors that block the install.
     */
    public function runInstaller2(): void
    {
        // Load Lang
        if (!isset($_POST['lang']) || !is_string($_POST['lang'])) {
            die('Lang not set.');
        }
        $_SESSION['or_install_lang'] = $_POST['lang'];
        $this->loadLang($_SESSION['or_install_lang']);
        // Show welcome text
        echo '<h2>' . $this->lang['install_welcome'] . '</h2>
      <p>' . $this->lang['install_intro'] . '</p>

      <p>' . $this->lang['install_step_one_header'] . '</p>
      ';
        $files = [];
        // Check PHP version
        $check_for_version = "8.0.0";
        $php_version = $this->checkPHPVersion($check_for_version);
        if ($php_version) {
            $common = '../include/common.php';
            if (file_exists($common)) {
                $files[] = '../include/common.php';
            } else {
                $files[] = '../include';
            }
            $files[] = '../images/listing_photos';
            $files[] = '../images/user_photos';
            $files[] = '../images/vtour_photos';
            $files[] = '../images/page_upload';
            $files[] = '../images/blog_uploads';
            $files[] = '../files/listings';
            $files[] = '../files/users';
            $files[] = '../addons';
            $files[] = '../files/browsercap_cache';
            $files[] = '../files/download_cache';
            $file_perm = $this->checkFilePermissions($files);
            //Check Required Moduels
            $warnings = [];
            $warnings_warn = [];
            if (ini_get('safe_mode')) {
                $warnings[] = '<div class="warnings_message">' . $this->lang['warnings_safe_mode'] . '</div>';
            }
            //CHECK session.auto-start
            if (ini_get('session.auto_start')) {
                $warnings[] = '<div class="warnings_message">PHP session.auto_start is enabled. Disable this option to continue</div>';
            }
            // CHECK MBString
            if (!extension_loaded('mbstring')) {
                $warnings[] = '<div class="warnings_message">' . $this->lang['warnings_mb_convert_encoding'] . '</div>';
            }
            if (!extension_loaded('curl')) {
                $warnings[] = '<div class="warnings_message">' . $this->lang['curl_not_enabled'] . '</div>';
            }
            if (!extension_loaded('zip')) {
                $warnings[] = '<div class="warnings_message">' . $this->lang['warnings_php_zip'] . '</div>';
            }
            if (!extension_loaded('gd')) {
                $warnings[] = '<div class="warnings_message">' . $this->lang['warnings_php_gd'] . '</div>';
            } else {
                $gdinfo = gd_info();
                if (!isset($gdinfo['FreeType Support']) || !$gdinfo['FreeType Support']) {
                    $warnings[] = '<div class="warnings_message">' . $this->lang['warnings_php_freetype'] . '</div>';
                }
            }
            if (!extension_loaded('exif')) {
                $warnings[] = '<div class="warnings_message">' . $this->lang['warnings_php_exif'] . '</div>';
            }
            if (!extension_loaded('zip')) {
                $warnings[] = '<div class="warnings_message">' . $this->lang['warnings_php_zip'] . '</div>';
            }
            if (!extension_loaded('pdo')) {
                $warnings[] = '<div class="warnings_message">' . $this->lang['warnings_php_pdo'] . '</div>';
            }
            if (!extension_loaded('pdo_mysql')) {
                $warnings[] = '<div class="warnings_message">' . $this->lang['warnings_php_pdo_mysql'] . '</div>';
            }

            // CHECK OpenSSL
            if (!in_array('openssl', get_loaded_extensions())) {
                $warnings[] = '<div class="warnings_message">' . $this->lang['warnings_openssl'] . '</div>';
            }
            // CHECK mod_rewrite AND htaccess file

            if (defined('APACHE_GET_MODULES')) {
                if (!in_array('mod_rewrite', apache_get_modules())) {
                    $warnings_warn[] = '<div class="warnings_message">' . $this->lang['warnings_mod_rewrite'] . '</div>';
                }
                if (!file_exists('../.htaccess')) {
                    $warnings_warn[] = '<div class="warnings_message">' . $this->lang['warnings_htaccess'] . '</div>';
                }
            }
            $physical = $this->getBasePath();
            //echo $physical;
            if (str_contains($physical, ' ')) {
                $warnings[] = '<div class="warnings_message">' . $this->lang['file_path_contains_a_space'] . '</div>';
            }

            if ($file_perm === true && count($warnings) == 0) {
                if (count($warnings_warn) > 0) {
                    foreach ($warnings_warn as $warn) {
                        echo $warn;
                    }
                    echo '<br /> <form action="index.php?step=3" method="post"><input type="submit" name="Submit" value="' . $this->lang['install_continue'] . '" /></form>';
                } else {
                    echo '<br /><strong>' . $this->lang['install_all_correct'] . '</strong> <form action="index.php?step=3" method="post"><input type="submit" name="Submit" value="' . $this->lang['install_continue'] . '" /></form>';
                }
            } else {
                if ($file_perm !== true) {
                    echo '<br /><strong>' . $this->lang['install_please_fix'] . '</strong>';
                } elseif (count($warnings) > 0) {
                    foreach ($warnings as $warn) {
                        echo $warn;
                    }
                    echo '<br /><strong>' . $this->lang['install_please_fix'] . '</strong>';
                }
            }
        } else {
            echo '<span style="color: red"><strong>' . $this->lang['install_version_warn'] . '</strong></span><br />';
            echo '<strong>' . $this->lang['install_php_version'];
            print phpversion();
            echo $this->lang['install_php_required'] . ' ' . $check_for_version . '</strong>';
        }
    }

    /**
     * Install Step 3 shows install type selection. Install, Upgrade, Move
     */
    public function runInstaller3(): void
    {
        $this->loadLang($_SESSION['or_install_lang']);
        echo '<form name="install_type_form" method="post" action="index.php?step=4">';
        echo $this->lang['install_select_type'] . ' <select name="install_type">';
        // install options\
        echo '<option value="Installer">' . $this->lang['install_new'] . ' ' . $this->version_number . '</option>';
        echo '<option value="Upgrader">' . $this->lang['upgrade_200'] . '</option>';
        //echo'<option value="upgrade_115">' . $this->lang['upgrade_115'] . '</option>';
        echo '<option value="Mover">' . $this->lang['move'] . '</option>';

        echo '</select>
      <input type="submit" name="Submit" value="Submit" />
      </form>';
    }

    /**
     * Install Step 4 Load Previous Settings if they exist and call getNewSettings
     */
    public function runInstaller4(): void
    {
        if (isset($_POST['install_type'])) {
            $_SESSION['or_install_type'] = $_POST['install_type'];
        }

        $this->loadLang($_SESSION['or_install_lang']);
        $this->loadPrevSettings();
    }

    /**
     * Load Previous Settings if they exist.
     *
     * @param boolean $return Should we return current setting as a array or call getNetSettings.
     *
     * @return null|string[]
     */
    public function loadPrevSettings(bool $return = false): ?array
    {
        if (is_file(dirname(__FILE__) . '/../include/common.php')) {
            $old_file = file_get_contents(dirname(__FILE__) . '/../include/common.php');
            // Get values
            preg_match('/\$db_type = "(.*?)"/', $old_file, $old_db_type);
            preg_match('/\$db_server = "(.*?)"/', $old_file, $old_db_server);
            preg_match('/\$db_database = "(.*?)"/', $old_file, $old_db_name);
            preg_match('/\$db_user = "(.*?)"/', $old_file, $old_db_user);
            preg_match('/\$db_password = "(.*?)"/', $old_file, $old_db_password);
            preg_match('/\$config\["table_prefix_no_lang"\] = "(.*?)"/', $old_file, $old_table_prefix);
            if (!$return) {
                $this->getNewSettings($old_db_server[1], $old_db_name[1], $old_db_user[1], $old_db_password[1], $old_table_prefix[1]);
            } else {
                return ['db_type' => $old_db_type[1], 'db_server' => $old_db_server[1], 'db_database' => $old_db_name[1], 'db_user' => $old_db_user[1], 'db_password' => $old_db_password[1], 'table_prefix' => $old_table_prefix[1]];
            }
        } else {
            if (!$return) {
                $this->getNewSettings();
            } else {
                return ['db_type' => '', 'db_server' => '', 'db_database' => '', 'db_user' => '', 'db_password' => '', 'table_prefix' => ''];
            }
        }
        return null;
    }

    /**
     * Function should be overridden by classes that extend that need to create Tables
     */
    public function createTables(string $old_version = ''): void
    {
    }

    /**
     * Function should be overridden by classes that extend that need to update Tables
     *
     * @param string $old_version Previous Version of Open-Realty that we are upgrading from.
     */
    public function updateTables(string $old_version = ''): void
    {
    }

    /**
     * Function should be overridden by classes that extend that need to create indexes on tables
     *
     * @param string $old_version Previous Version of Open-Realty that we are upgrading from.
     */
    public function createIndex(string $old_version = ''): void
    {
    }

    /**
     * Function should be overridden by classes that extend that need to insert values in the database
     *
     * @param string $old_version Previous Version of Open-Realty that we are upgrading from.
     */
    public function insertValues(string $old_version = ''): void
    {
    }

    /**
     * Runs an array of sql commands against the database
     *
     * @param string[] $sql_insert Array of SQL Commands.
     * @param boolean  $SilentFail Boolean if we should fail silently or die on error.
     */
    public function runSQL(array $sql_insert = [], bool $SilentFail = false): void
    {
        if (empty($sql_insert)) {
            return;
        }
        $ORconn = $this->buildConn();

        foreach ($sql_insert as $elementContents) {
            $recordSet = $ORconn->Execute($elementContents);
            if ($recordSet === false) {
                if ($_SESSION['devel_mode'] == 'no') {
                    if (!$SilentFail) {
                        die("<strong><span style=\"red\">ERROR - $elementContents</span></strong><br />");
                    }
                } else {
                    echo "<strong><span style=\"red\">ERROR - $elementContents</span></strong><br />";
                }
            }
        }
    }
}
