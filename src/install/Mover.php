<?php

declare(strict_types=1);

namespace OpenRealty\Install;

/**
 * Mover class handles updating baseurl & basepath on migrations
 */
class Mover extends BaseInstall
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Updates controlpanel table with new basepath & paseurl
     *
     * @param string $old_version Previous version of Open-realty we are runnings.
     */
    public function updateTables(string $old_version = ''): void
    {
        // this is the setup for the ADODB library
        include dirname(__FILE__) . '/../vendor/adodb/adodb-php/adodb.inc.php';

        $ORconn = ADONewConnection($_SESSION['db_type']);
        if (is_bool($ORconn)) {
            die('Failed to Establish Database Connection');
        }
        /** @psalm-suppress InvalidArgument */
        $ORconn->setConnectionParameter(MYSQLI_SET_CHARSET_NAME, 'utf8');
        $ORconn->connect($_SESSION['db_server'], $_SESSION['db_user'], $_SESSION['db_password'], $_SESSION['db_database']);
        $config = [];
        //$config['table_prefix'] = $_SESSION['table_prefix'] . $_SESSION['or_install_lang'] . '_';
        $config['table_prefix_no_lang'] = $_SESSION['table_prefix'];
        $sql_insert = [];
        $sql_insert[] = 'UPDATE  ' . $config['table_prefix_no_lang'] . "controlpanel 
						SET  controlpanel_basepath ='" . trim($_SESSION['basepath']) . "', 
							controlpanel_baseurl = '" . trim($_SESSION['baseurl']) . "'";
        foreach ($sql_insert as $elementIndexValue => $elementContents) {
            $recordSet = $ORconn->Execute($elementContents);
            if (is_bool($recordSet)) {
                die("<strong><span style=\"red\">ERROR - $elementContents</span></strong><br />");
            }
        }
    }

    /**
     * Run individual steps for moves.
     */
    public function loadVersion(): void
    {
        $this->loadLang($_SESSION['or_install_lang']);
        switch ($_GET['step']) {
            case 5:
                if (
                    !isset(
                        $_POST['table_prefix'],
                        $_POST['db_type'],
                        $_POST['db_user'],
                        $_POST['db_password'],
                        $_POST['db_database'],
                        $_POST['db_server'],
                        $_POST['basepath'],
                        $_POST['baseurl'],
                    )
                    || !is_string($_POST['table_prefix'])
                    || !is_string($_POST['db_type'])
                    || !is_string($_POST['db_user'])
                    || !is_string($_POST['db_password'])
                    || !is_string($_POST['db_database'])
                    || !is_string($_POST['db_server'])
                    || !is_string($_POST['basepath'])
                    || !is_string($_POST['baseurl'])
                ) {
                    die('Missing Paramaters');
                }
                $_SESSION['table_prefix'] = trim($_POST['table_prefix']);
                $_SESSION['db_type'] = trim($_POST['db_type']);
                $_SESSION['db_user'] = trim($_POST['db_user']);
                $_SESSION['db_password'] = trim($_POST['db_password']);
                $_SESSION['db_database'] = trim($_POST['db_database']);
                $_SESSION['db_server'] = trim($_POST['db_server']);
                $_SESSION['basepath'] = trim($_POST['basepath']);
                $_SESSION['baseurl'] = trim($_POST['baseurl']);
                $this->writeConfig();
                break;
            case 6:
                $this->updateTables();
                $this->createTables();
                $this->createIndex();
                $this->insertValues();
                echo '<br /><strong>' . $this->lang['install_installation_complete'] . ' <a href="../admin/index.php?action=configure">' . $this->lang['install_configure_installation'] . '</a></strong>';
                break;
        }
    }
}
