<?php

declare(strict_types=1);

namespace OpenRealty\Install;

require_once dirname(__FILE__) . "/../autoloader.php";

if (file_exists(dirname(__FILE__) . '/../c3.php')) {
    /** @psalm-suppress MissingFile */
    include_once dirname(__FILE__) . '/../c3.php';
}
error_reporting(E_ALL ^ E_NOTICE);
// just so we know it is broken
session_start([
    "cookie_httponly" => true,
    "cookie_samesite" => "Lax",
    "use_strict_mode" => true,
    "sid_bits_per_character" => 6,
]);

$installer = new BaseInstall();
$installer->showHeader();
$installer->runInstaller();
$installer->showFooter();
