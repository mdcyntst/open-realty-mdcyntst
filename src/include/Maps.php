<?php

declare(strict_types=1);

namespace OpenRealty;

class Maps extends BaseClass
{
    /**
     * maps::createMapLink()
     * This is the function to call to show a map link. It should be called from the listing detail page, or any page
     * where $_GET['listingID'] is set. This function then calls the appropriate make_mapname function as specified in
     * the configuration.
     *
     * @return string Return the URL for the map as long as the required fields are filled out, if not it returns a
     *                empty string.
     */
    public function createMapLink(string $url_only = 'no', string $rokbox = 'no'): string
    {
        $listing_pages = $this->newListingPages();

        $display = '';
        $listingsdb_id = intval($_GET['listingID']);
        $listing_title = urlencode($listing_pages->getListingSingleValue('listingsdb_title', $listingsdb_id));
        $address1 = urlencode($listing_pages->getListingSingleValue($this->config['map_address'], $listingsdb_id));
        $address2 = urlencode($listing_pages->getListingSingleValue($this->config['map_address2'], $listingsdb_id));
        $address3 = urlencode($listing_pages->getListingSingleValue($this->config['map_address3'], $listingsdb_id));
        $address = urlencode($address1) . ' ' . urlencode($address2) . ' ' . urlencode($address3);
        $city = urlencode($listing_pages->getListingSingleValue($this->config['map_city'], $listingsdb_id));
        $state = urlencode($listing_pages->getListingSingleValue($this->config['map_state'], $listingsdb_id));
        $zip = urlencode($listing_pages->getListingSingleValue($this->config['map_zip'], $listingsdb_id));
        $country = urlencode($listing_pages->getListingSingleValue($this->config['map_country'], $listingsdb_id));

        if ($address != '' || $city != '' || $state != '' || $zip != '') {
            if (str_contains($this->config['map_type'], 'global_')) {
                if (str_contains($this->config['map_type'], 'mapquest')) {
                    $display = $this->makeMapquest($country, $address, $city, $state, $zip, $listing_title, $url_only, $rokbox);
                } elseif (str_contains($this->config['map_type'], 'multimap')) {
                    $display = $this->makeMultimap($country, $address, $city, $state, $zip, $listing_title, $url_only, $rokbox);
                }
            } elseif (str_contains($this->config['map_type'], 'mapquest')) {
                $country = substr($this->config['map_type'], -2);
                $display = $this->makeMapquest($country, $address, $city, $state, $zip, $listing_title, $url_only, $rokbox);
            } elseif (str_contains($this->config['map_type'], 'multimap')) {
                $country = substr($this->config['map_type'], -2);
                $display = $this->makeMultimap($country, $address, $city, $state, $zip, $listing_title, $url_only, $rokbox);
            } elseif (str_contains($this->config['map_type'], 'yahoo')) {
                $display = $this->makeYahooUS($address, $city, $state, $zip, $listing_title, $url_only, $rokbox);
            } elseif (str_contains($this->config['map_type'], 'google')) {
                $display = $this->makeGoogleUS($address, $city, $state, $zip, $listing_title, $url_only, $rokbox);
            }
        }
        return $display;
    }

    public function makeMapquest(string $country, string $address, string $city, string $state, string $zip, string $listing_title, string $url_only, string $rokbox): string
    {
        // renders a link to yahoo maps on the page
        global $lang;
        $mapquest_string = "country=$country&amp;addtohistory=&amp;address=$address&amp;city=$city&amp;zipcode=$zip";
        if ($url_only == 'no') {
            $display = "<a href=\"http://www.mapquest.com/maps/map.adp?$mapquest_string\" onclick=\"window.open(this.href,'_blank','location=0,status=0,scrollbars=1,toolbar=0,menubar=0,width=800,height=600,noopener,noreferrer');return false\">$lang[map_link]</a>";
        } else {
            $display = "https://www.mapquest.com/maps/map.adp?$mapquest_string";
        }
        return $display;
    }

    public function makeYahooUS(string $address, string $city, string $state, string $zip, string $listing_title, string $url_only, string $rokbox): string
    {
        global $lang;
        $yahoo_string = "Pyt=Tmap&amp;addr=$address&amp;csz=$city,$state,$zip&amp;Get+Map=Get+Map";
        if ($url_only == 'no') {
            $display = "<a href=\"http://maps.yahoo.com/py/maps.py?$yahoo_string\" onclick=\"window.open(this.href,'_blank','location=0,status=0,scrollbars=1,toolbar=0,menubar=0,width=800,height=600,noopener,noreferrer');return false\">$lang[map_link]</a>";
        } else {
            $display = "http://maps.yahoo.com/py/maps.py?$yahoo_string";
        }
        return $display;
    }

    public function makeGoogleUS(string $address, string $city, string $state, string $zip, string $listing_title, string $url_only, string $rokbox): string
    {
        global $lang;
        $google_string = "maps?q=loc:$address%20$city%20$state%20$zip%20($listing_title)";
        if ($url_only == 'no') {
            $display = "<a href=\"https://maps.google.com/$google_string\" onclick=\"window.open(this.href,'_blank','location=0,status=0,scrollbars=1,toolbar=0,menubar=0,width=800,height=600,noopener,noreferrer');return false\">$lang[map_link]</a>";
        } else {
            $display = "http://maps.google.com/$google_string";
        }
        return $display;
    }

    public function makeMultimap(string $country, string $address, string $city, string $state, string $zip, string $listing_title, string $url_only, string $rokbox): string
    {
        // renders a link to multi map on the page
        global $lang;
        $multimap_string = "&amp;db=$country&amp;addr2=$address&amp;addr3=$city&amp;pc=$zip";
        if ($url_only == 'no') {
            $display = '<a href="https://www.multimap.com/map/places.cgi?client=public' . $multimap_string . '" target="_map">' . $lang['map_link'] . '</a>';
        } else {
            $display = 'http://www.multimap.com/map/places.cgi?client=public' . $multimap_string;
        }
        return $display;
    }
}
