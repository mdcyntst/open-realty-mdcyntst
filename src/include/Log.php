<?php

declare(strict_types=1);

namespace OpenRealty;

class Log extends BaseClass
{
    public function ajaxViewlogDatatable(): string
    {
        global $ORconn;
        $misc = $this->newMisc();
        header('Content-type: application/json');
        $aColumns = ['activitylog_id', 'activitylog_log_date', 'activitylog_ip_address', 'userdb_id', 'activitylog_action'];
        $ARGS = [];
        //Do Search to get total record count, no need pass in soring information
        $limit = 0;
        $offset = 0;
        $sql = 'SELECT count(*) as mycount FROM ' . $this->config['table_prefix'] . 'activitylog';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $iTotal = $recordSet->fields('mycount');
        if (isset($_GET['start']) && $_GET['length'] != '-1') {
            $limit = intval($_GET['length']);
            $offset = intval($_GET['start']);
            //          $sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".mysql_real_escape_string( $_GET['iDisplayLength'] );
        }
        $sortby = [];
        $sorttype = [];
        //Deal with sorting
        if (isset($_GET['order'])) {
            for ($i = 0; $i < intval($_GET['order']); $i++) {
                if (isset($_GET['order'][$i]['column']) && isset($_GET['order'][$i]['dir'])) {
                    $sortby[$i] = $aColumns[intval($_GET['order'][$i]['column'])];
                    $sorttype[$i] = strtoupper($_GET['order'][$i]['dir']);
                }
            }
        }
        $sWhere = '';
        if (isset($_GET['search']['value']) && is_string($_GET['search']['value']) && $_GET['search']['value'] != '') {
            $sql_sSearch = $ORconn->qstr('%' . $_GET['search']['value'] . '%');

            $sWhere = 'WHERE (';
            for ($i = 0; $i < count($aColumns); $i++) {
                if (isset($aColumns[$i])) {
                    if ($aColumns[$i] == 'userdb_id') {
                        $sWhere .= '`' . $aColumns[$i] . '` IN (
														SELECT userdb_id 
														FROM ' . $this->config['table_prefix'] . "userdb 
														WHERE CONCAT(userdb_user_last_name,', ',userdb_user_first_name) 
														LIKE " . $sql_sSearch . ') OR ';
                    } else {
                        $sWhere .= '`' . $aColumns[$i] . '` LIKE ' . $sql_sSearch . ' OR ';
                    }
                }
            }
            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }
        //Deal with column filters
        for ($i = 0; $i < count($aColumns); $i++) {
            if (isset($aColumns[$i]) && isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == 'true' && in_array('sSearch_' . $i, $_GET) && $_GET['sSearch_' . $i] != '') {
                $ARGS[$aColumns[$i]] = $_GET['sSearch_' . $i];
            }
        }
        $where = '';
        if (!empty($ARGS)) {
            if ($sWhere == '') {
                $where .= ' WHERE ';
            }
            foreach ($ARGS as $f => $k) {
                if (is_scalar($k)) {
                    if ($where != ' WHERE ') {
                        $where .= ' AND ';
                    }
                    $where .= $ORconn->addQ($f) . ' LIKE \'%' . $ORconn->addQ($k) . '%\'';
                }
            }
        }
        $sort = '';
        if (!empty($sortby)) {
            $sort .= ' ORDER BY ';
            foreach ($sortby as $x => $f) {
                if ($sort != ' ORDER BY ') {
                    $sort .= ', ';
                }
                $sort .= $ORconn->addQ($f) . ' ' . $ORconn->addQ($sorttype[$x]);
            }
        }
        $limitstr = '';
        if ($limit > 0) {
            $limitstr .= 'LIMIT ' . $offset . ', ' . $limit;
        }
        $sql = 'SELECT count(*) as filteredcount 
				FROM ' . $this->config['table_prefix'] . "activitylog $sWhere $where";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $iFilteredTotal = $recordSet->fields('filteredcount');
        $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . "activitylog $sWhere $where $sort $limitstr ";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $draw = 0;
        if (isset($_GET['draw'])) {
            $draw = intval($_GET['draw']);
        }
        $output = [
            'draw' => $draw,
            'recordsTotal' => $iTotal,
            'recordsFiltered' => $iFilteredTotal,
            'data' => [],
        ];
        while (!$recordSet->EOF) {
            $row = [];
            $row[] = $recordSet->fields('activitylog_id');
            $row[] = $recordSet->UserTimeStamp($recordSet->fields('activitylog_log_date'), 'D M j G:i:s T Y');
            $row[] = $recordSet->fields('activitylog_ip_address');
            $sqlUser = 'SELECT userdb_user_first_name, userdb_user_last_name
						FROM ' . $this->config['table_prefix'] . 'userdb
						WHERE userdb_id =' . $recordSet->fields('userdb_id');
            $recordSet2 = $ORconn->execute($sqlUser);
            if (is_bool($recordSet2)) {
                $misc->logErrorAndDie($sqlUser);
            }
            $first_name = $recordSet2->fields('userdb_user_first_name');
            $last_name = $recordSet2->fields('userdb_user_last_name');
            $row[] = $last_name . ', ' . $first_name;
            $row[] = $recordSet->fields('activitylog_action');
            $output['data'][] = $row;
            $recordSet->MoveNext();
        }
        return json_encode($output) ?: '';
    }

    public function view(string $app_status_text = ''): string
    {
        // Verify User is an Admin

        $login = $this->newLogin();
        $security = $login->verifyPriv('canViewLogs');
        $display = '';
        if ($security) {
            //Load the Core Template

            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/view_log.html');
            $page->replaceTag('application_status_text', $app_status_text);
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        }
        return $display;
    }

    public function clearLog(): string
    {
        global $ORconn, $lang;
        $misc = $this->newMisc();
        $display = '';
        //$display .= "<h3>$lang[log_delete]</h3>";
        // Check for Admin privs before doing anything
        if ($_SESSION['admin_privs'] == 'yes') {
            // find the number of log items
            $sql = 'TRUNCATE TABLE ' . $this->config['table_prefix'] . 'activitylog';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $misc->logAction($lang['log_reset']);
            $display .= $this->view($lang['log_cleared']);
        } else {
            $display .= $this->view($lang['clear_log_need_privs']);
        }
        return $display;
    }

    /**
     * @return string
     */
    public function ajaxExportLogs(): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('canViewLogs');

        if ($security === true) {
            $sql = 'SELECT * 
					FROM ' . $this->config['table_prefix'] . 'activitylog';
            $recordSet = $ORconn->Execute($sql);

            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }

            $headings = ['activitylog_id', 'activitylog_log_date', 'userdb_id', 'activitylog_action', 'activitylog_ip_address'];

            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename="activitylog.csv";');

            $fh = fopen('php://output', 'w');

            fputcsv($fh, $headings);


            while (!$recordSet->EOF) {
                fputcsv(
                    $fh,
                    [
                        $recordSet->fields('activitylog_id'),
                        $recordSet->fields('activitylog_log_date'),
                        $recordSet->fields('userdb_id'),
                        $recordSet->fields('activitylog_action'),
                        $recordSet->fields('activitylog_ip_address'),
                    ]
                );
                $recordSet->MoveNext();
            }

            fclose($fh);

            $csv = ob_get_clean();
            if (is_string($csv)) {
                return $csv;
            }
            return '';
        } else {
            return 'Unauthorized access';
        }
    }
}
