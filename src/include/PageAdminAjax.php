<?php

declare(strict_types=1);

namespace OpenRealty;

use Abraham\TwitterOAuth\TwitterOAuth;
use Exception;

class PageAdminAjax extends Page
{
    public function callAjax(): string
    {
        global $lang;

        $login = $this->newLogin();
        $login_status = $login->loginCheck('Agent');
        if ($login_status !== true) {
            die('Not Logged In');
        } else {
            if (isset($_GET['action'])) {
                switch ($_GET['action']) {
                    case 'ajax_update_page_post':
                        $class = $this->newPageFunctions();
                        return $class->ajaxUpdatePagePost();
                    case 'ajax_get_blog_post':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxGetBlogPost();
                    case 'ajax_get_page_post':
                        $class = $this->newPageFunctions();
                        return $class->ajaxGetPagePost();
                    case 'ajax_update_page_post_autosave':
                        $class = $this->newPageFunctions();
                        return $class->ajaxUpdatePagePostAutosave();
                    case 'ajax_delete_page_post':
                        $class = $this->newPageFunctions();
                        return $class->ajaxDeletePagePost();
                    case 'ajax_create_page_post':
                        $class = $this->newPageFunctions();
                        return $class->ajaxCreatePagePost();
                    case 'ajax_create_blog_post':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxCreateBlogPost();
                    case 'ajax_set_blog_cat':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxSetBlogCat();
                    case 'ajax_create_blog_category':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxCreateBlogCategory();
                    case 'ajax_update_blog_post':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxUpdateBlogPost();
                    case 'ajax_update_blog_post_autosave':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxUpdateBlogPostAutosave();
                    case 'ajax_delete_blog_post':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxDeleteBlogPost();
                    case 'ajax_show_store_addons':
                        $class = $this->newAddonManager();
                        return $class->ajaxShowStoreAddons();
                    case 'configure_general':
                        $class = $this->newConfigurator();
                        return $class->ajaxConfigureGeneral();
                    case 'configure_uploads':
                        $class = $this->newConfigurator();
                        return $class->ajaxConfigureUploads();
                    case 'configure_listings':
                        $class = $this->newConfigurator();
                        return $class->ajaxConfigureListings();
                    case 'configure_users':
                        $class = $this->newConfigurator();
                        return $class->ajaxConfigureUsers();
                    case 'configure_social':
                        $class = $this->newConfigurator();
                        return $class->ajaxConfigureSocial();
                    case 'configure_seo_links':
                        $class = $this->newConfigurator();
                        return $class->ajaxConfigureSeoLinks();
                    case 'configure_seo':
                        $class = $this->newConfigurator();
                        return $class->ajaxConfigureSeo();
                    case 'update_site_config':
                        $class = $this->newConfigurator();
                        return $class->ajaxUpdateSiteConfig();
                    case 'smtp_test':
                        $class = $this->newConfigurator();
                        try {
                            $r = $class->ajaxSmtpTest();
                            if (is_string($r)) {
                                return $r;
                            }
                        } catch (Exception) {
                        }
                        break;
                    case 'ajax_export_emails':
                        $class = $this->newConfigurator();
                        return $class->ajaxExportEmails();
                    case 'update_seouris':
                        $class = $this->newConfigurator();
                        return $class->ajaxUpdateSeoUris();
                    case 'configure_templates':
                        $class = $this->newConfigurator();
                        return $class->ajaxConfigureTemplates();
                    case 'edit_blog_post_categories':
                        $class = $this->newBlogEditor();
                        return $class->editBlogPostCategories();
                    case 'edit_blog_post_tags':
                        $class = $this->newBlogEditor();
                        return $class->editBlogPostTags();
                    case 'load_most_used_tags':
                        $class = $this->newBlogEditor();
                        return $class->editBlogPostTags(true);
                    case 'ajax_create_blog_tag':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxCreateBlogTag();
                    case 'ajax_remove_assigned_blog_tag':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxRemoveAssignedBlogTag();
                    case 'ajax_add_assigned_blog_tag_byid':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxAddAssignedBlogTagById();
                    case 'blog_settings_general':
                        $class = $this->newBlogEditor();
                        return $class->ajaxGeneralSettings();
                    case 'blog_settings_categories':
                        $class = $this->newBlogEditor();
                        return $class->ajaxGeneralCategories();
                    case 'blog_settings_tags':
                        $class = $this->newBlogEditor();
                        return $class->ajaxGeneralTags();
                    case 'ajax_rankup_blog_category':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxRankUpBlogCategory();
                    case 'ajax_rankdown_blog_category':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxRankDownBlogCategory();
                    case 'ajax_delete_blog_category':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxDeleteBlogCategory();
                    case 'ajax_get_category_info':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxGetCategoryInfo();
                    case 'ajax_update_category_info':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxUpdateCategoryInfo();
                    case 'ajax_get_listing_field_info':
                        $class = $this->newTemplateEditor();
                        return $class->ajaxGetListingFieldInfo();
                    case 'ajax_save_listing_field_order':
                        $class = $this->newTemplateEditor();
                        return $class->ajaxSaveListingFieldOrder();
                    case 'ajax_update_listing_field':
                        $class = $this->newTemplateEditor();
                        return $class->ajaxUpdateListingField();
                    case 'ajax_add_listing_field':
                        $class = $this->newTemplateEditor();
                        return $class->ajaxAddListingField();
                    case 'edit_listing_template_qed':
                        $class = $this->newTemplateEditor();
                        return $class->editListingTemplateQED();
                    case 'edit_listing_template_spo':
                        $class = $this->newTemplateEditor();
                        return $class->editListingTemplateSPO();
                    case 'edit_listing_template_sro':
                        $class = $this->newTemplateEditor();
                        return $class->editListingTemplateSRO();
                    case 'ajax_insert_listing_field':
                        $class = $this->newTemplateEditor();
                        return $class->ajaxInsertListingField();
                    case 'ajax_save_listing_search_order':
                        $class = $this->newTemplateEditor();
                        return $class->ajaxSaveListingSearchOrder('spo');
                    case 'ajax_save_search_result_order':
                        $class = $this->newTemplateEditor();
                        return $class->ajaxSaveListingSearchOrder('sro');
                    case 'edit_form_qed':
                        $class = $this->newLeadManager();
                        return $class->editLeadTemplateQED();
                    case 'edit_form_preview':
                        $class = $this->newLeadFunctions();
                        return $class->editFormPreview();
                    case 'ajax_add_form_field':
                        $class = $this->newLeadManager();
                        return $class->ajaxAddFormField();
                    case 'ajax_insert_form_field':
                        $class = $this->newLeadManager();
                        return $class->ajaxInsertFormField();
                    case 'ajax_get_form_field_info':
                        $class = $this->newLeadManager();
                        return $class->ajaxGetFormFieldInfo();
                    case 'ajax_save_form_field_order':
                        $class = $this->newLeadManager();
                        return $class->ajaxSaveFormFieldOrder();
                    case 'ajax_get_tag_info':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxGetTagInfo();
                    case 'ajax_update_tag_info':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxUpdateTagInfo();
                    case 'ajax_delete_blog_tag':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxDeleteBlogTag();
                    case 'ajax_create_blog_tag_noassignment':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxCreateBlogTagNoAssignment();
                    case 'ajax_display_upload_media':
                        $class = $this->newMediaHandler();
                        if (
                            isset($_GET['edit'], $_GET['media_type'])
                            && is_numeric($_GET['edit'])
                            && is_string($_GET['media_type'])
                        ) {
                            return $class->ajaxDisplayUploadMedia((int)$_GET['edit'], $_GET['media_type']);
                        }
                        break;
                    case 'ajax_get_media_info':
                        $class = $this->newMediaHandler();
                        if (
                            isset($_GET['media_id'], $_GET['media_type'])
                            && is_numeric($_GET['media_id'])
                            && is_string($_GET['media_type'])
                        ) {
                            return $class->ajaxGetMediaInfo((int)$_GET['media_id'], $_GET['media_type']);
                        }
                        break;
                    case 'ajax_upload_media':
                        $class = $this->newMediaHandler();
                        return $class->ajaxUploadMedia();
                    case 'ajax_upload_media_JSON':
                        $class = $this->newMediaHandler();
                        return $class->ajaxUploadMediaJson();
                    case 'ajax_display_listing_images':
                        $class = $this->newImageHandler();
                        if (isset($_GET['listing_id']) && is_numeric($_GET['listing_id'])) {
                            return $class->ajaxDisplayListingImages((int)$_GET['listing_id']);
                        }
                        break;
                    case 'ajax_save_media_order':
                        $class = $this->newMediaHandler();
                        return $class->ajaxSaveMediaOrder();
                    case 'ajax_delete_media':
                        $class = $this->newMediaHandler();
                        return $class->ajaxDeleteMedia();
                    case 'ajax_update_media':
                        $class = $this->newMediaHandler();
                        return $class->ajaxUpdateMedia();
                    case 'ajax_display_listing_vtours':
                        $class = $this->newVtourHandler();
                        if (isset($_GET['listing_id']) && is_numeric($_GET['listing_id'])) {
                            return $class->ajaxDisplayListingVtours((int)$_GET['listing_id']);
                        }
                        break;
                    case 'ajax_display_listing_files':
                        $class = $this->newFileHandler();
                        if (isset($_GET['listing_id']) && is_numeric($_GET['listing_id'])) {
                            return $class->ajaxDisplayListingFiles((int)$_GET['listing_id']);
                        }
                        break;
                    case 'ajax_delete_all':
                        //ajax_delete_all&media_object_id=*&media_type=listingsimages&media_parent_id={listing_id}

                        $class = $this->newMediaHandler();
                        if (
                            isset($_GET['media_object_id'], $_GET['media_type'], $_GET['media_parent_id'])
                            && is_numeric($_GET['media_object_id'])
                            && is_numeric($_GET['media_parent_id'])
                            && is_string($_GET['media_type'])
                        ) {
                            return $class->ajaxDeleteAll($_GET['media_type'], (int)$_GET['media_parent_id'], (int)$_GET['media_object_id']);
                        }
                        break;
                    case 'ajax_update_listing_data':
                        $listing_editor = $this->newListingEditor();
                        if (isset($_POST['edit']) && is_numeric($_POST['edit'])) {
                            return $listing_editor->ajaxUpdateListingData((int)$_POST['edit']);
                        }
                        break;
                    case 'ajax_display_add_listing':
                        $listing_editor = $this->newListingEditor();
                        return $listing_editor->ajaxDisplayAddListing();
                    case 'ajax_add_listing':
                        $listing_editor = $this->newListingEditor();
                        return $listing_editor->ajaxAddListing();
                    case 'ajax_display_user_images':
                        $class = $this->newImageHandler();
                        if (isset($_GET['user_id']) && is_numeric($_GET['user_id'])) {
                            return $class->ajaxDisplayUserImages((int)$_GET['user_id']);
                        }
                        break;
                    case 'ajax_display_user_files':
                        $class = $this->newFileHandler();
                        if (isset($_GET['user_id']) && is_numeric($_GET['user_id'])) {
                            return $class->ajaxDisplayUserFiles((int)$_GET['user_id']);
                        }
                        break;
                    case 'ajax_update_user_data':
                        $user_managment = $this->newUserManagement();
                        if (isset($_POST['user_id']) && is_numeric($_POST['user_id'])) {
                            return $user_managment->ajaxUpdateUserData((int)$_POST['user_id']);
                        }
                        break;
                    case 'ajax_delete_user':
                        $user_managment = $this->newUserManagement();
                        if (isset($_GET['user_id']) && is_numeric($_GET['user_id'])) {
                            return $user_managment->ajaxDeleteUser((int)$_GET['user_id']);
                        }
                        break;
                    case 'ajax_delete_listing':
                        $listing_editor = $this->newListingEditor();
                        if (isset($_GET['listing_id']) && is_numeric($_GET['listing_id'])) {
                            return $listing_editor->ajaxDeleteListing((int)$_GET['listing_id']);
                        }
                        break;
                    case 'ajax_display_add_user':
                        $user_managment = $this->newUserManagement();
                        return $user_managment->ajaxDisplayAddUser();
                    case 'ajax_add_user':
                        $user_managment = $this->newUserManagement();
                        return $user_managment->ajaxAddUser();
                    case 'blog_wpinject':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxWpInjectRun();
                    case 'ajax_save_feedback':
                        $lead_manager = $this->newLeadManager();
                        return $lead_manager->ajaxSaveFeedback();
                    case 'ajax_update_lead_field':
                        $lead_manager = $this->newLeadManager();
                        return $lead_manager->ajaxUpdateLeadField();
                    case 'ajax_save_feedback_status':
                        $lead_manager = $this->newLeadManager();
                        return $lead_manager->ajaxSaveFeedbackStatus();
                    case 'ajax_change_lead_agent':
                        $lead_manager = $this->newLeadManager();
                        return $lead_manager->ajaxChangeLeadAgent();
                    case 'ajax_change_lead_status':
                        $lead_manager = $this->newLeadManager();
                        return $lead_manager->ajaxChangeLeadStatus();
                    case 'ajax_change_lead_priority':
                        $lead_manager = $this->newLeadManager();
                        return $lead_manager->ajaxChangeLeadPriority();
                    case 'ajax_change_lead_notes':
                        $lead_manager = $this->newLeadManager();
                        return $lead_manager->ajaxChangeLeadNotes();
                    case 'ajax_delete_lead':
                        $lead_manager = $this->newLeadManager();
                        if (isset($_GET['lead_id']) && is_numeric($_GET['lead_id'])) {
                            return $lead_manager->ajaxDeleteLead((int)$_GET['lead_id']);
                        }
                        break;
                    case 'do_upgrade':
                        $login_status = $login->verifyPriv('Admin');
                        if ($login_status !== true) {
                            die('You Must Be The Site Admin To Do This');
                        }

                        $class = $this->newGeneralAdmin();
                        ;
                        $class->doUpgrade();
                        return '';
                    case 'twitter_disconnect':
                        $social = $this->newSocial();

                        $login_status = $login->verifyPriv('edit_site_config');
                        if ($login_status !== true) {
                            return json_encode(['error' => true, 'error_msg' => 'Permission Denied']) ?: '';
                        }

                        $ORconnection = new TwitterOAuth($this->config['twitter_consumer_key'], $this->config['twitter_consumer_secret']);

                        $exception_message = '';
                        $url = '';
                        try {
                            /* Get temporary credentials. */
                            $request_token = $ORconnection->oauth('oauth/request_token', ['oauth_callback' => $this->config['baseurl'] . '/admin/index.php?action=twitterback']);
                            /* Save temporary credentials to session. */
                            $_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
                            $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

                            /* If last connection failed don't display authorization link. */

                            switch ($ORconnection->getLastHttpCode()) {
                                case 200:
                                    /* Build authorize URL and redirect user to Twitter. */
                                    //$url = $ORconnection->getAuthorizeURL($token);
                                    $url = $ORconnection->url('oauth/authorize', ['oauth_token' => $token]);
                                    break;
                                case 415:
                                    // Callback not approved.
                                    break;
                            }
                        } catch (Exception $e) {
                            $exception_message = $e->getMessage() . "\n";
                        }

                        if ($url != '') {
                            $url = '<a href="' . $url . '" class="or_std_button">' . $lang['connect_to_twitter'] . '</a>';
                        } else {
                            $url = 'Could not connect to Twitter. Refresh the page or try again later. ' . $exception_message;
                        }
                        return json_encode(['error' => false, 'status' => $social->twitterDisconnect(), 'url' => $url]) ?: '';
                    case 'addlead_lookup_member':
                        if (isset($_GET['term']) && is_string($_GET['term'])) {
                            $user_managment = $this->newUserManagement();
                            $result = $user_managment->ajaxAddLeadLookupUser('no', $_GET['term']);
                            header('Content-type: application/json');
                            return json_encode($result) ?: '';
                        }

                        break;
                    case 'addlead_lookup_agent':
                        if (isset($_GET['term']) && is_string($_GET['term'])) {
                            $user_managment = $this->newUserManagement();
                            $result = $user_managment->ajaxAddLeadLookupUser('yes', $_GET['term']);
                            header('Content-type: application/json');
                            return json_encode($result) ?: '';
                        }

                        break;
                    case 'addlead_lookup_listing':
                        if (isset($_GET['term']) && is_string($_GET['term'])) {
                            $listing_editor = $this->newListingEditor();
                            $result = $listing_editor->ajaxAddLeadLookupListing($_GET['term']);
                            header('Content-type: application/json');
                            return json_encode($result) ?: '';
                        }
                        break;
                    case 'addlead_create_member':
                        if (
                            isset($_POST['email'], $_POST['fname'], $_POST['lname'])
                            && is_string($_POST['email'])
                            && is_string($_POST['fname'])
                            && is_string($_POST['lname'])
                        ) {
                            $user_managment = $this->newUserManagement();
                            return $user_managment->ajaxMemberCreation($_POST['email'], $_POST['fname'], $_POST['lname'], false);
                        }
                        break;
                    case 'addlead_create_lead':
                        if (isset($_POST['member_id']) && (isset($_POST['listing_id']) || isset($_POST['agent_id'])) && isset($_POST['notes'])) {
                            $lead_manager = $this->newLeadManager();
                            return json_encode($lead_manager->addLeadCreateLead()) ?: '';
                        }
                        break;
                    case 'ajax_make_inactive_listing':
                        if (isset($_GET['listing_id']) && is_numeric($_GET['listing_id'])) {
                            $listing_editor = $this->newListingEditor();
                            return $listing_editor->ajaxMakeInactiveListing((int)$_GET['listing_id']);
                        }
                        break;
                    case 'ajax_make_active_listing':
                        if (isset($_GET['listing_id']) && is_numeric($_GET['listing_id'])) {
                            $listing_editor = $this->newListingEditor();
                            return $listing_editor->ajaxMakeActiveListing((int)$_GET['listing_id']);
                        }
                        break;
                    case 'ajax_make_unfeatured_listing':
                        if (isset($_GET['listing_id']) && is_numeric($_GET['listing_id'])) {
                            $listing_editor = $this->newListingEditor();
                            return $listing_editor->ajaxMakeUnfeaturedListing((int)$_GET['listing_id']);
                        }
                        break;
                    case 'ajax_make_featured_listing':
                        if (isset($_GET['listing_id']) && is_numeric($_GET['listing_id'])) {
                            $listing_editor = $this->newListingEditor();
                            return $listing_editor->ajaxMakeFeaturedListing((int)$_GET['listing_id']);
                        }
                        break;
                    case 'ajax_viewlog_datatable':
                        $log = $this->newLog();
                        return $log->ajaxViewlogDatatable();
                    case 'ajax_get_menus':
                        $menu_editor = $this->newMenuEditor();
                        return $menu_editor->ajaxGetMenus();
                    case 'ajax_get_menu_items':
                        if (isset($_POST['menu_selection']) && is_numeric($_POST['menu_selection'])) {
                            $menu_editor = $this->newMenuEditor();
                            return $menu_editor->ajaxGetMenuItems((int)$_POST['menu_selection']);
                        }
                        break;
                    case 'ajax_get_menu_item_details':
                        if (isset($_GET['item_id']) && is_numeric($_GET['item_id'])) {
                            $menu_editor = $this->newMenuEditor();
                            return $menu_editor->ajaxGetMenuItemDetails((int)$_GET['item_id']);
                        }
                        break;
                    case 'ajax_set_menu_order':
                        if (
                            isset($_POST['menu_id'], $_POST['menu_items'])
                            && is_numeric($_POST['menu_id'])
                            && is_array($_POST['menu_items'])
                        ) {
                            $menu_editor = $this->newMenuEditor();
                            return $menu_editor->ajaxSetMenuOrder((int)$_POST['menu_id'], $_POST['menu_items']);
                        }
                        break;
                    case 'ajax_get_pages':
                        $class = $this->newPageFunctions();
                        return $class->ajaxGetPages();
                    case 'ajax_get_blogs':
                        $class = $this->newBlogFunctions();
                        ;
                        return $class->ajaxGetBlogs();
                    case 'ajax_save_menu_item':
                        if (
                            isset(
                                $_POST['item_id'],
                                $_POST['item_name'],
                                $_POST['item_type'],
                                $_POST['visible_guest'],
                                $_POST['visible_member'],
                                $_POST['visible_agent'],
                                $_POST['visible_admin'],
                            )
                            && is_numeric($_POST['item_id'])
                            && is_numeric($_POST['item_type'])
                            && is_numeric($_POST['visible_guest'])
                            && is_numeric($_POST['visible_member'])
                            && is_numeric($_POST['visible_agent'])
                            && is_numeric($_POST['visible_admin'])
                            && is_string($_POST['item_name'])
                        ) {
                            $class = $this->newMenuEditor();
                            return $class->ajaxSaveMenuItem(
                                (int)$_POST['item_id'],
                                $_POST['item_name'],
                                (int)$_POST['item_type'],
                                isset($_POST['item_value']) && is_string($_POST['item_value']) ? $_POST['item_value'] : '',
                                isset($_POST['item_target']) && is_string($_POST['item_target']) ? $_POST['item_target'] : '',
                                isset($_POST['item_class']) && is_string($_POST['item_class']) ? $_POST['item_class'] : '',
                                (bool)$_POST['visible_guest'],
                                (bool)$_POST['visible_member'],
                                (bool)$_POST['visible_agent'],
                                (bool)$_POST['visible_admin']
                            );
                        }
                        break;
                    case 'ajax_add_menu_item':
                        if (
                            isset($_POST['item_name'], $_POST['parent_id'], $_POST['menu_id'])
                            && is_string($_POST['item_name'])
                            && is_numeric($_POST['parent_id'])
                            && is_numeric($_POST['menu_id'])
                        ) {
                            $class = $this->newMenuEditor();
                            return $class->ajaxAddMenuItem((int)$_POST['menu_id'], $_POST['item_name'], (int)$_POST['parent_id']);
                        }
                        break;
                    case 'ajax_delete_menu_item':
                        if (isset($_POST['item_id']) && is_numeric($_POST['item_id'])) {
                            $class = $this->newMenuEditor();
                            return $class->ajaxDeleteMenuItem((int)$_POST['item_id']);
                        }
                        break;
                    case 'ajax_delete_menu':
                        if (isset($_POST['menu_id']) && is_numeric($_POST['menu_id'])) {
                            $class = $this->newMenuEditor();
                            return $class->ajaxDeleteMenu((int)$_POST['menu_id']);
                        }
                        break;
                    case 'ajax_create_menu':
                        if (isset($_POST['add_menu_name']) && is_string($_POST['add_menu_name'])) {
                            $class = $this->newMenuEditor();
                            return $class->ajaxCreateMenu($_POST['add_menu_name']);
                        }
                        break;
                    case 'ajax_leadmanager_datatable':
                        if (isset($_GET['show_all_leads'])) {
                            $lead_manager = $this->newLeadManager();
                            if ($_GET['show_all_leads'] == 'true') {
                                return $lead_manager->ajaxLeadManagerDatatable(true);
                            } else {
                                return $lead_manager->ajaxLeadManagerDatatable();
                            }
                        }
                        break;
                    case 'ajax_reset_password':
                        $login = $this->newLogin();
                        return $login->ajaxResetPassword();
                    case 'ajax_change_user_status':
                        if (
                            isset($_POST['user_id'])
                            && is_numeric($_POST['user_id'])
                            && isset($_POST['user_active'])
                            && is_string($_POST['user_active'])
                        ) {
                            $user_managment = $this->newUserManagement();
                            return $user_managment->changeUserStatus((int)$_POST['user_id'], $_POST['user_active']);
                        } else {
                            header('Content-type: application/json');
                            return json_encode(['error' => '3', 'error_msg' => $lang['access_denied']]) ?: '';
                        }
                        // no break
                    case 'generate_sitemap':
                        $sitemap = $this->newSitemap();
                        return $sitemap->generate();
                    case 'send_notifications':
                        $notify = $this->newNotification();
                        return $notify->notifyUsersOfAllNewListings();
                    case 'ajax_save_class_rank':
                        $class = $this->newPropertyClass();
                        return $class->ajaxSaveClassRank();
                    case 'ajax_modify_property_class':
                        $class = $this->newPropertyClass();
                        return $class->ajaxModifyPropertyClass();
                    case 'ajaxSaveUserRank(':
                        $sort = $this->newTemplateEditor();
                        return $sort->ajaxSaveUserRank();
                    case 'ajax_get_user_field_info':
                        $class = $this->newTemplateEditor();
                        if (isset($_GET['user_type']) && is_string($_GET['user_type'])) {
                            return $class->ajaxGetIUserFieldInfo($_GET['user_type']);
                        } elseif (isset($_POST['user_type']) && is_string($_POST['user_type'])) {
                            return $class->ajaxGetIUserFieldInfo($_POST['user_type']);
                        }
                        break;
                    case 'ajax_update_user_field':
                        $class = $this->newTemplateEditor();
                        return $class->ajaxUpdateUserField();
                    case 'ajax_add_user_field':
                        $class = $this->newTemplateEditor();
                        if (isset($_GET['user_type']) && is_string($_GET['user_type'])) {
                            return $class->ajaxAddUserField($_GET['user_type']);
                        } elseif (isset($_POST['user_type']) && is_string($_POST['user_type'])) {
                            return $class->ajaxAddUserField($_POST['user_type']);
                        }
                        break;
                    case 'ajax_insert_user_field':
                        $class = $this->newTemplateEditor();
                        return $class->ajaxInsertUserField();
                    case 'ajax_export_logs':
                        $class = $this->newLog();
                        return $class->ajaxExportLogs();
                    default:
                        // Handle Addons
                        $addon_name = [];
                        if (is_string($_GET['action'])) {
                            if (preg_match('/^addon_(.\S*?)_.*/', $_GET['action'], $addon_name)) {
                                $addonClassName = '\\OpenRealty\\Addons\\' . $addon_name[1] . '\\Addon';
                                if (class_exists($addonClassName)) {
                                    $addonClass = new $addonClassName($this->dbh, $this->config);
                                    return $addonClass->runActionAjax();
                                }
                            }
                        }
                }
            }
        }
        return '';
    }
}
