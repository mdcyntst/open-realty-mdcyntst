<?php

declare(strict_types=1);

namespace OpenRealty;

use Exception;
use OpenRealty\Api\Commands\MediaApi;

class MediaHandler extends BaseClass
{
    protected array $media_types = ['listingsimages', 'listingsfiles', 'listingsvtours', 'userimages', 'usersfiles'];

    public function handleExtLink(string $media_type, int $edit): string
    {
        // deals with incoming uploads
        $misc = $this->newMisc();
        $mediaobject = [];
        if (is_array($_POST['extlink'])) {
            foreach ($_POST['extlink'] as $ext_link) {
                if (is_string($ext_link)) {
                    if (!str_starts_with($ext_link, 'http://') && !str_starts_with($ext_link, 'https://') && !str_starts_with($ext_link, '//')) {
                        //Invalid URL
                        continue;
                    }
                    $realname = md5($ext_link) . '.jpg';
                    $mediaobject[$realname]['description'] = '';
                    $mediaobject[$realname]['data'] = $ext_link;
                    $mediaobject[$realname]['remote'] = true;
                }
            }
        }
        $media_api = new MediaApi($this->dbh, $this->config);
        try {
            $result = $media_api->create(['media_parent_id' => $edit, 'media_type' => $media_type, 'media_data' => $mediaobject]);
        } catch (Exception $e) {
            $misc->logErrorAndDie($e->getMessage());
        }
        $status = '';
        foreach ($result['media_error'] as $file => $error) {
            if ($error) {
                $status .= 'File ' . $file . ' - ' . $result['media_response'][$file] . "\r\n";
            }
        }
        if ($status == '') {
            $status .= 'File(s) Uploaded Successfully';
        }
        return $status;
    }

    public function handleUpload(string $media_type, int $edit): string
    {
        // deals with incoming uploads
        $misc = $this->newMisc();

        $mediaobject = [];

        if ($_FILES['userfile']['name'] == '') {
            return 'ERROR';
        }

        foreach ($_FILES['userfile']['name'] as $file_count => $realname) {
            /** @psalm-suppress InvalidArrayAccess  Ignored becuase psalm doesn't handle typing for $_FILES correctly for multiple file uploads. */
            if ($_FILES['userfile']['error'][$file_count] == UPLOAD_ERR_OK) {
                $imgDesc = '';
                $realname = $edit . '_' . $misc->cleanFilename($realname);
                $filename = $_FILES['userfile']['tmp_name'][$file_count];
                $contents = file_get_contents($filename);
                if (is_bool($contents)) {
                    $misc->logErrorAndDie('Error Getting File Contents: ' . $filename);
                }
                $mediaobject[$realname]['data'] = $contents;
                $mediaobject[$realname]['description'] = $imgDesc;
            }
        }
        $media_api = new MediaApi($this->dbh, $this->config);
        try {
            $result = $media_api->create(['media_parent_id' => $edit, 'media_type' => $media_type, 'media_data' => $mediaobject]);
        } catch (Exception $e) {
            $misc->logErrorAndDie($e->getMessage());
        }
        $status = '';
        foreach ($result['media_error'] as $file => $error) {
            if ($error) {
                $status .= 'File ' . $file . ' - ' . $result['media_response'][$file] . "\r\n";
            }
        }
        if ($status == '') {
            $status .= 'File(s) Uploaded Successfully';
        }
        return $status;
    }

    public function ajaxDisplayUploadMedia(int $edit_id, string $media_type): string
    {
        global $ORconn, $lang;
        $misc = $this->newMisc();
        $status_text = '';
        $support_external = false;
        $has_permission = $this->mediaPermissionCheck(0, $media_type, $edit_id);
        if ($has_permission) {
            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/media_upload.html');
            $avaliable_media = 0;
            $max_media_size = 0;
            $upload_lang = '';
            switch ($media_type) {
                case 'listingsimages':
                    $support_external = true;
                    $sql = 'SELECT count(listingsimages_id) as num_images 
							FROM ' . $this->config['table_prefix'] . "listingsimages 
							WHERE listingsdb_id = $edit_id";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $media_count = (int)$recordSet->fields('num_images');
                    $avaliable_media = $this->config['max_listings_uploads'] - $media_count;
                    $max_media_size = $this->config['max_listings_upload_size'];
                    $upload_lang = $lang['upload_a_picture'];
                    break;
                case 'userimages':
                    $sql = 'SELECT count(userimages_id) as num_images 
							FROM ' . $this->config['table_prefix'] . "userimages 
							WHERE userdb_id = $edit_id";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $media_count = (int)$recordSet->fields('num_images');
                    $avaliable_media = $this->config['max_user_uploads'] - $media_count;
                    $max_media_size = $this->config['max_user_upload_size'];
                    $upload_lang = $lang['upload_a_picture'];
                    break;
                case 'listingsvtours':
                    $sql = 'SELECT count(listingsvtours_id) as num_images 
							FROM ' . $this->config['table_prefix'] . "listingsvtours 
							WHERE listingsdb_id = $edit_id";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $media_count = (int)$recordSet->fields('num_images');
                    $avaliable_media = $this->config['max_vtour_uploads'] - $media_count;
                    $max_media_size = $this->config['max_vtour_upload_size'];
                    $upload_lang = $lang['upload_a_vtour'];
                    break;
                case 'listingsfiles':
                    $sql = 'SELECT count(listingsfiles_id) as num_files 
							FROM ' . $this->config['table_prefix'] . "listingsfiles 
							WHERE listingsdb_id = $edit_id";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $media_count = (int)$recordSet->fields('num_files');
                    $avaliable_media = $this->config['max_listings_file_uploads'] - $media_count;
                    $max_media_size = $this->config['max_listings_file_upload_size'];
                    $upload_lang = $lang['upload_a_file'];
                    break;
                case 'usersfiles':
                    $sql = 'SELECT count(usersfiles_id) as num_files FROM ' . $this->config['table_prefix'] . "usersfiles WHERE userdb_id = $edit_id";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $media_count = (int)$recordSet->fields('num_files');
                    $avaliable_media = $this->config['max_users_file_uploads'] - $media_count;
                    $max_media_size = $this->config['max_users_file_upload_size'];
                    $upload_lang = $lang['upload_a_file'];
                    break;
                default:
                    $misc->logErrorAndDie('Invalid Media Type');
            }
            $x = 0;
            $html = $page->getTemplateSection('media_upload_block');
            $new_html = '';
            while ($x < $avaliable_media) {
                //limit this to 10 at a time
                if ($x < 10) {
                    $new_html .= $html;
                }
                $x++;
            }
            $page->replaceTemplateSection('media_upload_block', $new_html);
            //External Image Form
            if ($support_external) {
                $x = 0;
                $html = $page->getTemplateSection('media_ext_link_block');
                $new_html = '';
                while ($x < $avaliable_media) {
                    //limit this to 10 at a time
                    if ($x < 10) {
                        $new_html .= $html;
                    }
                    $x++;
                }
                $page->replaceTemplateSection('media_ext_link_block', $new_html);
                $page->page = $page->cleanupTemplateBlock('show_ext_upload', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('show_ext_upload', $page->page);
            }
            $page->page = str_replace('{upload_lang_text}', $upload_lang, $page->page);
            $page->page = str_replace('{edit_id}', (string)$edit_id, $page->page);
            $page->page = str_replace('{media_type}', $media_type, $page->page);
            $page->page = str_replace('{max_upload_size}', (string)$max_media_size, $page->page);
            //Finish Loading Template
            $page->replaceTag('application_status_text', $status_text);
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        } else {
            return 'Permission Denied';
        }
    }

    public function mediaPermissionCheck(int $media_id = 0, string $media_type = '', int $parent_id = 0): false|int
    {
        global $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $has_permission = false;
        //Make sure Media Type is valid
        if (!in_array($media_type, $this->media_types)) {
            return false;
        }

        switch ($media_type) {
            case 'listingsimages':
                $listing_pages = $this->newListingPages();

                if ($media_id != 0 && $parent_id == 0) {
                    $sql = 'SELECT listingsdb_id FROM ' . $this->config['table_prefix'] . "$media_type WHERE ( " . $media_type . "_id = $media_id)";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $parent_id = (int)$recordSet->fields('listingsdb_id');
                }
                //Get Listing owner
                $listing_agent_id = $listing_pages->getListingAgentValue('userdb_id', $parent_id);
                //Make sure we can Edit this lisitng
                if ($_SESSION['userID'] != $listing_agent_id) {
                    $security = $login->verifyPriv('edit_all_listings');
                    if ($security === true) {
                        $has_permission = $parent_id;
                    }
                } else {
                    $has_permission = $parent_id;
                }
                break;

            case 'userimages':
                if ($media_id != 0 && $parent_id == 0) {
                    $sql = 'SELECT userdb_id 
							FROM ' . $this->config['table_prefix'] . "$media_type 
							WHERE ( " . $media_type . "_id = $media_id)";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $parent_id = (int)$recordSet->fields('userdb_id');
                }
                if ($_SESSION['userID'] != $parent_id) {
                    $security = $login->verifyPriv('edit_all_users');
                    if ($security === true) {
                        $has_permission = $parent_id;
                    }
                } else {
                    $has_permission = $parent_id;
                }

                break;
            case 'listingsvtours':
                $listing_pages = $this->newListingPages();
                if ($media_id != 0 && $parent_id == 0) {
                    $sql = 'SELECT listingsdb_id 
							FROM ' . $this->config['table_prefix'] . "$media_type 
							WHERE ( " . $media_type . "_id = $media_id)";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $parent_id = (int)$recordSet->fields('listingsdb_id');
                }
                //Get Listing owner
                $listing_agent_id = $listing_pages->getListingAgentValue('userdb_id', $parent_id);
                //Make sure we can Edit this lisitng
                if ($_SESSION['userID'] != $listing_agent_id) {
                    if ($_SESSION['edit_all_listings'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                        $has_permission = $parent_id;
                    }
                } elseif ($_SESSION['havevtours'] == 'yes') {
                    $has_permission = $parent_id;
                }
                break;
            case 'usersfiles':
                if ($media_id != 0 && $parent_id == 0) {
                    $sql = 'SELECT userdb_id 
							FROM ' . $this->config['table_prefix'] . "$media_type 
							WHERE ( " . $media_type . "_id = $media_id)";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $parent_id = (int)$recordSet->fields('userdb_id');
                }

                //Make sure we can Edit this lisitng
                if ($_SESSION['userID'] != $parent_id) {
                    if ($_SESSION['edit_all_users'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                        $has_permission = $parent_id;
                    }
                } elseif ($_SESSION['havefiles'] == 'yes') {
                    $has_permission = $parent_id;
                }
                break;
            case 'listingsfiles':
                $listing_pages = $this->newListingPages();
                if ($media_id != 0 && $parent_id == 0) {
                    $sql = 'SELECT listingsdb_id FROM ' . $this->config['table_prefix'] . "$media_type WHERE ( " . $media_type . "_id = $media_id)";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $parent_id = (int)$recordSet->fields('listingsdb_id');
                }
                //Get Listing owner
                $listing_agent_id = $listing_pages->getListingAgentValue('userdb_id', $parent_id);
                //Make sure we can Edit this lisitng
                if ($_SESSION['userID'] != $listing_agent_id) {
                    if ($_SESSION['edit_all_listings'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                        $has_permission = $parent_id;
                    }
                } elseif ($_SESSION['havefiles'] == 'yes') {
                    $has_permission = $parent_id;
                }
                break;
        }
        return $has_permission;
    }

    public function ajaxUpdateMedia(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        if (is_string($_POST['media_id']) && is_string($_POST['media_type'])) {
            $media_type = $_POST['media_type'];
            $media_id = intval($_POST['media_id']);
            $has_permission = $this->mediaPermissionCheck($media_id, $media_type);
            if (is_int($has_permission)) {
                if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
                }

                $parent_id = $has_permission;
                $foriegn_key = '';
                switch ($media_type) {
                    case 'listingsimages':
                    case 'listingsvtours':
                    case 'listingsfiles':
                        $foriegn_key = 'listingsdb_id';
                        break;
                    case 'userimages':
                    case 'usersfiles':
                        $foriegn_key = 'userdb_id';
                        break;
                    default:
                        $misc->logErrorAndDie('Invalid Media Type');
                }
                if (is_string($_POST['caption']) && is_string($_POST['description'])) {
                    $sql_caption = $misc->makeDbSafe($_POST['caption']);
                    $sql_description = $misc->makeDbSafe($_POST['description']);
                    $sql = 'UPDATE ' . $this->config['table_prefix'] . "$media_type 
							SET " . $media_type . "_caption = $sql_caption, " . $media_type . "_description = $sql_description 
							WHERE (($foriegn_key = $parent_id) 
							AND (" . $media_type . "_id = $media_id ))";

                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    header('Content-type: application/json');
                    return json_encode(['error' => '0', 'error_msg' => '']) ?: '';
                } else {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => 'Image Id/Caption/Description Not Set']) ?: '';
                }
            } else {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => 'Permission Denied']) ?: '';
            }
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => 'Media Type or Listing ID not Set']) ?: '';
        }
    }

    public function ajaxDeleteAll(string $media_type, int $media_parent_id, int $media_object_id): string
    {
        $media_api = new MediaApi($this->dbh, $this->config);
        try {
            $media_api->delete(['media_parent_id' => $media_parent_id, 'media_type' => $media_type, 'media_object_id' => $media_object_id]);
        } catch (Exception $e) {
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
        }

        header('Content-type: application/json');
        return json_encode(['error' => '0', 'error_msg' => '']) ?: '';
    }

    public function ajaxDeleteMedia(): string
    {
        if (isset($_POST['media_type']) && isset($_POST['media_id']) && is_string($_POST['media_type'])) {
            $media_id = intval($_POST['media_id']);
            $media_type = $_POST['media_type'];
            $has_permission = $this->mediaPermissionCheck($media_id, $media_type);
            if (is_int($has_permission)) {
                $media_api = new MediaApi($this->dbh, $this->config);
                try {
                    $media_api->delete(['media_parent_id' => $has_permission, 'media_type' => $media_type, 'media_object_id' => $media_id]);
                } catch (Exception $e) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
                }

                header('Content-type: application/json');
                return json_encode(['error' => '0', 'error_msg' => '']) ?: '';
            } else {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => 'Permission Denied']) ?: '';
            }
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => 'Media Type or Listing ID not Set']) ?: '';
        }
    }

    public function ajaxSaveMediaOrder(): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        if (is_string($_POST['media_type']) && is_array($_POST['mediaOrder'])) {
            $media_order = $_POST['mediaOrder'];
            $media_type = $_POST['media_type'];
            $parent_id = intval($_POST['parent_id']);
            //  Make sure Media Type is valid
            $has_permission = $this->mediaPermissionCheck(0, $media_type, $parent_id);
            if ($has_permission) {
                foreach ($media_order as $image_rank => $image_id) {
                    $image_rank = intval($image_rank);
                    $image_id = intval($image_id);
                    $foriegn_key = '';
                    switch ($media_type) {
                        case 'listingsimages':
                        case 'listingsvtours':
                        case 'listingsfiles':
                            $foriegn_key = 'listingsdb_id';
                            break;
                        case 'userimages':
                        case 'usersfiles':
                            $foriegn_key = 'userdb_id';
                            break;
                        default:
                            $misc->logErrorAndDie('Invalid Media Type');
                    }
                    $sql = 'UPDATE ' . $this->config['table_prefix'] . $media_type . ' 
							SET ' . $media_type . '_rank = ' . $image_rank . ' 
							WHERE ((' . $foriegn_key . ' = ' . $parent_id . ') 
							AND (' . $media_type . '_id = ' . $image_id . '))';
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'error_msg' => ""]) ?: '';
            } else {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => 'Permission Denied']) ?: '';
            }
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => 'Media Type or Listing ID not Set']) ?: '';
        }
        //$.post("ajax_save_listing_media_order", { media_type: "image",listing_id,"{listing_id}", order },
    }

    public function ajaxUploadMediaJson(): string
    {
        $status = '';
        if (is_string($_POST['edit']) && is_string($_POST['media_type'])) {
            $media_type = $_POST['media_type'];
            if (!in_array($media_type, $this->media_types)) {
                $status = 'Invalid Media Type';
            }
            $edit_id = intval($_POST['edit']);
            $has_permission = $this->mediaPermissionCheck(0, $media_type, $edit_id);
            if ($has_permission) {
                switch ($media_type) {
                    case 'listingsimages':
                        $status = $this->handleUpload('listingsimages', $edit_id);
                        break;
                    case 'listingsvtours':
                        $status = $this->handleUpload('listingsvtours', $edit_id);
                        break;
                    case 'listingsfiles':
                        $status = $this->handleUpload('listingsfiles', $edit_id);
                        break;
                    case 'userimages':
                        $status = $this->handleUpload('userimages', $edit_id);
                        break;
                    case 'usersfiles':
                        $status = $this->handleUpload('usersfiles', $edit_id);
                        break;
                }
            } else {
                $status = 'Permission Denined';
            }
        }

        return json_encode(['status' => $status]) ?: '';
    }

    public function ajaxUploadMedia(): string
    {
        global $lang;

        $misc = $this->newMisc();
        $display = '';
        if (isset($_POST['edit']) && is_string($_POST['media_type'])) {
            $media_type = $_POST['media_type'];
            if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
                return $lang['invalid_csrf_token'];
            }
            if (!in_array($media_type, $this->media_types)) {
                return 'Invalid Media Type';
            }
            $edit_id = intval($_POST['edit']);
            $has_permission = $this->mediaPermissionCheck(0, $media_type, $edit_id);
            if ($has_permission) {
                switch ($media_type) {
                    case 'listingsimages':
                        if (isset($_POST['extlink'])) {
                            $display = $this->handleExtLink('listingsimages', $edit_id);
                        } else {
                            $display = $this->handleUpload('listingsimages', $edit_id);
                        }
                        break;
                    case 'listingsvtours':
                        if (isset($_POST['extlink'])) {
                            //Skip as vtours do not support external links at this time
                            return 'External Images Not Supported';
                        } else {
                            $display = $this->handleUpload('listingsvtours', $edit_id);
                        }
                        break;
                    case 'listingsfiles':
                        if (isset($_POST['extlink'])) {
                            //Skip as vtours do not support external links at this time
                            return 'External Media Not Supported';
                        } else {
                            $display = $this->handleUpload('listingsfiles', $edit_id);
                        }
                        break;
                    case 'userimages':
                        if (isset($_POST['extlink'])) {
                            $display = $this->handleExtLink('userimages', $edit_id);
                        } else {
                            $display = $this->handleUpload('userimages', $edit_id);
                        }
                        break;
                    case 'usersfiles':
                        if (isset($_POST['extlink'])) {
                            //Skip as vtours do not support external links at this time
                            return 'External Media Not Supported';
                        } else {
                            $display = $this->handleUpload('usersfiles', $edit_id);
                        }
                        break;
                    default:
                        $misc->logErrorAndDie('Invalid Media Type');
                }

                return $display;
            }
        }
        return 'Permission Denined';
    }

    public function getMediaCaption(string $media_type, int $media_id): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $caption = '';
        //Check for allowed mediatypes
        if (in_array($media_type, $this->media_types)) {
            $sql = 'SELECT ' . $media_type . '_caption 
					FROM ' . $this->config['table_prefix'] . $media_type . ' 
					WHERE (' . $media_type . '_id = ' . $media_id . ')';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $caption = $recordSet->fields($media_type . '_caption');
        }
        return $caption;
    }

    /**
     * @return string
     */
    public function ajaxGetMediaInfo(int $media_id, string $media_type): string
    {
        global $ORconn;

        $misc = $this->newMisc();

        //Get Listing ID for Image
        $has_permission = $this->mediaPermissionCheck($media_id, $media_type);
        $view_path = '';
        $upload_path = '';
        $full_displaywidth = '';
        switch ($media_type) {
            case 'listingsvtours':
                $view_path = $this->config['vtour_view_images_path'];
                $upload_path = $this->config['vtour_upload_path'];
                $full_displaywidth = 600;
                break;
            case 'listingsimages':
                $view_path = $this->config['listings_view_images_path'];
                $upload_path = $this->config['listings_upload_path'];
                break;
            case 'listingsfiles':
                $view_path = $this->config['listings_view_file_path'];
                $upload_path = $this->config['listings_file_upload_path'];
                break;
            case 'userimages':
                $view_path = $this->config['user_view_images_path'];
                $upload_path = $this->config['user_upload_path'];
                break;
            case 'usersfiles':
                $view_path = $this->config['users_view_file_path'];
                $upload_path = $this->config['users_file_upload_path'];
                break;
            default:
                $misc->logErrorAndDie('Invalid Media Type');
        }
        $media_filesize = '';
        if ($has_permission !== false) {
            $caption = '';
            $description = '';
            $file_name = '';
            $parent_id = 0;
            $thumb_displaywidth = '';
            $thumb_displayheight = '';
            $image_full_src = '';
            $image_thumb_src = '';
            $full_imagewidth = '';
            $full_imageheight = '';
            switch ($media_type) {
                case 'listingsvtours':
                case 'listingsimages':
                case 'userimages':
                    /*
                                         $result = $api->loadLocalApi('Media__read',array(
                                                'media_type'=>$media_type,
                                                'media_parent_id'=>$media_id,
                                                'media_output'=>'URL'
                                        ));

                                        $caption =  $result['media_object'][0]['caption'];
                                        $description = $result['media_object'][0]['description'];
                                        $thumb_file_name = $result['media_object'][0]['thumb_file_name'];
                                        $file_name = $result['media_object'][0]['file_name'];
                    */
                    $sql = 'SELECT ' . $media_type . '_caption, ' . $media_type . '_file_name, ' . $media_type . '_thumb_file_name, ' . $media_type . '_description ,' . $media_type . '_id
							FROM ' . $this->config['table_prefix'] . "$media_type 
							WHERE ( " . $media_type . "_id = $media_id)";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $caption = (string)$recordSet->fields($media_type . '_caption');
                    $description = (string)$recordSet->fields($media_type . '_description');
                    $thumb_file_name = (string)$recordSet->fields($media_type . '_thumb_file_name');
                    $file_name = (string)$recordSet->fields($media_type . '_file_name');
                    $parent_id = (int)$recordSet->fields($media_type . '_id');

                    if (str_starts_with($thumb_file_name, 'http://') || str_starts_with($thumb_file_name, 'https://') || str_starts_with($thumb_file_name, '//')) {
                        $full_displaywidth = '';
                        $image_full_src = $file_name;
                        $image_thumb_src = $thumb_file_name;
                        $thumb_displaywidth = $this->config['thumbnail_width'];
                        $thumb_displayheight = $this->config['thumbnail_height'];
                        $full_imagedata = GetImageSize($file_name);
                        $full_imagewidth = $full_imagedata[0];
                        $full_imageheight = $full_imagedata[1];
                        $head_array = get_headers($file_name, true);
                        if (is_array($head_array)) {
                            $head = array_change_key_case($head_array);
                            $media_filesize = round(intval($head['content-length']) * .0009765625);
                        }
                    } else {
                        $image_full_src = $view_path . '/' . $file_name;
                        $image_thumb_src = $view_path . '/' . $thumb_file_name;

                        if (file_exists("$upload_path/$file_name")) {
                            $full_imagedata = GetImageSize("$upload_path/$file_name");
                            if ($full_imagedata !== false) {
                                $full_imagewidth = $full_imagedata[0];
                                $full_imageheight = $full_imagedata[1];
                            }
                            $media_filesize = round(filesize("$upload_path/$file_name") * .0009765625);
                        }

                        if (file_exists("$upload_path/$thumb_file_name")) {
                            $thumb_imagedata = GetImageSize("$upload_path/$thumb_file_name");
                            if ($thumb_imagedata !== false) {
                                $thumb_imagewidth = $thumb_imagedata[0];
                                $thumb_imageheight = $thumb_imagedata[1];
                                $thumb_max_width = $this->config['thumbnail_width'];
                                $thumb_max_height = $this->config['thumbnail_height'];
                                $resize_by = $this->config['resize_thumb_by'];
                                if (($thumb_max_width == $thumb_imagewidth) || ($thumb_max_height == $thumb_imageheight)) {
                                    $thumb_displaywidth = $thumb_imagewidth;
                                    $thumb_displayheight = $thumb_imageheight;
                                } else {
                                    if ($resize_by == 'width') {
                                        $shrinkage = $thumb_imagewidth / $thumb_max_width;
                                        $thumb_displaywidth = $thumb_max_width;
                                        $thumb_displayheight = round($thumb_imageheight / $shrinkage);
                                    } elseif ($resize_by == 'height') {
                                        $shrinkage = $thumb_imageheight / $thumb_max_height;
                                        $thumb_displayheight = $thumb_max_height;
                                        $thumb_displaywidth = round($thumb_imagewidth / $shrinkage);
                                    } elseif ($resize_by == 'both') {
                                        $thumb_displayheight = $thumb_max_height;
                                        $thumb_displaywidth = $thumb_max_width;
                                    } elseif ($resize_by == 'bestfit') {
                                        $shrinkage_width = $thumb_imagewidth / $thumb_max_width;
                                        $shrinkage_height = $thumb_imageheight / $thumb_max_height;
                                        $shrinkage = max($shrinkage_width, $shrinkage_height);
                                        $thumb_displayheight = round($thumb_imageheight / $shrinkage);
                                        $thumb_displaywidth = round($thumb_imagewidth / $shrinkage);
                                    }
                                }
                            }
                        }
                    }
                    break;
                case 'listingsfiles':
                case 'usersfiles':
                    $sql = 'SELECT ' . $media_type . '_caption, ' . $media_type . '_file_name, ' . $media_type . '_description , ' . $media_type . '_id
							FROM ' . $this->config['table_prefix'] . "$media_type 
							WHERE ( " . $media_type . "_id = $media_id)";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $caption = $recordSet->fields($media_type . '_caption');
                    $description = $recordSet->fields($media_type . '_description');
                    $file_name = $recordSet->fields($media_type . '_file_name');
                    $parent_id = $recordSet->fields($media_type . '_id');
                    break;
                default:
                    $misc->logErrorAndDie('Invalid Media Type');
            }

            header('Content-type: application/json');
            return json_encode([
                'error' => '0',
                'media_id' => "$media_id",
                'parent_id' => "$parent_id",
                'thumb_width' => "$thumb_displaywidth",
                'thumb_height' => "$thumb_displayheight",
                'full_width' => "$full_displaywidth",
                'media_caption' => "$caption",
                'media_description' => "$description",
                'media_full_src' => "$image_full_src",
                'media_thumb_src' => "$image_thumb_src",
                'file_name' => $file_name,
                'media_file_size' => $media_filesize . 'KB',
                'media_file_width' => $full_imagewidth,
                'media_file_height' => $full_imageheight,
            ]);
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => 'Permission Denied']) ?: '';
        }
    }
}
