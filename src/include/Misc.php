<?php

declare(strict_types=1);

namespace OpenRealty;

use JetBrains\PhpStorm\NoReturn;
use OpenRealty\Api\Commands\LogApi;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use OpenRealty\Api\Commands\UserApi;

/**
 * This is our misc class that contains common functions used by other classes
 */
class Misc extends BaseClass
{
    /**
     * Generate a csfr token
     *
     * @return string
     */
    public function generateCsrfToken(): string
    {
        if (isset($_SESSION['csrf_token'])) {
            return (string)$_SESSION['csrf_token'];
        }
        try {
            $token = bin2hex(random_bytes(32));
        } catch (\Exception $e) {
            $this->logErrorAndDie($e->getMessage());
        }
        $_SESSION['csrf_token'] = $token;
        return $token;
    }

    /**
     * Validate CSRF token, removes token from session for one time validations.
     *
     * @param string $token Validate a csrf token.
     *
     * @return boolean
     */
    public function validateCsrfToken(string $token): bool
    {
        if (isset($_SESSION['csrf_token']) && hash_equals((string)$_SESSION['csrf_token'], $token)) {
            unset($_SESSION['csrf_token']);
            return true;
        }
        return false;
    }

    /**
     * Validate CSRF token for ajax calls. Doesn't clear token from session allowing multiple ajax calls to use same
     * token.
     *
     * @param string $token CSRF Token to validate.
     *
     * @return boolean
     */
    public function validateCsrfTokenAjax(string $token): bool
    {
        if (isset($_SESSION['csrf_token']) && hash_equals((string)$_SESSION['csrf_token'], $token)) {
            return true;
        }
        return false;
    }

    /**
     * Tries to detect if a the user is connecting from a mobile device.
     *
     * @return boolean
     */
    public function detectMobileBrowser(): bool
    {
        $hooks = $this->newHooks();
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            /** @var null|array{is_mobile: boolean} */
            $hook_result = $hooks->load('detectMobileBrowser', $_SERVER['HTTP_USER_AGENT']);
            if (is_array($hook_result)) {
                if (isset($hook_result['is_mobile'])) {
                    return $hook_result['is_mobile'];
                }
            }
        }
        //echo $_SERVER['HTTP_USER_AGENT'];
        $regex_match = '/(nokia|iphone|android(?!(.*xoom\sbuild))|motorola|^mot\-|softbank|foma|docomo|kddi|up\.browser|up\.link|';
        $regex_match .= 'htc|dopod|blazer|netfront|helio|hosin|huawei|novarra|CoolPad|webos|techfaith|palmsource|';
        $regex_match .= 'blackberry|alcatel|amoi|ktouch|nexian|samsung|^sam\-|s[cg]h|^lge|ericsson|philips|sagem|wellcom|bunjalloo|maui|';
        $regex_match .= 'symbian|smartphone|midp|wap|phone|windows ce|iemobile|^spice|^bird|^zte\-|longcos|pantech|gionee|^sie\-|portalmmm|';
        $regex_match .= 'jig\s browser|hiptop|^ucweb|^benq|haier|^lct|opera\s*mobi|opera\*mini|320x320|240x320|176x220|skyfire';
        $regex_match .= ')/i';
        if (isset($_SERVER['HTTP_X_WAP_PROFILE'])) {
            return true;
        } elseif (isset($_SERVER['HTTP_PROFILE'])) {
            return true;
        } elseif (isset($_SERVER['HTTP_USER_AGENT'])) {
            return (bool)preg_match($regex_match, strtolower($_SERVER['HTTP_USER_AGENT']));
        } else {
            return false;
        }
    }

    /**
     * Money Formating
     *
     * Formats a number with the international money sign (dollar sign) in the correct place.
     *
     * @param string $number Takes a formated number eg. 1,000.00.
     *
     * @return string Returns the number string with the money sign in the correct place. Eg. $1,000.00
     */
    public function moneyFormats(string $number): string
    {
        return match ($this->config['money_format']) {
            2 => $number . $this->config['money_sign'],
            3 => $this->config['money_sign'] . ' ' . $number,
            default => $this->config['money_sign'] . $number,
        };
    }

    /**
     * Internationalizes numbers
     *
     * Internationalizes numbers on the site according to the fomat defined in the
     * $this->config['number_format_style'])
     *
     * @param float   $input    A float number to format.
     * @param integer $decimals Number of decimal points.
     *
     * @return string Formatted number to resturn.
     */
    public function internationalNumFormat(float $input, int $decimals = 2): string
    {
        //

        switch ($this->config['number_format_style']) {
            case '2': // spain, germany
                if ($this->config['force_decimals']) {
                    $output = number_format($input, $decimals, ',', '.');
                } else {
                    $output = $this->formatNumber($input, $decimals, ',', '.');
                }
                break;
            case '3': // estonia
                if ($this->config['force_decimals']) {
                    $output = number_format($input, $decimals, '.', ' ');
                } else {
                    $output = $this->formatNumber($input, $decimals, '.', ' ');
                }
                break;
            case '4': // france, norway
                if ($this->config['force_decimals']) {
                    $output = number_format($input, $decimals, ',', ' ');
                } else {
                    $output = $this->formatNumber($input, $decimals, ',', ' ');
                }
                break;
            case '5': // switzerland
                if ($this->config['force_decimals']) {
                    $output = number_format($input, $decimals, ',', "'");
                } else {
                    $output = $this->formatNumber($input, $decimals, ',', "'");
                }
                break;
            case '6': // kazahistan
                if ($this->config['force_decimals']) {
                    $output = number_format($input, $decimals, ',', '.');
                } else {
                    $output = $this->formatNumber($input, $decimals, ',', '.');
                }
                break;
            default:
                if ($this->config['force_decimals']) {
                    $output = number_format($input, $decimals);
                } else {
                    $output = $this->formatNumber($input, $decimals, '.', ',');
                }
                break;
        } // end switch
        return $output;
    }

    /**
     * Formats a number with cetain decimal points and thousands seperator.
     *
     * @param float|integer|string $number        Raw number to format.
     * @param integer              $decimals      Number of decimal points.
     * @param string               $dec_point     Character to use as decimal point.
     * @param string               $thousands_sep Character to use as thousands seperator.
     *
     * @return string
     */
    public function formatNumber(float|int|string $number, int $decimals, string $dec_point, string $thousands_sep): string
    {
        //Make sure $number is a double or a numeric value, and user did not provide a string
        if (is_string($number)) {
            $number = floatval($number);
        }
        $nocomma = abs($number - floor($number));
        $strnocomma = number_format($nocomma, $decimals, '.', '');
        for ($i = 1; $i <= $decimals; $i++) {
            if (substr($strnocomma, ($i * -1), 1) != '0') {
                break;
            }
        }
        return number_format($number, ($decimals - $i + 1), $dec_point, $thousands_sep);
    }

    /**
     * Recursive directory deletion.
     *
     * https://stackoverflow.com/questions/3338123/how-do-i-recursively-delete-a-directory-and-its-entire-contents-files-sub-dir
     * @param string $dir Path to dir to delete.
     *
     * @return boolean
     */
    public function recurseRmdir(string $dir): bool
    {
        $dir_contents = scandir($dir);
        if (is_array($dir_contents)) {
            $files = array_diff($dir_contents, ['.', '..']);
            /* @var string */
            foreach ($files as $file) {
                (is_dir("$dir/$file") && !is_link("$dir/$file")) ? $this->recurseRmdir("$dir/$file") : unlink("$dir/$file");
            }
            return rmdir($dir);
        }
        return false;
    }

    /**
     * Get microtime
     *
     * @return float
     */
    public function getmicrotime(): float
    {
        [$usec, $sec] = explode(' ', microtime());
        return ((float)$usec + (float)$sec);
    }

    /**
     * Makes input safe for insertion into a $sql string. Adds any needed quotes, strips html if configured in site
     * config, with an option to skip.
     *
     * @param string|integer|float|boolean $input         Input to quote.
     * @param boolean                      $skipHtmlStrip Should we skip striping HTML.
     *
     * @return string
     */
    public function makeDbSafe(string|int|float|bool $input, bool $skipHtmlStrip = false): string
    {
        global $ORconn;
        $input = strval($input);
        if ($this->config['strip_html'] && !$skipHtmlStrip) {
            $input = strip_tags($input, $this->config['allowed_html_tags']);
        }
        return $ORconn->qstr($input);
    }


    /**
     * Makes input safe for insertion into a $sql string. Adds any needed quotes and strips html.
     *
     * @param string|integer|float|boolean $input Input to quote.
     *
     * @return string
     */
    public function makeDbExtraSafe(string|int|float|bool $input): string
    {
        global $ORconn;
        $input = strval($input);
        $output = strip_tags($input); // strips out all tags
        $output = $ORconn->qstr($output);
        return trim($output);
    }

    /**
     * Logs a SQL Error and Then Dies with 500 Error
     *
     * @param string $sql SQL Command that failed.
     *
     * @psalm-return never
     */
    #[NoReturn] public function logErrorAndDie(string $sql): void
    {

        $message = $this->logError($sql);
        header('Location: 500.shtml');
        die(nl2br($message));
    }

    /**
     * Logs a SQL Error and returns the message sent via email
     *
     * @param string $sql SQL Command that failed.
     *
     * @return string
     */
    public function logError(string $sql): string
    {
        // logs SQL errrors for later inspection
        global $ORconn;
        $remote_ip = '';
        if (array_key_exists('REMOTE_ADDR', $_SERVER)) {
            $remote_ip = $_SERVER['REMOTE_ADDR'];
        }

        $message = 'Fatal Error triggered by User at IP --> ' . $remote_ip . ' ON ' . date('F j, Y, g:i:s a') . "\r\n\r\n";
        $message .= 'SQL Error Message: ' . $ORconn->ErrorMsg() . "\r\n";
        $message .= 'SQL statement that failed below: ' . "\r\n";
        $message .= '---------------------------------------------------------' . "\r\n";
        $message .= $sql . "\r\n";
        $message .= "\r\n" . '---------------------------------------------------------' . "\r\n";
        $message .= "\r\n" . 'ERROR REPORT ' . $this->config['baseurl'] . ': ' . date('F j, Y, g:i:s a') . "\r\n";
        $message .= "\r\n" . '---------------------------------------------------------' . "\r\n";
        if (isset($_SERVER['SERVER_SOFTWARE'])) {
            $message .= 'Server Type: ' . $_SERVER['SERVER_SOFTWARE'] . "\r\n";
        }
        if (isset($_SERVER['REQUEST_METHOD'])) {
            $message .= 'Request Method: ' . $_SERVER['REQUEST_METHOD'] . "\r\n";
        }
        if (isset($_SERVER['QUERY_STRING'])) {
            $message .= 'Query String: ' . $_SERVER['QUERY_STRING'] . "\r\n";
        }
        if (isset($_SERVER['HTTP_REFERER'])) {
            $message .= 'Refereer: ' . $_SERVER['HTTP_REFERER'] . "\r\n";
        }
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $message .= 'User Agent: ' . $_SERVER['HTTP_USER_AGENT'] . "\r\n";
        }
        if (isset($_SERVER['REQUEST_URI'])) {
            $message .= 'Request URI: ' . $_SERVER['REQUEST_URI'] . "\r\n";
        }

        $message .= 'POST Variables: ' . var_export($_POST, true) . "\r\n";
        $message .= 'GET Variables: ' . var_export($_GET, true) . "\r\n";

        if (isset($this->config['site_email']) && $this->config['site_email'] != '') {
            $sender_email = $this->config['site_email'];
        } else {
            $sender_email = $this->config['admin_email'];
        }
        $this->sendEmail($this->config['admin_name'], $sender_email, $this->config['admin_email'], $message, 'SQL Error http://' . $this->config['baseurl']);
        return $message;
    }

    /**
     * Summary of nextPrev
     *
     * @param integer $num_rows Number of rows per page.
     * @param integer $cur_page Current page Number.
     * @param string  $template Template file to load. will load nex_prev_$TEMPLATE.html.
     * @param boolean $admin    Is this being used in the admin area.
     *
     * @return string
     */
    public function nextPrev(int $num_rows, int $cur_page, string $template = '', bool $admin = false): string
    {
        global $lang;


        $page = $this->newPageUser();
        if ($template != '') {
            $template_file = 'next_prev_' . $template . '.html';
        } else {
            $template_file = 'next_prev.html';
        }
        if ($admin) {
            $page->loadPage($this->config['admin_template_path'] . '/' . $template_file);
        } else {
            $page->loadPage($this->config['template_path'] . '/' . $template_file);
        }
        $guidestring = '';
        $guidestring_no_action = '';
        // Save GET
        foreach ($_GET as $k => $v) {
            $k_val = strval($k);
            if ($v !== '' && $k_val != 'cur_page' && $k_val != 'PHPSESSID' && ($k_val != 'printer_friendly' && $v)) {
                if (is_array($v)) {
                    foreach ($v as $vitem) {
                        if (is_scalar($vitem)) {
                            $guidestring .= '&amp;' . urlencode($k_val) . '[]=' . urlencode($vitem);
                        }
                    }
                } else {
                    $guidestring .= '&amp;' . urlencode($k_val) . '=' . urlencode($v);
                }
            }
            if ($v !== '' && $k_val != 'cur_page' && $k_val != 'PHPSESSID' && $k_val != 'action' && ($k_val != 'printer_friendly' && $v)) {
                if (is_array($v)) {
                    foreach ($v as $vitem) {
                        if (is_scalar($vitem)) {
                            $guidestring_no_action .= '&amp;' . urlencode($k_val) . '[]=' . urlencode($vitem);
                        }
                    }
                } else {
                    $guidestring_no_action .= '&amp;' . urlencode($k_val) . '=' . urlencode($v);
                }
            }
        }
        $page->page = str_replace('{nextprev_guidestring}', $guidestring, $page->page);
        $page->page = str_replace('{nextprev_guidestring_no_action}', $guidestring_no_action, $page->page);
        if ($cur_page == '') {
            $cur_page = 0;
        }
        $page_num = $cur_page + 1;

        $page->page = str_replace('{nextprev_num_rows}', (string)$num_rows, $page->page);
        $items_per_page = 25;
        if ($_GET['action'] == 'view_log') {
            $page->page = str_replace('{nextprev_page_type}', $lang['log'], $page->page);
            $page->page = str_replace('{nextprev_meet_your_search}', $lang['logs_meet_your_search'], $page->page);

            if ($num_rows == 1) {
                $page->page = $page->removeTemplateBlock('!nextprev_num_of_rows_is_1', $page->page);
                $page->page = $page->cleanupTemplateBlock('nextprev_num_of_rows_is_1', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('nextprev_num_of_rows_is_1', $page->page);
                $page->page = $page->cleanupTemplateBlock('!nextprev_num_of_rows_is_1', $page->page);
            }
        } elseif ($_GET['action'] == 'edit_blog' || $_GET['action'] == 'blog_index') {
            $items_per_page = $this->config['blogs_per_page'];
            $page->page = str_replace('{nextprev_page_type}', $lang['blog'], $page->page);
            $page->page = str_replace('{nextprev_meet_your_search}', $lang['blogs'], $page->page);
            if ($num_rows == 1) {
                $page->page = $page->removeTemplateBlock('!nextprev_num_of_rows_is_1', $page->page);
                $page->page = $page->cleanupTemplateBlock('nextprev_num_of_rows_is_1', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('nextprev_num_of_rows_is_1', $page->page);
                $page->page = $page->cleanupTemplateBlock('!nextprev_num_of_rows_is_1', $page->page);
            }
        } elseif ($_GET['action'] == 'view_users') {
            $items_per_page = $this->config['users_per_page'];
            $page->page = str_replace('{nextprev_page_type}', $lang['agent'], $page->page);
            $page->page = str_replace('{nextprev_meet_your_search}', $lang['agents'], $page->page);
            if ($num_rows == 1) {
                $page->page = $page->removeTemplateBlock('!nextprev_num_of_rows_is_1', $page->page);
                $page->page = $page->cleanupTemplateBlock('nextprev_num_of_rows_is_1', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('nextprev_num_of_rows_is_1', $page->page);
                $page->page = $page->cleanupTemplateBlock('!nextprev_num_of_rows_is_1', $page->page);
            }
        } else {
            if ($_GET['action'] == 'edit_listings' || $_GET['action'] == 'edit_my_listings') {
                $items_per_page = $this->config['admin_listing_per_page'];
            } else {
                $items_per_page = $this->config['listings_per_page'];
            }
            if ($_GET['action'] == 'user_manager') {
                $page->page = str_replace('{nextprev_page_type}', $lang['user_manager_users'], $page->page);
                $page->page = str_replace('{nextprev_meet_your_search}', $lang['user_manager_users'], $page->page);
            } else {
                $page->page = str_replace('{nextprev_page_type}', $lang['listing'], $page->page);
                $page->page = str_replace('{nextprev_meet_your_search}', $lang['listings_meet_your_search'], $page->page);
            }

            if ($num_rows == 1) {
                $page->page = $page->removeTemplateBlock('!nextprev_num_of_rows_is_1', $page->page);
                $page->page = $page->cleanupTemplateBlock('nextprev_num_of_rows_is_1', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('nextprev_num_of_rows_is_1', $page->page);
                $page->page = $page->cleanupTemplateBlock('!nextprev_num_of_rows_is_1', $page->page);
            }
        }
        $total_num_page = ceil($num_rows / $items_per_page);
        if ($total_num_page == 0) {
            $listing_num_min = 0;
            $listing_num_max = 0;
        } else {
            $listing_num_min = (($cur_page * $items_per_page) + 1);
            if ($page_num == $total_num_page) {
                $listing_num_max = $num_rows;
            } else {
                $listing_num_max = $page_num * $items_per_page;
            }
        }

        $page->page = str_replace('{nextprev_listing_num_min}', (string)$listing_num_min, $page->page);
        $page->page = str_replace('{nextprev_listing_num_max}', (string)$listing_num_max, $page->page);
        $prevpage = $cur_page - 1;
        $nextpage = $cur_page + 1;
        $next10page = $cur_page + 10;
        $prev10page = $cur_page - 10;
        $page->page = str_replace('{nextprev_nextpage}', (string)$nextpage, $page->page);
        $page->page = str_replace('{nextprev_prevpage}', (string)$prevpage, $page->page);
        $page->page = str_replace('{nextprev_next10page}', (string)$next10page, $page->page);
        $page->page = str_replace('{nextprev_prev10page}', (string)$prev10page, $page->page);

        if ($_GET['action'] == 'searchresults') {
            $page->page = $page->cleanupTemplateBlock('nextprev_show_save_search', $page->page);
        } else {
            $page->page = $page->removeTemplateBlock('nextprev_show_save_search', $page->page);
        }
        if ($_GET['action'] == 'searchresults') {
            $page->page = $page->cleanupTemplateBlock('nextprev_show_refine_search', $page->page);
        } else {
            $page->page = $page->removeTemplateBlock('nextprev_show_refine_search', $page->page);
        }
        if ($page_num <= 1) {
            $page->page = $page->cleanupTemplateBlock('nextprev_is_firstpage', $page->page);
            $page->page = $page->removeTemplateBlock('!nextprev_is_firstpage', $page->page);
        }

        if ($page_num > 1) {
            $page->page = $page->cleanupTemplateBlock('!nextprev_is_firstpage', $page->page);
            $page->page = $page->removeTemplateBlock('nextprev_is_firstpage', $page->page);
        }
        // begin 10 page menu selection
        $count = $cur_page;

        //Determine Where to Start the Page Count At
        $count_start = $count - 10;
        if ($count_start < 0) {
            $count_start = 0;
        } else {
            while (!str_ends_with((string)$count_start, '0')) {
                $count_start++;
            }
        }
        $page_section_part = $page->getTemplateSection('nextprev_page_section');
        $page_section = '';

        $reverse_count = $count_start;
        while ($count > $count_start) {
            // If the last number is a zero, it's divisible by 10 check it...
            if (str_ends_with((string)$count, '0')) {
                break;
            }
            $page_section .= $page_section_part;
            $disp_count = ($reverse_count + 1);

            $page_section = str_replace('{nextprev_count}', (string)$reverse_count, $page_section);
            $page_section = str_replace('{nextprev_disp_count}', (string)$disp_count, $page_section);
            $page_section = $page->cleanupTemplateBlock('nextprev_page_other', $page_section);
            $page_section = $page->removeTemplateBlock('nextprev_page_current', $page_section);
            $count--;
            $reverse_count++;
        }
        $count = $cur_page;
        while ($count < $total_num_page) {
            $page_section .= $page_section_part;
            $disp_count = ($count + 1);
            $page_section = str_replace('{nextprev_count}', (string)$count, $page_section);
            $page_section = str_replace('{nextprev_disp_count}', (string)$disp_count, $page_section);
            if ($page_num == $disp_count) {
                // the currently selected page
                $page_section = $page->cleanupTemplateBlock('nextprev_page_current', $page_section);
                $page_section = $page->removeTemplateBlock('nextprev_page_other', $page_section);
            } else {
                $page_section = $page->cleanupTemplateBlock('nextprev_page_other', $page_section);
                $page_section = $page->removeTemplateBlock('nextprev_page_current', $page_section);
            }
            $count++;
            // If the last number is a zero, it's divisible by 10 check it...
            if (str_ends_with((string)$count, '0')) {
                break;
            }
        }
        $page->replaceTemplateSection('nextprev_page_section', $page_section);
        if ($page_num >= $total_num_page) {
            $page->page = $page->cleanupTemplateBlock('nextprev_lastpage', $page->page);
            $page->page = $page->removeTemplateBlock('!nextprev_lastpage', $page->page);
        }
        if ($page_num < $total_num_page) {
            $page->page = $page->cleanupTemplateBlock('!nextprev_lastpage', $page->page);
            $page->page = $page->removeTemplateBlock('nextprev_lastpage', $page->page);
        }
        // search buttons
        if ($page_num >= 11) { // previous 10 page
            $page->page = $page->cleanupTemplateBlock('nextprev_prev_100_button', $page->page);
            $page->page = $page->removeTemplateBlock('!nextprev_prev_100_button', $page->page);
        } else {
            $page->page = $page->cleanupTemplateBlock('!nextprev_prev_100_button', $page->page);
            $page->page = $page->removeTemplateBlock('nextprev_prev_100_button', $page->page);
        }
        // Next 100 button
        if (($cur_page < ($total_num_page - 10)) && ($total_num_page > 10)) {
            $page->page = $page->cleanupTemplateBlock('nextprev_next_100_button', $page->page);
            $page->page = $page->removeTemplateBlock('!nextprev_next_100_button', $page->page);
        } else {
            $page->page = $page->cleanupTemplateBlock('!nextprev_next_100_button', $page->page);
            $page->page = $page->removeTemplateBlock('nextprev_next_100_button', $page->page);
        }
        if ($_GET['action'] == 'view_log' && $_SESSION['admin_privs'] == 'yes') {
            $page->page = $page->cleanupTemplateBlock('nextprev_clearlog', $page->page);
        } else {
            $page->page = $page->removeTemplateBlock('nextprev_clearlog', $page->page);
        }
        return $page->page;
    }

    /**
     * Check HTTP Referer matches baseurl
     *
     * @return boolean
     */
    public function refererCheck(): bool
    {
        // Make sure data is comming from the site. (Easily faked, but will stop some of the spammers)
        $referers = $this->config['baseurl'];
        $referers = str_replace('http://', '', $referers);
        $referers = str_replace('https://', '', $referers);
        $referers = str_replace('www.', '', $referers);
        $referers = explode('/', $referers);
        //print_r($referers);
        $found = false;
        if (isset($_SERVER['HTTP_REFERER'])) {
            $temp = explode('/', $_SERVER['HTTP_REFERER']);
            $referer = $temp[2];
            //echo $_SERVER['HTTP_REFERER'];
            if (preg_match('/' . $referers[0] . '/i', $referer)) {
                $found = true;
            }
        } elseif (isset($_SERVER['HTTP_ORIGIN'])) {
            $temp = explode('/', $_SERVER['HTTP_ORIGIN']);
            $referer = $temp[2];
            if (preg_match('/' . $referers[0] . '/i', $referer)) {
                $found = true;
            }
        }
        //echo $referers[0];
        //echo $referer;
        return $found;
    }

    /**
     * Function for sending email.
     *
     * @param string      $sender        Sender Name For Email.
     * @param string      $sender_email  Sender Email Address.
     * @param string      $recipient     Recipient's Email.
     * @param string      $message       Message Body.
     * @param string      $subject       Email Subject.
     * @param boolean     $isHTML        Is Email Body HTML.
     * @param boolean     $skipRefCheck  Should we Skip Referrer Check.
     * @param string|null $replyto       Set Optional ReplyTo Name.
     * @param string|null $replyto_email Set Options ReplyTo Email.
     *
     * @return boolean|string Returns string error messages, for validation errors. True/False if email was
     *                        successfully delivered to mail server.
     */
    public function sendEmail(string $sender, string $sender_email, string $recipient, string $message, string $subject, bool $isHTML = false, bool $skipRefCheck = false, ?string $replyto = null, ?string $replyto_email = null): bool|string
    {
        global $lang;
        // Make sure data is comming from the site. (Easily faked, but will stop some of the spammers)
        $referers = $this->config['baseurl'];
        $referers = str_replace('http://', '', $referers);
        $referers = str_replace('https://', '', $referers);
        $referers = str_replace('www.', '', $referers);
        $referers = explode('/', $referers);
        $found = false;
        if ($skipRefCheck) {
            $found = true;
        } elseif (isset($_SERVER['HTTP_REFERER'])) {
            $temp = explode('/', $_SERVER['HTTP_REFERER']);
            $referer = $temp[2];
            if (preg_match('/' . $referers[0] . '/i', $referer)) {
                $found = true;
            }
        }
        if ($found) {
            // First, make sure the form was posted from a browser.
            // For basic web-forms, we don't care about anything
            // other than requests from a browser:
            if (php_sapi_name() != 'cli' && !isset($_SERVER['HTTP_USER_AGENT'])) {
                return '2' . $lang['email_not_authorized'];
            }
            // Attempt to defend against header injections:
            $badStrings = [
                'Content-Type:',
                'MIME-Version:',
                'Content-Transfer-Encoding:',
                'bcc:',
                'cc:',
            ];
            foreach ($badStrings as $v2) {
                if (str_contains($sender, $v2)) {
                    return $lang['email_not_authorized'];
                }
                if (str_contains($sender_email, $v2)) {
                    return $lang['email_not_authorized'];
                }
                if (str_contains($recipient, $v2)) {
                    return $lang['email_not_authorized'];
                }
                if (str_contains($message, $v2)) {
                    return $lang['email_not_authorized'];
                }
                if (str_contains($subject, $v2)) {
                    return $lang['email_not_authorized'];
                }
                if ($replyto != null && str_contains($replyto, $v2)) {
                    return $lang['email_not_authorized'];
                }
                if ($replyto_email != null && str_contains($replyto_email, $v2)) {
                    return $lang['email_not_authorized'];
                }
            }
            // validate Sender_email as a Spam check
            $valid = $this->validateEmail($sender_email);
            if ($valid) {
                //Are we sending via mail() or via phpmailer?
                if ($this->config['phpmailer']) {
                    $mail = new PHPMailer();

                    $mail->IsSMTP(); // telling the class to use SMTP
                    /* Enable  SMTP debug information for testing
                    $mail->SMTPDebug  = 2;
                    */

                    try {
                        $mail->Host = $this->config['mailserver']; // SMTP server
                        if ($this->config['mailport'] == 465) {
                            $mail->SMTPSecure = 'ssl';                 // sets the prefix to the servier
                        }
                        if ($this->config['mailport'] == 587) {
                            $mail->SMTPSecure = 'tls';                 // sets the prefix to the servier
                        }
                        $mail->Port = $this->config['mailport'];                   // set the SMTP port for the server

                        if (trim($this->config['mailuser']) !== '') {
                            $mail->SMTPAuth = true;                  // enable SMTP authentication
                            $mail->Username = $this->config['mailuser'];  // GMAIL username
                            $mail->Password = $this->config['mailpass'];            // GMAIL password
                        }
                        if ($replyto_email == null || $replyto == null) {
                            $mail->AddReplyTo($sender_email, $sender);
                        } else {
                            $mail->AddReplyTo($replyto_email, $replyto);
                        }
                        $mail->AddAddress($recipient);
                        $mail->SetFrom($sender_email, $sender);
                        $mail->Subject = $subject;
                        if ($isHTML) {
                            $htmlmessage = $mail->MsgHTML($message);
                            $mail->IsHTML(false);
                            $mail->Body = $htmlmessage;
                            $mail->AltBody = strip_tags($message);
                        } else {
                            $mail->IsHTML(false);
                            $mail->Body = $message;
                        }

                        if ($mail->Send()) {
                            return true;
                        }
                    } catch (Exception $e) {
                        return $e->errorMessage(); //Pretty error messages from PHPMailer
                    } catch (\Exception $e) {
                        return $e->getMessage(); //Boring error messages from anything else!
                    }
                } else {
                    $message = stripslashes($message);
                    $subject = stripslashes($subject);
                    if ($isHTML) {
                        $header = 'Content-Type: text/html; charset=' . $this->config['charset'] . "\n";
                    } else {
                        $header = 'Content-Type: text/plain; charset=' . $this->config['charset'] . "\n";
                    }

                    $header .= "Content-Transfer-Encoding: 8bit\n";
                    $header .= 'From: "' . $sender . '" <' . $sender_email . ">\n";
                    if ($replyto_email != null && $replyto != null) {
                        $header .= 'Reply-To: "' . $replyto . '" <' . $replyto_email . ">\n";
                    }
                    $header .= 'Return-Path: ' . $this->config['admin_email'] . "\n";
                    $header .= 'X-Sender: <' . $this->config['admin_email'] . ">\n";
                    $header .= 'X-Mailer: Open-Realty ' . $this->config['version'] . ' - Installed at ' . $this->config['baseurl'] . "\n";
                    return mail($recipient, $subject, $message, $header);
                }
            }
        }
        return '1' . $lang['email_not_authorized'];
    }

    /**
     * Determine if user was banned. We track by IP and Cookie
     *
     * @return boolean Returns true is user is banned.
     */
    public function isBannedSiteIp(): bool
    {
        $ip = '';
        //Get Users IP
        if (isset($_SERVER['HTTP_X_FORWARD_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARD_FOR'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $banned_ips = explode("\n", $this->config['banned_ips_site']);
        $is_banned_ip = false;
        foreach ($banned_ips as $bi) {
            if (trim($bi) !== '') {
                if (stripos($ip, $bi) === 0) {
                    $is_banned_ip = true;
                }
            }
        }
        if ($is_banned_ip) {
            //Set a cookie to track the banned user
            setcookie('cookban', $ip, time() + 60 * 60 * 24 * 365, '/');
        } else {
            //Check to see if user has a ban cookie
            if (isset($_COOKIE['cookban']) && $_COOKIE['cookban'] != '') {
                //User was banned on a different IP, alert admin
                $old_ip = $_COOKIE['cookban'];
                //See if old_ip is still banned
                $ip_still_banned = false;
                foreach ($banned_ips as $bi) {
                    if (trim($bi) !== '') {
                        if (stripos($old_ip, $bi) === 0) {
                            $ip_still_banned = true;
                        }
                    }
                }
                if ($ip_still_banned) {
                    echo 'IP is still banned';
                    if (!isset($_SESSION['cookalert_sent'])) {
                        $this->sendEmail($this->config['admin_email'], $this->config['admin_email'], $this->config['admin_email'], 'User origionally banned at IP ' . $old_ip . ' is back using ' . $ip, 'Banned User Has Returned', false, true);
                        $_SESSION['cookalert_sent'] = $ip;
                    } elseif ($_SESSION['cookalert_sent'] != $ip) {
                        $this->sendEmail($this->config['admin_email'], $this->config['admin_email'], $this->config['admin_email'], 'User origionally banned at IP ' . $old_ip . ' is back using ' . $ip, 'Banned User Has Returned', false, true);
                        $_SESSION['cookalert_sent'] = $ip;
                    }
                } else {
                    echo 'IP is not still banned';
                    //We unbanned their IP, stop tracking.
                    setcookie('cookban', '', time() - (3600 * 25), '/');
                }
            }
        }
        return $is_banned_ip;
    }

    /**
     * Wrapper around setcookie function for better testability
     *
     * @param string  $name     Name Of Cookie.
     * @param string  $value    Value of cookie.
     * @param integer $expire   Time to expire.
     * @param string  $path     Cookie Path.
     * @param string  $domain   Cookie Domain.
     * @param boolean $secure   Is Cookie Secure.
     * @param boolean $httponly Is Cookie HTTP Only.
     *
     * @return boolean
     */
    public function setCookie(string $name, string $value = "", int $expire = 0, string $path = "", string $domain = "", bool $secure = false, bool $httponly = false): bool
    {
        return setcookie($name, $value, $expire, $path, $domain, $secure, $httponly);
    }


    /**
     * Wrapper for log__create api for quicly creating log entries.
     *
     * @param string $log_action Log Message.
     */
    public function logAction(string $log_action): void
    {
        // logs user actions
        $log_api = $this->newLogApi();
        try {
            $log_api->create(['log_api_command' => 'MISC', 'log_message' => $log_action]);
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Determine OS Type Windows or Linux
     *
     * @return string
     */
    public function osType(): string
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $OS = 'Windows';
        } else {
            $OS = 'Linux';
        }
        return $OS;
    }

    /**
     * Validate user Email. Check that the syntac is valid and on Linux verify the dns has an MX record.
     *
     * @param string $email Email address to verify.
     *
     * @return boolean
     */
    public function validateEmail(string $email): bool
    {
        // Validate the syntax
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            [, $domaintld] = explode('@', $email);
            $OS = $this->osType();
            if ($OS == 'Linux') {
                if (checkdnsrr($domaintld)) {
                    return true;
                }
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * Convers a date string back to timestamp.
     *
     * @param string $date   Date string.
     * @param string $format Format the date string is in.
     *
     * @return boolean|integer False on error or timestamp
     */
    public function parseDate(string $date, string $format): bool|int
    {
        //Supported formats
        //%Y - year as a decimal number including the century
        //%m - month as a decimal number (range 01 to 12)
        //%d - day of the month as a decimal number (range 01 to 31)
        //%H - hour as a decimal number using a 24-hour clock (range 00 to 23)
        //%M - minute as a decimal number
        // Builds up date pattern from the given $format, keeping delimiters in place.
        if (!preg_match_all('/%([YmdHMp])([^%])*/', $format, $formatTokens, PREG_SET_ORDER)) {
            return false;
        }
        $datePattern = '';
        foreach ($formatTokens as $formatToken) {
            $delimiter = preg_quote($formatToken[2], '/');
            $datePattern .= '(.*)' . $delimiter;
        }
        // Splits up the given $date
        if (!preg_match('/' . $datePattern . '/', $date, $dateTokens)) {
            return false;
        }
        $dateSegments = [];
        $formatTokenCount = count($formatTokens);
        for ($i = 0; $i < $formatTokenCount; $i++) {
            $dateSegments[$formatTokens[$i][1]] = $dateTokens[$i + 1];
        }
        // Reformats the given $date into US English date format, suitable for strtotime()
        if ($dateSegments['Y'] && $dateSegments['m'] && $dateSegments['d']) {
            $dateReformated = $dateSegments['Y'] . '-' . $dateSegments['m'] . '-' . $dateSegments['d'];
        } else {
            return false;
        }
        if ($dateSegments['H'] && $dateSegments['M']) {
            $dateReformated .= ' ' . $dateSegments['H'] . ':' . $dateSegments['M'];
        }

        return strtotime($dateReformated);
    }

    /**
     * Makes file names safe. Strips all non AlphaNum . or _ chars.
     *
     * @param string $filename Filename to parse.
     *
     * @return string
     */
    public function cleanFilename(string $filename): string
    {
        return preg_replace('/[^a-zA-Z0-9\_\.]/', '', $filename);
    }

    /**
     * Format a timestamp to a string formatted date. Matches $this->config['date_format']
     *
     * @param integer|string $timestamp Unix Timestamp string or int.
     * @param boolean        $time      Should we return the time or just date.
     *
     * @return string
     */
    public function convertTimestamp(int|string $timestamp, bool $time = false): string
    {
        $format = '';
        switch ($this->config['date_format']) {
            case 1:
                $format = 'm/d/Y';
                break;
            case 2:
                $format = 'Y/d/m';
                break;
            case 3:
                $format = 'd/m/Y';
                break;
        }
        if ($time === true) {
            $format .= ' h:i A';
        }
        $timestamp = intval($timestamp);
        return date($format, $timestamp);
    }

    /**
     * Fetch a URL's content
     *
     * @param string  $url   URL to fetch.
     * @param integer $cache How long cached results are valid.
     *
     * @return boolean|string
     */
    public function getUrl(string $url, int $cache = 0): bool|string
    {
        $cache_file = $this->config["basepath"] . "/files/download_cache/" . sha1($url) . '.cache';

        if ($cache > 0 && file_exists($cache_file) && (time() - $cache < filemtime($cache_file))) {
            $data = file_get_contents($cache_file);
        } else {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            $data = curl_exec($ch);
            if (!is_bool($data)) {
                /**
                 * @var array|bool $header
                 */
                $header = curl_getinfo($ch);
                curl_close($ch);
                if (is_array($header) && $header['http_code'] != '200') {
                    $this->logAction('Get URL Failed - ' . $url . ' Status Code: ' . $header['http_code']);
                    $this->logAction(print_r($header, true));
                    return false;
                }
                if ($cache > 0) {
                    file_put_contents($cache_file, $data);
                }
            }
        }

        return $data;
    }

    /**
     * Random password generator accepts numerical password length.
     *
     * @param integer $length Length of password 10 chars by default.
     *
     * @return string
     */
    public function generatePassword(int $length = 10): string
    {
        $chars = 'abcdefghjklmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        return substr(str_shuffle($chars), 0, $length);
    }

    /**
     * Get s users admin status
     *
     * @param integer $userdb_id UserID to check if they are an admin.
     *
     * @return boolean
     */
    public function getAdminStatus(int $userdb_id): bool
    {
        $user_api = $this->newUserApi();
        try {
            $result = $user_api->read([
                'user_id' => $userdb_id,
                'resource' => 'agent',
                'fields' => ['userdb_is_admin'],
            ]);
        } catch (\Exception) {
            return false;
        }
        if (isset($result['user']['userdb_is_admin']) && $result['user']['userdb_is_admin'] === 'yes') {
            return true;
        }
        return false;
    }

    /**
     * Get Agent status
     *
     * @param integer $userdb_id User ID to check if they are an agent.
     *
     * @return boolean
     */
    public function getAgentStatus(int $userdb_id): bool
    {
        $user_api = $this->newUserApi();
        try {
            $result = $user_api->read([
                'user_id' => $userdb_id,
                'resource' => 'agent',
                'fields' => ['userdb_is_agent'],
            ]);
        } catch (\Exception) {
            return false;
        }
        if (isset($result['user']['userdb_is_agent']) && $result['user']['userdb_is_agent'] == 'yes') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get active status
     *
     * @param integer $userdb_id User ID to check if they are active.
     *
     * @return boolean
     */
    public function getActiveStatus(int $userdb_id): bool
    {
        $is_agent = $this->getAgentStatus($userdb_id);

        if ($is_agent) {
            $res = 'agent';
        } else {
            $res = 'member';
        }

        $user_api = $this->newUserApi();
        try {
            $result = $user_api->read([
                'user_id' => $userdb_id,
                'resource' => $res,
                'fields' => ['userdb_active'],
            ]);
        } catch (\Exception) {
            return false;
        }

        if (isset($result['user']['userdb_active']) && $result['user']['userdb_active'] == 'yes') {
            return true;
        } else {
            return false;
        }
    }
}
