<?php

declare(strict_types=1);

namespace OpenRealty;

class Calculators extends BaseClass
{
    /**
     * startCalc()
     * This is the function which displays the calculator.
     *
     * @return string Returns the html to dispaly the calculators
     */
    public function startCalc(): string
    {
        $page = $this->newPageUser();

        //Load TEmplate File
        $page->loadPage($this->config['template_path'] . '/calculator.html');

        //We are done finish output
        $page->replaceLangTemplateTags();
        $page->replacePermissionTags();
        $page->autoReplaceTags('', true);
        return $page->returnPage();
    }
}
