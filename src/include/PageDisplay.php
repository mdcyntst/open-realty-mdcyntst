<?php

declare(strict_types=1);

namespace OpenRealty;

class PageDisplay extends BaseClass
{
    public function display(): string
    {
        global $ORconn, $lang, $meta_canonical;

        $misc = $this->newMisc();
        $page = $this->newPageUser();
        // Make Sure we passed the PageID
        $display = '';
        if (!isset($_GET['PageID'])) {
            $display .= 'ERROR. PageID not sent';
        }
        $page_id = intval($_GET['PageID']);
        $display .= '<div class="page_display">';
        $sql = 'SELECT pagesmain_full,pagesmain_id,pagesmain_published 
				FROM ' . $this->config['table_prefix'] . 'pagesmain 
				WHERE pagesmain_id=' . $page_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $full = $recordSet->fields('pagesmain_full');

        //Deal with Code Tags
        preg_match_all('/<code>(.*?)<\/code>/is', $full, $code_tags);
        if (isset($code_tags[1])) {
            foreach ($code_tags[1] as $x => $tag) {
                $new_tag = str_replace('{', '&#123;', $tag);
                $new_tag = str_replace('}', '&#125;', $new_tag);
                $new_tag = str_replace('>', '&gt;', $new_tag);
                $new_tag = str_replace('<', '&lt;', $new_tag);
                $code_tags[1][$x] = $new_tag;
            }
            foreach ($code_tags[0] as $x => $tag) {
                $full = str_replace($tag, '<code>' . $code_tags[1][$x] . '</code>', $full);
            }
        }
        $id = $recordSet->fields('pagesmain_id');
        $status = $recordSet->fields('pagesmain_published');
        if ($status != 1) {
            return $lang['listing_editor_permission_denied'];
        }

        $display .= $full;
        // Allow Admin To Edit #
        if (((isset($_SESSION['editpages']) && $_SESSION['editpages'] == 'yes') || (isset($_SESSION['editpages']) && $_SESSION['admin_privs'] == 'yes')) && $this->config['wysiwyg_show_edit']) {
            $display .= '<br />';
            $display .= "<a href=\"" . $this->config['baseurl'] . "/admin/index.php?action=edit_page_post&amp;id=$id\">$lang[edit_html_from_site]</a>";
        }

        //Add Canonical Link
        $meta_canonical = $page->magicURIGenerator('page', strval($page_id), true);
        $display .= '</div>';
        // parse page for template varibales

        $template = $this->newPageUser();
        $template->page = $display;
        $template->replaceTags(['templated_search_form', 'featured_listings_horizontal', 'featured_listings_vertical', 'company_name', 'link_printer_friendly']);
        $page->replaceSearchFieldTags();
        return $template->returnPage();
    }

    public function showPageNotFound(): string
    {
        $page = $this->newPageUser();
        $page->loadPage($this->config['template_path'] . '/not_found.html');
        header('HTTP/1.0 404 Not Found');
        return $page->returnPage();
    }

    //consolidate these 3 functions?
    public function getPageTitle(int $page_id): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $sql = 'SELECT pagesmain_title 
				FROM ' . $this->config['table_prefix'] . 'pagesmain 
				WHERE pagesmain_id=' . $page_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        /**
         * @var string $title
         */
        $title = $recordSet->fields('pagesmain_title');
        return $title;
    }

    public function getPageDescription(int $page_id): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        if (isset($_GET['PageID'])) {
            $sql = 'SELECT pagesmain_description 
					FROM ' . $this->config['table_prefix'] . 'pagesmain
					WHERE pagesmain_id=' . $page_id;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            /**
             * @var string $description
             */
            $description = $recordSet->fields('pagesmain_description');
            return $description;
        } else {
            return '';
        }
    }

    public function getPageKeywords(int $page_id): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        if (isset($_GET['PageID'])) {
            $sql = 'SELECT pagesmain_keywords 
					FROM ' . $this->config['table_prefix'] . 'pagesmain 
					WHERE pagesmain_id=' . $page_id;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            /**
             * @var string $keywords
             */
            $keywords = $recordSet->fields('pagesmain_keywords');
            return $keywords;
        } else {
            return '';
        }
    }
}
