<?php

declare(strict_types=1);

namespace OpenRealty;

class LeadManager extends BaseClass
{
    /**
     * Renders the add_lead.html page.
     *
     * @return string
     */
    public function showAddLead(): string
    {
        global $jscript;


        $page = $this->newPageAdmin();

        $lead_functions = $this->newLeadFunctions();
        $jscript .= '{load_css_add_lead}';
        $page->loadPage($this->config['admin_template_path'] . '/add_lead.html');
        $lead_fields = $lead_functions->getFeedbackFormelements();
        $page->replaceTag('feedback_formelements', $lead_fields);
        return $page->returnPage();
    }

    /*
     * Renders the view_leads.html view to view/edit leads.
     * @param boolean $all_lead_view Show the All Leads view or the My Leads view.
     * @return string
     */
    public function feedbackView(bool $all_lead_view = false): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();

        $lead_functions = $this->newLeadFunctions();
        $page = $this->newPageAdmin();

        if (isset($_GET['feedback_id'])) {
            $feedback_id = intval($_GET['feedback_id']);
            $sql = 'SELECT feedbackdb_creation_date, feedbackdb_status, feedbackdb_notes, listingdb_id, feedbackdb_last_modified, userdb_id, feedbackdb_member_userdb_id
					FROM ' . $this->config['table_prefix'] . "feedbackdb
					WHERE feedbackdb_id = " . $feedback_id;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $user_id = (int)$recordSet->fields('userdb_id');
            $listingsdb_id = (int)$recordSet->fields('listingdb_id');
            $permission = false;
            if ($_SESSION['userID'] !== $user_id) {
                if ($all_lead_view) {
                    $permission = $login->verifyPriv('edit_all_leads');
                }
            } else {
                $permission = true;
            }
            if (!$permission) {
                return $lang['listing_editor_permission_denied'];
            }

            $member_id = (int)$recordSet->fields('feedbackdb_member_userdb_id');
            $creation_day = $recordSet->UserTimeStamp($recordSet->fields('feedbackdb_creation_date'), 'd');
            $creation_month = $recordSet->UserTimeStamp($recordSet->fields('feedbackdb_creation_date'), 'm');
            $creation_year = $recordSet->UserTimeStamp($recordSet->fields('feedbackdb_creation_date'), 'Y');
            $creation_hour = $recordSet->UserTimeStamp($recordSet->fields('feedbackdb_creation_date'), 'G');
            $creation_minute = $recordSet->UserTimeStamp($recordSet->fields('feedbackdb_creation_date'), 'i');
            $feedbackdb_status = $recordSet->fields('feedbackdb_status');
            //Format Y-m-d per ISO8601 for fullcalendar
            $calendar_creation_date = $recordSet->UserTimeStamp($recordSet->fields('feedbackdb_creation_date'), 'Y-m-d');
            $notes = htmlentities($recordSet->fields('feedbackdb_notes'), ENT_COMPAT, $this->config['charset']);

            $active_query_string = "";
            if (!isset($_GET['active']) || $_GET['active'] == 1) {
                $active_query_string = '&active=1';
            } else {
                $active_query_string = '&active=0';
            }
            $page->loadPage($this->config['admin_template_path'] . '/view_leads.html');
            $page->page = str_replace('{active_query_string}', $active_query_string, $page->page);
            $page->page = str_replace('{creation_hour}', $creation_hour, $page->page);
            $page->page = str_replace('{creation_minute}', $creation_minute, $page->page);
            $page->page = str_replace('{creation_day}', $creation_day, $page->page);
            $page->page = str_replace('{creation_month}', $creation_month, $page->page);
            $page->page = str_replace('{creation_year}', $creation_year, $page->page);
            $page->page = str_replace('{calendar_creation_date}', $calendar_creation_date, $page->page);
            $page->page = $this->leadManagerPaginationTags($all_lead_view, $page->page);

            //$page->replaceTag('leadmanager_status', $this->leadmanager_status());

            if ($feedbackdb_status == 1) {
                $page->page = $page->cleanupTemplateBlock('leadmanager_status', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('leadmanager_status', $page->page);
            }

            $page->page = $lead_functions->getLeadmanagerPriority($feedback_id, $page->page);

            $field = $lead_functions->renderTemplateArea('headline', $feedback_id);
            $page->page = str_replace('{headline}', $field, $page->page);

            $field = $lead_functions->renderTemplateArea('top_left', $feedback_id);
            $page->page = str_replace('{top_left}', $field, $page->page);
            $field = $lead_functions->renderTemplateArea('top_right', $feedback_id);
            $page->page = str_replace('{top_right}', $field, $page->page);

            $field = $lead_functions->renderTemplateArea('center', $feedback_id);

            $page->page = str_replace('{center}', $field, $page->page);

            $field = $lead_functions->renderTemplateArea('bottom_left', $feedback_id);
            $page->page = str_replace('{bottom_left}', $field, $page->page);
            $field = $lead_functions->renderTemplateArea('bottom_right', $feedback_id);
            $page->page = str_replace('{bottom_right}', $field, $page->page);
            //End of Template Areas

            $page->page = str_replace('{notes}', $notes, $page->page);
            $feedback_last_modified = $recordSet->UserTimeStamp($recordSet->fields('feedbackdb_last_modified'), 'm/d/Y g:ia');
            $feedbackdb_creation_date = $recordSet->UserTimeStamp($recordSet->fields('feedbackdb_creation_date'), 'm/d/Y g:ia');
            $modifiedby = $lead_functions->getFeedbackModData($feedback_id, 'modifiedby');
            $page->replaceTag('modifiedby', $modifiedby);

            //Agent Dropdown
            $sql = 'SELECT userdb_user_first_name,  userdb_user_last_name, userdb_id
             FROM ' . $this->config['table_prefix'] . "userdb
             WHERE userdb_is_agent = 'yes' ";
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $agents = [];
            // get main users data
            while (!$recordSet->EOF) {
                $agent = (int)$recordSet->fields('userdb_id');
                $first_name = $recordSet->fields('userdb_user_first_name');
                $last_name = $recordSet->fields('userdb_user_last_name');
                $user_name = $first_name . ' ' . $last_name;
                $agents[$agent] = $user_name;
                $recordSet->MoveNext();
            } //end while

            $html = $page->getTemplateSection('leadmanager_agent_block');
            $html = $page->formOptions($agents, (string)$user_id, $html);
            $page->replaceTemplateSection('leadmanager_agent_block', $html);

            $page->replaceTag('lastmodified', $feedback_last_modified);
            $page->replaceTag('creationdate', $feedbackdb_creation_date);
            $page->replaceTag('feedback_id', (string)$feedback_id);
            $page->replaceUserFieldTags($member_id);
            $edit_listing_link = $this->config['baseurl'] . '/admin/index.php?action=edit_listing&edit=' . $listingsdb_id;
            $page->replaceTag('edit_listing_link', $edit_listing_link);
            $page->replaceListingFieldTags($listingsdb_id);
            $page->replacePermissionTags();
            $page->replaceUrls();
            //$page->replaceMetaTemplateTags();
            $page->autoReplaceTags();
            //$page->replaceTags(array('load_js', 'load_ORjs', 'load_js_last'));
            $page->replaceLangTemplateTags();
            $page->replaceCssTemplateTags();

            $output = $page->returnPage();
        } else {
            $output = '<a href="index.php">Not a valid request</a>';
        }
        return $output;
    }

    public function ajaxLeadManagerDatatable(bool $show_all_leads = false): string
    {
        global $ORconn, $lang;
        $misc = $this->newMisc();
        header('Content-type: application/json');
        $ARGS = [];
        $login = $this->newLogin();

        $aColumns = ['feedbackdb_id', 'feedbackdb_member_userdb_id', 'feedbackdb_creation_date', 'feedbackdb_priority', 'feedbackdb_status', 'userdb_id'];
        $where = 'userdb_id = ' . intval($_SESSION['userID']) . ' ';
        if ($show_all_leads) {
            $login_status = $login->verifyPriv('edit_all_leads');
            if ($login_status === true) {
                $where = '';
            } else {
                return $lang['permission_denied'];
            }
        }

        //Do Search to get total record count, no need pass in soring information
        $limit = 0;
        $offset = 0;
        if ($where != '') {
            $sql = 'SELECT count(*) as mycount 
				FROM ' . $this->config['table_prefix'] . "feedbackdb WHERE $where";
        } else {
            $sql = 'SELECT count(*) as mycount 
				FROM ' . $this->config['table_prefix'] . "feedbackdb";
        }
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $iTotal = $recordSet->fields('mycount');
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $limit = intval($_GET['iDisplayLength']);
            $offset = intval($_GET['iDisplayStart']);
            //$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".mysql_real_escape_string( $_GET['iDisplayLength'] );
        }
        $sortby = [];
        $sorttype = [];
        //Deal with sorting
        if (isset($_GET['order'])) {
            for ($i = 0; $i < intval($_GET['order']); $i++) {
                if (isset($_GET['order'][$i]['column']) && isset($_GET['columns'][$_GET['order'][$i]['column']]['orderable']) && $_GET['columns'][$_GET['order'][$i]['column']]['orderable'] == 'true') {
                    $sortby[$i] = $aColumns[$_GET['order'][$i]['column']];
                    $sorttype[$i] = strtoupper($_GET['order'][$i]['dir']);
                }
            }
        }
        $sWhere = '';
        if (isset($_GET['search']['value']) && is_scalar($_GET['search']['value']) && $_GET['search']['value'] != '') {
            $sql_sSearch = $ORconn->qstr('%' . $_GET['search']['value'] . '%');

            $sWhere = 'WHERE (';

            for ($i = 0; $i < count($aColumns); $i++) {
                /** @var int<0,5> $i */
                if ($aColumns[$i] == 'userdb_id') {
                    continue;
                }
                if ($aColumns[$i] == 'feedbackdb_member_userdb_id') {
                    $sWhere .= '`' . $aColumns[$i] . '` IN (SELECT userdb_id FROM ' . $this->config['table_prefix'] . "userdb 
																		WHERE CONCAT(userdb_user_last_name,', ',userdb_user_first_name) 
																		LIKE " . $sql_sSearch . ') OR ';
                } else {
                    $sWhere .= '`' . $aColumns[$i] . '` LIKE ' . $sql_sSearch . ' OR ';
                }
            }
            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }
        //Deal with column filters
        if (isset($_GET['columns']) && is_array($_GET['columns'])) {
            for ($i = 0; $i < count($_GET['columns']); $i++) {
                /** @var int<0,5> $i */
                if (isset($_GET['columns'][$i]['searchable']) && isset($_GET['columns'][$i]['search']['value']) && $_GET['columns'][$i]['searchable'] == 'true' && $_GET['columns'][$i]['search']['value'] != '') {
                    $ARGS[$aColumns[$i]] = $_GET['columns'][$i]['search']['value'];
                }
            }
        }
        if (!empty($ARGS)) {
            foreach ($ARGS as $f => $k) {
                if (empty($sWhere)) {
                    $sWhere .= ' WHERE ';
                } else {
                    $sWhere .= 'AND ';
                }

                if ($f == 'feedbackdb_status') {
                    $sWhere .= $ORconn->addQ($f) . ' = ' . intval($k);
                } elseif ($f == 'feedbackdb_member_userdb_id') {
                    $sWhere .= '`' . $ORconn->addQ($f) . '` IN (SELECT userdb_id 
													FROM ' . $this->config['table_prefix'] . "userdb 
													WHERE CONCAT(userdb_user_last_name,', ',userdb_user_first_name) 
													LIKE '%" . $ORconn->addQ($k) . "%') ";
                } else {
                    $sWhere .= $ORconn->addQ($f) . ' LIKE \'%' . $ORconn->addQ($k) . '%\'';
                }
            }
        }
        $sort = '';
        if (count($sortby) > 0) {
            $sort .= ' ORDER BY ';
            foreach ($sortby as $x => $f) {
                if ($sort != ' ORDER BY ') {
                    $sort .= ', ';
                }
                $sort .= $ORconn->addQ($f) . ' ' . $ORconn->addQ($sorttype[$x]);
            }
        }
        $limitstr = '';
        if ($limit > 0) {
            $limitstr .= 'LIMIT ' . $offset . ', ' . $limit;
        }
        $join = '';
        if ($sWhere != '' && $where != '') {
            $join = ' AND ';
        } elseif ($sWhere == '' && $where != '') {
            $join = ' WHERE ';
        }
        $sql = 'SELECT count(*) as filteredcount FROM ' . $this->config['table_prefix'] . "feedbackdb $sWhere $join $where";

        //echo $sql;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $iFilteredTotal = $recordSet->fields('filteredcount');
        $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . "feedbackdb $sWhere $join $where $sort $limitstr ";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }

        $draw = 0;
        if (isset($_GET['draw'])) {
            $draw = intval($_GET['draw']);
        }

        $output = [
            'draw' => $draw,
            'recordsTotal' => $iTotal,
            'recordsFiltered' => $iFilteredTotal,
            'data' => [],
        ];
        $status_array = [];
        $status_array[0] = $lang['leadmanager_status_inactive'];
        $status_array[1] = $lang['leadmanager_status_active'];
        if ($show_all_leads) {
            $lead_edit_action = 'leadmanager_feedback_edit';
            $lead_view_action = 'leadmanager_viewfeedback';
        } else {
            $lead_edit_action = 'leadmanager_my_feedback_edit';
            $lead_view_action = 'leadmanager_my_viewfeedback';
        }
        while (!$recordSet->EOF) {
            $row = [];
            //feedbackdb_id, feedbackdb_member_userdb_id, userdb_id, feedbackdb_creation_date, feedbackdb_priority, feedbackdb_status, ,
            $lead_id = $recordSet->fields('feedbackdb_id');
            $row[] = $recordSet->fields('feedbackdb_id');
            $sqlMember = 'SELECT userdb_user_first_name, userdb_user_last_name
			FROM ' . $this->config['table_prefix'] . 'userdb
			WHERE userdb_id =' . $recordSet->fields('feedbackdb_member_userdb_id');

            $recordSet2 = $ORconn->execute($sqlMember);
            if (is_bool($recordSet2)) {
                $misc->logErrorAndDie($sql);
            }
            $first_name = $recordSet2->fields('userdb_user_first_name');
            $last_name = $recordSet2->fields('userdb_user_last_name');
            $row[] = $last_name . ', ' . $first_name;

            $row[] = $recordSet->UserTimeStamp($recordSet->fields('feedbackdb_creation_date'), 'D M j G:i:s T Y');
            $row[] = $recordSet->fields('feedbackdb_priority');
            $row[] = $status_array[$recordSet->fields('feedbackdb_status')];
            $sqlMember = 'SELECT userdb_user_first_name, userdb_user_last_name
			FROM ' . $this->config['table_prefix'] . 'userdb
			WHERE userdb_id =' . $recordSet->fields('userdb_id');

            $recordSet2 = $ORconn->execute($sqlMember);
            if (is_bool($recordSet2)) {
                $misc->logErrorAndDie($sql);
            }
            $first_name = $recordSet2->fields('userdb_user_first_name');
            $last_name = $recordSet2->fields('userdb_user_last_name');
            $row[] = $last_name . ', ' . $first_name;
            $row[] = '<a class="btn btn-sm btn-primary mb-0" onclick="gotoLead(\'' . $this->config['baseurl'] . '/admin/index.php?action=' . $lead_edit_action . '&feedback_id=' . $lead_id . '\');" href="#" title="' . $lang['edit'] . '">
                <i class="fa-solid fa-pencil-alt"></i>
				</a>

				<a class="btn btn-sm btn-primary mb-0" onclick="gotoLead(\'' . $this->config['baseurl'] . '/admin/index.php?action=' . $lead_view_action . '&feedback_id=' . $lead_id . '\');" href="#" title="' . $lang['view'] . '">
                <i class="fa-solid fa-eye"></i>
				</a>

				<a class="btn btn-sm btn-danger mb-0" href="#"	onclick="deleteLead(' . $lead_id . ');" title="' . $lang['delete'] . '">
                <i class="fa-solid fa-trash"></i>
				</a> 
				';
            $output['data'][] = $row;
            $recordSet->MoveNext();
        }
        return json_encode($output) ?: '';
    }


    /**
     * showLeads() Displays a list of available leads
     *
     * @param boolean $show_all_leads Show all leads or my leads.
     *
     * @return string
     */
    public function showLeads(bool $show_all_leads = false): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $page = $this->newPageAdmin();
        $login = $this->newLogin();

        if ($show_all_leads) {
            $login_status = $login->verifyPriv('edit_all_leads');
        } else {
            $login_status = $login->verifyPriv('Agent');
        }
        if ($login_status !== true) {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }

        //Load Template File
        $page->loadPage($this->config['admin_template_path'] . '/leadmanager.html');

        $display = '';

        if ($show_all_leads) {
            $page->replaceTag('show_agent_column', 'true');
        } else {
            $page->replaceTag('show_agent_column', 'false');
        }


        //Get Counts
        if ($show_all_leads) {
            $sql2 = 'SELECT count(feedbackdb_id) as count 
					FROM ' . $this->config['table_prefix'] . 'feedbackdb 
					WHERE feedbackdb_status = \'1\'';
        } else {
            //
            $sql2 = 'SELECT count(feedbackdb_id) as count 
					FROM ' . $this->config['table_prefix'] . 'feedbackdb 
					WHERE userdb_id = ' . intval($_SESSION['userID']) . ' 
					AND feedbackdb_status = \'1\'';
        }
        $recordSet2 = $ORconn->Execute($sql2);
        if (is_bool($recordSet2)) {
            $misc->logErrorAndDie($sql2);
        }
        $active_lead_count = $recordSet2->fields('count');
        if ($show_all_leads) {
            $sql2 = 'SELECT count(feedbackdb_id) as count 
					FROM ' . $this->config['table_prefix'] . 'feedbackdb 
					WHERE feedbackdb_status = \'0\'';
        } else {
            // WHERE userdb_id = '.intval($_SESSION['userID']).'
            $sql2 = 'SELECT count(feedbackdb_id) as count 
					FROM ' . $this->config['table_prefix'] . 'feedbackdb 
					WHERE userdb_id = ' . intval($_SESSION['userID']) . ' 
					AND feedbackdb_status = \'0\'';
        }
        $recordSet2 = $ORconn->Execute($sql2);
        if (is_bool($recordSet2)) {
            $misc->logErrorAndDie($sql2);
        }
        $inactive_lead_count = $recordSet2->fields('count');

        //lead_inactive_count
        //lead_active_count
        $page->replaceTag('lead_inactive_count', $inactive_lead_count);
        $page->replaceTag('lead_active_count', $active_lead_count);
        //Feed Back Status Array

        $page->replacePermissionTags();
        $page->autoReplaceTags('', true);
        $display .= $page->returnPage();
        return $display;
    }

    public function ajaxChangeLeadNotes(): string
    {
        global $lang;

        if (isset($_POST['feedback_id']) && is_numeric($_POST['feedback_id']) && isset($_POST['notes']) && is_string($_POST['notes'])) {
            $lead_functions = $this->newLeadFunctions();

            $login = $this->newLogin();
            $feedback_id = intval($_POST['feedback_id']);
            $notes = $_POST['notes'];
            $current_owner = $lead_functions->getFeedbackOwner($feedback_id);
            //Make sure we have permissions.

            if ($_SESSION['userID'] !== $current_owner) {
                $permission = $login->verifyPriv('edit_all_leads');
            } else {
                $permission = true;
            }
            if (!$permission) {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
            }
            $lead_functions->setNotes($feedback_id, $notes);
            $lead_functions->setFeedbackMods($feedback_id);
            header('Content-type: application/json');
            return json_encode(['error' => '0', 'lead_id' => $feedback_id]) ?: '';
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
        }
    }

    public function ajaxChangeLeadPriority(): string
    {
        global $lang;

        if (isset($_POST['feedback_id']) && is_numeric($_POST['feedback_id']) && isset($_POST['priority']) && is_string($_POST['priority']) && in_array($_POST['priority'], ['Low', 'Normal', 'Urgent'])) {
            $lead_functions = $this->newLeadFunctions();

            $login = $this->newLogin();
            $feedback_id = intval($_POST['feedback_id']);
            $priority = $_POST['priority'];
            $current_owner = $lead_functions->getFeedbackOwner($feedback_id);
            //Make sure we have permissions.
            if ($_SESSION['userID'] !== $current_owner) {
                $permission = $login->verifyPriv('edit_all_leads');
            } else {
                $permission = true;
            }
            if (!$permission) {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
            }
            $lead_functions->setFeedbackPriority($feedback_id, $priority);
            $lead_functions->setFeedbackMods($feedback_id);
            header('Content-type: application/json');
            return json_encode(['error' => '0', 'lead_id' => $feedback_id]) ?: '';
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
        }
    }

    public function ajaxChangeLeadStatus(): string
    {
        global $lang;

        if (isset($_POST['feedback_id']) && is_numeric($_POST['feedback_id']) && isset($_POST['status']) && is_numeric($_POST['status'])) {
            $lead_functions = $this->newLeadFunctions();

            $login = $this->newLogin();
            $feedback_id = intval($_POST['feedback_id']);
            $status = intval($_POST['status']);
            $current_owner = $lead_functions->getFeedbackOwner($feedback_id);
            //Make sure we have permissions.
            if ($_SESSION['userID'] !== $current_owner) {
                $permission = $login->verifyPriv('edit_all_leads');
            } else {
                $permission = true;
            }
            if (!$permission) {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
            }
            $lead_functions->setFeedbackStatus($feedback_id, $status);
            $lead_functions->setFeedbackMods($feedback_id);
            header('Content-type: application/json');
            return json_encode(['error' => '0', 'lead_id' => $feedback_id]) ?: '';
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
        }
    }

    public function ajaxChangeLeadAgent(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        if (isset($_POST['feedback_id']) && is_numeric($_POST['feedback_id']) && isset($_POST['user']) && is_numeric($_POST['user'])) {
            $lead_functions = $this->newLeadFunctions();

            $login = $this->newLogin();

            $feedback_id = intval($_POST['feedback_id']);
            $new_user_id = intval($_POST['user']);
            $user_id = intval($_SESSION['userID']);
            $current_owner = $lead_functions->getFeedbackOwner($feedback_id);
            //Make sure we have permissions.
            if ($_SESSION['userID'] !== $current_owner) {
                $permission = $login->verifyPriv('edit_all_leads');
            } else {
                $permission = true;
            }
            if (!$permission) {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
            }
            $sql = 'UPDATE ' . $this->config['table_prefix'] . 'feedbackdb
           		SET userdb_id = ' . $new_user_id . '
           		WHERE (feedbackdb_id = ' . $feedback_id . ')';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $sql = 'UPDATE ' . $this->config['table_prefix'] . 'feedbackdbelements
           		SET userdb_id = ' . $new_user_id . '
           		WHERE (feedbackdb_id = ' . $feedback_id . ')';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $lead_functions->setFeedbackMods($feedback_id);
            $lead_functions->sendAgentLeadAssignedNotice($feedback_id, $user_id);
            header('Content-type: application/json');
            return json_encode(['error' => '0', 'lead_id' => $feedback_id]) ?: '';
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
        }
    }

    /*
     *  showFeedbackEdit() Edit a specific lead
     */
    public function showFeedbackEdit(int $edit, bool $all_lead_view = false): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $forms = $this->newForms();
        $lead_functions = $this->newLeadFunctions();
        $page = $this->newPageAdmin();
        $display = '';


        // first, grab the feedback's main info
        $sql = 'SELECT feedbackdb_id, feedbackdb_notes, userdb_id, feedbackdb_creation_date, feedbackdb_last_modified,
				listingdb_id, feedbackdb_member_userdb_id, feedbackdb_status
        		FROM ' . $this->config['table_prefix'] . 'feedbackdb
        		WHERE (feedbackdb_id = ' . $edit . ')';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $num_records = $recordSet->RecordCount();
        if ($num_records == 1) {
            // collect up the main DB's various fields
            $feedback_id = (int)$recordSet->fields('feedbackdb_id');
            $listingsdb_id = (int)$recordSet->fields('listingdb_id');
            $edit_notes = $recordSet->fields('feedbackdb_notes');
            $creation_date = $recordSet->UserTimeStamp($recordSet->fields('feedbackdb_creation_date'), 'm-d-Y g:ia');
            $user_id = (int)$recordSet->fields('userdb_id');
            $member_id = (int)$recordSet->fields('feedbackdb_member_userdb_id');
            $last_modified = $recordSet->UserTimeStamp($recordSet->fields('feedbackdb_last_modified'), 'm-d-Y g:ia');
            $feedbackdb_status = $recordSet->fields('feedbackdb_status');

            $recordSet->MoveNext();
        } else {
            die('Failed to access Lead DB');
        }
        //Make sure we have permission.
        $permission = false;
        if (intval($_SESSION['userID']) !== $user_id) {
            if ($all_lead_view) {
                $permission = $login->verifyPriv('edit_all_leads');
            }
        } else {
            $permission = true;
        }
        if (!$permission) {
            return $lang['listing_editor_permission_denied'];
        }

        $headline = '';
        $top_left = '';
        $top_right = '';
        $center = '';
        $bottom_left = '';
        $bottom_right = '';
        $misc_hold = '';

        $sql = 'SELECT feedbackformelements_field_name, feedbackdbelements_field_value, feedbackformelements_location,
						feedbackformelements_field_type, feedbackformelements_field_caption, feedbackformelements_default_text,
						feedbackformelements_field_elements, feedbackformelements_required, feedbackformelements_tool_tip
        		FROM ' . $this->config['table_prefix'] . 'feedbackformelements
        		LEFT JOIN ' . $this->config['table_prefix'] . 'feedbackdbelements
        		ON feedbackdbelements_field_name = feedbackformelements_field_name and feedbackdb_id = ' . $edit . '
        		ORDER BY feedbackformelements_rank';
        $recordSet = $ORconn->Execute($sql);

        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }

        while (!$recordSet->EOF) {
            $field_type = $recordSet->fields('feedbackformelements_field_type');
            $field_name = $recordSet->fields('feedbackformelements_field_name');
            $field_value = (string)$recordSet->fields('feedbackdbelements_field_value');
            $field_caption = $recordSet->fields('feedbackformelements_field_caption');
            $default_text = $recordSet->fields('feedbackformelements_default_text');
            $field_elements = (string)$recordSet->fields('feedbackformelements_field_elements');
            $required = $recordSet->fields('feedbackformelements_required');
            $tool_tip = $recordSet->fields('feedbackformelements_tool_tip');
            $location = $recordSet->fields('feedbackformelements_location');
            $field_length = '';
            // pass the data to the function
            $field = $forms->renderFormElement($field_type, $field_name, $field_value, $field_caption, $default_text, $required, $field_elements, $field_length, $tool_tip);

            switch ($location) {
                case 'headline':
                    $headline .= $field;
                    break;
                case 'top_left':
                    $top_left .= $field;
                    break;
                case 'top_right':
                    $top_right .= $field;
                    break;
                case 'center':
                    $center .= $field;
                    break;
                case 'bottom_left':
                    $bottom_left .= $field;
                    break;
                case 'bottom_right':
                    $bottom_right .= $field;
                    break;
                default:
                    $misc_hold .= $field;
                    break;
            }
            $recordSet->MoveNext();
        }

        $active_query_string = "";
        if (!isset($_GET['active']) || $_GET['active'] == 1) {
            $active_query_string = '&active=1';
        } else {
            $active_query_string = '&active=0';
        }

        //get the name of the last person who modified this
        $modified_by = $lead_functions->getFeedbackModData($edit, 'modifiedby');
        $page->loadPage($this->config['admin_template_path'] . '/leadmanager_edit.html');
        $page->page = str_replace('{active_query_string}', $active_query_string, $page->page);
        $page->page = str_replace('{headline}', $headline, $page->page);
        $page->page = str_replace('{top_left}', $top_left, $page->page);
        $page->page = str_replace('{top_right}', $top_right, $page->page);
        $page->page = str_replace('{center}', $center, $page->page);
        $page->page = str_replace('{bottom_left}', $bottom_left, $page->page);
        $page->page = str_replace('{bottom_right}', $bottom_right, $page->page);
        $page->page = str_replace('{misc_hold}', $misc_hold, $page->page);

        $page->page = $this->leadManagerPaginationTags($all_lead_view, $page->page);

        $page->page = $lead_functions->getLeadmanagerPriority($feedback_id, $page->page);


        if ($feedbackdb_status == 1) {
            $page->page = $page->cleanupTemplateBlock('leadmanager_status', $page->page);
        } else {
            $page->page = $page->removeTemplateBlock('leadmanager_status', $page->page);
        }

        //Agent Dropdown
        $sql = 'SELECT userdb_user_first_name,  userdb_user_last_name, userdb_id
        FROM ' . $this->config['table_prefix'] . "userdb
        WHERE userdb_is_agent = 'yes' ";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $agents = [];
        // get main users data
        while (!$recordSet->EOF) {
            $agent = (int)$recordSet->fields('userdb_id');
            $first_name = $recordSet->fields('userdb_user_first_name');
            $last_name = $recordSet->fields('userdb_user_last_name');
            $user_name = $first_name . ' ' . $last_name;
            $agents[$agent] = $user_name;
            $recordSet->MoveNext();
        } //end while

        $html = $page->getTemplateSection('leadmanager_agent_block');
        $html = $page->formOptions($agents, (string)$user_id, $html);
        $page->replaceTemplateSection('leadmanager_agent_block', $html);


        $page->replaceTag('agent_id', (string)$user_id);
        $page->replaceTag('feedback_id', (string)$feedback_id);
        $page->replaceTag('creationdate', $creation_date);
        $page->replaceTag('modifiedby', $modified_by);
        $page->replaceTag('lastmodified', $last_modified);

        $page->replaceTag('edit_notes', $edit_notes);

        $page->replaceListingFieldTags($listingsdb_id);

        $edit_listing_link = $this->config['baseurl'] . '/admin/index.php?action=edit_listing&edit=' . $listingsdb_id;
        $page->replaceTag('edit_listing_link', $edit_listing_link);
        $page->replaceUserFieldTags($member_id);
        $page->replacePermissionTags();
        $page->autoReplaceTags('', true);

        return $page->returnPage();
    }

    public function ajaxSaveFeedbackStatus(): string
    {
        header('Content-type: application/json');

        $lead_functions = $this->newLeadFunctions();
        if (isset($_POST['feedback_id']) && isset($_POST['status'])) {
            $feedback_id = intval($_POST['feedback_id']);
            $status = intval($_POST['status']);
            $lead_functions->setFeedbackStatus($feedback_id, $status);
            $lead_functions->setFeedbackMods($feedback_id);
            return json_encode(['error' => 0, 'status' => $status, 'feedback_id' => $feedback_id]) ?: '';
        }
        http_response_code(500);
        return json_encode(['error' => '1']) ?: '';
    }

    public function ajaxDeleteLead(int $lead_id): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('Admin');
        if (!$security) {
            return json_encode(['error' => '1', 'error_msg' => 'Permission Denied']) ?: '';
        }

        // delete a listing
        // delete a listing
        $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'feedbackdb 
				WHERE feedbackdb_id = ' . $lead_id;
        $recordSet = $ORconn->Execute($sql);

        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        // delete all the elements associated with a listing
        $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'feedbackdbelements 
				WHERE feedbackdb_id = ' . $lead_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        header('Content-type: application/json');
        return json_encode(['error' => 0, 'lead_id' => $lead_id]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxSaveFeedback(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $forms = $this->newForms();
        $login = $this->newLogin();

        $lead_functions = $this->newLeadFunctions();
        $security = $login->verifyPriv('Admin');
        if (!$security) {
            return json_encode(['error' => '1', 'error_msg' => 'Permission Denied']) ?: '';
        }
        // validates the form
        $pass_the_form = $forms->validateForm('feedbackformelements');

        if ($pass_the_form != 'Yes') {
            // if we're not going to pass it, tell that they forgot to fill in one of the fields
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => $lang['required_fields_not_filled']]) ?: '';
        }


        if (isset($_POST['notes']) && is_string($_POST['notes']) && $_POST['edit'] && is_numeric($_POST['edit'])) {
            $notes = $_POST['notes'];
            $feedbackdb_id = (int)$_POST['edit'];
            $sql_notes = $misc->makeDbSafe($notes);
            $sql_feedbackdb_id = $feedbackdb_id;
            $owner = $lead_functions->getFeedbackOwner($sql_feedbackdb_id);
            $user_id = intval($_SESSION['userID']);
            //Make sure we have permission
            $login_status = $login->verifyPriv('edit_all_leads');
            if ($login_status !== true) {
                if ($user_id != $owner) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => 'Permission Denied']) ?: '';
                }
            }

            // update the feedback data
            $sql = 'UPDATE ' . $this->config['table_prefix'] . 'feedbackdb
	                		SET feedbackdb_notes = ' . $sql_notes . '
	                		WHERE feedbackdb_id = ' . $sql_feedbackdb_id;
            $recordSet = $ORconn->Execute($sql);

            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $message = $lead_functions->updateFeedbackData($feedbackdb_id, $owner);
            if ($message == 'success') {
                $lead_functions->setFeedbackMods($feedbackdb_id);
                $misc->logAction('Lead number ' . $feedbackdb_id . ' updated');
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'lead_id' => $feedbackdb_id]) ?: '';
            } else {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => 'Misc Error']) ?: '';
            } // end else
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => 'Incomplete Form Data']) ?: '';
    }


    public function leadManagerPaginationTags(bool $all_leads = false, string $template = ''): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $page = $this->newPageAdmin();

        $sql_queries = [];
        $active_query_string = "";
        if (!isset($_GET['active']) || $_GET['active'] == 1) {
            $sql_queries[] = 'feedbackdb_status = 1';
            $active_query_string = '&active=1';
        } else {
            $sql_queries[] = 'feedbackdb_status = 0';
            $active_query_string = '&active=0';
        }

        if (isset($_GET['action']) && is_string($_GET['action'])) {
            $action = $_GET['action'];

            if (!empty($_GET['feedback_id'])) {
                $feedback_id = intval($_GET['feedback_id']);

                //Set SQL Based ON view my or view all leads
                if (!$all_leads) {
                    $sql_queries[] = 'userdb_id = ' . intval($_SESSION['userID']);
                }
                $sql_queries_string = ' WHERE ' . implode(' AND ', $sql_queries);

                $sql = 'SELECT feedbackdb_id
				FROM ' . $this->config['table_prefix'] . 'feedbackdb
				' . $sql_queries_string . '
				ORDER by feedbackdb_id';
                $recordSet = $ORconn->Execute($sql);

                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $num_rows = $recordSet->RecordCount();
                $ID = [];
                $loop = 0;
                $cur_ID = 0;
                while (!$recordSet->EOF) {
                    $ID[$loop] = (int)$recordSet->fields('feedbackdb_id');
                    if ($ID[$loop] == $feedback_id) {
                        $cur_ID = $loop;
                    }
                    $loop++;
                    $recordSet->MoveNext();
                }
                $real_rows = $num_rows - 1;
                $previous = 0;
                if ($cur_ID > 0) {
                    $previous = $cur_ID - 1;
                }

                if ($feedback_id == $ID[0]) {
                    $previous = $cur_ID;
                }


                $next = $cur_ID + 1;
                if ($cur_ID == $real_rows) {
                    $next = $real_rows;
                }

                $first_page_action = 'index.php?action=' . $action . '&feedback_id=' . $ID[0] . $active_query_string;
                $previous_page_action = 'index.php?action=' . $action . '&feedback_id=' . $ID[$previous] . $active_query_string;
                $next_page_action = 'index.php?action=' . $action . '&feedback_id=' . $ID[$next] . $active_query_string;
                $last_page_action = 'index.php?action=' . $action . '&feedback_id=' . $ID[$real_rows] . $active_query_string;

                $template = $page->replaceTagSafe('leadmanager_pg_first_action', $first_page_action, $template);
                $template = $page->replaceTagSafe('leadmanager_pg_previous_action', $previous_page_action, $template);
                $template = $page->replaceTagSafe('leadmanager_pg_next_action', $next_page_action, $template);
                $template = $page->replaceTagSafe('leadmanager_pg_last_action', $last_page_action, $template);
            }
        }
        return $template;
    }

    /*
     * formEdit() Arrange and edit the Lead Forms
     */
    public function formEdit(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        // Verify User is an Admin
        $security = $login->verifyPriv('edit_lead_template');

        //Load the Core Template

        $page = $this->newPageAdmin();
        $page->loadPage($this->config['admin_template_path'] . '/lead_template_editor.html');
        $display = '';
        $display1 = '';

        if ($security) {
            $display1 .= $this->deleteLeadField();
            $display .= $display1;


            $page->replaceTag('content', $display);


            $sql = 'SELECT feedbackformelements_field_name, feedbackformelements_field_caption, feedbackformelements_id
					FROM ' . $this->config['table_prefix'] . 'feedbackformelements';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $all_fields = [];
            while (!$recordSet->EOF) {
                $all_fields[$recordSet->fields('feedbackformelements_field_name')] = $recordSet->fields('feedbackformelements_field_caption') . ' (' . $recordSet->fields('feedbackformelements_field_name') . ')';
                $recordSet->MoveNext();
            }
            $selected_field = '';
            if (isset($_GET['edit_field']) && is_string($_GET['edit_field'])) {
                $selected_field = $_GET['edit_field'];
            }
            $page->replaceTag('content', $display);

            $html = $page->getTemplateSection('lead_template_editor_field_edit_block');
            $html = $page->formOptions($all_fields, $selected_field, $html);
            $page->replaceTemplateSection('lead_template_editor_field_edit_block', $html);


            $page->replaceTag('application_status_text', '');
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        }
        return '<div class="error_text">' . $lang['access_denied'] . '</div>';
    }

    public function editLeadTemplateQED(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        // Verify User is an Admin
        $security = $login->verifyPriv('edit_lead_template');
        $display = '';

        if ($security === true) {
            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/lead_template_editor_qed.html');
            $sections = explode(',', $this->config['template_lead_sections']);
            $sections[] = 'misc';
            foreach ($sections as $section) {
                $section_name = trim($section);
                if ($section_name == 'misc') {
                    $sql_section_name = $misc->makeDbSafe('');
                } else {
                    $sql_section_name = $misc->makeDbSafe($section_name);
                }

                // Grab a list of field_names in the Database to Edit
                $sql = 'SELECT feedbackformelements_id, feedbackformelements_field_name,
						feedbackformelements_required,
						feedbackformelements_field_caption, feedbackformelements_rank
						FROM ' . $this->config['table_prefix'] . "feedbackformelements
						WHERE feedbackformelements_location = " . $sql_section_name . "
						ORDER BY feedbackformelements_rank";

                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $html_results = '';
                $html = $page->getTemplateSection($section_name . '_item_block');
                while (!$recordSet->EOF) {
                    $new_field_block = $html;
                    $fid = $recordSet->fields('feedbackformelements_id');
                    $f_rank = $recordSet->fields('feedbackformelements_rank');

                    // Get Caption from users selected language
                    if (!isset($_SESSION['users_lang'])) {
                        $caption = htmlentities($recordSet->fields('feedbackformelements_field_caption'));
                    } else {
                        $field_id = $misc->makeDbSafe($fid);
                        $sql2 = 'SELECT feedbackformelements_field_caption
								FROM ' . $this->config['lang_table_prefix'] . "feedbackformelements
								WHERE feedbackformelements_id = $field_id";
                        $recordSet2 = $ORconn->Execute($sql2);
                        if (is_bool($recordSet2)) {
                            $misc->logErrorAndDie($sql2);
                        }
                        $caption = htmlentities($recordSet2->fields('feedbackformelements_field_caption'));
                    }
                    $field_name = htmlentities($recordSet->fields('feedbackformelements_field_name'));

                    $new_field_block = str_replace('{field_rank}', $f_rank, $new_field_block);
                    $new_field_block = str_replace('{field_id}', $fid, $new_field_block);
                    $new_field_block = str_replace('{field_name}', $field_name, $new_field_block);
                    $new_field_block = str_replace('{field_caption}', $caption, $new_field_block);

                    if ($recordSet->fields('feedbackformelements_required') == 'Yes') {
                        $new_field_block = $page->cleanupTemplateBlock('required', $new_field_block);
                    } else {
                        $new_field_block = $page->removeTemplateBlock('required', $new_field_block);
                    }
                    $html_results .= $new_field_block;
                    $recordSet->MoveNext();
                }
                $page->replaceTemplateSection($section_name . '_item_block', $html_results);
            }
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            $display .= $page->returnPage();
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function editFormField(string $edit_form_field_name): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();

        $page = $this->newPageAdmin();
        $security = $login->verifyPriv('edit_lead_template');

        if ($security === true) {
            $page->loadPage($this->config['admin_template_path'] . '/lead_template_edit_field.html');
            $edit_form_field_name = $misc->makeDbSafe($edit_form_field_name);

            $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . 'feedbackformelements
					WHERE feedbackformelements_field_name = ' . $edit_form_field_name;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $id = $recordSet->fields('feedbackformelements_id');
            $field_type = $recordSet->fields('feedbackformelements_field_type');
            $field_name = $recordSet->fields('feedbackformelements_field_name');

            // Multi Lingual Support
            if (!isset($_SESSION['users_lang'])) {
                $field_caption = $recordSet->fields('feedbackformelements_field_caption');
                $default_text = $recordSet->fields('feedbackformelements_default_text');
                $field_elements = $recordSet->fields('feedbackformelements_field_elements');
            } else {
                $field_id = intval($recordSet->fields('feedbackformelements_id'));

                $lang_sql = 'SELECT feedbackformelements_field_caption,feedbackformelements_default_text,
								feedbackformelements_field_elements, feedbackformelements_search_label
							FROM ' . $this->config['lang_table_prefix'] . 'feedbackformelements
							WHERE feedbackformelements_id = ' . $field_id;
                $lang_recordSet = $ORconn->Execute($lang_sql);
                if (is_bool($lang_recordSet)) {
                    $misc->logErrorAndDie($lang_sql);
                }
                $field_caption = $lang_recordSet->fields('feedbackformelements_field_caption');
                $default_text = $lang_recordSet->fields('feedbackformelements_default_text');
                $field_elements = $lang_recordSet->fields('feedbackformelements_field_elements');
            }

            $rank = $recordSet->fields('feedbackformelements_rank');
            $required = $recordSet->fields('feedbackformelements_required');
            $location = $recordSet->fields('feedbackformelements_location');
            $tool_tip = $recordSet->fields('feedbackformelements_tool_tip');

            $page->replaceTagSafe('field_id', $id);
            $page->replaceTagSafe('field_name', $field_name);
            $page->replaceTagSafe('field_caption', $field_caption);
            $page->replaceTagSafe('field_type', $field_type);
            $page->replaceTagSafe('required', $required);


            $locations = [];
            $sections = explode(',', $this->config['template_lead_sections']);
            foreach ($sections as $section) {
                $locations[$section] = $section;
            }

            $html = $page->getTemplateSection('location_block');
            $html = $page->formOptions($locations, $location, $html);
            $page->replaceTemplateSection('location_block', $html);

            $page->replaceTagSafe('required_lower', strtolower($required));
            $page->replaceTagSafe('field_elements', $field_elements);
            $page->replaceTagSafe('default_text', $default_text);


            $page->replaceTagSafe('tool_tip', $tool_tip);
            $page->replaceTagSafe('rank', $rank);


            $page->replaceTag('application_status_text', '');
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
    }


    public function ajaxGetFormFieldInfo(): string
    {
        global $lang;


        $login = $this->newLogin();
        // Verify User is an Admin
        $security = $login->verifyPriv('edit_lead_template');
        $display = '';

        if ($security === true) {
            if (isset($_GET['edit_field']) && is_string($_GET['edit_field'])) {
                $display .= $this->editFormField($_GET['edit_field']);
            }
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function ajaxAddFormField(): string
    {
        global $lang;


        $login = $this->newLogin();
        // Verify User is an Admin
        $security = $login->verifyPriv('edit_lead_template');

        if ($security) {
            $display = $this->addFormTemplateField();
        } else {
            $display = '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function ajaxSaveFormFieldOrder(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        // Verify User is an Admin
        $security = $login->verifyPriv('edit_lead_template');

        if ($security === true) {
            if (isset($_POST['section']) && is_string($_POST['section']) && isset($_POST['fields']) && is_array($_POST['fields'])) {
                //Verify Section is valid
                $valid_section = explode(',', $this->config["template_lead_sections"]);
                $valid_section[] = 'misc';
                if (in_array($_POST['section'], $valid_section)) {
                    if ($_POST['section'] == "misc") {
                        $section = "";
                    } else {
                        $section = $_POST['section'];
                    }
                } else {
                    return json_encode(['error' => true, 'error_msg' => $lang['invalid_template_section']]) ?: '';
                }
                $sql_section = $misc->makeDbSafe($section);
                foreach ($_POST['fields'] as $rank => $field_name) {
                    //empty locations are skipped
                    if (!empty($field_name) && is_string($field_name)) {
                        $sql_field_name = $misc->makeDbSafe($field_name);
                        $sql_rank = intval($rank);

                        $sql = 'UPDATE ' . $this->config['table_prefix'] . "feedbackformelements
                        SET feedbackformelements_location = " . $sql_section . ",
                            feedbackformelements_rank = " . $sql_rank . "
                        WHERE feedbackformelements_field_name = " . $sql_field_name;
                        $recordSet = $ORconn->Execute($sql);
                        //echo $sql.'<br>';
                        if (is_bool($recordSet)) {
                            $misc->logErrorAndDie($sql);
                        }
                    }
                }
                return json_encode(['error' => false, 'status_msg' => $lang['admin_template_editor_field_order_set']]) ?: '';
            }
        }
        return json_encode(['error' => true, 'error_msg' => $lang['access_denied']]) ?: '';
    }

    public function ajaxInsertFormField(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_lead_template');

        if ($security === true) {
            if (
                isset($_POST['edit_field']) && !isset($_POST['lang_change'])
                && is_scalar($_POST['field_type'])
                && is_scalar($_POST['edit_field'])
                && is_scalar($_POST['field_caption'])
                && is_scalar($_POST['default_text'])
                && is_scalar($_POST['field_elements'])
                && is_scalar($_POST['required'])
                && is_scalar($_POST['location'])
                && is_scalar($_POST['tool_tip'])
                && is_scalar($_POST['rank'])
            ) {
                $field_type = $misc->makeDbSafe($_POST['field_type']);
                $_POST['edit_field'] = str_replace(' ', '_', $_POST['edit_field']);
                $field_name = $misc->makeDbSafe($_POST['edit_field']);
                $field_caption = $misc->makeDbSafe($_POST['field_caption']);
                $default_text = $misc->makeDbSafe($_POST['default_text']);
                $field_elements = $misc->makeDbSafe($_POST['field_elements']);
                $rank = intval($_POST['rank']);
                $required = $misc->makeDbSafe($_POST['required']);
                $location = $misc->makeDbSafe($_POST['location']);
                $tool_tip = $misc->makeDbSafe($_POST['tool_tip']);

                $id_rand = rand(0, 999999);

                $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'feedbackformelements
						(feedbackformelements_field_type, feedbackformelements_field_name, feedbackformelements_field_caption,
						feedbackformelements_default_text, feedbackformelements_field_elements, feedbackformelements_rank,
						feedbackformelements_required, feedbackformelements_location, feedbackformelements_tool_tip)
						VALUES (' . $field_type . ',' . $id_rand . ',' . $field_caption . ',' . $default_text . ',' . $field_elements . ', ' . $rank . ', ' . $required . ', ' . $location . ', ' . $tool_tip . ')';
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                // Now we need to get the field ID
                $sql = 'SELECT feedbackformelements_id 
						FROM ' . $this->config['table_prefix'] . 'feedbackformelements
						WHERE feedbackformelements_field_name = ' . $id_rand;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $feedbackformelements_id = $recordSet->fields('feedbackformelements_id');
                // Set Real Name
                $sql = 'UPDATE ' . $this->config['table_prefix'] . 'feedbackformelements 
						SET feedbackformelements_field_name = ' . $field_name . '
						WHERE feedbackformelements_field_name = ' . $id_rand;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                // We should now add a blank field for each lead that already exist.
                $sql = 'SELECT feedbackdb_id, userdb_id 
						FROM ' . $this->config['table_prefix'] . 'feedbackdb';
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $id = [];
                $user = [];
                while (!$recordSet->EOF) {
                    $id[] = $recordSet->fields('feedbackdb_id');
                    $user[] = $recordSet->fields('userdb_id');
                    $recordSet->MoveNext();
                }
                $count = count($id);
                $x = 0;
                while ($x < $count) {
                    $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'feedbackdbelements
							(feedbackdbelements_field_name, feedbackdb_id,userdb_id,feedbackdbelements_field_value)
							VALUES (' . $field_name . ',' . $id[$x] . ',' . $user[$x] . ', \'\')';
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $x++;
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'field_id' => $feedbackformelements_id]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['access_denied']]) ?: '';
    }

    public function ajaxUpdateLeadField(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $display = '';
        $security = $login->verifyPriv('edit_lead_template');
        if ($security === true) {
            if (
                isset($_POST['update_id']) && !isset($_POST['lang_change'])
                && is_scalar($_POST['field_type'])
                && is_scalar($_POST['edit_field'])
                && is_scalar($_POST['field_caption'])
                && is_scalar($_POST['default_text'])
                && is_scalar($_POST['field_elements'])
                && is_scalar($_POST['required'])
                && is_scalar($_POST['location'])
                && is_scalar($_POST['tool_tip'])
                && is_scalar($_POST['rank'])
                && is_scalar($_POST['old_field_name'])
            ) {
                $id = intval($_POST['update_id']);
                $_POST['old_field_name'] = str_replace(' ', '_', $_POST['old_field_name']);
                $_POST['edit_field'] = str_replace(' ', '_', $_POST['edit_field']);
                $field_name = $misc->makeDbSafe($_POST['edit_field']);
                $old_field_name = $misc->makeDbSafe($_POST['old_field_name']);
                $required = $misc->makeDbSafe($_POST['required']);
                $update_field_name = false;

                if ($old_field_name != $field_name) {
                    $update_field_name = true;
                }

                $field_type = $misc->makeDbSafe($_POST['field_type']);
                $field_caption = $misc->makeDbSafe($_POST['field_caption']);
                $default_text = $misc->makeDbSafe($_POST['default_text']);
                $field_elements = $misc->makeDbSafe($_POST['field_elements']);
                $rank = $misc->makeDbSafe($_POST['rank']);
                $location = $misc->makeDbSafe($_POST['location']);
                $tool_tip = $misc->makeDbSafe($_POST['tool_tip']);

                $sql = 'UPDATE ' . $this->config['table_prefix'] . 'feedbackformelements
							SET feedbackformelements_field_type = ' . $field_type . ', feedbackformelements_field_name = ' . $field_name . ',
							feedbackformelements_rank = ' . $rank . ', feedbackformelements_required = ' . $required . ',
							feedbackformelements_location = ' . $location . ', feedbackformelements_tool_tip = ' . $tool_tip . '
							WHERE feedbackformelements_id = ' . $id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                // Update Current language
                if (!isset($_SESSION['users_lang'])) {
                    $lang_sql = 'UPDATE  ' . $this->config['table_prefix'] . 'feedbackformelements
									SET feedbackformelements_field_caption = ' . $field_caption . ', feedbackformelements_field_elements = ' . $field_elements . ', feedbackformelements_default_text = ' . $default_text . ',
									feedbackformelements_tool_tip = ' . $tool_tip . '
									WHERE feedbackformelements_id = ' . $id;
                } else {
                    $lang_sql = 'DELETE FROM  ' . $this->config['lang_table_prefix'] . 'feedbackformelements
									WHERE feedbackformelements_id = ' . $id;
                    $lang_recordSet = $ORconn->Execute($lang_sql);
                    if (!$lang_recordSet) {
                        $misc->logErrorAndDie($lang_sql);
                    }
                    $lang_sql = 'INSERT INTO ' . $this->config['lang_table_prefix'] . 'feedbackformelements
									(feedbackformelements_id, feedbackformelements_field_caption, feedbackformelements_default_text,
									feedbackformelements_field_elements, feedbackformelements_tool_tip)
									VALUES (' . $id . ', ' . $field_caption . ',' . $default_text . ',' . $field_elements . ',' . $tool_tip . ')';
                }
                $lang_recordSet = $ORconn->Execute($lang_sql);
                if (!$lang_recordSet) {
                    $misc->logErrorAndDie($lang_sql);
                }
                // Check if field name changed, if it as update all feedbackdbelement tables
                if ($update_field_name) {
                    $lang_sql = 'UPDATE  ' . $this->config['table_prefix'] . 'feedbackdbelements
									SET feedbackdbelements_field_name = ' . $field_name . '
									WHERE feedbackdbelements_field_name = ' . $old_field_name;
                    $lang_recordSet = $ORconn->Execute($lang_sql);
                    if (!$lang_recordSet) {
                        $misc->logErrorAndDie($lang_sql);
                    }
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'field_id' => $id]) ?: '';
            }
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => $lang['access_denied']]) ?: '';
        }
        return $display;
    }

    public function addFormTemplateField(): string
    {
        global $lang;


        $page = $this->newPageAdmin();

        $login = $this->newLogin();

        $page->loadPage($this->config['admin_template_path'] . '/lead_template_add_field.html');

        $security = $login->verifyPriv('edit_lead_template');

        if ($security === true) {
            $sections = explode(',', $this->config['template_lead_sections']);
            $html = $page->getTemplateSection('location_block');
            $html = $page->formOptions($sections, "", $html);
            $page->replaceTemplateSection('location_block', $html);


            $page->replaceTag('application_status_text', '');
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        }
        return '<div class="error_text">' . $lang['access_denied'] . '</div>';
    }

    /**
     * @return (bool|int)[]|null
     *
     * @psalm-return array{error: bool, lead_id?: int}|null
     */
    public function addLeadCreateLead()
    {
        global $ORconn;

        $misc = $this->newMisc();
        $listing_pages = $this->newListingPages();
        $lead_functions = $this->newLeadFunctions();

        if (isset($_POST['listing_id'])) {
            $listingID = intval($_POST['listing_id']);
            $memberID = intval($_POST['member_id']);
            $agent_id = $listing_pages->getListingAgentValue('userdb_id', $listingID);
        } else {
            $listingID = 0;
            $agent_id = intval($_POST['agent_id']);
            $memberID = intval($_POST['member_id']);
        }

        $notes = is_string($_POST['notes']) ? $_POST['notes'] : '';
        if ($agent_id > 0 && $memberID > 0) {
            //START - Determine AgentID Based on listingID
            //Check to see if agent's notifications should be redirected to the floor agent.
            $sql = 'SELECT userdb_send_notifications_to_floor FROM ' . $this->config['table_prefix'] . 'userdb WHERE userdb_id = ' . $agent_id;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $userdb_send_notifications_to_floor = $recordSet->fields('userdb_send_notifications_to_floor');
            if ($userdb_send_notifications_to_floor == 1) {
                //Get Floor Agent ID
                $sql = 'SELECT controlpanel_floor_agent, controlpanel_floor_agent_last FROM ' . $this->config['table_prefix_no_lang'] . 'controlpanel';
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $controlpanel_floor_agent = $recordSet->fields('controlpanel_floor_agent');
                $controlpanel_floor_agent_last = $recordSet->fields('controlpanel_floor_agent_last');
                $floor_agents = explode(',', $controlpanel_floor_agent);
                if (count($floor_agents) > 1) {
                    $update_floor = false;
                    //Determine Last Floor Agent
                    if ($controlpanel_floor_agent_last > 1) {
                        $use_next = false;
                        $found_agent = false;
                        foreach ($floor_agents as $floor_id) {
                            if (!$use_next) {
                                if ($floor_id == $controlpanel_floor_agent_last) {
                                    $use_next = true;
                                }
                            } else {
                                $new_agent_id = $floor_id;
                                if ($new_agent_id > 1) {
                                    $found_agent = true;
                                    $update_floor = true;
                                    $agent_id = $new_agent_id;
                                }
                            }
                        }
                        if (!$found_agent) {
                            //No Last Agent set so use the first in list
                            $new_agent_id = $floor_agents[0];
                            if ($new_agent_id > 1) {
                                $update_floor = true;
                                $agent_id = $new_agent_id;
                            }
                        }
                    } else {
                        //No Last Agent set so use the first in list
                        $new_agent_id = $floor_agents[0];
                        if ($new_agent_id > 1) {
                            $update_floor = true;
                            $agent_id = $new_agent_id;
                        }
                    }
                    if ($update_floor) {
                        $sql = 'UPDATE ' . $this->config['table_prefix_no_lang'] . 'controlpanel SET controlpanel_floor_agent_last = ' . $agent_id;
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            $misc->logErrorAndDie($sql);
                        }
                    }
                } else {
                    $new_agent_id = $floor_agents[0];
                    if ($new_agent_id > 1) {
                        $agent_id = $new_agent_id;
                    }
                }
            }
            $agent_id = (int)$agent_id;
            //END Agent ID Detection
            $sql_notes = $misc->makeDbSafe($notes);
            $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "feedbackdb (feedbackdb_notes, userdb_id, listingdb_id, feedbackdb_creation_date, feedbackdb_last_modified, feedbackdb_status, feedbackdb_priority,feedbackdb_member_userdb_id )
			                		VALUES ($sql_notes, $agent_id, $listingID, " . $ORconn->DBTimeStamp(time()) . ',' . $ORconn->DBTimeStamp(time()) . ", 1, 'Normal',$memberID) ";
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $new_feedback_id = $ORconn->Insert_ID(); // this is the new feedback's ID number
            // now that's taken care of, it's time to insert all the rest
            // of the variables into the database
            $message = $lead_functions->updateFeedbackData($new_feedback_id, $agent_id);
            //$sql_user = $misc->makeDbSafe($agent_id);
            if ($message == 'success') {
                //get the Agent's full name & email
                $sql = 'SELECT userdb_user_first_name, userdb_user_last_name, userdb_emailaddress
										FROM ' . $this->config['table_prefix'] . "userdb
										WHERE userdb_id = $agent_id";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $user_name = $recordSet->fields('userdb_user_first_name') . ' ' . $recordSet->fields('userdb_user_last_name');
                // Report that Feedback has been sent to the AGENT
                $misc->logAction("Created feedback $new_feedback_id for $user_name");
                $lead_functions->sendAgentFeedbackNotice($new_feedback_id);
                if (isset($_POST['sendmember_email']) && $_POST['sendmember_email'] == 1) {
                    $lead_functions->sendUserFeedbackNotice($new_feedback_id);
                }

                $hooks = $this->newHooks();
                $hooks->load('afterLeadCreate', $new_feedback_id);
                return ['error' => false, 'lead_id' => $new_feedback_id];
            } else {
                return ['error' => true];
                //$output .= '<p>There\'s been a problem -- please contact the site administrator</p>';
            } // end else
        }
        return ['error' => true];
    }

    /**
     * @return null|string
     */
    public function deleteLeadField()
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_lead_template');
        if ($security === true) {
            if (isset($_GET['delete_field']) && is_string($_GET['delete_field']) && !isset($_POST['lang_change'])) {
                $field_name = $misc->makeDbSafe($_GET['delete_field']);
                $sql = 'SELECT feedbackformelements_id FROM ' . $this->config['table_prefix'] . 'feedbackformelements
						WHERE feedbackformelements_field_name = ' . $field_name;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                // Delete All Translation for this field.
                $configured_langs = explode(',', $this->config['configured_langs']);
                while (!$recordSet->EOF) {
                    $feedbackformelements_id = $recordSet->fields('feedbackformelements_id');
                    foreach ($configured_langs as $configured_lang) {
                        $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . $configured_lang . '_feedbackformelements
								WHERE feedbackformelements_id = ' . $feedbackformelements_id;
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            $misc->logErrorAndDie($sql);
                        }
                    }
                }
                // Cleanup any feedbackdbelements entries from this field.
                foreach ($configured_langs as $configured_lang) {
                    $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . $configured_lang . '_feedbackdbelements
							WHERE feedbackdbelements_field_name = ' . $field_name;
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                }
            }
        }
        return '<div class="error_text">' . $lang['access_denied'] . '</div>';
    }
}
