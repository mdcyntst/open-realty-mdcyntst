<?php

declare(strict_types=1);

namespace OpenRealty;

use Exception;
use Google\Client;
use Google\Service\Oauth2;
use Google\Service\Oauth2\Userinfo;
use JetBrains\PhpStorm\NoReturn;

/**
 * Class Handles all login activity
 */
class Login extends BaseClass
{
    /**
     * Gets a Client for Google Auth
     *
     * @param string $redirectUri RedirectURL for Google Auth.
     *
     * @return Client|boolean
     */
    private function getGoogleClient(string $redirectUri = ''): Client|bool
    {
        if (strlen($this->config['google_client_id']) > 0 && strlen($this->config['google_client_secret']) > 0) {
            // create Client Request to access Google API
            $client = new Client();
            $client->setClientId($this->config['google_client_id']);
            $client->setClientSecret($this->config['google_client_secret']);
            $client->setRedirectUri($redirectUri);
            $client->addScope("email");
            $client->addScope("profile");
            return $client;
        }
        return false;
    }

    /**
     * Clears the RememberMe Cookies
     */
    public function clearRememberMeCookie(): void
    {
        global $ORconn;
        $misc = $this->newMisc();
        $parse = parse_url($this->config['baseurl']);
        $domain = '';
        if (isset($parse['host'])) {
            $domain = $parse['host'];
        }

        if (isset($parse['scheme']) && $parse['scheme'] == 'https') {
            $https = true;
        } else {
            $https = false;
        }

        if (isset($_COOKIE['user_id']) && isset($_COOKIE['cookie_selector'])) {
            $sql_user_id = $misc->makeDbSafe($_COOKIE['user_id']);
            $sql_selector = $misc->makeDbSafe($_COOKIE['cookie_selector']);
            $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . 'auth_tokens
            WHERE userdb_id = ' . $sql_user_id . ' AND  selector = ' . $sql_selector;

            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
        }

        $misc->setCookie('user_id', '', time() - 3600, '/', $domain, $https, true);
        $misc->setCookie('cookie_validator', '', time() - 3600, '/', $domain, $https, true);
        $misc->setCookie('cookie_selector', '', time() - 3600, '/', $domain, $https, true);
    }

    /**
     * Set all the session vars for a user, by either username of userid
     *
     * @param string|null  $username Username to create session vars for.
     * @param integer|null $userid   UserID to create session var for.
     *
     * @return boolean Returns true if session vars created, else false.
     */
    public function setSessionVars(?string $username, ?int $userid = null): bool
    {
        global $ORconn;

        $misc = $this->newMisc();
        if (isset($username) && $username !== '') {
            $sql_username = $misc->makeDbSafe($username);
            $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . 'userdb 
                WHERE  userdb_user_name=' . $sql_username;
        } elseif (isset($userid) && $userid > 0) {
            $sql_userid = $misc->makeDbSafe($userid);
            $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . 'userdb 
                WHERE  userdb_id=' . $sql_userid;
        } else {
            return false;
        }


        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }

        $_SESSION['userID'] = (int)$recordSet->fields('userdb_id');
        $_SESSION['username'] = (string)$recordSet->fields('userdb_user_name');
        $_SESSION['admin_privs'] = (string)$recordSet->fields('userdb_is_admin');
        $_SESSION['active'] = (string)$recordSet->fields('userdb_active');
        $_SESSION['isAgent'] = (string)$recordSet->fields('userdb_is_agent');
        $_SESSION['featureListings'] = (string)$recordSet->fields('userdb_can_feature_listings');
        $_SESSION['viewLogs'] = (string)$recordSet->fields('userdb_can_view_logs');
        $_SESSION['moderator'] = (string)$recordSet->fields('userdb_can_moderate');
        $_SESSION['editpages'] = (string)$recordSet->fields('userdb_can_edit_pages');
        $_SESSION['havevtours'] = (string)$recordSet->fields('userdb_can_have_vtours');
        $_SESSION['haveuserfiles'] = (string)$recordSet->fields('userdb_can_have_user_files');
        $_SESSION['havefiles'] = (string)$recordSet->fields('userdb_can_have_files');
        $_SESSION['is_member'] = 'yes';

        // New Permissions with OR 2.1
        $_SESSION['edit_site_config'] = (string)$recordSet->fields('userdb_can_edit_site_config');
        $_SESSION['edit_member_template'] = (string)$recordSet->fields('userdb_can_edit_member_template');
        $_SESSION['edit_agent_template'] = (string)$recordSet->fields('userdb_can_edit_agent_template');
        $_SESSION['edit_listing_template'] = (string)$recordSet->fields('userdb_can_edit_listing_template');
        $_SESSION['export_listings'] = (string)$recordSet->fields('userdb_can_export_listings');
        $_SESSION['edit_all_listings'] = (string)$recordSet->fields('userdb_can_edit_all_listings');
        $_SESSION['edit_all_users'] = (string)$recordSet->fields('userdb_can_edit_all_users');
        $_SESSION['edit_property_classes'] = (string)$recordSet->fields('userdb_can_edit_property_classes');
        $_SESSION['edit_expiration'] = (string)$recordSet->fields('userdb_can_edit_expiration');
        $_SESSION['blog_user_type'] = (string)$recordSet->fields('userdb_blog_user_type');
        $_SESSION['can_manage_addons'] = (string)$recordSet->fields('userdb_can_manage_addons');
        $_SESSION['edit_lead_template'] = (string)$recordSet->fields('userdb_can_edit_lead_template');
        $_SESSION['edit_all_leads'] = (string)$recordSet->fields('userdb_can_edit_all_leads');

        return true;
    }

    /**
     * Create cookies to remember user login.
     */
    public function createRememberMeCookie(): void
    {
        global $ORconn;

        $misc = $this->newMisc();
        $userIdString = (string)$_SESSION['userID'];
        $parse = parse_url($this->config['baseurl']);
        $domain = '';
        if (isset($parse['host'])) {
            $domain = $parse['host'];
        }
        if (isset($parse['scheme']) && $parse['scheme'] == 'https') {
            $https = true;
        } else {
            $https = false;
        }
        try {
            $validator = bin2hex(random_bytes(32));
            $selector = bin2hex(random_bytes(32));
        } catch (Exception $e) {
            $misc->logErrorAndDie($e->getMessage());
        }

        $arr_cookie_options = [
            'expires' => time() + 60 * 60 * 24 * 30,
            'path' => '/',
            'domain' => $domain, // leading dot for compatibility or use subdomain
            'secure' => $https,     // or false
            'httponly' => true,    // or false
            'samesite' => 'Lax',
        ];
        setcookie('user_id', $userIdString, $arr_cookie_options);
        setcookie('cookie_validator', $validator, $arr_cookie_options);
        setcookie('cookie_selector', $selector, $arr_cookie_options);


        $sql_userID = $misc->makeDbSafe($userIdString);
        $sql_selector = $misc->makeDbSafe($selector);
        $sql_hash = $misc->makeDbSafe(password_hash($validator, PASSWORD_DEFAULT));
        $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . 'auth_tokens
            SET 
                expires = DATE_ADD(NOW(),INTERVAL 30 DAY),
                userdb_id = ' . $sql_userID . ',
                selector = ' . $sql_selector . ',
                validator = ' . $sql_hash;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
    }


    /**
     * Check if User is Logged In
     *
     * @return boolean
     */
    public function checkLogin(): bool
    {
        global $ORconn;

        $misc = $this->newMisc();
        /* User already has a session */
        if (isset($_SESSION['username'])) {
            return true;
        } elseif (isset($_COOKIE['user_id']) && isset($_COOKIE['cookie_validator']) && isset($_COOKIE['cookie_selector'])) {
            $sql_selector = $misc->makeDbSafe($_COOKIE['cookie_selector']);
            $sql_user_id = $misc->makeDbSafe($_COOKIE['user_id']);
            $sql = 'SELECT validator  FROM ' . $this->config['table_prefix_no_lang'] . 'auth_tokens WHERE selector = ' . $sql_selector . ' AND userdb_id = ' . $sql_user_id . ' AND expires > NOW()';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool(($recordSet))) {
                $misc->logErrorAndDie($sql);
            }
            //print_r($recordSet);
            if ($recordSet->RecordCount() < 1) {
                //Clear Cookie as it was invalid
                $this->clearRememberMeCookie();
                return false;
            } elseif (password_verify($_COOKIE['cookie_validator'], (string)$recordSet->fields('validator'))) {
                //Hash is valid
                if (!$this->setSessionVars('', (int)$_COOKIE['user_id'])) {
                    return false;
                }
                return true;
            } else {
                //Hash was invalid
                $this->clearRememberMeCookie();
                return false;
            }
        }

        /* User not logged in */
        return false;
    }

    /**
     * Verify a member by email.
     *
     * @param string $email User email to check for.
     *
     * @return string
     */
    public function verifyMemberEmail(string $email): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        header('Content-type: application/json');

        //Make sure this from our site.
        if (!$misc->refererCheck()) {
            return json_encode(['error' => true, 'error_code' => 1, 'error_msg' => 'Form Security Violation1']) ?: '';
        } else {
            //Make Sure Email is Valid
            $valid = $misc->validateEmail($email);
            if (!$valid) {
                return json_encode(['error' => true, 'error_code' => 4, 'error_msg' => 'The Email address you entered is invalid.']) ?: '';
            }
            $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . 'userdb 
							WHERE  userdb_emailaddress = ' . $misc->makeDbSafe($email);
            $recordSet = $ORconn->Execute($sql);
            if (is_bool(($recordSet))) {
                $misc->logErrorAndDie($sql);
            }
            //print_r($recordSet);
            if ($recordSet->RecordCount() < 1) {
                return json_encode(['error' => true, 'error_code' => 2, 'error_msg' => 'Not A Member']) ?: '';
            }
            if ($recordSet->fields('userdb_active') == 'yes') {
                return json_encode(['error' => false]) ?: '';
            } else {
                return json_encode(['error' => true, 'error_code' => 3, 'error_msg' => 'Member Account is Disabled']) ?: '';
            }
        }
    }

    /**
     * Ajax call to check member login.
     *
     * @param string $email Email Address.
     * @param string $pass  User Password.
     *
     * @return string
     */
    public function ajaxCheckMemberLogin(string $email, string $pass): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        header('Content-type: application/json');

        //Make sure this from our site.
        if (!$misc->refererCheck()) {
            return json_encode(['error' => true, 'error_code' => 1, 'error_msg' => 'Form Security Violation2']) ?: '';
        }
        //Make Sure Email is Valid
        $valid = $misc->validateEmail($email);
        if (!$valid) {
            return json_encode(['error' => true, 'error_code' => 4, 'error_msg' => 'The Email address you entered is invalid.']) ?: '';
        }
        $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . 'userdb WHERE  userdb_emailaddress = ' . $misc->makeDbSafe($email);
        $recordSet = $ORconn->Execute($sql);
        if (is_bool(($recordSet))) {
            $misc->logErrorAndDie($sql);
        }
        //print_r($recordSet);
        if ($recordSet->RecordCount() < 1) {
            return json_encode(['error' => true, 'error_code' => 2, 'error_msg' => 'Not A Member']) ?: '';
        }
        if ($recordSet->fields('userdb_active') == 'yes') {
            $_POST['user_name'] = (string)$recordSet->fields('userdb_user_name');
            $_POST['user_pass'] = trim($pass);
            /** @var array */
            $login_result = $this->loginCheck('Member', 'v2');
            $login_status = (bool)$login_result[0];
            $error_msg = (string)$login_result[1];
            if (!$login_status) {
                return json_encode(['error' => true, 'error_code' => 5, 'error_msg' => $error_msg]) ?: '';
            } else {
                //Return Users First and Last Name

                $user = $this->newUser();
                $fname = $user->getUserSingleItem('userdb_user_first_name', $_SESSION['userID']);
                $lname = $user->getUserSingleItem('userdb_user_last_name', $_SESSION['userID']);
                return json_encode(['error' => false, 'fname' => $fname, 'lname' => $lname]) ?: '';
            }
        }
        return json_encode(['error' => false, 'error_code' => 6, 'error_msg' => 'User Inactive']) ?: '';
    }


    /**
     * Summary of verifyGoogleUser
     *
     * @param Client $gclient           Google Client.
     * @param string $priv_level_needed Privilate Needed.
     *
     * @return boolean
     * @see getGoogleClient
     */
    public function verifyGoogleUser(Client $gclient, string $priv_level_needed): bool
    {
        $misc = $this->newMisc();
        if (isset($_SESSION['username']) && $_SESSION['username']) {
            return true;
        } else {
            //Create Object of Google Service OAuth 2 class
            $google_service = new Oauth2($gclient);
            /**
             * @var Userinfo
             * @psalm-suppress MixedMethodCall
             */
            $google_account_info = $google_service->userinfo->get();
            $email = $google_account_info->email;
            $familyName = $google_account_info->familyName;
            $givenName = $google_account_info->givenName;

            //Check if email
            /** @var array */
            $user_result = $this->confirmUserByEmail($email);
            $confirm_err = (bool)$user_result[0];

            if ($confirm_err !== true) {
                $confirm_result = (string)$user_result[1];
                $this->setSessionVars($confirm_result);
                return true;
            } else {
                $confirm_result = (int)$user_result[1];
                //User was not found. Create it if signup is supported
                if ($confirm_result === 1) {
                    if ($priv_level_needed == 'Member') {
                        $type = 'member';
                    } else {
                        $type = 'agent';
                    }
                    if ($this->config['allow_' . $type . '_signup']) {
                        $set_active = 'no';
                        if (!$this->config['moderate_' . $type . 's']) {
                            if ($type == 'agent') {
                                if ($this->config['agent_default_active']) {
                                    $set_active = 'yes';
                                }
                            } else {
                                $set_active = 'yes';
                            }
                        }

                        $user_manager = $this->newUserManagement();
                        $user_manager->createBaseUser($type, $email, $email, $misc->generatePassword(), $set_active, $givenName, $familyName);
                        /**
                         * @var array $user_result
                         */
                        $user_result = $this->confirmUserByEmail($email);
                        $confirm_err = (bool)$user_result[0];

                        if ($confirm_err !== true) {
                            $confirm_result = (string)$user_result[1];
                            $this->setSessionVars($confirm_result);
                            return true;
                        }
                    }
                }
            }

            /* Variables are incorrect, user not logged in */
            unset($_SESSION['access_token']);
            unset($_SESSION['username']);
            return false;
        }
    }

    /**
     * loginCheck - Verify the username and password passed by session or post and then verify privilage level.
     *
     * @param string         $priv_level_needed Permission level to check for.
     * @param boolean|string $internal          True, False, or v2.
     * @param boolean        $skip_csrf         Skip CSRF Check.
     *
     * @return boolean|array|string Returns an array with the login status and error message.
     * @psalm-return ($internal is false ? string|true : ($internal is string ? list{boolean, string} : boolean))
     *
     */
    public function loginCheck(string $priv_level_needed, bool|string $internal = false, bool $skip_csrf = false): bool|array|string
    {
        global $lang;

        $misc = $this->newMisc();
        $display = '';
        $new_login = true;
        if (isset($_SESSION['username'])) {
            $new_login = false;
        }
        if ($priv_level_needed == 'Member') {
            $redirectUri = $this->config['baseurl'] . '/';
        } else {
            $redirectUri = $this->config['baseurl'] . '/admin/';
        }
        $gclient = $this->getGoogleClient($redirectUri);
        $google_auth_success = false;
        if (!is_bool($gclient)) {
            //Handle Google Auth Redirect
            if (isset($_GET["code"]) && is_string($_GET["code"])) {
                unset($_SESSION['access_token']);
                unset($_SESSION['username']);
                //It will Attempt to exchange a code for an valid authentication token.
                $token = $gclient->fetchAccessTokenWithAuthCode($_GET["code"]);
                //This condition will check there is any error occur during geting authentication token. If there is no any error occur then it will execute if block of code/
                if (!isset($token['error']) && isset($token['access_token'])) {
                    $access_token = (string)$token['access_token'];
                    //Set the access token used for requests
                    $gclient->setAccessToken($access_token);
                    //Store "access_token" value in $_SESSION variable for future use.
                    $_SESSION['access_token'] = $access_token;
                    $google_auth_success = $this->verifyGoogleUser($gclient, $priv_level_needed);
                    if (!$google_auth_success) {
                        if ($internal === false) {
                            $display .= $this->displayLogin($priv_level_needed, $lang['google_auth_invalid']);
                            return $display;
                        } elseif ($internal === 'v2') {
                            return [false, $lang['google_auth_invalid']];
                        } else {
                            return false;
                        }
                    }
                }
            } elseif (isset($_SESSION['access_token']) && is_string($_SESSION['access_token'])) {
                $gclient->setAccessToken($_SESSION['access_token']);
                $google_auth_success = $this->verifyGoogleUser($gclient, $priv_level_needed);
                if (!$google_auth_success) {
                    if ($internal === false) {
                        $display .= $this->displayLogin($priv_level_needed, $lang['google_auth_invalid']);
                        return $display;
                    } elseif ($internal === 'v2') {
                        return [false, $lang['google_auth_invalid']];
                    } else {
                        return false;
                    }
                }
            }
        }

        if ($google_auth_success) {
            $checked = true;
        } else {
            $checked = $this->checkLogin();
        }
        if (!$checked and !isset($_POST['user_name'])) {
            if ($internal === false) {
                return $this->displayLogin($priv_level_needed);
            } elseif ($internal === 'v2') {
                return [false, ''];
            } else {
                return false;
            }
        } elseif (isset($_POST['user_name']) && !$checked) {
            if (php_sapi_name() != 'cli' && !$skip_csrf) {
                if (!isset($_POST['token']) || is_string($_POST['token']) && !$misc->validateCsrfToken($_POST['token'])) {
                    if ($internal === false) {
                        $display .= $this->displayLogin($priv_level_needed, $lang['invalid_csrf_token']);
                        return $display;
                    } elseif ($internal === 'v2') {
                        return [false, $lang['invalid_csrf_token']];
                    } else {
                        return false;
                    }
                }
            }
            if (!is_string($_POST['user_name']) || !$_POST['user_pass'] || !is_string($_POST['user_pass'])) {
                if ($internal === false) {
                    $display .= $this->displayLogin($priv_level_needed, $lang['required_field_not_filled']);
                    return $display;
                } elseif ($internal === 'v2') {
                    return [false, $lang['required_field_not_filled']];
                } else {
                    return false;
                }
            }
            /* Spruce up username, check length */
            $_POST['user_name'] = trim($_POST['user_name']);
            if (strlen($_POST['user_name']) > 30) {
                if ($internal === false) {
                    $display .= $this->displayLogin($priv_level_needed, $lang['username_excessive_length']);
                    return $display;
                } elseif ($internal === 'v2') {
                    return [false, $lang['username_excessive_length']];
                } else {
                    return false;
                }
            }
            /* Checks that username is in database and password is correct */
            $result = $this->confirmUser($_POST['user_name'], $_POST['user_pass']);
            /* Check error codes */
            if ($result == 1) {
                if ($internal === false) {
                    $display .= $this->displayLogin($priv_level_needed, $lang['incorrect_username_password']);
                    return $display;
                } elseif ($internal === 'v2') {
                    return [false, $lang['incorrect_username_password']];
                } else {
                    return false;
                }
            } elseif ($result == 2) {
                if ($internal === false) {
                    $display .= $this->displayLogin($priv_level_needed, $lang['incorrect_username_password']);
                    return $display;
                } elseif ($internal === 'v2') {
                    return [false, $lang['incorrect_username_password']];
                } else {
                    return false;
                }
            } elseif ($result == 3) {
                if ($internal === false) {
                    $display .= $this->displayLogin($priv_level_needed, $lang['inactive_user']);
                    return $display;
                } elseif ($internal === 'v2') {
                    return [false, $lang['inactive_user']];
                } else {
                    return false;
                }
            }
        }
        if (isset($_POST['user_name']) && is_string($_POST['user_name'])) {
            if (!$this->setSessionVars($_POST['user_name'])) {
                if ($internal === false) {
                    $display .= $this->displayLogin($priv_level_needed, $lang['access_denied']);
                    return $display;
                } elseif ($internal === 'v2') {
                    return [false, $lang['access_denied']];
                } else {
                    return false;
                }
            }

            if (isset($_POST['remember'])) {
                $this->createRememberMeCookie();
                // setcookie('cookname', $_SESSION['username'], time() + 60 * 60 * 24 * 100, '/', '', false, true);
                // setcookie('cookpass', $_SESSION['userpassword'], time() + 60 * 60 * 24 * 100, '/', '', false, true);
            }
            //call the after login plugin function
            if ($new_login) {
                $hooks = $this->newHooks();
                $hooks->load('afterUserLogin', $_SESSION['userID']);
            }
        }
        if (!$this->verifyPriv($priv_level_needed)) {
            if ($internal === false) {
                $display .= $this->displayLogin($priv_level_needed, $lang['access_denied']);
                return $display;
            } elseif ($internal === 'v2') {
                return [false, $lang['access_denied']];
            } else {
                return false;
            }
        } else {
            if ($internal === false) {
                return true;
            } elseif ($internal === 'v2') {
                return [true, ''];
            } else {
                return true;
            }
        }
    }

    /**
     * Display Login Form
     *
     * @param string      $priv_level_needed Privilage Level Needed.
     * @param string|null $error_msg         If you are reshowing login, after a failed login attempt include error
     *                                       message to display.
     *
     * @return string
     */
    public function displayLogin(string $priv_level_needed, ?string $error_msg = null): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $display = '';
        if (isset($error_msg) && $error_msg != '') {
            $login_status = false;
        } else {
            /** @var array */
            $login_result = $this->loginCheck('Member', 'v2');
            $login_status = (bool)$login_result[0];
            $error_msg = (string)$login_result[1];
        }

        if (isset($_SERVER['HTTP_REFERER']) && isset($_GET['action']) && $_GET['action'] == 'member_login') {
            if (str_starts_with($_SERVER['HTTP_REFERER'], $this->config['baseurl'])) {
                if (!isset($_SESSION['login_referer'])) {
                    $_SESSION['login_referer'] = $_SERVER['HTTP_REFERER'];
                }
            }
        }
        if ($login_status) {
            if ($_SESSION['login_referer']) {
                $referer_url = (string)$_SESSION['login_referer'];
                unset($_SESSION['login_referer']);
            } else {
                $referer_url = $this->config['baseurl'] . '/index.php';
            }
            header('Location: ' . $referer_url);
        } else {
            if ($priv_level_needed == 'Member') {
                $page = $this->newPageUser();
                $page->loadPage($this->config['template_path'] . '/login.html');
                $redirectUri = $this->config['baseurl'] . '/';
            } else {
                $page = $this->newPageAdmin();
                $page->loadPage($this->config['admin_template_path'] . '/login.html');
                $redirectUri = $this->config['baseurl'] . '/admin/';
            }

            $gclient = $this->getGoogleClient($redirectUri);
            if (!is_bool($gclient)) {
                $page->page = $page->cleanupTemplateBlock('google_auth', $page->page);
                $page->replaceTag('google_auth_url', $gclient->createAuthUrl());
            } else {
                $page->page = $page->removeTemplateBlock('google_auth', $page->page);
            }

            if ($error_msg != '') {
                $page->page = $page->cleanupTemplateBlock('login_error_msg', $page->page);
                $page->replaceTag('login_error_msg', $error_msg);
            } else {
                $page->page = $page->removeTemplateBlock('login_error_msg', $page->page);
            }

            $display .= $page->returnPage();

            // Run the cleanup for the forgot password table
            $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . 'forgot WHERE forgot_time < NOW() - INTERVAL 1 DAY';
            $recordSet = $ORconn->execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
        }
        return $display;
    }

    /**
     * Checks Session Vars to determine if user has a privilege
     *
     * @param string $priv_level_needed Privilege level needed.
     *
     * @return boolean
     */
    public function verifyPriv(string $priv_level_needed): bool
    {
        if (!isset($_SESSION['is_member'])) {
            return false;
        }
        switch ($priv_level_needed) {
            case 'Agent':
                if ($_SESSION['isAgent'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'Admin':
                if ($_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'canViewLogs':
                if ($_SESSION['viewLogs'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'CanEditExpiration':
                if ($_SESSION['edit_expiration'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'editpages':
                if ($_SESSION['editpages'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'havevtours':
                if ($_SESSION['havevtours'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'havefiles':
                if ($_SESSION['havefiles'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'Member':
                if ($_SESSION['is_member'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_site_config':
                if ($_SESSION['edit_site_config'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_member_template':
                if ($_SESSION['edit_member_template'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_lead_template':
                if ($_SESSION['edit_lead_template'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_agent_template':
                if ($_SESSION['edit_agent_template'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_listing_template':
                if ($_SESSION['edit_listing_template'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'export_listings':
                if ($_SESSION['export_listings'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_all_listings':
                if ($_SESSION['edit_all_listings'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_all_leads':
                if ($_SESSION['edit_all_leads'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_all_users':
                if ($_SESSION['edit_all_users'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'edit_property_classes':
                if ($_SESSION['edit_property_classes'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'can_access_blog_manager':
                if ($_SESSION['blog_user_type'] > 1 || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'is_blog_editor':
                if ($_SESSION['blog_user_type'] == 4 || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            case 'can_manage_addons':
                if ($_SESSION['can_manage_addons'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                    return true;
                }
                break;
            default:
                return false;
        } // End switch($priv_level_needed)
        return false;
    }

    /**
     * Confirm Email Belongs to Active User and return UserName
     *
     * @param string $email Email adddress to lookup.
     *
     * @return array{boolean, int|string}
     */
    public function confirmUserByEmail(string $email): array
    {
        global $ORconn;

        $misc = $this->newMisc();
        //$username = array($username);
        $email = $ORconn->qstr($email);

        /* Verify that user is in database */
        $sql = 'SELECT userdb_user_name, userdb_active
				FROM ' . $this->config['table_prefix'] . 'userdb 
				WHERE  userdb_emailaddress = ' . $email;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        if ($recordSet->RecordCount() < 1) {
            return [true, 1]; //Indicates email not found
        }
        if ($recordSet->fields('userdb_active') != 'yes') {
            return [true, 2]; //Indicates user is inactive
        }

        return [false, (string)$recordSet->fields('userdb_user_name')]; //Success! Email Found and User is Active
    }

    /**
     * Confirm Username and Password is valid
     *
     * @param string $username Username to confirm.
     * @param string $password Password to confirm.
     *
     * @return integer
     */
    public function confirmUser(string $username, string $password): int
    {
        global $ORconn;

        $misc = $this->newMisc();
        //$username = array($username);
        $username = $ORconn->qstr($username);

        /* Verify that user is in database */
        $sql = 'SELECT * 
				FROM ' . $this->config['table_prefix'] . 'userdb 
				WHERE  userdb_user_name=' . $username;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        if ($recordSet->RecordCount() < 1) {
            return 1; //Indicates username failure
        }
        if ($recordSet->fields('userdb_active') != 'yes') {
            return 3; //Indicates user is inactive
        }
        /* Retrieve password from result, strip slashes */
        $dbpassword = (string)$recordSet->fields('userdb_user_password');


        if (password_verify($password, $dbpassword)) {
            return 0; //Success! Username and password confirmed
        } elseif (md5($password) == $dbpassword) {
            //Test For Old MD5 Password
            $hash = password_hash($password, PASSWORD_DEFAULT);
            $sql_hash = $misc->makeDbSafe($hash);
            $sql = 'UPDATE ' . $this->config['table_prefix'] . 'userdb 
								SET userdb_user_password = ' . $sql_hash . '
								WHERE userdb_user_name = ' . $username;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            return 0; //Success! Username and password confirmed
        }
        return 2; //Indicates password failure
    }

    /**
     * Log User Out. Will echo a Location header and cause page to redirect.
     *
     * @param string $type Type should be "admin" for admin logouts, any other value for member.
     *
     * @psalm-return never
     */
    #[NoReturn] public function logOut(string $type = 'admin'): void
    {
        $old_id = (int)$_SESSION['userID'];
        $_SESSION = [];
        // Destroy Cookie
        $this->clearRememberMeCookie();
        @session_destroy();

        //call the after logout plugin function

        $hooks = $this->newHooks();
        $hooks->load('afterUserLogout', $old_id);
        unset($old_id);

        // Refresh the screen
        if ($type == 'admin') {
            header('Location:' . $this->config['baseurl'] . '/admin/');
        } else {
            header('Location:' . $this->config['baseurl'] . '/index.php');
        }
        exit();
    }

    /**
     * Send forgot password email via ajax
     *
     * @param boolean $admin Are we in the admin area.
     *
     * @return string
     */
    public function ajaxForgotPassword(bool $admin = true): string
    {
        global $lang;
        $result = $this->forgotPassword($admin);
        if (str_contains($result, $lang['check_your_email'])) {
            return json_encode(['error' => false, 'msg' => '<div style="font-weight: bold; text-align: center;  padding: 20px;">' . $lang['check_your_email'] . '</div>']) ?: '';
        }
        return json_encode(['error' => true, 'error_msg' => $result]) ?: '';
    }

    /**
     * Send forgot email password
     *
     * @param boolean $admin Set to true if we are in the admin area.
     *
     * @return string Returns error string, or message to check email.
     */
    public function forgotPassword(bool $admin = true): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $email = null;
        if (isset($_POST['email'])) {
            $email = $_POST['email'];
        }
        if (is_string($email)) {
            if (!isset($_POST['token']) || (is_string($_POST['token']) && !$misc->validateCsrfToken($_POST['token']))) {
                return '<font color="red">' . $lang['invalid_csrf_token'] . '</font>';
            }
            $valid = $misc->validateEmail($email);
            if ($valid) {
                $email = $ORconn->qstr($email);
                //$email = mysql_real_escape_string($email);
                // Verify the user has not tried to reset more then 3 times in 24 hours.
                $sql = 'SELECT forgot_id FROM ' . $this->config['table_prefix_no_lang'] . 'forgot 
						WHERE forgot_email = ' . $email . ' 
						AND forgot_time > NOW() - INTERVAL 1 DAY';
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                if ($recordSet->Recordcount() > 3) {
                    return $lang['to_many_password_reset_attempts'];
                }
                if ($this->config['demo_mode']) {
                    return $lang['password_reset_denied_demo_mode'];
                }
                $sql = 'SELECT userdb_user_name, userdb_emailaddress 
						FROM ' . $this->config['table_prefix'] . 'userdb 
						WHERE userdb_emailaddress=' . $email;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $num = $recordSet->RecordCount();
                if ($num == 1) {
                    $forgot_rand = mt_rand(100000, 999999);
                    $user_email = (string)$recordSet->fields('userdb_emailaddress');
                    $user_name = (string)$recordSet->fields('userdb_user_name');
                    $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . "forgot (forgot_rand, forgot_email, forgot_time) 
							VALUES ($forgot_rand,'$user_email', CURRENT_TIMESTAMP())";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    if ($admin) {
                        $forgot_link = $this->config['baseurl'] . '/admin/index.php?action=forgot&id=' . $forgot_rand . '&email=' . $user_email;
                    } else {
                        $forgot_link = $this->config['baseurl'] . '/index.php?action=forgot&id=' . $forgot_rand . '&email=' . $user_email;
                    }
                    $message = $lang['your_username'] . ' ' . $user_name . "\r\n";
                    $message .= $lang['click_to_reset_password'] . "\r\n";
                    $message .= $forgot_link . "\r\n";
                    $message .= $lang['link_expires'] . "\r\n";
                    $misc->sendEmail($this->config['admin_name'], $this->config['admin_email'], $user_email, $message, $lang['forgotten_password']);

                    return '<div style="font-weight: bold; text-align: center; padding: 20px;">' . $lang['check_your_email'] . '</div>';
                } else {
                    return '<font color="red">' . $lang['email_invalid_email_address'] . '</font>';
                }
            }
        }
        return '<font color="red">' . $lang['email_invalid_email_address'] . '</font>';
    }

    /**
     * Show and process password forgot form.
     *
     * @return string
     */
    public function forgotPasswordReset(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();

        $data = $lang['invalid_expired_link'];

        if (!isset($_POST['user_pass']) || !is_string($_POST['user_pass'])) {
            if (isset($_GET['id']) && (isset($_GET['email']) && is_string($_GET['email']))) {
                $id = intval($_GET['id']);

                $email = filter_var($_GET['email'], FILTER_VALIDATE_EMAIL);
                if ($email !== false) {
                    $sql_email = $ORconn->qstr($email);
                } else {
                    $sql_email = $ORconn->qstr('');
                    $email = '';
                }

                //is this an agent
                $is_agent = $this->isUserAgent('', $email);

                //set where the form posts to based on user type
                if ($is_agent) {
                    $action_var = $this->config['baseurl'] . '/admin/index.php?action=forgot';
                } else {
                    $action_var = $this->config['baseurl'] . '/index.php?action=forgot';
                }

                $sql = 'SELECT forgot_id FROM ' . $this->config['table_prefix_no_lang'] . 'forgot 
						WHERE forgot_email = ' . $sql_email . " 
						AND forgot_rand = $id 
						AND forgot_time > NOW() - INTERVAL 1 DAY";
                $recordSet = $ORconn->execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $num = $recordSet->RecordCount();
                if ($num == 1) {
                    $data = '<div style="text-align: center; padding: 20px;">
								<form id="pass_reset" action="' . $action_var . '" method="post">
									<input type="hidden" name="rand_id" value="' . $id . '">
									<input type="hidden" name="email" value="' . htmlentities($email) . '">
									<p>' . $lang['enter_new_password'] . ' <input type="password" spellcheck="false" name="user_pass" /></p>
									<p><input class="or_std_button" type="submit" value="' . $lang['reset_password'] . '" /></p>
								</form>
							</div>';
                }
            }
        } else {
            $id = intval($_POST['rand_id']);

            $email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
            if ($email !== false) {
                $sql_email = $ORconn->qstr($email);
            } else {
                $sql_email = $ORconn->qstr('');
            }

            $sql = 'SELECT forgot_id 
					FROM ' . $this->config['table_prefix_no_lang'] . 'forgot 
					WHERE forgot_email = ' . $sql_email . " 
					AND forgot_rand = $id 
					AND forgot_time > NOW() - INTERVAL 1 DAY";
            $recordSet = $ORconn->execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $num = $recordSet->RecordCount();
            if ($num == 1) {
                // Delete ID from Forgot list
                $delete_id = intval($recordSet->fields('forgot_id'));
                $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . "forgot 
						WHERE forgot_id = $delete_id";
                $recordSet = $ORconn->execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                // Set Password
                $hash = password_hash($_POST['user_pass'], PASSWORD_DEFAULT);

                $sql_hash = $ORconn->qstr($hash);

                $sql = 'UPDATE ' . $this->config['table_prefix'] . "userdb 
						SET userdb_user_password = $sql_hash 
						WHERE userdb_emailaddress = " . $sql_email;
                $recordSet = $ORconn->execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                } else {
                    $data .= '<div style="text-align: center; padding: 20px;">
								<h3>' . $lang['password_changed'] . '</h3>
								<br />
								' . $lang['login'] . ': <a href="' . $this->config['baseurl'] . '/admin/index.php">' . $this->config['baseurl'] . '/admin/index.php</a>
							</div>';
                }
            } else {
                $data .= $lang['invalid_expired_link'];
            }
        }
        return $data;
    }

    /**
     * Reset User password. Requires edit_all_users privilage
     *
     * @return string
     */
    public function ajaxResetPassword(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();

        //Check for edit all user permissions
        $security = $this->verifyPriv('edit_all_users');
        if ($security) {
            $userID = intval($_GET['user_id']);
            // no touching the admin if you ain't the admin yo-self
            if ($userID === 1 && $_SESSION['userID'] !== 1) {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => $lang['access_denied']]) ?: '';
            }

            // Generate and set password
            $newpass = $misc->generatePassword();
            $hash = password_hash($newpass, PASSWORD_DEFAULT);
            $sql_hash = $ORconn->qstr($hash);

            $sql = 'UPDATE ' . $this->config['table_prefix'] . 'userdb 
					SET userdb_user_password = ' . $sql_hash . ' 
					WHERE userdb_id = ' . $userID;
            $recordSet = $ORconn->execute($sql);

            if (is_bool($recordSet)) {
                //no such user ID
                $misc->logErrorAndDie($sql);
            } else {
                //send account info email

                $usermg = $this->newUserManagement();

                //is this an agent
                $is_agent = $this->isUserAgent((string)$userID);

                if ($is_agent || $userID === 1) {
                    $usermg->sendUserSignupEmail($userID, 'agent', $newpass);
                } else {
                    $usermg->sendUserSignupEmail($userID, 'member', $newpass);
                }

                header('Content-type: application/json');
                return json_encode(['error' => '0', 'user_id' => $_GET['user_id']]) ?: '';
            }
        }
        //No permission to change
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['access_denied']]) ?: '';
    }

    /**
     * Checks if user is an agent. Can check by user id or user email
     *
     * @param string $user_id    User ID to lookup.
     * @param string $user_email User Email to lookup.
     *
     * @return boolean
     */
    public function isUserAgent(string $user_id = '', string $user_email = ''): bool
    {
        global $ORconn;

        $misc = $this->newMisc();
        //returns true if the given user_id or email addy is an Agent
        $is_agent = false;

        if ($user_id != '') {
            $user_id = intval($user_id);
            $where_clause = 'userdb_id = ' . $user_id;
        } else {
            $user_email = $ORconn->qstr($user_email);
            $where_clause = 'userdb_emailaddress = ' . $user_email;
        }

        $sql = 'SELECT userdb_is_agent, userdb_is_admin 
				FROM ' . $this->config['table_prefix'] . 'userdb 
				WHERE ' . $where_clause;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $userdb_is_agent = (string)$recordSet->fields('userdb_is_agent');
        $userdb_is_admin = (string)$recordSet->fields('userdb_is_admin');
        if ($userdb_is_agent == 'yes') {
            $is_agent = true;
        } else {
            if ($userdb_is_admin == 'yes') {
                $is_agent = true;
            }
        }

        return $is_agent;
    }
}
