<?php

declare(strict_types=1);

namespace OpenRealty;

use stdClass;
use ZipArchive;

/**
 * Class handles all addon code.
 */
class AddonManager extends BaseClass
{
    /**
     * Checks that addon name is valid. die if addon name contains an invalid character.
     *
     * @param string $addon_name Addon name to check.
     */
    public function checkAddonName(string $addon_name): void
    {
        global $lang;
        $bad_char = preg_match('/[^A-Za-z0-9_-]/', $addon_name);
        if ($bad_char == 1) {
            die($lang['addon_name_invalid']);
        }
    }

    /**
     * Displays an addon's help links
     *
     * @param string $addon_name Addon name to get help for.
     *
     * @return string
     */
    public function displayAddonHelp(string $addon_name): string
    {
        global $lang;
        $display = '';
        $this->checkAddonName($addon_name);
        $addonClassName = '\\OpenRealty\\Addons\\' . $addon_name[1] . '\\Addon';
        if (class_exists($addonClassName)) {
            $addonClass = new $addonClassName($this->dbh, $this->config);
            if (method_exists($addonClass, 'addonManagerHelp')) {
                $help_array = $addonClass->addonManagerHelp();
                //return array($template_tags,$action_urls,$doc_url);
                $help_template_tags = $help_array[0];
                $help_action_urls = $help_array[1];
                $help_doc_url = $help_array[2];
                if ($help_doc_url != '') {
                    $display .= '<div class="addon_manager_ext_help_link"><a href="' . $help_doc_url . '" title="' . $lang['addon_manager_ext_help_link'] . '" onclick="window.open(\'' . $help_doc_url . '\', \'newwindow\', \'width=800, height=700\'); return false;" >' . $lang['addon_manager_ext_help_link'] . '</a></div>';
                }
                if (!empty($help_template_tags)) {
                    $display .= '<div class="addon_manager_template_tag_header">' . $lang['addon_manager_template_tags'] . '</div>';
                    foreach ($help_template_tags as $tagname => $tagdesc) {
                        $display .= '<div class="addon_manager_template_tag_data">
                                <span class="addon_manager_template_tag_name">' . $tagname . '</span>
                                <span class="addon_manager_template_tag_desc">' . $tagdesc . '</span>
                                </div>';
                    }
                }
                if (!empty($help_action_urls)) {
                    $display .= '<div class="addon_manager_action_url_header">' . $lang['addon_manager_action_urls'] . '</div>';
                    foreach ($help_action_urls as $tagname => $tagdesc) {
                        $display .= '<div class="addon_manager_action_url_data">
                                <span class="addon_manager_action_url_name">' . $tagname . '</span>
                                <span class="addon_manager_action_url_desc">' . $tagdesc . '</span>
                                </div>';
                    }
                }
            }
        }

        return $display;
    }

    /**
     * @return string
     */
    public function installLocalAddon(): string
    {
        global $lang;
        $display = '';
        $misc = $this->newMisc();
        $realname = $misc->cleanFilename($_FILES['userfile']['name']);
        $install_file = $_FILES['userfile']['tmp_name'];
        $filetype = $_FILES['userfile']['type'];
        $extPosition = strpos($realname, '.');
        if (!$extPosition) {
            return '<div class="addon_manager_bad_info">' . htmlentities($realname) . ' - ' . $lang['addon_name_invalid'] . '</div>';
        }
        $addon_name = substr($realname, 0, $extPosition);
        $this->checkAddonName($addon_name);
        //check to make sure it is a zip file
        //$zip_mimetypes = array('application/zip','application/x-zip','application/x-zip-compressed','application/octet-stream','application/x-compress','application/x-compressed','multipart/x-zip');
        //if (in_array($filetype,$zip_mimetypes)){
        $installed_addons = $this->getInstalledAddons();
        if (in_array($addon_name, $installed_addons) && (!isset($_POST['upgrade']) || $_POST['upgrade'] != 'yes')) {
            $display .= '<div class="addon_manager_bad_info">' . htmlentities($addon_name) . ' - ' . $lang['addon_already installed'] . '</div>';
        } else {
            $install_status = $this->extract($install_file, $addon_name);
            if ($install_status === false) {
                $display .= '<div class="addon_manager_bad_info">' . htmlentities($addon_name) . ' - ' . $lang['addon_install_failed'] . '</div>';
            } else {
                $addonClassName = '\\OpenRealty\\Addons\\' . $addon_name . '\\Addon';
                if (class_exists($addonClassName)) {
                    $addonClass = new $addonClassName($this->dbh, $this->config);
                    if (method_exists($addonClass, 'installAddon')) {
                        $data = $addonClass->installAddon();
                    }
                    if (isset($_POST['upgrade']) && $_POST['upgrade'] == 'yes') {
                        $display .= '<div class="addon_manager_good_info">' . htmlentities($addon_name) . ' - ' . $lang['addon_upgade_successful'] . '</div>';
                    } else {
                        $display .= '<div class="addon_manager_good_info">' . htmlentities($addon_name) . ' - ' . $lang['addon_install_successful'] . '</div>';
                    }
                } else {
                    $display .= '<div class="addon_manager_bad_info">' . htmlentities($addon_name) . ' - ' . $lang['addon_install_failed'] . '</div>';
                }
            }
            unlink($install_file);
        }

        //  } else {
        //      $display.='<div class="addon_manager_bad_info">'.htmlentities($addon_name).' - '.$lang['addon_upload_file_not_zip'].'</div>';
        //  }
        return $display;
    }

    public function uninstallAddon(string $addon_name): string
    {
        global $ORconn, $lang;

        $display = '';
        $misc = $this->newMisc();
        $this->checkAddonName($addon_name);
        $addonClassName = '\\OpenRealty\\Addons\\' . $addon_name . '\\Addon';
        if (class_exists($addonClassName)) {
            $addonClass = new $addonClassName($this->dbh, $this->config);
            if (method_exists($addonClass, 'uninstallTables')) {
                $folder_removed = false;
                $db_uninstalled = $addonClass->uninstallTables();
                if ($db_uninstalled) {
                    $folder_removed = $this->rmDirRecurse($this->config['basepath'] . '/addons/' . $addon_name);
                }
                if ($folder_removed) {
                    //Ok Addon is now removed, lets remove it from the addon table.
                    $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . 'addons WHERE addons_name = ' . $misc->makeDbSafe($addon_name);
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $display .= '<div class="addon_manager_good_info">' . $lang['removed_addon'] . ' ' . htmlentities($addon_name) . '</div>';
                }
            }
        }

        return $display;
    }

    public function checkStoreAddonUpdate(string $addon_name): string
    {
        global $ORconn, $lang;

        $display = '';
        $misc = $this->newMisc();
        $this->checkAddonName($addon_name);
        //New Update Method
        $details = $this->getAddonDetails($addon_name);
        if (is_array($details)) {
            $latest_version = $details['version'];
            if ($latest_version) {
                $sql = 'SELECT addons_version 
                    FROM ' . $this->config['table_prefix_no_lang'] . 'addons 
                    WHERE addons_name =' . $misc->makeDbSafe($addon_name);
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $current_version = trim($recordSet->fields('addons_version'));
                if ($current_version == $latest_version) {
                    $display .= '<div class="addon_manager_good_info">' . $lang['addon_already_latest_version'] . ' ' . htmlentities($addon_name) . '</div>';
                } else {
                    $display .= '<div class="addon_manager_bad_info">' . $lang['addon_update_avaliable'] . ' <a href="' . $this->config['baseurl'] . '/admin/index.php?action=addon_manager&amp;install_update=' . $addon_name . '" title="' . $lang['addon_check_for_updates'] . '">' . $lang['addon_download_update'] . ' ' . htmlentities($addon_name) . '</a></div>';
                }
            }
        }

        return $display;
    }

    public function installStoreAddon(string $addon_name): string
    {
        global $lang;

        $display = '';
        $misc = $this->newMisc();
        [$file_download_url, $folder] = $this->getDownloadUrl($addon_name);
        if (is_bool($file_download_url) || is_bool($folder)) {
            return '<div class="addon_manager_bad_info">' . htmlentities($addon_name) . ' - ' . $lang['addon_install_file_not_avaliable'] . '</div>';
        }
        $file = $misc->getUrl($file_download_url);
        if (is_bool($file)) {
            $display .= '<div class="addon_manager_bad_info">' . htmlentities($addon_name) . ' - ' . $lang['addon_install_file_not_avaliable'] . '</div>';
        } else {
            //we have the file unzip it and then install it
            $install_file = $this->writeTmpZip($file);
            $install_status = $this->extract($install_file, $folder);
            if ($install_status === false) {
                $display .= '<div class="addon_manager_bad_info">' . htmlentities($addon_name) . ' - ' . $lang['addon_install_failed'] . '</div>';
            } else {
                //install successful, run the addon install function
                $addonClassName = '\\OpenRealty\\Addons\\' . $folder . '\\Addon';
                if (class_exists($addonClassName)) {
                    $addonClass = new $addonClassName($this->dbh, $this->config);
                    if (method_exists($addonClass, 'installAddon')) {
                        $data = $addonClass->installAddon();
                        $display .= '<div class="addon_manager_good_info">' . htmlentities($addon_name) . ' - ' . $lang['addon_install_successful'] . '</div>';
                    }
                } else {
                    $display .= '<div class="addon_manager_bad_info">' . htmlentities($addon_name) . ' - ' . $lang['addon_install_failed'] . '</div>';
                }
            }
            unlink($install_file);
        }
        return $display;
    }

    /**
     * @return true
     */
    public function rmDirRecurse(string $path): bool
    {
        $path = rtrim($path, '/') . '/';
        $handle = opendir($path);
        while (false !== ($file = readdir($handle))) {
            if ($file != '.' and $file != '..') {
                $fullpath = $path . $file;
                if (is_dir($fullpath)) {
                    $this->rmDirRecurse($fullpath);
                } else {
                    unlink($fullpath);
                }
            }
        }
        closedir($handle);
        rmdir($path);
        return true;
    }

    public function getTmp(): string
    {
        $tmpfile = @tempnam('dummy', '');
        $path = dirname($tmpfile);
        unlink($tmpfile);
        return $path;
    }

    public function extract(string $file, string $addon_name): bool
    {
        $file_structure = true;
        $path_insert = '';
        if (class_exists('ZipArchive')) {
            $zip = new ZipArchive();
            $res = $zip->open($file);
            if ($res === true) {
                for ($i = 0; $i < $zip->numFiles; $i++) {
                    $zip_name = $zip->getNameIndex($i);
                    if (!str_starts_with($zip_name, $addon_name)) {
                        $file_structure = false;
                    }
                }
                if ($file_structure === false) {
                    $path_insert = $addon_name . '/';
                }

                if (!is_dir($this->config['basepath'] . '/addons/' . $addon_name)) {
                    mkdir($this->config['basepath'] . '/addons/' . $addon_name);
                }

                for ($i = 0; $i < $zip->numFiles; $i++) {
                    $zip_name = $zip->getNameIndex($i);
                    $path_parts = pathinfo($this->config['basepath'] . '/addons/' . $path_insert . $zip_name);
                    //echo'<pre>'.print_r($path_parts,true).'</pre>';
                    if (!isset($path_parts['extension']) && !file_exists($this->config['basepath'] . '/addons/' . $path_insert . $zip_name)) {
                        mkdir($this->config['basepath'] . '/addons/' . $path_insert . $zip_name);
                    }
                    if (!is_dir($this->config['basepath'] . '/addons/' . $path_insert . $zip_name)) {
                        $fp = fopen($this->config['basepath'] . '/addons/' . $path_insert . $zip_name, 'wb');
                        if ($fp) {
                            $buf = $zip->getStream($zip->getNameIndex($i));
                            stream_copy_to_stream($buf, $fp);

                            fclose($buf);
                        }
                        fclose($fp);
                    }
                }
                $zip->close();
                return true;
            } else {
                echo '<pre>Failed to Open Zip</pre>';
            }
        }
        return false;
    }

    public function writeTmpZip(string $data): string
    {
        $tmp_path = $this->getTmp();
        $file_name = time() . '.zip';
        $fp = fopen($tmp_path . '/' . $file_name, 'wb');
        fwrite($fp, $data);
        fclose($fp);
        return $tmp_path . '/' . $file_name;
    }

    public function displayAddonManager(): string
    {
        global $ORconn, $lang;
        $page = $this->newPageAdmin();

        $display = '';
        $misc = $this->newMisc();
        //Load TEmplate File
        $page->loadPage($this->config['admin_template_path'] . '/addon_manager.html');


        //Check addon folder is writeable
        $addon_permission = is_writeable($this->config['basepath'] . '/addons');
        $status_message = '';
        if (!$addon_permission) {
            $display .= '<div class="redtext">' . $lang['warning_addon_folder_not_writeable'] . '</div>';
            return $display;
        }
        //Are we deleting?
        if (isset($_GET['uninstall']) && is_string($_GET['uninstall'])) {
            $uninstall_name = $_GET['uninstall'];
            $status_message .= $this->uninstallAddon($uninstall_name);
        }
        //Are we installing?
        if (isset($_GET['install']) && is_string($_GET['install'])) {
            $status_message .= $this->installStoreAddon($_GET['install']);
        }
        if (isset($_GET['install_update']) && is_string($_GET['install_update'])) {
            $status_message .= $this->installStoreAddon($_GET['install_update']);
        }
        //Are we Updating an addon?
        if (isset($_GET['check_update']) && is_string($_GET['check_update'])) {
            $update_name = $_GET['check_update'];
            $status_message .= $this->checkStoreAddonUpdate($update_name);
        }

        //Are we manually installing?
        if (isset($_POST['action']) && is_string($_POST['token']) && $_POST['action'] == 'man_install' && $_FILES['userfile']['error'] == 0) {
            if (!isset($_POST['token']) || !$misc->validateCsrfToken($_POST['token'])) {
                $status_message .= $lang['invalid_csrf_token'];
            } else {
                $status_message .= $this->installLocalAddon();
            }
        }

        if (isset($_GET['view_help']) && is_string($_GET['view_help'])) {
            $help_name = $_GET['view_help'];
            $display .= $this->displayAddonHelp($help_name);
            return $display;
        }

        //Load Intalled Addons into Template.
        $sql = 'SELECT * FROM ' . $this->config['table_prefix_no_lang'] . 'addons ORDER BY addons_name;';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }

        if ($recordSet->RecordCount() == 0) {
            $display .= '<tr><td colspan="4" style="text-align:center;">' . $lang['addon_manager_none_installed'] . '</td></tr>';
        }

        $install_addon_template = '';
        while (!$recordSet->EOF) {
            $install_addon_template .= $page->getTemplateSection('addon_installed_block');
            $name = $recordSet->fields('addons_name');
            $version = $recordSet->fields('addons_version');
            $install_addon_template = $page->parseTemplateSection($install_addon_template, 'addon_name', $name);
            $install_addon_template = $page->parseTemplateSection($install_addon_template, 'addon_version', $version);
            //Check Addon Status
            $status_msg = $lang['addon_ok'];
            //Status Code 0=ok 1=FatalError 2=Warngin
            $status_code = 0;
            //Define action variables
            $template_tags = [];
            $action_urls = [];
            $doc_url = '';
            $has_help = false;
            $in_store = false;
            $has_uninstall = false;
            //See if addon was removed.
            $addonClassName = '\\OpenRealty\\Addons\\' . $name . '\\Addon';
            if (class_exists($addonClassName)) {
                $addonClass = new $addonClassName($this->dbh, $this->config);
                if (method_exists($addonClass, 'addonManagerHelp')) {
                    $help_array = $addonClass->addonManagerHelp();
                    //return array($template_tags,$action_urls,$doc_url);
                    $template_tags = $help_array[0];
                    $action_urls = $help_array[1];
                    $doc_url = $help_array[2];
                    if (!empty($template_tags)) {
                        $has_help = true;
                    }
                    if (!empty($action_urls)) {
                        $has_help = true;
                    }
                    if (!empty($doc_url)) {
                        $has_help = true;
                    }
                }
                if (method_exists($addonClass, 'uninstallTables')) {
                    $has_uninstall = true;
                }
                //Check if addon is the store
                $in_store = $this->getAddonDetails($name);
            } else {
                $status_msg = $lang['addon_files_removed'];
                $status_code = 1;
            }

            if (!$in_store) {
                $install_addon_template = $page->removeTemplateBlock('action_update', $install_addon_template);
            } else {
                $install_addon_template = $page->cleanupTemplateBlock('action_update', $install_addon_template);
            }


            if (!$has_help) {
                $install_addon_template = $page->removeTemplateBlock('action_help', $install_addon_template);
            } else {
                $install_addon_template = $page->cleanupTemplateBlock('action_help', $install_addon_template);
            }
            if (!$has_uninstall) {
                $install_addon_template = $page->removeTemplateBlock('action_uninstall', $install_addon_template);
            } else {
                $install_addon_template = $page->cleanupTemplateBlock('action_uninstall', $install_addon_template);
            }
            $install_addon_template = $page->parseTemplateSection($install_addon_template, 'status_code', (string)$status_code);
            $install_addon_template = $page->parseTemplateSection($install_addon_template, 'status_msg', $status_msg);

            $recordSet->MoveNext();
        }

        $page->replaceTemplateSection('addon_installed_block', $install_addon_template);
        $page->replaceTag('application_status_text', $status_message);
        $page->replacePermissionTags();
        $page->autoReplaceTags('', true);
        $display .= $page->returnPage();
        return $display;
    }

    /**
     * @return (bool|string)[]|false
     *
     * @psalm-return array{name: string, title: string, author: string, version: string, installed: bool}|false
     */
    public function getAddonDetails(string $addon_name): array|false
    {
        $misc = $this->newMisc();
        $installed_addons = $this->getInstalledAddons();
        $store_url = 'https://www.open-realty.org/addons.json';
        $data = $misc->getUrl($store_url, 1800);
        if (is_bool($data)) {
            return false;
        }
        $parsed_data = $this->parseStoreResponse($data);
        foreach ($parsed_data as $addon) {
            $installed = false;
            if (in_array($addon->title, $installed_addons)) {
                $installed = true;
            }
            if ($addon->folder == $addon_name) {
                $details = [];
                $details['name'] = $addon->folder;
                $details['title'] = $addon->title;
                $details['author'] = $addon->author;
                $details['version'] = $addon->version;
                $details['installed'] = $installed;
                return $details;
            }
        }
        return false;
    }

    /**
     * @return (false|mixed)[]
     *
     * @psalm-return list{false|string, false|string}
     */
    private function getDownloadUrl(string $addon_name): array
    {
        $misc = $this->newMisc();
        $url = false;
        $folder = false;
        $store_url = 'https://www.open-realty.org/addons.json';
        $data = $misc->getUrl($store_url, 1800);
        $parsed_data = $this->parseStoreResponse($data);
        foreach ($parsed_data as $addon) {
            if ($addon->folder == $addon_name) {
                $url = $addon->download_url;
                $folder = $addon->folder;
                break;
            }
        }
        return [$url, $folder];
    }

    /**
     * @return object[]
     *
     * @psalm-return list<object{title:string, author:string, homepage:string, docs:string, folder:string,
     *               version:string, download_url:string, stability:string, min_compatibility:string,
     *               max_compatibility:string}>
     */
    private function parseStoreResponse(string|bool $data, bool $prerelease = true): array
    {
        $addons = [];
        if (!is_bool($data)) {
            $feed = json_decode($data, true);
            if ($feed) {
                foreach ($feed['items'] as $item) {
                    $addon = (object)[
                        'title' => (string)$item['title'],
                        'author' => (string)$item['author'],
                        'homepage' => (string)$item['homepage'],
                        'docs' => (string)$item['docs'],
                        'folder' => (string)$item['folder'],
                        'version' => '',
                        'download_url' => '',
                        'stability' => '',
                        'min_compatibility' => '',
                        'max_compatibility' => '',

                    ];
                    foreach ($item['versions'] as $version) {
                        if (!array_key_exists('min_compatibility', $version)) {
                            //Skipping item as it does correctly define a min_compatibility
                            continue;
                        }
                        if (!version_compare($version['min_compatibility'], $this->config['version'], '<=')) {
                            //We do not meet minimum capabilities
                            continue;
                        }
                        if (array_key_exists('max_compatibility', $version) && !empty($version['max_compatibility'])) {
                            //This addon has a max_compatibility check it
                            if (!version_compare($version['max_compatibility'], $this->config['version'], '>=')) {
                                //We do not meet maximum capabilities
                                continue;
                            }
                        }
                        if (!$prerelease && $version['stability'] !== 'stable') {
                            continue;
                        }
                        $addon->version = $version['version'];
                        $addon->download_url = $version['download_url'];
                        $addon->stability = $version['stability'];
                        $addon->min_compatibility = $version['min_compatibility'];
                        $addon->max_compatibility = $version['max_compatibility'] ?? '';
                        $addons[] = $addon;
                        break;
                    }
                }
            }
        }
        return $addons;
    }

    public function ajaxShowStoreAddons(): string
    {
        $misc = $this->newMisc();
        $url = 'https://www.open-realty.org/addons.json';
        $data = $misc->getUrl($url, 1800);
        $parsed_data = $this->parseStoreResponse($data);

        $x = new stdClass();
        $x->data = $parsed_data;
        header('Content-type: application/json');
        return json_encode($x) ?: '';
    }

    /**
     * @psalm-return list<mixed>
     */
    public function getInstalledAddons(): array
    {
        global $ORconn;

        $misc = $this->newMisc();
        $sql = 'SELECT addons_name 
                FROM ' . $this->config['table_prefix_no_lang'] . 'addons;';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $installed_addons = [];
        while (!$recordSet->EOF) {
            $installed_addons[] = $recordSet->fields('addons_name');
            $recordSet->MoveNext();
        }
        return $installed_addons;
    }
}
