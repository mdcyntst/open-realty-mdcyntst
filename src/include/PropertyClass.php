<?php

declare(strict_types=1);

namespace OpenRealty;

class PropertyClass extends BaseClass
{
    public function showClasses(string $status_text = ''): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $display = '';
        // Verify User is an Admin

        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_property_classes');

        if ($security === true) {
            //Load the Core Template

            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/pclass_editor.html');
            $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . 'class 
					ORDER BY class_rank';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $html_template = $page->getTemplateSection('pclass_block');
            $html = '';
            while (!$recordSet->EOF) {
                $html .= $html_template;
                $class_name = $recordSet->fields('class_name');
                $class_id = $recordSet->fields('class_id');
                $class_rank = $recordSet->fields('class_rank');
                $html = $page->replaceTagSafe('class_name', $class_name, $html);
                $html = $page->replaceTagSafe('class_id', $class_id, $html);
                $html = $page->replaceTagSafe('class_rank', $class_rank, $html);
                //Replace Template Block
                $recordSet->MoveNext();
            }
            $page->replaceTemplateSection('pclass_block', $html);
            $page->replaceTag('application_status_text', $status_text);
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);

            return $page->returnPage();
        }
        return $display;
    }

    public function insertPropertyClass(): string
    {
        global $ORconn, $lang;
        // Verify User is an Admin
        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_property_classes');
        $display = '';
        if ($security === true) {
            //Load the Core Template

            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/pclass_insert.html');

            // Get Max rank
            $sql = 'SELECT max(class_rank) as max_rank 
					FROM ' . $this->config['table_prefix'] . 'class';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $rank = $recordSet->fields('max_rank');
            $rank++;

            if (isset($_POST['class_name']) && is_string($_POST['class_name'])) {
                if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfToken($_POST['token'])) {
                    $display .= $lang['invalid_csrf_token'] . '<br />';
                } else {
                    $class_name = $misc->makeDbSafe($_POST['class_name']);

                    $class_rank = abs(intval($_POST['class_rank']));
                    if ($class_rank == 0) {
                        $class_rank = $rank;
                    }

                    $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'class (class_name,class_rank) 
						VALUES (' . $class_name . ',' . $class_rank . ')';
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    } else {
                        $new_class_id = $ORconn->Insert_ID();
                    }
                    if (isset($_POST['field_id']) && is_array($_POST['field_id'])) {
                        foreach ($_POST['field_id'] as $field_id) {
                            if (is_numeric($field_id)) {
                                $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . 'classformelements (class_id,listingsformelements_id) 
								VALUES (' . $new_class_id . ',' . (int)$field_id . ')';
                                $recordSet2 = $ORconn->Execute($sql);
                                if (!$recordSet2) {
                                    $misc->logErrorAndDie($sql);
                                }
                            }
                        }
                    }
                    $display .= $lang['property_class_updated'] . '<br />';
                }
                $display .= $this->showClasses();
            } else {
                $sql = 'SELECT listingsformelements_id, listingsformelements_field_caption 
						FROM ' . $this->config['table_prefix'] . 'listingsformelements 
						ORDER BY listingsformelements_field_caption';
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }

                $html_template = $page->getTemplateSection('formelement_block');
                $html = '';
                while (!$recordSet->EOF) {
                    $html .= $html_template;
                    $field_id = $recordSet->fields('listingsformelements_id');
                    $field_caption = $recordSet->fields('listingsformelements_field_caption');
                    $html = $page->replaceTagSafe('field_id', $field_id, $html);
                    $html = $page->replaceTagSafe('field_caption', $field_caption, $html);

                    $recordSet->MoveNext();
                }
                $page->replaceTemplateSection('formelement_block', $html);
                $page->replaceTagSafe('rank', (string)$rank);
                $page->replaceTag('application_status_text', '');
                $page->replaceLangTemplateTags();
                $page->replacePermissionTags();
                $page->autoReplaceTags('', true);
                return $page->returnPage();
            }
        }
        return $display;
    }

    public function deletePropertyClass(): string
    {
        global $ORconn, $lang;

        // Verify User is an Admin
        $display = '';
        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_property_classes');

        if ($security === true) {
            //Load the Core Template
            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/pclass_editor.html');

            if (isset($_GET['id'])) {
                $class_id = intval($_GET['id']);
                // Now remove any fields associated with the class that are no longer associtaed with any other classes
                // First we have to determine which form elements belong to other classes.
                $sql = 'SELECT DISTINCT (listingsformelements_id) 
						FROM ' . $this->config['table_prefix_no_lang'] . "classformelements 
						WHERE class_id <> $class_id";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $other_class_id = '';
                while (!$recordSet->EOF) {
                    if ($other_class_id == '') {
                        $other_class_id .= $recordSet->fields('listingsformelements_id');
                    } else {
                        $other_class_id .= ',' . $recordSet->fields('listingsformelements_id');
                    }
                    $recordSet->MoveNext();
                }
                if ($other_class_id == '') {
                    $other_class_id = '0';
                }
                // Ok now grab a list of the id's to delete them from the listingformelements table.
                // Also delete them from the lass_form_elements.
                $sql = 'SELECT DISTINCT (listingsformelements_id) 
						FROM ' . $this->config['table_prefix_no_lang'] . "classformelements 
						WHERE class_id = $class_id 
						AND listingsformelements_id NOT IN ($other_class_id)";

                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $ids = '';
                while (!$recordSet->EOF) {
                    if ($ids == '') {
                        $ids .= $recordSet->fields('listingsformelements_id');
                    } else {
                        $ids .= ',' . $recordSet->fields('listingsformelements_id');
                    }
                    $recordSet->MoveNext();
                }
                if ($ids == '') {
                    $ids = '0';
                }
                $sql = 'DELETE FROM  ' . $this->config['table_prefix_no_lang'] . "classformelements 
						WHERE listingsformelements_id  IN ($ids)";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $sql = 'SELECT listingsformelements_field_name 
						FROM ' . $this->config['table_prefix'] . "listingsformelements 
						WHERE listingsformelements_id  IN ($ids)";
                $recordSet1 = $ORconn->Execute($sql);
                if (is_bool($recordSet1)) {
                    $misc->logErrorAndDie($sql);
                }
                while (!$recordSet1->EOF) {
                    $field_name = $misc->makeDbSafe($recordSet1->fields('listingsformelements_field_name'));
                    // Delete All Translationf for this field.
                    $configured_langs = explode(',', $this->config['configured_langs']);
                    while (!$recordSet->EOF) {
                        foreach ($configured_langs as $configured_lang) {
                            $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . $configured_lang . "_listingsformelements 
									WHERE listingsformelements_id IN ($ids)";
                            $recordSet = $ORconn->Execute($sql);
                            if (is_bool($recordSet)) {
                                $misc->logErrorAndDie($sql);
                            }
                        }
                    }
                    // Cleanup any listingdbelemts entries from this field.
                    $sql = 'SELECT listingsdbelements_id 
							FROM ' . $this->config['table_prefix'] . "listingsdbelements 
							WHERE listingsdbelements_field_name = $field_name";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    while (!$recordSet->EOF) {
                        $listingsdbelements_id = $recordSet->fields('listingsdbelements_id');
                        foreach ($configured_langs as $configured_lang) {
                            $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . $configured_lang . "_listingsdbelements 
									WHERE listingsdbelements_id = $listingsdbelements_id";
                            $recordSet = $ORconn->Execute($sql);
                            if (is_bool($recordSet)) {
                                $misc->logErrorAndDie($sql);
                            }
                        }
                    }
                    $recordSet1->MoveNext();
                }

                $sql = 'SELECT listingsdb_id 
						FROM ' . $this->config['table_prefix'] . "listingsdb 
						WHERE listingsdb_pclass_id = $class_id";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $ids = '';
                while (!$recordSet->EOF) {
                    if ($ids == '') {
                        $ids .= $recordSet->fields('listingsdb_id');
                    } else {
                        $ids .= ',' . $recordSet->fields('listingsdb_id');
                    }
                    $recordSet->MoveNext();
                }
                if ($ids == '') {
                    $ids = '0';
                }
                // now that we have the listingids delete the listings and any associated listingsdbelements
                $configured_langs = explode(',', $this->config['configured_langs']);
                foreach ($configured_langs as $configured_lang) {
                    $sql = 'DELETE FROM  ' . $this->config['table_prefix_no_lang'] . $configured_lang . "_listingsdb 
							WHERE listingsdb_id  IN ($ids)";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $sql = 'DELETE FROM  ' . $this->config['table_prefix_no_lang'] . $configured_lang . "_listingsdbelements 
							WHERE listingsdb_id  IN ($ids)";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                }
                // Get all images and vtours and delete the images.
                // listingsvtours_id, userdb_id, listingsvtours_caption, listingsvtours_file_name, listingsvtours_thumb_file_name, listingsvtours_description, listingsdb_id, listingsvtours_rank, listingsvtours_active
                $sql = 'SELECT  listingsvtours_thumb_file_name, listingsvtours_file_name 
						FROM  ' . $this->config['table_prefix'] . "listingsvtours 
						WHERE listingsdb_id  IN ($ids)";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                while (!$recordSet->EOF) {
                    $file_name = $recordSet->fields('listingsvtours_file_name');
                    @unlink($this->config['vtour_upload_path'] . "/$file_name");
                    @unlink($this->config['vtour_upload_path'] . "/$file_name");
                    $recordSet->MoveNext();
                }
                // listingsimages_id, userdb_id, listingsimages_caption, listingsimages_file_name, listingsimages_thumb_file_name, listingsimages_description, listingsdb_id, listingsimages_rank, listingsimages_active
                $sql = 'SELECT  listingsimages_thumb_file_name, listingsimages_file_name 
						FROM  ' . $this->config['table_prefix'] . "listingsimages 
						WHERE listingsdb_id  IN ($ids)";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                while (!$recordSet->EOF) {
                    $file_name = $recordSet->fields('listingsimages_file_name');
                    @unlink($this->config['listings_upload_path'] . "/$file_name");
                    @unlink($this->config['listings_upload_path'] . "/$file_name");
                    $recordSet->MoveNext();
                }
                // Now delete DB records of the images and vtours for all langs.
                foreach ($configured_langs as $configured_lang) {
                    $sql = 'DELETE FROM  ' . $this->config['table_prefix_no_lang'] . $configured_lang . "_listingsimages 
							WHERE listingsdb_id  IN ($ids)";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $sql = 'DELETE FROM  ' . $this->config['table_prefix_no_lang'] . $configured_lang . "_listingsvtours 
							WHERE listingsdb_id  IN ($ids)";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                }
                // Now we jsut need to delete all associates from the classformelements and class tables.
                $sql = 'DELETE FROM  ' . $this->config['table_prefix_no_lang'] . "classformelements 
						WHERE class_id = $class_id";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $configured_langs = explode(',', $this->config['configured_langs']);
                foreach ($configured_langs as $configured_lang) {
                    $sql = 'DELETE FROM  ' . $this->config['table_prefix_no_lang'] . $configured_lang . "_class 
							WHERE class_id = $class_id";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                }
                $display = $this->showClasses($lang['property_class_deleted']);
            }
        } else {
            $display = 'Permission Denied - Delete Pclass';
        }
        return $display;
    }

    public function ajaxSaveClassRank(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        // Verify User is an Admin
        $security = $login->verifyPriv('edit_property_classes');

        if ($security === true) {
            if (isset($_POST['search_setup']) && isset($_POST['class_id']) && is_array($_POST['class_id'])) {
                $rank_field = 'class_rank';
                $class_rank = 0;

                foreach ($_POST['class_id'] as $class_id) {
                    //empty locations are skipped
                    if (is_numeric($class_id)) {
                        $class_rank = $class_rank + 1;

                        $sql = 'UPDATE ' . $this->config['table_prefix'] . 'class
						SET  ' . $rank_field . " = '" . $class_rank . "'
						WHERE class_id = '" . (int)$class_id . "'";
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            $misc->logErrorAndDie($sql);
                        }
                    }
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'status_msg' => $lang['admin_template_editor_field_order_set']]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['access_denied']]) ?: '';
    }

    public function ajaxModifyPropertyClass(): string
    {
        global $ORconn, $lang;

        // Verify User is an Admin
        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_property_classes');
        if ($security === true) {
            if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
            }

            if (
                isset($_POST['class_id'], $_POST['class_name'], $_POST['class_rank'])
                && is_numeric($_POST['class_id'])
                && is_numeric($_POST['class_rank'])
                && is_string($_POST['class_name'])
            ) {
                // Get Max rank
                $sql = 'SELECT max(class_rank) as max_rank 
						FROM ' . $this->config['table_prefix'] . 'class';
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $rank = $recordSet->fields('max_rank');
                $rank++;

                $class_id = intval($_POST['class_id']);
                $class_name = $misc->makeDbSafe($_POST['class_name']);
                $class_rank = abs(intval($_POST['class_rank']));
                if ($class_rank == 0) {
                    $class_rank = $rank;
                }
                $sql = 'UPDATE ' . $this->config['table_prefix'] . 'class 
						SET class_name = ' . $class_name . ',class_rank = ' . $class_rank . ' 
						WHERE class_id = ' . $class_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'statustext' => $lang['property_class_updated']]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }
}
