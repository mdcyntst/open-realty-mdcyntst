<?php

declare(strict_types=1);

namespace OpenRealty;

class PageUserAjax extends Page
{
    public function callAjax(): string
    {
        global $lang;
        switch ($_GET['action']) {
            case 'is_member_check':
                if (isset($_POST['email']) && is_string($_POST['email'])) {
                    $login = $this->newLogin();
                    return $login->verifyMemberEmail($_POST['email']);
                }
                break;
            case 'is_member_full_check':
                if (
                    isset($_POST['email'], $_POST['password'])
                    && is_string($_POST['email'])
                    && is_string($_POST['password'])
                ) {
                    $login = $this->newLogin();
                    return $login->ajaxCheckMemberLogin($_POST['email'], $_POST['password']);
                }
                break;
            case 'create_member_account':
                if ($this->config['allow_member_signup']) {
                    if (
                        isset($_POST['email'], $_POST['fname'], $_POST['lname'])
                        && is_string($_POST['email'])
                        && is_string($_POST['fname'])
                        && is_string($_POST['lname'])
                    ) {
                        $user_managment = $this->newUserManagement();
                        return $user_managment->ajaxMemberCreation($_POST['email'], $_POST['fname'], $_POST['lname']);
                    } else {
                        return json_encode(['error' => true, 'error_msg' => $lang['required_field_not_filled']]) ?: '';
                    }
                } else {
                    return json_encode(['error' => true, 'error_msg' => $lang['no_user_signup']]) ?: '';
                }

                break;
            case 'ajax_forgot_password':
                if (isset($_POST['email'])) {
                    $login = $this->newLogin();
                    return $login->ajaxForgotPassword(false);
                }
                break;
            default:
                // Handle Addons
                $addon_name = [];
                if (is_string($_GET['action'])) {
                    if (preg_match('/^addon_(.\S*?)_.*/', $_GET['action'], $addon_name)) {
                        $addonClassName = '\\OpenRealty\\Addons\\' . $addon_name[1] . '\\Addon';
                        if (class_exists($addonClassName)) {
                            $addonClass = new $addonClassName($this->dbh, $this->config);
                            return $addonClass->runUserActionAjax();
                        }
                    }
                }
        }
        return '';
    }
}
