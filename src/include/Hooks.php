<?php

declare(strict_types=1);

namespace OpenRealty;

class Hooks extends BaseClass
{
    public function load(string $hook_name, mixed $hooked_id): null|array|string
    {
        $result = null;
        //Load each hook file and call specified hook
        if ($handle = opendir(dirname(__FILE__, 2) . '/hooks')) {
            while (false !== ($file = readdir($handle))) {
                if ($file != '.' && $file != '..' && $file != 'CVS' && $file != '.svn' && !str_contains($file, '.dist.php') && strpos($file, '.php')) {
                    if (is_file(dirname(__FILE__, 2) . '/hooks/' . $file)) {
                        $path_parts = pathinfo(dirname(__FILE__, 2) . '/hooks/' . $file);
                        //Make sure this is a PHP file and not something esle..
                        if (isset($path_parts['extension']) && $path_parts['extension'] == 'php') {
                            /** @psalm-suppress UnresolvableInclude */
                            include_once dirname(__FILE__, 2) . '/hooks/' . $file;
                            $class_name = str_replace('.php', '', $file);
                            //Add Namespace to class name.
                            $class_name = "OpenRealty\Hooks\\$class_name\\$class_name";
                            /** @psalm-suppress InvalidStringClass */
                            $sub_hook = new $class_name($this->dbh, $this->config);
                            if (method_exists($sub_hook, $hook_name)) {
                                $result = $sub_hook->$hook_name($hooked_id);
                            }
                        }
                    }
                }
            }
            closedir($handle);
        }
        return $result;
    }
}
