<?php

declare(strict_types=1);

namespace OpenRealty;

use JetBrains\PhpStorm\NoReturn;

class FileHandler extends MediaHandler
{
    public function ajaxDisplayListingFiles(int $listing_id): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $listing_pages = $this->newListingPages();

        $login = $this->newLogin();
        $status_text = '';
        //Get Listing owner
        $listing_agent_id = $listing_pages->getListingAgentValue('userdb_id', $listing_id);
        //Make sure we can Edit this lisitng
        $has_permission = true;
        if ($_SESSION['userID'] != $listing_agent_id) {
            $security = $login->verifyPriv('edit_all_listings');
            if ($security !== true) {
                $has_permission = false;
            }
        }
        if ($has_permission) {
            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/listing_editor_files_display.html');
            //Load Listing Images
            $sql = 'SELECT listingsfiles_id, listingsfiles_caption, listingsfiles_file_name FROM ' . $this->config['table_prefix'] . "listingsfiles WHERE (listingsdb_id = $listing_id) ORDER BY listingsfiles_rank";
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $page->page = str_replace('{listing_id}', (string)$listing_id, $page->page);
            $html = $page->getTemplateSection('file_block');
            $new_html = '';
            $num_images = $recordSet->RecordCount();
            while (!$recordSet->EOF) {
                $new_html .= $html;
                $caption = $recordSet->fields('listingsfiles_caption');
                $file_name = $recordSet->fields('listingsfiles_file_name');
                if (strlen($file_name) > 27) {
                    $file_name = substr(strip_tags($file_name), 0, 23) . ' ...';
                }
                $file_id = $recordSet->fields('listingsfiles_id');
                // gotta grab the image size
                $ext = substr(strrchr($file_name, '.'), 1);
                //Lookup Icon
                $iconpath = $this->config['file_icons_path'] . '/' . $ext . '.png';
                if (file_exists($iconpath)) {
                    $icon = $this->config['listings_view_file_icons_path'] . '/' . $ext . '.png';
                } else {
                    $icon = $this->config['listings_view_file_icons_path'] . '/default.png';
                }
                $new_html = str_replace('{file_caption}', htmlentities($caption, ENT_COMPAT, $this->config['charset']), $new_html);
                $new_html = str_replace('{file_id}', $file_id, $new_html);
                $new_html = str_replace('{icon_src}', $icon, $new_html);
                $new_html = str_replace('{file_name}', $file_name, $new_html);
                // gotta grab the image size
                //echo '<br />'."$this->config[file_upload_path]/$thumb_file_name".'<br />';

                $recordSet->MoveNext();
            } // end while
            $page->replaceTemplateSection('file_block', $new_html);
            $avaliable_images = $this->config['max_listings_file_uploads'] - $num_images;
            if ($avaliable_images > 0) {
                $page->page = $page->cleanupTemplateBlock('file_upload', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('file_upload', $page->page);
            }
            //End Listing Images
            //Finish Loading Template
            $page->replaceTag('application_status_text', $status_text);
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        } else {
            return 'Permission Denied';
        }
    }

    public function ajaxDisplayUserFiles(int $user_id): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $status_text = '';
        //Make sure we can Edit this lisitng
        $has_permission = true;
        if ($_SESSION['userID'] != $user_id) {
            $security = $login->verifyPriv('edit_all_users');
            if ($security !== true) {
                $has_permission = false;
            }
        }
        if ($has_permission) {
            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/user_editor_files_display.html');
            //Load Listing Images
            $sql = 'SELECT usersfiles_id, usersfiles_caption, usersfiles_file_name 
					FROM ' . $this->config['table_prefix'] . "usersfiles 
					WHERE (userdb_id = $user_id) 
					ORDER BY usersfiles_rank";
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $page->page = str_replace('{user_id}', (string)$user_id, $page->page);
            $html = $page->getTemplateSection('file_block');
            $new_html = '';
            $num_images = $recordSet->RecordCount();
            while (!$recordSet->EOF) {
                $new_html .= $html;
                $caption = $recordSet->fields('usersfiles_caption');
                $file_name = $recordSet->fields('usersfiles_file_name');
                if (strlen($file_name) > 27) {
                    $file_name = substr(strip_tags($file_name), 0, 23) . ' ...';
                }
                $file_id = $recordSet->fields('usersfiles_id');
                // gotta grab the image size
                $ext = substr(strrchr($file_name, '.'), 1);
                //Lookup Icon
                $iconpath = $this->config['file_icons_path'] . '/' . $ext . '.png';
                if (file_exists($iconpath)) {
                    $icon = $this->config['listings_view_file_icons_path'] . '/' . $ext . '.png';
                } else {
                    $icon = $this->config['listings_view_file_icons_path'] . '/default.png';
                }
                $new_html = str_replace('{file_caption}', htmlentities($caption, ENT_COMPAT, $this->config['charset']), $new_html);
                $new_html = str_replace('{file_id}', $file_id, $new_html);
                $new_html = str_replace('{icon_src}', $icon, $new_html);
                $new_html = str_replace('{file_name}', $file_name, $new_html);
                // gotta grab the image size
                //echo '<br />'."$this->config[file_upload_path]/$thumb_file_name".'<br />';

                $recordSet->MoveNext();
            } // end while
            $page->replaceTemplateSection('file_block', $new_html);
            $avaliable_images = $this->config['max_listings_file_uploads'] - $num_images;
            if ($avaliable_images > 0) {
                $page->page = $page->cleanupTemplateBlock('file_upload', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('file_upload', $page->page);
            }
            //End Listing Images
            //Finish Loading Template
            $page->replaceTag('application_status_text', $status_text);
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        } else {
            return 'Permission Denied';
        }
    }

    public function renderFilesSelect(int $ID, string $type): string
    {
        // shows the files connected to a given image
        global $ORconn, $lang;

        $misc = $this->newMisc();

        if ($type == 'listing') {
            $file_upload_path = $this->config['listings_file_upload_path'];
            $sqltype = 'listings';
        } else {
            $file_upload_path = $this->config['users_file_upload_path'];
            $sqltype = 'user';
        }
        //Declare an empty display variable to hold all output from function.
        $display = '';
        $sql = 'SELECT ' . $type . 'sfiles_id, ' . $type . 'sfiles_caption, ' . $type . 'sfiles_description, ' . $type . 'sfiles_file_name 
				FROM ' . $this->config['table_prefix'] . $type . 'sfiles 
				WHERE (' . $sqltype . "db_id = $ID) 
				ORDER BY " . $type . 'sfiles_rank';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $num_files = $recordSet->RecordCount();
        if ($num_files >= 1) {
            //ENTER OPENING FORM TAG, ACTION AND HIDDEN FORM FIELDS
            $display .= '<form action="index.php?action=create_download" method="POST">';
            $display .= '<input type="hidden" name="ID" value="' . $ID . '" />';
            $display .= '<input type="hidden" name="type" value="' . $type . '" />';
            $display .= '<select name="file_id">';
            while (!$recordSet->EOF) {
                $optionvalue = '';
                $file_caption = $recordSet->fields($type . 'sfiles_caption');
                $file_filename = $recordSet->fields($type . 'sfiles_file_name');
                $file_id = $recordSet->fields($type . 'sfiles_id');

                if ($file_filename != '' && file_exists("$file_upload_path/$ID/$file_filename")) {
                    $filesize = filesize($file_upload_path . '/' . $ID . '/' . $file_filename);
                }
                if ($this->config['file_display_option'] == 'filename') {
                    $optionvalue .= $file_filename;
                    if ($this->config['file_display_size'] && isset($filesize)) {
                        $optionvalue .= ' - ' . $this->byteSize($filesize);
                    }
                } elseif ($this->config['file_display_option'] == 'caption') {
                    if ($file_caption != '') {
                        $optionvalue .= $file_caption;
                    } else {
                        $optionvalue .= $file_filename;
                    }
                    if ($this->config['file_display_size'] && isset($filesize)) {
                        $optionvalue .= ' - ' . $this->byteSize($filesize);
                    }
                } elseif ($this->config['file_display_option'] == 'both') {
                    if ($file_caption != '') {
                        $optionvalue .= $file_caption . ' - ';
                    }
                    $optionvalue .= $file_filename;
                    if ($this->config['file_display_size'] && isset($filesize)) {
                        $optionvalue .= ' - ' . $this->byteSize($filesize);
                    }
                }
                //ENTER SINGLE FORM OPTION HERE
                $display .= '<option value="' . $file_id . '">' . $optionvalue . '</option>';
                $recordSet->MoveNext();
            }
            //END while (!$recordSet->EOF)
            //ENTER SUBMIT BUTTONS AND CLOSING FORM TAG HERE
            $display .= '</select>';
            $display .= '<input type="button" value="' . $lang['download_file'] . '" onclick="submit();" />';
            $display .= '</form>';
        }
        return $display;
    }

    public function byteSize(int $bytes): string
    {
        $size = $bytes / 1024;
        if ($size < 1024) {
            $size = number_format($size, 2);
            $size .= ' KB';
        } else {
            if ($size / 1024 < 1024) {
                $size = number_format($size / 1024, 2);
                $size .= ' MB';
            } elseif ($size / 1024 / 1024 < 1024) {
                $size = number_format($size / 1024 / 1024, 2);
                $size .= ' GB';
            }
        }
        return (string)$size;
    }

    public function renderTemplatedFiles(int $ID, string $type, string $template): string
    {
        global $ORconn;

        //Load the Core Template class and the Misc Class
        $misc = $this->newMisc();
        $page = $this->newPageUser();
        $folderid = $ID;
        $ID = $misc->makeDbExtraSafe($ID);
        //Declare an empty display variable to hold all output from function.
        $display = '';
        if ($type == 'listing') {
            $file_upload_path = $this->config['listings_file_upload_path'];
            $file_view_path = $this->config['listings_view_file_path'];
            $sqltype = 'listings';
        } else {
            $file_upload_path = $this->config['users_file_upload_path'];
            $file_view_path = $this->config['users_view_file_path'];
            $sqltype = 'user';
        }
        $sql = 'SELECT ' . $type . 'sfiles_id, ' . $type . 'sfiles_caption, ' . $type . 'sfiles_description, ' . $type . 'sfiles_file_name 
				FROM ' . $this->config['table_prefix'] . $type . 'sfiles 
				WHERE (' . $sqltype . "db_id = $ID) 
				ORDER BY " . $type . 'sfiles_rank';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $num_files = $recordSet->RecordCount();
        if ($num_files >= 1) {
            //Load the File Template specified by the calling tag unless a template was specified in the calling template tag.
            $page->loadPage($this->config['template_path'] . '/files_' . $type . '_' . $template . '.html');
            // Determine if the template uses rows.
            // First item in array is the row conent second item is the number of block per block row
            $file_template_row = $page->getTemplateSectionRow('file_block_row');
            $col_count = 1;
            $row = '';
            //Create an empty array to hold the row contents
            $new_row_data = [];
            if (isset($file_template_row[0], $file_template_row[1])) {
                $row = $file_template_row[0];
                $col_count = $file_template_row[1];
                $uses_rows = true;
            } else {
                $uses_rows = false;
            }
            $file_template_section = '';
            $x = 0;
            while (!$recordSet->EOF) {
                if ($uses_rows && $x > $col_count) {
                    //We are at then end of a row. Save the template section as a new row.
                    $new_row_data[] = $page->replaceTemplateSection('file_block', $file_template_section, $row);
                    //$new_row_data[] = $file_template_section;
                    $file_template_section = $page->getTemplateSection('file_block');
                    $x = 1;
                } else {
                    $file_template_section .= $page->getTemplateSection('file_block');
                }
                $file_caption = $recordSet->fields($type . 'sfiles_caption');
                $file_filename = $recordSet->fields($type . 'sfiles_file_name');
                $file_id = $recordSet->fields($type . 'sfiles_id');
                $file_url = $file_view_path . '/' . $folderid . '/' . $file_filename;
                $file_download_url = 'index.php?action=create_download&amp;ID=' . $folderid . '&amp;file_id=' . $file_id . '&amp;type=' . $type;
                $file_description = urldecode($recordSet->fields($type . 'sfiles_description'));
                $file_icon_height = $this->config['file_icon_height'];
                $file_icon_width = $this->config['file_icon_width'];
                $file_icon = $this->config['listings_view_file_icons_path'] . '/default.png';
                $file_filesize = 0;
                if ($file_filename != '' && file_exists("$file_upload_path/$folderid/$file_filename")) {
                    $ext = substr(strrchr($file_filename, '.'), 1);
                    $filesize = filesize($file_upload_path . '/' . $folderid . '/' . $file_filename);

                    $iconpath = $this->config['file_icons_path'] . '/' . $ext . '.png';
                    if (file_exists($iconpath)) {
                        $file_icon = $this->config['listings_view_file_icons_path'] . '/' . $ext . '.png';
                    }
                    $file_filesize = $this->byteSize($filesize);
                }
                $file_template_section = $page->parseTemplateSection($file_template_section, 'file_url', $file_url);
                $file_template_section = $page->parseTemplateSection($file_template_section, 'file_download_url', $file_download_url);
                $file_template_section = $page->parseTemplateSection($file_template_section, 'file_filename', $file_filename);
                $file_template_section = $page->parseTemplateSection($file_template_section, 'file_caption', $file_caption);
                $file_template_section = $page->parseTemplateSection($file_template_section, 'file_description', $file_description);
                $file_template_section = $page->parseTemplateSection($file_template_section, 'file_icon', $file_icon);
                $file_template_section = $page->parseTemplateSection($file_template_section, 'file_icon_height', (string)$file_icon_height);
                $file_template_section = $page->parseTemplateSection($file_template_section, 'file_icon_width', (string)$file_icon_width);
                $file_template_section = $page->parseTemplateSection($file_template_section, 'file_filesize', (string)$file_filesize);
                $recordSet->MoveNext();
                if ($uses_rows) {
                    $x++;
                }
            }
            //END while (!$recordSet->EOF)
            if ($uses_rows) {
                $file_template_section = $page->cleanupTemplateBlock('file', $file_template_section);
                $new_row_data[] = $page->replaceTemplateSection('file_block', $file_template_section, $row);
                $replace_row = implode('', $new_row_data);
                $page->replaceTemplateSectionRow('file_block_row', $replace_row);
            } else {
                $page->replaceTemplateSection('file_block', $file_template_section);
            }
            $page->replacePermissionTags();
            $display .= $page->returnPage();
        }
        return $display;
    }

    //Create Download Function to prevent direct links to files
    #[NoReturn] public function createDownload(int $ID, int $file_id, string $type)
    {
        global $ORconn;

        $misc = $this->newMisc();

        if ($type == 'listing') {
            $file_upload_path = $this->config['listings_file_upload_path'];
            $sqltype = 'listings';
        } else {
            $file_upload_path = $this->config['users_file_upload_path'];
            $sqltype = 'user';
        }
        $sql = 'SELECT DISTINCT ' . $type . 'sfiles_file_name 
				FROM ' . $this->config['table_prefix'] . $type . 'sfiles 
				WHERE (' . $sqltype . 'db_id = ' . $ID . ') 
				AND (' . $type . 'sfiles_id = ' . $file_id . ') 
				ORDER BY ' . $type . 'sfiles_rank';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        if ($recordSet->RecordCount() == 1) {
            $file_filename = $recordSet->fields($type . 'sfiles_file_name');


            $fullPath = $file_upload_path . '/' . $ID . '/' . $file_filename;

            if ($fd = fopen($fullPath, 'r')) {
                $fsize = filesize($fullPath);
                $path_parts = pathinfo($fullPath);
                header('Content-type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . $path_parts['basename'] . '"');
                header("Content-length: $fsize");
                header('Cache-control: private'); //use this to open files directly
                while (!feof($fd)) {
                    $buffer = fread($fd, 2048);
                    echo $buffer;
                    ob_flush();
                    flush();
                }
            }
            fclose($fd);
        }
        exit;
    }
}
