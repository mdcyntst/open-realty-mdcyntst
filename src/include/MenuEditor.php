<?php

declare(strict_types=1);

namespace OpenRealty;

use Exception;
use OpenRealty\Api\Commands\MenuApi;

class MenuEditor extends BaseClass
{
    public function ajaxGetMenus(): string
    {
        $menu_api = $this->newMenuApi();
        header('Content-Type: application/json; charset=utf-8');
        try {
            return json_encode($menu_api->metadata([])) ?: '';
        } catch (Exception $e) {
            http_response_code(500);
            return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
        }
    }

    public function ajaxGetMenuItems(int $menu_id): string
    {
        global $lang;
        $misc = $this->newMisc();
        $menu_api = $this->newMenuApi();
        header('Content-Type: application/json; charset=utf-8');
        if (!isset($_POST['token']) || (is_string($_POST['token']) && !$misc->validateCsrfTokenAjax($_POST['token']))) {
            return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
        }
        try {
            $results = $menu_api->read(['menu_id' => $menu_id]);
            return json_encode($results) ?: '';
        } catch (Exception $e) {
            http_response_code(500);
            return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
        }
    }

    public function ajaxGetMenuItemDetails(int $item_id): string
    {
        header('Content-Type: application/json; charset=utf-8');
        $menu_api = $this->newMenuApi();
        try {
            return json_encode($menu_api->itemDetails(['item_id' => $item_id])) ?: '';
        } catch (Exception $e) {
            http_response_code(500);
            return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
        }
    }

    public function ajaxSetMenuOrder(int $menu_id, array $menu_items): string
    {
        header('Content-Type: application/json; charset=utf-8');
        $menu_api = $this->newMenuApi();
        //Validate Menu Items Array we recieved
        $valid_menu_items = [];
        foreach ($menu_items as $i => $a) {
            if (is_int($i) && isset($a['order'], $a['parent']) && is_int($a['order']) && is_int($a['parent'])) {
                $valid_menu_items[$i] = [
                    'order' => $a['order'],
                    'parent' => $a['parent'],
                ];
            }
        }
        try {
            /** @psalm-suppress ArgumentTypeCoercion  Appears to be a bug that I can not reproduce on the pslam.dev site. Will retest after 5.5.0 release */
            return json_encode($menu_api->setMenuOrder(['menu_id' => $menu_id, 'menu_items' => $valid_menu_items])) ?: '';
        } catch (Exception $e) {
            http_response_code(500);
            return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
        }
    }

    public function ajaxSaveMenuItem(int $item_id, string $item_name, int $item_type, string $item_value, string $item_target, string $item_class, bool $visible_guest, bool $visible_member, bool $visible_agent, bool $visible_admin): string
    {
        header('Content-Type: application/json; charset=utf-8');
        $menu_api = $this->newMenuApi();
        try {
            return json_encode($menu_api->saveMenuItem([
                'item_id' => $item_id, 'item_name' => $item_name, 'item_type' => $item_type, 'item_value' => $item_value,
                'item_target' => $item_target, 'item_class' => $item_class, 'visible_guest' => $visible_guest, 'visible_member' => $visible_member, 'visible_agent' => $visible_agent, 'visible_admin' => $visible_admin,
            ]));
        } catch (Exception $e) {
            http_response_code(500);
            return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
        }
    }

    public function ajaxAddMenuItem(int $menu_id, string $item_name, int $parent_id): string
    {
        global $lang;
        $misc = $this->newMisc();
        header('Content-Type: application/json; charset=utf-8');
        if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
            return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
        }
        $menu_api = $this->newMenuApi();
        try {
            return json_encode($menu_api->addMenuItem(['menu_id' => $menu_id, 'item_name' => $item_name, 'parent_id' => $parent_id])) ?: '';
        } catch (Exception $e) {
            http_response_code(500);
            return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
        }
    }

    public function ajaxDeleteMenuItem(int $item_id): string
    {
        header('Content-Type: application/json; charset=utf-8');
        $menu_api = $this->newMenuApi();
        try {
            return json_encode($menu_api->deleteMenuItem(['item_id' => $item_id])) ?: '';
        } catch (Exception $e) {
            http_response_code(500);
            return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
        }
    }

    public function ajaxDeleteMenu(int $menu_id): string
    {
        header('Content-Type: application/json; charset=utf-8');
        $menu_api = $this->newMenuApi();
        try {
            return json_encode($menu_api->delete(['menu_id' => $menu_id])) ?: '';
        } catch (Exception $e) {
            http_response_code(500);
            return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
        }
    }

    public function ajaxCreateMenu(string $menu_name): string
    {
        global $lang;
        $misc = $this->newMisc();
        header('Content-Type: application/json; charset=utf-8');
        $menu_api = $this->newMenuApi();
        if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
            return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
        }
        try {
            return json_encode($menu_api->create(['menu_name' => $menu_name])) ?: '';
        } catch (Exception $e) {
            http_response_code(500);
            return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
        }
    }

    public function renderMenu(int $menu_id): string
    {
        $misc = $this->newMisc();
        $menu_api = $this->newMenuApi();
        try {
            $api_result = $menu_api->read(['menu_id' => $menu_id]);
            return '<ul class="or_menu" id="or_menu_' . $menu_id . '">' . $this->buildMenuHtml($api_result['menu'], 0) . '</ul>';
        } catch (Exception $e) {
            $misc->logErrorAndDie($e->getMessage());
        }
    }

    private function buildMenuHtml(array $menuarray, int $parent_id): string
    {
        $misc = $this->newMisc();
        $login = $this->newLogin();
        $page = $this->newPageUser();
        $menu = '';
        if (isset($menuarray[$parent_id])) {
            foreach ($menuarray[$parent_id] as $menu_item) {
                $item_id = $menu_item['item_id'];
                $item_name = $menu_item['item_name'];
                $item_name = htmlentities($item_name, ENT_COMPAT, $this->config['charset']);
                $item_type = $menu_item['item_type'];
                $item_target = $menu_item['item_target'];
                $item_value = $menu_item['item_value'];
                $item_class = $menu_item['item_class'];
                $item_types = [1 => 'action', 2 => 'page', 3 => 'custom', 4 => 'divider', '5' => 'block', 6 => 'blog'];
                //Get Users permission Levels
                $is_agent = $login->verifyPriv('Agent');
                $is_admin = $login->verifyPriv('Admin');
                $is_member = $login->verifyPriv('Member');
                //Get Items Visible States
                $visible_guest = $menu_item['visible_guest'];
                $visible_member = $menu_item['visible_member'];
                $visible_agent = $menu_item['visible_agent'];
                $visible_admin = $menu_item['visible_admin'];
                $visible = false;
                if (!$is_member && !$is_agent && !$is_admin && $visible_guest) {
                    $visible = true;
                } elseif ($is_member && !$is_agent && !$is_admin && $visible_member) {
                    $visible = true;
                } elseif ($is_agent && !$is_admin && $visible_agent) {
                    $visible = true;
                } elseif ($is_admin && $visible_admin) {
                    $visible = true;
                }
                if (!$visible) {
                    continue;
                }
                //Build URL..
                $url = '';
                switch ($item_type) {
                    case 1:
                        //Action
                        if ($item_value != '') {
                            $url = '{' . $item_value . '}';
                        } else {
                            $url = '#';
                        }
                        break;
                    case 2:
                        //Page
                        if ($item_value > 0) {
                            $url = '{page_link_' . intval($item_value) . '}';
                        } else {
                            $url = '#';
                        }
                        break;
                    case 6:
                        //Page
                        if ($item_value > 0) {
                            $url = '{blog_link_' . intval($item_value) . '}';
                        } else {
                            $url = '#';
                        }
                        break;
                    case 3:
                        //Custom
                        $url = htmlentities($item_value, ENT_COMPAT, $this->config['charset']);
                        //                        $title = htmlentities($item_name, ENT_COMPAT, $this->config['charset']);
                        //                        $menu .= '<li class="or_menu_item or_menu_item_type_' . $item_types[$item_type] . '"><a href="' . $url . '" class="' . $item_class . '" title="' . $title . '">' . $title . '</a></li>';
                        break;
                    case 4:
                        //Divider
                        if (isset($menuarray[$item_id])) {
                            $menu .= '<li class="or_menu_item or_menu_parent or_menu_item_type_' . $item_types[$item_type] . '" id="or_menu_item_' . $item_id . '">' . $item_name . '<ul>';
                            $menu .= $this->buildMenuHtml($menuarray, $item_id);
                            $menu .= '</ul></li>';
                        } else {
                            $menu .= '<li class="or_menu_item or_menu_item_type_' . $item_types[$item_type] . '" id="or_menu_item_' . $item_id . '">' . $item_name . '</li>';
                        }
                        continue (2);
                    case 5:
                        switch ($item_value) {
                            case 'blog_recent_comments_block':
                                $menu .= '{blog_recent_comments_block}
                                <li class="or_menu_item or_menu_item_type_' . $item_types[$item_type] . '"><a href="{blog_recent_comments_url}" class="' . $item_class . '" title="{blog_recent_comments_title}">{blog_recent_comments_title}</a></li>
                                {/blog_recent_comments_block}';
                                break;
                            case 'blog_recent_post_block':
                                $menu .= '{blog_recent_post_block}
                            <li class="or_menu_item or_menu_item_type_' . $item_types[$item_type] . '"><a href="{blog_recent_post_url}" class="' . $item_class . '" title="{blog_recent_post_title}">{blog_recent_post_title}</a></li>
                            {/blog_recent_post_block}';
                                break;
                            case 'blog_archive_link_block':
                                $menu .= '{blog_archive_link_block}
                                <li class="or_menu_item or_menu_item_type_' . $item_types[$item_type] . '"><a href="{blog_archive_url}" class="' . $item_class . '" title="{blog_archive_title}">{blog_archive_title}</a></li>
                            {/blog_archive_link_block}';
                                break;
                            case 'blog_category_link_block':
                                $menu .= '{blog_category_link_block}
                                <li class="or_menu_item or_menu_item_type_' . $item_types[$item_type] . '"><a href="{blog_cat_url}" class="' . $item_class . '" title="{blog_cat_title}">{blog_cat_title}</a></li>
                            {/blog_category_link_block}';
                                break;
                            case 'pclass_searchlinks_block':
                                $pclass_api = $this->newPClassApi();
                                try {
                                    $class_metadata = $pclass_api->metadata();
                                } catch (Exception $e) {
                                    $misc->logErrorAndDie($e->getMessage());
                                }
                                $keys = array_keys($class_metadata['metadata']);
                                foreach ($keys as $class_id) {
                                    $menu .= '<li class="or_menu_item or_menu_item_type_' . $item_types[$item_type] . '"><a href="' . $this->config['baseurl'] . '/index.php?action=search_step_2&amp;pclass%5B%5D=' . $class_id . '" class="' . $item_class . '" title="' . $class_metadata['metadata'][$class_id]['name'] . '">' . $class_metadata['metadata'][$class_id]['name'] . '</a></li>';
                                }
                                break;
                            default:
                                break;
                        }
                        continue (2);
                }
                if (isset($menuarray[$item_id])) {
                    $menu .= '<li class="or_menu_item or_menu_parent or_menu_item_type_' . $item_types[$item_type] . '" id="or_menu_item_' . $item_id . '"><a href="' . $url . '" title="' . $item_name . '" class="' . $item_class . '" target="' . $item_target . '">' . $item_name . '</a><ul>';
                    $menu .= $this->buildMenuHtml($menuarray, $item_id);
                    $menu .= '</ul></li>';
                } else {
                    $menu .= '<li class="or_menu_item or_menu_item_type_' . $item_types[$item_type] . '" id="or_menu_item_' . $item_id . '"><a href="' . $url . '" title="' . $item_name . '" class="' . $item_class . '"  target="' . $item_target . '">' . $item_name . '</a></li>';
                }
            }
        }

        $page->page = $menu;
        $page->replaceUrls();
        $page->replaceBlogTemplateTags();
        $page->autoReplaceTags();
        $page->replaceLangTemplateTags();
        return $page->returnPage();
    }

    public function showEditor(): string
    {
        $login = $this->newLogin();
        $security = $login->verifyPriv('editpages');
        $display = '';
        if ($security === true) {
            //Load the Core Template
            $page = $this->newPageAdmin();

            //Load Template File
            $page->loadPage($this->config['admin_template_path'] . '/menu_editor_index.html');

            //We are done finish output
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            $display .= $page->returnPage();
        } else {
            $display = '{lang_emenu_permission_denied}';
        }

        return $display;
    }
}
