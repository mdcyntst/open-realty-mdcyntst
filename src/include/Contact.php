<?php

declare(strict_types=1);

namespace OpenRealty;

/**
 * contact
 * This class contains all functions related to contacting people agents and friends about listings.
 *
 * @author Ryan Bonham
 */
class Contact extends BaseClass
{
    private function getContactAgentID(int $listing_id): int
    {
        global $ORconn;

        $misc = $this->newMisc();
        $listing_pages = $this->newListingPages();
        $sql_listing_id = $listing_id;
        $agent_id = (int)$listing_pages->getListingAgentValue('userdb_id', $sql_listing_id);
        //Check to see if agent's notifications should be redirected to the floor agent.
        $sql = 'SELECT userdb_send_notifications_to_floor FROM ' . $this->config['table_prefix'] . 'userdb WHERE userdb_id = ' . $agent_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $userdb_send_notifications_to_floor = (int)$recordSet->fields('userdb_send_notifications_to_floor');
        if ($userdb_send_notifications_to_floor == 1) {
            //Get Floor Agent ID
            $sql = 'SELECT controlpanel_floor_agent, controlpanel_floor_agent_last 
                    FROM ' . $this->config['table_prefix_no_lang'] . 'controlpanel';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $controlpanel_floor_agent = $recordSet->fields('controlpanel_floor_agent');
            $controlpanel_floor_agent_last = (int)$recordSet->fields('controlpanel_floor_agent_last');
            $floor_agents = array_map('intval', explode(',', $controlpanel_floor_agent));
            if (count($floor_agents) > 1) {
                $update_floor = false;
                //Determine Last Floor Agent
                if ($controlpanel_floor_agent_last > 1) {
                    $use_next = false;
                    $found_agent = false;
                    foreach ($floor_agents as $floor_id) {
                        if (!$use_next) {
                            if ($floor_id == $controlpanel_floor_agent_last) {
                                $use_next = true;
                            }
                        } else {
                            $new_agent_id = $floor_id;
                            if ($new_agent_id > 0) {
                                $found_agent = true;
                                $update_floor = true;
                                $agent_id = $new_agent_id;
                            }
                        }
                    }
                    if (!$found_agent) {
                        //No Last Agent set so use the first in list
                        $new_agent_id = $floor_agents[0];
                        if ($new_agent_id > 0) {
                            $update_floor = true;
                            $agent_id = $new_agent_id;
                        }
                    }
                } else {
                    //No Last Agent set so use the first in list
                    $new_agent_id = $floor_agents[0];
                    if ($new_agent_id > 0) {
                        $update_floor = true;
                        $agent_id = $new_agent_id;
                    }
                }
                if ($update_floor) {
                    $sql = 'UPDATE ' . $this->config['table_prefix_no_lang'] . 'controlpanel SET controlpanel_floor_agent_last = ' . $agent_id;
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                }
            } else {
                $new_agent_id = $floor_agents[0];
                if ($new_agent_id > 0) {
                    $agent_id = $new_agent_id;
                }
            }
        }
        return $agent_id;
    }

    public function contactAgentForm(int $listing_id = 0, int $agent_id = 0): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $page = $this->newPageUser();
        $login = $this->newLogin();
        $captcha = $this->newCaptcha();

        $lead_functions = $this->newLeadFunctions();
        $sql_listing_id = $listing_id;
        $sql_agent_id = $agent_id;
        $display = '';


        $extra_url = '';
        if ($sql_listing_id != 0) {
            $extra_url .= '&amp;listing_id=' . $sql_listing_id;
        }
        if (isset($_GET['popup']) && $_GET['popup'] == 'yes') {
            $extra_url .= '&amp;popup=yes';
        }
        if ($sql_agent_id != 0) {
            $extra_url .= '&amp;agent_id=' . $sql_agent_id;
        }

        $is_member = $login->loginCheck('Member');
        if ($is_member === true) {
            //User is logged in, so lets load the contact form
            $page->loadPage($this->config['template_path'] . '/' . $this->config['contact_template']);
            if (isset($_POST['formaction']) && $_POST['formaction'] == 'create_new_lead') {
                //Check CSRF token
                if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfToken($_POST['token'])) {
                    $correct_token = false;
                } else {
                    $correct_token = true;
                }
                //Check Captcha
                $correct_captcha = $captcha->validate();
                if (!$correct_token || !$correct_captcha) {
                    if (!$correct_captcha) {
                        $page->page = $page->cleanupTemplateBlock('user_captcha_failed', $page->page);
                    }
                    if (!$correct_token) {
                        $page->page = $page->cleanupTemplateBlock('invalid_csrf_token', $page->page);
                    }
                } else {
                    //Form Submittion is valid, so lets process the form
                    if ($listing_id > 0) {
                        $sql_agent_id = $this->getContactAgentID($listing_id);
                    }
                    $sql_member_id = intval($_SESSION['userID']);

                    $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "feedbackdb (feedbackdb_notes, userdb_id, listingdb_id, feedbackdb_creation_date, feedbackdb_last_modified, feedbackdb_status, feedbackdb_priority,feedbackdb_member_userdb_id )
                            VALUES ('', " . $sql_agent_id . ", " . $sql_listing_id . ", " . $ORconn->DBTimeStamp(time()) . ',' . $ORconn->DBTimeStamp(time()) . ", 1, 'Normal',$sql_member_id) ";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $new_feedback_id = $ORconn->Insert_ID();
                    $message = $lead_functions->updateFeedbackData($new_feedback_id, $agent_id);
                    if ($message == 'success') {
                        //get the Agent's full name & email
                        $sql = 'SELECT userdb_user_first_name, userdb_user_last_name, userdb_emailaddress
                                    FROM ' . $this->config['table_prefix'] . "userdb
                                    WHERE userdb_id = $sql_agent_id";
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            $misc->logErrorAndDie($sql);
                        }
                        $user_name = $recordSet->fields('userdb_user_first_name') . ' ' . $recordSet->fields('userdb_user_last_name');

                        $misc->logAction("Created feedback $new_feedback_id for $user_name");
                        //notifyNewFeedback($new_feedback_id);
                        $lead_functions->sendAgentFeedbackNotice($new_feedback_id);
                        $lead_functions->sendUserFeedbackNotice($new_feedback_id);

                        $hooks = $this->newHooks();
                        $hooks->load('afterLeadCreate', $new_feedback_id);
                        // Report that Feedback has been sent to the AGENT
                        return '<div id="feedback_sent_message">' . $lang['your_feedback_has_been_sent'] . ' ' . $user_name . '</div>';
                    } else {
                        return '<p>There\'s been a problem -- please contact the site administrator</p>';
                    } // end else
                }
            }
            $page->page = $page->removeTemplateBlock('user_captcha_failed', $page->page);
            $page->page = $page->removeTemplateBlock('invalid_csrf_token', $page->page);
            $page->page = $page->removeTemplateBlock('misc_signup_error', $page->page);
            $page->page = $page->removeTemplateBlock('user_email_activation', $page->page);
            $page->page = $page->removeTemplateBlock('user_moderation', $page->page);

            $page->replacePermissionTags();
            $page->replaceUrls();
            if ($listing_id > 0) {
                $page->replaceListingFieldTags($listing_id);
                $page->replaceTag('listing_id', (string)$listing_id);
                $page->page = $page->removeTemplateBlock('agent_contact', $page->page);
                $page->page = $page->cleanupTemplateBlock('listing_contact', $page->page);
            } elseif ($sql_agent_id > 0) {
                $page->replaceUserFieldTags($sql_agent_id, '', 'listing_agent');
                $page->replaceTag('agent_id', (string)$sql_agent_id);
                $page->page = $page->cleanupTemplateBlock('agent_contact', $page->page);
                $page->page = $page->removeTemplateBlock('listing_contact', $page->page);
            }
            $page->replaceTag('extra_url', $extra_url);
            $lead_fields = $lead_functions->getFeedbackFormelements();
            $page->replaceTag('feedback_formelements', $lead_fields);


            $page->replaceUserFieldTags($_SESSION['userID']);
            $page->replaceTag('captcha_display', $captcha->show());
            $page->autoReplaceTags();
            $page->replaceLangTemplateTags();

            $display = $page->returnPage();
        }
        return $display;
    }

    /**
     * Contact::contactFriendForm()
     *
     * @param integer $listing_id This should hold the listing ID that you aer emailing your friend about.
     * @return
     */
    public function contactFriendForm(int $listing_id): string
    {
        global $lang, $jscript;

        $misc = $this->newMisc();
        $user_manager = $this->newUserManagement();
        $login = $this->newLogin();

        $captcha = $this->newCaptcha();
        $user = $this->newUser();

        $page = $this->newPageUser();
        $page->loadPage($this->config['template_path'] . '/email_a_friend.html');
        $page->replaceLangTemplateTags();
        $page->replaceListingFieldTags($listing_id);

        $display = '';
        $error = [];
        if (isset($_POST['user_name']) && isset($_POST['user_pass'])) {
            $login_passed = $login->loginCheck('Member');
            if ($login_passed !== true) {
                $page->page = $page->cleanupTemplateBlock('login_failed', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('login_failed', $page->page);
            }
        }
        $is_member = $login->verifyPriv('Member');

        if ($is_member) {
            if (
                isset($_POST['message'], $_POST['subject'], $_POST['friend_email'])
                && is_string($_POST['message'])
                && is_string($_POST['subject'])
                && is_string($_POST['friend_email'])
            ) {
                // Make sure there is a message
                if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfToken($_POST['token'])) {
                    $correct_token = false;
                } else {
                    $correct_token = true;
                }
                if (!$correct_token) {
                    $page->page = $page->cleanupTemplateBlock('invalid_csrf_token', $page->page);
                } else {
                    $correct_code = $captcha->validate();
                    if (!$correct_code) {
                        $error[] = 'email_verification_code_not_valid';
                    }

                    if (trim($_POST['friend_email']) == '') {
                        $error[] = 'email_no_email_address';
                    } elseif ($misc->validateEmail($_POST['friend_email']) !== true) {
                        $error[] = 'email_invalid_email_address';
                    }
                    if (trim($_POST['subject']) == '') {
                        $error[] = 'email_no_subject';
                    }
                    if (trim($_POST['message']) == '') {
                        $error[] = 'email_no_message';
                    }

                    if (count($error) == 0) {
                        // Send Mail
                        //Get Member Info
                        $member_name = $user->getUserSingleItem('userdb_user_first_name', $_SESSION['userID']) . ' ' . $user->getUserSingleItem('userdb_user_last_name', $_SESSION['userID']);
                        $member_email = $user->getUserSingleItem('userdb_emailaddress', $_SESSION['userID']);
                        $sent = $misc->sendEmail($member_name, $member_email, $_POST['friend_email'], $_POST['message'], $_POST['subject']);
                        if ($sent === true) {
                            $display .= $lang['email_listing_sent'] . ' ' . $_POST['friend_email'];
                        } else {
                            if (is_string($sent)) {
                                $display .= $sent;
                            } else {
                                $display .= '<div class="error_text">Unkown Error Sending Email</div>';
                            }
                        }
                    } else {
                        foreach ($error as $err) {
                            $display .= '<div class="error_text">' . $lang[$err] . '</div>';
                        }
                    }
                }
            }

            $name = '';
            $email = '';
            $friend_email = '';
            if (
                isset($_POST['message'], $_POST['subject'], $_POST['friend_email'])
                && is_string($_POST['message'])
                && is_string($_POST['subject'])
                && is_string($_POST['friend_email'])
            ) {
                $message = stripslashes($_POST['message']);
                $subject = stripslashes($_POST['subject']);
                $friend_email = stripslashes($_POST['friend_email']);
                $page->page = $page->removeTemplateBlock('default_subject', $page->page);
                $page->page = $page->removeTemplateBlock('default_message', $page->page);
                $page->page = $page->removeTemplateBlock('invalid_csrf_token', $page->page);
            } else {
                $subject = $page->getTemplateSection('default_subject_block');
                $page->page = $page->removeTemplateBlock('default_subject', $page->page);
                $message = $page->getTemplateSection('default_message_block');
                $page->page = $page->removeTemplateBlock('default_message', $page->page);
                $page->page = $page->removeTemplateBlock('invalid_csrf_token', $page->page);
            }

            $page->replaceTag('captcha_display', $captcha->show());

            $page->replaceTag('message', htmlentities($message, ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('name', htmlentities($name, ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('email', htmlentities($email, ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('friend_email', htmlentities($friend_email, ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('subject', htmlentities($subject, ENT_COMPAT, $this->config['charset']));
        } else {
            if (isset($_POST['edit_user_name'])) {
                if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
                    $correct_token = false;
                } else {
                    $correct_token = true;
                }
                if (!$correct_token) {
                    $page->page = $page->cleanupTemplateBlock('invalid_csrf_token', $page->page);
                    $page->page = $page->removeTemplateBlock('user_captcha_failed', $page->page);
                    $page->page = $page->removeTemplateBlock('misc_signup_error', $page->page);
                    $page->page = $page->removeTemplateBlock('user_email_activation', $page->page);
                    $page->page = $page->removeTemplateBlock('user_moderation', $page->page);
                    $page->page = $page->removeTemplateBlock('invalid_csrf_token', $page->page);
                } else {
                    $page->page = $page->removeTemplateBlock('invalid_csrf_token', $page->page);


                    if ($this->config['use_signup_image_verification']) {
                        $correct_code = $captcha->validate();
                    } else {
                        $correct_code = true;
                    }

                    if (!$correct_code) {
                        //Failed
                        //echo 'Incorrect Code';
                        $page->page = $page->cleanupTemplateBlock('user_captcha_failed', $page->page);
                        $page->page = $page->removeTemplateBlock('misc_signup_error', $page->page);
                        $page->page = $page->removeTemplateBlock('user_email_activation', $page->page);
                        $page->page = $page->removeTemplateBlock('user_moderation', $page->page);
                    } else {
                        //echo 'Correct Code';
                        $page->page = $page->removeTemplateBlock('user_captcha_failed', $page->page);
                        $user_signup_status = $user_manager->memberCreation();
                        //print_r($user_signup_status);
                        if (is_array($user_signup_status)) {
                            //User was signed up.. CHeck to see if they were moderated.
                            $user_active = $user_signup_status['active'];
                            if ($user_active == 'mod') {
                                $page->page = $page->cleanupTemplateBlock('user_moderation', $page->page);
                            } else {
                                $page->page = $page->removeTemplateBlock('user_moderation', $page->page);
                            }
                            if ($user_active == 'email') {
                                $page->page = $page->cleanupTemplateBlock('user_email_activation', $page->page);
                            } else {
                                $page->page = $page->removeTemplateBlock('user_email_activation', $page->page);
                            }
                            if ($user_active == 'yes') {
                                $login->loginCheck('Member');
                            }
                            $page->page = $page->removeTemplateBlock('misc_signup_error', $page->page);
                        } else {
                            $page->replaceTag('misc_signup_error_msg', $user_signup_status);
                            $page->page = $page->cleanupTemplateBlock('misc_signup_error', $page->page);
                            $page->page = $page->removeTemplateBlock('user_email_activation', $page->page);
                            $page->page = $page->removeTemplateBlock('user_moderation', $page->page);
                            if (is_string($_POST['user_email'])) {
                                $page->replaceTag('user_email', htmlentities($_POST['user_email'], ENT_NOQUOTES, $this->config['charset']));
                            }
                            if (is_string($_POST['user_last_name'])) {
                                $page->replaceTag('user_last_name', htmlentities($_POST['user_last_name'], ENT_NOQUOTES, $this->config['charset']));
                            }
                            if (is_string($_POST['user_first_name'])) {
                                $page->replaceTag('user_first_name', htmlentities($_POST['user_first_name'], ENT_NOQUOTES, $this->config['charset']));
                            }
                            if (is_string($_POST['edit_user_name'])) {
                                $page->replaceTag('edit_user_name', htmlentities($_POST['edit_user_name'], ENT_NOQUOTES, $this->config['charset']));
                            }
                        }
                    }
                }
            } else {
                $page->page = $page->removeTemplateBlock('user_captcha_failed', $page->page);
                $page->page = $page->removeTemplateBlock('misc_signup_error', $page->page);
                $page->page = $page->removeTemplateBlock('user_email_activation', $page->page);
                $page->page = $page->removeTemplateBlock('user_moderation', $page->page);
                $page->page = $page->removeTemplateBlock('invalid_csrf_token', $page->page);
            }

            $jscript .= '<script type="text/javascript">

									$(document).ready(function() {

										$("#signup_section_link").click(function(){
											$("#login_section").hide();
											$("#signup_section").show();
											return false;
										});
										$("#login_section_link").click(function(){
											$("#signup_section").hide();
											$("#login_section").show();
											return false;
										});
									});

			    				</script>';

            $jscript .= "\n";

            $page->page = $page->removeTemplateBlock('login_failed', $page->page);

            if ($this->config['use_signup_image_verification']) {
                $captcha = $this->newCaptcha();
                $page->replaceTag('captcha_display', $captcha->show());
            } else {
                $page->replaceTag('captcha_display', '');
            }
        }
        $page->replaceLangTemplateTags();
        $subject = $page->getTemplateSection('default_subject_block');
        $page->page = $page->removeTemplateBlock('default_subject', $page->page);
        $message = $page->getTemplateSection('default_message_block');
        $page->page = $page->removeTemplateBlock('default_message', $page->page);
        $page->replaceTag('message', htmlentities($message, ENT_COMPAT, $this->config['charset']));
        $page->replaceTag('subject', htmlentities($subject, ENT_COMPAT, $this->config['charset']));
        $page->replacePermissionTags();
        $page->replaceMetaTemplateTags();
        $page->autoReplaceTags();
        $page->replaceTags(['load_js', 'load_ORjs', 'load_js_last']);
        $page->replaceLangTemplateTags();
        $page->replaceCssTemplateTags();

        $display .= $page->returnPage();
        return $display;
    }
}
