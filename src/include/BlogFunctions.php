<?php

declare(strict_types=1);

namespace OpenRealty;

use Exception;
use PhpXmlRpc\Client;
use PhpXmlRpc\Request;
use PhpXmlRpc\Value;
use SimpleXMLElement;

use function simplexml_load_file;

class BlogFunctions extends BaseClass
{
    /**
     * @return string
     */
    public function ajaxGetBlogs(): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $sql = 'SELECT blogmain_id, blogmain_title 
				FROM ' . $this->config['table_prefix'] . 'blogmain';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $blogs = [];
        while (!$recordSet->EOF) {
            /** @var integer $blog_id */
            $blog_id = $recordSet->fields('blogmain_id');
            /** @var string $title */
            $title = $recordSet->fields('blogmain_title');
            $blogs[$blog_id] = $title;
            $recordSet->Movenext();
        }
        return json_encode(['error' => false, 'blogs' => $blogs]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxGetBlogPost(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');

        if ($security === true) {
            // Do we need to save?
            if (isset($_POST['blogID'])) {
                // Save page now
                $pageID = intval($_POST['blogID']);

                $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . "blogmain 
						WHERE blogmain_id = $pageID";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                if ($recordSet->RecordCount() == 0) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => 'Page Not Found']) ?: '';
                }
                /**
                 * @var integer $id
                 */
                $id = $recordSet->fields('blogmain_id');
                /**
                 * @var string $title
                 */
                $title = $recordSet->fields('blogmain_title');
                /**
                 * @var string $date
                 */
                $date = $recordSet->fields('blogmain_date');
                /**
                 * @var string $full
                 */
                $full = $recordSet->fields('blogmain_full');
                /**
                 * @var integer $published
                 */
                $published = $recordSet->fields('blogmain_published');
                /**
                 * @var string $description
                 */
                $description = $recordSet->fields('blogmain_description');
                /**
                 * @var string $keywords
                 */
                $keywords = $recordSet->fields('blogmain_keywords');
                /**
                 * @var string $full_autosave
                 */
                $full_autosave = $recordSet->fields('blogmain_full_autosave');
                /**
                 * @var string $seotitle
                 */
                $seotitle = $recordSet->fields('blog_seotitle');
                header('Content-type: application/json');

                $full = str_replace('{template_url}', $this->config['template_url'], $full);
                $full = str_replace('{baseurl}', $this->config['baseurl'], $full);

                return json_encode([
                    'error' => '0',
                    'id' => $id,
                    'title' => $title,
                    'date' => $date,
                    'full' => $full,
                    'published' => $published,
                    'description' => $description,
                    'keywords' => $keywords,
                    'full_autosave' => $full_autosave,
                    'seotitle' => $seotitle,
                ]);
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxUpdateBlogPostAutosave(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');
        $blog_user_type = intval($_SESSION['blog_user_type']);
        $blog_user_id = intval($_SESSION['userID']);

        if ($security === true) {
            // Do we need to save?
            if (isset($_POST['ta']) && is_string($_POST['ta']) && isset($_POST['blogID'])) {
                // Save blog now
                $blogID = intval($_POST['blogID']);
                //Verify Blog Owner
                $sql = 'SELECT userdb_id 
						FROM ' . $this->config['table_prefix'] . 'blogmain 
						WHERE blogmain_id = ' . $blogID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $blog_owner = intval($recordSet->fields('userdb_id'));
                //Make sure user is and editor or the blog owner.
                if ($blog_owner != $blog_user_id && $blog_user_type != 4) {
                    //Throw Error
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
                }

                $save_full = $ORconn->addQ($_POST['ta']);

                $sql = 'UPDATE ' . $this->config['table_prefix'] . "blogmain 
						SET blogmain_full_autosave = '$save_full' 
						WHERE blogmain_id = " . $blogID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'blog_id' => $blogID]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxUpdateBlogPost(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');
        $blog_user_type = intval($_SESSION['blog_user_type']);
        $blog_user_id = intval($_SESSION['userID']);

        if ($security === true) {
            $page = $this->newPageAdmin();
            // Do we need to save?
            if (
                isset($_POST['ta'], $_POST['blogID'], $_POST['description'], $_POST['title'], $_POST['keywords'], $_POST['seotitle'])
                && is_string($_POST['description'])
                && is_string($_POST['title'])
                && is_string($_POST['keywords'])
                && is_string($_POST['seotitle'])
                && is_string($_POST['ta'])
            ) {
                // Save blog now
                $blogID = intval($_POST['blogID']);
                //Verify Blog Owner
                $sql = 'SELECT userdb_id FROM ' . $this->config['table_prefix'] . 'blogmain WHERE blogmain_id = ' . $blogID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $blog_owner = intval($recordSet->fields('userdb_id'));
                //Make sure user is and editor or the blog owner.
                if ($blog_owner != $blog_user_id && $blog_user_type != 4) {
                    //Throw Error
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
                }

                $save_full = $_POST['ta'];
                //Replace Paths with template tags
                $save_full_xhtml = str_replace($this->config['template_url'], '{template_url}', $save_full);
                $save_full_xhtml = str_replace($this->config['baseurl'], '{baseurl}', $save_full_xhtml);
                $save_full_xhtml = $ORconn->addQ($save_full_xhtml);
                $save_description = $misc->makeDbSafe($_POST['description']);
                $title = trim($_POST['title']);
                if ($title == '') {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['blog_title_not_blank']]) ?: '';
                }
                $save_title = $misc->makeDbSafe($title);
                //Make sure Blog Title is unique.
                $sql = 'SELECT blogmain_id 
						FROM ' . $this->config['table_prefix'] . "blogmain 
						WHERE STRCMP(blogmain_title,$save_title) = 0 
						AND blogmain_id <> $blogID";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                if ($recordSet->RecordCount() > 0) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => "$lang[blog_title_not_unique]"]) ?: '';
                }

                $save_keywords = $misc->makeDbSafe($_POST['keywords']);
                $seotitle = trim($_POST['seotitle']);
                if ($seotitle == '') {
                    $seotitle = $page->createSeoUri($_POST['title'], false);
                }
                $sql_seotitle = $misc->makeDbSafe($seotitle);
                //Verify the SEO title is unique
                $sql = 'SELECT blogmain_id 
						FROM ' . $this->config['table_prefix'] . 'blogmain 
						WHERE blog_seotitle = ' . $sql_seotitle . ' 
						AND blogmain_id <> ' . $blogID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                if ($recordSet->RecordCount() > 0) {
                    $seotitle = $page->createSeoUri($seotitle . '-' . $blogID, false);
                    $sql_seotitle = $misc->makeDbSafe($seotitle);
                }
                if (isset($_POST['status'])) {
                    $save_status = intval($_POST['status']);
                } else {
                    $save_status = null;
                }
                $current_status = $this->getBlogStatus($blogID);
                // Check to see if user can publish a blog
                if ($blog_user_type == 2 && $save_status == 1) {
                    //Throw Error
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
                }
                $status_sql = '';
                if ($save_status !== null) {
                    $status_sql = ', blogmain_published = ' . $save_status;
                }
                $sql = 'UPDATE ' . $this->config['table_prefix'] . 'blogmain 
						SET blog_seotitle = ' . $sql_seotitle . ", blogmain_full_autosave = '', blogmain_full = '" . $save_full_xhtml . "', blogmain_title = " . $save_title . ', blogmain_description = ' . $save_description . ', blogmain_keywords = ' . $save_keywords . $status_sql . ' 
						WHERE blogmain_id = ' . $blogID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                } else {
                    //See if this is a new publish.
                    //Send Twitter and PingBacks
                    if ($save_status == 1 && $current_status != 1) {
                        if ($this->config['twitter_new_blog']) {
                            $twitter_url = ' ' . $this->config['baseurl'] . '/b/' . $blogID;
                            $twitter_title = $_POST['title'];
                            if (strlen($twitter_url) + strlen($twitter_title) > 140) {
                                $twitter_title = substr($twitter_title, 0, 137 - strlen($twitter_url)) . '...';
                            }
                            $twitter_post = $twitter_title . $twitter_url;
                            $twitter_api = $this->newTwitterApi();
                            try {
                                $twitter_api->post(['message' => $twitter_post]);
                            } catch (Exception $e) {
                                header('Content-type: application/json');
                                return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
                            }
                        }
                        //Pingbacks
                        $article_url = $page->magicURIGenerator('blog', (string)$blogID, true);
                        if ($this->config['send_service_pingbacks']) {
                            $this->pingbackServices($article_url);
                        }
                        //Do Additional Pingback based on article
                        $pingback_url_status = '';
                        if ($this->config['send_url_pingbacks']) {
                            $pingback_url_status .= $this->pingbackUrls($article_url, $save_full);
                        } else {
                            $pingback_url_status = 'Skipping based config';
                        }
                    } else {
                        $pingback_url_status = 'Not Run';
                    }
                    header('Content-type: application/json');
                    return json_encode(['error' => '0', 'blog_id' => $blogID, 'seotitle' => $seotitle, 'pingback_url_status' => $pingback_url_status]) ?: '';
                }
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxDeleteBlogPost(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        //TODO: Fix Security Check
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');

        if ($security === true) {
            // Do we need to save?
            if (isset($_POST['blogID'])) {
                // Save blog now
                $blogID = intval($_POST['blogID']);

                // Check to see if user can delete this post
                /*
                if ($blog_user_type==2 && $save_status == 1){
                //Throw Error
                header('Content-type: application/json');
                return json_encode(array('error' => "1",'error_msg' => $lang['blog_permission_denied'])) ?: '';
                }
                */
                $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'blogmain  WHERE blogmain_id = ' . $blogID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation  WHERE blogmain_id = ' . $blogID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation  WHERE blogmain_id = ' . $blogID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }

                $dir = $this->config['basepath'] . '/images/blog_uploads/' . $blogID;
                if ($dir == $this->config['basepath']) {
                    return json_encode(['error' => '1', 'blog_id' => $blogID . 'No folder present']) ?: '';
                } else {
                    if (!$misc->recurseRmdir($dir)) {
                        return json_encode(['error' => '1', 'blog_id' => $blogID . 'Unable to delete media folder']) ?: '';
                    }
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'blog_id' => $blogID]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxAddAssignedBlogTagById(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');

        if ($security === true) {
            if (isset($_POST['tag_id'])) {
                $tag_id = intval($_POST['tag_id']);
                $blogID = intval($_SESSION['blogID']);
                //Make Sure Tag is not already assigned
                $sql = 'SELECT relation_id 
						FROM ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation 
						WHERE tag_id = ' . $tag_id . ' 
						AND blogmain_id = ' . $blogID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                //Assign Tag
                if ($recordSet->RecordCount() > 0) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['blog_tag_already_assigned']]) ?: '';
                }
                $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . "blogtag_relation
						(tag_id,blogmain_id)
						VALUES ($tag_id,$blogID)";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                //Get Tag Name for json return
                $sql = 'SELECT tag_name FROM ' . $this->config['table_prefix'] . 'blogtags WHERE tag_id = ' . $tag_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $tag_name = $recordSet->fields('tag_name');
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'tag_id' => $tag_id, 'tag_name' => $tag_name]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxRemoveAssignedBlogTag(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');

        if ($security === true) {
            // Do we need to save?
            if (isset($_POST['tag_id'])) {
                $tag_id = intval($_POST['tag_id']);
                $blogID = intval($_SESSION['blogID']);
                $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation 
				WHERE tag_id = ' . $tag_id . ' 
				AND blogmain_id = ' . $blogID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'tag_id' => $tag_id]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxCreateBlogTag(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');

        if ($security === true) {
            $page = $this->newPageAdmin();
            if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
            }
            // Do we need to save?
            if (isset($_POST['title']) && is_string($_POST['title'])) {
                // Save blog now
                $title = trim($_POST['title']);
                $seoname = $page->createSeoUri($title, false);
                $save_seoname = $misc->makeDbSafe($seoname);
                $save_title = $misc->makeDbSafe($title);
                //Make sure Blog Title is unique.
                $sql = 'SELECT tag_id FROM ' . $this->config['table_prefix'] . "blogtags WHERE STRCMP(tag_name,$save_title) = 0";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                //Tag Already Exists just assign it
                if ($recordSet->RecordCount() > 0) {
                    $tag_id = $recordSet->fields('tag_id');
                } else {
                    $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "blogtags
					(tag_name,tag_seoname)
					VALUES ($save_title,$save_seoname)";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $tag_id = $ORconn->Insert_ID();
                }
                $blogID = intval($_SESSION['blogID']);
                //Make sure Blog Title is unique.
                $sql = 'SELECT relation_id FROM ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation WHERE tag_id = ' . $tag_id . ' AND blogmain_id = ' . $blogID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                //Tag Already Exists just assign it
                if ($recordSet->RecordCount() > 0) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['blog_tag_already_assigned']]) ?: '';
                } else {
                    $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . "blogtag_relation
						(tag_id,blogmain_id)
						VALUES ($tag_id,$blogID)";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $rel_id = $ORconn->Insert_ID();
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'tag_id' => $tag_id, 'tag_name' => "$title", 'tag_relid' => "$rel_id", 'tag_seoname' => "$seoname"]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxCreateBlogTagNoAssignment(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');

        if ($security === true) {
            $page = $this->newPageAdmin();
            // Do we need to save?
            if (
                isset($_POST['title'], $_POST['seoname'], $_POST['description'])
                && is_string($_POST['title'])
                && is_string($_POST['seoname'])
                && is_string($_POST['description'])
            ) {
                // Save blog now
                $title = trim($_POST['title']);
                $seoname = trim($_POST['seoname']);
                $description = trim($_POST['description']);
                if ($seoname == '') {
                    $seoname = $page->createSeoUri($title, false);
                }
                $save_seoname = $misc->makeDbSafe($seoname);
                $save_title = $misc->makeDbSafe($title);
                $save_description = $misc->makeDbSafe($description);

                //Make sure Blog Title is unique.
                $sql = 'SELECT tag_id FROM ' . $this->config['table_prefix'] . "blogtags WHERE STRCMP(tag_name,$save_title) = 0";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                //Tag Already Exists just assign it
                if ($recordSet->RecordCount() > 0) {
                    $tag_id = $recordSet->fields('tag_id');
                } else {
                    $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "blogtags
					(tag_name,tag_seoname,tag_description)
					VALUES ($save_title,$save_seoname,$save_description)";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $tag_id = $ORconn->Insert_ID();
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'tag_id' => $tag_id, 'tag_name' => "$title", 'tag_seoname' => "$seoname"]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxGetCategoryInfo()
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');

        if ($security === true) {
            // Do we need to save?
            if (isset($_POST['category_id'])) {
                $cat_id = intval($_POST['category_id']);
                $sql = 'SELECT parent_id, category_id, category_name, category_seoname, category_description 
						FROM ' . $this->config['table_prefix'] . 'blogcategory 
						WHERE category_id = ' . $cat_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                if ($recordSet->RecordCount() == 1) {
                    $parent_id = $recordSet->fields('parent_id');
                    $cat_id = $recordSet->fields('category_id');
                    $cat_name = $recordSet->fields('category_name');
                    $cat_seoname = $recordSet->fields('category_seoname');
                    $cat_description = $recordSet->fields('category_description');
                    $recordSet->MoveNext();
                    header('Content-type: application/json');
                    return json_encode(['error' => '0', 'category_id' => $cat_id, 'category_name' => $cat_name, 'category_seoname' => $cat_seoname, 'category_description' => $cat_description, 'parent_id' => $parent_id]) ?: '';
                }
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => 'No Such Category Found']) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @param int $tag_id
     * @return string
     */
    public function getTagSeoName(int $tag_id): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $sql = 'SELECT tag_seoname FROM ' . $this->config['table_prefix'] . 'blogtags WHERE tag_id = ' . $tag_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        /** @var string $seoname */
        $seoname = $recordSet->fields('tag_seoname');
        return $seoname;
    }

    /**
     * @param int $tag_id
     * @return string
     */
    public function getTagName(int $tag_id): string
    {
        global $ORconn;

        $misc = $this->newMisc();

        $sql = 'SELECT tag_name 
				FROM ' . $this->config['table_prefix'] . 'blogtags 
				WHERE tag_id = ' . $tag_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        /** @var string $tag_name */
        $tag_name = $recordSet->fields('tag_name');
        return $tag_name;
    }

    /**
     * @param int $tag_id
     * @return string
     */
    public function getTagDescription(int $tag_id): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $sql = 'SELECT tag_description 
				FROM ' . $this->config['table_prefix'] . 'blogtags 
				WHERE tag_id = ' . $tag_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        /** @var string $description */
        $description = $recordSet->fields('tag_description');
        return $description;
    }

    /**
     * @return string
     */
    public function ajaxGetTagInfo(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');

        if ($security === true) {
            // Do we need to save?
            if (isset($_POST['tag_id'])) {
                $cat_id = intval($_POST['tag_id']);
                $sql = 'SELECT  tag_id, tag_name, tag_seoname, tag_description 
						FROM ' . $this->config['table_prefix'] . 'blogtags 
						WHERE tag_id = ' . $cat_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                if ($recordSet->RecordCount() == 1) {
                    $cat_id = $recordSet->fields('tag_id');
                    $cat_name = $recordSet->fields('tag_name');
                    $cat_seoname = $recordSet->fields('tag_seoname');
                    $cat_description = $recordSet->fields('tag_description');
                    $recordSet->MoveNext();
                    header('Content-type: application/json');
                    return json_encode(['error' => '0', 'tag_id' => $cat_id, 'tag_name' => $cat_name, 'tag_seoname' => $cat_seoname, 'tag_description' => $cat_description]) ?: '';
                }
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => 'No Such tag Found']) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxUpdateCategoryInfo(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');

        if ($security === true) {
            if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
            }


            $page = $this->newPageAdmin();
            // Do we need to save?
            if (
                isset($_POST['category_id'], $_POST['title'], $_POST['seoname'], $_POST['description'], $_POST['parent'])
                && is_string($_POST['title'])
                && is_string($_POST['seoname'])
                && is_string($_POST['description'])
            ) {
                $cat_id = intval($_POST['category_id']);
                $parent_id = intval($_POST['parent']);
                //Prevent Cat From being it's own parent
                if ($parent_id == $cat_id) {
                    $parent_id = 0;
                }
                $name = strip_tags(trim($_POST['title']));
                $seoname = strip_tags(trim($_POST['seoname']));
                $description = trim($_POST['description']);
                if (trim($seoname) != '') {
                    $seoname = trim($seoname);
                } else {
                    $seoname = $page->createSeoUri($name, false);
                }

                $name = $misc->makeDbSafe($name);
                $seoname = $misc->makeDbSafe($seoname);
                $description = $misc->makeDbSafe($description);
                $sql = 'UPDATE ' . $this->config['table_prefix'] . "blogcategory 
						SET parent_id = $parent_id, category_id = $cat_id, category_name = $name, category_seoname = $seoname, category_description = $description 
						WHERE category_id = " . $cat_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'cat_id' => $cat_id]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxUpdateTagInfo(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');

        if ($security === true) {
            if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
            }


            $page = $this->newPageAdmin();
            // Do we need to save?
            if (
                isset($_POST['tag_id'], $_POST['title'], $_POST['seoname'], $_POST['description'])
                && is_string($_POST['title'])
                && is_string($_POST['seoname'])
                && is_string($_POST['description'])
            ) {
                $tag_id = intval($_POST['tag_id']);
                $name = strip_tags(trim($_POST['title']));
                $seoname = strip_tags(trim($_POST['seoname']));
                $description = trim($_POST['description']);
                if (trim($seoname) != '') {
                    $seoname = trim($seoname);
                } else {
                    $seoname = $page->createSeoUri($seoname, false);
                }

                $name = $misc->makeDbSafe($name);
                $seoname = $misc->makeDbSafe($seoname);
                $description = $misc->makeDbSafe($description);
                $sql = 'UPDATE ' . $this->config['table_prefix'] . "blogtags SET  tag_id = $tag_id, tag_name = $name, tag_seoname = $seoname, tag_description = $description WHERE tag_id = " . $tag_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'tag_id' => $tag_id]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxDeleteBlogCategory(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');

        if ($security === true) {
            // Do we need to save?
            if (isset($_POST['category_id'])) {
                $cat_id = intval($_POST['category_id']);
                //Delete BLog Category Relations
                $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation 
						WHERE category_id = ' . $cat_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }

                //Look For Blogs with no categories
                $sql = 'SELECT blogmain_id FROM ' . $this->config['table_prefix'] . 'blogmain WHERE blogmain_id NOT IN (SELECT DISTINCT(blogmain_id) FROM ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation)';
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }

                while (!$recordSet->EOF) {
                    $fix_id = $recordSet->fields('blogmain_id');
                    $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation (blogmain_id,category_id) VALUES (' . $fix_id . ',1)';
                    $recordSet2 = $ORconn->Execute($sql);
                    if (!$recordSet2) {
                        $misc->logErrorAndDie($sql);
                    }
                    $recordSet->MoveNext();
                }

                $sql = 'UPDATE ' . $this->config['table_prefix'] . 'blogcategory 
						SET parent_id = 0 
						WHERE parent_id = ' . $cat_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'blogcategory 
						WHERE category_id = ' . $cat_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'error_msg' => '']) ?: '';
            } else {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => 'No category_id']) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxDeleteBlogTag(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');

        if ($security === true) {
            // Do we need to save?
            if (isset($_POST['tag_id'])) {
                $tag_id = intval($_POST['tag_id']);
                //Delete BLog tag Relations
                $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation 
						WHERE tag_id = ' . $tag_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'blogtags 
						WHERE tag_id = ' . $tag_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                header('Content-type: applitagion/json');
                return json_encode(['error' => '0', 'error_msg' => '']) ?: '';
            } else {
                header('Content-type: applitagion/json');
                return json_encode(['error' => '1', 'error_msg' => 'No tag_id']) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxRankDownBlogCategory(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');

        if ($security === true) {
            // Do we need to save?
            if (isset($_POST['category_id'])) {
                $cat_id = intval($_POST['category_id']);
                //Get Category Rank
                $sql = 'SELECT category_rank,parent_id 
						FROM ' . $this->config['table_prefix'] . 'blogcategory 
						WHERE category_id = ' . $cat_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $category_rank = $recordSet->fields('category_rank');
                $parent_id = $recordSet->fields('parent_id');
                //Get Next Rank For this Category Group.
                $sql = 'SELECT max(category_rank) as maxrank 
						FROM ' . $this->config['table_prefix'] . 'blogcategory 
						WHERE parent_id = ' . $parent_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $rank = $recordSet->fields('max_rank');
                if ($category_rank == $rank) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => 'Max Can Not Move Down']) ?: '';
                }
                $new_rank = $category_rank + 1;
                $sql = 'UPDATE ' . $this->config['table_prefix'] . "blogcategory 
						SET category_rank = $category_rank 
						WHERE category_rank = $new_rank 
						AND parent_id = $parent_id";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $sql = 'UPDATE ' . $this->config['table_prefix'] . "blogcategory 
						SET category_rank = $new_rank WHERE category_id = $cat_id";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'error_msg' => '']) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxRankUpBlogCategory(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');

        if ($security === true) {
            // Do we need to save?
            if (isset($_POST['category_id'])) {
                $cat_id = intval($_POST['category_id']);
                //Get Category Rank
                $sql = 'SELECT category_rank,parent_id 
						FROM ' . $this->config['table_prefix'] . 'blogcategory 
						WHERE category_id = ' . $cat_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $category_rank = $recordSet->fields('category_rank');
                $parent_id = $recordSet->fields('parent_id');
                if ($category_rank == 0) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => 'Rank 0 Can Not Move Up']) ?: '';
                }
                $new_rank = $category_rank - 1;
                $sql = 'UPDATE ' . $this->config['table_prefix'] . "blogcategory 
						SET category_rank = $category_rank 
						WHERE category_rank = $new_rank 
						AND parent_id = $parent_id";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $sql = 'UPDATE ' . $this->config['table_prefix'] . "blogcategory 
						SET category_rank = $new_rank 
						WHERE category_id = $cat_id";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'error_msg' => '']) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxCreateBlogCategory(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');
        $blog_user_type = intval($_SESSION['blog_user_type']);

        if ($security === true) {
            $page = $this->newPageAdmin();
            if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
            }

            // Do we need to save?
            if (isset($_POST['title']) && is_string($_POST['title'])) {
                // Save blog now
                if (!isset($_POST['parent'])) {
                    $parent = 0;
                } else {
                    $parent = intval($_POST['parent']);
                }
                if ($blog_user_type == 4 || $_SESSION['admin_privs'] == 'yes') {
                    if (isset($_POST['seoname']) && is_string($_POST['seoname']) && trim($_POST['seoname']) != '') {
                        $seotitle = trim($_POST['seoname']);
                    } else {
                        $seotitle = $page->createSeoUri($_POST['title'], false);
                    }
                    if (isset($_POST['description']) && is_string($_POST['description'])) {
                        $cat_desc = trim($_POST['description']);
                    } else {
                        $cat_desc = '';
                    }
                } else {
                    $cat_desc = '';
                    $seotitle = $page->createSeoUri($_POST['title'], false);
                }
                //Get Next Rank For this Category Group.
                $sql = 'SELECT max(category_rank) as maxrank,count(category_id) as post_count 
						FROM ' . $this->config['table_prefix'] . 'blogcategory 
						WHERE parent_id = ' . $parent;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $rank = $recordSet->fields('maxrank');
                $post_count = $recordSet->fields('post_count');
                if ($post_count == 0) {
                    $rank = 0;
                } else {
                    $rank++;
                }
                $title = trim($_POST['title']);

                $save_title = $misc->makeDbSafe($title);
                $sql_seotitle = $misc->makeDbSafe($seotitle);
                $save_description = $misc->makeDbSafe($cat_desc);
                //Make sure Blog Title is unique.
                $sql = 'SELECT category_id 
						FROM ' . $this->config['table_prefix'] . "blogcategory 
						WHERE STRCMP(category_name,$save_title) = 0";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                if ($recordSet->RecordCount() > 0) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => "$lang[blog_category_not_unique]"]) ?: '';
                }
                $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "blogcategory	
						(category_name,category_seoname,category_description,category_rank,parent_id)
						VALUES ($save_title,$sql_seotitle,$save_description,$rank,$parent)";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $blog_id = $ORconn->Insert_ID();
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'cat_id' => $blog_id, 'cat_name' => "$title", 'cat_rank' => "$rank", 'cat_parent' => "$parent"]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxCreateBlogPost(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');

        if ($security === true) {
            $page = $this->newPageAdmin();
            // Do we need to save?
            if (isset($_POST['title']) && is_string($_POST['title'])) {
                if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
                }
                // Save blog now
                $title = trim($_POST['title']);
                $save_title = $misc->makeDbSafe($title);
                //Make sure Blog Title is unique.
                $sql = 'SELECT blogmain_id 
						FROM ' . $this->config['table_prefix'] . "blogmain 
						WHERE STRCMP(blogmain_title,$save_title) = 0";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                if ($recordSet->RecordCount() > 0) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => "$lang[blog_title_not_unique]"]) ?: '';
                }
                $userdb_id = $misc->makeDbSafe($_SESSION['userID']);
                //Generate seo URL
                $seotitle = $page->createSeoUri($title, false);
                $sql_seotitle = $misc->makeDbSafe($seotitle);

                $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "blogmain
				(userdb_id,blogmain_full,blogmain_title,blogmain_date,blogmain_published,blogmain_description,blogmain_keywords,blog_seotitle)
				VALUES ($userdb_id,'',$save_title," . strtotime('now') . ",0,'',''," . $sql_seotitle . ')';
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $blog_id = $ORconn->Insert_ID();
                $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . "blogcategory_relation
				(category_id,blogmain_id)
				VALUES (1,$blog_id)";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                //Verify the SEO title is unique
                $sql = 'SELECT blogmain_id 
						FROM ' . $this->config['table_prefix'] . 'blogmain 
						WHERE blog_seotitle = ' . $sql_seotitle . ' 
						AND blogmain_id <> ' . $blog_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                if ($recordSet->RecordCount() > 0) {
                    $seotitle = $page->createSeoUri($title . '-' . $blog_id, false);
                    $sql_seotitle = $misc->makeDbSafe($seotitle);
                    $sql = 'UPDATE ' . $this->config['table_prefix'] . "blogmain 
							SET blog_seotitle = $sql_seotitle 
							WHERE blogmain_id = " . $blog_id;
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'id' => $blog_id, 'title' => "$title"]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxSetBlogCat(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('can_access_blog_manager');

        if ($security === true) {
            // Do we need to save?
            if (isset($_POST['blog_id']) && isset($_POST['cat_id'])) {
                // Save blog now
                $blog_id = intval($_POST['blog_id']);
                $cat_id = intval($_POST['cat_id']);
                $status = intval($_POST['status']);
                if ($status == 1) {
                    $sql = 'SELECT category_id 
							FROM ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation 
							WHERE blogmain_id = ' . $blog_id . ' 
							AND category_id =' . $cat_id;
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    if ($recordSet->RecordCount() == 0) {
                        $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation 
								(blogmain_id,category_id) 
								VALUES (' . $blog_id . ',' . $cat_id . ')';
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            $misc->logErrorAndDie($sql);
                        }
                    }
                } else {
                    $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation 
							WHERE blogmain_id = ' . $blog_id . ' 
							AND category_id = ' . $cat_id;
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                }
                header('Content-type: application/json');
                return json_encode(['blog_id' => $blog_id, 'cat_id' => $cat_id, 'status' => $status]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['blog_permission_denied']]) ?: '';
    }

    /**
     * @param int $blog_id
     *
     * @return array<int, array{tag_fontsize: integer, tag_link: string, tag_name: string, tag_seoname:
     *               string}>
     */
    public function getBlogTagAssignment(int $blog_id): array
    {
        global $ORconn;

        $misc = $this->newMisc();
        $page = $this->newPageUser();
        //Get Min/Max population.
        $sql = 'SELECT count(tag_id) as population 
				FROM ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation 
				GROUP BY tag_id 
				ORDER BY population';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $min_count = (int)$recordSet->fields('population');
        $recordSet->MoveLast();
        $max_count = (int)$recordSet->fields('population');

        $sql = 'SELECT ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation.tag_id,tag_name,tag_seoname 
				FROM ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation 
				LEFT JOIN ' . $this->config['table_prefix'] . 'blogtags 
				ON ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation.tag_id = ' . $this->config['table_prefix'] . 'blogtags.tag_id 
				WHERE blogmain_id = ' . $blog_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }

        $assigned = [];
        while (!$recordSet->EOF) {
            $tag_id = (int)$recordSet->fields('tag_id');
            $tag_name = (string)$recordSet->fields('tag_name');
            $tag_seoname = (string)$recordSet->fields('tag_seoname');
            //Get Tag LInk
            $tag_link = $page->magicURIGenerator('blog_tag', (string)$tag_id, true);
            //Get Tag Population
            $sql = 'SELECT count(tag_id) as population 
					FROM ' . $this->config['table_prefix_no_lang'] . "blogtag_relation 
					WHERE tag_id = '" . $tag_id . "' 
					GROUP BY tag_id";
            $recordSet2 = $ORconn->Execute($sql);
            if (is_bool($recordSet2)) {
                $misc->logErrorAndDie($sql);
            }
            $tag_population = (int)$recordSet2->fields('population');
            if ($min_count == $max_count) {
                $font_size = 8;
            } else {
                $weight = (log($tag_population) - log($min_count)) / (log($max_count) - log($min_count));
                $font_size = intval(8 + round((22 - 8) * $weight));
            }
            $assigned[$tag_id] = ['tag_name' => $tag_name, 'tag_seoname' => $tag_seoname, 'tag_link' => $tag_link, 'tag_fontsize' => $font_size];
            $recordSet->MoveNext();
        }
        return $assigned;
    }

    /**
     * @param int $blog_id ID of Blog to det categories for.
     *
     * @return int[]
     */
    public function getBlogCategoriesAssignment(int $blog_id): array
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT category_id 
				FROM ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation 
				WHERE blogmain_id = ' . $blog_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $assigned = [];
        while (!$recordSet->EOF) {
            $assigned[] = (int)$recordSet->fields('category_id');
            $recordSet->MoveNext();
        }
        return $assigned;
    }

    /**
     * @param int $blog_id
     * @return array<int, string>
     */
    public function getBlogCategoriesAssignmentNames(int $blog_id): array
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT ' . $this->config['table_prefix'] . 'blogcategory.category_id, category_name 
				FROM ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation, ' . $this->config['table_prefix'] . 'blogcategory 
				WHERE ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation.category_id =  ' . $this->config['table_prefix'] . 'blogcategory.category_id 
				AND blogmain_id = ' . $blog_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $assigned = [];
        while (!$recordSet->EOF) {
            $assigned[(int)$recordSet->fields('category_id')] = (string)$recordSet->fields('category_name');
            $recordSet->MoveNext();
        }
        return $assigned;
    }

    /**
     * @param integer $blog_id
     *
     * @psalm-return string[]
     */
    public function getBlogCategoriesAssignmentSeoNames(int $blog_id): array
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT category_seoname 
				FROM ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation, ' . $this->config['table_prefix'] . 'blogcategory 
				WHERE ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation.category_id =  ' . $this->config['table_prefix'] . 'blogcategory.category_id AND blogmain_id = ' . $blog_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $assigned = [];
        while (!$recordSet->EOF) {
            $assigned[] = (string)$recordSet->fields('category_seoname');
            $recordSet->MoveNext();
        }
        return $assigned;
    }

    /**
     * @param int $cat_id
     * @return int
     */
    public function getBlogCategoryParent(int $cat_id): int
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT parent_id 
				FROM ' . $this->config['table_prefix'] . 'blogcategory 
				WHERE category_id = ' . $cat_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        return (int)$recordSet->fields('parent_id');
    }

    /**
     * @param int $cat_id
     * @return string
     */
    public function getBlogCategorySeoName(int $cat_id): string
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT category_seoname FROM ' . $this->config['table_prefix'] . 'blogcategory WHERE category_id = ' . $cat_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        return (string)$recordSet->fields('category_seoname');
    }

    /**
     * @param int $cat_id
     * @return string
     */
    public function getBlogCategoryName(int $cat_id): string
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT category_name 
				FROM ' . $this->config['table_prefix'] . 'blogcategory 
				WHERE category_id = ' . $cat_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $cat_name = "";
        while (!$recordSet->EOF) {
            $cat_name = (string)$recordSet->fields('category_name');
            $recordSet->MoveNext();
        }
        return $cat_name;
    }

    /**
     * @param int $cat_id
     * @return string
     */
    public function getBlogCategoryDescription(int $cat_id): string
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT category_description 
				FROM ' . $this->config['table_prefix'] . 'blogcategory 
				WHERE category_id = ' . $cat_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $cat_name = "";
        while (!$recordSet->EOF) {
            $cat_name = (string)$recordSet->fields('category_description');
            $recordSet->MoveNext();
        }
        return $cat_name;
    }

    /**
     * @return array<int, string>
     */
    public function getBlogTags(): array
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT tag_id, tag_name 
				FROM ' . $this->config['table_prefix'] . 'blogtags';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $tags = [];
        while (!$recordSet->EOF) {
            $tag_id = (int)$recordSet->fields('tag_id');
            $tag_name = (string)$recordSet->fields('tag_name');
            $tags[$tag_id] = $tag_name;
            $recordSet->MoveNext();
        }
        return $tags;
    }

    /**
     * Get Array of blog categories id and name, ordered by rank.
     *
     * @return array<int, string>
     */
    public function getBlogCategoriesFlat(): array
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT category_id, category_name 
				FROM ' . $this->config['table_prefix'] . 'blogcategory 
				ORDER BY category_rank';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $categories = [];
        while (!$recordSet->EOF) {
            $id = (int)$recordSet->fields('category_id');
            $name = (string)$recordSet->fields('category_name');
            $categories[$id] = $name;
            $recordSet->MoveNext();
        }
        return $categories;
    }

    /**
     *
     * @return array<int, array<int, string>>
     */
    public function getBlogCategories(): array
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT parent_id, category_id, category_name 
				FROM ' . $this->config['table_prefix'] . 'blogcategory';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $categories = [];
        while (!$recordSet->EOF) {
            $categories[(int)$recordSet->fields('parent_id')][(int)$recordSet->fields('category_id')] = (string)$recordSet->fields('category_name');
            $recordSet->MoveNext();
        }
        return $categories;
    }

    /**
     * @param int $cat_id
     * @return string
     */
    public function getCategoryDescription(int $cat_id): string
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT category_description 
				FROM ' . $this->config['table_prefix'] . 'blogcategory 
				WHERE category_id = ' . $cat_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        return (string)$recordSet->fields('category_description');
    }

    public function getCategoryKeywords(int $cat_id): string
    {
        global $ORconn;

        //gets the keywords from all the blog articles on the current page
        //then returns the 6 most used
        $sql = 'SELECT blogmain_keywords 
				FROM ' . $this->config['table_prefix'] . 'blogmain 
				WHERE blogmain_published = 1
				AND blogmain_id IN (
					SELECT blogmain_id 
					FROM ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation 
					WHERE category_id = ' . $cat_id . ')
				ORDER BY blogmain_date DESC';
        if (!isset($_GET['cur_page']) || !is_scalar($_GET['cur_page'])) {
            $_GET['cur_page'] = 0;
        } else {
            $_GET['cur_page'] = (int)$_GET['cur_page'];
        }
        $blogs_per_page = $this->config['blogs_per_page'];
        $limit_str = $_GET['cur_page'] * $blogs_per_page;
        $recordSet = $ORconn->SelectLimit($sql, $blogs_per_page, $limit_str);
        $keywords = '';
        $count = 0;
        while (!$recordSet->EOF) {
            /** @var string $blogmain_keywords */
            $blogmain_keywords = $recordSet->fields('blogmain_keywords');
            if (!empty($blogmain_keywords)) {
                if ($count == 0) {
                    $keywords .= rtrim($blogmain_keywords, ',');
                } else {
                    $keywords .= ',' . rtrim($blogmain_keywords, ',');
                }
                $count++;
            }
            $recordSet->MoveNext();
        }
        $keywords_arr = array_map('trim', explode(',', $keywords));
        //limit this to 6 keywords
        $keywords_arr = array_slice(array_count_values($keywords_arr), 0, 5);
        return implode(', ', array_keys($keywords_arr));
    }

    /**
     * @return (float|int|mixed)[][]
     *
     * @psalm-return array<int, array{tag_name: string, tag_fontsize: int}>
     */
    public function getBlogPopularTags(): array
    {
        global $ORconn;
        $misc = $this->newMisc();
        //Get Min/Max population.
        $sql = 'SELECT count(tag_id) as population 
				FROM ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation 
				GROUP BY tag_id ORDER BY population';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $min_count = $recordSet->fields('population');
        $recordSet->MoveLast();
        $max_count = $recordSet->fields('population');

        $sql = 'SELECT ' . $this->config['table_prefix'] . 'blogtags.tag_id, tag_name, count(' . $this->config['table_prefix_no_lang'] . 'blogtag_relation.tag_id) as population 
				FROM ' . $this->config['table_prefix'] . 'blogtags 
				LEFT JOIN ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation 
				ON ' . $this->config['table_prefix'] . 'blogtags.tag_id = ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation.tag_id 
				GROUP BY tag_id 
				ORDER BY population DESC LIMIT 50';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $tags = [];
        while (!$recordSet->EOF) {
            $tag_id = (int)$recordSet->fields('tag_id');
            $tag_name = (string)$recordSet->fields('tag_name');
            //Get Tag Population
            $sql = 'SELECT count(tag_id) as population 
					FROM ' . $this->config['table_prefix_no_lang'] . "blogtag_relation 
					WHERE tag_id = '" . $tag_id . "' GROUP BY tag_id";
            $recordSet2 = $ORconn->Execute($sql);
            if (is_bool($recordSet2)) {
                $misc->logErrorAndDie($sql);
            }
            $tag_population = (int)$recordSet2->fields('population');
            if ($min_count == $max_count) {
                $font_size = 8;
            } else {
                $weight = (log($tag_population) - log($min_count)) / (log($max_count) - log($min_count));
                $font_size = intval(8 + round((22 - 8) * $weight));
            }
            $tags[$tag_id] = ['tag_name' => $tag_name, 'tag_fontsize' => $font_size];
            $recordSet->MoveNext();
        }
        return $tags;
    }

    public function getBlogPopularCategories(): array
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT ' . $this->config['table_prefix'] . 'blogcategory.category_id, category_name, count(' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation.category_id) as population 
				FROM ' . $this->config['table_prefix'] . 'blogcategory 
				LEFT JOIN ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation 
				ON ' . $this->config['table_prefix'] . 'blogcategory.category_id = ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation.category_id 
				GROUP BY category_id 
				ORDER BY population DESC LIMIT 5';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $categories = [];
        while (!$recordSet->EOF) {
            $categories[$recordSet->fields('category_id')] = $recordSet->fields('category_name');
            $recordSet->MoveNext();
        }
        return $categories;
    }

    /**
     * Summary of getRecentBlogPosts
     *
     * @param integer $count
     *
     * @return array<integer, string>
     */
    public function getRecentBlogPosts(int $count = 5): array
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT blogmain_id, blogmain_title 
				FROM ' . $this->config['table_prefix'] . "blogmain 
				WHERE blogmain_published = '1' 
				ORDER BY blogmain_date DESC LIMIT " . $count;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $posts = [];
        while (!$recordSet->EOF) {
            /**
             * @var integer $id
             */
            $id = $recordSet->fields('blogmain_id');
            /**
             * @var string $title
             */
            $title = $recordSet->fields('blogmain_title');
            $posts[$id] = $title;
            $recordSet->MoveNext();
        }
        return $posts;
    }

    /**
     * Summary of getRecentBlogComments
     *
     * @param integer $count
     *
     * @return array<int, array{
     *                  blog_id: int,
     *                  text: string,
     *                  userdb_id: int
     *              }>
     */
    public function getRecentBlogComments(int $count = 5): array
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT blogcomments_id, blogcomments_text,blogmain_id,userdb_id 
				FROM ' . $this->config['table_prefix'] . "blogcomments 
				WHERE blogcomments_moderated = '1' 
				ORDER BY blogcomments_timestamp 
				DESC LIMIT " . $count;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $comments = [];
        while (!$recordSet->EOF) {
            /**
             * @var integer $id
             */
            $id = $recordSet->fields('blogcomments_id');
            /**
             * @var integer $user_id
             */
            $user_id = $recordSet->fields('userdb_id');
            /**
             * @var string $text
             */
            $text = $recordSet->fields('blogcomments_text');
            $comments[$id] = ['text' => $text, 'blog_id' => $id, 'userdb_id' => $user_id];
            $recordSet->MoveNext();
        }
        return $comments;
    }

    /**
     * Summary of getArchiveList
     *
     * @return string[]
     */
    public function getArchiveList(): array
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT blogmain_date 
				FROM ' . $this->config['table_prefix'] . 'blogmain 
				WHERE blogmain_published = 1 
				ORDER BY blogmain_date DESC';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $archive = [];
        while (!$recordSet->EOF) {
            if (count($archive) >= 10) {
                break;
            }

            $date = intval($recordSet->fields('blogmain_date'));
            $date = date('Y/F', $date);
            $date = strtolower($date);
            $archive[$date] = $date;
            $recordSet->MoveNext();
        }
        return $archive;
    }

    public function getBlogTitle(int $blog_id): string
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT blogmain_title 
				FROM ' . $this->config['table_prefix'] . 'blogmain 
				WHERE blogmain_id=' . $blog_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        /** @var string $title */
        $title = $recordSet->fields('blogmain_title');
        return $title;
    }

    public function getBlogSeoTitle(int $blog_id): string
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT blog_seotitle 
				FROM ' . $this->config['table_prefix'] . 'blogmain 
				WHERE blogmain_id=' . $blog_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        /** @var string $title */
        $title = $recordSet->fields('blog_seotitle');
        return $title;
    }

    public function getBlogStatus(int $blog_id): int
    {
        global $ORconn;
        $misc = $this->newMisc();
        $blog_id = $misc->makeDbSafe($blog_id);
        $sql = 'SELECT blogmain_published 
				FROM ' . $this->config['table_prefix'] . 'blogmain 
				WHERE blogmain_id=' . $blog_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        return (int)$recordSet->fields('blogmain_published');
    }

    public function getBlogDate(int $blog_id): string
    {
        global $ORconn;
        $misc = $this->newMisc();
        $blog_id = $misc->makeDbSafe($blog_id);
        $sql = 'SELECT blogmain_date 
				FROM ' . $this->config['table_prefix'] . 'blogmain 
				WHERE blogmain_id=' . $blog_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $date = $recordSet->fields('blogmain_date');
        return $misc->convertTimestamp($date, true);
    }

    public function getBlogCommentCount(int $blog_id): int
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT count(blogcomments_id) as commentcount 
				FROM ' . $this->config['table_prefix'] . 'blogcomments WHERE blogmain_id=' . $blog_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        return (int)$recordSet->fields('commentcount');
    }

    public function getBlogIdFromCommentId(int $comment_id): int
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT blogmain_id 
				FROM ' . $this->config['table_prefix'] . 'blogcomments 
				WHERE blogcomments_id=' . $comment_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        return (int)$recordSet->fields('blogmain_id');
    }

    public function getBlogAuthor(int $blog_id): string
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT userdb_id 
				FROM ' . $this->config['table_prefix'] . 'blogmain 
				WHERE blogmain_id=' . $blog_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $userid = (int)$recordSet->fields('userdb_id');

        $user = $this->newUser();
        return $user->getUserSingleItem('userdb_user_first_name', $userid) . ' ' . $user->getUserSingleItem('userdb_user_last_name', $userid);
    }

    public function getBlogDescription(int $blog_id): string
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT blogmain_description 
				FROM ' . $this->config['table_prefix'] . 'blogmain 
				WHERE blogmain_id=' . $blog_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        /**
         * @var string $description
         */
        $description = $recordSet->fields('blogmain_description');
        return $description;
    }

    public function getBlogKeywords(int $blog_id): string
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'SELECT blogmain_keywords 
				FROM ' . $this->config['table_prefix'] . 'blogmain 
				WHERE blogmain_id=' . $blog_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        /**
         * @var string $keywords
         */
        $keywords = $recordSet->fields('blogmain_keywords');
        return $keywords;
    }

    public function pingbackUrls(string $myURL, string $text): string
    {
        $misc = $this->newMisc();
        $display = '';
        preg_match_all('/(href)=[\'"]?([^\'" >]+)[\'" >]/im', $text, $matches);
        $unique_link = array_unique($matches[2]);
        foreach ($unique_link as $mURL) {
            $display .= 'Checking ' . $mURL . "\n";
            $agent = 'Open-Realty Pingback Agent (' . $this->config['baseurl'] . ')';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $mURL);
            curl_setopt($ch, CURLOPT_USERAGENT, $agent);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $buffer = curl_exec($ch);
            $curl_info = curl_getinfo($ch);
            curl_close($ch);
            if (is_string($buffer)) {
                $header_size = $curl_info['header_size'];
                $header = substr($buffer, 0, $header_size);
                $body = substr($buffer, $header_size + 1);
                $h_array = explode("\n", $header);
                $pingback_url = false;
                //Look at header first.
                foreach ($h_array as $header) {
                    $display .= 'Checking Header "' . $header . "\"\n";
                    if (stripos($header, 'X-Pingback: ') === 0) {
                        $display .= 'Found X-Pingback header';
                        $pingback_url = trim(substr($header, 12));
                    }
                }
                //If no header look for <link>
                if (!$pingback_url) {
                    $link_found = preg_match('|<link rel="pingback" href="([^"]+)\s?/?>|i', $body, $link_matches);
                    if ($link_found == 1) {
                        $pingback_url = html_entity_decode($link_matches[1]);
                    }
                }
                //If we have a pingback URL ping it.
                if ($pingback_url !== false) {
                    $misc->logAction('Sending Pingback to ' . $pingback_url);
                    $display .= 'Found it sending pickback request for ' . $pingback_url . "\n";
                    $result = $this->doPingback($myURL, $pingback_url, $mURL);
                    if ($result !== true) {
                        $result = is_string($result) ? $result : '';
                        $misc->logAction('Pingback failed:' . $result);
                        $display .= $result . "\n";
                    }
                }
            }
        }
        return $display;
    }

    public function pingbackServices(string $myurl): void
    {
        $servicelist = $this->config['pingback_services'];
        $service_urls = explode("\n", $servicelist);
        foreach ($service_urls as $url) {
            $this->doPingback($myurl, $url);
        }
    }

    /**
     * @return string|boolean
     */
    public function doPingback(string $myURL, string $pingURL, string $linkURL = ''): bool|string
    {
        $url = trim($pingURL);
        //Check if path is a full url
        if (str_contains($url, 'http://') || str_contains($url, 'https://')) {
            $udata = parse_url($url);
            if (isset($udata['port'])) {
                $port = intval($udata['port']);
            } else {
                $port = 80;
            }
            $server = $udata['host'] ?? '';
            $path = $udata['path'] ?? '';
            if (isset($udata['query'])) {
                $path .= $udata['query'];
            }
            $xmlrpc_client = new Client($path, $server, $port);
            //$xmlrpc_client->setDebug(1); //this will print all the responses as they come back
            if ($linkURL != '') {
                $xmlrpc_message = new Request('pingback.ping', [new Value($myURL), new Value($linkURL)]);
            } else {
                $xmlrpc_message = new Request('pingback.ping', [new Value($myURL), new Value($url)]);
            }

            $xmlrpc_response = $xmlrpc_client->send($xmlrpc_message);
            if (is_array($xmlrpc_response)) {
                # THis should never be an array, so just fail.
                return false;
            }
            if ($xmlrpc_response->faultCode() == 0) {
                return true;
            } else {
                return $xmlrpc_response->faultString();
            }
        }
        return false;
    }

    public function ajaxWpInjectRun(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $blog_functions = $this->newBlogFunctions();

        $page = $this->newPageAdmin();

        $user_manager = $this->newUserManagement();
        $display = '';

        if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfToken($_POST['token'])) {
            $display .= '<span class="redtext">' . $lang['invalid_csrf_token'] . '</span>';
            return $display;
        }

        // $_FILES['uploadedfile']['name']);
        // $_FILES['uploadedfile']['tmp_name']
        if (!isset($_FILES['uploadedfile'])) {
            return $display;
        }
        $display .= 'File "' . $_FILES['uploadedfile']['name'] . '" uploaded sucessfully.<br />';
        $xml = simplexml_load_file($_FILES['uploadedfile']['tmp_name'], SimpleXMLElement::class, LIBXML_NOCDATA);
        if (!$xml) {
            return '<span class="redtext"> Invalid File </span>';
        }
        //Load User List
        $user_email_list = [];
        $sql = 'SELECT userdb_id,userdb_emailaddress,userdb_user_name
FROM ' . $this->config['table_prefix'] . 'userdb';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        // get main listings data
        while (!$recordSet->EOF) {
            $user_email = $recordSet->fields('userdb_emailaddress');
            $user_id = $recordSet->fields('userdb_id');
            $user_email_list[$user_id] = $user_email;

            $recordSet->MoveNext();
        } // end while
        //Get The Channel Properties, as they hold categories and slugs
        if (is_null($xml->{'channel'})) {
            return '<span class="redtext"> Invalid File (null channel)</span>';
        }
        $wp_channel = (array)$xml->{'channel'}->children('http://wordpress.org/export/1.2/');
        if (empty($wp_channel)) {
            $wp_channel = (array)$xml->{'channel'}->children('http://wordpress.org/export/1.1/');
        }
        if (empty($wp_channel)) {
            $wp_channel = (array)$xml->{'channel'}->children('http://wordpress.org/export/1.0/');
        }

        $cat_obj = $wp_channel['category'];

        if (isset($cat_obj->{'category_nicename'})) {
            $cat_obj = [0 => $cat_obj];
        }


        $display .= 'Getting Category List from Open-Realty<br />';
        $or_cats = $blog_functions->getBlogCategoriesFlat();
        $display .= 'Importing Wordpress Categories.<br />';

        foreach ($cat_obj as $wpCat) {
            $cat_parent = (string)$wpCat->{'category_parent'};
            $cat_name = (string)$wpCat->{'cat_name'};

            //Check if category exists
            if (in_array($cat_name, $or_cats)) {
                $display .= 'Category ' . $cat_name . ' already exists.<br />';
            } else {
                //Make Sure Parent Category exists.
                if ($cat_parent != '' && !in_array($cat_parent, $or_cats)) {
                    $display .= '<span class="redtext">Category ' . $cat_name . ' can not be imported into parent category ' . $cat_parent .
                        ' becuase it does not exist, importing as a top level category.</span>';
                    $parent_id = 0;
                } else {
                    $parent_id = array_search($cat_name, $or_cats);
                    $parent_id = intval($parent_id);
                }
                //Get Next Rank For this Category Group.
                $sql = 'SELECT max(category_rank) as maxrank,count(category_id) as post_count ' .
                    'FROM ' . $this->config['table_prefix'] . 'blogcategory ' .
                    'WHERE parent_id = ' . $parent_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $rank = $recordSet->fields('maxrank');
                $post_count = $recordSet->fields('post_count');
                if ($post_count == 0) {
                    $rank = 0;
                } else {
                    $rank++;
                }
                $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "blogcategory '.
                '(category_name,category_seoname,category_description,category_rank,parent_id)'.
                'VALUES (" . $misc->makeDbSafe($cat_name) . "," . $misc->makeDbSafe($cat_name) . "," . $misc->makeDbSafe($cat_name)
                    . ",$rank,$parent_id)";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $blog_id = $ORconn->Insert_ID();
                //Add New Category to list
                $or_cats[$blog_id] = $cat_name;
                $display .= 'Category ' . $cat_name . ' created.<br />';
            }
        }
        //Load Tags
        $tag_obj = $wp_channel['tag'] ?? [];


        $display .= 'Getting Taq List from Open-Realty<br />';
        $or_tags = $blog_functions->getBlogTags();
        $display .= 'Importing Wordpress Tags.<br />';
        foreach ($tag_obj as $wpTag) {
            $tag_slug = (string)$wpTag->{'tag_slug'};
            $tag_name = (string)$wpTag->{'tag_name'};

            //Check if category exists
            if (in_array($tag_name, $or_tags)) {
                $display .= 'Tag ' . $tag_name . ' already exists.<br />';
            } else {
                $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "blogtags
(tag_name,tag_seoname,tag_description)
VALUES (" . $misc->makeDbSafe($tag_name) . "," . $misc->makeDbSafe($tag_slug) . ",'')";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $blog_id = $ORconn->Insert_ID();
                //Add New Category to list
                $or_tags[$blog_id] = $tag_name;
                $display .= 'Tag ' . $tag_name . ' created.<br />';
            }
        }
        if (!is_null($xml->{'channel'}->{'item'})) {
            foreach ($xml->{'channel'}->{'item'} as $data) {
                /* xmlns:excerpt="http://wordpress.org/export/1.0/excerpt/"
                xmlns:content="http://purl.org/rss/1.0/modules/content/"
                xmlns:wfw="http://wellformedweb.org/CommentAPI/"
                xmlns:dc="http://purl.org/dc/elements/1.1/"
                xmlns:wp="http://wordpress.org/export/1.0/"
                */
                $ns_content = $data->children('http://purl.org/rss/1.0/modules/content/');
                $wp_content = $data->children('http://wordpress.org/export/1.0/');
                if (empty($wp_content)) {
                    $wp_content = $data->children('http://wordpress.org/export/1.2/');
                }


                $blog_title = (string)$data->{'title'};
                $blog_pubdate = (string)$data->{'pubDate'};
                $blog_pubdate = strtotime($blog_pubdate);
                $blog_content = (string)$ns_content->{'encoded'};
                $blog_content = nl2br($blog_content);
                $blog_type = (string)$wp_content->{'post_type'};
                $blog_status = (string)$wp_content->{'status'};
                $blog_metakeywords = '';
                $blog_metadescription = '';
                $blog_cateories = [];
                $blog_tags = [];
                $category = $data->{'category'};
                if (!is_null($category)) {
                    foreach ($category as $tmpobj) {
                        $attributes = $tmpobj->attributes();
                        if (!is_null($attributes)) {
                            if (!is_null($attributes->domain)) {
                                $cat_tag = (string)$attributes->domain;
                                $cat_tag_name = (string)$tmpobj->{0};
                                if ($cat_tag == 'category') {
                                    //See if this category exists
                                    if (in_array($cat_tag_name, $or_cats)) {
                                        $cat_id = array_search($cat_tag_name, $or_cats);
                                        $blog_cateories[$cat_id] = $cat_id;
                                    }
                                }
                                if ($cat_tag == 'tag') {
                                    //See if this category exists
                                    if (in_array($cat_tag_name, $or_tags)) {
                                        $tag_id = array_search($cat_tag_name, $or_tags);
                                        $blog_tags[$tag_id] = $tag_id;
                                    }
                                }
                            }
                        }
                    }
                }
                //Make sure at least one category is selected
                if (count($blog_cateories) == 0) {
                    $blog_cateories[1] = 1;
                }

                //Get Metadata
                $metadata = $wp_content->{'postmeta'};
                if (!is_null($metadata)) {
                    foreach ($metadata as $tmpobj) {
                        $key = (string)$tmpobj->{'meta_key'};
                        $value = (string)$tmpobj->{'meta_value'};
                        if ($key == '_aioseop_description') {
                            $blog_metadescription = $value;
                        } elseif ($key == '_aioseop_keywords') {
                            $blog_metakeywords = $value;
                        }
                    }
                }

                //INSERT BLog POSTS
                //Insert WYSIWYG Pages
                if ($blog_type == 'page') {
                    //Make sure blog post does not yet exsists.
                    $sql_blog_title = $misc->makeDbSafe($blog_title);
                    $sql_blog_content = $ORconn->addQ($blog_content);
                    $sql_blog_metadescription = $misc->makeDbSafe($blog_metadescription);
                    $sql_blog_metakeywords = $misc->makeDbSafe($blog_metakeywords);
                    $seotitle = $page->createSeoUri($blog_title, false);
                    $sql_seotitle = $misc->makeDbSafe($seotitle);
                    $sql_blog_status = 0;
                    switch ($blog_status) {
                        case 'publish':
                            $sql_blog_status = 1;
                            break;
                        case 'review':
                            $sql_blog_status = 2;
                            break;
                    }

                    $sql = 'SELECT pagesmain_id FROM ' . $this->config['table_prefix'] . "pagesmain WHERE pagesmain_title = $sql_blog_title";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    if ($recordSet->RecordCount() == 0) {
                        //Add Blog
                        $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "pagesmain (pagesmain_title,pagesmain_date,pagesmain_full,pagesmain_description,pagesmain_keywords,pagesmain_published,page_seotitle) VALUES ($sql_blog_title,$blog_pubdate,'$sql_blog_content',$sql_blog_metadescription,$sql_blog_metakeywords,$sql_blog_status,$sql_seotitle)";
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            $misc->logErrorAndDie($sql);
                        }

                        $display .= 'WYSIWYG PAGE ' . htmlentities($blog_title, ENT_COMPAT, $this->config['charset']) . ' created.<br />';
                    } else {
                        $display .= 'WYSIWYG PAGE ' . htmlentities($blog_title, ENT_COMPAT, $this->config['charset']) . ' already exists.<br />';
                    }
                } elseif ($blog_type == 'post') {
                    //Make sure blog post does not yet exsists.
                    $sql_blog_title = $misc->makeDbSafe($blog_title);
                    $sql_blog_content = $ORconn->addQ($blog_content);
                    $sql_blog_metadescription = $misc->makeDbSafe($blog_metadescription);
                    $sql_blog_metakeywords = $misc->makeDbSafe($blog_metakeywords);
                    $seotitle = $page->createSeoUri($blog_title, false);
                    $sql_seotitle = $misc->makeDbSafe($seotitle);
                    $sql_blog_status = 0;
                    switch ($blog_status) {
                        case 'publish':
                            $sql_blog_status = 1;
                            break;
                        case 'review':
                            $sql_blog_status = 2;
                            break;
                    }

                    $sql = 'SELECT blogmain_id FROM ' . $this->config['table_prefix'] . "blogmain WHERE blogmain_title = $sql_blog_title";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }

                    if ($recordSet->RecordCount() == 0) {
                        //Add Blog
                        //TODO: Change Insert to map wp blog users to OR Agents.
                        $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "blogmain (userdb_id,blogmain_title,blogmain_date,blogmain_full,blogmain_description,blogmain_keywords,blogmain_published,blog_seotitle) VALUES (1,$sql_blog_title,$blog_pubdate,'$sql_blog_content',$sql_blog_metadescription,$sql_blog_metakeywords,$sql_blog_status,$sql_seotitle)";
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            $misc->logErrorAndDie($sql);
                        }
                        $blog_id = $ORconn->Insert_ID();
                        //Assign Categories
                        foreach ($blog_cateories as $cid) {
                            $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . "blogcategory_relation (category_id,blogmain_id) VALUES ($cid,$blog_id)";
                            $recordSet = $ORconn->Execute($sql);
                            if (is_bool($recordSet)) {
                                $misc->logErrorAndDie($sql);
                            }
                        }
                        foreach ($blog_tags as $tid) {
                            $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . "blogtag_relation (tag_id,blogmain_id) VALUES ($tid,$blog_id)";
                            $recordSet = $ORconn->Execute($sql);
                            if (is_bool($recordSet)) {
                                $misc->logErrorAndDie($sql);
                            }
                        }
                        //Start Importing COmmnets & Pingbacks
                        if (isset($wp_content->{'comment'})) {
                            $comments = $wp_content->{'comment'};
                            foreach ($comments as $comment) {
                                $comment_name = (string)$comment->{'comment_author'};
                                $comment_name_email = (string)$comment->{'comment_author_email'};
                                $comment_url = (string)$comment->{'comment_author_url'};
                                $comment_content = (string)$comment->{'comment_content'};
                                $comment_approved = (string)$comment->{'comment_approved'};
                                $comment_date = (string)$comment->{'comment_date'};
                                $comment_date = strtotime($comment_date);
                                $comment_type = (string)$comment->{'comment_type'};

                                if ($comment_type == 'pingback') {
                                    $display .= 'Found Pingback from URL: ' . $comment_url . '<br />';
                                } else {
                                    //Get the user
                                    //$user_email_list=array();
                                    //$user_name_list=array();
                                    $user_search = array_search($comment_name_email, $user_email_list);
                                    if ($user_search === false) {
                                        //No User found with that email
                                        //Create a user
                                        $_POST['user_first_name'] = 'WP IMPORT';
                                        $_POST['user_last_name'] = $comment_name;
                                        $safe_user_name = preg_replace('/[^a-zA-Z0-9]/', '', $comment_name);
                                        $_POST['edit_user_name'] = 'wp' . $safe_user_name;
                                        $_POST['user_email'] = $comment_name_email;
                                        $rand_pass = $user_manager->generatePassword();
                                        $_POST['edit_user_pass'] = $rand_pass;
                                        $_POST['edit_user_pass2'] = $rand_pass;
                                        $_POST['edit_active'] = 'yes';
                                        $_POST['edit_isAgent'] = 'no';
                                        $_POST['edit_isAdmin'] = 'no';
                                        $user_search = $user_manager->createUser();
                                        if (!is_numeric($user_search)) {
                                            $display .= $user_search;
                                        }
                                    }
                                    //See if the comment already exists..
                                    $comment_user_id = intval($user_search);
                                    $comment_approved = intval($comment_approved);
                                    $safe_comment_content = $misc->makeDbSafe($comment_content);
                                    $sql = 'SELECT blogcomments_id FROM ' . $this->config['table_prefix'] . "blogcomments WHERE userdb_id = $comment_user_id AND blogcomments_timestamp = $comment_date AND blogcomments_text = $safe_comment_content AND blogmain_id = $blog_id";
                                    $recordSet = $ORconn->Execute($sql);
                                    if (is_bool($recordSet)) {
                                        $misc->logErrorAndDie($sql);
                                    }
                                    if ($recordSet->RecordCount() == 0) {
                                        $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "blogcomments (userdb_id,blogcomments_timestamp,blogcomments_text,blogmain_id,blogcomments_moderated) VALUES ($comment_user_id, $comment_date, $safe_comment_content,$blog_id,$comment_approved)";
                                        $recordSet = $ORconn->Execute($sql);
                                        if (is_bool($recordSet)) {
                                            $misc->logErrorAndDie($sql);
                                        }
                                        $display .= 'Blog Comment from ' . $comment_name_email . ' created.<br />';
                                    } else {
                                        $display .= 'Blog Comment from ' . $comment_name_email . ' already exists.<br />';
                                    }
                                }
                            }
                        }
                        //END Importing COmmnets & Pingbacks

                        $display .= 'Blog Post ' . htmlentities($blog_title, ENT_COMPAT, $this->config['charset']) . ' created.<br />';
                    } else {
                        $blog_id = $recordSet->fields('blogmain_id');
                        $display .= 'Blog Post ' . htmlentities($blog_title, ENT_COMPAT, $this->config['charset']) . ' already exists.<br />';
                        //Start Importing COmmnets & Pingbacks
                        if (isset($wp_content->{'comment'})) {
                            $comments = $wp_content->{'comment'};
                            foreach ($comments as $comment) {
                                $comment_name = (string)$comment->{'comment_author'};
                                $comment_name_email = (string)$comment->{'comment_author_email'};
                                $comment_url = (string)$comment->{'comment_author_url'};
                                $comment_content = (string)$comment->{'comment_content'};
                                $comment_approved = (string)$comment->{'comment_approved'};
                                $comment_date = (string)$comment->{'comment_date'};
                                $comment_date = strtotime($comment_date);
                                $comment_type = (string)$comment->{'comment_type'};
                                $safe_comment_url = $misc->makeDbSafe($comment_url);
                                $comment_approved = intval($comment_approved);

                                if ($comment_type == 'pingback' || $comment_type == 'trackback') {
                                    $sql = 'SELECT blogpingback_id FROM ' . $this->config['table_prefix_no_lang'] . "blogpingbacks WHERE blogpingback_source = '$safe_comment_url' AND blogmain_id = $blog_id;";
                                    $recordSet = $ORconn->Execute($sql);
                                    if (is_bool($recordSet)) {
                                        $misc->logErrorAndDie($sql);
                                    }

                                    if ($recordSet->RecordCount() > 0) { # Pingback already registered
                                        $display .= 'Blog Pingback from ' . $comment_url . ' already exists.<br />';
                                    } else {
                                        $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . "blogpingbacks (blogpingback_source,blogmain_id,blogpingback_timestamp,blogcomments_moderated) VALUES ($safe_comment_url,$blog_id,$comment_date,$comment_approved);";
                                        $recordSet = $ORconn->Execute($sql);
                                        if (is_bool($recordSet)) {
                                            $misc->logErrorAndDie($sql);
                                        }
                                        $display .= 'Blog Pingback from ' . $comment_url . ' created.<br />';
                                    }
                                } else {
                                    //Get the user
                                    //$user_email_list=array();
                                    //$user_name_list=array();
                                    $user_search = array_search($comment_name_email, $user_email_list);
                                    if ($user_search === false) {
                                        //No User found with that email
                                        //Create a user
                                        $_POST['user_first_name'] = 'WP IMPORT';
                                        $_POST['user_last_name'] = $comment_name;
                                        $safe_user_name = preg_replace('/[^a-zA-Z0-9]/', '', $comment_name);
                                        $_POST['edit_user_name'] = 'wp' . $safe_user_name;
                                        $_POST['user_email'] = $comment_name_email;
                                        $rand_pass = $user_manager->generatePassword();
                                        $_POST['edit_user_pass'] = $rand_pass;
                                        $_POST['edit_user_pass2'] = $rand_pass;
                                        $user_search = $user_manager->createUser();
                                        if (!is_numeric($user_search)) {
                                            $display .= $user_search;
                                        }
                                    }
                                    //See if the comment already exists..
                                    $comment_user_id = intval($user_search);

                                    $safe_comment_content = $misc->makeDbSafe($comment_content);
                                    $sql = 'SELECT blogcomments_id FROM ' . $this->config['table_prefix'] . "blogcomments WHERE userdb_id = $comment_user_id AND blogcomments_timestamp = $comment_date AND blogcomments_text = '$safe_comment_content' AND blogmain_id = $blog_id";
                                    $recordSet = $ORconn->Execute($sql);
                                    if (is_bool($recordSet)) {
                                        $misc->logErrorAndDie($sql);
                                    }
                                    if ($recordSet->RecordCount() == 0) {
                                        $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "blogcomments (userdb_id,blogcomments_timestamp,blogcomments_text,blogmain_id,blogcomments_moderated) VALUES ($comment_user_id, $comment_date, $safe_comment_content,$blog_id,$comment_approved)";
                                        $recordSet = $ORconn->Execute($sql);
                                        if (is_bool($recordSet)) {
                                            $misc->logErrorAndDie($sql);
                                        }
                                        $display .= 'Blog Comment from ' . $comment_name_email . ' created.<br />';
                                    } else {
                                        $display .= 'Blog Comment from ' . $comment_name_email . ' already exists.<br />';
                                    }
                                }
                            }
                        }
                        //END Importing COmmnets & Pingbacks
                    }
                }
            }
        }
        return $display;
    }
}
