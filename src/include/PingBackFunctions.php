<?php

namespace OpenRealty;

use PhpXmlRpc\Request;
use PhpXmlRpc\Response;
use PhpXmlRpc\Value;

class PingBackFunctions extends BaseClass
{
    /**
     * Extracts links from html
     *
     * @param string $html HTML to parse to find links.
     *
     * @return string[] Array of HTML Links.
     */
    public function linkExtractor(string $html): array
    {
        $linkArray = [];
        if (preg_match_all('/<a\s+.*?href=[\"\']?([^\"\' >]*)[\"\']?[^>]*>.*?<\/a>/i', $html, $matches, PREG_SET_ORDER)) {
            foreach ($matches as $match) {
                $linkArray[] = $match[1];
            }
        }
        return $linkArray;
    }


    /**
     * Processes Pingback Request
     * Tutorial at quietearth.us was very helpful in building the pingback parser.
     *
     * @param Request $m The xmlrpc request.
     *
     * @return Response
     */
    public function processPing(Request $m): Response
    {
        global $ORconn;
        $page = $this->newPageUser();
        $misc = $this->newMisc();
        $x1 = $m->getParam(0);
        //echo 'x1: <pre>'.print_r($x1).'</pre>';
        $x2 = $m->getParam(1);
        //echo 'x2: <pre>'.print_r($x2).'</pre>';
        $source_uri = (string)$x1->scalarval(); # their article
        $dest_uri = (string)$x2->scalarval(); # your article
        $agent = 'Open-Realty Pingback Service (' . $this->config['baseurl'] . ')';
        //echo $source;
        // INSERT CODE
        // here we can check for valid urls in source and dest, security
        // lookup the dest article in our database etc..
        if (!$this->config['allow_pingbacks']) { # Access denied
            return new Response(0, 49, 'Access denied');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $source_uri);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $source = curl_exec($ch);
        curl_close($ch);

        if (is_bool($source)) { # source uri does not exist
            $curl_error = curl_error($ch);
            return new Response(0, 16, 'Source uri does not exist: ' . $curl_error);
        }
        //$source = htmlspecialchars_decode($source);
        $links = $this->linkExtractor($source);
        $linkFound = false;
        //echo '<pre>Finding Link: '.$dest_uri.' </pre>';
        foreach ($links as $link) {
            if (str_contains(htmlspecialchars_decode(htmlspecialchars_decode($link)), $dest_uri)) {
                $linkFound = true;
            }
        }
        if ($linkFound === false) { # source uri does not have a link to target uri
            return new Response(0, 17, 'Source uri does not have link to target uri');
        }

        //Check that target is a local URI
        $base_url_nowww = str_replace('www.', '', $this->config['baseurl']);
        if (!str_contains($dest_uri, $this->config['baseurl']) && !str_contains($dest_uri, $base_url_nowww)) { # target uri cannot be used as target
            return new Response(0, 33, 'Target uri cannot be used as target (baseurl)');
        }


        $dest_url_path = str_replace($base_url_nowww, '', $dest_uri);
        $dest_url_path = str_replace($this->config['baseurl'], '', $dest_url_path);
        //Check that it is a published artile
        $article_id = 0;
        unset($_GET['ArticleID']);
        //Check if this is a magicurl
        $output = [];
        $url_parts = parse_url($dest_url_path);
        if (isset($url_parts['query'])) {
            parse_str($url_parts['query'], $output);
        }
        if (isset($output['ArticleID']) && isset($output['action']) && $output['action'] == 'blog_view_article') {
            $_GET['ArticleID'] = $output['ArticleID'];
        } else {
            $page->magicURIParser(false, $dest_url_path);
        }
        if (isset($_GET['ArticleID'])) {
            $article_id = intval($_GET['ArticleID']);
        }
        if ($article_id > 0) {
            //Make sure article is published.
            $sql = 'SELECT blogmain_id FROM ' . $this->config['table_prefix'] . 'blogmain WHERE blogmain_id = ' . $article_id . ' AND blogmain_published = 1;';
            //echo $sql;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            //echo 'RC:'.$recordSet->RecordCount();
            if ($recordSet->RecordCount() == 0) { # Pingback already registered
                return new Response(0, 33, 'Target uri cannot be used as target (nonpublished)');
            }
        } else {
            return new Response(0, 33, 'Target uri cannot be used as target (invalid article id)');
        }
        //Al
        //Make sure this is not a duplicate ping
        /*  CREATE TABLE  `blogtest`.`default_blogpingbacks` (
            `blogpingback_id` int(11) NOT NULL auto_increment,
            `blogmain_id` int(11) NOT NULL,
            `blogpingback_timestamp` int(11) NOT NULL,
            `blogpingback_source` varchar(2000) NOT NULL,
            `blogpingback_dest` varchar(2000) NOT NULL,
            `blogcomments_moderated` tinyint(1) default NULL,
            PRIMARY KEY  (`blogpingback_id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=latin1
            */
        $sql = 'SELECT blogpingback_id FROM ' . $this->config['table_prefix_no_lang'] . 'blogpingbacks 
			WHERE blogpingback_source = ' . $misc->makeDbSafe($source_uri) . ' 
			AND blogmain_id = ' . $article_id . ';';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }

        if ($recordSet->RecordCount() > 0) { # Pingback already registered
            return new Response(0, 48, 'Pingback already registered');
        }

        /*
            if (..) { # Could not communicate with upstream server or got error
            return new PhpXmlRpc\Response(0, 50, "Problem with upstream server");
            }
        */
        if ($this->config['blog_requires_moderation']) {
            $moderated = 0;
        } else {
            $moderated = 1;
        }
        $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . 'blogpingbacks (blogpingback_source,blogmain_id,blogpingback_timestamp,blogcomments_moderated)
	VALUES (' . $misc->makeDbSafe($source_uri) . ',' . $article_id . ',
	' . time() . ',' . $moderated . '
	);';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            return new Response(0, 50, 'Unkown error');
        }

        return new Response(new Value('Pingback registered.', 'string'));
    }
}
