<?php

declare(strict_types=1);

namespace OpenRealty;

class Notification extends BaseClass
{
    public function notifyUsersOfAllNewListings(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();

        $display = '';
        $ORIGIONAL_GET = $_GET;

        $search_page = $this->newSearchPage();
        //Get Last Notification Timestamp
        $sql = 'SELECT controlpanel_notification_last_timestamp 
				FROM ' . $this->config['table_prefix_no_lang'] . 'controlpanel';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $last_timestamp = $ORconn->UnixTimeStamp((string)$recordSet->fields('controlpanel_notification_last_timestamp'));
        //echo 'Timestamp'.$last_timestamp;

        //generate message with date since last notification was run
        if (!is_bool($last_timestamp)) {
            $display .= $lang['notification_task_message'] . date(DATE_RFC822, $last_timestamp) . "<br />\r\n";
        }
        $current_timestamp = time();

        $sql = 'SELECT ' . $this->config['table_prefix'] . 'usersavedsearches.userdb_id, usersavedsearches_title, usersavedsearches_query_string, usersavedsearches_notify, userdb_user_name, userdb_emailaddress
				FROM ' . $this->config['table_prefix'] . 'userdb , ' . $this->config['table_prefix'] . 'usersavedsearches
				WHERE ' . $this->config['table_prefix'] . 'userdb.userdb_id = ' . $this->config['table_prefix'] . "usersavedsearches.userdb_id 
				AND usersavedsearches_notify = 'yes'";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }

        while (!$recordSet->EOF) {
            $query_string = (string)$recordSet->fields('usersavedsearches_query_string');
            $search_title = (string)$recordSet->fields('usersavedsearches_title');
            $email = (string)$recordSet->fields('userdb_emailaddress');
            $user_name = (string)$recordSet->fields('userdb_user_name');

            //generate checking message
            $display .= $lang['notification_task_check'] . $search_title . ' -> ' . $user_name . "<br />\r\n";
            // Break Quesry String up into $_GET variables.

            unset($_GET);

            $query_string = urldecode($query_string);
            $criteria = explode('&', $query_string);
            foreach ($criteria as $crit) {
                if ($crit != '') {
                    $pieces = explode('=', $crit);
                    $pos = strpos($pieces[0], '[]');
                    if ($pos !== false) {
                        $name = substr($pieces[0], 0, -2);
                        if (isset($pieces[1])) {
                            $_GET[$name][] = $pieces[1];
                        } else {
                            $_GET[$name][] = '';
                        }
                    } else {
                        if (isset($pieces[1])) {
                            $_GET[$pieces[0]] = $pieces[1];
                        } else {
                            $_GET[$pieces[0]] = '';
                        }
                    }
                }
            }
            $_GET['listingsdb_active'] = 'yes';
            if ($this->config['use_expiration']) {
                $_GET['listingsdb_expiration_greater'] = time();
            }
            $_GET['listing_last_modified_greater'] = $last_timestamp;
            //echo '<pre>'.print_r($_GET,TRUE).'</pre>';
            $matched_listing_ids = $search_page->searchResults(true);
            //echo '<pre>'.print_r($matched_listing_ids,TRUE).'</pre>';
            if (count($matched_listing_ids) >= 1) {
                //print_r($matched_listing_ids);
                //Get User Details
                //Now that we have a list of the listings, render the template
                $template = $this->renderNotifyListings($matched_listing_ids);
                // Send Mail
                if (isset($this->config['site_email']) && $this->config['site_email'] != '') {
                    $sender_email = $this->config['site_email'];
                } else {
                    $sender_email = $this->config['admin_email'];
                }
                $subject = $lang['new_listing_notify'] . $search_title;
                $sent = $misc->sendEmail($this->config['admin_name'], $sender_email, $email, $template, $subject, true, true);
                if (!is_bool($sent)) {
                    $display .= '<span class="redtext">Error sending listing notification to ' . $user_name . '&lt;' . $email . '&gt; - ' . $sent . '</span>';
                } else {
                    $display .= '<span>Sent Listing Notification to ' . $user_name . '&lt;' . $email . '&gt; for listings ' . implode(',', $matched_listing_ids) . "</span><br />\r\n";
                }
            }
            $recordSet->MoveNext();
        }
        //Swt Last Notification Timestamp
        $db_timestamp = $ORconn->DBTimeStamp($current_timestamp);
        $sql = 'UPDATE ' . $this->config['table_prefix_no_lang'] . 'controlpanel 
				SET controlpanel_notification_last_timestamp = ' . $db_timestamp;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $_GET = $ORIGIONAL_GET;
        $display .= "Finish Sending Notifications<br />\r\n";
        return $display;
    }

    /**
     * @param integer[] $listingIDArray
     * @return string
     */
    public function renderNotifyListings(array $listingIDArray): string
    {
        global $ORconn, $current_ID;

        //Load the Core Template class and the Misc Class

        $page = $this->newPageUser();

        //Declare an empty display variable to hold all output from function.
        $display = '';
        //If We have a $current_ID save it
        $old_current_ID = '';
        if ($current_ID != '') {
            $old_current_ID = $current_ID;
        }

        //Load the Notify Listing Template specified in the Site Config
        $page->loadPage($this->config['template_path'] . '/' . $this->config['notify_listings_template']);

        // Determine if the template uses rows.
        // First item in array is the row conent second item is the number of block per block row
        $notify_template_row = $page->getTemplateSectionRow('notify_listing_block_row');

        $x = 0;
        $col_count = 0;
        $user_rows = false;
        $row = '';
        $new_row_data = [];
        if (isset($notify_template_row[0], $notify_template_row[1])) {
            $row = $notify_template_row[0];
            $col_count = $notify_template_row[1];
            $user_rows = true;
            $x = 1;
        }
        $notify_template_section = '';
        $listingIDString = implode(',', $listingIDArray);
        $page->replaceTag('notify_results_link', $this->config['baseurl'] . '/index.php?action=searchresults&listing_id=' . $listingIDString);

        foreach ($listingIDArray as $current_ID) {
            if ($user_rows && $x > $col_count) {
                //We are at then end of a row. Save the template section as a new row.
                $new_row_data[] = $page->replaceTemplateSection('notify_listing_block', $notify_template_section, $row);
                //$new_row_data[] = $notify_template_section;
                $notify_template_section = $page->getTemplateSection('notify_listing_block');
                $x = 1;
            } else {
                $notify_template_section .= $page->getTemplateSection('notify_listing_block');
            }
            $notify_url = $page->magicURIGenerator('listing', (string)$current_ID, true);
            $notify_template_section = $page->replaceListingFieldTags($current_ID, $notify_template_section);
            $notify_template_section = $page->parseTemplateSection($notify_template_section, 'notify_url', $notify_url);
            $notify_template_section = $page->parseTemplateSection($notify_template_section, 'listingid', (string)$current_ID);

            // Setup Image Tags
            $sql2 = 'SELECT listingsimages_thumb_file_name,listingsimages_file_name
				FROM ' . $this->config['table_prefix'] . "listingsimages
				WHERE (listingsdb_id = $current_ID)
				ORDER BY listingsimages_rank";
            $recordSet2 = $ORconn->SelectLimit($sql2, 1, 0);
            $notify_thumb_width = '';
            $notify_thumb_height = '';
            $notify_thumb_src = '';
            $notify_width = '';
            $notify_height = '';
            $notify_src = '';
            if ($recordSet2->RecordCount() > 0) {
                $thumb_file_name = $recordSet2->fields('listingsimages_thumb_file_name');
                $file_name = $recordSet2->fields('listingsimages_file_name');
                if ($thumb_file_name != '' && file_exists($this->config['listings_upload_path'] . "/$thumb_file_name")) {
                    // gotta grab the thumbnail image size
                    $imagedata = GetImageSize($this->config['listings_upload_path'] . "/$thumb_file_name");
                    $imagewidth = $imagedata[0];
                    $imageheight = $imagedata[1];
                    $shrinkage = $this->config['thumbnail_width'] / $imagewidth;
                    $notify_thumb_width = $imagewidth * $shrinkage;
                    $notify_thumb_height = $imageheight * $shrinkage;
                    $notify_thumb_src = $this->config['listings_view_images_path'] . '/' . $thumb_file_name;
                    // gotta grab the thumbnail image size
                    $imagedata = GetImageSize($this->config['listings_upload_path'] . "/$file_name");
                    $imagewidth = $imagedata[0];
                    $imageheight = $imagedata[1];
                    $notify_width = $imagewidth;
                    $notify_height = $imageheight;
                    $notify_src = $this->config['listings_view_images_path'] . '/' . $file_name;
                }
            } else {
                if ($this->config['show_no_photo']) {
                    $imagedata = GetImageSize($this->config['basepath'] . '/images/nophoto.gif');
                    $imagewidth = $imagedata[0];
                    $imageheight = $imagedata[1];
                    $shrinkage = $this->config['thumbnail_width'] / $imagewidth;
                    $notify_thumb_width = $imagewidth * $shrinkage;
                    $notify_thumb_height = $imageheight * $shrinkage;
                    $notify_thumb_src = $this->config['baseurl'] . '/images/nophoto.gif';
                    $notify_width = $notify_thumb_width;
                    $notify_height = $notify_thumb_height;
                    $notify_src = $this->config['baseurl'] . '/images/nophoto.gif';
                }
            }
            if (!empty($notify_thumb_src)) {
                $notify_template_section = $page->parseTemplateSection($notify_template_section, 'notify_thumb_src', $notify_thumb_src);
                $notify_template_section = $page->parseTemplateSection($notify_template_section, 'notify_thumb_height', (string)$notify_thumb_height);
                $notify_template_section = $page->parseTemplateSection($notify_template_section, 'notify_thumb_width', (string)$notify_thumb_width);
                $notify_template_section = $page->cleanupTemplateBlock('notify_img', $notify_template_section);
            } else {
                $notify_template_section = $page->removeTemplateBlock('notify_img', $notify_template_section);
            }
            if (!empty($notify_src)) {
                $notify_template_section = $page->parseTemplateSection($notify_template_section, 'notify_large_src', $notify_src);
                $notify_template_section = $page->parseTemplateSection($notify_template_section, 'notify_large_height', (string)$notify_height);
                $notify_template_section = $page->parseTemplateSection($notify_template_section, 'notify_large_width', (string)$notify_width);
                $notify_template_section = $page->cleanupTemplateBlock('notify_img_large', $notify_template_section);
            } else {
                $notify_template_section = $page->removeTemplateBlock('notify_img_large', $notify_template_section);
            }
            if ($user_rows) {
                $x++;
            }
        }
        if ($user_rows) {
            $notify_template_section = $page->cleanupTemplateBlock('notify_listing', $notify_template_section);
            $new_row_data[] = $page->replaceTemplateSection('notify_listing_block', $notify_template_section, $row);
            $replace_row = implode('', $new_row_data);
            $page->replaceTemplateSectionRow('notify_listing_block_row', $replace_row);
        } else {
            $page->replaceTemplateSection('notify_listing_block', $notify_template_section);
        }
        $page->replacePermissionTags();
        $page->replaceUrls();
        $page->autoReplaceTags();
        $page->replaceLangTemplateTags();
        $display .= $page->returnPage();

        $current_ID = '';
        if ($old_current_ID != '') {
            $current_ID = $old_current_ID;
        }
        return $display;
    }
}
