<?php

declare(strict_types=1);

namespace OpenRealty;

use Composer\Semver\VersionParser;
use ZipArchive;

/****
 * general_admin
 * This class contains the functions related to the administrative index page section.
 *
 */
class GeneralAdmin extends BaseClass
{
    /**
     * Builds HTML for admin index page. or_index.html
     *
     * @return string
     */
    public function indexPage(): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $page = $this->newPageAdmin();
        $login = $this->newLogin();
        $page->loadPage($this->config['admin_template_path'] . '/or_index.html');
        $page->replaceTags(['general_info', 'openrealty_links', 'baseurl', 'lang', 'user_id', 'addon_links']);
        $page->replaceForeachPclassBlock();
        $page->replaceIfAddonBlock();
        //Handle Blog Counts
        //Replace Status Counts
        //{blog_edit_status_all_count}
        $blog_user_type = intval($_SESSION['blog_user_type']);
        $blog_user_id = intval($_SESSION['userID']);
        if ($blog_user_type == 4 || $_SESSION['admin_privs'] == 'yes') {
            $sql = 'SELECT count(blogmain_id) as blogcount  FROM ' . $this->config['table_prefix'] . 'blogmain';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $count_all = (int)$recordSet->fields('blogcount');

            $sql = 'SELECT count(blogmain_id) as blogcount  FROM ' . $this->config['table_prefix'] . 'blogmain WHERE blogmain_published = 1';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $count_published = (int)$recordSet->fields('blogcount');

            $sql = 'SELECT count(blogmain_id) as blogcount  FROM ' . $this->config['table_prefix'] . 'blogmain WHERE blogmain_published = 0';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $count_draft = (int)$recordSet->fields('blogcount');

            $sql = 'SELECT count(blogmain_id) as blogcount  FROM ' . $this->config['table_prefix'] . 'blogmain WHERE blogmain_published = 2';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $count_review = (int)$recordSet->fields('blogcount');
        } else {
            $sql = 'SELECT count(blogmain_id) as blogcount  FROM ' . $this->config['table_prefix'] . 'blogmain WHERE userdb_id = ' . $blog_user_id;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $count_all = (int)$recordSet->fields('blogcount');

            $sql = 'SELECT count(blogmain_id) as blogcount  FROM ' . $this->config['table_prefix'] . 'blogmain WHERE blogmain_published = 1 AND userdb_id = ' . $blog_user_id;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $count_published = (int)$recordSet->fields('blogcount');

            $sql = 'SELECT count(blogmain_id) as blogcount  FROM ' . $this->config['table_prefix'] . 'blogmain WHERE blogmain_published = 0 AND userdb_id = ' . $blog_user_id;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $count_draft = (int)$recordSet->fields('blogcount');
            $sql = 'SELECT count(blogmain_id) as blogcount  FROM ' . $this->config['table_prefix'] . 'blogmain WHERE blogmain_published = 2 AND userdb_id = ' . $blog_user_id;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $count_review = (int)$recordSet->fields('blogcount');
        }

        $page->replaceTag('blog_edit_status_all_count', (string)$count_all);
        $page->replaceTag('blog_edit_status_published_count', (string)$count_published);
        $page->replaceTag('blog_edit_status_draft_count', (string)$count_draft);
        $page->replaceTag('blog_edit_status_review_count', (string)$count_review);
        //Get Status
        /* Handle Page Counts */
        //Replace Status Counts
        //{page_edit_status_all_count}

        $sql = 'SELECT count(pagesmain_id) as pagecount  
				FROM ' . $this->config['table_prefix'] . 'pagesmain';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $count_all = (int)$recordSet->fields('pagecount');

        $sql = 'SELECT count(pagesmain_id) as pagecount  
				FROM ' . $this->config['table_prefix'] . 'pagesmain 
				WHERE pagesmain_published = 1';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $count_published = (int)$recordSet->fields('pagecount');

        $sql = 'SELECT count(pagesmain_id) as pagecount  
				FROM ' . $this->config['table_prefix'] . 'pagesmain 
				WHERE pagesmain_published = 0';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $count_draft = (int)$recordSet->fields('pagecount');

        $sql = 'SELECT count(pagesmain_id) as pagecount  
				FROM ' . $this->config['table_prefix'] . 'pagesmain 
				WHERE pagesmain_published = 2';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $count_review = (int)$recordSet->fields('pagecount');

        $page->replaceTag('page_edit_status_all_count', (string)$count_all);
        $page->replaceTag('page_edit_status_published_count', (string)$count_published);
        $page->replaceTag('page_edit_status_draft_count', (string)$count_draft);
        $page->replaceTag('page_edit_status_review_count', (string)$count_review);
        /* Handle Lead Counts */

        $login_status = $login->verifyPriv('edit_all_leads');
        if ($login_status === true) {
            $perm_sql = '';
        } else {
            $perm_sql = ' WHERE userdb_id = ' . intval($_SESSION['userID']) . ' ';
        }

        //Get Counts

        if ($perm_sql == '') {
            $perm_sql = ' WHERE ';
        } else {
            $perm_sql .= ' AND ';
        }
        $sql2 = 'SELECT count(feedbackdb_id) as count
		FROM ' . $this->config['table_prefix'] . 'feedbackdb ' . $perm_sql . ' feedbackdb_status = \'1\'';
        $recordSet2 = $ORconn->Execute($sql2);
        if (is_bool($recordSet2)) {
            $misc->logErrorAndDie($sql2);
        }
        $active_lead_count = (int)$recordSet2->fields('count');
        $sql2 = 'SELECT count(feedbackdb_id) as count FROM ' . $this->config['table_prefix'] . 'feedbackdb ' . $perm_sql . ' feedbackdb_status = \'0\'';
        $recordSet2 = $ORconn->Execute($sql2);
        if (is_bool($recordSet2)) {
            $misc->logErrorAndDie($sql2);
        }
        $inactive_lead_count = (int)$recordSet2->fields('count');

        //lead_inactive_count
        //lead_active_count
        $page->replaceTag('lead_inactive_count', (string)$inactive_lead_count);
        $page->replaceTag('lead_active_count', (string)$active_lead_count);
        return $page->returnPage();
    }

    /**
     * displayAddons()
     * This functions first calls the add-on install function to make sure it is intalled/updated. Then calls the
     * addons show_admin_icons function.
     *
     * @param string $addon Should be the name of the addon to install and then load.
     *
     * @return string[]  Array or links to show Addon Links
     */
    public function displayAddons(string $addon = ''): array
    {
        $links = [];
        $addonClassName = '\\OpenRealty\\Addons\\' . $addon . '\\Addon';
        if (class_exists($addonClassName)) {
            $addonClass = new $addonClassName($this->dbh, $this->config);
            if (method_exists($addonClass, 'installAddon')) {
                $addonClass->installAddon();
            }
            if (method_exists($addonClass, 'showAdminIcons')) {
                $links = $addonClass->showAdminIcons();
            }
        }
        return $links;
    }

    /**
     * openrealtyLinks()
     * This function displays the Open-Realty Support, Wiki, Defects, and Upgrade Links.
     *
     * @return string HTML For Open-Realty Upgrade links.
     */
    public function openrealtyLinks(): string
    {
        global $lang;
        $display = '';

        if ($this->config['automatic_update_check']) {
            $check_for_updates = $this->updateCheck();
            if ($check_for_updates === true) {
                $display .= '<a id="updatemelink" class="upgrade_true" rel="#upgradeyesno" href="#"> ' . $lang['link_upgrade_available'] . '(' . $_SESSION['updateversion'] . ')</a>';
            }
        } else {
            $display .= '<a id="updatemelink" class="upgrade_false" rel="#upgradeyesno" href="#">' . $lang['link_upgrade_manual'] . '</a>';
        }

        return $display;
    }

    /**
     * getLatestRelease($prerelease=false)
     * This function contacts gitlab to get the latest release
     *
     * @return boolean|object{version:string, download_url:non-empty-string}
     */
    public function getLatestRelease(): bool|object
    {
        $misc = $this->newMisc();
        $result = false;
        // Determine Current Stability
        $prerelease = false;
        $current_stability = VersionParser::parseStability($this->config['version']);
        if ($current_stability != 'stable') {
            $prerelease = true;
        }

        $releases_response = $misc->getUrl('https://gitlab.com/api/v4/projects/appsbytherealryanbonham%2Fopen-realty/releases', 1800);

        if (is_bool($releases_response)) {
            return false;
        } else {
            /** @var mixed */
            $releases = json_decode($releases_response, true);
            if (is_array($releases)) {
                /** @var mixed */
                foreach ($releases as $release) {
                    if (is_array($release)) {
                        $version = (string)$release['tag_name'];
                        $stability = VersionParser::parseStability($version);
                        if (!$prerelease && $stability != 'stable') {
                            continue;
                        }
                        //Check if this version has a download link
                        $download_url = '';
                        if (is_array($release['assets'])) {
                            if (array_key_exists('links', $release['assets'])) {
                                /** @var mixed */
                                foreach ($release['assets']['links'] as $link) {
                                    if (is_array($link)) {
                                        if ($link['name'] == 'Open-Realty-' . $version . '.zip') {
                                            $download_url = (string)$link['direct_asset_url'];
                                            break;
                                        }
                                    }
                                }
                            }
                            if ($download_url !== '') {
                                $result = (object)[
                                    'version' => $version,
                                    'download_url' => $download_url,
                                ];
                                break;
                            }
                        }
                    }
                }
            }
        }
        return $result;
    }

    /**
     * updateCheck()
     * This function Check to see if it is necessary to update Open-Realty. It looks at the Version number in the
     * config table and compares it to the number located in http://www.open-realty.org/release/version.txt. If
     * allow_url_fopen is off, it show a manual update link, which the user cna click on to open the open-realty site.
     *
     * @param boolean $force Force check to call out to internet even if cache has not expired.
     *
     * @return boolean
     */
    public function updateCheck(bool $force = false): bool
    {
        if ($force || (!isset($_SESSION['updatechecked']) || $_SESSION['updatechecked'] < time())) {
            $lastest_version = $this->getLatestRelease();
        } else {
            $lastest_version = (object)[
                'version' => (string)$_SESSION['updateversion'],
                'download_url' => '',
            ];
        }
        if (!is_bool($lastest_version)) {
            $new_version = $lastest_version->version;
            $check = version_compare($this->config['version'], $new_version, '>=');
            //Dont Check for Updates again for 24 Hours durring this session.
            $_SESSION['updatechecked'] = time() + (24 * 60 * 60);
            $_SESSION['updateversion'] = $new_version;
            if ($check) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * Recursively delete directory
     *
     * @param string $dir Directory to delete.
     */
    private function rrmdir(string $dir): void
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != '.' && $object != '..') {
                    if (filetype($dir . '/' . $object) == 'dir') {
                        $this->rrmdir($dir . '/' . $object);
                    } else {
                        unlink($dir . '/' . $object);
                    }
                }
            }
            rmdir($dir);
        }
    }

    /**
     * Download and installs latest update.
     */
    public function doUpgrade(): void
    {
        global $ORconn;
        $misc = $this->newMisc();
        error_reporting(E_ALL ^ E_DEPRECATED);
        $lastest_version = $this->getLatestRelease();
        if (!is_bool($lastest_version)) {
            $download_url = $lastest_version->download_url;
            $misc->logAction('Attempting Upgrading From Open-Realty ' . $this->config['version']);
            $data = $misc->getUrl($download_url);
            if (!is_bool($data)) {
                echo 'Upgrade Downloaded.<br />';
                //Write Upgrade File
                $temp_file = tempnam(sys_get_temp_dir(), 'upgrade' . $this->config['version']);
                $fp = fopen($temp_file, 'wb');
                if (is_bool($fp)) {
                    echo '<div class="alert alert-danger">Failed to write to temp directory.</div>';
                    die;
                } else {
                    fwrite($fp, $data);
                    fclose($fp);
                }

                echo 'Upgrade Extracting...<br/>';
                if (class_exists('ZipArchive')) {
                    $zip = new ZipArchive();
                    $res = $zip->open($temp_file);
                    if ($res === true) {
                        echo 'Removing Old Templates.<br />';
                        //Remove Old Template Files.
                        if (is_dir($this->config['basepath'] . '/template/lazuli')) {
                            $this->rrmdir($this->config['basepath'] . '/template/lazuli');
                        }
                        if (is_dir($this->config['basepath'] . '/template/defualt')) {
                            $this->rrmdir($this->config['basepath'] . '/template/defualt');
                        }
                        if (is_dir($this->config['basepath'] . '/template/cms_integration')) {
                            $this->rrmdir($this->config['basepath'] . '/template/cms_integration');
                        }
                        if (is_dir($this->config['basepath'] . '/template/html5')) {
                            $this->rrmdir($this->config['basepath'] . '/template/html5');
                        }
                        if (is_dir($this->config['basepath'] . '/template/mobile')) {
                            $this->rrmdir($this->config['basepath'] . '/template/mobile');
                        }
                        //Remove Old Admin Template Files.
                        if (is_dir($this->config['basepath'] . '/admin/template/defualt')) {
                            $this->rrmdir($this->config['basepath'] . '/admin/template/defualt');
                        }
                        if (is_dir($this->config['basepath'] . '/admin/template/cms_integration')) {
                            $this->rrmdir($this->config['basepath'] . '/admin/template/cms_integration');
                        }
                        if (is_dir($this->config['basepath'] . '/admin/template/OR_small')) {
                            $this->rrmdir($this->config['basepath'] . '/admin/template/OR_small');
                        }
                        for ($i = 0; $i < $zip->numFiles; $i++) {
                            $zip_name = $zip->getNameIndex($i);
                            $path_parts = pathinfo($this->config['basepath'] . '/' . $zip_name);
                            if (!file_exists($this->config['basepath'] . '/' . $zip_name) && (!isset($path_parts['extension']) || isset($path_parts['filename']) && $path_parts['filename'] == "")) {
                                $success = mkdir($this->config['basepath'] . '/' . $zip_name);
                                if (!$success) {
                                    echo '<div class="alert alert-danger">Failed to create directory: ' . $this->config['basepath'] . '/' . $zip_name . '</div>';
                                    die;
                                }
                            }
                            if (!is_dir($this->config['basepath'] . '/' . $zip_name)) {
                                if ($zip_name == ".htaccess") {
                                    $fileToWrite = $this->config['basepath'] . '/.htaccess.upgrade';
                                } else {
                                    $fileToWrite = $this->config['basepath'] . '/' . $zip_name;
                                }
                                if (file_exists($fileToWrite)) {
                                    if (!is_writable($fileToWrite)) { // Test if the file is writable
                                        echo "Permissions are not writeable $fileToWrite";
                                        die;
                                    }
                                } else {
                                    $fileToWriteDir = dirname($fileToWrite);
                                    if (!file_exists($fileToWriteDir)) {
                                        $success = mkdir($fileToWriteDir);
                                        if (!$success) {
                                            echo '<div class="alert alert-danger">Failed to create directory: ' . $fileToWriteDir . '</div>';
                                            die;
                                        }
                                    }
                                    if (!is_writable($fileToWriteDir)) { // Test if the file is writable
                                        echo "Permissions are not writeable $fileToWriteDir";
                                        die;
                                    }
                                }

                                $fp = fopen($fileToWrite, 'wb');
                                if (!is_resource($fp)) { // Test if PHP could open the file
                                    echo "Could not open $fileToWrite for writting.";
                                    die;
                                }
                                $buf = $zip->getStream($zip->getNameIndex($i));
                                stream_copy_to_stream($buf, $fp);
                                fclose($buf);
                                fclose($fp);
                            }
                        }
                        echo "Extraction Complete.<br />";
                        $zip->close();
                        unlink($temp_file);

                        //Run installer

                        echo 'Upgrading Database...<br/>';
                        $display = $misc->getUrl($this->config['baseurl'] . '/install/index.php?step=autoupdate&or_install_lang=en&or_install_type=Upgrader');
                        if ($display === false) {
                            echo 'Calling Installer Failed, check site logs in Open-Realty admin. <br />';
                            die;
                        }
                        echo $display;
                        //Remove Install
                        if (is_dir($this->config['basepath'] . '/install')) {
                            $this->rrmdir($this->config['basepath'] . '/install');
                        }
                        $old_version = $this->config['version'];
                        $sql = 'SELECT controlpanel_version 
                                FROM ' . $this->config['table_prefix_no_lang'] . 'controlpanel';
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            $misc->logErrorAndDie($sql);
                        }
                        // Loop throught Control Panel and save to Array
                        $this->config['version'] = (string)$recordSet->fields('controlpanel_version');
                        if ($old_version == $this->config['version']) {
                            echo 'Post Version Sanity Check Failed. Stopping Update Process Contact Support.<br />';
                            die;
                        } else {
                            echo 'Upgrade Complete From Version ' . $old_version . ' to ' . $this->config['version'] . '<br />';
                            $misc->logAction('Upgrade Complete From Version ' . $old_version . ' to ' . $this->config['version']);
                        }
                        //Refresh the update check
                        $this->updateCheck(true);
                    }
                }
            }
        } else {
            echo 'Failed Getting Upgrade Data';
        }
    }

    /**
     * generalInfo()
     * This displays the general information section on the index page. It is showing the following information.
     *
     * @return string HTML Showing General Information About Site
     * @see agentCount()
     * @see listingCount()
     */
    public function generalInfo(): string
    {
        global $lang;
        $display = '<div >
						<a href="' . $this->config['baseurl'] . '/admin/index.php?action=edit_listings">
							<span class="general_info_cap">' . $lang['total_listings'] . '</span> <span class="general_info_data">' . $this->listingCount() . '</span>
						</a>
					</div>
					<div>
						<a href="javascript:document.getElementById(\'edit_active\').submit()">
							<span class="general_info_cap">' . $lang['active_listings'] . '</span> <span class="general_info_data">' . $this->listingCount('yes') . '</span>
						</a>
					</div>
					<div>
						<a href="javascript:document.getElementById(\'edit_inactive\').submit()">
						<span class="general_info_cap">' . $lang['inactive_listings'] . '</span> <span class="general_info_data">' . $this->listingCount('no') . '</span>
						</a>
					</div>
					<div>
						<a href="javascript:document.getElementById(\'edit_featured\').submit()">
							<span class="general_info_cap">' . $lang['featured_listings'] . '</span> <span class="general_info_data">' . $this->listingCount('featured') . '</span>
						</a>
					</div>';
        if ($this->config['use_expiration']) {
            $display .= '<div>
							<a href="javascript:document.getElementById(\'edit_expired\').submit()">
								<span class="general_info_cap">' . $lang['expired_listings'] . '</span> <span class="general_info_data">' . $this->listingCount('expired') . '</span>
							</a>
						</div>';
        }
        $display .= '<div>
						<a href="javascript:document.getElementById(\'edit_agents\').submit()">
							<span class="general_info_cap">' . $lang['number_of_agents'] . '</span> <span class="general_info_data">' . $this->agentCount() . '</span>
						</a>
					</div>
					<div>
						<a href="javascript:document.getElementById(\'edit_members\').submit()">
							<span class="general_info_cap">' . $lang['number_of_members'] . '</span> <span class="general_info_data">' . $this->memberCount() . '</span>
						</a>
					</div>';

        $display .= '<div id="HiddenFilterForm" style="display:none">';
        $display .= '<form id="edit_active" action="' . $this->config['baseurl'] . '/admin/index.php?action=edit_listings" method="post"><input type="hidden" name="token" value="{csrf_token}" /><fieldset><input type="hidden" name="filter" value="active" /></fieldset></form>';
        $display .= '<form id="edit_inactive" action="' . $this->config['baseurl'] . '/admin/index.php?action=edit_listings" method="post"><input type="hidden" name="token" value="{csrf_token}" /><fieldset><input type="hidden" name="filter" value="inactive" /></fieldset></form>';
        $display .= '<form id="edit_featured" action="' . $this->config['baseurl'] . '/admin/index.php?action=edit_listings" method="post"><input type="hidden" name="token" value="{csrf_token}" /><fieldset><input type="hidden" name="filter" value="featured" /></fieldset></form>';
        if ($this->config['use_expiration']) {
            $display .= '<form id="edit_expired" action="' . $this->config['baseurl'] . '/admin/index.php?action=edit_listings" method="post"><input type="hidden" name="token" value="{csrf_token}" /><fieldset><input type="hidden" name="filter" value="expired" /></fieldset></form>';
        }
        $display .= '<form id="edit_agents" action="' . $this->config['baseurl'] . '/admin/index.php?action=user_manager" method="post"><input type="hidden" name="token" value="{csrf_token}" /><fieldset><input type="hidden" name="filter" value="agents" /></fieldset></form>';
        $display .= '<form id="edit_members" action="' . $this->config['baseurl'] . '/admin/index.php?action=user_manager" method="post"><input type="hidden" name="token" value="{csrf_token}" /><fieldset><input type="hidden" name="filter" value="members" /></fieldset></form>';
        $display .= '</div>';
        return $display;
    }

    /**
     * agentCount()
     * Returns the number of agents currently in the database.
     *
     * @return integer Number of agents
     */
    public function agentCount(): int
    {
        global $ORconn;
        $misc = $this->newMisc();
        $agent_count_sql = 'SELECT count(userdb_id)
							AS agent_count
							FROM ' . $this->config['table_prefix'] . 'userdb
							WHERE userdb_is_agent = \'yes\'';
        $agent_count = $ORconn->Execute($agent_count_sql);
        if (is_bool($agent_count)) {
            $misc->logErrorAndDie($agent_count_sql);
        }
        return (int)$agent_count->fields('agent_count');
    }

    /**
     * listingCount()
     * Returns the number of listings currently in the database.
     *
     * @param string $view View of listings get count for. Possible values: all, yes, no featured, expired.
     *
     * @return integer Number of listings found.
     */
    public function listingCount(string $view = 'all'): int
    {
        global $ORconn;
        $misc = $this->newMisc();
        $listing_count_sql = '';
        if ($view == 'all') {
            $listing_count_sql = 'SELECT count(listingsdb_id) as listing_count FROM ' . $this->config['table_prefix'] . 'listingsdb';
        } elseif ($view == 'yes') {
            $listing_count_sql = 'SELECT count(listingsdb_id) as listing_count FROM ' . $this->config['table_prefix'] . 'listingsdb WHERE listingsdb_active = \'yes\'';
        } elseif ($view == 'no') {
            $listing_count_sql = 'SELECT count(listingsdb_id) as listing_count FROM ' . $this->config['table_prefix'] . 'listingsdb WHERE listingsdb_active = \'no\'';
        } elseif ($view == 'featured') {
            $listing_count_sql = 'SELECT count(listingsdb_id) as listing_count FROM ' . $this->config['table_prefix'] . 'listingsdb WHERE listingsdb_featured = \'yes\'';
        } elseif ($view == 'expired') {
            $listing_count_sql = 'SELECT count(listingsdb_id) as listing_count FROM ' . $this->config['table_prefix'] . 'listingsdb WHERE listingsdb_expiration < ' . $ORconn->DBDate(time());
        }

        $listing_count = $ORconn->Execute($listing_count_sql);
        if (is_bool($listing_count)) {
            $misc->logErrorAndDie($listing_count_sql);
        }
        return (int)$listing_count->fields('listing_count');
    }

    /**
     * memberCount()
     * Returns the number of members currently in the database.
     *
     * @return integer Number of members
     */
    public function memberCount(): int
    {
        global $ORconn;
        $misc = $this->newMisc();
        $member_count_sql = 'SELECT count(userdb_id) as member_count
							FROM ' . $this->config['table_prefix'] . 'userdb
							WHERE userdb_is_agent = \'no\'
							AND userdb_is_admin = \'no\'';
        $member_count = $ORconn->Execute($member_count_sql);
        if (is_bool($member_count)) {
            $misc->logErrorAndDie($member_count_sql);
        }
        return (int)$member_count->fields('member_count');
    }
}
