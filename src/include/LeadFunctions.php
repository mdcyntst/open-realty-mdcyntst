<?php

declare(strict_types=1);

namespace OpenRealty;

class LeadFunctions extends BaseClass
{
    public function updateFeedbackData(int $new_feedback_id, int $user): string
    {
        // UPDATES THE FEEDBACK INFORMATION
        global $ORconn;

        $misc = $this->newMisc();
        $sql_feedback_id = $misc->makeDbSafe($new_feedback_id);

        $sql = 'DELETE FROM ' . $this->config['table_prefix'] . "feedbackdbelements
				WHERE feedbackdb_id = $sql_feedback_id";
        $recordSet = $ORconn->Execute($sql);

        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        foreach ($_POST as $ElementIndexValue => $ElementContents) {
            // first, ignore all the stuff that's been taken care of above

            if ($ElementIndexValue == 'listingID') {
                continue;
            }
            if ($ElementIndexValue == 'notes') {
                continue;
            } elseif ($ElementIndexValue == 'formaction') {
                continue;
            } elseif ($ElementIndexValue == 'captcha_code') {
                continue;
            } elseif (is_string($ElementIndexValue) && preg_match('/asmSelect[0-9]+/i', $ElementIndexValue)) {
                continue;
            } elseif ($ElementIndexValue == 'PHPSESSID') {
                continue;
            } elseif ($ElementIndexValue == 'edit') {
                continue;
            } elseif ($ElementIndexValue == 'edit_expiration') {
                continue;
            } elseif ($ElementIndexValue == 'user') {
                continue;
            } elseif (is_array($ElementContents)) {
                // deal with checkboxes & multiple selects elements
                $feature_insert = '';
                foreach ($ElementContents as $feature_item) {
                    if (is_scalar($feature_item)) {
                        $feature_insert = "$feature_insert||$feature_item";
                    }
                } // end while

                // now remove the first two characters
                $feature_insert_length = strlen($feature_insert);
                $feature_insert_length = $feature_insert_length - 2;
                $feature_insert = substr($feature_insert, 2, $feature_insert_length);
                $sql_ElementIndexValue = $misc->makeDbSafe($ElementIndexValue);
                $sql_feature_insert = $misc->makeDbSafe($feature_insert);
                $sql_owner = $misc->makeDbSafe($user);
                $sql_feedback_id = $misc->makeDbSafe($new_feedback_id);

                $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'feedbackdbelements (feedbackdbelements_field_name, feedbackdbelements_field_value, feedbackdb_id, userdb_id)
                		VALUES (' . $sql_ElementIndexValue . ', ' . $sql_feature_insert . ', ' . $sql_feedback_id . ', ' . $sql_owner . ')';
                $recordSet = $ORconn->Execute($sql);

                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
            } else {
                // process the form
                $sql_ElementIndexValue = $misc->makeDbSafe($ElementIndexValue);
                $sql_ElementContents = $misc->makeDbSafe($ElementContents);
                $sql_owner = $misc->makeDbSafe($user);
                $sql_feedback_id = $misc->makeDbSafe($new_feedback_id);

                $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "feedbackdbelements (feedbackdbelements_field_name, feedbackdbelements_field_value, feedbackdb_id, userdb_id)
						VALUES ($sql_ElementIndexValue, $sql_ElementContents, $sql_feedback_id, $sql_owner)";
                $recordSet = $ORconn->Execute($sql);

                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
            } // end else
        } // end while

        return 'success';
    }

    public function getFeedbackModData(int $feedbackID, string $datetype): string
    {
        // get the main data for a given feedback
        global $ORconn;

        $user = $this->newUser();
        $misc = $this->newMisc();

        if ($datetype == 'created') {
            $select_txt = 'feedbackdb_creation_date';
        } elseif ($datetype == 'modified') {
            $select_txt = 'feedbackdb_last_modified';
        } elseif ($datetype == 'modifiedby') {
            $select_txt = 'feedbackdb_last_modified_by';
        } else {
            $select_txt = '';
        }

        $feedbackID = $misc->makeDbExtraSafe($feedbackID);

        $sql = 'SELECT ' . $select_txt . '
        		FROM ' . $this->config['table_prefix'] . 'feedbackdb
        		WHERE feedbackdb_id = ' . $feedbackID;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }

        // get last_modified date

        if ($datetype == 'modifiedby') {
            $user_id = (int)$recordSet->fields($select_txt);

            if ($user_id == 0) {
                $feedback_last_modified = 'Unmodified';
            } else {
                $feedback_last_modified = $user->getUserSingleItem('userdb_user_first_name', $user_id) . ', ' . $user->getUserSingleItem('userdb_user_last_name', $user_id);
            }
        } else {
            $feedback_last_modified = $recordSet->UserTimeStamp($recordSet->fields($select_txt), 'm-d-Y g:ia');
        }

        return $feedback_last_modified;
    }

    /**
     * @return true
     */
    public function setNotes(int $feedback_id, string $notes): bool
    {
        global $ORconn;

        $misc = $this->newMisc();
        $feedback_id = intval($feedback_id);
        $sql_notes = $misc->makeDbSafe($notes);
        $sql = 'UPDATE  ' . $this->config['table_prefix'] . 'feedbackdb
        		SET feedbackdb_notes = ' . $sql_notes . '
        		WHERE feedbackdb_id = ' . intval($feedback_id);
        $recordSet = $ORconn->Execute($sql);

        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        return true;
    }

    /**
     * @return true
     */
    public function setFeedbackMods(int $feedback_id): bool
    {
        global $ORconn;

        $misc = $this->newMisc();
        $sql_user_id = intval($_SESSION['userID']);
        $sql = 'UPDATE  ' . $this->config['table_prefix'] . 'feedbackdb
        		SET feedbackdb_last_modified_by = ' . $sql_user_id . ', feedbackdb_last_modified = ' . $ORconn->DBTimeStamp(time()) . '
        		WHERE feedbackdb_id = ' . $feedback_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        return true;
    }

    /**
     * @return true
     */
    public function setFeedbackStatus(int $feedback_id, int $status): bool
    {
        global $ORconn;

        $misc = $this->newMisc();
        $sql = 'UPDATE  ' . $this->config['table_prefix'] . 'feedbackdb
        		SET feedbackdb_status = ' . $status . '
        		WHERE feedbackdb_id = ' . $feedback_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        return true;
    }

    /**
     * @return true
     */
    public function setFeedbackPriority(int $feedback_id, string $priority): bool
    {
        global $ORconn;

        $misc = $this->newMisc();
        $sql_priority = $misc->makeDbSafe($priority);
        $sql = 'UPDATE  ' . $this->config['table_prefix'] . 'feedbackdb
        		SET feedbackdb_priority = ' . $sql_priority . '
        		WHERE feedbackdb_id = ' . $feedback_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        return true;
    }

    public function getLeadmanagerPriority(int $feedback_id, string $template): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $page = $this->newPageAdmin();


        $sql = 'SELECT feedbackdb_priority
        			FROM ' . $this->config['table_prefix'] . 'feedbackdb
        			WHERE feedbackdb_id = ' . $feedback_id;

        $recordSet = $ORconn->Execute($sql);

        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }

        $priority = $recordSet->fields('feedbackdb_priority');
        $priority_options = ['Low' => 'Low', 'Normal' => 'Normal', 'Urgent' => 'Urgent'];

        $html = $page->getTemplateSection('leadmanager_priority_block', $template);
        $html = $page->formOptions($priority_options, $priority, $html);
        return $page->replaceTemplateSection('leadmanager_priority_block', $html, $template);
    }


    public function getFeedbackOwner(int $feedback_id): int
    {
        global $ORconn;

        $misc = $this->newMisc();
        $sql = 'SELECT userdb_id
        		FROM ' . $this->config['table_prefix'] . "feedbackdb
        		WHERE feedbackdb_id = $feedback_id";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        return (int)$recordSet->fields('userdb_id');
    }

    public function getFeedbackFormelements(): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $forms = $this->newForms();

        $output = '';

        //Builds the form  on the add_feedback page
        $sql = 'SELECT feedbackformelements_id, feedbackformelements_field_type, feedbackformelements_field_name, feedbackformelements_field_caption, feedbackformelements_default_text, feedbackformelements_field_elements, feedbackformelements_rank, feedbackformelements_required
    	FROM ' . $this->config['table_prefix'] . 'feedbackformelements
       	ORDER BY feedbackformelements_rank, feedbackformelements_field_name';
        $recordSet = $ORconn->Execute($sql);

        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }

        while (!$recordSet->EOF) {
            $field_type = $recordSet->fields('feedbackformelements_field_type');
            $field_name = $recordSet->fields('feedbackformelements_field_name');
            $field_caption = $recordSet->fields('feedbackformelements_field_caption');
            $default_text = $recordSet->fields('feedbackformelements_default_text');
            $field_elements = $recordSet->fields('feedbackformelements_field_elements');
            $required = $recordSet->fields('feedbackformelements_required');

            //renderFormElement($field_type, $field_name, $field_value, $field_caption, $default_text, $required, $field_elements, $field_length='', $tool_tip='')
            $field_value = '';
            //print_r($_POST);
            if (isset($_POST[$field_name]) && is_string($_POST[$field_name])) {
                $field_value = htmlentities($_POST[$field_name], ENT_COMPAT, $this->config['charset']);
            }
            $output .= $forms->renderFormElement($field_type, $field_name, $field_value, $field_caption, $default_text, $required, $field_elements);

            $recordSet->MoveNext();
        } // end while

        return $output;
    }

    public function renderSingleFeedbackItem(int $feedbackID, string $name, string $display_type = 'both'): string
    {
        // renders a single item on the view leads page
        // includes the caption
        global $ORconn, $lang;
        $misc = $this->newMisc();
        $display = '';
        $name = $misc->makeDbExtraSafe($name);
        $sql = 'SELECT feedbackdbelements_field_value, feedbackformelements_field_type, feedbackformelements_field_caption
				FROM ' . $this->config['table_prefix'] . 'feedbackdbelements, ' . $this->config['table_prefix'] . "feedbackformelements
				WHERE ((feedbackdb_id = $feedbackID)
				AND (feedbackformelements_field_name = feedbackdbelements_field_name)
				AND (feedbackdbelements_field_name = " . $name . '))';
        $recordSet = $ORconn->Execute($sql);

        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }

        while (!$recordSet->EOF) {
            $field_value = $recordSet->fields('feedbackdbelements_field_value');
            $field_type = $recordSet->fields('feedbackformelements_field_type');
            $field_caption = $recordSet->fields('feedbackformelements_field_caption');

            if ($field_type == 'divider') {
                $display .= "<br /><strong>$field_caption</strong>";
            } elseif ($field_value != '') {
                if ($display_type === 'both' || $display_type === 'caption') {
                    $display .= '<span class="field_caption">' . $field_caption . '</span>';
                }
                if ($display_type == 'both') {
                    $display .= ':&nbsp;';
                }
                if ($display_type === 'both' || $display_type === 'value') {
                    if ($field_type == 'select-multiple' or $field_type == 'option' or $field_type == 'checkbox') {
                        // handle field types with multiple options
                        // $display .= "<br /><strong>$field_caption</strong>";
                        $feature_index_list = explode('||', $field_value);
                        sort($feature_index_list);
                        $list_count = count($feature_index_list);
                        $l = 1;
                        foreach ($feature_index_list as $feature_list_item) {
                            if ($l < $list_count) {
                                $display .= htmlentities($feature_list_item, ENT_NOQUOTES, $this->config['charset']);
                                $display .= $this->config['feature_list_separator'];
                                $l++;
                            } else {
                                $display .= htmlentities($feature_list_item, ENT_NOQUOTES, $this->config['charset']);
                            }
                        } // end while
                    } elseif ($field_type == 'price') {
                        $field_value = htmlentities($field_value, ENT_NOQUOTES, $this->config['charset']);
                        $money_amount = $misc->internationalNumFormat((float)$field_value, $this->config['number_decimals_price_fields']);
                        $display .= $misc->moneyFormats($money_amount);
                    } elseif ($field_type == 'number') {
                        $field_value = htmlentities($field_value, ENT_NOQUOTES, $this->config['charset']);
                        $display .= $misc->internationalNumFormat((float)$field_value, $this->config['number_decimals_number_fields']);
                    } elseif ($field_type == 'url') {
                        $field_value = htmlentities($field_value, ENT_NOQUOTES, $this->config['charset']);
                        $display .= "<a href=\"$field_value\" onclick=\"window.open(this.href,'_blank','location=1,resizable=1,status=1,scrollbars=1,toolbar=1,menubar=1,noopener,noreferrer');return false\">$field_value</a>";
                    } elseif ($field_type == 'email') {
                        $field_value = htmlentities($field_value, ENT_NOQUOTES, $this->config['charset']);
                        $display .= "<a href=\"mailto:$field_value\">$field_value</a>";
                    } elseif ($field_type == 'text' or $field_type == 'textarea') {
                        $field_value = htmlentities($field_value, ENT_NOQUOTES, $this->config['charset']);
                        if ($this->config['add_linefeeds']) {
                            $field_value = nl2br($field_value); //replace returns with <br />
                        } // end if
                        $display .= $field_value;
                    } elseif ($field_type == 'date') {
                        $field_value = $misc->convertTimestamp($field_value);
                        $display .= "<br />$field_value";
                    } else {
                        $field_value = htmlentities($field_value, ENT_NOQUOTES, $this->config['charset']);
                        $display .= $field_value;
                    } // end else
                }
                if ($display_type === 'rawvalue') {
                    $display .= $field_value;
                }
            } else {
                if ($field_type == 'price' && $display_type !== 'rawvalue' && $this->config['zero_price']) {
                    $display .= $lang['call_for_price'] . '<br />';
                } // end if
            } // end else
            $recordSet->MoveNext();
        } // end while
        return $display;
    }

    public function sendAgentLeadAssignedNotice(int $feedback_id, int $modified_by): void
    {
        global $ORconn;

        $misc = $this->newMisc();
        $user = $this->newUser();

        $page = $this->newPageUser();
        $page->loadPage($this->config['admin_template_path'] . '/email/lead_assigned.html');
        $sql_feedback_id = intval($feedback_id);
        $lead_url = $this->config['baseurl'] . '/admin/index.php?action=leadmanager_my_feedback_edit&feedback_id=' . $feedback_id;
        $via = $_SERVER['REMOTE_ADDR'] ?? '';
        $via .= ' -- ' . date('F j, Y, g:i:s a');
        //Lookup Lead Information
        $sql = 'SELECT listingdb_id, userdb_id, feedbackdb_member_userdb_id FROM ' . $this->config['table_prefix'] . 'feedbackdb WHERE feedbackdb_id = ' . $sql_feedback_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $agent_id = $recordSet->fields('userdb_id');
        $agent_email = $user->getUserSingleItem('userdb_emailaddress', $agent_id);
        $member_id = $recordSet->fields('feedbackdb_member_userdb_id');
        $listing_id = $recordSet->fields('listingdb_id');

        $page->replaceTag('lead_id', (string)$sql_feedback_id);
        $page->replaceTag('lead_url', $lead_url);
        $page->replaceTag('via', $via);

        if ($listing_id > 0) {
            $page->replaceListingFieldTags($listing_id);
            $page->page = $page->removeTemplateBlock('agent_contact', $page->page);
            $page->page = $page->cleanupTemplateBlock('listing_contact', $page->page);
        } else {
            $page->page = $page->removeTemplateBlock('listing_contact', $page->page);
            $page->page = $page->cleanupTemplateBlock('agent_contact', $page->page);
        }
        $page->replaceLeadFieldTags($sql_feedback_id);
        $page->replaceUserFieldTags($member_id);
        $page->replaceUserFieldTags($modified_by, '', 'assigned_by');
        $subject = $page->getTemplateSection('subject_block');
        $page->page = $page->removeTemplateBlock('subject', $page->page);
        $page->autoReplaceTags();
        $message = $page->returnPage();
        if (isset($this->config['site_email']) && $this->config['site_email'] != '') {
            $sender_email = $this->config['site_email'];
        } else {
            $sender_email = $this->config['admin_email'];
        }
        $misc->sendEmail($this->config['admin_name'], $sender_email, $agent_email, $message, $subject, true);
    }

    public function sendAgentFeedbackNotice(int $feedback_id): string
    {
        // UPDATES THE FEEDBACK INFORMATION
        global $ORconn;

        $misc = $this->newMisc();
        $user = $this->newUser();

        $page = $this->newPageUser();
        $page->loadPage($this->config['admin_template_path'] . '/email/agent_lead_notification.html');
        $lead_url = $this->config['baseurl'] . '/admin/index.php?action=leadmanager_my_feedback_edit&feedback_id=' . $feedback_id;
        $via = $_SERVER['REMOTE_ADDR'] ?? '';
        $via .= ' -- ' . date('F j, Y, g:i:s a');
        //Lookup Lead Information
        $sql = 'SELECT listingdb_id, userdb_id, feedbackdb_member_userdb_id 
				FROM ' . $this->config['table_prefix'] . 'feedbackdb 
				WHERE feedbackdb_id = ' . $feedback_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $agent_id = (int)$recordSet->fields('userdb_id');
        $agent_email = $user->getUserSingleItem('userdb_emailaddress', $agent_id);
        $member_id = (int)$recordSet->fields('feedbackdb_member_userdb_id');
        $listing_id = (int)$recordSet->fields('listingdb_id');

        $page->replaceTag('lead_id', (string)$feedback_id);
        $page->replaceTag('lead_url', $lead_url);
        $page->replaceTag('via', $via);
        if ($listing_id > 0) {
            $page->replaceListingFieldTags($listing_id);
            $page->page = $page->removeTemplateBlock('agent_contact', $page->page);
            $page->page = $page->cleanupTemplateBlock('listing_contact', $page->page);
        } else {
            $page->page = $page->removeTemplateBlock('listing_contact', $page->page);
            $page->page = $page->cleanupTemplateBlock('agent_contact', $page->page);
        }
        $page->replaceLeadFieldTags($feedback_id);
        $page->replaceUserFieldTags($member_id);
        //Get Member Email and First and Last Name.
        $member_email = $user->getUserSingleItem('userdb_emailaddress', $member_id);
        $member_name = $user->getUserSingleItem('userdb_user_last_name', $member_id) . ', ' . $user->getUserSingleItem('userdb_user_first_name', $member_id);

        $subject = $page->getTemplateSection('subject_block');
        $page->page = $page->removeTemplateBlock('subject', $page->page);
        $page->autoReplaceTags();
        $message = $page->returnPage();
        if (isset($this->config['site_email']) && $this->config['site_email'] != '') {
            $sender_email = $this->config['site_email'];
        } else {
            $sender_email = $this->config['admin_email'];
        }
        $misc->sendEmail($this->config['admin_name'], $sender_email, $agent_email, $message, $subject, true, false, $member_name, $member_email);
        return 'success';
    }

    public function sendUserFeedbackNotice(int $feedback_id): string
    {
        // UPDATES THE FEEDBACK INFORMATION
        global $ORconn;


        $misc = $this->newMisc();
        $user = $this->newUser();

        $page = $this->newPageUser();
        $page->loadPage($this->config['admin_template_path'] . '/email/user_lead_notification.html');
        $via = $_SERVER['REMOTE_ADDR'] ?? '';
        $via .= ' -- ' . date('F j, Y, g:i:s a');
        //Lookup Lead Information
        $sql = 'SELECT listingdb_id, userdb_id, feedbackdb_member_userdb_id 
				FROM ' . $this->config['table_prefix'] . 'feedbackdb 
				WHERE feedbackdb_id = ' . $feedback_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $agent_id = (int)$recordSet->fields('userdb_id');

        $member_id = (int)$recordSet->fields('feedbackdb_member_userdb_id');
        $member_email = $user->getUserSingleItem('userdb_emailaddress', $member_id);
        $listing_id = (int)$recordSet->fields('listingdb_id');

        $page->replaceTag('lead_id', (string)$feedback_id);
        $page->replaceTag('via', $via);

        if ($listing_id > 0) {
            $page->replaceListingFieldTags($listing_id);
            $page->page = $page->removeTemplateBlock('agent_contact', $page->page);
            $page->page = $page->cleanupTemplateBlock('listing_contact', $page->page);
        } else {
            $page->page = $page->removeTemplateBlock('listing_contact', $page->page);
            $page->page = $page->cleanupTemplateBlock('agent_contact', $page->page);
        }
        $page->replaceLeadFieldTags($feedback_id);
        $page->replaceUserFieldTags($member_id);
        $page->replaceUserFieldTags($agent_id, '', 'agent');
        $subject = $page->getTemplateSection('subject_block');
        $page->page = $page->removeTemplateBlock('subject', $page->page);
        $page->autoReplaceTags();
        $message = $page->returnPage();
        if (isset($this->config['site_email']) && $this->config['site_email'] != '') {
            $sender_email = $this->config['site_email'];
        } else {
            $sender_email = $this->config['admin_email'];
        }
        $misc->sendEmail($this->config['admin_name'], $sender_email, $member_email, $message, $subject, true);
        return 'success';
    }

    public function renderTemplateArea(string $templateArea, int $feedbackID): string
    {
        // renders all the elements in a given template area on the listing pages
        global $ORconn, $lang;

        $misc = $this->newMisc();

        $templateArea = $misc->makeDbExtraSafe($templateArea);
        $sql = 'SELECT feedbackdbelements_field_value, feedbackformelements_id, feedbackformelements_field_type,
				feedbackformelements_field_caption
				FROM ' . $this->config['table_prefix'] . 'feedbackdbelements, ' . $this->config['table_prefix'] . 'feedbackformelements
				WHERE ((' . $this->config['table_prefix'] . "feedbackdbelements.feedbackdb_id = $feedbackID)
				AND (feedbackformelements_field_name = feedbackdbelements_field_name)
				AND (feedbackformelements_location = $templateArea))
				ORDER BY feedbackformelements_rank ASC";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $display = '';
        while (!$recordSet->EOF) {
            $form_elements_id = $recordSet->fields('feedbackformelements_id');
            $field_type = $recordSet->fields('feedbackformelements_field_type');

            $field_value = $recordSet->fields('feedbackdbelements_field_value');
            if (!isset($_SESSION['users_lang'])) {
                // Hold empty string for translation fields, as we are workgin with teh default lang
                $field_caption = $recordSet->fields('feedbackformelements_field_caption');
            } else {
                $lang_sql = 'SELECT feedbackformelements_field_caption
				FROM ' . $this->config['lang_table_prefix'] . "feedbackformelements
				WHERE feedbackformelements_id = $form_elements_id";
                $lang_recordSet = $ORconn->Execute($lang_sql);
                if (is_bool($lang_recordSet)) {
                    $misc->logErrorAndDie($lang_sql);
                }
                $field_caption = $lang_recordSet->fields('feedbackformelements_field_caption');
            }

            if ($field_type == 'divider') {
                $display .= '<br /><span class="normal_caption">$field_caption</span>';
            } elseif ($field_value != '') {
                if ($field_type == 'select-multiple' or $field_type == 'option' or $field_type == 'checkbox') {
                    // handle field types with multiple options
                    //$display .= "<br /><strong>$field_caption</strong><br />";
                    $display .= '<div class="multiple_options_caption">' . $field_caption . '</div>';

                    $feature_index_list = explode('||', $field_value);
                    sort($feature_index_list);
                    $list_count = count($feature_index_list);
                    $l = 1;
                    $display .= '<div class="multiple_options">';
                    $display .= '<ul>';
                    foreach ($feature_index_list as $feature_list_item) {
                        if ($l < $list_count) {
                            $display .= '<li>';
                            $feature_list_item = htmlentities($feature_list_item, ENT_NOQUOTES, $this->config['charset']);
                            $display .= $feature_list_item;
                            $display .= $this->config['feature_list_separator'];
                            $display .= '</li>';
                            $l++;
                        } else {
                            $display .= '<li>';
                            $feature_list_item = htmlentities($feature_list_item, ENT_NOQUOTES, $this->config['charset']);
                            $display .= $feature_list_item;
                            $display .= '</li>';
                        }
                    } // end while
                    $display .= '</ul>';
                    $display .= '</div>';
                } elseif ($field_type == 'price') {
                    $money_amount = $misc->internationalNumFormat($field_value, $this->config['number_decimals_price_fields']);
                    $display .= "<strong>$field_caption</strong>: " . $misc->moneyFormats($money_amount);
                } elseif ($field_type == 'number') {
                    $display .= "<strong>$field_caption</strong>: " . $misc->internationalNumFormat($field_value, $this->config['number_decimals_number_fields']);
                } elseif ($field_type == 'url') {
                    $field_value = htmlentities($field_value, ENT_NOQUOTES, $this->config['charset']);
                    $display .= "<strong>$field_caption</strong>: <a href=\"$field_value\" onclick=\"window.open(this.href,'_blank','location=1,resizable=1,status=1,scrollbars=1,toolbar=1,menubar=1,noopener,noreferrer');return false\">$field_value</a>";
                } elseif ($field_type == 'email') {
                    $field_value = htmlentities($field_value, ENT_NOQUOTES, $this->config['charset']);
                    $display .= "<strong>$field_caption</strong>: <a href=\"mailto:$field_value\">$field_value</a>";
                } elseif ($field_type == 'text' or $field_type == 'textarea') {
                    $field_value = htmlentities($field_value, ENT_NOQUOTES, $this->config['charset']);
                    if ($this->config['add_linefeeds']) {
                        $field_value = nl2br($field_value); //replace returns with <br />
                    } // end if
                    $display .= "<strong>$field_caption</strong>: $field_value";
                } elseif ($field_type == 'date') {
                    if ($this->config['date_format'] == 1) {
                        $format = 'm/d/Y';
                    } elseif ($this->config['date_format'] == 2) {
                        $format = 'Y/d/m';
                    } else {
                        $format = 'd/m/Y';
                    }
                    $field_value = date($format, $field_value);
                    $display .= "<strong>$field_caption</strong>: $field_value";
                } else {
                    $field_value = htmlentities($field_value, ENT_NOQUOTES, $this->config['charset']);
                    $display .= "<strong>$field_caption</strong>: $field_value";
                } // end else
                $display .= '<br />';
            } else {
                if ($field_type == 'price' && $this->config['zero_price']) {
                    $display .= "<strong>$field_caption</strong>: " . $lang['call_for_price'] . '<br />';
                } // end if
            } // end else

            $recordSet->MoveNext();
        } // end while
        return $display;
    }

    public function editFormPreview(): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $forms = $this->newForms();

        $page = $this->newPageAdmin();
        $page->loadPage($this->config['admin_template_path'] . '/lead_forms_preview.html');

        //Load Template Area From Config
        $sections = explode(',', $this->config['template_lead_sections']);
        $template_holder = ['misc_hold' => ''];
        foreach ($sections as $section) {
            if (str_contains($page->page, $section)) {
                $template_holder[$section] = '';
            }
        }

        $sql = 'SELECT feedbackformelements_field_type, feedbackformelements_field_name, feedbackformelements_field_caption,
				feedbackformelements_default_text, feedbackformelements_field_elements, feedbackformelements_required, feedbackformelements_tool_tip,
				feedbackformelements_location
				FROM ' . $this->config['table_prefix'] . 'feedbackformelements
				ORDER BY feedbackformelements_rank, feedbackformelements_field_name';
        $recordSet = $ORconn->Execute($sql);

        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        while (!$recordSet->EOF) {
            $field_type = $recordSet->fields('feedbackformelements_field_type');
            $field_name = $recordSet->fields('feedbackformelements_field_name');
            $field_caption = $recordSet->fields('feedbackformelements_field_caption');
            $default_text = $recordSet->fields('feedbackformelements_default_text');
            $field_elements = $recordSet->fields('feedbackformelements_field_elements');
            $required = $recordSet->fields('feedbackformelements_required');
            $tool_tip = $recordSet->fields('feedbackformelements_tool_tip');
            $location = $recordSet->fields('feedbackformelements_location');

            //$output .= $forms->renderFormElement($field_type, $field_name, '', $field_caption, $default_text, $required,$field_elements);

            $field = $forms->renderFormElement($field_type, $field_name, '', $field_caption, $default_text, $required, $field_elements, '', $tool_tip);

            if (array_key_exists($location, $template_holder)) {
                $template_holder[$location] .= $field;
            } else {
                $template_holder['misc_hold'] .= $field;
            }

            $recordSet->MoveNext();
        } // end while

        foreach ($template_holder as $tag => $value) {
            $page->page = str_replace('{' . $tag . '}', $value, $page->page);
        }

        $page->replaceLangTemplateTags();
        $page->replacePermissionTags();
        $page->autoReplaceTags('', true);
        return $page->returnPage();
    }
}
