<?php

declare(strict_types=1);

namespace OpenRealty;

use Exception;
use OpenRealty\Api\Commands\FieldsApi;
use OpenRealty\Api\Commands\ListingApi;
use OpenRealty\Api\Commands\UserApi;

class UserManagement extends BaseClass
{
    public function createBaseUser(string $type, string $user_name, string $email, string $password, string $active, string $fist_name, string $last_name): int
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql_user_name = $misc->makeDbExtraSafe($user_name);
        $hash = password_hash($password, PASSWORD_DEFAULT);
        $sqh_hash = $misc->makeDbSafe($hash);
        $sql_user_email = $misc->makeDbExtraSafe($email);
        $sql_set_active = $misc->makeDbSafe($active);
        $sql_user_first_name = $misc->makeDbExtraSafe($fist_name);
        $sql_user_last_name = $misc->makeDbExtraSafe($last_name);
        // create the account with the random number as the password
        $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'userdb (
                    userdb_user_name, userdb_user_password, userdb_user_first_name,userdb_user_last_name, userdb_emailaddress, userdb_creation_date,
                    userdb_last_modified, userdb_active, userdb_comments, userdb_is_admin, userdb_can_edit_site_config, userdb_can_edit_member_template,
                    userdb_can_edit_agent_template, userdb_can_edit_listing_template, userdb_can_feature_listings, userdb_can_view_logs, userdb_hit_count,
                    userdb_can_moderate, userdb_can_edit_pages, userdb_can_have_vtours, userdb_is_agent, userdb_limit_listings, userdb_can_edit_expiration,
                    userdb_can_export_listings, userdb_can_edit_all_users, userdb_can_edit_all_listings, userdb_can_edit_property_classes,
                    userdb_can_have_files,userdb_can_have_user_files) 
                VALUES (' . $sql_user_name . ', ' . $sqh_hash . ', ' . $sql_user_first_name . ', ' . $sql_user_last_name . ', ' . $sql_user_email . ', 
                        ' . $ORconn->DBDate(time()) . ',' . $ORconn->DBTimeStamp(time()) . ',' . $sql_set_active . ',\'\',\'no\',\'no\',\'no\',\'no\',\'no\',
                        \'no\',\'no\',0,\'no\',\'no\',\'no\',\'no\',0,\'no\',\'no\',\'no\',\'no\',\'no\',\'no\',\'no\')';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $new_user_id = $ORconn->Insert_ID(); // this is the new User's ID number
        // Update Agent Settings
        if ($type == 'agent') {
            $is_agent = $misc->makeDbSafe('yes');
            if (!$this->config['agent_default_admin']) {
                $agent_default_admin = $misc->makeDbSafe('no');
            } else {
                $agent_default_admin = $misc->makeDbSafe('yes');
            }
            if (!$this->config['agent_default_feature']) {
                $agent_default_feature = $misc->makeDbSafe('no');
            } else {
                $agent_default_feature = $misc->makeDbSafe('yes');
            }
            if (!$this->config['agent_default_moderate']) {
                $agent_default_moderate = $misc->makeDbSafe('no');
            } else {
                $agent_default_moderate = $misc->makeDbSafe('yes');
            }
            if (!$this->config['agent_default_logview']) {
                $agent_default_logview = $misc->makeDbSafe('no');
            } else {
                $agent_default_logview = $misc->makeDbSafe('yes');
            }
            if (!$this->config['agent_default_edit_site_config']) {
                $agent_default_edit_site_config = $misc->makeDbSafe('no');
            } else {
                $agent_default_edit_site_config = $misc->makeDbSafe('yes');
            }
            if (!$this->config['agent_default_edit_member_template']) {
                $agent_default_edit_member_template = $misc->makeDbSafe('no');
            } else {
                $agent_default_edit_member_template = $misc->makeDbSafe('yes');
            }
            if (!$this->config['agent_default_edit_agent_template']) {
                $agent_default_edit_agent_template = $misc->makeDbSafe('no');
            } else {
                $agent_default_edit_agent_template = $misc->makeDbSafe('yes');
            }
            if (!$this->config['agent_default_edit_listing_template']) {
                $agent_default_edit_listing_template = $misc->makeDbSafe('no');
            } else {
                $agent_default_edit_listing_template = $misc->makeDbSafe('yes');
            }
            if (!$this->config['agent_default_canchangeexpirations']) {
                $agent_default_canchangeexpirations = $misc->makeDbSafe('no');
            } else {
                $agent_default_canchangeexpirations = $misc->makeDbSafe('yes');
            }
            if (!$this->config['agent_default_editpages']) {
                $agent_default_editpages = $misc->makeDbSafe('no');
            } else {
                $agent_default_editpages = $misc->makeDbSafe('yes');
            }
            if (!$this->config['agent_default_havevtours']) {
                $agent_default_havevtours = $misc->makeDbSafe('no');
            } else {
                $agent_default_havevtours = $misc->makeDbSafe('yes');
            }
            if (!$this->config['agent_default_havefiles']) {
                $agent_default_havefiles = $misc->makeDbSafe('no');
            } else {
                $agent_default_havefiles = $misc->makeDbSafe('yes');
            }
            if (!$this->config['agent_default_haveuserfiles']) {
                $agent_default_have_user_files = $misc->makeDbSafe('no');
            } else {
                $agent_default_have_user_files = $misc->makeDbSafe('yes');
            }
            if (!$this->config['agent_default_can_export_listings']) {
                $agent_default_can_export_listings = $misc->makeDbSafe('no');
            } else {
                $agent_default_can_export_listings = $misc->makeDbSafe('yes');
            }
            if (!$this->config['agent_default_edit_all_users']) {
                $agent_default_edit_all_users = $misc->makeDbSafe('no');
            } else {
                $agent_default_edit_all_users = $misc->makeDbSafe('yes');
            }
            if (!$this->config['agent_default_edit_all_listings']) {
                $agent_default_edit_all_listings = $misc->makeDbSafe('no');
            } else {
                $agent_default_edit_all_listings = $misc->makeDbSafe('yes');
            }
            if (!$this->config['agent_default_edit_property_classes']) {
                $agent_default_edit_property_classes = $misc->makeDbSafe('no');
            } else {
                $agent_default_edit_property_classes = $misc->makeDbSafe('yes');
            }
            $sql = 'UPDATE ' . $this->config['table_prefix'] . 'userdb 
                    SET userdb_is_agent = ' . $is_agent . ', 
                        userdb_is_admin = ' . $agent_default_admin . ', 
                        userdb_can_feature_listings = ' . $agent_default_feature . ', 
                        userdb_can_moderate = ' . $agent_default_moderate . ', 
                        userdb_can_view_logs =' . $agent_default_logview . ', 
                        userdb_can_edit_site_config = ' . $agent_default_edit_site_config . ', 
                        userdb_can_edit_member_template = ' . $agent_default_edit_member_template . ', 
                        userdb_can_edit_agent_template = ' . $agent_default_edit_agent_template . ', 
                        userdb_can_edit_listing_template = ' . $agent_default_edit_listing_template . ', 
                        userdb_can_edit_pages = ' . $agent_default_editpages . ',
                        userdb_can_have_vtours = ' . $agent_default_havevtours . ',
                        userdb_can_have_files = ' . $agent_default_havefiles . ',
                        userdb_can_have_user_files = ' . $agent_default_have_user_files . ', 
                        userdb_limit_listings = ' . $this->config['agent_default_num_listings'] . ', 
                        userdb_can_edit_expiration = ' . $agent_default_canchangeexpirations . ', 
                        userdb_can_export_listings = ' . $agent_default_can_export_listings . ', 
                        userdb_can_edit_all_users = ' . $agent_default_edit_all_users . ', 
                        userdb_can_edit_all_listings = ' . $agent_default_edit_all_listings . ', 
                        userdb_can_edit_property_classes = ' . $agent_default_edit_property_classes . ' 
                    WHERE userdb_id = ' . $new_user_id;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
        } else {
            $is_agent = $misc->makeDbSafe('no');
            $agent_default_admin = $misc->makeDbSafe('no');
            $sql = 'UPDATE ' . $this->config['table_prefix'] . 'userdb 
                    SET userdb_is_agent = ' . $is_agent . ', 
                        userdb_is_admin = ' . $agent_default_admin . ' 
                    WHERE userdb_id = ' . $new_user_id;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
        }
        return $new_user_id;
    }

    public function userSignup(string $type): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $forms = $this->newForms();

        $login = $this->newLogin();

        $page = $this->newPageUser();
        $display = '';

        //See if we are already logged in
        $login_passed = $login->loginCheck('Member', true);
        if ($login_passed) {
            return $lang['signup_already_logged_in'];
        }
        if ($this->config['allow_' . $type . '_signup']) {
            if (
                isset(
                    $_POST['edit_user_name'],
                    $_POST['edit_user_pass'],
                    $_POST['edit_user_pass2'],
                    $_POST['user_email']
                )
                && is_string($_POST['edit_user_name'])
                && is_string($_POST['edit_user_pass'])
                && is_string($_POST['edit_user_pass2'])
                && is_string($_POST['user_email'])
            ) {
                //Check CRSF token
                if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfToken($_POST['token'])) {
                    return $lang['invalid_csrf_token'];
                }
                if ($_POST['edit_user_pass'] != $_POST['edit_user_pass2']) {
                    $display .= '<p>' . $lang['user_creation_password_identical'] . '</p>';
                    $display .= '<form><input type="button" value="' . $lang['back_button_text'] . '" onclick="history.back()" /></form>';
                } elseif ($_POST['edit_user_pass'] == '') {
                    $display .= '<p>' . $lang['user_creation_password_blank'] . '</p>';
                    $display .= '<form><input type="button" value="' . $lang['back_button_text'] . '" onclick="history.back()" /></form>';
                } elseif ($_POST['edit_user_name'] == '') {
                    $display .= '<p>' . $lang['user_editor_need_username'] . '</p>';
                    $display .= '<form><input type="button" value="' . $lang['back_button_text'] . '" onclick="history.back()" /></form>';
                } elseif ($_POST['user_email'] == '') {
                    $display .= '<p>' . $lang['user_editor_need_email_address'] . '</p>';
                    $display .= '<form><input type="button" value="' . $lang['back_button_text'] . '" onclick="history.back()" /></form>';
                } elseif (empty($_POST['user_first_name']) || !is_string($_POST['user_first_name'])) {
                    $display .= '<p>' . $lang['user_editor_need_first_name'] . '</p>';
                    $display .= '<form><input type="button" value="' . $lang['back_button_text'] . '" onclick="history.back()" /></form>';
                } elseif (empty($_POST['user_last_name']) || !is_string($_POST['user_last_name'])) {
                    $display .= '<p>' . $lang['user_editor_need_last_name'] . '</p>';
                    $display .= '<form><input type="button" value="' . $lang['back_button_text'] . '" onclick="history.back()" /></form>';
                } else {
                    if ($this->config['use_signup_image_verification']) {
                        $captcha = $this->newCaptcha();
                        $correct_code = $captcha->validate();

                        if (!$correct_code) {
                            $display .= '<p>' . $lang['signup_verification_code_not_valid'] . '</p>';
                            $display .= '<form><input type="button" value="' . $lang['back_button_text'] . '" onclick="history.back()" /></form>';
                            return $display;
                        }
                    }
                    $sql_user_name = $misc->makeDbSafe(strip_tags($_POST['edit_user_name']));
                    $sql_user_email = $misc->makeDbSafe(strip_tags($_POST['user_email']));
                    $pass_the_form = 'No';
                    // first, make sure the user name isn't in use
                    $sql = 'SELECT userdb_user_name 
							FROM ' . $this->config['table_prefix'] . 'userdb 
							WHERE userdb_user_name = ' . $sql_user_name;
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $num = $recordSet->RecordCount();
                    // second, make sure the user eamail isn't in use
                    $sql2 = 'SELECT userdb_emailaddress 
							FROM ' . $this->config['table_prefix'] . 'userdb 
							WHERE userdb_emailaddress = ' . $sql_user_email;
                    $recordSet2 = $ORconn->Execute($sql2);
                    if (is_bool($recordSet2)) {
                        $misc->logErrorAndDie($sql2);
                    }
                    $num2 = $recordSet2->RecordCount();
                    //Make sure email address is not banned.
                    $banned_domains = explode("\n", $this->config['banned_domains_signup']);
                    $is_banned_domain = false;
                    foreach ($banned_domains as $bd) {
                        if ($bd !== '') {
                            if (stripos($_POST['user_email'], $bd) !== false) {
                                $is_banned_domain = true;
                            }
                        }
                    }
                    //Get Users IP
                    $ip = $_SERVER['HTTP_X_FORWARD_FOR'] ?? $_SERVER['REMOTE_ADDR'] ?? '';
                    $banned_ips = explode("\n", $this->config['banned_ips_signup']);
                    $is_banned_ip = false;
                    foreach ($banned_ips as $bi) {
                        if ($bi !== '') {
                            if (stripos($ip, $bi) === 0) {
                                $is_banned_ip = true;
                            }
                        }
                    }
                    if ($is_banned_ip) {
                        $display .= $lang['ip_banned_signup_ban'];
                        $display .= '<form><input type="button" value="' . $lang['back_button_text'] . '" onclick="history.back()" /></form>';
                    } elseif ($is_banned_domain) {
                        $display .= $lang['email_domain_signup_ban'];
                        $display .= '<form><input type="button" value="' . $lang['back_button_text'] . '" onclick="history.back()" /></form>';
                    } elseif ($num >= 1) {
                        $display .= $lang['user_creation_username_taken'];
                        $display .= '<form><input type="button" value="' . $lang['back_button_text'] . '" onclick="history.back()" /></form>';
                    } elseif ($num2 >= 1) {
                        $display .= $lang['email_address_already_registered'];
                        $display .= '<form><input type="button" value="' . $lang['back_button_text'] . '" onclick="history.back()" /></form>';
                    } else {
                        // validate the user form
                        $pass_the_form = $forms->validateForm($type . 'formelements');
                        if ($pass_the_form == 'No') {
                            // if we're not going to pass it, tell that they forgot to fill in one of the fields

                            $display .= '<p>' . $lang['required_fields_not_filled'] . '</p>';
                            $display .= '<form><input type="button" value="' . $lang['back_button_text'] . '" onclick="history.back()" /></form>';
                        }
                        if ($pass_the_form != 'Yes') {
                            // if we're not going to pass it, tell that they forgot to fill in one of the fields
                            $display .= '<p>' . $lang['required_fields_not_filled'] . '</p>';
                            $display .= '<form><input type="button" value="' . $lang['back_button_text'] . '" onclick="history.back()" /></form>';
                        }
                    }

                    if ($pass_the_form == 'Yes') {
                        // what the program should do if the form is valid
                        // generate a random number to enter in as the password (initially)
                        // we'll need to know the actual account id to help with retrieving the user
                        // We will be putting in a random number that we know the value of, we can easily
                        // retrieve the account id in a few moment
                        // check to see if moderation is turned on...
                        if ($this->config['moderate_' . $type . 's']) {
                            $set_active = 'no';
                        } else {
                            if ($type == 'agent') {
                                if (!$this->config['agent_default_active']) {
                                    $set_active = 'no';
                                } else {
                                    $set_active = 'yes';
                                }
                            } else {
                                $set_active = 'yes';
                            }
                        }

                        if ($this->config['require_email_verification']) {
                            $set_active = 'no';
                        }

                        #Todo: Call Create User
                        $new_user_id = $this->createBaseUser(
                            $type,
                            $_POST['edit_user_name'],
                            $_POST['user_email'],
                            $_POST['edit_user_pass'],
                            $set_active,
                            $_POST['user_first_name'],
                            $_POST['user_last_name']
                        );
                        // Update Remaining Variables

                        $message = $this->updateUserData($new_user_id);
                        if ($message == 'success') {
                            // $user_name = $_POST['edit_user_name'];
                            $display .= '<p>' . $lang['user_creation_username_success'] . ', ' . $_POST['edit_user_name'] . '</p>';
                            if ($this->config['moderate_' . $type . 's']) {
                                // if moderation is turned on...
                                $display .= '<p>' . $lang['admin_new_user_moderated'] . '</p>';
                            } elseif ($this->config['require_email_verification']) {
                                $display .= '<p>' . $lang['admin_new_user_email_verification'] . '</p>';
                            } else {
                                //log the user in
                                $_POST['user_name'] = $_POST['edit_user_name'];
                                $_POST['user_pass'] = $_POST['edit_user_pass'];
                                $login->setSessionVars($_POST['user_name']);
                                $display .= '<p>' . $lang['you_may_now_view_priv'] . '</p>';
                            }
                            $misc->logAction($lang['log_created_user'] . ' #' . $new_user_id . ' : ' . $_POST['edit_user_name']);
                            //call the new User plugin function

                            $hooks = $this->newHooks();
                            $hooks->load('afterUserSignup', $new_user_id);

                            if ($this->config['email_notification_of_new_users'] && !$this->config['require_email_verification']) {
                                // if the site admin should be notified when a new User is added
                                $remote_ip = $_SERVER['REMOTE_ADDR'] ?? '';
                                $signup_timestamp = date('F j, Y, g:i:s a');
                                $this->sendUserSignupNotification($new_user_id, $type, $remote_ip, $signup_timestamp);
                            }
                            if ($this->config['email_information_to_new_users'] || $this->config['require_email_verification']) {
                                $this->sendUserSignupEmail($new_user_id, $type);
                            }
                        } else {
                            $display .= '<p>' . $lang['alert_site_admin'] . '</p>';
                        }
                    } // end if ($pass_the_form == 'Yes')
                } // end else
            } else {
                if ($type == 'agent') {
                    $page->loadPage($this->config['template_path'] . '/agent_signup.html');
                } else {
                    $page->loadPage($this->config['template_path'] . '/member_signup.html');
                }

                $custom_fields = '';
                if ($type == 'agent') {
                    $sql = 'SELECT ' . $type . 'formelements_field_type, ' . $type . 'formelements_field_name, ' . $type . 'formelements_field_caption, 
									' . $type . 'formelements_default_text, ' . $type . 'formelements_field_elements, ' . $type . 'formelements_required, 
									' . $type . 'formelements_tool_tip
							FROM ' . $this->config['table_prefix'] . $type . 'formelements 
							WHERE agentformelements_display_priv <= 2
							ORDER BY ' . $type . 'formelements_rank, ' . $type . 'formelements_field_caption';
                } else {
                    $sql = 'SELECT ' . $type . 'formelements_field_type, ' . $type . 'formelements_field_name, ' . $type . 'formelements_field_caption, 
									' . $type . 'formelements_default_text, ' . $type . 'formelements_field_elements, ' . $type . 'formelements_required, 
									' . $type . 'formelements_tool_tip
							FROM ' . $this->config['table_prefix'] . $type . 'formelements
							ORDER BY ' . $type . 'formelements_rank, ' . $type . 'formelements_field_caption';
                }
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                while (!$recordSet->EOF) {
                    $field_type = (string)$recordSet->fields($type . 'formelements_field_type');
                    $field_name = (string)$recordSet->fields($type . 'formelements_field_name');
                    $field_caption = (string)$recordSet->fields($type . 'formelements_field_caption');
                    $default_text = (string)$recordSet->fields($type . 'formelements_default_text');
                    $field_elements = (string)$recordSet->fields($type . 'formelements_field_elements');
                    $required = (string)$recordSet->fields($type . 'formelements_required');
                    $tool_tip = (string)$recordSet->fields($type . 'formelements_tool_tip');

                    $custom_fields .= $forms->renderFormElement($field_type, $field_name, '', $field_caption, $default_text, $required, $field_elements, '', $tool_tip);

                    $recordSet->MoveNext();
                } // end while

                $page->replaceTag('custom_fields', $custom_fields);
                if ($this->config['use_signup_image_verification']) {
                    $captcha = $this->newCaptcha();
                    $page->replaceTag('captcha_display', $captcha->show());
                } else {
                    $page->replaceTag('captcha_display', '');
                }

                $display .= $page->returnPage();
            }
        } else {
            // if users can't sign up...
            $display .= '<h3>' . $lang['no_user_signup'] . '</h3>';
        }
        return $display;
    }

    public function ajaxMemberCreation(string $email, string $fname, string $lname, bool $login = true): string
    {
        $_POST['edit_user_pass'] = $this->generatePassword();
        $_POST['edit_user_pass2'] = $_POST['edit_user_pass'];
        $_POST['user_email'] = trim($email);
        $_POST['edit_user_name'] = trim($email);
        $_POST['user_first_name'] = trim($fname);
        $_POST['user_last_name'] = trim($lname);
        $user_signup_status = $this->memberCreation($login);
        if (is_array($user_signup_status)) {
            //return array('user_id'=>$new_user_id,'active'=>$set_active);
            return json_encode(['error' => false, 'active' => $user_signup_status['active'], 'fname' => $_POST['user_first_name'], 'lname' => $_POST['user_last_name'], 'user_id' => $user_signup_status['user_id'], 'email' => $_POST['user_email']]) ?: '';
        } else {
            return json_encode(['error' => true, 'error_msg' => $user_signup_status]) ?: '';
        }
    }

    /**
     * @return (int|string)[]|string
     *
     * @psalm-return array{user_id: int, active: 'email'|'mod'|'yes'}|string
     */
    public function memberCreation(bool $do_login = true): array|string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $type = 'member';
        if ($_POST['edit_user_pass'] != $_POST['edit_user_pass2']) {
            return $lang['user_creation_password_identical'];
        } elseif ($_POST['edit_user_pass'] == '') {
            return $lang['user_creation_password_blank'];
        } elseif (!is_string($_POST['edit_user_name']) || empty($_POST['edit_user_name'])) {
            return $lang['user_editor_need_username'];
        } elseif ($_POST['user_email'] == '') {
            return $lang['user_editor_need_email_address'];
        } elseif ($_POST['user_first_name'] == '') {
            return $lang['user_editor_need_first_name'];
        } elseif ($_POST['user_last_name'] == '') {
            return $lang['user_editor_need_last_name'];
        } else {
            if (
                isset(
                    $_POST['edit_user_pass'],
                    $_POST['user_email'],
                    $_POST['user_first_name'],
                    $_POST['user_last_name'],
                )
                && is_string($_POST['edit_user_pass'])
                && is_string($_POST['user_email'])
                && is_string($_POST['user_first_name'])
                && is_string($_POST['user_last_name'])
            ) {
                $sql_user_name = $misc->makeDbSafe(strip_tags($_POST['edit_user_name']));
                $sql_user_email = $misc->makeDbSafe(strip_tags($_POST['user_email']));
                $sql_user_first_name = $misc->makeDbSafe(strip_tags($_POST['user_first_name']));
                $sql_user_last_name = $misc->makeDbSafe(strip_tags($_POST['user_last_name']));
                // first, make sure the user name isn't in use
                $sql = 'SELECT userdb_user_name from ' . $this->config['table_prefix'] . 'userdb WHERE userdb_user_name = ' . $sql_user_name;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $num = $recordSet->RecordCount();
                // second, make sure the user eamail isn't in use
                $sql2 = 'SELECT userdb_emailaddress from ' . $this->config['table_prefix'] . 'userdb WHERE userdb_emailaddress = ' . $sql_user_email;
                $recordSet2 = $ORconn->Execute($sql2);
                if (is_bool($recordSet2)) {
                    $misc->logErrorAndDie($sql2);
                }
                $num2 = $recordSet2->RecordCount();
                //Make sure email address is not banned.
                $banned_domains = explode("\n", $this->config['banned_domains_signup']);
                $is_banned_domain = false;
                foreach ($banned_domains as $bd) {
                    if ($bd != '') {
                        if (stripos($_POST['user_email'], $bd) !== false) {
                            $is_banned_domain = true;
                        }
                    }
                }
                //Get Users IP
                if (in_array('HTTP_X_FORWARD_FOR', $_SERVER)) {
                    $ip = $_SERVER['HTTP_X_FORWARD_FOR'];
                } else {
                    $ip = $_SERVER['REMOTE_ADDR'] ?? '';
                }
                $banned_ips = explode("\n", $this->config['banned_ips_signup']);
                $is_banned_ip = false;
                foreach ($banned_ips as $bi) {
                    if ($bi != '') {
                        if (stripos($ip, $bi) === 0) {
                            $is_banned_ip = true;
                        }
                    }
                }
                if ($is_banned_ip) {
                    return $lang['ip_banned_signup_ban'];
                } elseif ($is_banned_domain) {
                    return $lang['email_domain_signup_ban'];
                } elseif ($num >= 1) {
                    return $lang['user_creation_username_taken'];
                } elseif ($num2 >= 1) {
                    return $lang['email_address_already_registered'];
                } // end if

                // what the program should do if the form is valid
                // generate a random number to enter in as the password (initially)
                // we'll need to know the actual account id to help with retrieving the user
                // We will be putting in a random number that we know the value of, we can easily
                // retrieve the account id in a few moment
                // check to see if moderation is turned on...
                $set_active = 'yes';

                if ($this->config['moderate_' . $type . 's']) {
                    $set_active = 'mod';
                }

                if ($this->config['require_email_verification']) {
                    $set_active = 'email';
                }

                $hash = password_hash($_POST['edit_user_pass'], PASSWORD_DEFAULT);
                $sql_hash = $misc->makeDbSafe($hash);
                if ($set_active == 'yes') {
                    $sql_set_active = $misc->makeDbSafe($set_active);
                } else {
                    $sql_set_active = $misc->makeDbSafe('no');
                }

                // create the account with the random number as the password
                $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'userdb (
					userdb_user_name, userdb_user_password, userdb_user_first_name,userdb_user_last_name, userdb_emailaddress, userdb_creation_date,userdb_last_modified, 
					userdb_active, userdb_comments, userdb_is_admin, userdb_can_edit_site_config, userdb_can_edit_member_template, userdb_can_edit_agent_template, 
					userdb_can_edit_listing_template, userdb_can_feature_listings,userdb_can_view_logs, userdb_hit_count, userdb_can_moderate, userdb_can_edit_pages,
					userdb_can_have_vtours, userdb_is_agent, userdb_limit_listings, userdb_can_edit_expiration, userdb_can_export_listings,userdb_can_edit_all_users,
					userdb_can_edit_all_listings, userdb_can_edit_property_classes, userdb_can_have_files, userdb_can_have_user_files ) 
					VALUES (' . $sql_user_name . ', ' . $sql_hash . ', ' . $sql_user_first_name . ', ' . $sql_user_last_name . ', ' . $sql_user_email . ', 
					' . $ORconn->DBDate(time()) . ',' . $ORconn->DBTimeStamp(time()) . ',' . $sql_set_active . ',\'\',\'no\',\'no\',\'no\',\'no\',\'no\',\'no\',
					\'no\',0,\'no\',\'no\',\'no\',\'no\',0,\'no\',\'no\',\'no\',\'no\',\'no\',\'no\',\'no\')';
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $new_user_id = $ORconn->Insert_ID(); // this is the new User's ID number
                if ($set_active == 'yes' && $do_login) {
                    //User should be logged in.
                    $_POST['user_name'] = $_POST['edit_user_name'];
                    $_POST['user_pass'] = $_POST['edit_user_pass'];
                    $login->setSessionVars($_POST['user_name']);
                }
                //call the new User plugin function

                $hooks = $this->newHooks();
                $hooks->load('afterUserSignup', $new_user_id);

                if ($this->config['email_notification_of_new_users'] && !$this->config['require_email_verification']) {
                    $remote_ip = $_SERVER['REMOTE_ADDR'] ?? '';
                    $signup_timestamp = date('F j, Y, g:i:s a');

                    $this->sendUserSignupNotification($new_user_id, 'member', $remote_ip, $signup_timestamp);
                    //refactored/deprecated for v3.2.11
                    //$this->send_member_signup_notification($new_user_id,$remote_ip,$signup_timestamp);
                } // end if
                if ($this->config['email_information_to_new_users'] || $this->config['require_email_verification']) {
                    $this->sendUserSignupEmail($new_user_id, 'member', $_POST['edit_user_pass']);
                } //end if
                return ['user_id' => $new_user_id, 'active' => $set_active];
            }
            return 'Missing Paramaters';
        }
    }

    public function editMemberProfile(int $user_id): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $page = $this->newPageUser();

        $forms = $this->newForms();
        $display = '';
        // Set Variable to hold errors
        if ($_SESSION['userID'] == $user_id && $_SESSION['is_member'] == 'yes') {
            $sql_edit = intval($_SESSION['userID']);
            $raw_id = intval($_SESSION['userID']);
        } else {
            return $lang['user_manager_permission_denied'];
        }
        // $raw_id = $sql_edit;
        // Save any Changes that were posted
        if (isset($_POST['edit'])) {
            $this->updateMemberProfile($raw_id);
        }

        // Show Account Edit Form
        //Load Template
        $page->loadPage($this->config['template_path'] . '/edit_profile.html');
        // first, grab the user's main info
        $sql = 'SELECT * 
				FROM ' . $this->config['table_prefix'] . 'userdb 
				WHERE userdb_id = ' . $sql_edit;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }

        // collect up the main DB's various fields
        $edit_user_name = $recordSet->fields('userdb_user_name');
        $edit_emailAddress = $recordSet->fields('userdb_emailaddress');
        $edit_isAgent = $recordSet->fields('userdb_is_agent');
        $edit_firstname = $recordSet->fields('userdb_user_first_name');
        $edit_lastname = $recordSet->fields('userdb_user_last_name');
        $last_modified = $recordSet->UserTimeStamp($recordSet->fields('userdb_last_modified'), $this->config['date_format_timestamp']);
        $page->replaceTag('user_id', (string)$raw_id);
        $page->replaceTag('user_name', htmlentities($edit_user_name, ENT_COMPAT, $this->config['charset']));
        $page->replaceTag('user_firstname', htmlentities($edit_firstname, ENT_COMPAT, $this->config['charset']));
        $page->replaceTag('user_lastname', htmlentities($edit_lastname, ENT_COMPAT, $this->config['charset']));
        $page->replaceTag('user_last_modified', htmlentities($last_modified, ENT_COMPAT, $this->config['charset']));
        $page->replaceTag('user_emailAddress', htmlentities($edit_emailAddress, ENT_COMPAT, $this->config['charset']));

        if (!$this->config['demo_mode'] || $_SESSION['admin_privs']) {
            $page->page = $page->cleanupTemplateBlock('password', $page->page);
        } else {
            $page->page = $page->removeTemplateBlock('password', $page->page);
        }

        if ($edit_isAgent == 'yes') {
            $db_to_use = 'agentformelements';
        } else {
            $db_to_use = 'memberformelements';
        }

        $sql = 'SELECT ' . $db_to_use . '_field_name, userdbelements_field_value, ' . $db_to_use . '_field_type, ' . $db_to_use . '_rank, ' . $db_to_use . '_field_caption, ' . $db_to_use . '_default_text, ' . $db_to_use . '_required, ' . $db_to_use . '_field_elements, ' . $db_to_use . '_tool_tip 
				FROM ' . $this->config['table_prefix'] . $db_to_use . ' 
				LEFT JOIN ' . $this->config['table_prefix'] . 'userdbelements on userdbelements_field_name = ' . $db_to_use . '_field_name and userdb_id = ' . $sql_edit . ' 
				ORDER BY ' . $db_to_use . '_rank';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $detail_fields = '';
        while (!$recordSet->EOF) {
            $field_name = (string)$recordSet->fields($db_to_use . '_field_name');
            $field_value = (string)$recordSet->fields('userdbelements_field_value');
            $field_type = (string)$recordSet->fields($db_to_use . '_field_type');
            $field_caption = (string)$recordSet->fields($db_to_use . '_field_caption');
            $default_text = (string)$recordSet->fields($db_to_use . '_default_text');
            $field_elements = (string)$recordSet->fields($db_to_use . '_field_elements');
            $required = (string)$recordSet->fields($db_to_use . '_required');
            $tool_tip = (string)$recordSet->fields($db_to_use . '_tool_tip');
            // pass the data to the function
            $detail_fields .= $forms->renderFormElement($field_type, $field_name, $field_value, $field_caption, $default_text, $required, $field_elements, '', $tool_tip);
            $recordSet->MoveNext();
        } // end while

        $page->replaceTag('user_detail_fields', $detail_fields);
        $page->replacePermissionTags();
        $page->autoReplaceTags('', true);
        $display .= $page->returnPage();
        //print_r($_SESSION);
        return $display;
    }

    /**
     * @return null|string
     */
    public function updateMemberProfile(int $user_id)
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $forms = $this->newForms();
        $display = '';
        if ($_SESSION['edit_all_users'] == 'yes' || $_SESSION['admin_privs'] == 'yes' || $user_id = $_SESSION['userID']) {
            if ($_POST['edit_user_pass'] != $_POST['edit_user_pass2']) {
                return '<p>' . $lang['user_manager_password_identical'] . '</p>';
            }
            if (
                empty($_POST['user_email'])
                || empty($_POST['user_first_name'])
                || empty($_POST['user_last_name'])
                || !is_string($_POST['user_email'])
                || !is_string($_POST['user_first_name'])
                || !is_string($_POST['user_last_name'])
            ) {
                return '<p class="redtext">' . $lang['required_fields_not_filled'] . '</p>';
            }
            // Get Current User type
            $sql = 'SELECT userdb_is_agent, userdb_is_admin, userdb_active 
					FROM ' . $this->config['table_prefix'] . 'userdb 
					WHERE userdb_id = ' . $user_id;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }

            $is_agent = $recordSet->fields('userdb_is_agent');
            $is_admin = $recordSet->fields('userdb_is_admin');

            if ($is_agent == 'yes' || $is_admin == 'yes') {
                $db_to_validate = 'agentformelements';
            } else {
                $db_to_validate = 'memberformelements';
            }
            $pass_the_form = $forms->validateForm($db_to_validate);
            $sql_user_email = $misc->makeDbSafe(strip_tags($_POST['user_email']));
            $sql_user_first_name = $misc->makeDbSafe(strip_tags($_POST['user_first_name']));
            $sql_user_last_name = $misc->makeDbSafe(strip_tags($_POST['user_last_name']));
            //Make sure no other user has this email address.
            $sql = 'SELECT userdb_id 
						FROM ' . $this->config['table_prefix'] . 'userdb 
						WHERE  userdb_emailaddress = ' . $sql_user_email;

            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            while (!$recordSet->EOF) {
                if ($recordSet->fields('userdb_id') != $user_id) {
                    $display .= "<p class=\"redtext\">$lang[email_address_already_used]</p>";
                    return $display;
                }
                $recordSet->MoveNext();
            }

            if (is_array($pass_the_form)) {
                // if we're not going to pass it, tell that they forgot to fill in one of the fields
                foreach ($pass_the_form as $k => $v) {
                    if ($v == 'REQUIRED') {
                        $display .= "<p class=\"redtext\">$k: $lang[required_fields_not_filled]</p>";
                    }
                    if ($v == 'TYPE') {
                        $display .= "<p class=\"redtext\">$k: $lang[field_type_does_not_match]</p>";
                    }
                }
            } else {
                $hooks = $this->newHooks();
                $hooks->load('before_user_change', $user_id);
                if (empty($_POST['edit_user_pass']) || !is_string($_POST['edit_user_pass'])) {
                    $sql = 'UPDATE ' . $this->config['table_prefix'] . 'userdb 
								SET userdb_emailaddress = ' . $sql_user_email . ', 
									userdb_user_first_name = ' . $sql_user_first_name . ', 
									userdb_user_last_name = ' . $sql_user_last_name . ', 
									userdb_last_modified = ' . $ORconn->DBTimeStamp(time()) . ' 
								WHERE userdb_id = ' . $user_id;
                } else {
                    $hash = password_hash($_POST['edit_user_pass'], PASSWORD_DEFAULT);
                    $sql_hash = $misc->makeDbSafe($hash);
                    $sql = 'UPDATE ' . $this->config['table_prefix'] . 'userdb 
								SET userdb_emailaddress = ' . $sql_user_email . ', 
									userdb_user_first_name = ' . $sql_user_first_name . ', 
									userdb_user_last_name = ' . $sql_user_last_name . ', 
									userdb_user_password = ' . $sql_hash . ', 
									userdb_last_modified = ' . $ORconn->DBTimeStamp(time()) . ' 
								WHERE userdb_id = ' . $user_id;
                }
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }

                $message = $this->updateUserData($user_id);
                if ($message == 'success') {
                    //call the user change plugin function

                    $hooks = $this->newHooks();
                    $hooks->load('afterUserUpdate', $user_id);

                    $display .= '<p>' . $lang['user_editor_account_updated'] . ', ' . htmlentities($_SESSION['username']) . '</p>';
                } else {
                    $display .= '<p>' . $lang['alert_site_admin'] . '</p>';
                }
            }
        }
        return $display;
    }

    /**
     * @return string
     */
    public function ajaxDisplayAddUser(): string
    {
        global $lang, $jscript;
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_all_users');
        $status_text = '';
        if ($security) {
            //Load the Core Template

            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/add_user.html');
            $yes_no = [];
            $yes_no['no'] = $lang['no'];
            $yes_no['yes'] = $lang['yes'];

            //Load CSS File
            $jscript .= '{load_css_user_manager}';

            $html = $page->getTemplateSection('user_status_option_block');
            $html = $page->formOptions($yes_no, '', $html);
            $page->replaceTemplateSection('user_status_option_block', $html);

            $html = $page->getTemplateSection('user_isAdmin_option_block');
            $html = $page->formOptions($yes_no, '', $html);
            $page->replaceTemplateSection('user_isAdmin_option_block', $html);

            $html = $page->getTemplateSection('user_isAgent_option_block');
            $html = $page->formOptions($yes_no, '', $html);
            $page->replaceTemplateSection('user_isAgent_option_block', $html);

            $page->replaceTags(['curley_open', 'curley_close', 'baseurl']);
            $page->replaceTag('application_status_text', $status_text);
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        }
        return '';
    }

    /**
     * @return string
     */
    public function ajaxAddUser(): string
    {
        global $lang;
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_all_users');
        if ($security) {
            $result = $this->createUser();
            if (is_numeric($result)) {
                header('Content-type: application/json');
                return json_encode(['error' => 0, 'user_id' => $result]) ?: '';
            } else {
                header('Content-type: application/json');
                return json_encode(['error' => 1, 'error_msg' => $result]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => 1, 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
    }

    public function ajaxDeleteUser(int $user_id): string
    {
        global $lang;

        $misc = $this->newMisc();
        $is_admin = $misc->getAdminStatus($user_id);
        $has_permission = false;
        if ((($this->config['demo_mode'] != 1) && ($_SESSION['edit_all_users'] == 'yes')) || ($_SESSION['admin_privs'] == 'yes')) {
            if (!$is_admin || $_SESSION['admin_privs'] == 'yes') {
                $has_permission = true;
            }
        }
        if ($has_permission) {
            $result = $this->deleteUser($user_id);
            //will need to change this when switching to the user API it returns false
            if (is_numeric($result)) {
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'user_id' => $user_id]) ?: '';
            } else {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => $result]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
    }

    public function ajaxUpdateUserData(int $user_id): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        //$user_id is current logged-in user
        $display = '';

        $forms = $this->newForms();

        $login = $this->newLogin();

        //Make sure we can edit this listing.
        //if you're not the current user you better have edit_all permissions
        $has_permission = true;

        if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
        }

        if ($_SESSION['userID'] != $user_id) {
            $security = $login->verifyPriv('edit_all_users');
            if (!$security) {
                $has_permission = false;
            }
        }
        if ($has_permission) {
            if ($_POST['edit_user_pass'] != $_POST['edit_user_pass2']) {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => $lang['user_manager_password_identical']]) ?: '';
            } elseif (
                isset(
                    $_POST['edit_user_pass'],
                    $_POST['edit_user_pass2']
                )
                && is_string($_POST['edit_user_pass'])
                && is_string($_POST['edit_user_pass2'])
                && is_string($_POST['user_email'])
                && !empty($_POST['user_email'])
                && is_string($_POST['user_first_name'])
                && !empty($_POST['user_first_name'])
                && is_string($_POST['user_last_name'])
                && !empty($_POST['user_last_name'])
            ) {
                $is_admin = $misc->getAdminStatus($user_id);
                $is_agent = $misc->getAgentStatus($user_id);

                $sql_user_email = $misc->makeDbSafe(strip_tags($_POST['user_email']));

                $sql = 'SELECT userdb_id 
					FROM ' . $this->config['table_prefix'] . 'userdb 
					WHERE  userdb_emailaddress = ' . $sql_user_email;

                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }

                if ($recordSet->RecordCount() > 0 && $recordSet->fields('userdb_id') != $user_id) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['email_address_already_used']]) ?: '';
                } else {
                    if ($is_agent === true || $is_admin === true) {
                        $db_to_validate = 'agentformelements';
                    } else {
                        $db_to_validate = 'memberformelements';
                    }
                    $pass_the_form = $forms->validateForm($db_to_validate);

                    if (is_array($pass_the_form)) {
                        // if we're not going to pass it, tell that they forgot to fill in one of the fields
                        foreach ($pass_the_form as $k => $v) {
                            if ($v == 'REQUIRED') {
                                $display .= "$k: $lang[required_fields_not_filled]";
                            }
                            if ($v == 'TYPE') {
                                $display .= "$k: $lang[field_type_does_not_match]";
                            }
                        }
                        header('Content-type: application/json');
                        return json_encode(['error' => '1', 'error_msg' => $display]) ?: '';
                    } else {
                        $hooks = $this->newHooks();
                        $hooks->load('before_user_change', $user_id);
                        if (empty($_POST['edit_user_pass'])) {
                            $sql = 'UPDATE ' . $this->config['table_prefix'] . 'userdb 
								SET userdb_emailaddress = ' . $sql_user_email . ', userdb_last_modified = ' . $ORconn->DBTimeStamp(time()) . ' 
								WHERE userdb_id = ' . $user_id;
                        } else {
                            $hash = password_hash($_POST['edit_user_pass'], PASSWORD_DEFAULT);
                            $sql_hash = $misc->makeDbSafe($hash);
                            $sql = 'UPDATE ' . $this->config['table_prefix'] . 'userdb 
								SET userdb_emailaddress = ' . $sql_user_email . ', 
									userdb_user_password = ' . $sql_hash . ', 
									userdb_last_modified = ' . $ORconn->DBTimeStamp(time()) . ' 
								WHERE userdb_id = ' . $user_id;
                        }
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            $misc->logErrorAndDie($sql);
                        }
                        if ($_SESSION['admin_privs'] == 'yes' && $is_admin === true) {
                            if (
                                isset(
                                    $_POST['edit_limitListings'],
                                    $_POST['edit_limitFeaturedListings'],
                                    $_POST['edit_userFloorNotify'],
                                    $_POST['edit_userRank']
                                )
                                && is_numeric($_POST['edit_limitListings'])
                                && is_numeric($_POST['edit_limitFeaturedListings'])
                                && is_numeric($_POST['edit_userFloorNotify'])
                                && is_numeric($_POST['edit_userRank'])
                            ) {
                                $sql_edit_limitListings = (int)$_POST['edit_limitListings'];
                                $sql_edit_limitFeaturedListings = (int)$_POST['edit_limitFeaturedListings'];
                                $edit_userFloorNotify = (int)$_POST['edit_userFloorNotify'];
                                $sql_edit_userRank = (int)$_POST['edit_userRank'];
                                $sql = 'UPDATE ' . $this->config['table_prefix'] . 'userdb 
								SET userdb_send_notifications_to_floor = ' . $edit_userFloorNotify . ', 
									userdb_rank = ' . $sql_edit_userRank . ', 
									userdb_featuredlistinglimit = ' . $sql_edit_limitFeaturedListings . ', 
									userdb_limit_listings = ' . $sql_edit_limitListings . ' 
								WHERE userdb_id = ' . $user_id;

                                $recordSet = $ORconn->Execute($sql);
                                if (is_bool($recordSet)) {
                                    $misc->logErrorAndDie($sql);
                                }
                            }
                        }
                        // If Admin is upadting and agent set other fields
                        if ($_SESSION['admin_privs'] == 'yes' && $is_agent) {
                            if (
                                isset(
                                    $_POST['edit_userFloorNotify'],
                                    $_POST['edit_canEditSiteConfig'],
                                    $_POST['edit_canEditMemberTemplate'],
                                    $_POST['edit_canEditAgentTemplate'],
                                    $_POST['edit_canEditListingTemplate'],
                                    $_POST['edit_canEditAllListings'],
                                    $_POST['edit_canEditAllUsers'],
                                    $_POST['edit_canViewLogs'],
                                    $_POST['edit_canModerate'],
                                    $_POST['edit_canPages'],
                                    $_POST['edit_canVtour'],
                                    $_POST['edit_canFiles'],
                                    $_POST['edit_canUserFiles'],
                                    $_POST['edit_limitListings'],
                                    $_POST['edit_canExportListings'],
                                    $_POST['edit_canEditListingExpiration'],
                                    $_POST['edit_canEditPropertyClasses'],
                                    $_POST['edit_BlogPrivileges'],
                                    $_POST['edit_limitFeaturedListings'],
                                    $_POST['edit_userRank'],
                                    $_POST['edit_canManageAddons'],
                                    $_POST['edit_can_edit_all_leads'],
                                    $_POST['edit_can_edit_lead_template'],
                                    $_POST['edit_canFeatureListings'],
                                )
                                && is_numeric($_POST['edit_userFloorNotify'])
                                && is_string($_POST['edit_canEditSiteConfig'])
                                && is_string($_POST['edit_canEditMemberTemplate'])
                                && is_string($_POST['edit_canEditAgentTemplate'])
                                && is_string($_POST['edit_canEditListingTemplate'])
                                && is_string($_POST['edit_canEditAllListings'])
                                && is_string($_POST['edit_canEditAllUsers'])
                                && is_string($_POST['edit_canViewLogs'])
                                && is_string($_POST['edit_canModerate'])
                                && is_string($_POST['edit_canPages'])
                                && is_string($_POST['edit_canVtour'])
                                && is_string($_POST['edit_canFiles'])
                                && is_string($_POST['edit_canUserFiles'])
                                && is_numeric($_POST['edit_limitListings'])
                                && is_string($_POST['edit_canExportListings'])
                                && is_string($_POST['edit_canEditListingExpiration'])
                                && is_string($_POST['edit_canEditPropertyClasses'])
                                && is_string($_POST['edit_BlogPrivileges'])
                                && is_numeric($_POST['edit_limitFeaturedListings'])
                                && is_numeric($_POST['edit_userRank'])
                                && is_string($_POST['edit_canManageAddons'])
                                && is_string($_POST['edit_can_edit_all_leads'])
                                && is_string($_POST['edit_canFeatureListings'])
                                && is_string($_POST['edit_can_edit_lead_template'])
                            ) {
                                $edit_userFloorNotify = intval($_POST['edit_userFloorNotify']);
                                $edit_first_name = $misc->makeDbSafe(strip_tags($_POST['user_first_name']));
                                $edit_last_name = $misc->makeDbSafe(strip_tags($_POST['user_last_name']));
                                $edit_canEditSiteConfig = $misc->makeDbSafe($_POST['edit_canEditSiteConfig']);
                                $edit_canEditMemberTemplate = $misc->makeDbSafe($_POST['edit_canEditMemberTemplate']);
                                $edit_canEditAgentTemplate = $misc->makeDbSafe($_POST['edit_canEditAgentTemplate']);
                                $edit_canEditListingTemplate = $misc->makeDbSafe($_POST['edit_canEditListingTemplate']);
                                $edit_canEditAllListings = $misc->makeDbSafe($_POST['edit_canEditAllListings']);
                                $edit_canEditAllUsers = $misc->makeDbSafe($_POST['edit_canEditAllUsers']);
                                $edit_can_view_logs = $misc->makeDbSafe($_POST['edit_canViewLogs']);
                                $edit_can_moderate = $misc->makeDbSafe($_POST['edit_canModerate']);
                                $edit_can_feature_listings = $misc->makeDbSafe($_POST['edit_canFeatureListings']);
                                $edit_can_edit_pages = $misc->makeDbSafe($_POST['edit_canPages']);
                                $edit_can_have_vtours = $misc->makeDbSafe($_POST['edit_canVtour']);
                                $edit_can_have_files = $misc->makeDbSafe($_POST['edit_canFiles']);
                                $edit_can_have_user_files = $misc->makeDbSafe($_POST['edit_canUserFiles']);
                                $edit_limitListings = $misc->makeDbSafe($_POST['edit_limitListings']);
                                $sql_edit_canExportListings = $misc->makeDbSafe($_POST['edit_canExportListings']);
                                $sql_edit_canEditListingExpiration = $misc->makeDbSafe($_POST['edit_canEditListingExpiration']);
                                $sql_edit_canEditPropertyClasses = $misc->makeDbSafe($_POST['edit_canEditPropertyClasses']);
                                $sql_userdb_blog_user_type = $misc->makeDbSafe($_POST['edit_BlogPrivileges']);
                                $sql_edit_limitFeaturedListings = $misc->makeDbSafe($_POST['edit_limitFeaturedListings']);
                                $sql_edit_userRank = $misc->makeDbSafe($_POST['edit_userRank']);
                                $sql_edit_canManageAddons = $misc->makeDbSafe($_POST['edit_canManageAddons']);
                                $sql_edit_can_edit_all_leads = $misc->makeDbSafe($_POST['edit_can_edit_all_leads']);
                                $sql_edit_can_edit_lead_template = $misc->makeDbSafe($_POST['edit_can_edit_lead_template']);

                                $sql = 'UPDATE ' . $this->config['table_prefix'] . 'userdb SET
                                        userdb_user_first_name = ' . $edit_first_name . ',
                                        userdb_user_last_name = ' . $edit_last_name . ',
                                        userdb_can_edit_site_config = ' . $edit_canEditSiteConfig . ',
                                        userdb_can_edit_member_template = ' . $edit_canEditMemberTemplate . ',
                                        userdb_can_edit_agent_template = ' . $edit_canEditAgentTemplate . ',
                                        userdb_can_edit_listing_template = ' . $edit_canEditListingTemplate . ',
                                        userdb_can_view_logs = ' . $edit_can_view_logs . ',
                                        userdb_can_moderate = ' . $edit_can_moderate . ',
                                        userdb_can_feature_listings = ' . $edit_can_feature_listings . ',
                                        userdb_can_edit_pages = ' . $edit_can_edit_pages . ',
                                        userdb_can_have_vtours = ' . $edit_can_have_vtours . ',
                                        userdb_can_have_files = ' . $edit_can_have_files . ',
                                        userdb_can_have_user_files = ' . $edit_can_have_user_files . ',
                                        userdb_limit_listings = ' . $edit_limitListings . ',
                                        userdb_can_edit_expiration = ' . $sql_edit_canEditListingExpiration . ',
                                        userdb_can_export_listings = ' . $sql_edit_canExportListings . ',
                                        userdb_can_edit_all_users = ' . $edit_canEditAllUsers . ',
                                        userdb_can_edit_all_listings = ' . $edit_canEditAllListings . ',
                                        userdb_can_edit_property_classes = ' . $sql_edit_canEditPropertyClasses . ',
                                        userdb_can_manage_addons = ' . $sql_edit_canManageAddons . ',
                                        userdb_rank = ' . $sql_edit_userRank . ',
                                        userdb_featuredlistinglimit = ' . $sql_edit_limitFeaturedListings . ',
                                        userdb_blog_user_type = ' . $sql_userdb_blog_user_type . ',
                                        userdb_can_edit_all_leads = ' . $sql_edit_can_edit_all_leads . ',
                                        userdb_can_edit_lead_template = ' . $sql_edit_can_edit_lead_template . ',
                                        userdb_send_notifications_to_floor = ' . $edit_userFloorNotify . '
                                        WHERE userdb_id = ' . $user_id;
                                $recordSet = $ORconn->Execute($sql);
                                if (is_bool($recordSet)) {
                                    $misc->logErrorAndDie($sql);
                                }
                            }
                        } else {
                            $edit_first_name = $ORconn->qstr(strip_tags($_POST['user_first_name']));
                            $edit_last_name = $ORconn->qstr(strip_tags($_POST['user_last_name']));

                            //  $edit_first_name = mysql_real_escape_string(strip_tags($_POST['user_first_name']));
                            //  $edit_last_name = mysql_real_escape_string(strip_tags($_POST['user_last_name']));
                            $sql = 'UPDATE ' . $this->config['table_prefix'] . 'userdb 
								SET	userdb_user_first_name = ' . $edit_first_name . ', 
									userdb_user_last_name = ' . $edit_last_name . ' 
									WHERE userdb_id = ' . $user_id;
                            $recordSet = $ORconn->Execute($sql);
                            if (is_bool($recordSet)) {
                                $misc->logErrorAndDie($sql);
                            }
                        }

                        $message = $this->updateUserData($user_id);
                        if ($message == 'success') {
                            //call the user change plugin function

                            $hooks = $this->newHooks();
                            $hooks->load('afterUserUpdate', $user_id);
                            $display .= $lang['user_editor_account_updated'] . ', ' . $_SESSION['username'];
                        } else {
                            $display .= $lang['alert_site_admin'];
                        } // end else

                        $misc->logAction($lang['log_updated_user'] . ': ' . $user_id);
                        header('Content-type: application/json');
                        return json_encode(['error' => '0', 'status_msg' => $display, 'user_id' => $user_id]) ?: '';
                    } // end if $pass_the_form == "Yes"
                } // end else
            } else {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => $lang['required_fields_not_filled']]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
    }

    public function createUser(): string|int
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $security = false;
        if ((($this->config['demo_mode'] != 1) && ($_SESSION['edit_all_users'] == 'yes')) || ($_SESSION['admin_privs'] == 'yes')) {
            $security = true;
        }
        $display = '';
        if ($security) {
            // create the user
            if ($_POST['edit_user_pass'] != $_POST['edit_user_pass2']) {
                $display .= '<p>' . $lang['user_creation_password_identical'] . '</p>';
            } elseif ($_POST['edit_user_pass'] == '') {
                $display .= '<p>' . $lang['user_creation_password_blank'] . '</p>';
            } elseif (empty($_POST['edit_user_name']) || !is_string($_POST['edit_user_name'])) {
                $display .= '<p>' . $lang['user_editor_need_username'] . '</p>';
            } elseif (empty($_POST['user_email']) || !is_string($_POST['user_email'])) {
                $display .= '<p>' . $lang['user_editor_need_email_address'] . '</p>';
            } elseif ($_POST['user_first_name'] == '') {
                $display .= '<p>' . $lang['user_editor_need_first_name'] . '</p>';
            } elseif ($_POST['user_last_name'] == '') {
                $display .= '<p>' . $lang['user_editor_need_last_name'] . '</p>';
            } else {
                $sql_user_name = $misc->makeDbSafe(strip_tags($_POST['edit_user_name']));
                $sql_user_email = $misc->makeDbSafe(strip_tags($_POST['user_email']));
                $pass_the_form = 'Yes';

                // first, make sure the user name isn't in use
                $sql = 'SELECT userdb_user_name from ' . $this->config['table_prefix'] . 'userdb WHERE userdb_user_name = ' . $sql_user_name;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $num = $recordSet->RecordCount();
                // second, make sure the user eamail isn't in use
                $sql2 = 'SELECT userdb_emailaddress from ' . $this->config['table_prefix'] . 'userdb WHERE userdb_emailaddress = ' . $sql_user_email;
                $recordSet2 = $ORconn->Execute($sql2);
                if (is_bool($recordSet2)) {
                    $misc->logErrorAndDie($sql2);
                }
                $num2 = $recordSet2->RecordCount();
                if ($num >= 1) {
                    $pass_the_form = 'No';
                    $display .= $lang['user_creation_username_taken'];
                } elseif ($num2 >= 1) {
                    $pass_the_form = 'No';
                    $display .= $lang['email_address_already_registered'];
                } // end if
                if ($pass_the_form == 'Yes') {
                    if (
                        isset(
                            $_POST['edit_user_pass'],
                            $_POST['user_first_name'],
                            $_POST['user_last_name'],
                            $_POST['edit_active'],
                            $_POST['edit_isAgent'],
                            $_POST['edit_isAdmin']
                        )
                        && is_string($_POST['edit_user_pass'])
                        && is_string($_POST['user_first_name'])
                        && is_string($_POST['user_last_name'])
                        && is_string($_POST['edit_active'])
                        && is_string($_POST['edit_isAgent'])
                        && is_string($_POST['edit_isAdmin'])
                    ) {
                        // what the program should do if the form is valid
                        // generate a random number to enter in as the password (initially)
                        // we'll need to know the actual account id to help with retrieving the user
                        // We will be putting in a random number that we know the value of, we can easily
                        // retrieve the account id in a few moments
                        $sql_user_name = $misc->makeDbSafe(strip_tags($_POST['edit_user_name']));
                        $hash = password_hash($_POST['edit_user_pass'], PASSWORD_DEFAULT);
                        $sql_hash = $misc->makeDbSafe($hash);
                        $sql_user_email = $misc->makeDbSafe(strip_tags($_POST['user_email']));
                        $sql_user_first_name = $misc->makeDbSafe(strip_tags($_POST['user_first_name']));
                        $sql_user_last_name = $misc->makeDbSafe(strip_tags($_POST['user_last_name']));
                        $sql_edit_active = $misc->makeDbSafe($_POST['edit_active']);
                        $sql_edit_isAgent = $misc->makeDbSafe($_POST['edit_isAgent']);
                        $sql_edit_isAdmin = $misc->makeDbSafe($_POST['edit_isAdmin']);
                        if ($_POST['edit_isAdmin'] == 'yes') {
                            $sql_edit_limitFeaturedListings = $misc->makeDbSafe('-1');
                            if (isset($_POST['edit_userRank']) && is_numeric($_POST['edit_userRank'])) {
                                $sql_edit_userRank = intval($_POST['edit_userRank']);
                            } else {
                                $sql_edit_userRank = 1;
                            }
                            $sql_limitListings = $misc->makeDbSafe('-1');
                            $sql_edit_canEditSiteConfig = $misc->makeDbSafe('no');
                            $sql_edit_canEditMemberTemplate = $misc->makeDbSafe('no');
                            $sql_edit_canEditAgentTemplate = $misc->makeDbSafe('no');
                            $sql_edit_canEditListingTemplate = $misc->makeDbSafe('no');
                            $sql_edit_canFeatureListings = $misc->makeDbSafe('no');
                            $sql_edit_canViewLogs = $misc->makeDbSafe('no');
                            $sql_edit_canModerate = $misc->makeDbSafe('no');
                            $sql_edit_canPages = $misc->makeDbSafe('no');
                            $sql_edit_canVtour = $misc->makeDbSafe('no');
                            $sql_edit_canFiles = $misc->makeDbSafe('no');
                            $sql_edit_canUserFiles = $misc->makeDbSafe('no');
                            $sql_edit_canExportListings = $misc->makeDbSafe('no');
                            $sql_edit_canEditListingExpiration = $misc->makeDbSafe('no');
                            $sql_edit_canEditAllListings = $misc->makeDbSafe('no');
                            $sql_edit_canEditAllUsers = $misc->makeDbSafe('no');
                            $sql_edit_canEditPropertyClasses = $misc->makeDbSafe('no');
                            $sql_edit_canManageAddons = $misc->makeDbSafe('no');
                            $sql_edit_blogUserType = $misc->makeDbSafe('4');
                        } elseif ($_POST['edit_isAgent'] == 'yes') {
                            if ($this->config['agent_default_edit_site_config']) {
                                $sql_edit_canEditSiteConfig = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canEditSiteConfig = $misc->makeDbSafe('no');
                            }
                            if ($this->config['agent_default_edit_member_template']) {
                                $sql_edit_canEditMemberTemplate = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canEditMemberTemplate = $misc->makeDbSafe('no');
                            }
                            if ($this->config['agent_default_edit_agent_template']) {
                                $sql_edit_canEditAgentTemplate = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canEditAgentTemplate = $misc->makeDbSafe('no');
                            }

                            if ($this->config['agent_default_edit_listing_template']) {
                                $sql_edit_canEditListingTemplate = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canEditListingTemplate = $misc->makeDbSafe('no');
                            }
                            if ($this->config['agent_default_feature']) {
                                $sql_edit_canFeatureListings = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canFeatureListings = $misc->makeDbSafe('no');
                            }
                            if ($this->config['agent_default_logview']) {
                                $sql_edit_canViewLogs = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canViewLogs = $misc->makeDbSafe('no');
                            }
                            if ($this->config['agent_default_moderate']) {
                                $sql_edit_canModerate = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canModerate = $misc->makeDbSafe('no');
                            }
                            if ($this->config['agent_default_editpages']) {
                                $sql_edit_canPages = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canPages = $misc->makeDbSafe('no');
                            }
                            if ($this->config['agent_default_havevtours']) {
                                $sql_edit_canVtour = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canVtour = $misc->makeDbSafe('no');
                            }
                            if ($this->config['agent_default_havefiles']) {
                                $sql_edit_canFiles = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canFiles = $misc->makeDbSafe('no');
                            }
                            if ($this->config['agent_default_haveuserfiles']) {
                                $sql_edit_canUserFiles = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canUserFiles = $misc->makeDbSafe('no');
                            }
                            $sql_limitListings = $misc->makeDbSafe($this->config['agent_default_num_listings']);
                            $sql_edit_limitFeaturedListings = $misc->makeDbSafe($this->config['agent_default_num_featuredlistings']);

                            if ($this->config['agent_default_can_export_listings']) {
                                $sql_edit_canExportListings = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canExportListings = $misc->makeDbSafe('no');
                            }
                            if ($this->config['agent_default_canchangeexpirations']) {
                                $sql_edit_canEditListingExpiration = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canEditListingExpiration = $misc->makeDbSafe('no');
                            }
                            if ($this->config['agent_default_edit_all_listings']) {
                                $sql_edit_canEditAllListings = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canEditAllListings = $misc->makeDbSafe('no');
                            }
                            if ($this->config['agent_default_edit_all_users']) {
                                $sql_edit_canEditAllUsers = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canEditAllUsers = $misc->makeDbSafe('no');
                            }

                            if ($this->config['agent_default_edit_property_classes']) {
                                $sql_edit_canEditPropertyClasses = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canEditPropertyClasses = $misc->makeDbSafe('no');
                            }

                            if ($this->config['agent_default_canManageAddons']) {
                                $sql_edit_canManageAddons = $misc->makeDbSafe('yes');
                            } else {
                                $sql_edit_canManageAddons = $misc->makeDbSafe('no');
                            }
                            if (isset($_POST['edit_userRank'])) {
                                $sql_edit_userRank = intval($_POST['edit_userRank']);
                            } else {
                                $sql_edit_userRank = 1;
                            }
                            $sql_edit_blogUserType = $misc->makeDbSafe($this->config['agent_default_blogUserType']);
                        } else {
                            $sql_edit_canEditSiteConfig = $misc->makeDbSafe('no');
                            $sql_edit_canEditMemberTemplate = $misc->makeDbSafe('no');
                            $sql_edit_canEditAgentTemplate = $misc->makeDbSafe('no');
                            $sql_edit_canEditListingTemplate = $misc->makeDbSafe('no');
                            $sql_edit_canFeatureListings = $misc->makeDbSafe('no');
                            $sql_edit_canViewLogs = $misc->makeDbSafe('no');
                            $sql_edit_canModerate = $misc->makeDbSafe('no');
                            $sql_edit_canPages = $misc->makeDbSafe('no');
                            $sql_edit_canVtour = $misc->makeDbSafe('no');
                            $sql_edit_canFiles = $misc->makeDbSafe('no');
                            $sql_edit_canUserFiles = $misc->makeDbSafe('no');
                            $sql_edit_canExportListings = $misc->makeDbSafe('no');
                            $sql_edit_canEditListingExpiration = $misc->makeDbSafe('no');
                            $sql_edit_canEditAllListings = $misc->makeDbSafe('no');
                            $sql_edit_canEditAllUsers = $misc->makeDbSafe('no');
                            $sql_limitListings = 0;
                            $sql_edit_limitFeaturedListings = 0;
                            $sql_edit_userRank = 0;
                            $sql_edit_canEditPropertyClasses = $misc->makeDbSafe('no');
                            $sql_edit_canManageAddons = $misc->makeDbSafe('no');
                            $sql_edit_blogUserType = $misc->makeDbSafe('1');
                        }
                        // create the account with the random number as the password
                        $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'userdb (userdb_user_name, userdb_user_password,userdb_user_first_name ,userdb_user_last_name, userdb_emailAddress,
						userdb_creation_date,userdb_last_modified,userdb_active,userdb_is_agent,userdb_is_admin,userdb_can_edit_member_template,
						userdb_can_edit_agent_template,userdb_can_edit_listing_template,userdb_can_feature_listings,userdb_can_view_logs,
						userdb_can_moderate,userdb_can_edit_pages,userdb_can_have_vtours,userdb_can_have_files,userdb_can_have_user_files,userdb_limit_listings,userdb_comments,userdb_hit_count,
						userdb_can_edit_expiration,userdb_can_export_listings,userdb_can_edit_all_users,userdb_can_edit_all_listings,userdb_can_edit_site_config,userdb_can_edit_property_classes,userdb_can_manage_addons,userdb_rank,userdb_featuredlistinglimit,userdb_email_verified,userdb_blog_user_type) VALUES
						(' . $sql_user_name . ',' . $sql_hash . ',' . $sql_user_first_name . ',' . $sql_user_last_name . ',' . $sql_user_email . ',' . $ORconn->DBDate(time()) . ',' . $ORconn->DBTimeStamp(time()) . ','
                            . $sql_edit_active . ',' . $sql_edit_isAgent . ',' . $sql_edit_isAdmin . ',' . $sql_edit_canEditMemberTemplate . ',' . $sql_edit_canEditAgentTemplate . ',' . $sql_edit_canEditListingTemplate . ',' . $sql_edit_canFeatureListings . ',' . $sql_edit_canViewLogs . ',' . $sql_edit_canModerate . ','
                            . $sql_edit_canPages . ',' . $sql_edit_canVtour . ',' . $sql_edit_canFiles . ',' . $sql_edit_canUserFiles . ',' . $sql_limitListings . ',\'\',0,' . $sql_edit_canEditListingExpiration . ',' . $sql_edit_canExportListings
                            . ',' . $sql_edit_canEditAllUsers . ',' . $sql_edit_canEditAllListings . ',' . $sql_edit_canEditSiteConfig . ',' . $sql_edit_canEditPropertyClasses . ',' . $sql_edit_canManageAddons . ',' . $sql_edit_userRank . ',' . $sql_edit_limitFeaturedListings . ',\'yes\',' . $sql_edit_blogUserType . ')';
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            $misc->logErrorAndDie($sql);
                        }
                        $new_user_id = $ORconn->Insert_ID(); // this is the new User's ID number

                        //call the new User plugin function

                        $hooks = $this->newHooks();
                        $hooks->load('afterUserSignup', $new_user_id);

                        return $new_user_id;
                    }
                }
            }
        }
        return $display;
    }

    public function deleteUser(int $user_id): int|string
    {
        //deletes userdb_id #4 and any associated listings and media
        $user_api = $this->newUserApi();
        try {
            $user_api->delete([
                'user_id' => $user_id,
            ]);
        } catch (Exception) {
            return 'Deletion of ' . $user_id . ' Failed';
        }
        return 1;
    }

    public function updateUserData(int $user_id): string
    {
        // UPDATES THE USER INFORMATION
        global $ORconn;

        $misc = $this->newMisc();
        $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'userdbelements 
				WHERE userdb_id = ' . $user_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $sql3 = 'SELECT userdb_is_agent, userdb_is_admin 
				FROM ' . $this->config['table_prefix'] . 'userdb 
				WHERE userdb_id = ' . $user_id;
        $recordSet3 = $ORconn->Execute($sql3);
        if (is_bool($recordSet3)) {
            $misc->logErrorAndDie($sql3);
        }
        if ($recordSet3->fields('userdb_is_agent') == 'yes' || $recordSet3->fields('userdb_is_admin') == 'yes') {
            $db_to_use = 'agent';
        } else {
            $db_to_use = 'member';
        }
        foreach ($_POST as $ElementIndexValue => $ElementContents) {
            $sql_field_name = $misc->makeDbSafe($ElementIndexValue);
            $sql2 = 'SELECT ' . $db_to_use . 'formelements_field_type 
					FROM ' . $this->config['table_prefix'] . $db_to_use . 'formelements 
					WHERE ' . $db_to_use . "formelements_field_name=" . $sql_field_name;
            $recordSet2 = $ORconn->Execute($sql2);
            if (is_bool($recordSet2)) {
                $misc->logErrorAndDie($sql2);
            }
            if ($recordSet2->RecordCount() == 1) {
                $field_type = $recordSet2->fields($db_to_use . 'formelements_field_type');
                // first, ignore all the stuff that's been taken care of above
                if ($ElementIndexValue == 'token' || $ElementIndexValue == 'user_user_name' || $ElementIndexValue == 'edit_user_pass' || $ElementIndexValue == 'edit_user_pass2' || $ElementIndexValue == 'user_email' || $ElementIndexValue == 'PHPSESSID' || $ElementIndexValue == 'edit' || $ElementIndexValue == 'edit_isAdmin' || $ElementIndexValue == 'edit_active' || $ElementIndexValue == 'edit_isAgent' || $ElementIndexValue == 'edit_limitListings' || $ElementIndexValue == 'edit_canEditSiteConfig' || $ElementIndexValue == 'edit_canMemberTemplate' || $ElementIndexValue == 'edit_canAgentTemplate' || $ElementIndexValue == 'edit_canListingTemplate' || $ElementIndexValue == 'edit_canViewLogs' || $ElementIndexValue == 'edit_canModerate' || $ElementIndexValue == 'edit_canFeatureListings' || $ElementIndexValue == 'edit_canPages' || $ElementIndexValue == 'edit_canVtour' || $ElementIndexValue == 'edit_canFiles' || $ElementIndexValue == 'edit_canUserFiles') {
                    // do nothing
                } elseif (is_array($ElementContents)) {
                    $ElementContentsStrings = array_filter($ElementContents, 'is_string');
                    // deal with checkboxes & multiple selects elements
                    $feature_insert = $misc->makeDbSafe(implode('||', $ElementContentsStrings));
                    $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'userdbelements (userdbelements_field_name, userdbelements_field_value, userdb_id) 
							VALUES (' . $sql_field_name . ', ' . $feature_insert . ', ' . $user_id . ')';
                    // }
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                } else {
                    // it's time to actually insert the form data into the db
                    $sql_ElementIndexValue = $misc->makeDbSafe($ElementIndexValue);
                    $sql_ElementContents = $misc->makeDbSafe($ElementContents);
                    // if ($_SESSION['admin_privs'] == 'yes' && $_GET['edit'] != "")
                    // {
                    // $sql_edit = $misc->makeDbSafe($_GET['edit']);
                    // $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'userdbelements (userdbelements_field_name, userdbelements_field_value, userdb_id) VALUES ('.$sql_ElementIndexValue.', '.$sql_ElementContents.', '.$sql_edit.')';
                    // }
                    // else
                    // {
                    // $sql_user_id = $misc->makeDbSafe($_SESSION['userID']);
                    if ($field_type == 'date' && $ElementContents != '') {
                        if ($this->config['date_format'] == 1) {
                            $format = '%m/%d/%Y';
                        } elseif ($this->config['date_format'] == 2) {
                            $format = '%Y/%d/%m';
                        } else {
                            $format = '%d/%m/%Y';
                        }
                        $returnValue = $misc->parseDate($ElementContents, $format);
                        $sql_ElementContents = $misc->makeDbSafe($returnValue);
                    }
                    $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'userdbelements (userdbelements_field_name, userdbelements_field_value, userdb_id) 
							VALUES (' . $sql_ElementIndexValue . ', ' . $sql_ElementContents . ', ' . $user_id . ')';
                    // }
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                } // end else
            }
        } // end while
        return 'success';
    }

    public function verifyEmail(): string
    {
        global $ORconn, $lang;
        $display = '';

        $misc = $this->newMisc();
        $login = $this->newLogin();

        if (isset($_GET['hash']) && is_string($_GET['hash']) && $_GET['hash'] != '' && isset($_GET['id']) && is_numeric($_GET['id'])) {
            //Read Key From DB
            $id = intval($_GET['id']);
            $userID = $misc->makeDbSafe($id);
            $sql = 'SELECT  userdb_verification_hash FROM ' . $this->config['table_prefix'] . 'userdb WHERE userdb_id = ' . $userID;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $key = $recordSet->fields('userdb_verification_hash');

            $raw = base64_decode($_GET['hash']);
            $decrypted = openssl_decrypt($raw, 'AES-128-ECB', $key);

            $params = explode(',', $decrypted);

            $userID = intval($params['0']);
            if ($id !== $userID) {
                $display .= '<p class="notice">' . $lang['verify_email_invalid_link'] . '</div>';
                return $display;
            }
            //the User API won't get the password, so we leave this a SQL lookup
            $sql = 'SELECT userdb_id, userdb_user_name, userdb_user_password, userdb_emailaddress, userdb_is_agent 
					FROM ' . $this->config['table_prefix'] . "userdb 
					WHERE userdb_id = '" . $userID . "'";

            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $user_id = (int)$recordSet->fields('userdb_id');
            $user_name = (string)$recordSet->fields('userdb_user_name');
            $emailAddress = (string)$recordSet->fields('userdb_emailaddress');
            $valid = false;
            if ($emailAddress == $params['1']) {
                $valid = true;
            }
            if ($recordSet->fields('userdb_is_agent') == 'yes') {
                $type = 'agent';
            } else {
                $type = 'member';
            }
            if ($this->config['moderate_' . $type . 's'] == 0) {
                if ($type == 'agent') {
                    if (!$this->config['agent_default_active']) {
                        $set_active = 'no';
                    } else {
                        $set_active = 'yes';
                    }
                } else {
                    $set_active = 'yes';
                }
            } else {
                $set_active = 'no';
            }
            $sql_set_active = $misc->makeDbSafe($set_active);
            if ($valid) {
                if ($this->config['email_notification_of_new_users']) {
                    // if the site admin should be notified when a new User is added
                    $remote_ip = $_SERVER['REMOTE_ADDR'] ?? '';
                    $signup_timestamp = date('F j, Y, g:i:s a');
                    $this->sendUserSignupNotification($user_id, $type, $remote_ip, $signup_timestamp);
                }
                $verified = $misc->makeDbSafe('yes');
                $sql = 'UPDATE ' . $this->config['table_prefix'] . 'userdb 
						SET userdb_active = ' . $sql_set_active . ', 
                        userdb_email_verified = ' . $verified . ',
                        userdb_verification_hash = NULL
						WHERE userdb_id = ' . $userID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $display .= '<p class="notice">' . $lang['verify_email_thanks'] . '</p>';
                if ($this->config['moderate_' . $type . 's'] == 1) {
                    // if moderation is turned on...
                    $display .= '<p>' . $lang['admin_new_user_moderated'] . '</p>';
                } else {
                    //log the user in
                    $login->setSessionVars($user_name);
                    //$login->verifyPriv('Member');
                    $display .= '<p>' . $lang['you_may_now_view_priv'] . '</p>';
                }
            } else {
                $display .= '<p class="notice">' . $lang['verify_email_invalid_link'] . '</div>';
            }
        } else {
            $display .= '<p class="notice">' . $lang['verify_email_invalid_link'] . '</div>';
        }
        return $display;
    }

    //consolidated from send_agent_signup_email() and send_member_signup_email()
    //refactored for v3.2.11
    public function sendUserSignupEmail(int $userid, string $type, string $pass = ''): void
    {
        global $ORconn;

        $misc = $this->newMisc();
        $page = $this->newPageUser();

        $user = $this->newUser();

        if ($type == 'agent') {
            $page->loadPage($this->config['admin_template_path'] . '/email/agent_signup_email.html');
            $page->replaceTag('agent_login_link', $page->magicURIGenerator('agent_login', null, true));
            $page->replaceUserFieldTags($userid, '', 'agent');
            $passtag = 'agent_password';
        } else {
            $page->loadPage($this->config['admin_template_path'] . '/email/member_signup_email.html');
            $page->replaceTag('member_login_link', $page->magicURIGenerator('member_login', null, true));
            $page->replaceUserFieldTags($userid);
            $passtag = 'member_password';
        }

        $reg_info = $user->getUserRegInfo($userid);
        $user_email = $reg_info['emailaddress'];
        //$user_email = $user->getUserSingleItem('userdb_emailaddress', $userid);

        if (($type == 'agent' && $this->config['moderate_agents'] == 1) || ($type == 'member' && $this->config['moderate_members'])) {
            $page->page = $page->cleanupTemplateBlock('moderated', $page->page);
        } else {
            $page->page = $page->removeTemplateBlock('moderated', $page->page);
        }
        if ($this->config['require_email_verification'] && $pass == '') {
            try {
                $rand = random_bytes(32);
            } catch (Exception $e) {
                $misc->logErrorAndDie($e->getMessage());
            }
            $key = substr(base64_encode($rand), 0, 32);
            $safe_key = $misc->makeDbSafe($key);
            //Store Key in DB
            $sql = 'UPDATE ' . $this->config['table_prefix'] . 'userdb 
                SET userdb_verification_hash = ' . $safe_key . '
                WHERE userdb_id = ' . $userid;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $params = $userid . ',' . $user_email;

            $encrypted = urlencode(base64_encode(openssl_encrypt($params, 'AES-128-ECB', $key)));

            $page->page = $page->cleanupTemplateBlock('require_email_verification', $page->page);
            $verification_link = $this->config['baseurl'] . '/index.php?action=verify_email&hash=' . $encrypted . '&amp;id=' . $userid;
            $page->replaceTag('verification_email_link', $verification_link);
        } else {
            $page->page = $page->removeTemplateBlock('require_email_verification', $page->page);
        }

        if ($pass == '') {
            $page->page = $page->cleanupTemplateBlock('!password', $page->page);
            $page->page = $page->removeTemplateBlock('password', $page->page);
        } else {
            $page->replaceTag($passtag, $pass);
            $page->page = $page->cleanupTemplateBlock('password', $page->page);
            $page->page = $page->removeTemplateBlock('!password', $page->page);
        }

        $page->replaceLangTemplateTags();
        $subject = $page->getTemplateSection('subject_block');
        $page->page = $page->removeTemplateBlock('subject', $page->page);
        $page->autoReplaceTags();
        $message = $page->returnPage();

        if (isset($this->config['site_email']) && $this->config['site_email'] != '') {
            $sender_email = $this->config['site_email'];
        } else {
            $sender_email = $this->config['admin_email'];
        }
        $misc->sendEmail($this->config['admin_name'], $sender_email, $user_email, $message, $subject, true);
    }

    //consolidated from send_agent_signup_notification() and send_member_signup_notification()
    //refactored for v3.2.11
    public function sendUserSignupNotification(int $userid, string $type, string $signup_ip, string $signup_timestamp): void
    {
        $misc = $this->newMisc();
        $page = $this->newPageUser();

        if ($type == 'agent') {
            $page->loadPage($this->config['admin_template_path'] . '/email/agent_signup_notification.html');
            $page->replaceUserFieldTags($userid, '', 'agent');
        } else {
            $page->loadPage($this->config['admin_template_path'] . '/email/member_signup_notification.html');
            $page->replaceUserFieldTags($userid);
        }

        $page->replaceTag('user_ip', $signup_ip);
        $page->replaceTag('notification_time', $signup_timestamp);
        $page->replaceLangTemplateTags();
        $subject = $page->getTemplateSection('subject_block');
        $page->page = $page->removeTemplateBlock('subject', $page->page);
        $page->autoReplaceTags();
        $message = $page->returnPage();
        if (isset($this->config['site_email']) && $this->config['site_email'] != '') {
            $sender_email = $this->config['site_email'];
        } else {
            $sender_email = $this->config['admin_email'];
        }
        $misc->sendEmail($this->config['admin_name'], $sender_email, $this->config['admin_email'], $message, $subject, true, true);
    }

    //consolidated from send_agent_verification_email() and send_member_verification_email()
    //refactored for v3.2.11
    public function sendUserVerificationEmail(int $userid, string $type): void
    {
        $misc = $this->newMisc();
        $page = $this->newPageUser();

        $user = $this->newUser();

        if ($type == 'agent') {
            $page->loadPage($this->config['admin_template_path'] . '/email/agent_activation_email.html');
            $page->replaceTag('agent_login_link', $page->magicURIGenerator('agent_login', null, true));
            $page->replaceUserFieldTags($userid, '', 'agent');
        } else {
            $page->loadPage($this->config['admin_template_path'] . '/email/member_activation_email.html');
            $page->replaceTag('member_login_link', $page->magicURIGenerator('member_login', null, true));
            $page->replaceUserFieldTags($userid);
        }

        $reg_info = $user->getUserRegInfo($userid);
        $user_email = $reg_info['emailaddress'];

        //$user_email = $user->getUserSingleItem('userdb_emailaddress', $userid);
        $page->replaceLangTemplateTags();
        $subject = $page->getTemplateSection('subject_block');
        $page->page = $page->removeTemplateBlock('subject', $page->page);
        $page->autoReplaceTags();
        $message = $page->returnPage();

        if (isset($this->config['site_email']) && $this->config['site_email'] != '') {
            $sender_email = $this->config['site_email'];
        } else {
            $sender_email = $this->config['admin_email'];
        }
        $misc->sendEmail($this->config['admin_name'], $sender_email, $user_email, $message, $subject, true);
    }


    public function showEditUser(): string
    {
        global $lang;

        $misc = $this->newMisc();
        $page = $this->newPageAdmin();

        $forms = $this->newForms();

        $login = $this->newLogin();
        $page->loadPage($this->config['admin_template_path'] . '/edit_user.html');

        $display = '';

        if (isset($_GET['user_id']) && $_GET['user_id'] == $_SESSION['userID']) {
            $security = true;
        } else {
            $security = $login->verifyPriv('edit_all_users');
        }

        if ($security) {
            if (isset($_GET['user_id'])) {
                $user_id = intval($_GET['user_id']);
                $yes_no = ['no' => 'No', 'yes' => 'Yes'];
                $yes_no_10 = [0 => 'No', 1 => 'Yes'];
                /*//Blog Permissions
                 * 1 - Subscriber - A subscriber can read posts, comment on posts.
                 * 2 - Contributor - A contributor can post and manage their own post but they cannot publish the posts. An administrator must first approve the post before it can be published.
                 * 3 - Author - The Author role allows someone to publish and manage posts. They can only manage their own posts, no one else’s.
                 * 4 - Editor - An editor can publish posts. They can also manage and edit other users posts. If you are looking for someone to edit your posts, you would assign the Editor role to that person.
                 $lang['blog_perm_subscriber'] = 'Subscriber';
                 $lang['blog_perm_contributor'] = 'Contributor';
                 $lang['blog_perm_author'] = 'Author';
                 $lang['blog_perm_editor'] = 'Editor';
                 */
                $blog_type = [1 => $lang['blog_perm_subscriber'], 2 => $lang['blog_perm_contributor'], 3 => $lang['blog_perm_author'], 4 => $lang['blog_perm_editor']];

                $is_admin = $misc->getAdminStatus($user_id);
                $is_agent = $misc->getAgentStatus($user_id);

                if ($is_agent === true || $is_admin === true) {
                    $resource = 'agent';
                } else {
                    $resource = 'member';
                }

                //read all data for this user
                $user_api = $this->newUserApi();
                try {
                    $result = $user_api->read([
                        'user_id' => $user_id,
                        'resource' => $resource,
                    ]);
                } catch (Exception) {
                    $display .= 'User lookup failure';
                    return $display;
                }


                $edit_username = $result['user']['userdb_user_name'];
                $edit_emailAddress = $result['user']['userdb_emailaddress'];
                // $edit_comments = $recordSet->fields('userdb_comments');
                $edit_firstname = $result['user']['userdb_user_first_name'];
                $edit_lastname = $result['user']['userdb_user_last_name'];
                $edit_active = $result['user']['userdb_active'];
                $edit_isAgent = $result['user']['userdb_is_agent'];
                $edit_isAdmin = $result['user']['userdb_is_admin'];
                $edit_limitListings = $result['user']['userdb_limit_listings'];
                $edit_limitFeaturedListings = $result['user']['userdb_featuredlistinglimit'];
                $edit_userRank = $result['user']['userdb_rank'];
                $edit_canEditAllListings = $result['user']['userdb_can_edit_all_listings'];
                $edit_canEditAllUsers = $result['user']['userdb_can_edit_all_users'];
                $edit_canEditSiteConfig = $result['user']['userdb_can_edit_site_config'];
                $edit_canEditMemberTemplate = $result['user']['userdb_can_edit_member_template'];
                $edit_canEditAgentTemplate = $result['user']['userdb_can_edit_agent_template'];
                $edit_canEditListingTemplate = $result['user']['userdb_can_edit_listing_template'];
                $edit_canExportListings = $result['user']['userdb_can_export_listings'];
                $edit_canEditListingExpiration = $result['user']['userdb_can_edit_expiration'];
                $edit_canEditPropertyClasses = $result['user']['userdb_can_edit_property_classes'];
                $edit_canModerate = $result['user']['userdb_can_moderate'];
                $edit_canViewLogs = $result['user']['userdb_can_view_logs'];
                $edit_canVtour = $result['user']['userdb_can_have_vtours'];
                $edit_canFiles = $result['user']['userdb_can_have_files'];
                $edit_canUserFiles = $result['user']['userdb_can_have_user_files'];
                $edit_canFeatureListings = $result['user']['userdb_can_feature_listings'];
                $edit_canPages = $result['user']['userdb_can_edit_pages'];
                $edit_BlogPrivileges = $result['user']['userdb_blog_user_type'];
                $last_modified = date($this->config['date_format_timestamp'], strtotime($result['user']['userdb_last_modified']));

                //$last_modified = $recordSet->UserTimeStamp($result['user']['userdb_last_modified'], $this->config["date_format_timestamp"]);
                $edit_canManageAddons = $result['user']['userdb_can_manage_addons'];
                $edit_can_edit_all_leads = $result['user']['userdb_can_edit_all_leads'];
                $edit_can_edit_lead_template = $result['user']['userdb_can_edit_lead_template'];
                $edit_userFloorNotify = $result['user']['userdb_send_notifications_to_floor'];

                //Replace Fields
                $page->page = str_replace('{user_id}', (string)$user_id, $page->page);
                $page->page = str_replace('{user_name}', $edit_username, $page->page);
                $page->page = str_replace('{user_email}', $edit_emailAddress, $page->page);
                $page->page = str_replace('{user_first_name}', $edit_firstname, $page->page);
                $page->page = str_replace('{user_last_name}', $edit_lastname, $page->page);
                $page->page = str_replace('{last_modified}', $last_modified, $page->page);

                if ($_SESSION['admin_privs'] == 'yes' || $_GET['user_id'] == $_SESSION['userID'] && !$this->config['demo_mode']) {
                    $page->page = $page->cleanupTemplateBlock('user_password', $page->page);
                } else {
                    $page->page = $page->removeTemplateBlock('user_password', $page->page);
                }

                $html = $page->getTemplateSection('user_status_option_block');
                $html = $page->formOptions($yes_no, $edit_active, $html);
                $page->replaceTemplateSection('user_status_option_block', $html);

                $page->replaceTag('user_isAdmin', $edit_isAdmin);
                $page->replaceTag('user_isAgent', $edit_isAgent);

                if ($is_agent === true || $is_admin === true) {
                    $page->page = $page->cleanupTemplateBlock('agentadmin', $page->page);
                } else {
                    $page->page = $page->removeTemplateBlock('agentadmin', $page->page);
                }

                $page->replaceTag('user_limitListings', (string)$edit_limitListings);
                $page->replaceTag('user_limitFeaturedListings', (string)$edit_limitFeaturedListings);
                $page->replaceTag('user_userRank', (string)$edit_userRank);

                //Floor Notification
                $html = $page->getTemplateSection('userFloorNotify_block');
                $html = $page->formOptions($yes_no_10, (string)$edit_userFloorNotify, $html);
                $page->replaceTemplateSection('userFloorNotify_block', $html);

                if ($is_agent === true) {
                    $page->page = $page->cleanupTemplateBlock('agent', $page->page);
                } else {
                    $page->page = $page->removeTemplateBlock('agent', $page->page);
                }

                //Agent Permissions
                $html = $page->getTemplateSection('canEditAllListings_block');
                $html = $page->formOptions($yes_no, (string)$edit_canEditAllListings, $html);
                $page->replaceTemplateSection('canEditAllListings_block', $html);

                $html = $page->getTemplateSection('canEditAllUsers_block');
                $html = $page->formOptions($yes_no, (string)$edit_canEditAllUsers, $html);
                $page->replaceTemplateSection('canEditAllUsers_block', $html);

                $html = $page->getTemplateSection('canEditSiteConfig_block');
                $html = $page->formOptions($yes_no, (string)$edit_canEditSiteConfig, $html);
                $page->replaceTemplateSection('canEditSiteConfig_block', $html);

                $html = $page->getTemplateSection('canEditMemberTemplate_block');
                $html = $page->formOptions($yes_no, (string)$edit_canEditMemberTemplate, $html);
                $page->replaceTemplateSection('canEditMemberTemplate_block', $html);

                $html = $page->getTemplateSection('canEditAgentTemplate_block');
                $html = $page->formOptions($yes_no, (string)$edit_canEditAgentTemplate, $html);
                $page->replaceTemplateSection('canEditAgentTemplate_block', $html);

                $html = $page->getTemplateSection('canEditListingTemplate_block');
                $html = $page->formOptions($yes_no, $edit_canEditListingTemplate, $html);
                $page->replaceTemplateSection('canEditListingTemplate_block', $html);

                $html = $page->getTemplateSection('canEditPropertyClasses_block');
                $html = $page->formOptions($yes_no, $edit_canEditPropertyClasses, $html);
                $page->replaceTemplateSection('canEditPropertyClasses_block', $html);

                $html = $page->getTemplateSection('canViewLogs_block');
                $html = $page->formOptions($yes_no, $edit_canViewLogs, $html);
                $page->replaceTemplateSection('canViewLogs_block', $html);

                $html = $page->getTemplateSection('canModerate_block');
                $html = $page->formOptions($yes_no, $edit_canModerate, $html);
                $page->replaceTemplateSection('canModerate_block', $html);

                $html = $page->getTemplateSection('canFeatureListings_block');
                $html = $page->formOptions($yes_no, $edit_canFeatureListings, $html);
                $page->replaceTemplateSection('canFeatureListings_block', $html);

                $html = $page->getTemplateSection('canPages_block');
                $html = $page->formOptions($yes_no, $edit_canPages, $html);
                $page->replaceTemplateSection('canPages_block', $html);

                $html = $page->getTemplateSection('canVtour_block');
                $html = $page->formOptions($yes_no, $edit_canVtour, $html);
                $page->replaceTemplateSection('canVtour_block', $html);

                $html = $page->getTemplateSection('canFiles_block');
                $html = $page->formOptions($yes_no, $edit_canFiles, $html);
                $page->replaceTemplateSection('canFiles_block', $html);

                $html = $page->getTemplateSection('canUserFiles_block');
                $html = $page->formOptions($yes_no, $edit_canUserFiles, $html);
                $page->replaceTemplateSection('canUserFiles_block', $html);

                $html = $page->getTemplateSection('canEditListingExpiration_block');
                $html = $page->formOptions($yes_no, $edit_canEditListingExpiration, $html);
                $page->replaceTemplateSection('canEditListingExpiration_block', $html);

                $html = $page->getTemplateSection('BlogPrivileges_block');
                $html = $page->formOptions($blog_type, (string)$edit_BlogPrivileges, $html);
                $page->replaceTemplateSection('BlogPrivileges_block', $html);

                $html = $page->getTemplateSection('canManageAddons_block');
                $html = $page->formOptions($yes_no, $edit_canManageAddons, $html);
                $page->replaceTemplateSection('canManageAddons_block', $html);

                $html = $page->getTemplateSection('canExportListings_block');
                $html = $page->formOptions($yes_no, $edit_canExportListings, $html);
                $page->replaceTemplateSection('canExportListings_block', $html);

                $html = $page->getTemplateSection('can_edit_all_leads_block');
                $html = $page->formOptions($yes_no, $edit_can_edit_all_leads, $html);
                $page->replaceTemplateSection('can_edit_all_leads_block', $html);

                $html = $page->getTemplateSection('can_edit_lead_template_block');
                $html = $page->formOptions($yes_no, $edit_can_edit_lead_template, $html);
                $page->replaceTemplateSection('can_edit_lead_template_block', $html);

                //Handle Custom User Fields
                $fields_api = $this->newFieldsApi();
                ;
                try {
                    $field_list = $fields_api->metadata(['resource' => $resource]);
                } catch (Exception $e) {
                    $misc->logErrorAndDie($e->getMessage());
                }

                $misc_hold = '';
                foreach ($field_list['fields'] as $field) {
                    $field_name = $field['field_name'] ?? '';
                    if (array_key_exists($field_name, $result['user'])) {
                        $field_value = $result['user'][$field_name];
                    } else {
                        $field_value = '';
                    }

                    $field_type = $field['field_type'] ?? '';
                    $field_caption = $field['field_caption'] ?? '';
                    $default_text = $field['default_text'] ?? '';
                    $field_elements = $field['field_elements'] ?? [];
                    $required = $field['required'] ?? 'No';
                    $tool_tip = $field['tool_tip'] ?? '';
                    // pass the data to the function
                    $field = $forms->renderFormElement($field_type, $field_name, $field_value, $field_caption, $default_text, $required, $field_elements, '', $tool_tip);

                    $misc_hold .= $field;
                }

                $page->page = str_replace('{misc_hold}', $misc_hold, $page->page);
            }
            $page->replaceTags(['curley_open', 'curley_close', 'baseurl']);
            $page->replaceTag('application_status_text', '');
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        } else {
            $display .= $lang['listing_editor_permission_denied'];
            return $display;
        }
    }

    public function showUserManager(): string
    {
        global $ORconn, $lang;
        $misc = $this->newMisc();
        $login = $this->newLogin();
        // Verify User is an Admin
        $security = $login->verifyPriv('edit_all_users');
        $display = '';
        //Load the Core Template

        $page = $this->newPageAdmin();
        $page->loadPage($this->config['admin_template_path'] . '/user_manager.html');


        if ($security) {
            if (isset($_POST['filter']) || isset($_POST['lookup_field']) || isset($_POST['lookup_value'])) {
                if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfToken($_POST['token'])) {
                    $display .= '<div class="text-danger text-center">' . $lang['invalid_csrf_token'] . '</div>';
                    unset($_POST);
                    return $display;
                }
            }
            $options = [];
            $options[""] = $lang['user_manager_show_all'];
            $options["agents"] = $lang['user_manager_agents'];
            $options["admins"] = $lang['user_manager_admins'];
            $options["members"] = $lang['user_manager_members'];

            $default_option = "";
            if (isset($_POST['filter']) && is_string($_POST['filter'])) {
                $default_option = $_POST['filter'];
            }


            $html = $page->getTemplateSection('user_manager_show_filter_block');
            $html = $page->formOptions($options, $default_option, $html);
            $page->replaceTemplateSection('user_manager_show_filter_block', $html);


            $filter_sql = '';
            if (isset($_POST['filter'])) {
                $filter = $_POST['filter'];
                $_GET['cur_page'] = 0;
                $_SESSION['um_filter'] = $_POST['filter'];
            } else {
                $filter = $_SESSION['um_filter'] ?? null;
            }
            if ($filter == 'agents') {
                $filter_sql = " WHERE userdb_is_agent = 'yes'";
            } elseif ($filter == 'members') {
                $filter_sql = " WHERE userdb_is_agent = 'no' AND userdb_is_admin = 'no'";
            } elseif ($filter == 'admins') {
                $filter_sql = " WHERE userdb_is_admin = 'yes'";
            }

            $security2 = $login->verifyPriv('Admin');
            if (!$security2) {
                if ($filter === 'Show All' || $filter === '') {
                    $filter_sql = " WHERE userdb_is_admin = 'no'";
                }
            }
            if (isset($_POST['lookup_field']) && is_string($_POST['lookup_field']) && isset($_POST['lookup_value']) && is_string($_POST['lookup_value']) && $_POST['lookup_value'] != '') {
                $lookup_value = $misc->makeDbSafe($_POST['lookup_value']);
                $lookup_field = $ORconn->addQ($_POST['lookup_field']);
                $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . 'userdb WHERE ' . $lookup_field . ' = ' . $lookup_value;
            } else {
                $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . "userdb $filter_sql ORDER BY userdb_id ";
            }
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $num_rows = $recordSet->RecordCount();

            $max_pages = ceil($num_rows / $this->config['listings_per_page']);

            if (!isset($_GET['cur_page']) || $_GET['cur_page'] == 0) {
                $_GET['cur_page'] = 0;
            } else {
                if (intval($_GET['cur_page']) + 1 > $max_pages || intval($_GET['cur_page']) < 0) {
                    header('HTTP/1.0 403 Forbidden');
                    $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
                    return $display;
                }
            }

            $next_prev = $misc->nextPrev($num_rows, intval($_GET['cur_page']), '', true);
            $page->replaceTag('next_prev', $next_prev);

            // build the string to select a certain number of users per page
            $limit_str = intval($_GET['cur_page']) * $this->config['listings_per_page'];
            $recordSet = $ORconn->SelectLimit($sql, $this->config['listings_per_page'], $limit_str);
            $count = 0;
            //Get Template Setion
            $user_template = $page->getTemplateSection('user_dataset');
            $user_section = '';
            // $display .= "<br /><br />";
            while (!$recordSet->EOF) {
                // alternate the colors
                if ($count == 0) {
                    $count = $count + 1;
                } else {
                    $count = 0;
                }
                $user_section .= $user_template;
                // strip slashes so input appears correctly
                $user_id = (int)$recordSet->fields('userdb_id');
                $edit_active = $recordSet->fields('userdb_active');
                $edit_isAgent = $recordSet->fields('userdb_is_agent');
                $edit_isAdmin = $recordSet->fields('userdb_is_admin');

                $edit_last_modified = date($this->config['date_format_timestamp'], strtotime($recordSet->fields('userdb_last_modified')));
                $edit_creation = date($this->config['date_format_timestamp'], strtotime($recordSet->fields('userdb_creation_date')));

                // Determine user type
                if ($edit_isAgent == 'yes') {
                    $user_type = $lang['user_manager_agent'];
                } elseif ($edit_isAdmin == 'yes') {
                    $user_type = $lang['user_manager_admin'];
                } else {
                    $user_type = $lang['user_manager_member'];
                }

                if ($edit_active == 'yes') {
                    $user_make_active = 'make_inactive';
                } else {
                    $user_make_active = 'make_active';
                }

                $user_section = $page->replaceUserFieldTags($user_id, $user_section, 'user');
                $user_section = $page->parseTemplateSection($user_section, 'user_type', $user_type);
                $user_section = $page->parseTemplateSection($user_section, 'is_agent', $edit_isAgent);

                $user_section = $page->parseTemplateSection($user_section, 'user_last_modified', $edit_last_modified);
                $user_section = $page->parseTemplateSection($user_section, 'user_creation_date', $edit_creation);

                $user_section = $page->parseTemplateSection($user_section, 'user_status', $edit_active);
                $user_section = $page->parseTemplateSection($user_section, 'user_make_active', $user_make_active);

                $recordSet->MoveNext();
            } // end while
            $page->replaceTemplateSection('user_dataset', $user_section);
        }

        $page->replaceTag('application_status_text', '');
        $page->replaceLangTemplateTags();
        $page->replacePermissionTags();
        $page->autoReplaceTags('', true);
        return $page->returnPage();
    }

    public function changeUserStatus(int $user_id, string $active_status): string
    {
        global $lang;
        $misc = $this->newMisc();

        // no touching the admin
        if ($user_id == 1) {
            header('Content-type: application/json');
            return json_encode(['error' => '2', 'error_msg' => $lang['admin_status_warning']]) ?: '';
        }

        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_all_users');

        if ($security) {
            //based on the active status, set some vars
            if ($active_status == 'yes') {
                $active_state = true;
                $lang_var = $lang['alert_user_active'];
            } else {
                $active_state = false;
                $lang_var = $lang['alert_user_inactive'];
            }

            //is this an agent?
            $agent_check = $misc->getAgentStatus($user_id);

            //what was the previous active status?
            $prev_status = $misc->getActiveStatus($user_id);

            //is this user the admin?
            $is_admin = $misc->getAdminStatus($user_id);

            // set user status to $active_state
            $user_api = $this->newUserApi();
            try {
                $user_api->update([
                    'user_id' => $user_id,
                    'user_details' => [
                        'active' => $active_state,
                    ],
                ]);
            } catch (Exception $e) {
                $misc->logErrorAndDie($e->getMessage());
            }

            if ($agent_check) {
                //get a list of this agent's listing ID#s
                $listing_api = $this->newListingApi();
                try {
                    $result = $listing_api->search([
                        'parameters' => [
                            'user_ID' => $user_id,
                            'listingsdb_active' => 'any',
                        ],
                    ]);
                } catch (Exception $e) {
                    $misc->logErrorAndDie($e->getMessage());
                }


                //only do this if there are listings
                if ($result['listing_count'] > 0) {
                    //set each listing in the array to $active_state T/F
                    foreach ($result['listings'] as $listing_id) {
                        //get pclass of the agent's listings
                        try {
                            $api_class = $listing_api->read([
                                'listing_id' => $listing_id,
                                'fields' => ['listingsdb_pclass_id'],
                            ]);
                        } catch (Exception $e) {
                            $misc->logErrorAndDie($e->getMessage());
                        }
                        try {
                            $listing_api->update([
                                'class_id' => is_int($api_class['listing']['listingsdb_pclass_id']) ? $api_class['listing']['listingsdb_pclass_id'] : 0,
                                'listing_id' => $listing_id,
                                'listing_details' => [
                                    'active' => $active_state,
                                ],
                                'listing_agents' => [
                                    $user_id,
                                ],
                                'disable_logs' => false,
                            ]);
                        } catch (Exception $e) {
                            $misc->logErrorAndDie($e->getMessage());
                        }
                    }
                } // end if ($result['listing_count'] > 0)
            } // end if ($agent_check)

            //if this account was inactive and is now active
            if ($prev_status === false && $active_state === true) {
                //if agent moderation is active, and this user is an agent, or member moderation is active and this user is not an Agent.
                //these conditions seem kind of broad, and should probably be reconsidered.
                if (($this->config['moderate_agents'] && $agent_check === true) || ($this->config['moderate_members'] && $agent_check === false)) {
                    // if the site admin should be notified when a new User is added
                    if ($agent_check === true || $is_admin === true) {
                        $this->sendUserVerificationEmail($user_id, 'agent');
                    } else {
                        $this->sendUserVerificationEmail($user_id, 'member');
                    }
                }
            }
            header('Content-type: application/json');
            return json_encode(['error' => '0', 'status_msg' => $user_id . ' ' . $lang_var]) ?: '';
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => $lang['user_manager_permission_denied']]) ?: '';
        }
    }


    /**
     * Add Lead lookup user
     *
     * @param 'yes'|'no' $is_agent (yes/no)
     * @param string     $term     (user serach term)
     *
     * @return array<int<0, max>, array{label: non-empty-string, value: int}> $results  (id, name, email)
     * consolidated from prior ajax_addlead_lookup_member() and ajax_addlead_lookup_agent() functions
     */
    public function ajaxAddLeadLookupUser(string $is_agent = 'no', string $term = ''): array
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql_term = $ORconn->addQ($term);
        $sql = 'SELECT userdb_user_last_name,userdb_user_first_name, userdb_emailaddress,userdb_id
				FROM ' . $this->config['table_prefix'] . 'userdb
				WHERE userdb_is_admin = \'no\' 
				AND userdb_is_agent = \'' . $is_agent . '\' 
				AND ( userdb_user_last_name LIKE \'' . $sql_term . '%\' 
					OR userdb_user_first_name LIKE \'' . $sql_term . '%\' 
					OR userdb_emailaddress LIKE \'' . $sql_term . '%\'
				)';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        // get main listings data
        $results = [];
        $x = 0;
        while (!$recordSet->EOF) {
            $results[$x]['value'] = (int)$recordSet->fields('userdb_id');
            $results[$x]['label'] = $recordSet->fields('userdb_user_last_name') . ', ' . $recordSet->fields('userdb_user_first_name') . ' (' . $recordSet->fields('userdb_emailaddress') . ')';
            $x++;
            $recordSet->MoveNext();
        } // end while
        return $results;
    }

    public function generatePassword(int $length = 8): string
    {
        $password = '';

        // define possible characters
        $possible = '0123456789bcdfghjkmnpqrstvwxyzaeiuo$!@%AEIOUBCDEFGHJKLMNPQRSTVWXYZ';

        // set up a counter
        $i = 0;

        // add random characters to $password until $length is reached
        while ($i < $length) {
            // pick a random character from the possible ones
            $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);

            // we don't want this character if it's already in the password
            if (!strstr($password, $char)) {
                $password .= $char;
                $i++;
            }
        }
        // done!
        return $password;
    }
}
