<?php

declare(strict_types=1);

namespace OpenRealty;

class MembersFavorites extends BaseClass
{
    public function deleteFavorites(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $display = '';
        $security = $login->loginCheck('Member');
        if ($security === true) {
            if (!isset($_GET['listingID']) || !is_numeric($_GET['listingID'])) {
                $display .= '<a href="' . $this->config['baseurl'] . '/index.php">' . $lang['perhaps_you_were_looking_something_else'] . '</a>';
            } elseif ($_GET['listingID'] == 0) {
                $display .= '<a href="' . $this->config['baseurl'] . '/index.php">' . $lang['perhaps_you_were_looking_something_else'] . '</a>';
            } else {
                $userID = $misc->makeDbSafe($_SESSION['userID']);
                $listingID = $misc->makeDbSafe($_GET['listingID']);
                $sql = 'DELETE FROM ' . $this->config['table_prefix'] . "userfavoritelistings WHERE userdb_id = $userID AND listingsdb_id = $listingID";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $display .= '<div class="deletedfromfavorite">' . $lang['listing_deleted_from_favorites'] . '</div>';
                $display .= $this->viewFavorites();
            }
            return $display;
        }
        return $security;
    }

    public function addToFavorites(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->loginCheck('Member');
        if ($security === true) {
            $display = '';
            if ($_GET['listingID'] == '') {
                $display .= '<a href="' . $this->config['baseurl'] . '/index.php">' . $lang['perhaps_you_were_looking_something_else'] . '</a>';
            } else {
                $userID = intval($_SESSION['userID']);
                $listingID = intval($_GET['listingID']);
                $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . "userfavoritelistings 
						WHERE userdb_id = $userID 
						AND listingsdb_id = $listingID";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $num_columns = $recordSet->RecordCount();
                if ($num_columns == 0) {
                    $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "userfavoritelistings (userdb_id, listingsdb_id) 
							VALUES ($userID, $listingID)";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    $display .= '<div class="addedtofavorite">' . $lang['listing_added_to_favorites'] . '</div>';
                } else {
                    $display .= '<div class="alreadyaddedtofavorite">' . $lang['listing_already_in_favorites'] . '</div>';
                }
            }
            $listing_pages = $this->newListingPages();
            $display .= $listing_pages->listingView();
            return $display;
        }
        return $security;
    }

    public function viewFavorites(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->loginCheck('Member');
        if ($security === true) {
            $display = '<h3>' . $lang['favorite_listings'] . '</h3>';
            $userID = intval($_SESSION['userID']);
            $sql = 'SELECT listingsdb_id 
					FROM ' . $this->config['table_prefix'] . "userfavoritelistings 
					WHERE userdb_id = $userID";
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $num_columns = $recordSet->RecordCount();
            if ($num_columns == 0) {
                $display .= $lang['no_listing_in_favorites'] . '<br /><br />';
            } else {
                $recordNum = 0;
                $listings = '';
                while (!$recordSet->EOF) {
                    if ($recordNum == 0) {
                        $listings .= $recordSet->fields('listingsdb_id');
                    } else {
                        $listings .= ',' . $recordSet->fields('listingsdb_id');
                    }
                    $recordNum++;
                    $recordSet->MoveNext();
                }
                $_GET['listing_id'] = $listings;

                $search = $this->newSearchPage();
                $display .= $search->searchResults();
            } // End else
            return $display;
        }
        return $security;
    }
}
