<?php

declare(strict_types=1);

namespace OpenRealty;

use Abraham\TwitterOAuth\TwitterOAuth;
use Abraham\TwitterOAuth\TwitterOAuthException;

class Social extends BaseClass
{
    //Send data to twitter message can be no longer then 140 chars
    public function twitterCallback(): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        /* If the oauth_token is old redirect to the connect page. */
        if (isset($_REQUEST['oauth_token']) && isset($_SESSION['oauth_token']) && $_SESSION['oauth_token'] !== $_REQUEST['oauth_token']) {
            //  Old Session Go Back to Site Config.'
            return 'Expired oAuth Token';
        }

        /* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
        $ORconnection = new TwitterOAuth($this->config['twitter_consumer_key'], $this->config['twitter_consumer_secret'], $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

        /* Request access tokens from twitter */

        //$access_token = $ORconnection->getAccessToken($_REQUEST['oauth_verifier']);
        try {
            $access_token = $ORconnection->oauth('oauth/access_token', ['oauth_verifier' => $_REQUEST['oauth_verifier']]);
        } catch (TwitterOAuthException $e) {
            return $e->getMessage();
        }
        /* Save the access tokens. Normally these would be saved in a database for future use. */
        $sql = 'UPDATE ' . $this->config['table_prefix_no_lang'] . 'controlpanel SET controlpanel_twitter_auth = ' . $misc->makeDbSafe(serialize($access_token));
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        /* Remove no longer needed request tokens */
        unset($_SESSION['oauth_token']);
        unset($_SESSION['oauth_token_secret']);
        return 'Twitter Connected';
    }

    public function twitterDisconnect(): string
    {
        global $ORconn;
        $misc = $this->newMisc();
        $sql = 'UPDATE ' . $this->config['table_prefix_no_lang'] . 'controlpanel SET controlpanel_twitter_auth = \'\'';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        return 'Twitter Disconnected';
    }
}
