<?php

declare(strict_types=1);

namespace OpenRealty;

class VtourHandler extends MediaHandler
{
    public function ajaxDisplayListingVtours(int $listing_id): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $listing_pages = $this->newListingPages();

        $login = $this->newLogin();
        $status_text = '';
        //Get Listing owner
        $listing_agent_id = $listing_pages->getListingAgentValue('userdb_id', $listing_id);
        //Make sure we can Edit this lisitng
        $has_permission = true;
        if ($_SESSION['userID'] != $listing_agent_id) {
            $security = $login->verifyPriv('edit_all_listings');
            if (!$security) {
                $has_permission = false;
            }
        }
        if ($has_permission) {
            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/listing_editor_vtour_display.html');
            //Load Listing Images
            $sql = 'SELECT listingsvtours_id, listingsvtours_caption, listingsvtours_file_name, listingsvtours_thumb_file_name FROM ' . $this->config['table_prefix'] . "listingsvtours WHERE (listingsdb_id = $listing_id) ORDER BY listingsvtours_rank";
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $page->page = str_replace('{listing_id}', (string)$listing_id, $page->page);
            $html = $page->getTemplateSection('vtour_block');
            $new_html = '';
            $num_images = $recordSet->RecordCount();
            while (!$recordSet->EOF) {
                $new_html .= $html;
                $caption = $recordSet->fields('listingsvtours_caption');
                $thumb_file_name = $recordSet->fields('listingsvtours_thumb_file_name');
                $file_name = $recordSet->fields('listingsvtours_file_name');
                $vtour_id = $recordSet->fields('listingsvtours_id');
                // gotta grab the image size
                $ext = substr(strrchr($file_name, '.'), 1);
                $new_html = str_replace('{vtour_caption}', htmlentities($caption, ENT_COMPAT, $this->config['charset']), $new_html);
                $new_html = str_replace('{vtour_id}', $vtour_id, $new_html);
                if ($ext == 'jpg' || $ext == 'jpeg') {
                    // gotta grab the image size
                    //echo '<br />'."$this->config[vtour_upload_path]/$thumb_file_name".'<br />';
                    $imagedata = GetImageSize($this->config['vtour_upload_path'] . "/$thumb_file_name");
                    $imagewidth = $imagedata[0];
                    $imageheight = $imagedata[1];
                    $shrinkage = $this->config['thumbnail_width'] / $imagewidth;
                    $displaywidth = $imagewidth * $shrinkage;
                    $displayheight = $imageheight * $shrinkage;
                    $new_html = str_replace('{vtour_thumb_src}', $this->config['vtour_view_images_path'] . "/$thumb_file_name", $new_html);
                    $new_html = str_replace('{vtour_height}', (string)$displayheight, $new_html);
                    $new_html = str_replace('{vtour_width}', (string)$displaywidth, $new_html);
                    $new_html = $page->removeTemplateBlock('vtour_unsupported', $new_html);
                    $new_html = $page->cleanupTemplateBlock('!vtour_unsupported', $new_html);
                } else {
                    $new_html = $page->removeTemplateBlock('!vtour_unsupported', $new_html);
                    $new_html = $page->cleanupTemplateBlock('vtour_unsupported', $new_html);
                }

                $recordSet->MoveNext();
            } // end while
            $page->replaceTemplateSection('vtour_block', $new_html);
            $avaliable_images = $this->config['max_vtour_uploads'] - $num_images;
            if ($avaliable_images > 0) {
                $page->page = $page->cleanupTemplateBlock('vtour_upload', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('vtour_upload', $page->page);
            }
            //End Listing Images
            //Finish Loading Template
            $page->replaceTag('application_status_text', $status_text);
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        } else {
            return 'Permission Denied';
        }
    }

    public function showVtour(int $listingID, bool $popup = true): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $display = '';
        if (isset($_GET['listingID'])) {
            if ($_GET['listingID'] != '') {
                $page = $this->newPageUser();
                $page->loadPage($this->config['template_path'] . '/' . $this->config['vtour_template']);
                $page->replaceListingFieldTags($listingID);

                $sql = 'SELECT listingsvtours_caption, listingsvtours_description, listingsvtours_file_name, listingsvtours_rank 
						FROM ' . $this->config['table_prefix'] . "listingsvtours 
						WHERE (listingsdb_id = $listingID) 
						ORDER BY listingsvtours_rank";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $vtour_iamges = [];
                $a = 0;

                while (!$recordSet->EOF) {
                    $caption = $recordSet->fields('listingsvtours_caption');
                    $description = $recordSet->fields('listingsvtours_description');
                    $file_name = $recordSet->fields('listingsvtours_file_name');
                    $url = $this->config['vtour_view_images_path'] . '/' . $file_name;
                    if ($caption == '') {
                        $caption = 'Virtual Tour Image ' . $a;
                    }
                    $vtour_iamges[$a] = [
                        'title' => $caption,
                        'url' => $url,
                        'description' => $description,
                    ];
                    $a++;
                    $recordSet->MoveNext();
                } // end while


                $html = $page->getTemplateSection('vtour_block');
                $html_replace = '';
                foreach ($vtour_iamges as $options) {
                    //Yes Option
                    $html_replace .= $html;
                    $html_replace = str_replace('{url}', $options['url'], $html_replace);
                    $html_replace = str_replace('{title}', $options['title'], $html_replace);
                    $html_replace = str_replace('{description}', $options['description'], $html_replace);
                }
                $page->replaceTemplateSection('vtour_block', $html_replace);

                if (!$popup) {
                    $page->page = $page->removeTemplateBlock('vtour_header', $page->page);
                    $page->page = $page->removeTemplateBlock('vtour_footer', $page->page);
                    $page->page = $page->removeTemplateBlock('vtour_content', $page->page);
                } else {
                    $page->page = $page->cleanupTemplateBlock('vtour_header', $page->page);
                    $page->page = $page->cleanupTemplateBlock('vtour_footer', $page->page);
                    $page->page = $page->cleanupTemplateBlock('vtour_content', $page->page);
                }
                $page->page = str_replace('{template_url}', $this->config['template_url'], $page->page);
                $display = $page->returnPage();
            } else {
                $display .= "<a href=\"index.php\">$lang[perhaps_you_were_looking_something_else]</a>";
            }
        } else {
            $display .= "<a href=\"index.php\">$lang[perhaps_you_were_looking_something_else]</a>";
        }
        return $display;
    }

    public function rendervtourlink(int $listingID, bool $use_small_image = false): string
    {
        // shows the images connected to a given image
        global $lang, $ORconn;
        $misc = $this->newMisc();
        // grab the images
        $output = '';
        $sql = 'SELECT listingsvtours_file_name 
				FROM ' . $this->config['table_prefix'] . "listingsvtours 
				WHERE (listingsdb_id = $listingID) 
				ORDER BY listingsvtours_rank";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $num_images = $recordSet->RecordCount();
        if ($num_images > 0) {
            $supported = false;
            while (!$recordSet->EOF) {
                $file_name = $recordSet->fields('listingsvtours_file_name');
                $ext = substr(strrchr($file_name, '.'), 1);
                if ($ext == 'jpg') {
                    $supported = true;
                }
                $recordSet->MoveNext();
            } // end while
            if ($supported) { // if it's a supported VTour then display the link button
                if ($use_small_image === true) {
                    $image = 'vtourbuttonsmall.jpg';
                } else {
                    $image = 'vtourbutton.jpg';
                }
                if (file_exists($this->config['template_path'] . '/images/' . $image)) {
                    $output .= '<a href="index.php?action=show_vtour&amp;popup=blank&amp;listingID=' . $listingID . '" onclick="window.open(\'index.php?action=show_vtour&amp;popup=blank&amp;listingID=' . $listingID . '\',\'\',\'width=' . $this->config['vt_popup_width'] . ',height=' . $this->config['vt_popup_height'] . '\');return false;"><img src="' . $this->config['template_url'] . '/images/' . $image . '" alt="' . $lang['click_here_for_vtour'] . '" /></a>';
                } else {
                    $output = '<a href="index.php?action=show_vtour&amp;popup=blank&amp;listingID=' . $listingID . '" onclick="window.open(\'index.php?action=show_vtour&amp;popup=blank&amp;listingID=' . $listingID . '\',\'\',\'width=' . $this->config['vt_popup_width'] . ',height=' . $this->config['vt_popup_height'] . '\');return false;">' . $lang['click_here_for_vtour'] . '</a>';
                }
            } //end if it's a supported VTour
        } // end if ($num_images > 0)
        return $output;
    }
}
