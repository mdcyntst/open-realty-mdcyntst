<?php

/**
 * jQuery File Tree PHP Connector
 *
 * Version 1.1.0
 *
 * @author - Cory S.N. LaViska A Beautiful Site (http://abeautifulsite.net/)
 * @author - Dave Rogers - https://github.com/daverogers/jQueryFileTree
 *
 * History:
 *
 * 1.1.1 - SECURITY: forcing root to prevent users from determining system's file structure (per DaveBrad)
 * 1.1.0 - adding multiSelect (checkbox) support (08/22/2014)
 * 1.0.2 - fixes undefined 'dir' error - by itsyash (06/09/2014)
 * 1.0.1 - updated to work with foreign characters in directory/file names (12 April 2008)
 * 1.0.0 - released (24 March 2008)
 *
 * Output a list of files for jQuery File Tree
 */

declare(strict_types=1);

use OpenRealty\Api\Commands\SettingsApi;
use OpenRealty\Api\Database;

/** @psalm-suppress InvalidGlobal */
$apiDB = new Database();
$dbh = $apiDB->getDatabaseHandler();
$settingApi = new SettingsApi($dbh);
$config = $settingApi->read(['is_mobile' => false])['config'];

$login = new OpenRealty\Login($dbh, $config);
$login_status = $login->loginCheck('Agent');
if ($login_status !== true) {
    exit();
}
if (!array_key_exists('HTTP_REFERER', $_SERVER)) {
    exit('No direct script access allowed');
}


/**
 * filesystem root - USER needs to set this!
 * -> prevents debug users from exploring system's directory structure
 * ex: $root = $_SERVER['DOCUMENT_ROOT'];
 */
//$root = null;
$root = $_SESSION['filemanager_basepath'] . $_SESSION['filemanager_pathpart'];
if (!$root) {
    exit("ERROR: Root filesystem directory not set in jqueryFileTree.php");
}
// phpcs:ignore
$postDir = ((isset($_POST['dir']) && is_string($_POST['dir']) && realpath($_POST['dir']) !== false && realpath($_POST['dir']) !== '/') ? realpath($_POST['dir']) : $root);
if (!str_starts_with($postDir, $root)) {
    exit("ERROR: Root filesystem directory cannot be traversed");
}
// set checkbox if multiSelect set to true
$checkbox = (isset($_POST['multiSelect']) && $_POST['multiSelect'] == 'true') ? "<input type='checkbox' />" : null;
$onlyFolders = isset($_POST['onlyFolders']) && $_POST['onlyFolders'] == 'true';
$onlyFiles = isset($_POST['onlyFiles']) && $_POST['onlyFiles'] == 'true';

if (file_exists($postDir)) {
    $files = scandir($postDir);
    $returnDir = substr($postDir, strlen($root));

    natcasesort($files);

    if (count($files) > 2) { // The 2 accounts for . and ..
        echo "<ul class='jqueryFileTree'>";

        foreach ($files as $file) {
            $htmlRel = htmlentities($postDir . $file, ENT_QUOTES);
            $htmlName = htmlentities($file);
            $ext = preg_replace('/^.*\./', '', $file);

            if (file_exists($postDir . "/" . $file) && $file != '.' && $file != '..') {
                if (is_dir($postDir . "/" . $file) && (!$onlyFiles || $onlyFolders)) {
                    echo "<li class='directory collapsed'>$checkbox<a rel='" . $htmlRel . "/'>" . $htmlName . "</a></li>";
                } elseif (!$onlyFolders || $onlyFiles) {
                    echo "<li class='file ext_$ext'>$checkbox<a rel='" . $htmlRel . "'>" . $htmlName . "</a></li>";
                }
            }
        }

        echo "</ul>";
    }
}
