<?php

declare(strict_types=1);

namespace OpenRealty;

class PageFunctions extends BaseClass
{
    public function ajaxGetPages(): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $sql = 'SELECT pagesmain_id, pagesmain_title 
				FROM ' . $this->config['table_prefix'] . 'pagesmain';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $pages = [];
        while (!$recordSet->EOF) {
            $pages[$recordSet->fields('pagesmain_id')] = $recordSet->fields('pagesmain_title');
            $recordSet->Movenext();
        }
        return json_encode(['error' => false, 'pages' => $pages]) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxUpdatePagePostAutosave(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('editpages');
        if ($security === true) {
            // Do we need to save?
            if (
                isset($_POST['ta'], $_POST['pageID'])
                && is_string($_POST['ta'])
                && is_numeric($_POST['pageID'])
            ) {
                if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
                }
                // Save page now
                $pageID = intval($_POST['pageID']);
                $save_full = $ORconn->qstr($_POST['ta']);

                $sql = 'UPDATE ' . $this->config['table_prefix'] . "pagesmain 
						SET pagesmain_full_autosave = " . $save_full . " 
						WHERE pagesmain_id = " . $pageID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                } else {
                    header('Content-type: application/json');
                    return json_encode(['error' => '0', 'page_id' => $pageID]) ?: '';
                }
            }
        }
        return json_encode(['error' => true, 'error_msg' => 'permission_denied']) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxGetPagePost(): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('editpages');
        if ($security === true) {
            if (isset($_POST['pageID'])) {
                // Save page now
                $pageID = intval($_POST['pageID']);

                $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . "pagesmain 
						WHERE pagesmain_id = $pageID";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                if ($recordSet->RecordCount() == 0) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => 'Page Not Found']) ?: '';
                }
                $id = $recordSet->fields('pagesmain_id');
                $title = $recordSet->fields('pagesmain_title');
                $date = $recordSet->fields('pagesmain_date');
                $full = $recordSet->fields('pagesmain_full');
                $published = $recordSet->fields('pagesmain_published');
                $description = $recordSet->fields('pagesmain_description');
                $keywords = $recordSet->fields('pagesmain_keywords');
                $full_autosave = $recordSet->fields('pagesmain_full_autosave');
                $seotitle = $recordSet->fields('page_seotitle');
                header('Content-type: application/json');

                $full = str_replace('{template_url}', $this->config['template_url'], $full);
                $full = str_replace('{baseurl}', $this->config['baseurl'], $full);

                return json_encode([
                    'error' => '0',
                    'id' => $id,
                    'title' => $title,
                    'date' => $date,
                    'full' => $full,
                    'published' => $published,
                    'description' => $description,
                    'keywords' => $keywords,
                    'full_autosave' => $full_autosave,
                    'seotitle' => $seotitle,
                ]);
            }
        }
        return json_encode(['error' => true, 'error_msg' => 'permission_denied']) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxUpdatePagePost(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('editpages');
        if ($security === true) {
            $page = $this->newPageAdmin();
            // Do we need to save?
            if (
                isset($_POST['ta'], $_POST['pageID'], $_POST['description'], $_POST['title'], $_POST['keywords'], $_POST['seotitle'])
                && is_string($_POST['ta'])
                && is_string($_POST['description'])
                && is_string($_POST['title'])
                && is_string($_POST['keywords'])
                && is_numeric($_POST['pageID'])
                && is_string($_POST['seotitle'])
            ) {
                if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
                }
                // Save page now
                $pageID = intval($_POST['pageID']);
                $save_full_xhtml = $_POST['ta'];
                //Replace Paths with template tags
                $save_full_xhtml = str_replace($this->config['template_url'], '{template_url}', $save_full_xhtml);
                $save_full_xhtml = str_replace($this->config['baseurl'], '{baseurl}', $save_full_xhtml);
                $save_full_xhtml = $ORconn->qstr($save_full_xhtml);
                $save_description = $misc->makeDbSafe($_POST['description']);
                $title = trim($_POST['title']);
                if ($title == '') {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['page_title_not_blank']]) ?: '';
                }
                $save_title = $misc->makeDbSafe($title);
                //Make sure Blog Title is unique.
                $sql = 'SELECT pagesmain_id FROM ' . $this->config['table_prefix'] . "pagesmain 
						WHERE STRCMP(pagesmain_title,$save_title) = 0 
						AND pagesmain_id <> $pageID";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                if ($recordSet->RecordCount() > 0) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['page_title_not_unique']]) ?: '';
                }
                $save_keywords = $misc->makeDbSafe($_POST['keywords']);

                $seotitle = trim($_POST['seotitle']);
                if ($seotitle == '') {
                    $seotitle = $page->createSeoUri($_POST['title'], false);
                }
                $sql_seotitle = $misc->makeDbSafe($seotitle);
                //Verify the SEO title is unique
                $sql = 'SELECT pagesmain_id FROM ' . $this->config['table_prefix'] . 'pagesmain 
						WHERE page_seotitle = ' . $sql_seotitle . ' 
						AND pagesmain_id <> ' . $pageID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                if ($recordSet->RecordCount() > 0) {
                    $seotitle = $page->createSeoUri($seotitle . '-' . $pageID, false);
                    $sql_seotitle = $misc->makeDbSafe($seotitle);
                }
                if (isset($_POST['status'])) {
                    $save_status = intval($_POST['status']);
                } else {
                    $save_status = null;
                }

                $status_sql = '';
                if ($save_status !== null) {
                    $status_sql = ', pagesmain_published = ' . $save_status;
                }
                $sql = 'UPDATE ' . $this->config['table_prefix'] . 'pagesmain 
						SET page_seotitle = ' . $sql_seotitle . ", pagesmain_full = " . $save_full_xhtml . ", pagesmain_title = " . $save_title . ', pagesmain_description = ' . $save_description . ', pagesmain_keywords = ' . $save_keywords . $status_sql . ' 
						WHERE pagesmain_id = ' . $pageID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                } else {
                    header('Content-type: application/json');
                    return json_encode(['error' => '0', 'page_id' => $pageID, 'seotitle' => $seotitle]) ?: '';
                }
            }
        }
        return json_encode(['error' => true, 'error_msg' => 'permission_denied']) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxDeletePagePost(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('editpages');
        if ($security === true) {
            if (isset($_POST['pageID'])) {
                // Save page now
                $pageID = intval($_POST['pageID']);
                //Don't allow deletion of Page 1 as it is the index page
                if ($pageID == 1) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['page_1_can_not_delete']]) ?: '';
                }
                $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'pagesmain  
						WHERE pagesmain_id = ' . $pageID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                } else {
                    header('Content-type: application/json');
                    return json_encode(['error' => '0', 'page_id' => $pageID]) ?: '';
                }
            }
        }
        return json_encode(['error' => true, 'error_msg' => 'permission_denied']) ?: '';
    }

    /**
     * @return string
     */
    public function ajaxCreatePagePost(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('editpages');
        if ($security === true) {
            $page = $this->newPageAdmin();
            // Do we need to save?
            if (isset($_POST['title']) && is_string($_POST['title'])) {
                if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
                }
                // Save page now
                $title = trim($_POST['title']);
                $save_title = $misc->makeDbSafe($title);
                //Make sure page Title is unique.
                $sql = 'SELECT pagesmain_id FROM ' . $this->config['table_prefix'] . "pagesmain 
						WHERE STRCMP(pagesmain_title,$save_title) = 0";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                if ($recordSet->RecordCount() > 0) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => "$lang[page_title_not_unique]"]) ?: '';
                }
                //Generate seo URL
                $seotitle = $page->createSeoUri($title, false);
                $sql_seotitle = $misc->makeDbSafe($seotitle);

                $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "pagesmain
				(pagesmain_full,pagesmain_title,pagesmain_date,pagesmain_published,pagesmain_description,pagesmain_keywords,page_seotitle)
				VALUES ('',$save_title," . strtotime('now') . ",0,'',''," . $sql_seotitle . ')';
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $page_id = $ORconn->Insert_ID();
                //Verify the SEO title is unique
                $sql = 'SELECT pagesmain_id FROM ' . $this->config['table_prefix'] . 'pagesmain 
						WHERE page_seotitle = ' . $sql_seotitle . ' 
						AND pagesmain_id <> ' . $page_id;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                if ($recordSet->RecordCount() > 0) {
                    $seotitle = $page->createSeoUri($title . '-' . $page_id, false);
                    $sql_seotitle = $misc->makeDbSafe($seotitle);
                    $sql = 'UPDATE ' . $this->config['table_prefix'] . "pagesmain 
							SET page_seotitle = $sql_seotitle 
							WHERE pagesmain_id = " . $page_id;
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                }
                header('Content-type: application/json');
                return json_encode(['error' => '0', 'id' => $page_id, 'title' => "$title"]) ?: '';
            }
        }
        return json_encode(['error' => true, 'error_msg' => 'permission_denied']) ?: '';
    }

    public function getPageUrl(int $page_id): string
    {
        $page = $this->newPageUser();
        //Get Title
        return $page->magicURIGenerator('page', (string)$page_id, true);
    }

    // TODO consolidate all these get_page_XXX functions below

    public function getPageTitle(int $page_id): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $sql = 'SELECT pagesmain_title 
				FROM ' . $this->config['table_prefix'] . 'pagesmain 
				WHERE pagesmain_id=' . $page_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        /** @var string $title */
        $title = $recordSet->fields('pagesmain_title');
        return $title;
    }

    public function getPageSeoTitle(int $page_id): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $sql = 'SELECT page_seotitle 
				FROM ' . $this->config['table_prefix'] . 'pagesmain 
				WHERE pagesmain_id=' . $page_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        /** @var string $title */
        $title = $recordSet->fields('page_seotitle');
        return $title;
    }
}
