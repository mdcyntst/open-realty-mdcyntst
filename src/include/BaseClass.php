<?php

declare(strict_types=1);

namespace OpenRealty;

use OpenRealty\Api\Commands\BlogApi;
use OpenRealty\Api\Commands\FieldsApi;
use OpenRealty\Api\Commands\ListingApi;
use OpenRealty\Api\Commands\LogApi;
use OpenRealty\Api\Commands\MediaApi;
use OpenRealty\Api\Commands\MenuApi;
use OpenRealty\Api\Commands\PClassApi;
use OpenRealty\Api\Commands\TwitterApi;
use OpenRealty\Api\Commands\UserApi;
use PDO;

abstract class BaseClass
{
    protected PDO $dbh;

    /** @var array{
     *          table_prefix_no_lang: string,
     *          version: string,
     *          basepath: string,
     *          baseurl: string,
     *          admin_name: string,
     *          admin_email: string,
     *          site_email: string,
     *          company_name: string,
     *          company_location: string,
     *          company_logo: string,
     *          automatic_update_check: boolean,
     *          url_style: integer,
     *          seo_default_keywords: string,
     *          seo_default_description: string,
     *          seo_listing_keywords: string,
     *          seo_listing_description: string,
     *          seo_default_title: string,
     *          seo_listing_title: string,
     *          seo_url_seperator: string,
     *          template: string,
     *          full_template: string,
     *          admin_template: string,
     *          listing_template: string,
     *          agent_template: string,
     *          template_listing_sections: string,
     *          search_result_template: string,
     *          vtour_template: string, lang: string,
     *          listings_per_page: integer,
     *          search_step_max: integer,
     *          max_search_results: integer,
     *          add_linefeeds: boolean,
     *          strip_html: boolean,
     *          allowed_html_tags: string,
     *          money_sign: string,
     *          show_no_photo: boolean,
     *          number_format_style: integer,
     *          number_decimals_number_fields: integer,
     *          number_decimals_price_fields: integer,
     *          money_format: integer,
     *          date_format: integer,
     *          date_to_timestamp: '%m/%d/%Y'|'%Y/%d/%m'|'%d/%m/%Y',
     *                  date_format_long: 'mm/dd/yyyy'|'yyyy/dd/mm'|'dd/mm/yyyy',
     *                  date_format_timestamp: 'm/d/Y'|'Y/d/m'|'d/m/Y',
     *                  max_listings_uploads: integer,
     *                  max_listings_upload_size: integer,
     *                  max_listings_upload_width: integer,
     *                  max_listings_upload_height: integer,
     *                  max_user_uploads: integer,
     *                  max_user_upload_size: integer,
     *                  max_user_upload_width: integer,
     *                  max_user_upload_height: integer,
     *                  max_vtour_uploads: integer,
     *                  max_vtour_upload_size: integer,
     *                  max_vtour_upload_width: integer,
     *                  allowed_upload_extensions: string,
     *                  make_thumbnail: boolean,
     *                  thumbnail_width: integer,
     *                  thumbnail_prog: string,
     *                  path_to_imagemagick: string,
     *                  resize_img: boolean,
     *                  jpeg_quality: integer,
     *                  use_expiration: boolean,
     *                  days_until_listings_expire: integer,
     *                  allow_member_signup: boolean,
     *                  allow_agent_signup: boolean,
     *                  agent_default_active: boolean,
     *                  agent_default_admin: boolean,
     *                  agent_default_feature: boolean,
     *                  agent_default_moderate: boolean,
     *                  agent_default_logview: boolean,
     *                  agent_default_edit_site_config: boolean,
     *                  agent_default_edit_member_template: boolean,
     *                  agent_default_edit_agent_template: boolean,
     *                  agent_default_edit_listing_template: boolean,
     *                  agent_default_canchangeexpirations: boolean,
     *                  agent_default_editpages: boolean,
     *                  agent_default_havevtours: boolean,
     *                  agent_default_num_listings: integer,
     *                  agent_default_num_featuredlistings: integer,
     *                  agent_default_can_export_listings: boolean,
     *                  agent_default_edit_all_users: boolean,
     *                  agent_default_edit_all_listings: boolean,
     *                  agent_default_edit_property_classes: boolean,
     *                  moderate_agents: boolean,
     *                  moderate_members: boolean,
     *                  moderate_listings: boolean,
     *                  export_listings: boolean,
     *                  email_notification_of_new_users: boolean,
     *                  email_information_to_new_users: boolean,
     *                  email_notification_of_new_listings: boolean,
     *                  configured_langs: string,
     *                  configured_show_count: boolean,
     *                  sortby: string,
     *                  sorttype: string,
     *                  special_sortby: string,
     *                  special_sorttype: string,
     *                  email_users_notification_of_new_listings: boolean,
     *                  num_featured_listings: integer,
     *                  map_type: string,
     *                  map_address: string,
     *                  map_address2: string,
     *                  map_address3: string,
     *                  map_address4: string,
     *                  map_city: string,
     *                  map_state: string,
     *                  map_zip: string,
     *                  map_country: string,
     *                  map_latitude: string,
     *                  map_longitude: string,
     *                  show_listedby_admin: boolean,
     *                  show_next_prev_listing_page: boolean,
     *                  show_notes_field: boolean,
     *                  vtour_width: integer,
     *                  vtour_height: integer,
     *                  vt_popup_width: integer,
     *                  vt_popup_height: integer,
     *                  zero_price: boolean,
     *                  vcard_phone: string,
     *                  vcard_fax: string,
     *                  vcard_mobile: string,
     *                  vcard_address: string,
     *                  vcard_city: string,
     *                  vcard_state: string,
     *                  vcard_zip: string,
     *                  vcard_country: string,
     *                  vcard_url: string,
     *                  vcard_notes: string,
     *                  demo_mode: boolean,
     *                  feature_list_separator: string,
     *                  search_list_separator: string,
     *                  rss_title_featured: string,
     *                  rss_desc_featured: string,
     *                  rss_listingdesc_featured: string,
     *                  rss_title_lastmodified: string,
     *                  rss_desc_lastmodified: string,
     *                  rss_listingdesc_lastmodified: string,
     *                  rss_limit_lastmodified: integer,
     *                  rss_title_latestlisting: string,
     *                  rss_desc_latestlisting: string,
     *                  rss_listingdesc_latestlisting: string,
     *                  rss_limit_latestlisting: integer,
     *                  resize_by: string,
     *                  resize_thumb_by: string,
     *                  thumbnail_height: integer,
     *                  charset: string,
     *                  wysiwyg_show_edit: boolean,
     *                  textarea_short_chars: integer,
     *                  main_image_display_by: string,
     *                  main_image_width: integer,
     *                  main_image_height: integer,
     *                  number_columns: integer,
     *                  rss_limit_featured: integer,
     *                  force_decimals: boolean,
     *                  max_listings_file_uploads: integer,
     *                  max_listings_file_upload_size: integer,
     *                  allowed_file_upload_extensions: string,
     *                  file_icon_width: integer,
     *                  file_icon_height: integer,
     *                  show_file_icon: boolean,
     *                  file_display_option: string,
     *                  file_display_size: boolean,
     *                  include_senders_ip: boolean,
     *                  agent_default_havefiles: boolean,
     *                  agent_default_haveuserfiles: boolean,
     *                  max_users_file_uploads: integer,
     *                  max_users_file_upload_size: integer,
     *                  disable_referrer_check: boolean,
     *                  price_field: string,
     *                  users_per_page: integer,
     *                  show_admin_on_agent_list: boolean,
     *                  use_signup_image_verification: boolean,
     *                  controlpanel_mbstring_enabled: boolean,
     *                  require_email_verification: boolean,
     *                  blog_requires_moderation: boolean,
     *                  maintenance_mode: boolean,
     *                  notify_listings_template: string,
     *                  twitter_consumer_secret: string,
     *                  twitter_consumer_key: string,
     *                  twitter_auth: string,
     *                  twitter_new_listings: boolean,
     *                  twitter_update_listings: boolean,
     *                  twitter_new_blog: boolean,
     *                  twitter_listing_photo: boolean,
     *                  pingback_services: string,
     *                  blogs_per_page: integer,
     *                  send_url_pingbacks: boolean,
     *                  send_service_pingbacks: boolean,
     *                  timezone: string,
     *                  default_page: string,
     *                  blog_pingback_urls: string,
     *                  banned_domains_signup: string,
     *                  banned_ips_signup: string,
     *                  banned_ips_site: string,
     *                  rss_title_blogposts: string,
     *                  rss_desc_blogposts: string,
     *                  rss_title_blogcomments: string,
     *                  rss_desc_blogcomments: string,
     *                  phpmailer: boolean,
     *                  mailserver: string,
     *                  mailport: integer,
     *                  mailuser: string,
     *                  mailpass: string,
     *                  agent_default_canManageAddons: boolean,
     *                  agent_default_blogUserType: integer,
     *                  agent_default_edit_all_leads: boolean,
     *                  agent_default_edit_lead_template: boolean,
     *                  enable_tracking: boolean,
     *                  enable_tracking_crawlers: boolean,
     *                  show_agent_no_photo: boolean,
     *                  template_lead_sections: string,
     *                  allow_template_change: boolean,
     *                  allow_language_change: boolean,
     *                  listingimages_slideshow_group_thumb: integer,
     *                  admin_listing_per_page: integer,
     *                  mobile_template: string,
     *                  captcha_system: string,
     *                  floor_agent: integer,
     *                  contact_template: string,
     *                  user_jpeg_quality: integer,
     *                  user_resize_img: boolean,
     *                  user_resize_by: string,
     *                  user_resize_thumb_by: string,
     *                  user_thumbnail_width: integer,
     *                  user_thumbnail_height: integer,
     *                  num_popular_listings: integer,
     *                  num_random_listings: integer,
     *                  num_latest_listings: integer,
     *                  google_client_id: string,
     *                  google_client_secret: string,
     *                  lang_table_prefix: string,
     *                  table_prefix: string,
     *                  path_to_thumbnailer: string,
     *                  template_path: string,
     *                  template_url: string,
     *                  admin_template_path: string,
     *                  admin_template_url: string,
     *                  listings_upload_path: string,
     *                  listings_view_images_path: string,
     *                  user_upload_path: string,
     *                  user_view_images_path: string,
     *                  vtour_upload_path: string,
     *                  vtour_view_images_path: string,
     *                  file_icons_path: string,
     *                  listings_view_file_icons_path: string,
     *                  listings_file_upload_path: string,
     *                  listings_view_file_path: string,
     *                  users_file_upload_path: string,
     *                  users_view_file_path: string,
     *                  allow_pingbacks: boolean,
     *                  uri_parts: array<
     *                  string, array{
     *                  'slug': string,
     *                  'uri': string
     *                  }
     *                  >
     *                  } $config
     */
    protected array $config;

    /**
     * Constructor loads settings.
     */
    public function __construct(PDO $dbh, array $config)
    {
        $this->dbh = $dbh;
        $this->config = $config;
    }

    /**
     * Returns Listing API Class
     * Needed to allow mocking of class
     *
     * @return Misc
     */
    public function newMisc(): Misc
    {
        return new Misc($this->dbh, $this->config);
    }

    public function newBlogApi(): BlogApi
    {
        return new BlogApi($this->dbh, $this->config);
    }

    public function newLogApi(): LogApi
    {
        return new LogApi($this->dbh, $this->config);
    }

    public function newMenuApi(): MenuApi
    {
        return new MenuApi($this->dbh, $this->config);
    }


    /**
     * Returns Listing API Class
     * Needed to allow mocking of class
     *
     * @return ListingApi
     */
    public function newListingApi(): ListingApi
    {
        return new ListingApi($this->dbh, $this->config);
    }

    /**
     * Returns PClass API Class
     * Needed to allow mocking of class
     *
     * @return PClassApi
     */
    public function newPClassApi(): PClassApi
    {
        return new PClassApi($this->dbh, $this->config);
    }


    public function newFieldsApi(): FieldsApi
    {
        return new FieldsApi($this->dbh, $this->config);
    }

    public function newMediaApi(): MediaApi
    {
        return new MediaApi($this->dbh, $this->config);
    }

    public function newUserApi(): UserApi
    {
        return new UserApi($this->dbh, $this->config);
    }

    public function newBlogFunctions(): BlogFunctions
    {
        return new BlogFunctions($this->dbh, $this->config);
    }

    public function newTwitterApi(): TwitterApi
    {
        return new TwitterApi($this->dbh, $this->config);
    }

    public function newUser(): User
    {
        return new User($this->dbh, $this->config);
    }

    public function newLogin(): Login
    {
        return new Login($this->dbh, $this->config);
    }

    public function newPageAdmin(): PageAdmin
    {
        return new PageAdmin($this->dbh, $this->config);
    }

    public function newPageUser(): PageUser
    {
        return new PageUser($this->dbh, $this->config);
    }

    public function newSearchPage(): SearchPage
    {
        return new SearchPage($this->dbh, $this->config);
    }

    public function newTemplateEditor(): TemplateEditor
    {
        return new TemplateEditor($this->dbh, $this->config);
    }

    public function newPageFunctions(): PageFunctions
    {
        return new PageFunctions($this->dbh, $this->config);
    }

    public function newConfigurator(): Configurator
    {
        return new Configurator($this->dbh, $this->config);
    }


    public function newHooks(): Hooks
    {
        return new Hooks($this->dbh, $this->config);
    }

    public function newLeadFunctions(): LeadFunctions
    {
        return new LeadFunctions($this->dbh, $this->config);
    }

    public function newListingPages(): ListingPages
    {
        return new ListingPages($this->dbh, $this->config);
    }

    public function newForms(): Forms
    {
        return new Forms($this->dbh, $this->config);
    }

    public function newGeneralAdmin(): GeneralAdmin
    {
        return new GeneralAdmin($this->dbh, $this->config);
    }

    public function newPageEditor(): PageEditor
    {
        return new PageEditor($this->dbh, $this->config);
    }

    public function newListingEditor(): ListingEditor
    {
        return new ListingEditor($this->dbh, $this->config);
    }

    public function newMediaHandler(): MediaHandler
    {
        return new MediaHandler($this->dbh, $this->config);
    }

    public function newImageHandler(): ImageHandler
    {
        return new ImageHandler($this->dbh, $this->config);
    }

    public function newFileHandler(): FileHandler
    {
        return new FileHandler($this->dbh, $this->config);
    }

    public function newVtourHandler(): VtourHandler
    {
        return new VtourHandler($this->dbh, $this->config);
    }

    public function newBlogEditor(): BlogEditor
    {
        return new BlogEditor($this->dbh, $this->config);
    }

    public function newPropertyClass(): PropertyClass
    {
        return new PropertyClass($this->dbh, $this->config);
    }

    public function newAddonManager(): AddonManager
    {
        return new AddonManager($this->dbh, $this->config);
    }

    public function newNotification(): Notification
    {
        return new Notification($this->dbh, $this->config);
    }

    public function newLeadManager(): LeadManager
    {
        return new LeadManager($this->dbh, $this->config);
    }

    public function newTracking(): Tracking
    {
        return new Tracking($this->dbh, $this->config);
    }

    public function newSitemap(): Sitemap
    {
        return new Sitemap($this->dbh, $this->config);
    }

    public function newUserManagement(): UserManagement
    {
        return new UserManagement($this->dbh, $this->config);
    }

    public function newLog(): Log
    {
        return new Log($this->dbh, $this->config);
    }

    public function newCaptcha(): Captcha
    {
        return new Captcha($this->dbh, $this->config);
    }

    public function newSocial(): Social
    {
        return new Social($this->dbh, $this->config);
    }

    public function newContact(): Contact
    {
        return new Contact($this->dbh, $this->config);
    }

    public function newRss(): Rss
    {
        return new Rss($this->dbh, $this->config);
    }

    public function newMenuEditor(): MenuEditor
    {
        return new MenuEditor($this->dbh, $this->config);
    }

    public function newPageDisplay(): PageDisplay
    {
        return new PageDisplay($this->dbh, $this->config);
    }

    public function newBlogDisplay(): BlogDisplay
    {
        return new BlogDisplay($this->dbh, $this->config);
    }

    public function newCalculators(): Calculators
    {
        return new Calculators($this->dbh, $this->config);
    }

    public function newMaps(): Maps
    {
        return new Maps($this->dbh, $this->config);
    }

    public function newMembersFavorites(): MembersFavorites
    {
        return new MembersFavorites($this->dbh, $this->config);
    }

    public function newMembersSearch(): MembersSearch
    {
        return new MembersSearch($this->dbh, $this->config);
    }
}
