<?php

declare(strict_types=1);

namespace OpenRealty;

class MembersSearch extends BaseClass
{
    /**
     * @return string
     */
    public function deleteSearch(): string
    {
        global $lang, $ORconn;
        $misc = $this->newMisc();

        $display = '';

        $login = $this->newLogin();
        $status = $login->loginCheck('Member');

        if ($status === true) {
            if (!isset($_GET['searchID'])) {
                $display .= '<a href="' . $this->config['baseurl'] . '/index.php">' . $lang['perhaps_you_were_looking_something_else'] . '</a>';
            } elseif ($_GET['searchID'] == '') {
                $display .= '<a href="' . $this->config['baseurl'] . '/index.php">' . $lang['perhaps_you_were_looking_something_else'] . '</a>';
            } else {
                $userID = intval($_SESSION['userID']);
                $searchID = intval($_GET['searchID']);
                $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'usersavedsearches 
						WHERE usersavedsearches_id = ' . $searchID . ' 
						AND userdb_id = ' . $userID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $display .= '<br />' . $lang['search_deleted_from_favorites'];
                $display .= $this->viewSavedSearches();
            }
        } else {
            $display = $status;
        }
        return $display;
    }

    public function saveSearch(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();

        $display = '';

        $login = $this->newLogin();
        $status = $login->loginCheck('Member');

        //Load the Core Template class

        $page = $this->newPageUser();

        //Load the Template
        $page->loadPage($this->config['template_path'] . '/saved_searches_add.html');

        if ($status === true) {
            $userID = intval($_SESSION['userID']);
            $guidestring = '';
            $query = '';

            if (is_string($_POST['title']) && $_POST['title'] != '' && is_string($_POST['query']) && is_string($_POST['notify'])) {
                if (!isset($_POST['token']) || (is_string($_POST['token']) && !$misc->validateCsrfToken($_POST['token']))) {
                    //File name contains non alphanum chars die to prevent file system attacks.
                    $display .= '<div style="text-align: center" class="redtext">
                    ' . $lang['invalid_csrf_token'] . '</div>';
                    unset($_POST['title']);
                    $display .= $this->saveSearch();
                    return $display;
                }
                $title = $misc->makeDbSafe($_POST['title']);
                $query = $misc->makeDbSafe($_POST['query']);
                $notify = $misc->makeDbSafe($_POST['notify']);
                $sql = 'SELECT * 
						FROM ' . $this->config['table_prefix'] . "usersavedsearches 
						WHERE userdb_id = $userID 
						AND usersavedsearches_query_string = $query";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $num_columns = $recordSet->RecordCount();
                if ($num_columns == 0) {
                    $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "usersavedsearches 
									(userdb_id, usersavedsearches_title, usersavedsearches_query_string,usersavedsearches_last_viewed,usersavedsearches_new_listings,usersavedsearches_notify) 
							VALUES ($userID, $title, $query, now(), 0, $notify)";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    } else {
                        $display .= '<br />' . $lang['search_added_to_saved_searches'] . ' - ' . $_POST['title'];
                        $display .= $this->viewSavedSearches();
                    }

                    //strip out the form, we don't need it.
                    $page->replaceTemplateSection('saved_search_block', '');
                    $page->replaceTag('lang_notify_saved_search', '');
                    $page->replaceTag('lang_notify_saved_search_disabled', '');
                } else {
                    $saved_title = $recordSet->fields('usersavedsearches_title');
                    $saved_search_exists = $lang['search_already_in_saved_searches'] . ': <a href="' . $this->config['baseurl'] . '/index.php?action=searchresults' . $_POST['query'] . '">' . htmlentities($saved_title, ENT_COMPAT, $this->config['charset']) . '</a>';
                    $page->replaceTag('saved_search_exists', $saved_search_exists);
                    foreach ($_GET as $k => $v) {
                        if (is_array($v)) {
                            foreach ($v as $vitem) {
                                if (is_scalar($vitem)) {
                                    $guidestring .= '&amp;' . urlencode("$k") . '[]=' . urlencode("$vitem");
                                }
                            }
                        } else {
                            $guidestring .= '&amp;' . urlencode("$k") . '=' . urlencode("$v");
                        }
                    }

                    if ($this->config['email_users_notification_of_new_listings']) {
                        $page->replaceTag('lang_notify_saved_search', $lang['notify_saved_search']);
                        $page->replaceTag('lang_notify_saved_search_disabled', '');
                        $notify_element = ' <select name="notify" id="notify" size="1">
												<option value="yes">' . $lang['yes'] . '
												<option value="no">' . $lang['no'] . '
											</select>';
                        $page->replaceTag('saved_search_notify', $notify_element);
                    } else {
                        $page->replaceTag('lang_notify_saved_search', '');
                        $page->replaceTag('lang_notify_saved_search_disabled', $lang['notify_saved_search_disabled']);
                        $page->replaceTag('saved_search_notify', '');
                    }

                    $template_section = $page->getTemplateSection('saved_search_block');

                    //Replace the section in the Page object with our alterations
                    $page->replaceTemplateSection('saved_search_block', $template_section);
                    $query = stripslashes($query);
                    $query = str_replace("'", '', $query);
                }
            } else {
                foreach ($_GET as $k => $v) {
                    if ($v && $k != 'action' && $k != 'PHPSESSID') {
                        if (is_array($v)) {
                            foreach ($v as $vitem) {
                                if (is_scalar($vitem)) {
                                    $query .= '&amp;' . urlencode("$k") . '[]=' . urlencode("$vitem");
                                }
                            }
                        } else {
                            $query .= '&amp;' . urlencode("$k") . '=' . urlencode("$v");
                        }
                    }
                }
                if (substr($query, 0, strcspn($query, '=')) == 'cur_page') {
                    $query = substr($query, strcspn($query, '&') + 1);
                    // echo $QUERY_STRING;
                }
                $sql = 'SELECT usersavedsearches_title, usersavedsearches_query_string 
						FROM ' . $this->config['table_prefix'] . "usersavedsearches 
						WHERE userdb_id = $_SESSION[userID] 
						AND usersavedsearches_query_string = '$query'";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $num_columns = $recordSet->RecordCount();

                if ($num_columns != 0) {
                    $saved_search_exists = $lang['search_already_in_saved_searches'] . ': <a href="' . $this->config['baseurl'] . '/index.php?searchresults&amp;' . $recordSet->fields('usersavedsearches_query_string') . '">' . htmlentities($recordSet->fields('usersavedsearches_title'), ENT_COMPAT, $this->config['charset']) . '</a>';
                    $page->replaceTag('saved_search_exists', $saved_search_exists);
                } else {
                    // Get full guidesting

                    foreach ($_GET as $k => $v) {
                        if (is_array($v)) {
                            foreach ($v as $vitem) {
                                if (is_scalar($vitem)) {
                                    $guidestring .= '&amp;' . urlencode("$k") . '[]=' . urlencode("$vitem");
                                }
                            }
                        } else {
                            $guidestring .= '&amp;' . urlencode("$k") . '=' . urlencode("$v");
                        }
                    }

                    if ($this->config['email_users_notification_of_new_listings']) {
                        $page->replaceTag('lang_notify_saved_search', $lang['notify_saved_search']);
                        $page->replaceTag('lang_notify_saved_search_disabled', '');
                        //$display .= $lang['notify_saved_search'];
                        $notify_element = ' <select name="notify" id="notify" size="1">
												<option value="yes">' . $lang['yes'] . '
												<option value="no">' . $lang['no'] . '
											</select>';
                        $page->replaceTag('saved_search_notify', $notify_element);
                    } else {
                        $page->replaceTag('saved_search_notify', '');
                        $page->replaceTag('lang_notify_saved_search', '');
                        $page->replaceTag('lang_notify_saved_search_disabled', $lang['notify_saved_search_disabled']);
                    }
                }

                $template_section = $page->getTemplateSection('saved_search_block');

                //Replace the section in the Page object with our alterations
                $page->replaceTemplateSection('saved_search_block', $template_section);
            }

            $page->replaceTag('saved_search_guidestring', $guidestring);
            $page->replaceTag('saved_search_query', $query);
            $page->replacePermissionTags();
            $page->replaceUrls();
            $page->autoReplaceTags();
            $page->replaceLangTemplateTags();

            $display .= $page->returnPage();
        } else {
            $display = $status;
        }
        return $display;
    }

    public function viewSavedSearches(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $display = '';
        $saved_searches = '';
        $saved_search_error = '';


        $login = $this->newLogin();

        $status = $login->loginCheck('Member');
        if ($status === true) {
            //Load the Core Template class

            $page = $this->newPageUser();

            //Load the Notify Listing Template specified in the Site Config
            $page->loadPage($this->config['template_path'] . '/saved_searches.html');

            $userID = intval($_SESSION['userID']);
            $sql = 'SELECT usersavedsearches_id, usersavedsearches_title, usersavedsearches_query_string, usersavedsearches_last_viewed
					FROM ' . $this->config['table_prefix'] . 'usersavedsearches 
					WHERE userdb_id = ' . $userID . ' 
					ORDER BY usersavedsearches_title';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }

            $num_columns = $recordSet->RecordCount();
            if ($num_columns == 0) {
                $saved_search_error = $lang['no_saved_searches'] . '<br /><br />';
            } else {
                $saved_search_dataset = $page->getTemplateSection('saved_search_dataset');
                while (!$recordSet->EOF) {
                    $saved_searches .= $saved_search_dataset;
                    $title = $recordSet->fields('usersavedsearches_title');
                    $last_viewed = $misc->convertTimestamp(strtotime($recordSet->fields('usersavedsearches_last_viewed')), true);
                    if ($title == '') {
                        $title = $lang['saved_search'];
                    }
                    $saved_search_link = '<a href="index.php?action=searchresults' . htmlspecialchars($recordSet->fields('usersavedsearches_query_string')) . '">' . htmlentities($title, ENT_COMPAT, $this->config['charset']) . '</a>';
                    $saved_search_delete = '<a href="index.php?action=delete_search&amp;searchID=' . $recordSet->fields('usersavedsearches_id') . '" onclick="return confirmDelete()">' . $lang['delete_search'] . '</a>';
                    $saved_searches = $page->parseTemplateSection($saved_searches, 'saved_search_link', $saved_search_link);
                    $saved_searches = $page->parseTemplateSection($saved_searches, 'saved_search_delete', $saved_search_delete);
                    $saved_searches = $page->parseTemplateSection($saved_searches, 'saved_search_last_viewed', $last_viewed);
                    $recordSet->MoveNext();
                }
            }

            $page->replaceTag('saved_search_error', $saved_search_error);
            $page->replaceTemplateSection('saved_search_dataset', $saved_searches);
            $page->replacePermissionTags();
            $page->replaceUrls();
            $page->autoReplaceTags();
            $page->replaceLangTemplateTags();
            $display .= $page->returnPage();
        } else {
            $display = $status;
        }
        return $display;
    }
}
