<?php

declare(strict_types=1);

namespace OpenRealty;

class Forms extends BaseClass
{
    /**
     * @param string          $field_type
     * @param string          $field_name
     * @param string|string[] $field_value
     * @param string          $field_caption
     * @param string          $default_text
     * @param string          $required
     * @param string|string[] $field_elements
     * @param string          $field_length
     * @param string          $tool_tip
     * @return string
     */
    public function renderFormElement(string $field_type, string $field_name, string|array $field_value, string $field_caption, string $default_text, string $required, string|array $field_elements, string $field_length = '', string $tool_tip = ''): string
    {
        // handles the rendering of already filled in user forms
        $field_value_array = [];
        $field_value_string = '';
        $field_value_raw_string = '';
        if (is_array($field_value)) {
            foreach ($field_value as $v) {
                $field_value_array[$v] = htmlentities($v, ENT_COMPAT, $this->config['charset']);
            }
        } else {
            $field_value_raw_string = $field_value;
            $field_value_string = htmlentities($field_value, ENT_COMPAT, $this->config['charset']);
        }
        sort($field_value_array);
        if (!is_array($field_elements)) {
            $field_elements = explode('||', $field_elements);
        }
        sort($field_elements);
        $display = '';
        $required_html = '';
        if ($required == 'Yes') {
            $required_html = 'required';
        }
        $max_lenth_html = '';
        if ($field_length != '' && $field_length != 0) {
            $max_lenth_html .= 'maxlength="' . $field_length . '" ';
        }
        switch ($field_type) {
            case 'lat':
            case 'long':
            case 'text': // handles text input boxes
                $tool_tip_html = '';
                if ($tool_tip != '') {
                    $tool_tip_safe = htmlentities($tool_tip, ENT_COMPAT, $this->config['charset']);
                    $tool_tip_html .= '<a href="#" data-bs-toggle="tooltip" data-bs-placement="top" style="margin-left:5px !important" title="' . $tool_tip_safe . '"><i class="fa-solid fa-circle-info"></i></a>';
                }
                $display .= '<div class="input-group input-group-static mb-2">
                <label for="' . $field_name . '"
                  >' . $field_caption . '</label
                >
                ' . $tool_tip_html . '
                <input
                  type="text"
                  name="' . $field_name . '"
                  id="' . $field_name . '"
                  value="' . $field_value_string . '"
                  class="form-control"
                  ' . $required_html . '
                  ' . $max_lenth_html . '
                />
              </div>';
                break;
            case 'date':
                $tool_tip_html = '';
                if ($tool_tip != '') {
                    $tool_tip_safe = htmlentities($tool_tip, ENT_COMPAT, $this->config['charset']);
                    $tool_tip_html .= '<a href="#" data-bs-toggle="tooltip" data-bs-placement="top" style="margin-left:5px !important" title="' . $tool_tip_safe . '"><i class="fa-solid fa-circle-info"></i></a>';
                }
                // HTML5 Date Fields always pass data in yyyy-mm-dd format
                if ($field_value_string != '') {
                    $field_value_string = date('Y-m-d', intval($field_value_string));
                }
                $display .= '<div class="input-group input-group-static mb-2">
                <label for="' . $field_name . '"
                  >' . $field_caption . '</label
                >
                ' . $tool_tip_html . '
                <input
                  type="date"
                  name="' . $field_name . '"
                  id="' . $field_name . '"
                  value="' . $field_value_string . '"
                  class="form-control"
                  ' . $required_html . '
                  ' . $max_lenth_html . '
                />
              </div>';
                break;

            case 'textarea': // handles textarea input
                $tool_tip_html = '';
                if ($tool_tip != '') {
                    $tool_tip_safe = htmlentities($tool_tip, ENT_COMPAT, $this->config['charset']);
                    $tool_tip_html .= '<a href="#" data-bs-toggle="tooltip" data-bs-placement="top" style="margin-left:5px !important" title="' . $tool_tip_safe . '"><i class="fa-solid fa-circle-info"></i></a>';
                }
                $display .= '<div class="input-group input-group-static mb-2">
              <label for="' . $field_name . '"
                >' . $field_caption . '</label
              >
              ' . $tool_tip_html . '
              <textarea
                name="' . $field_name . '"
                id="' . $field_name . '"
                cols="80"
                class="form-control"
                ' . $required_html . '
              >' . $field_value_string . '</textarea>
            </div>';
                break;

            case 'select':
                $tool_tip_html = '';
                if ($tool_tip != '') {
                    $tool_tip_safe = htmlentities($tool_tip, ENT_COMPAT, $this->config['charset']);
                    $tool_tip_html .= '<a href="#" data-bs-toggle="tooltip" data-bs-placement="top" style="margin-left:5px !important" title="' . $tool_tip_safe . '"><i class="fa-solid fa-circle-info"></i></a>';
                }
                // handles single item select boxes
                $display .= '<div class="input-group input-group-static">
              <label for="' . $field_name . '" class="ms-0"
                >' . $field_caption . '</label
              >
              ' . $tool_tip_html . '
              <select
                name="' . $field_name . '"
                class="form-control"
                id="' . $field_name . '"
              >';

                foreach ($field_elements as $list_item) {
                    $html_list_item = htmlentities($list_item, ENT_COMPAT, $this->config['charset']);
                    $display .= '			<option value="' . $html_list_item . '"';
                    if ($list_item == $field_value_raw_string || $list_item == "{lang_$field_value_raw_string}") {
                        $display .= ' selected ';
                    }
                    $display .= '>' . $list_item . '
                    </option>';
                }
                $display .= '
                </select>
              </div>';
                break;
            case 'select-multiple': // handles multiple item select boxes
                $tool_tip_html = '';
                if ($tool_tip != '') {
                    $tool_tip_safe = htmlentities($tool_tip, ENT_COMPAT, $this->config['charset']);
                    $tool_tip_html .= '<a href="#" data-bs-toggle="tooltip" data-bs-placement="top" style="margin-left:5px !important" title="' . $tool_tip_safe . '"><i class="fa-solid fa-circle-info"></i></a>';
                }
                // handles single item select boxes
                $display .= '<div class="input-group input-group-static">
              <label for="' . $field_name . '" class="ms-0"
                >' . $field_caption . '</label
              >
              ' . $tool_tip_html . '
              <select
                name="' . $field_name . '"
                class="form-control"
                id="' . $field_name . '"
                multiple
              >';
                foreach ($field_elements as $list_item) {
                    $html_list_item = htmlentities($list_item, ENT_COMPAT, $this->config['charset']);
                    $display .= '			<option value="' . $html_list_item . '"';
                    if ($list_item == $field_value_raw_string || $list_item == "{lang_$field_value_raw_string}") {
                        $display .= ' selected ';
                    }
                    $display .= '>' . $list_item . '</option>';
                }
                $display .= '
                </select>
              </div>';
                break;

            case 'divider': // handles dividers in forms
                $display .= '<div class="input-group input-group-static mb-2">
                  <label for="' . $field_name . '"
                    >' . $field_caption . '</label
                  >
                  <div class="field_element"></div>
                </div>';
                break;

            case 'price': // handles price input
                $tool_tip_html = '';
                if ($tool_tip != '') {
                    $tool_tip_safe = htmlentities($tool_tip, ENT_COMPAT, $this->config['charset']);
                    $tool_tip_html .= '<a href="#" data-bs-toggle="tooltip" data-bs-placement="top" style="margin-left:5px !important" title="' . $tool_tip_safe . '"><i class="fa-solid fa-circle-info"></i></a>';
                }
                $display .= '<div class="input-group input-group-static mb-2">
              <label for="' . $field_name . '">' . $field_caption . '</label>
              ' . $tool_tip_html . '
              <span class="input-group-text">' . $this->config['money_sign'] . '</span>
              <input
                type="number"
                name="' . $field_name . '"
                id="' . $field_name . '"
                value="' . $field_value_string . '"
                class="form-control"
                ' . $required_html . '
                ' . $max_lenth_html . '
              /></div>';
                break;

            case 'url': // handles url input fields
                $tool_tip_html = '';
                if ($tool_tip != '') {
                    $tool_tip_safe = htmlentities($tool_tip, ENT_COMPAT, $this->config['charset']);
                    $tool_tip_html .= '<a href="#" data-bs-toggle="tooltip" data-bs-placement="top" style="margin-left:5px !important" title="' . $tool_tip_safe . '"><i class="fa-solid fa-circle-info"></i></a>';
                }
                $display .= '<div class="input-group input-group-static mb-2">
              <label for="' . $field_name . '"
                >' . $field_caption . '</label
              >
              ' . $tool_tip_html . '
              <input
                type="url"
                name="' . $field_name . '"
                id="' . $field_name . '"
                value="' . $field_value_string . '"
                class="form-control"
                ' . $required_html . '
                ' . $max_lenth_html . '
              /></div>';
                break;
            case 'email': // handles email input
                $tool_tip_html = '';
                if ($tool_tip != '') {
                    $tool_tip_safe = htmlentities($tool_tip, ENT_COMPAT, $this->config['charset']);
                    $tool_tip_html .= '<a href="#" data-bs-toggle="tooltip" data-bs-placement="top" style="margin-left:5px !important" title="' . $tool_tip_safe . '"><i class="fa-solid fa-circle-info"></i></a>';
                }
                $display .= '<div class="input-group input-group-static mb-2">
                <label for="' . $field_name . '"
                  >' . $field_caption . '</label
                >
                ' . $tool_tip_html . '
                <input
                  type="email"
                  name="' . $field_name . '"
                  id="' . $field_name . '"
                  value="' . $field_value_string . '"
                  class="form-control"
                  ' . $required_html . '
                  ' . $max_lenth_html . '
                /></div>';
                break;
            case 'checkbox': // handles checkboxes
                $tool_tip_html = '';
                if ($tool_tip != '') {
                    $tool_tip_safe = htmlentities($tool_tip, ENT_COMPAT, $this->config['charset']);
                    $tool_tip_html .= '<a href="#" data-bs-toggle="tooltip" data-bs-placement="top" style="margin-left:5px !important" title="' . $tool_tip_safe . '"><i class="fa-solid fa-circle-info"></i></a>';
                }
                if ($required != 'Yes') {
                    $display .= '		<input type="hidden" value="" name="' . $field_name . '[]" />';
                }
                $count = 1;
                $display .= '<div class="input-group input-group-static mb-2">
              <label for="' . $field_name . '[]">' . $field_caption . '</label>
              ' . $tool_tip_html . '
            </div>';
                foreach ($field_elements as $feature_list_item) {
                    $html_feature_list_item = htmlentities($feature_list_item, ENT_COMPAT, $this->config['charset']);
                    $checked_html = '';
                    if (!empty($field_value_array)) {
                        foreach ($field_value_array as $field_value_list_item) {
                            if ($field_value_list_item == $feature_list_item || $field_value_list_item == "{lang_$feature_list_item}") {
                                $checked_html = ' checked ';
                            } // end if
                        } // end while
                    } else {
                        if ($field_value_raw_string == $feature_list_item || $field_value_raw_string == "{lang_$feature_list_item}") {
                            $checked_html = ' checked ';
                        } // end if
                    }
                    $display .= '<div class="form-check">
                <input class="form-check-input" type="checkbox" value="' . $html_feature_list_item . '" name="' . $field_name . '[]" id="' . $field_name . $count . '" ' . $checked_html . ' ' . $required_html . ' />
                <label class="custom-control-label" for="' . $field_name . $count . '">' . $html_feature_list_item . '</label>
                </div>';
                    $count++;
                } // end while
                break;
            case 'option': // handles options
                $tool_tip_html = '';
                if ($tool_tip != '') {
                    $tool_tip_safe = htmlentities($tool_tip, ENT_COMPAT, $this->config['charset']);
                    $tool_tip_html .= '<a href="#" data-bs-toggle="tooltip" data-bs-placement="top" style="margin-left:5px !important" title="' . $tool_tip_safe . '"><i class="fa-solid fa-circle-info"></i></a>';
                }
                if ($required != 'Yes') {
                    $display .= '		<input type="hidden" value="" name="' . $field_name . '[]" />';
                }
                $count = 1;
                $display .= '<div class="input-group input-group-static mb-2">
              <label >' . $field_caption . '</label>
              ' . $tool_tip_html . '
            </div>';
                foreach ($field_elements as $feature_list_item) {
                    $html_feature_list_item = htmlentities($feature_list_item, ENT_COMPAT, $this->config['charset']);
                    $checked_html = '';
                    if (!empty($field_value_array)) {
                        foreach ($field_value_array as $field_value_list_item) {
                            if ($field_value_list_item == $feature_list_item || $field_value_list_item == "{lang_$feature_list_item}") {
                                $checked_html = ' checked ';
                            } // end if
                        } // end while
                    } else {
                        if ($field_value_raw_string == $feature_list_item || $field_value_raw_string == "{lang_$feature_list_item}") {
                            $checked_html = ' checked ';
                        } // end if
                    }
                    $display .= '<div class="form-check">
                <input class="form-check-input" type="radio" value="' . $html_feature_list_item . '" name="' . $field_name . '" id="' . $field_name . $count . '" ' . $checked_html . ' ' . $required_html . '>
                <label class="custom-control-label" for="' . $field_name . $count . '">' . $html_feature_list_item . '</label>
               </div> ';
                    $count++;
                } // end while
                break;
            case 'number':
            case 'decimal':
                $tool_tip_html = '';
                if ($tool_tip != '') {
                    $tool_tip_safe = htmlentities($tool_tip, ENT_COMPAT, $this->config['charset']);
                    $tool_tip_html .= '<a href="#" data-bs-toggle="tooltip" data-bs-placement="top" style="margin-left:5px !important" title="' . $tool_tip_safe . '"><i class="fa-solid fa-circle-info"></i></a>';
                }
                $display .= '<div class="input-group input-group-static mb-2">
                  <label for="' . $field_name . '"
                    >' . $field_caption . '</label
                  >
                  ' . $tool_tip_html . '
                  <input
                    type="number"
                    name="' . $field_name . '"
                    id="' . $field_name . '"
                    value="' . $field_value_string . '"
                    class="form-control"
                    ' . $required_html . '
                    ' . $max_lenth_html . '
                  /></div>';
                break;

            case 'submit': // handles submit buttons
                $display .= '<div class="input-group input-group-static mb-2">
								<input class="btn btn-primary" type="submit" value="' . $field_value_string . '" />
							</div>';
                break;

            default: // the catch all... mostly for errors and whatnot
                $display .= '<div>no handler yet</div>';
        } // end switch statement
        return $display;
    }

    /**
     * @return string|string[]
     *
     * @psalm-return 'No'|'Yes'|array<'REQUIRED'|'TYPE'>
     */
    public function validateForm(string $db_to_validate, array $pclass = []): array|string
    {
        // Validates the info being put into the system
        global $ORconn;
        $misc = $this->newMisc();
        $errors = [];
        $pass_the_form = 'Yes';
        // this stuff is input that's already been dealt with
        // check to if the form should be passed
        $sql = 'SELECT ' . $db_to_validate . '_required, ' . $db_to_validate . '_field_type, ' . $db_to_validate . '_field_name 
				FROM ' . $this->config['table_prefix'] . $db_to_validate;
        if (count($pclass) > 0 && $db_to_validate == 'listingsformelements') {
            $sql .= ' WHERE listingsformelements_id IN (
							SELECT listingsformelements_id 
							FROM ' . $this->config['table_prefix_no_lang'] . 'classformelements 
							WHERE class_id IN (' . implode(',', $pclass) . ')
						)';
        }
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        while (!$recordSet->EOF) {
            $required = $recordSet->fields($db_to_validate . '_required');
            $field_type = $recordSet->fields($db_to_validate . '_field_type');
            $field_name = $recordSet->fields($db_to_validate . '_field_name');
            if ($required == 'Yes') {
                if (!isset($_POST[$field_name]) || (is_array($_POST[$field_name]) && count($_POST[$field_name]) == 0) || (!is_array($_POST[$field_name]) && trim($_POST[$field_name]) == '')) {
                    $pass_the_form = 'No';
                    $errors[$field_name] = 'REQUIRED';
                }
            } // end if
            if ($field_type == 'number' && isset($_POST[$field_name]) && !is_numeric($_POST[$field_name]) && $_POST[$field_name] != '') {
                $pass_the_form = 'No';
                $errors[$field_name] = 'TYPE';
            }
            $recordSet->MoveNext();
        }
        if (empty($errors)) {
            return $pass_the_form;
        } else {
            return $errors;
        }
    }
}
