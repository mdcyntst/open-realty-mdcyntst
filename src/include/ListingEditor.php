<?php

declare(strict_types=1);

namespace OpenRealty;

use Exception;

class ListingEditor extends BaseClass
{
    /**
     * @return (mixed|string)[][]
     *
     * @psalm-return array<int<0, max>, array{value: mixed, label: string}>
     */
    public function ajaxAddLeadLookupListing(string $term): array
    {
        global $ORconn;

        $misc = $this->newMisc();
        $sql_term = $ORconn->addQ($term);
        $sql = 'SELECT listingsdb_id,listingsdb_title
							FROM ' . $this->config['table_prefix'] . 'listingsdb
							WHERE listingsdb_active = \'yes\' 
							AND ( listingsdb_title LIKE \'%' . $sql_term . '%\' OR listingsdb_id = \'' . $sql_term . '\')';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        // get main listings data
        $results = [];
        $x = 0;
        while (!$recordSet->EOF) {
            $results[$x]['value'] = $recordSet->fields('listingsdb_id');
            $results[$x]['label'] = $recordSet->fields('listingsdb_title') . ' (' . $recordSet->fields('listingsdb_id') . ')';
            $x++;
            $recordSet->MoveNext();
        } // end while
        return $results;
    }

    public function notifyNewListing(int $listingID): string
    {
        global $ORconn, $lang;

        $display = '';
        $misc = $this->newMisc();
        $search_page = $this->newSearchPage();
        $notify_count = 0;
        $sql = 'SELECT userdb_id, usersavedsearches_title, usersavedsearches_query_string, usersavedsearches_notify FROM ' . $this->config['table_prefix'] . "usersavedsearches WHERE usersavedsearches_notify = 'yes'";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        while (!$recordSet->EOF) {
            $query_string = $recordSet->fields('usersavedsearches_query_string');
            $user_id = $recordSet->fields('userdb_id');
            $search_title = $recordSet->fields('usersavedsearches_title');
            // Break Quesry String up into $_GET variables.
            unset($_GET);
            $query_string = urldecode($query_string);
            $criteria = explode('&', $query_string);
            foreach ($criteria as $crit) {
                if ($crit != '') {
                    $pieces = explode('=', $crit);
                    $pos = strpos($pieces[0], '[]');
                    if ($pos !== false) {
                        $name = substr($pieces[0], 0, -2);
                        $_GET[$name][] = $pieces[1];
                    } else {
                        $_GET[$pieces[0]] = $pieces[1];
                    }
                }
            }
            $matched_listing_ids = $search_page->searchResults(true);
            if (in_array($listingID, $matched_listing_ids)) {
                // Listing Matches Search
                $sql = 'SELECT userdb_user_name, userdb_emailaddress FROM ' . $this->config['table_prefix'] . 'userdb WHERE userdb_id = ' . $user_id;
                $recordSet2 = $ORconn->Execute($sql);
                if (is_bool($recordSet2)) {
                    $misc->logErrorAndDie($sql);
                }
                $email = (string)$recordSet2->fields('userdb_emailaddress');

                $message = $lang['automated_email'] . "\r\n\r\n\r\n" . date('F j, Y, g:i:s a') . "\r\n\r\n" . $lang['new_listing_notify_long'] . "'" . $search_title . "'.\r\n\r\n" . $lang['click_on_link_to_view_listing'] . "\r\n\r\n" . $this->config['baseurl'] . "/index.php?action=listingview&listingID=" . $listingID . "\r\n\r\n\r\n" . $lang['click_to_view_saved_searches'] . "\r\n\r\n" . $this->config['baseurl'] . "/index.php?action=view_saved_searches\r\n\r\n\r\n" . $lang['automated_email'] . "\r\n";
                // Send Mail
                if (isset($this->config['site_email']) && $this->config['site_email'] != '') {
                    $sender_email = $this->config['site_email'];
                } else {
                    $sender_email = $this->config['admin_email'];
                }
                $subject = $lang['new_listing_notify'] . $search_title;
                $misc->sendEmail($this->config['admin_name'], $sender_email, $email, $message, $subject);
                $notify_count++;
            }
            $recordSet->MoveNext();
            if ($notify_count > 0) {
                $display .= $lang['new_listing_email_sent'] . $notify_count . $lang['new_listing_email_users'] . '<br />';
            }
        } // while
        return $display;
    }

    public function ajaxMakeInactiveListing(int $listing_id): string
    {
        global $lang;
        if (!$this->config['moderate_listings'] || ($_SESSION['admin_privs'] == 'yes' || $_SESSION['moderator'] == 'yes')) {
            $listing_api = $this->newListingApi();
            try {
                return json_encode($listing_api->update(['listing_id' => $listing_id, 'listing_details' => ['active' => false]])) ?: '';
            } catch (Exception $e) {
                header('Content-type: application/json');
                http_response_code(500);
                return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
            }
        } else {
            header('Content-type: application/json');
            http_response_code(500);
            return json_encode(['error' => true, 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
        }
    }

    /**
     * @return string
     */
    public function ajaxMakeActiveListing(int $listing_id): string
    {
        global $lang;
        if (!$this->config['moderate_listings'] || ($_SESSION['admin_privs'] == 'yes' || $_SESSION['moderator'] == 'yes')) {
            $listing_api = $this->newListingApi();
            try {
                return json_encode($listing_api->update(['listing_id' => $listing_id, 'listing_details' => ['active' => true]])) ?: '';
            } catch (Exception $e) {
                header('Content-type: application/json');
                http_response_code(500);
                return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
            }
        } else {
            header('Content-type: application/json');
            http_response_code(500);
            return json_encode(['error' => true, 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
        }
    }

    /**
     * @return string
     */
    public function ajaxMakeUnfeaturedListing(int $listing_id): string
    {
        global $lang;
        if ($_SESSION['featureListings'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
            $listing_api = $this->newListingApi();
            try {
                return json_encode($listing_api->update(['listing_id' => $listing_id, 'listing_details' => ['featured' => false]])) ?: '';
            } catch (Exception $e) {
                header('Content-type: application/json');
                http_response_code(500);
                return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
            }
        } else {
            header('Content-type: application/json');
            http_response_code(500);
            return json_encode(['error' => true, 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
        }
    }

    /**
     * @return string
     */
    public function ajaxMakeFeaturedListing(int $listing_id): string
    {
        global $lang;
        if ($_SESSION['featureListings'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
            $listing_api = $this->newListingApi();
            try {
                return json_encode($listing_api->update(['listing_id' => $listing_id, 'listing_details' => ['featured' => true]])) ?: '';
            } catch (Exception $e) {
                header('Content-type: application/json');
                http_response_code(500);
                return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
            }
        } else {
            header('Content-type: application/json');
            http_response_code(500);
            return json_encode(['error' => true, 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
        }
    }

    public function ajaxDisplayAddListing(): string
    {
        global $ORconn;

        $status_text = '';
        //Load the Core Template
        $misc = $this->newMisc();
        $page = $this->newPageAdmin();
        $page->loadPage($this->config['admin_template_path'] . '/add_listing.html');

        // get list of all property clases
        $sql = 'SELECT class_name, class_id 
				FROM ' . $this->config['table_prefix'] . 'class 
				ORDER BY class_rank';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $classes = [];
        while (!$recordSet->EOF) {
            $classes[(int)$recordSet->fields('class_id')] = (string)$recordSet->fields('class_name');
            $recordSet->MoveNext();
        }

        $html = $page->getTemplateSection('pclass_block');
        $html = $page->formOptions($classes, '', $html);
        $page->replaceTemplateSection('pclass_block', $html);

        $sql = 'SELECT userdb_id, userdb_user_first_name, userdb_user_last_name 
				FROM ' . $this->config['table_prefix'] . "userdb 
				WHERE userdb_is_agent = 'yes' or userdb_is_admin = 'yes' 
				ORDER BY userdb_user_last_name,userdb_user_first_name";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $all_agents = [];
        while (!$recordSet->EOF) {
            // strip slashes so input appears correctly
            $agent_ID = (int)$recordSet->fields('userdb_id');
            $agent_first_name = (string)$recordSet->fields('userdb_user_first_name');
            $agent_last_name = (string)$recordSet->fields('userdb_user_last_name');
            $all_agents[$agent_ID] = $agent_last_name . ', ' . $agent_first_name;
            $recordSet->MoveNext();
        }
        $listing_agent_id = intval($_SESSION['userID']);
        $html = $page->getTemplateSection('listing_agent_option_block');
        $html = $page->formOptions($all_agents, (string)$listing_agent_id, $html);
        $page->replaceTemplateSection('listing_agent_option_block', $html);

        $page->replaceTag('application_status_text', $status_text);
        $page->replaceLangTemplateTags();
        $page->replacePermissionTags();
        $page->autoReplaceTags('', true);
        return $page->returnPage();
    }

    public function ajaxAddListing(): string
    {
        global $lang;

        if (isset($_POST['pclass'][0]) && isset($_POST['title']) && is_string($_POST['title'])) {
            $class_id = intval($_POST['pclass'][0]);
            $new_listing_owner = intval($_POST['or_owner'] ?? $_SESSION['userID']);
            $listing_api = $this->newListingApi();
            try {
                $result = $listing_api->create(['class_id' => $class_id, 'listing_details' => ['title' => $_POST['title'], 'featured' => false, 'active' => false, 'notes' => ''], 'listing_agents' => [$new_listing_owner], 'listing_fields' => [], 'listing_media' => []]);
            } catch (Exception $e) {
                header('Content-type: application/json');
                http_response_code(500);
                return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
            }
            $new_listing_id = $result['listing_id'];
            header('Content-type: application/json');
            return json_encode(['error' => 0, 'listing_id' => $new_listing_id]) ?: '';
        } else {
            http_response_code(500);
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
        }
    }

    public function ajaxUpdateListingData(int $listing_id): string
    {
        global $lang;
        $display = '';

        $listing_pages = $this->newListingPages();

        $login = $this->newLogin();
        //Get Listing owner
        $listing_agent_id = $listing_pages->getListingAgentValue('userdb_id', $listing_id);
        //Make sure we can Edit this lisitng
        $has_permission = true;
        if ($_SESSION['userID'] != $listing_agent_id) {
            $security = $login->verifyPriv('edit_all_listings');
            if ($security !== true) {
                $has_permission = false;
            }
        }
        if ($has_permission) {
            if ($_SESSION['admin_privs'] == 'yes' || $_SESSION['edit_all_listings'] == 'yes') {
                $display .= $this->updateListing(false);
            } else {
                $display .= $this->updateListing();
            }
        } else {
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
        }
        return $display;
    }

    public function ajaxDeleteListing(int $listing_id): string
    {
        global $lang;

        $listing_pages = $this->newListingPages();

        $login = $this->newLogin();
        //Get Listing owner
        $listing_agent_id = $listing_pages->getListingAgentValue('userdb_id', $listing_id);
        //Make sure we can Edit this lisitng
        $has_permission = true;
        if ($_SESSION['userID'] != $listing_agent_id) {
            $security = $login->verifyPriv('edit_all_listings');
            if ($security !== true) {
                $has_permission = false;
            }
        }
        if ($has_permission) {
            $listing_api = $this->newListingApi();
            try {
                $listing_api->delete(['listing_id' => $listing_id]);
            } catch (Exception $e) {
                header('Content-type: application/json');
                http_response_code(500);
                return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
            }
            header('Content-type: application/json');
            return json_encode(['error' => '0', 'listing_id' => $listing_id]) ?: '';
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
    }

    public function displayListingEditor(int $listing_id): string
    {
        global $ORconn, $lang;

        $status_text = '';
        $misc = $this->newMisc();
        $listing_pages = $this->newListingPages();
        //Get Listing owner
        $listing_agent_id = $listing_pages->getListingAgentValue('userdb_id', $listing_id);

        $login = $this->newLogin();
        //Make sure we can Edit this lisitng
        $has_permission = true;
        if ($_SESSION['userID'] != $listing_agent_id) {
            $security = $login->verifyPriv('edit_all_listings');
            if ($security !== true) {
                $has_permission = false;
            }
        }
        if ($has_permission) {
            $forms = $this->newForms();
            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/edit_listing.html');

            $yes_no = [];
            $yes_no['no'] = $lang['no'];
            $yes_no['yes'] = $lang['yes'];

            $listing_api = $this->newListingApi();
            try {
                $api_result = $listing_api->read(['listing_id' => $listing_id, 'fields' => []]);
            } catch (Exception) {
                $page->page = $page->removeTemplateBlock('listing_found', $page->page);
                $page->page = $page->cleanupTemplateBlock('listing_not_found', $page->page);

                //Finish Loading Template
                $page->replaceTags(['curley_open', 'curley_close', 'baseurl']);
                $page->replaceTag('application_status_text', $status_text);
                $page->replaceLangTemplateTags();
                $page->replacePermissionTags();
                $page->autoReplaceTags('', true);
                return $page->returnPage();
            }


            //echo '<pre>'.print_r($api_result,TRUE).'</pre>';
            $edit_title = is_string($api_result['listing']['listingsdb_title']) ? $api_result['listing']['listingsdb_title'] : '';
            $edit_seotitle = is_string($api_result['listing']['listing_seotitle']) ? $api_result['listing']['listing_seotitle'] : '';
            $edit_notes = is_string($api_result['listing']['listingsdb_notes']) ? $api_result['listing']['listingsdb_notes'] : '';
            $edit_active = is_string($api_result['listing']['listingsdb_active']) ? $api_result['listing']['listingsdb_active'] : '';
            $edit_featured = is_string($api_result['listing']['listingsdb_featured']) ? $api_result['listing']['listingsdb_featured'] : '';
            $edit_mlsexport = is_string($api_result['listing']['listingsdb_mlsexport']) ? $api_result['listing']['listingsdb_mlsexport'] : '';
            $edit_pclass = is_int($api_result['listing']['listingsdb_pclass_id']) ? $api_result['listing']['listingsdb_pclass_id'] : 0;
            $hit_count = is_int($api_result['listing']['listingsdb_hit_count']) ? $api_result['listing']['listingsdb_hit_count'] : 0;
            //$email = $recordSet->fields('userdb_emailaddress');
            $last_modified = date('D M j G:i:s T Y', strtotime(is_string($api_result['listing']['listingsdb_last_modified']) ? $api_result['listing']['listingsdb_last_modified'] : ''));
            $expiration = date($this->config['date_format_timestamp'], strtotime(is_string($api_result['listing']['listingsdb_expiration']) ? $api_result['listing']['listingsdb_expiration'] : ''));

            $listing_url = $page->magicURIGenerator('listing', (string)$listing_id, true);
            //Start Template Replacement
            $page->page = str_replace('{template_url}', $this->config['admin_template_url'], $page->page);
            $page->page = str_replace('{listing_hit_count}', (string)$hit_count, $page->page);
            $page->page = str_replace('{listing_url}', $listing_url, $page->page);
            $page->page = str_replace('{listing_last_modified}', $last_modified, $page->page);
            $page->page = str_replace('{listing_id}', (string)$listing_id, $page->page);
            $page->page = str_replace('{listing_agent_id}', $listing_agent_id, $page->page);
            $page->page = str_replace('{listing_title}', htmlentities($edit_title, ENT_COMPAT, $this->config['charset']), $page->page);
            $page->page = str_replace('{listing_seotitle}', htmlentities($edit_seotitle, ENT_COMPAT, $this->config['charset']), $page->page);
            $page->page = str_replace('{listing_note}', htmlentities($edit_notes, ENT_COMPAT, $this->config['charset']), $page->page);
            $page->page = str_replace('{baseurl}', $this->config['baseurl'], $page->page);
            $page->page = $page->replaceListingFieldTags($listing_id, $page->page);

            $creationdate = is_string($api_result['listing']['listingsdb_creation_date']) ? $api_result['listing']['listingsdb_creation_date'] : '';
            $formatted_creationdate = date($this->config['date_format_timestamp'], strtotime($creationdate));
            $page->page = str_replace('{listing_creation_date}', $formatted_creationdate, $page->page);
            //Build Property Class Drop Down
            $pclass_api = $this->newPClassApi();
            try {
                $pclass_result = $pclass_api->metadata();
            } catch (Exception $e) {
                $misc->logErrorAndDie($e->getMessage());
            }

            $all_classes = [];
            foreach ($pclass_result['metadata'] as $class_id => $classarray) {
                $all_classes[$class_id] = $classarray['name'];
            }
            $html = $page->getTemplateSection('pclass_block');
            $html = $page->formOptions($all_classes, (string)$edit_pclass, $html);
            $page->replaceTemplateSection('pclass_block', $html);
            //Featured Listings
            if ($_SESSION['featureListings'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                $page->page = $page->cleanupTemplateBlock('featured_listing', $page->page);
                $html = $page->getTemplateSection('featured_listing_option_block');
                $html = $page->formOptions($yes_no, $edit_featured, $html);
                $page->replaceTemplateSection('featured_listing_option_block', $html);
            } else {
                $page->page = $page->removeTemplateBlock('featured_listing', $page->page);
            }
            //Show Listing Satus Dropdown
            if ($this->config['moderate_listings']) {
                if ($_SESSION['admin_privs'] == 'yes' || $_SESSION['moderator'] == 'yes') {
                    $page->page = $page->cleanupTemplateBlock('listing_status', $page->page);
                    $html = $page->getTemplateSection('listing_status_option_block');
                    $html = $page->formOptions($yes_no, $edit_active, $html);
                    $page->replaceTemplateSection('listing_status_option_block', $html);
                } else {
                    $page->page = $page->removeTemplateBlock('listing_status', $page->page);
                }
            } else {
                $page->page = $page->cleanupTemplateBlock('listing_status', $page->page);
                $html = $page->getTemplateSection('listing_status_option_block');
                $html = $page->formOptions($yes_no, $edit_active, $html);
                $page->replaceTemplateSection('listing_status_option_block', $html);
            }
            if (($_SESSION['admin_privs'] == 'yes' || $_SESSION['edit_expiration'] == 'yes') && $this->config['use_expiration']) {
                $page->page = str_replace('{listing_expiration}', $expiration, $page->page);
                $page->replaceTag('config_date_format_long', $this->config['date_format_long']);
                $page->replaceTag('config_date_format', (string)$this->config['date_format']);
                $page->page = $page->cleanupTemplateBlock('listing_expiration', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('listing_expiration', $page->page);
            }
            if ($this->config['export_listings'] && $_SESSION['export_listings'] == 'yes') {
                $page->page = $page->cleanupTemplateBlock('listing_export', $page->page);
                $html = $page->getTemplateSection('listing_export_option_block');
                $html = $page->formOptions($yes_no, $edit_mlsexport, $html);
                $page->replaceTemplateSection('listing_export_option_block', $html);
                $page->page = $page->removeTemplateBlock('!listing_export', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('listing_export', $page->page);
                $page->page = $page->cleanupTemplateBlock('!listing_export', $page->page);
            }
            if ($_SESSION['admin_privs'] == 'yes' || $_SESSION['edit_all_listings'] == 'yes') {
                //Get List of ALl Agents
                $sql = 'SELECT userdb_id, userdb_user_first_name, userdb_user_last_name 
							FROM ' . $this->config['table_prefix'] . "userdb 
							WHERE userdb_is_agent = 'yes' or userdb_is_admin = 'yes' 
							ORDER BY userdb_user_last_name,userdb_user_first_name";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $all_agents = [];
                while (!$recordSet->EOF) {
                    // strip slashes so input appears correctly
                    $agent_ID = (int)$recordSet->fields('userdb_id');
                    $agent_first_name = (string)$recordSet->fields('userdb_user_first_name');
                    $agent_last_name = (string)$recordSet->fields('userdb_user_last_name');
                    $all_agents[$agent_ID] = $agent_last_name . ', ' . $agent_first_name;
                    $recordSet->MoveNext();
                }
                $page->page = $page->cleanupTemplateBlock('listing_agent', $page->page);
                $html = $page->getTemplateSection('listing_agent_option_block');
                $html = $page->formOptions($all_agents, $listing_agent_id, $html);
                $page->replaceTemplateSection('listing_agent_option_block', $html);
                $page->page = $page->removeTemplateBlock('!listing_agent', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('listing_agent', $page->page);
                $page->page = $page->cleanupTemplateBlock('!listing_agent', $page->page);
            }
            if ($this->config['show_notes_field']) {
                $page->page = $page->cleanupTemplateBlock('listing_notes', $page->page);
                $page->page = $page->removeTemplateBlock('!listing_notes', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('listing_notes', $page->page);
                $page->page = $page->cleanupTemplateBlock('!listing_notes', $page->page);
            }
            //Load Template Area From Config
            $sections = explode(',', $this->config['template_listing_sections']);
            $template_holder = ['misc_hold' => ''];
            foreach ($sections as $section) {
                if (str_contains($page->page, $section)) {
                    $template_holder[$section] = '';
                }
            }
            $pclass_list = [$edit_pclass];

            $fields_api = $this->newFieldsApi();
            try {
                $field_result = $fields_api->metadata(['resource' => 'listing', 'class' => $pclass_list]);
            } catch (Exception $e) {
                $misc->logErrorAndDie($e->getMessage());
            }
            //No error so get the fields that were returned..
            $field_list = $field_result['fields'];
            foreach ($field_list as $field) {
                $field_name = $field['field_name'] ?? '';
                $field_value = '';
                if (array_key_exists($field_name, $api_result['listing'])) {
                    if (is_array($api_result['listing'][$field_name])) {
                        $field_value = $api_result['listing'][$field_name];
                    } else {
                        $field_value = (string)$api_result['listing'][$field_name];
                    }
                }
                //echo $field_name.' : '.$field_value.'<br />';
                $field_type = $field['field_type'] ?? '';
                $field_caption = $field['field_caption'] ?? '';
                $default_text = $field['default_text'] ?? '';
                $field_elements = $field['field_elements'] ?? '';
                $required = $field['required'] ?? '';
                $field_length = strval($field['field_length'] ?? 0);
                $tool_tip = $field['tool_tip'] ?? '';
                $location = $field['location'] ?? '';
                // pass the data to the function
                $field = $forms->renderFormElement($field_type, $field_name, $field_value, $field_caption, $default_text, $required, $field_elements, $field_length, $tool_tip);

                if (array_key_exists($location, $template_holder)) {
                    $template_holder[$location] .= $field;
                } else {
                    $template_holder['misc_hold'] .= $field;
                }
            }
            foreach ($template_holder as $tag => $value) {
                $page->page = str_replace('{' . $tag . '}', $value, $page->page);
            }

            $page->page = $page->removeTemplateBlock('listing_not_found', $page->page);
            $page->page = $page->cleanupTemplateBlock('listing_found', $page->page);

            //Finish Loading Template
            $page->replaceTags(['curley_open', 'curley_close', 'baseurl']);
            $page->replaceTag('application_status_text', $status_text);
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        } else {
            return 'Permission Denied';
        }
    }

    public function editListings(bool $only_my_listings = true): string
    {
        global $lang, $listingID;
        $listing_api = $this->newListingApi();
        $misc = $this->newMisc();
        $login = $this->newLogin();
        if (!$only_my_listings) {
            $security = $login->verifyPriv('edit_all_listings');
        } else {
            $security = $login->verifyPriv('Agent');
        }
        $status_text = '';
        $page = $this->newPageAdmin();
        $page->loadPage($this->config['admin_template_path'] . '/listing_editor.html');

        $display = '';
        if ($security === true) {
            if (isset($_POST['filter']) || isset($_POST['lookup_field']) || isset($_POST['lookup_value'])) {
                if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfToken($_POST['token'])) {
                    $display .= '<div class="text-danger text-center">' . $lang['invalid_csrf_token'] . '</div>';
                    unset($_POST);
                    return $display;
                }
            }

            if (!isset($_GET['edit'])) {
                $_GET['edit'] = '';
            }
            if (isset($_POST['lookup_field']) && isset($_POST['lookup_value'])) {
                $_SESSION['edit_listing_qeb_lookup_field'] = $_POST['lookup_field'];
                $_SESSION['edit_listing_qeb_lookup_value'] = $_POST['lookup_value'];
            }
            if (isset($_SESSION['edit_listing_qeb_lookup_field']) && isset($_SESSION['edit_listing_qeb_lookup_value'])) {
                if ($_SESSION['edit_listing_qeb_lookup_field'] != 'listingsdb_id') {
                    $_POST['lookup_field'] = $_SESSION['edit_listing_qeb_lookup_field'];
                    $_POST['lookup_value'] = $_SESSION['edit_listing_qeb_lookup_value'];
                }
            }
            if (isset($_POST['filter'])) {
                $_GET['cur_page'] = 0;
                $_SESSION['edit_listing_qeb_filter'] = $_POST['filter'];
            }
            if (isset($_SESSION['edit_listing_qeb_filter'])) {
                $_POST['filter'] = $_SESSION['edit_listing_qeb_filter'];
            }
            if (isset($_POST['agent_filter'])) {
                $_GET['cur_page'] = 0;
                $_SESSION['edit_listing_qeb_agent_filter'] = $_POST['agent_filter'];
            }
            if (isset($_SESSION['edit_listing_qeb_agent_filter'])) {
                $_POST['agent_filter'] = $_SESSION['edit_listing_qeb_agent_filter'];
            }
            if (isset($_POST['pclass_filter'])) {
                $_GET['cur_page'] = 0;
                $_SESSION['edit_listing_qeb_pclass_filter'] = $_POST['pclass_filter'];
            }
            if (isset($_SESSION['edit_listing_qeb_pclass_filter'])) {
                $_POST['pclass_filter'] = $_SESSION['edit_listing_qeb_pclass_filter'];
            }
            if (isset($_POST['lookup_field']) && isset($_POST['lookup_value']) && $_POST['lookup_field'] == 'listingsdb_id' && $_POST['lookup_value'] != '') {
                $_GET['edit'] = intval($_POST['lookup_value']);
                //TODO FIX THIS CRAPPY TONIGHT!!
                $lookup_error = false;
                try {
                    $result = $listing_api->read(['listing_id' => intval($_POST['lookup_value']), 'fields' => ['listingsdb_pclass_id', 'listingsdb_active']]);
                } catch (Exception) {
                    $lookup_error = true;
                }
                if (!$lookup_error) {
                    return $this->displayListingEditor($_GET['edit']);
                }
            }
            $ARGS = [];
            if ($only_my_listings) {
                unset($_POST['agent_filter']);
                $ARGS['user_ID'] = $_SESSION['userID'];
            }
            //Set Default to show all listings, not just active.
            $ARGS['listingsdb_active'] = 'any';
            // show all the listings
            if (isset($_POST['filter'])) {
                if ($_POST['filter'] == 'active') {
                    $ARGS['listingsdb_active'] = 'yes';
                } elseif ($_POST['filter'] == 'inactive') {
                    $ARGS['listingsdb_active'] = 'no';
                } else {
                    $ARGS['listingsdb_active'] = 'any';
                }
                if ($_POST['filter'] == 'expired') {
                    $ARGS['listingsdb_expiration_less'] = time();
                }
                if ($_POST['filter'] == 'featured') {
                    $ARGS['featuredOnly'] = 'yes';
                }
                if ($_POST['filter'] == 'created_1week') {
                    $ARGS['listingsdb_creation_date_greater'] = date('Y-m-d', strtotime('-1 week'));
                }
                if ($_POST['filter'] == 'created_1month') {
                    $ARGS['listingsdb_creation_date_greater'] = date('Y-m-d', strtotime('-1 month'));
                }
                if ($_POST['filter'] == 'created_3month') {
                    $ARGS['listingsdb_creation_date_greater'] = date('Y-m-d', strtotime('-3 month'));
                }
            }

            if (isset($_POST['lookup_field']) && is_string($_POST['lookup_field']) && isset($_POST['lookup_value']) && $_POST['lookup_field'] != 'listingsdb_id' && $_POST['lookup_field'] != 'listingsdb_title' && $_POST['lookup_value'] != '') {
                $ARGS[$_POST['lookup_field']][] = $_POST['lookup_value'];
            }
            if (isset($_POST['lookup_field']) && isset($_POST['lookup_value']) && $_POST['lookup_field'] == 'listingsdb_title' && $_POST['lookup_value'] != '') {
                $ARGS['listingsdb_title'] = $_POST['lookup_value'];
            }
            if (isset($_POST['pclass_filter']) && is_numeric($_POST['pclass_filter']) && $_POST['pclass_filter'] > 0) {
                $ARGS['pclass'][] = $_POST['pclass_filter'];
            }
            if (isset($_POST['agent_filter']) && $_POST['agent_filter'] != '') {
                $ARGS['user_ID'] = intval($_POST['agent_filter']);
            }
            //Do Search to get total record count, no need pass in soring information
            try {
                $result = $listing_api->search(['parameters' => $ARGS, 'limit' => 0, 'offset' => 0, 'count_only' => true]);
            } catch (Exception $e) {
                $misc->logErrorAndDie($e->getMessage());
            }
            //echo '<pre>'.print_r($result,TRUE).'</pre>';die;
            $num_rows = $result['listing_count'];
            $max_pages = ceil($num_rows / $this->config['admin_listing_per_page']);
            if (!isset($_GET['cur_page']) || $_GET['cur_page'] == 0) {
                $_GET['cur_page'] = 0;
            } else {
                if (intval($_GET['cur_page']) + 1 > $max_pages || intval($_GET['cur_page']) < 0) {
                    header('HTTP/1.0 403 Forbidden');
                    $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
                    return $display;
                } else {
                    $_GET['cur_page'] = intval($_GET['cur_page']);
                }
            }
            $next_prev = '<div class="d-flex justify-content-center">' . $misc->nextPrev($num_rows, $_GET['cur_page'], '', true) . '</div>'; // put in the next/previous stuff

            $quick_edit = $this->showQuickEditBar($next_prev, $only_my_listings);
            // build the string to select a certain number of listings per page
            $limit_str = intval($_GET['cur_page'] * $this->config['admin_listing_per_page']);
            $listing_api = $this->newListingApi();
            try {
                $result = $listing_api->search(['parameters' => $ARGS, 'limit' => $this->config['admin_listing_per_page'], 'offset' => $limit_str /*,'sortby'=>$_GET['sortby'],'sorttype'=>$_GET['sorttype']*/]);
            } catch (Exception $e) {
                $misc->logErrorAndDie($e->getMessage());
            }

            //print_r($result);
            $count = 0;
            $display .= '';
            $page->replaceLangTemplateTags();
            $page->replaceTags();
            $addons = $page->loadAddons();
            $page->page = str_replace('{quick_edit_bar}', $quick_edit, $page->page);
            $listing_section = $page->getTemplateSection('listing_dataset');
            $listing = '';
            foreach ($result['listings'] as $listingID) {
                $listingID = intval($listingID);
                if ($listingID == null) {
                    continue;
                }
                // alternate the colors
                if ($count == 0) {
                    $count = $count + 1;
                } else {
                    $count = 0;
                }
                $listing .= $listing_section;
                // strip slashes so input appears correctly
                //echo $listingID;
                try {
                    $api_result = $listing_api->read(['listing_id' => $listingID, 'fields' => [
                        'listingsdb_pclass_id', 'listingsdb_title', 'listingsdb_notes',
                        'listingsdb_active', 'listingsdb_featured', 'listingsdb_mlsexport', 'userdb_id', 'listingsdb_last_modified', 'listingsdb_expiration', 'listingsdb_hit_count', 'listingsdb_creation_date',
                    ]]);
                } catch (Exception $e) {
                    $misc->logErrorAndDie($e->getMessage());
                }
                //echo '<pre>'.print_r($api_result,TRUE).'</pre>';
//                $title = $api_result['listing']['listingsdb_title'];
                $notes = is_string($api_result['listing']['listingsdb_notes']) ? $api_result['listing']['listingsdb_notes'] : '';
                $active = is_string($api_result['listing']['listingsdb_active']) ? $api_result['listing']['listingsdb_active'] : '';
                $last_modified = is_string($api_result['listing']['listingsdb_last_modified']) ? $api_result['listing']['listingsdb_last_modified'] : '';
                $expiration = is_string($api_result['listing']['listingsdb_expiration']) ? $api_result['listing']['listingsdb_expiration'] : '';
                $featured = is_string($api_result['listing']['listingsdb_featured']) ? $api_result['listing']['listingsdb_featured'] : '';
                $creationdate = is_string($api_result['listing']['listingsdb_creation_date']) ? $api_result['listing']['listingsdb_creation_date'] : '';

                $formatted_creationdate = date($this->config['date_format_timestamp'], strtotime($creationdate));
                //$email = $recordSet->fields('userdb_emailaddress');
                $last_modified = date('D M j G:i:s T Y', strtotime($last_modified));
                $formatted_expiration = date($this->config['date_format_timestamp'], (int)$expiration);
                $hit_count = is_int($api_result['listing']['listingsdb_hit_count']) ? $api_result['listing']['listingsdb_hit_count'] : 0;
                $active_raw = $active;
                $featured_raw = $featured;
                //Add filters to link
                if (isset($_POST['lookup_field']) && isset($_POST['lookup_value'])) {
                    $_GET['lookup_field'] = $_POST['lookup_field'];
                    $_GET['lookup_value'] = $_POST['lookup_value'];
                }
                if (isset($_GET['lookup_field']) && isset($_GET['lookup_value'])) {
                    $_POST['lookup_field'] = $_GET['lookup_field'];
                    $_POST['lookup_value'] = $_GET['lookup_value'];
                }

                $edit_link = $this->config['baseurl'] . '/admin/index.php?action=edit_listing&amp;edit=' . $listingID;
                $listing = $page->replaceListingFieldTags($listingID, $listing);
                $listing = $page->parseTemplateSection($listing, 'listingid', (string)$listingID);
                $listing = $page->parseTemplateSection($listing, 'edit_listing_link', $edit_link);
                $listing = $page->parseTemplateSection($listing, 'listing_last_modified', $last_modified);
                $listing = $page->parseTemplateSection($listing, 'listing_creation_date', $formatted_creationdate);
                $listing = $page->parseTemplateSection($listing, 'listing_active_status', $active_raw);
                $listing = $page->parseTemplateSection($listing, 'listing_featured_status', $featured_raw);
                $listing = $page->parseTemplateSection($listing, 'listing_expiration', $formatted_expiration);
                $listing = $page->parseTemplateSection($listing, 'listing_notes', $notes);
                $listing = $page->parseTemplateSection($listing, 'row_num_even_odd', (string)$count);
                $listing = $page->parseTemplateSection($listing, 'listing_hit_count', (string)$hit_count);
                $listing_url = $page->magicURIGenerator('listing', (string)$listingID, true);
                $listing = $page->parseTemplateSection($listing, 'listing_url', $listing_url);
                $addon_fields = $page->getAddonTemplateFieldList($addons);
                $listing = $page->parseAddonTags($listing, $addon_fields);
                if ($this->config['use_expiration']) {
                    $listing = $page->removeTemplateBlock('show_expiration', $listing);
                } else {
                    $listing = $page->cleanupTemplateBlock('show_expiration', $listing);
                }
            } // end while

            $page->replaceTemplateSection('listing_dataset', $listing);
            $page->replacePermissionTags();
            $display .= $page->returnPage();
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        $page->replaceTag('content', $display);
        $page->replaceTag('application_status_text', $status_text);
        $page->replaceLangTemplateTags();
        $page->replacePermissionTags();
        $page->autoReplaceTags('', true);
        return $page->returnPage();
    }

    public function showQuickEditBar(string $next_prev = '', bool $only_my_listings = true): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $display = '<div class="card card-frame  mb-4">
        <div class="card-body py-2">';


        $display .= '<form method="post" action="" class="row align-items-end mb-2 g-3">
                    <input type="hidden" name="token" value="' . $misc->generateCsrfToken() . '" />
                        <div class="order-2 order-md-1 col-5 col-md-auto">
                            <div class="input-group input-group-static">
                            <label for="lookup_fields" class="ms-0">' . $lang['listing_editor_lookup'] . '</label>
							<select id="lookup_fields" name="lookup_field"  class="form-control" aria-label="Lookup Field">
								<option value="listingsdb_id" ';

        if (isset($_POST['lookup_field']) && $_POST['lookup_field'] == 'listingsdb_id') {
            $display .= ' selected';
        }
        $display .= '>' . $lang['admin_listings_editor_listing_number'] . '</option>';

        $display .= '<option value="listingsdb_title" ';
        if (isset($_POST['lookup_field']) && $_POST['lookup_field'] == 'listingsdb_title') {
            $display .= ' selected';
        }
        $display .= '>' . $lang['admin_listings_editor_listing_title'] . '</option>';

        $sql = 'SELECT listingsformelements_field_name, listingsformelements_field_caption, listingsformelements_field_type
				FROM ' . $this->config['table_prefix'] . 'listingsformelements 
				WHERE listingsformelements_field_type != \'divider\'';
        $recordSet = $ORconn->execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        while (!$recordSet->EOF) {
            $field_name = $recordSet->fields('listingsformelements_field_name');
            $field_caption = $recordSet->fields('listingsformelements_field_caption');
            $display .= '<option value="' . $field_name . '" ';
            if (isset($_POST['lookup_field']) && $_POST['lookup_field'] == $field_name) {
                $display .= ' selected ';
            }
            $display .= '>' . $field_caption . '</option>';
            $recordSet->MoveNext();
        }
        $display .= '</select></div></div>
        <div class="order-3 order-md-2 col-4 col-md-auto">
        <div class="input-group input-group-outline">
        <label class="form-label">' . $lang['listing_editor_field_value'] . '</label>
        ';

        $display .= '<input name="lookup_value" type="text" class="form-control" value="';
        if (isset($_POST['lookup_value']) && is_scalar($_POST['lookup_value'])) {
            $display .= $_POST['lookup_value'];
        }
        $display .= '" autofocus></div></div>
        <div class="order-4 order-md-3 col-3 col-md-auto"><div class="input-group input-group-outline justify-content-end just-content-md-start">';

        $display .= '<button type="submit" class="btn btn-primary mt-auto mb-0"><i class="fa fa-search"></i><span class="d-none d-md-inline-block">' . $lang['search_button'] . '</span></button></div></div>
        <div class="order-1 col-12 order-md-4 col-md justify-content-end">
        <a class="btn btn-primary float-end mt-auto mb-0" id="add_listing_link" href="#!" role="button">
        <i class="fa-solid fa-plus"></i> 
        ' . $lang['admin_add_listing'] . '</a>
        </div>
					</form>
                    
                    <div class="row">
						<form name="listing_editor_filter_form" class="col-12 col-md-auto" method="post" action="">
                        <input type="hidden" name="token" value="' . $misc->generateCsrfToken() . '" />
                        <div class="row align-items-end">
                        <div class="col-8 col-md-auto">
                        <div class="input-group input-group-static">
                        <label for="filter" class="ms-0"
                          >' . $lang['listing_editor_show'] . '</label
                        >
                        <select
                          name="filter"
                          class="form-control"
                          id="filter"
                        >
                          <option value="" ';

        if (!isset($_POST['filter']) || $_POST['filter'] == '') {
            $display .= ' selected="selected" ';
        }
        $display .= '>' . $lang['listing_editor_show_all'] . '</option>
        <option value="active" ';

        if (isset($_POST['filter']) && $_POST['filter'] == 'active') {
            $display .= ' selected="selected" ';
        }
        $display .= '>' . $lang['listing_editor_active'] . '</option>';
        $display .= '<option value="inactive" ';
        if (isset($_POST['filter']) && $_POST['filter'] == 'inactive') {
            $display .= ' selected="selected" ';
        }
        $display .= '>' . $lang['listing_editor_inactive'] . '</option>';
        $display .= '<option value="expired" ';
        if (isset($_POST['filter']) && $_POST['filter'] == 'expired') {
            $display .= ' selected="selected" ';
        }
        $display .= '>' . $lang['listing_editor_expired'] . '</option>';
        $display .= '<option value="featured" ';
        if (isset($_POST['filter']) && $_POST['filter'] == 'featured') {
            $display .= ' selected="selected" ';
        }
        $display .= '>' . $lang['listing_editor_featured'] . '</option>';
        //This Weeks Listings
        $display .= '<option value="created_1week" ';
        if (isset($_POST['filter']) && $_POST['filter'] == 'created_1week') {
            $display .= ' selected="selected" ';
        }
        $display .= '>' . $lang['listing_editor_created_1week'] . '</option>';
        //This Month's Listings
        $display .= '<option value="created_1month" ';
        if (isset($_POST['filter']) && $_POST['filter'] == 'created_1month') {
            $display .= ' selected="selected" ';
        }
        $display .= '>' . $lang['listing_editor_created_1month'] . '</option>';
        //Last 3 Month's Listings
        $display .= '<option value="created_3month" ';
        if (isset($_POST['filter']) && $_POST['filter'] == 'created_3month') {
            $display .= ' selected="selected" ';
        }
        $display .= '>' . $lang['listing_editor_created_3month'] . '</option>';

        $display .= '	</select>
                        </div>
                        </div>
                        <div class="col-4 col-md-auto">
                            <div class="input-group input-group-outline">
                                <button type="submit" class="btn btn-primary mt-auto mb-0">
                                <i class="fa-solid fa-filter"></i><span class="d-none d-md-inline-block"> ' . $lang['listing_editor_filter'] . '</span>
                                </button>
                            </div>
                        </div>
                        </div>
					</form>
                    ';

        if (!$only_my_listings) {
            $display .= '
							<form name="listing_editor_agent_filter_form" method="post" class="col-12 col-md-auto" action="">
                            <input type="hidden" name="token" value="' . $misc->generateCsrfToken() . '" />
                            <div class="row align-items-end">
                                <div class="col-8 col-md-auto">
                                <div class="input-group input-group-static">
                        <label for="agent_filter" class="ms-0"
                          >' . $lang['listing_editor_show_agent'] . '</label
                        >
                        <select
                          name="agent_filter"
                          class="form-control"
                          id="agent_filter"
                        >
						<option value="" selected="selected">' . $lang['listing_editor_show_all'] . '</option>';

            $sql = 'SELECT userdb_id, userdb_user_first_name, userdb_user_last_name 
					FROM ' . $this->config['table_prefix'] . 'userdb
					WHERE userdb_is_agent = \'yes\'
					ORDER BY userdb_user_last_name,userdb_user_first_name';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }

            while (!$recordSet->EOF) {
                $agent_ID = $recordSet->fields('userdb_id');
                $agent_first_name = $recordSet->fields('userdb_user_first_name');
                $agent_last_name = $recordSet->fields('userdb_user_last_name');
                if (isset($_POST['agent_filter']) && $_POST['agent_filter'] == $agent_ID) {
                    $display .= '<option value="' . $agent_ID . '" selected="selected">' . $agent_last_name . ',' . $agent_first_name . '</option>';
                } else {
                    $display .= '<option value="' . $agent_ID . '">' . $agent_last_name . ',' . $agent_first_name . '</option>';
                }
                $recordSet->MoveNext();
            }

            $display .= '</select>
                </div>
                </div>
                <div class="col-4 col-md-auto">
                <button  type="submit" class="btn btn-primary mt-auto mb-0">
                <i class="fa-solid fa-filter"></i><span class="d-none d-md-inline-block"> ' . $lang['listing_editor_filter'] . '</span>
                </button>
                </div>
                </div>
					</form>';
        }

        $display .= '<form name="form1" method="post" class="col-12 col-md-auto" action="">
        <input type="hidden" name="token" value="' . $misc->generateCsrfToken() . '" />
        <div class="row align-items-end">
                                <div class="col-8 col-md-auto">
                                
        <div class="input-group input-group-static">
        <label for="pclass_filter" class="ms-0"
          >' . $lang['listing_editor_show_pclass'] . '</label
        >
        <select
          name="pclass_filter"
          class="form-control"
          id="pclass_filter"
        >';

        $sql2 = 'SELECT class_id,class_name 
					FROM ' . $this->config['table_prefix'] . 'class';
        $recordSet2 = $ORconn->execute($sql2);
        if (is_bool($recordSet2)) {
            $misc->logErrorAndDie($sql2);
        }

        $display .= '<option value="" selected="selected">' . $lang['listing_editor_show_all'] . '</option>';

        while (!$recordSet2->EOF) {
            $class_id = $recordSet2->fields('class_id');
            $class_name = $recordSet2->fields('class_name');
            if (isset($_POST['pclass_filter']) && $_POST['pclass_filter'] == $class_id) {
                $display .= '<option value="' . $class_id . '" selected="selected">' . $class_name . '</option>';
            } else {
                $display .= '<option value="' . $class_id . '">' . $class_name . '</option>';
            }
            $recordSet2->MoveNext();
        }

        $display .= '	</select>
        </div>
        </div>
        <div class="col-4 col-md-auto">
            <div class="input-group input-group-outline">
            <button  type="submit" class="btn btn-primary mt-auto mb-0">
                                <i class="fa-solid fa-filter"></i><span class="d-none d-md-inline-block"> ' . $lang['listing_editor_filter'] . '</span>
                                </button>
            </div>
        </div>
                    </div>
                        
					</form>
			</div>';

        if ($next_prev != '') {
            $display .= $next_prev;
        }
        $display .= '</div></div>';

        return $display;
    }

    public function updateListing(bool $verify_user = true): string
    {
        global $lang;

        $misc = $this->newMisc();
        $forms = $this->newForms();

        $listing_pages = $this->newListingPages();

        $page = $this->newPageAdmin();
        $display = '';
        // update the listing
        $sql_edit = intval($_POST['edit']);
        $listing_api = $this->newListingApi();
        try {
            $api_result = $listing_api->read(['listing_id' => $sql_edit, 'fields' => ['listingsdb_pclass_id']]);
        } catch (Exception $e) {
            $misc->logErrorAndDie($e->getMessage());
        }
        $edit_pclass = is_integer($api_result['listing']['listingsdb_pclass_id']) ? $api_result['listing']['listingsdb_pclass_id'] : 0;

        //$edit_pclass =intval($_POST['pclass']);
        if ($verify_user) {
            $listing_ownerID = $listing_pages->getListingAgentValue('userdb_id', $sql_edit);
            if (intval($_SESSION['userID']) != $listing_ownerID) {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => $lang['listing_editor_permission_denied']]) ?: '';
            }
        }
        //CSRF
        if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
        }
        if (!is_string($_POST['title']) || trim($_POST['title']) == '') {
            // if the title is blank
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => $lang['admin_new_listing_enter_a_title']]) ?: '';
        } // end if
        if (!isset($_POST['or_owner']) || !intval($_POST['or_owner']) > 0) {
            // if the title is blank
            header('Content-type: application/json');
            return json_encode(['error' => '1', 'error_msg' => $lang['listing_error_invalid_agent']]) ?: '';
        } else {
            $pass_the_form = $forms->validateForm('listingsformelements', [$edit_pclass]);
            if ($pass_the_form !== 'Yes') {
                // if we're not going to pass it, tell that they forgot to fill in one of the fields
                $error_msg = '';
                if (is_array($pass_the_form)) {
                    foreach ($pass_the_form as $k => $v) {
                        if ($v == 'REQUIRED') {
                            $error_msg .= "$k: $lang[required_fields_not_filled]<br />";
                        }
                        if ($v == 'TYPE') {
                            $error_msg .= "$k: $lang[field_type_does_not_match]<br/>";
                        }
                    }
                }
                if ($error_msg != '') {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $error_msg]) ?: '';
                }
                // $display .= "<p>$lang[required_fields_not_filled]</p>";
            }
            if ($pass_the_form == 'Yes') {
                $listing_details = [];
                $listing_details['title'] = trim($_POST['title']);
                $edit_seotitle = is_string($_POST['seotitle']) ? trim($_POST['seotitle']) : '';
                if ($edit_seotitle == '') {
                    $edit_seotitle = 'AUTO';
                }
                $listing_details['seotitle'] = $edit_seotitle;
                if (isset($_POST['edit_expiration'])) {
                    $listing_details['expiration'] = is_string($_POST['edit_expiration']) ? trim($_POST['edit_expiration']) : '';
                }
                if (isset($_POST['featured'])) {
                    if ($_POST['featured'] == 'yes') {
                        $listing_details['featured'] = true;
                    } else {
                        $listing_details['featured'] = false;
                    }
                }
                if (isset($_POST['edit_active'])) {
                    if ($_POST['edit_active'] == 'yes') {
                        $listing_details['active'] = true;
                    } else {
                        $listing_details['active'] = false;
                    }
                }
                if (isset($_POST['notes']) && is_string($_POST['notes'])) {
                    $listing_details['notes'] = trim($_POST['notes']);
                }

                $edit_or_owner = intval($_POST['or_owner']);

                $listing_api = $this->newListingApi();
                try {
                    $listing_api->update(['class_id' => $edit_pclass, 'listing_id' => $sql_edit, 'listing_details' => $listing_details, 'listing_agents' => [$edit_or_owner], 'listing_fields' => $_POST, 'listing_media' => []]);
                } catch (Exception $e) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
                }
                //Get Seo Title
                try {
                    $seoresult = $listing_api->read(['listing_id' => $sql_edit, 'fields' => ['listing_seotitle']]);
                } catch (Exception $e) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
                }
                $seotitle = $seoresult['listing']['listing_seotitle'];

                header('Content-type: application/json');
                $listing_url = $page->magicURIGenerator('listing', (string)$sql_edit, true);
                return json_encode(['error' => '0', 'listing_id' => $sql_edit, 'listing_seotitle' => $seotitle, 'listing_url' => $listing_url]) ?: '';
            } // end if $pass_the_form == "Yes"
        } // end else
        return $display;
    }
}
