<?php

declare(strict_types=1);

namespace OpenRealty;

use OpenRealty\Api\Commands\ListingApi;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\Writer\PngWriter;
use Exception;
use OpenRealty\Api\Commands\UserApi;

class ListingPages extends BaseClass
{
    public function listingView(): string
    {
        global $lang, $meta_canonical, $meta_index;

        $images = $this->newImageHandler();
        $display = '';

        $login = $this->newLogin();
        $show_inactive_listing = false;
        $is_admin = $login->verifyPriv('Admin');
        $listingId = is_numeric($_GET['listingID']) ? (int)$_GET['listingID'] : 0;
        if ($listingId > 0) {
            $listing_api = $this->newListingApi();
            try {
                $api_result = $listing_api->read(['listing_id' => $listingId, 'fields' => ['listingsdb_pclass_id', 'listingsdb_active', 'userdb_id']]);
            } catch (Exception) {
            }
            if (isset($api_result)) {
                $class_id = $api_result['listing']['listingsdb_pclass_id'];
                $listingdb_active = $api_result['listing']['listingsdb_active'];
                $listing_agent = $api_result['listing']['userdb_id'];
                if ($is_admin) {
                    $show_inactive_listing = true;
                } else {
                    if (isset($_SESSION['userID']) && $listing_agent == $_SESSION['userID']) {
                        $show_inactive_listing = true;
                    }
                }
                // first, check to see whether the listing is currently active
                if ($listingdb_active == 'yes' || $show_inactive_listing) {
                    $page = $this->newPageUser();
                    //Lookup Class

                    if (file_exists($this->config['template_path'] . '/listing_detail_pclass' . $class_id . '.html')) {
                        $page->loadPage($this->config['template_path'] . '/listing_detail_pclass' . $class_id . '.html');
                    } else {
                        $page->loadPage($this->config['template_path'] . '/' . $this->config['listing_template']);
                    }
                    $page->replaceCustomListingSearchBlock();
                    $sections = explode(',', $this->config['template_listing_sections']);
                    foreach ($sections as $section) {
                        $section = trim($section);
                        $replace = $this->renderTemplateArea($section, $listingId);
                        $page->replaceTag($section, $replace);
                    }
                    $page->replaceListingFieldTags($listingId);

                    // Check to see if listing owner is an admin only.
                    $is_admin = $this->getListingAgentAdminStatus($listingId);

                    if ($is_admin && $this->config['show_listedby_admin']) {
                        $page->page = $page->removeTemplateBlock('show_listed_by_admin', $page->page);
                        $page->page = $page->cleanupTemplateBlock('!show_listed_by_admin', $page->page);
                    } else {
                        $page->page = $page->cleanupTemplateBlock('show_listed_by_admin', $page->page);
                        $page->page = $page->removeTemplateBlock('!show_listed_by_admin', $page->page);
                    }
                    if ($this->config['show_next_prev_listing_page']) {
                        $next_prev = $this->listingNextPrev();
                        $page->page = str_replace('{next_prev}', $next_prev, $page->page);
                    } else {
                        $page->page = str_replace('{next_prev}', '', $page->page);
                    }
                    //Show Slideshow
                    if (str_contains($page->page, '{slideshow_thumbnail_group_block')) {
                        //old < v3.3 slideshow JS moved to slideshow template
                        //$jscript .= '';
                        $page->page = $images->renderListingsMainImageSlideShow($listingId, $page->page);
                    }
                    //Add Canonical Link
                    $meta_canonical = $page->magicURIGenerator('listing', (string)$listingId, true);
                    $display .= $page->returnPage();
                } else {
                    $meta_index = false;
                    $display .= $lang['this_listing_is_not_active'];
                }
            } else {
                //No listing match, so show the custom not found page template

                $page = $this->newPageUser();
                $page->loadPage($this->config['template_path'] . '/not_found.html');
                $display .= $page->returnPage();
                header('HTTP/1.0 404 Not Found');
            }
        }
        return $display;
    }

    public function listingNextPrev(): string
    {
        $page = $this->newPageUser();
        $display = '';
        $listingID = intval($_GET['listingID']);
        if (isset($_SESSION['OR_REFERRER_ACTION'])) {
            //print_r($_SESSION);
            if ($_SESSION['OR_REFERRER_ACTION'] != 'listingqrcode' && $_SESSION['OR_REFERRER_ACTION'] != 'searchresults' && $_SESSION['OR_REFERRER_ACTION'] != 'save_search' && $_SESSION['OR_REFERRER_ACTION'] != 'listingview' && $_SESSION['OR_REFERRER_ACTION'] != 'view_listing_image' && $_SESSION['OR_REFERRER_ACTION'] != 'calculator') {
                unset($_SESSION['results']);
            }
            if (isset($_SESSION['results'])) {
                $url = $_SESSION['searchstring'];
                $cur_page = $_SESSION['cur_page'];
                $url_with_page = $url . '&amp;cur_page=' . $cur_page;
                //$np_listings = $_SESSION['results'];
                //find current posistion in array
                $cur_key = array_search($listingID, $_SESSION['results']);
                $count = count($_SESSION['results']);

                if ($count > 1) {
                    //
                    //$page = $this->newPageUser();
                    $page->loadPage($this->config['template_path'] . '/listing_next_prev.html');
                    $page->replaceTag('count', (string)$count);
                    if ($cur_key !== 0) {
                        $first_url = $page->magicURIGenerator('listing', $_SESSION['results'][0], true);
                        //Get URL for Previous Listing
                        $previous_url = $page->magicURIGenerator('listing', $_SESSION['results'][$cur_key - 1], true);
                        $page->replaceTag('first_url', $first_url);
                        $page->replaceTag('previous_url', $previous_url);
                        $page->page = $page->cleanupTemplateBlock('!first_listing', $page->page);
                        $page->page = $page->removeTemplateBlock('first_listing', $page->page);
                    } else {
                        $page->page = $page->cleanupTemplateBlock('first_listing', $page->page);
                        $page->page = $page->removeTemplateBlock('!first_listing', $page->page);
                    }
                    if (!empty($url)) {
                        $page->replaceTag('refine_search_args', $url_with_page);
                        $page->replaceTag('save_search_args', $url);
                    } else {
                        $page->replaceTag('refine_search_args', '');
                        $page->replaceTag('save_search_args', '');
                    }
                    //Show Next Last Buttons if we are not on the last Listing
                    $cur_num = $cur_key + 1;
                    $page->replaceTag('cur_num', $cur_num);
                    if ($cur_key != $count - 1) {
                        //Get URL for Next Listing
                        $next_url = $page->magicURIGenerator('listing', $_SESSION['results'][$cur_key + 1], true);
                        //Get URL for Last Listing
                        $last_url = $page->magicURIGenerator('listing', $_SESSION['results'][$count - 1], true);
                        $page->replaceTag('next_url', $next_url);
                        $page->replaceTag('last_url', $last_url);
                        $page->page = $page->cleanupTemplateBlock('!last_listing', $page->page);
                        $page->page = $page->removeTemplateBlock('last_listing', $page->page);
                    } else {
                        $page->page = $page->cleanupTemplateBlock('last_listing', $page->page);
                        $page->page = $page->removeTemplateBlock('!last_listing', $page->page);
                    }
                    $display .= $page->returnPage();
                }
            }
        }
        return $display;
    }

    public function renderSingleListingItem(int $listingsdb_id, string $name, string $display_type = 'both'): string
    {
        // Display_type - Sets what should be returned.
        // both - Displays both the caption and the formated value
        // value - Displays just the formated value
        // rawvalue - Displays just the raw value
        // caption - Displays only the captions
        global $ORconn, $lang;
        $misc = $this->newMisc();
        $display = '';

        if ($listingsdb_id !== 0) {
            $name = $misc->makeDbExtraSafe($name);
            $sql = 'SELECT listingsdbelements_field_value, listingsformelements_id, listingsformelements_field_type, listingsformelements_field_caption
					FROM ' . $this->config['table_prefix'] . 'listingsdbelements, ' . $this->config['table_prefix'] . "listingsformelements
					WHERE ((listingsdb_id = $listingsdb_id) AND (listingsformelements_field_name = listingsdbelements_field_name)
					AND (listingsdbelements_field_name = $name))";
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            while (!$recordSet->EOF) {
                $field_value = $recordSet->fields('listingsdbelements_field_value');
                $field_type = $recordSet->fields('listingsformelements_field_type');
                $form_elements_id = $recordSet->fields('listingsformelements_id');
                if (!isset($_SESSION['users_lang'])) {
                    // Hold empty string for translation fields, as we are workgin with teh default lang
                    $field_caption = $recordSet->fields('listingsformelements_field_caption');
                } else {
                    $lang_sql = 'SELECT listingsformelements_field_caption
								FROM ' . $this->config['lang_table_prefix'] . 'listingsformelements
								WHERE listingsformelements_id = ' . $form_elements_id;
                    $lang_recordSet = $ORconn->Execute($lang_sql);
                    if (is_bool($lang_recordSet)) {
                        $misc->logErrorAndDie($lang_sql);
                    }
                    $field_caption = (string)$lang_recordSet->fields('listingsformelements_field_caption');
                }
                if ($field_type == 'divider') {
                    $display .= "<br /><strong>$field_caption</strong>";
                } elseif ($field_value != '') {
                    if ($display_type === 'both' || $display_type === 'caption') {
                        $display .= '<span class="field_caption">' . $field_caption . '</span>';
                    }
                    if ($display_type == 'both') {
                        $display .= ':&nbsp;';
                    }
                    if ($display_type === 'both' || $display_type === 'value') {
                        if ($field_type == 'select-multiple' or $field_type == 'option' or $field_type == 'checkbox') {
                            // handle field types with multiple options
                            // $display .= "<br /><strong>$field_caption</strong>";
                            $feature_index_list = explode('||', $field_value);
                            sort($feature_index_list);
                            $list_count = count($feature_index_list);
                            $l = 1;
                            foreach ($feature_index_list as $feature_list_item) {
                                if ($l < $list_count) {
                                    $display .= $feature_list_item;
                                    $display .= $this->config['feature_list_separator'];
                                    $l++;
                                } else {
                                    $display .= $feature_list_item;
                                }
                            } // end while
                        } elseif ($field_type == 'price') {
                            $money_amount = $misc->internationalNumFormat((float)$field_value, $this->config['number_decimals_price_fields']);
                            $display .= $misc->moneyFormats($money_amount);
                        } elseif ($field_type == 'number') {
                            $display .= $misc->internationalNumFormat((float)$field_value, $this->config['number_decimals_number_fields']);
                        } elseif ($field_type == 'url') {
                            $display .= '<a href="' . $field_value . '" onclick="window.open(this.href,\'_blank\',\'location=1,resizable=1,status=1,scrollbars=1,toolbar=1,menubar=1,noopener,noreferrer\');return false">' . $field_value . '</a>';
                        } elseif ($field_type == 'email') {
                            $display .= '<a href="mailto:' . $field_value . '">' . $field_value . '</a>';
                        } elseif ($field_type == 'text' or $field_type == 'textarea') {
                            if ($this->config['add_linefeeds']) {
                                $field_value = nl2br($field_value); //replace returns with <br />
                            } // end if
                            $display .= $field_value;
                        } elseif ($field_type == 'date') {
                            $field_value = $misc->convertTimestamp($field_value);
                            $display .= $field_value;
                        } else {
                            $display .= $field_value;
                        } // end else
                    }
                    if ($display_type === 'rawvalue') {
                        $display .= $field_value;
                    }
                } else {
                    if ($field_type == 'price' && $display_type !== 'rawvalue' && $this->config['zero_price']) {
                        $display .= $lang['call_for_price'];
                    } // end if
                } // end else
                $recordSet->MoveNext();
            } // end while
            return $display;
        } else {
            return '';
        }
    }

    public function renderTemplateArea(string $templateArea, int $listingID): string
    {
        // renders all the elements in a given template area on the listing pages
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $templateArea = $misc->makeDbExtraSafe($templateArea);
        $sql = 'SELECT listingsdbelements_field_value, listingsformelements_id, listingsformelements_field_type, listingsformelements_field_caption,listingsformelements_display_priv
				FROM ' . $this->config['table_prefix'] . 'listingsdbelements, ' . $this->config['table_prefix'] . 'listingsformelements
				WHERE ((' . $this->config['table_prefix'] . "listingsdbelements.listingsdb_id = $listingID)
				AND (listingsformelements_field_name = listingsdbelements_field_name)
				AND (listingsformelements_location = $templateArea))
				ORDER BY listingsformelements_rank ASC";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $display = '';
        while (!$recordSet->EOF) {
            $form_elements_id = (int)$recordSet->fields('listingsformelements_id');
            $field_type = (string)$recordSet->fields('listingsformelements_field_type');
            $display_priv = (int)$recordSet->fields('listingsformelements_display_priv');
            $field_value = (string)$recordSet->fields('listingsdbelements_field_value');
            if (!isset($_SESSION['users_lang'])) {
                // Hold empty string for translation fields, as we are workgin with teh default lang
                $field_caption = (string)$recordSet->fields('listingsformelements_field_caption');
            } else {
                $lang_sql = 'SELECT listingsformelements_field_caption
							FROM ' . $this->config['lang_table_prefix'] . "listingsformelements
							WHERE listingsformelements_id = $form_elements_id";
                $lang_recordSet = $ORconn->Execute($lang_sql);
                if (is_bool($lang_recordSet)) {
                    $misc->logErrorAndDie($lang_sql);
                }
                $field_caption = (string)$lang_recordSet->fields('listingsformelements_field_caption');
            }
            if ($display_priv == 1) {
                $display_status = $login->verifyPriv('Member');
            } elseif ($display_priv == 2) {
                $display_status = $login->verifyPriv('Agent');
            } elseif ($display_priv == 3) {
                $display_status = $login->verifyPriv('Admin');
            } else {
                $display_status = true;
            }
            if ($display_status === true) {
                if ($field_type == 'divider') {
                    $display .= '<br /><strong>' . $field_caption . '</strong>';
                } elseif ($field_value != '') {
                    if ($field_type == 'select-multiple' or $field_type == 'option' or $field_type == 'checkbox') {
                        // handle field types with multiple options
                        $display .= '<div class="multiple_options_caption">' . $field_caption . '</div>';
                        $feature_index_list = explode('||', $field_value);
                        sort($feature_index_list);
                        $list_count = count($feature_index_list);
                        $l = 1;
                        $display .= '<div class="multiple_options">';
                        $display .= '<ul>';
                        foreach ($feature_index_list as $feature_list_item) {
                            if ($l < $list_count) {
                                $display .= '<li>';
                                $display .= $feature_list_item;
                                $display .= $this->config['feature_list_separator'];
                                $display .= '</li>';
                                $l++;
                            } else {
                                $display .= '<li>';
                                $display .= $feature_list_item;
                                $display .= '</li>';
                            }
                        } // end while
                        $display .= '</ul>';
                        $display .= '</div>';
                        $display .= '<div class="clear"></div>';
                    } elseif ($field_type == 'price') {
                        $money_amount = $misc->internationalNumFormat((float)$field_value, $this->config['number_decimals_price_fields']);
                        $display .= "<strong>$field_caption</strong>: " . $misc->moneyFormats($money_amount);
                    } elseif ($field_type == 'number') {
                        $display .= "<strong>$field_caption</strong>: " . $misc->internationalNumFormat((float)$field_value, $this->config['number_decimals_number_fields']);
                    } elseif ($field_type == 'url') {
                        $display .= "<strong>$field_caption</strong>: <a href=\"$field_value\" onclick=\"window.open(this.href,'_blank','location=1,resizable=1,status=1,scrollbars=1,toolbar=1,menubar=1,noopener,noreferrer');return false\">$field_value</a>";
                    } elseif ($field_type == 'email') {
                        $display .= "<strong>$field_caption</strong>: <a href=\"mailto:$field_value\">$field_value</a>";
                    } elseif ($field_type == 'text' or $field_type == 'textarea') {
                        if ($this->config['add_linefeeds']) {
                            $field_value = nl2br($field_value); //replace returns with <br />
                        } // end if
                        $display .= '<strong>' . $field_caption . '</strong>: ' . $field_value;
                    } elseif ($field_type == 'date') {
                        if ($this->config['date_format'] == 1) {
                            $format = 'm/d/Y';
                        } elseif ($this->config['date_format'] == 2) {
                            $format = 'Y/d/m';
                        } else {
                            $format = 'd/m/Y';
                        }
                        $field_value = date($format, (int)$field_value);
                        $display .= '<strong>' . $field_caption . '</strong>: ' . $field_value;
                    } else {
                        $display .= '<strong>' . $field_caption . '</strong>: ' . $field_value;
                    } // end else
                    $display .= '<br />';
                } else {
                    if ($field_type == 'price' && $this->config['zero_price']) {
                        $display .= '<strong>' . $field_caption . '</strong>: ' . $lang['call_for_price'] . '<br />';
                    } // end if
                } // end else
            }
            $recordSet->MoveNext();
        } // end while
        return $display;
    }

    public function renderTemplateAreaNoCaption(string $templateArea, int $listingID): string
    {
        // renders all the elements in a given template area on the listing pages
        // this time without the corresponding captions
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $templateArea = $misc->makeDbExtraSafe($templateArea);
        $sql = 'SELECT listingsdbelements_field_value, listingsformelements_field_type, listingsformelements_field_caption, listingsformelements_display_priv
				FROM ' . $this->config['table_prefix'] . 'listingsdbelements, ' . $this->config['table_prefix'] . 'listingsformelements
				WHERE ((' . $this->config['table_prefix'] . "listingsdbelements.listingsdb_id = $listingID)
				AND (listingsformelements_field_name = listingsdbelements_field_name)
				AND (listingsformelements_location = $templateArea))
				ORDER BY listingsformelements_rank ASC";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $display = '';
        while (!$recordSet->EOF) {
            $field_value = $recordSet->fields('listingsdbelements_field_value');
            $field_type = $recordSet->fields('listingsformelements_field_type');
            $field_caption = $recordSet->fields('listingsformelements_field_caption');
            $display_priv = $recordSet->fields('listingsformelements_display_priv');
            if ($display_priv == 1) {
                $display_status = $login->verifyPriv('Member');
            } elseif ($display_priv == 2) {
                $display_status = $login->verifyPriv('Agent');
            } elseif ($display_priv == 3) {
                $display_status = $login->verifyPriv('Admin');
            } else {
                $display_status = true;
            }
            if ($display_status === true) {
                if ($field_value != '') {
                    if ($field_type == 'select-multiple' or $field_type == 'option' or $field_type == 'checkbox') {
                        // handle field types with multiple options
                        $feature_index_list = explode('||', $field_value);
                        sort($feature_index_list);
                        $list_count = count($feature_index_list);
                        $l = 1;
                        foreach ($feature_index_list as $feature_list_item) {
                            if ($l < $list_count) {
                                $display .= $feature_list_item;
                                $display .= $this->config['feature_list_separator'];
                                $l++;
                            } else {
                                $display .= $feature_list_item;
                            }
                        } // end while
                    } elseif ($field_type == 'price') {
                        $money_amount = $misc->internationalNumFormat($field_value, $this->config['number_decimals_price_fields']);
                        $display .= '<strong>' . $field_caption . '</strong>: ' . $misc->moneyFormats($money_amount);
                    } elseif ($field_type == 'number') {
                        $display .= "<strong>$field_caption</strong>: " . $misc->internationalNumFormat($field_value, $this->config['number_decimals_number_fields']);
                    } elseif ($field_type == 'url') {
                        $display .= "<a href=\"$field_value\" onclick=\"window.open(this.href,'_blank','location=1,resizable=1,status=1,scrollbars=1,toolbar=1,menubar=1,noopener,noreferrer');return false\">$field_value</a>";
                    } elseif ($field_type == 'email') {
                        $display .= "<a href=\"mailto:$field_value\">$field_value</a>";
                    } elseif ($field_type == 'text' or $field_type == 'textarea') {
                        if ($this->config['add_linefeeds']) {
                            $field_value = nl2br($field_value); //replace returns with <br />
                        } // end if
                        $display .= $field_value;
                    } elseif ($field_type == 'Date') {
                        $field_value = $misc->convertTimestamp($field_value);
                        $display .= $field_value;
                    } else {
                        $display .= $field_value;
                    } // end else
                    $display .= '<br />';
                } else {
                    if ($field_type == 'price' && $this->config['zero_price']) {
                        $display .= $lang['call_for_price'] . '<br />';
                    } // end if
                } // end else
            }
            $recordSet->MoveNext();
        } // end while
        return $display;
    }

    public function renderFeaturedListingsVertical(int $num_of_listings = 0, bool $random = false, int $pclass = 0, bool $latest = false, bool $popular = false): string
    {
        return $this->renderFeaturedListings($num_of_listings, 'vertical', $random, $pclass, $latest, $popular);
    }

    public function renderFeaturedListingsHorizontal(int $num_of_listings = 0, bool $random = false, int $pclass = 0, bool $latest = false, bool $popular = false): string
    {
        return $this->renderFeaturedListings($num_of_listings, 'horizontal', $random, $pclass, $latest, $popular);
    }

    public function renderFeaturedListings(int $num_of_listings = 0, string $template_name = '', bool $random = false, int $pclass = 0, bool $latest = false, bool $popular = false): string
    {
        global $ORconn, $current_ID;

        $misc = $this->newMisc();
        $page = $this->newPageUser();

        $display = '';
        //If We have a $current_ID save it
        $old_current_ID = '';
        if ($current_ID != '') {
            $old_current_ID = $current_ID;
        }
        //Get the number of listing to display by default, unless user specified an override in the template file.
        if ($num_of_listings == 0) {
            $num_of_listings = $this->config['num_featured_listings'];
        }
        $ARGS = [];
        $SORTBY = [];
        $SORTTYPE = [];
        if ($latest) {
            $SORTBY[] = 'listingsdb_id';
            $SORTTYPE[] = 'DESC';
        }
        if ($popular) {
            $SORTBY[] = 'listingsdb_hit_count';
            $SORTTYPE[] = 'DESC';
        }
        if ($random || $latest || $popular) {
            $SORTBY[] = 'random';
        } else {
            $SORTBY[] = 'random';
            $ARGS['featuredOnly'] = 'yes';
        }
        if ($pclass > 0) {
            $ARGS['pclass'][] = $pclass;
        }
        $listing_api = $this->newListingApi();
        try {
            $result = $listing_api->search(['parameters' => $ARGS, 'sortby' => $SORTBY, 'sorttype' => $SORTTYPE, 'limit' => $num_of_listings, 'offset' => 0, 'count_only' => false]);
        } catch (Exception $e) {
            $misc->logErrorAndDie($e->getMessage());
        }
        //echo '<pre>'.print_r($result,TRUE).'</pre>';
        $returned_num_listings = $result['listing_count'];
        if ($returned_num_listings >= 1) {
            //Load the Featured Listing Template specified in the Site Config unless a template was specified in the calling template tag.
            if ($template_name == '') {
                $misc->logErrorAndDie('Template Name Must Be Set');
            } else {
                if ($random) {
                    if ($pclass > 0) {
                        if (file_exists($this->config['template_path'] . '/random_listing_' . $template_name . '_' . $pclass . '.html')) {
                            $page->loadPage($this->config['template_path'] . '/random_listing_' . $template_name . '_' . $pclass . '.html');
                        } else {
                            $page->loadPage($this->config['template_path'] . '/random_listing_' . $template_name . '.html');
                        }
                    } else {
                        $page->loadPage($this->config['template_path'] . '/random_listing_' . $template_name . '.html');
                    }
                    $page->loadPage($this->config['template_path'] . '/random_listing_' . $template_name . '.html');
                } elseif ($latest) {
                    if ($pclass > 0) {
                        if (file_exists($this->config['template_path'] . '/latest_listing_' . $template_name . '_' . $pclass . '.html')) {
                            $page->loadPage($this->config['template_path'] . '/latest_listing_' . $template_name . '_' . $pclass . '.html');
                        } else {
                            $page->loadPage($this->config['template_path'] . '/latest_listing_' . $template_name . '.html');
                        }
                    } else {
                        $page->loadPage($this->config['template_path'] . '/latest_listing_' . $template_name . '.html');
                    }
                    $page->loadPage($this->config['template_path'] . '/latest_listing_' . $template_name . '.html');
                } elseif ($popular) {
                    if ($pclass > 0) {
                        if (file_exists($this->config['template_path'] . '/popular_listing_' . $template_name . '_' . $pclass . '.html')) {
                            $page->loadPage($this->config['template_path'] . '/popular_listing_' . $template_name . '_' . $pclass . '.html');
                        } else {
                            $page->loadPage($this->config['template_path'] . '/popular_listing_' . $template_name . '.html');
                        }
                    } else {
                        $page->loadPage($this->config['template_path'] . '/popular_listing_' . $template_name . '.html');
                    }
                    $page->loadPage($this->config['template_path'] . '/popular_listing_' . $template_name . '.html');
                } else {
                    if ($pclass > 0) {
                        if (file_exists($this->config['template_path'] . '/featured_listing_' . $template_name . '_' . $pclass . '.html')) {
                            $page->loadPage($this->config['template_path'] . '/featured_listing_' . $template_name . '_' . $pclass . '.html');
                        } else {
                            $page->loadPage($this->config['template_path'] . '/featured_listing_' . $template_name . '.html');
                        }
                    } else {
                        $page->loadPage($this->config['template_path'] . '/featured_listing_' . $template_name . '.html');
                    }
                }
            }

            $page->replaceCustomListingSearchBlock();
            // Determine if the template uses rows.
            // First item in array is the row conent second item is the number of block per block row
            $featured_template_row = $page->getTemplateSectionRow('featured_listing_block_row');
            $new_row_data = [];
            if (isset($featured_template_row[0]) && isset($featured_template_row[1])) {
                $row = $featured_template_row[0];
                $col_count = $featured_template_row[1];
                $user_rows = true;
                $x = 1;
            } else {
                $user_rows = false;
                $col_count = 0;
                $x = 0;
                $row = '';
            }
            $featured_template_section = '';
            foreach ($result['listings'] as $listing) {
                if ($user_rows && $x > $col_count) {
                    //We are at then end of a row. Save the template section as a new row.
                    $new_row_data[] = $page->replaceTemplateSection('featured_listing_block', $featured_template_section, $row);
                    //$new_row_data[] = $featured_template_section;
                    $featured_template_section = $page->getTemplateSection('featured_listing_block');
                    $x = 1;
                } else {
                    $featured_template_section .= $page->getTemplateSection('featured_listing_block');
                }

                $current_ID = $listing;
                $featured_url = $page->magicURIGenerator('listing', (string)$current_ID, true);
                $featured_template_section = $page->replaceListingFieldTags($current_ID, $featured_template_section);
                $featured_template_section = $page->parseTemplateSection($featured_template_section, 'featured_url', $featured_url);
                $featured_template_section = $page->parseTemplateSection($featured_template_section, 'listingid', (string)$current_ID);
                // Start {isfavorite} featured template section tag
                if (isset($_SESSION['userID'])) {
                    $userID = intval($_SESSION['userID']);
                    $sql1 = 'SELECT listingsdb_id 
							FROM ' . $this->config['table_prefix'] . "userfavoritelistings 
							WHERE ((listingsdb_id = $current_ID) 
							AND (userdb_id=$userID))";
                    $recordSet1 = $ORconn->Execute($sql1);
                    if (is_bool($recordSet1)) {
                        $misc->logErrorAndDie($sql1);
                    }
                    $favorite_listingsdb_id = $recordSet1->fields('listingsdb_id');
                    if ($favorite_listingsdb_id !== $current_ID) {
                        $isfavorite = 'no';
                        $featured_template_section = $page->parseTemplateSection($featured_template_section, 'isfavorite', $isfavorite);
                    } else {
                        $isfavorite = 'yes';
                        $featured_template_section = $page->parseTemplateSection($featured_template_section, 'isfavorite', $isfavorite);
                    }
                }
                // End {isfavorite} featured template section tag
                // Setup Image Tags
                $sql2 = 'SELECT listingsimages_thumb_file_name,listingsimages_file_name 
						FROM ' . $this->config['table_prefix'] . "listingsimages 
						WHERE (listingsdb_id = $current_ID) 
						ORDER BY listingsimages_rank";
                $recordSet2 = $ORconn->SelectLimit($sql2, 1, 0);
                if ($recordSet2->RecordCount() > 0) {
                    $thumb_file_name = $recordSet2->fields('listingsimages_thumb_file_name');
                    $file_name = $recordSet2->fields('listingsimages_file_name');
                    if (str_starts_with($thumb_file_name, 'http://') || str_starts_with($thumb_file_name, 'https://') || str_starts_with($thumb_file_name, '//')) {
                        $featured_thumb_src = $thumb_file_name;
                        $featured_thumb_width = $this->config['thumbnail_width'];
                        $featured_thumb_height = $this->config['thumbnail_height'];
                        $featured_width = $this->config['main_image_width'];
                        $featured_height = $this->config['main_image_height'];
                        $featured_src = $file_name;
                    } elseif ($thumb_file_name != '' && file_exists($this->config['listings_upload_path'] . '/' . $thumb_file_name)) {
                        // gotta grab the thumbnail image size
                        $imagedata = GetImageSize($this->config['listings_upload_path'] . "/$thumb_file_name");
                        $imagewidth = $imagedata[0];
                        $imageheight = $imagedata[1];
                        $shrinkage = $this->config['thumbnail_width'] / $imagewidth;
                        $featured_thumb_width = $imagewidth * $shrinkage;
                        $featured_thumb_height = $imageheight * $shrinkage;
                        $featured_thumb_src = $this->config['listings_view_images_path'] . '/' . $thumb_file_name;
                        // gotta grab the thumbnail image size
                        $imagedata = GetImageSize($this->config['listings_upload_path'] . "/$file_name");
                        $imagewidth = $imagedata[0];
                        $imageheight = $imagedata[1];
                        $featured_width = $imagewidth;
                        $featured_height = $imageheight;
                        $featured_src = $this->config['listings_view_images_path'] . '/' . $file_name;
                    } else {
                        if ($this->config['show_no_photo']) {
                            $imagedata = GetImageSize('images/nophoto.gif');
                            $imagewidth = $imagedata[0];
                            $imageheight = $imagedata[1];
                            $shrinkage = $this->config['thumbnail_width'] / $imagewidth;
                            $featured_thumb_width = $imagewidth * $shrinkage;
                            $featured_thumb_height = $imageheight * $shrinkage;
                            $featured_thumb_src = $this->config['baseurl'] . '/images/nophoto.gif';
                            $featured_width = $featured_thumb_width;
                            $featured_height = $featured_thumb_height;
                            $featured_src = $this->config['baseurl'] . '/images/nophoto.gif';
                        } else {
                            $featured_thumb_width = '';
                            $featured_thumb_height = '';
                            $featured_thumb_src = '';
                            $featured_width = '';
                            $featured_height = '';
                            $featured_src = '';
                        }
                    }
                } else {
                    if ($this->config['show_no_photo']) {
                        $imagedata = GetImageSize('images/nophoto.gif');
                        $imagewidth = $imagedata[0];
                        $imageheight = $imagedata[1];
                        $shrinkage = $this->config['thumbnail_width'] / $imagewidth;
                        $featured_thumb_width = $imagewidth * $shrinkage;
                        $featured_thumb_height = $imageheight * $shrinkage;
                        $featured_thumb_src = $this->config['baseurl'] . '/images/nophoto.gif';
                        $featured_width = $featured_thumb_width;
                        $featured_height = $featured_thumb_height;
                        $featured_src = $this->config['baseurl'] . '/images/nophoto.gif';
                    } else {
                        $featured_thumb_width = '';
                        $featured_thumb_height = '';
                        $featured_thumb_src = '';
                        $featured_width = '';
                        $featured_height = '';
                        $featured_src = '';
                    }
                }
                if (!empty($featured_thumb_src)) {
                    $featured_template_section = $page->parseTemplateSection($featured_template_section, 'featured_thumb_src', $featured_thumb_src);
                    $featured_template_section = $page->parseTemplateSection($featured_template_section, 'featured_thumb_height', (string)$featured_thumb_height);
                    $featured_template_section = $page->parseTemplateSection($featured_template_section, 'featured_thumb_width', (string)$featured_thumb_width);
                    $featured_template_section = $page->cleanupTemplateBlock('featured_img', $featured_template_section);
                } else {
                    $featured_template_section = $page->removeTemplateBlock('featured_img', $featured_template_section);
                }
                if (!empty($featured_src)) {
                    $featured_template_section = $page->parseTemplateSection($featured_template_section, 'featured_large_src', $featured_src);
                    $featured_template_section = $page->parseTemplateSection($featured_template_section, 'featured_large_height', (string)$featured_height);
                    $featured_template_section = $page->parseTemplateSection($featured_template_section, 'featured_large_width', (string)$featured_width);
                    $featured_template_section = $page->cleanupTemplateBlock('featured_img_large', $featured_template_section);
                } else {
                    $featured_template_section = $page->removeTemplateBlock('featured_img_large', $featured_template_section);
                }
                if ($user_rows) {
                    $x++;
                }
            }
            if ($user_rows) {
                $featured_template_section = $page->cleanupTemplateBlock('featured_listing', $featured_template_section);
                $new_row_data[] = $page->replaceTemplateSection('featured_listing_block', $featured_template_section, $row);
                $replace_row = implode('', $new_row_data);
                $page->replaceTemplateSectionRow('featured_listing_block_row', $replace_row);
            } else {
                $page->replaceTemplateSection('featured_listing_block', $featured_template_section);
            }
            $page->replacePermissionTags();
            $page->autoReplaceTags();
            $display .= $page->returnPage();
        }
        $current_ID = '';
        if ($old_current_ID != '') {
            $current_ID = $old_current_ID;
        }
        return $display;
    }

    /**
     * get pclass (name)
     *
     * @param integer $listingsdb_id
     *
     * @return string
     */
    public function getPClass(int $listingsdb_id): string
    {
        $misc = $this->newMisc();
        $pclass_id = $this->getListingSingleValue('listingsdb_pclass_id', $listingsdb_id);

        $pclass_api = $this->newPClassApi();
        try {
            $api_result = $pclass_api->read(['class_id' => intval($pclass_id)]);
        } catch (Exception $e) {
            $misc->logErrorAndDie($e->getMessage());
        }
        return $api_result['class_name'];
    }

    /**
     * get creation date
     *
     * @param integer $listingsdb_id
     *
     * @return  string $listingsdb_creation_date
     */
    public function getCreationDate(int $listingsdb_id): string
    {
        $listingsdb_creation_date = $this->getListingSingleValue('listingsdb_creation_date', $listingsdb_id);
        return date($this->config['date_format_timestamp'], strtotime($listingsdb_creation_date));
    }

    /**
     * get Modified date
     *
     * @param integer $listingsdb_id
     *
     * @return  string $listingsdb_last_modified
     */
    public function getModifiedDate(int $listingsdb_id): string
    {
        $listingsdb_last_modified = $this->getListingSingleValue('listingsdb_last_modified', $listingsdb_id);
        return date($this->config['date_format_timestamp'], strtotime($listingsdb_last_modified));
    }

    /**
     * get listing seotitle
     *
     * @param integer $listingsdb_id
     *
     * @return  string $listing_seotitle
     */
    public function getListingSeoTitle(int $listingsdb_id): string
    {
        return $this->getListingSingleValue('listing_seotitle', $listingsdb_id);
    }

    /**
     * get Agent Listings Link
     *
     * @param integer $listingsdb_id
     *
     * @return  string $display
     */
    public function getAgentListingsLink(int $listingsdb_id): string
    {
        global $lang;
        // get the main data for a given listing
        $userdb_id = $this->getListingAgentValue('userdb_id', $listingsdb_id);
        return '<a href="' . $this->config['baseurl'] . '/index.php?action=searchresults&amp;user_ID=' . $userdb_id . '">' . $lang['user_listings_link_text'] . '</a>';
    }

    /**
     * get Listing Agent Thumbnail
     *
     * @param integer $listingsdb_id
     * @return string[]
     */
    public function getListingAgentThumbnail(int $listingsdb_id): array
    {
        global $lang;
        $misc = $this->newMisc();
        $listing_agent_thumbnail = [];
        if ($listingsdb_id !== 0) {
            $userdb_id = (int)$this->getListingSingleValue('userdb_id', $listingsdb_id);

            $media_api = $this->newMediaApi();
            try {
                $result = $media_api->read([
                    'media_type' => 'userimages',
                    'media_parent_id' => $userdb_id,
                    'media_output' => 'URL',
                ]);
            } catch (Exception $e) {
                $misc->logErrorAndDie($e->getMessage());
            }
            foreach ($result['media_object'] as $obj) {
                if (isset($obj['thumb_file_name'])) {
                    $thumb_file_name = $obj['thumb_file_name'];
                    $caption = $obj['caption'];
                    $listing_agent_thumbnail[] = '<img src="' . $this->config['user_view_images_path'] . '/' . $thumb_file_name . '" alt="' . $caption . '" />';
                }
            } // end while
        } else {
            $listing_agent_thumbnail[0] = '<img src="' . $this->config['baseurl'] . '/images/nophoto.gif" alt="' . $lang['no_photo'] . '" />';
        }
        return $listing_agent_thumbnail;
    }

    /**
     * get Listing Agent Admin Status (replaces several get_Listing_xx() functions)
     *
     * @param integer $listingsdb_id
     *
     * @return  boolean
     */
    public function getListingAgentAdminStatus(int $listingsdb_id): bool
    {
        $misc = $this->newMisc();
        $listing_api = $this->newListingApi();
        try {
            $result = $listing_api->read(['listing_id' => $listingsdb_id, 'fields' => ['userdb_id']]);
        } catch (Exception $e) {
            $misc->logErrorAndDie($e->getMessage());
        }

        $userdb_id = is_numeric($result['listing']['userdb_id']) ? (int)$result['listing']['userdb_id'] : 0;


        $is_admin = $misc->getAdminStatus($userdb_id);
        $is_agent = $misc->getAgentStatus($userdb_id);

        if ($is_admin && !$is_agent) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * pclass link
     *
     * @param integer $listingsdb_id
     *
     * @return  string $pclass_link
     */
    public function pClassLink(int $listingsdb_id): string
    {
        $misc = $this->newMisc();
        $listing_api = $this->newListingApi();
        try {
            $api_result = $listing_api->read(['listing_id' => $listingsdb_id, 'fields' => ['listingsdb_pclass_id']]);
        } catch (Exception $e) {
            $misc->logErrorAndDie($e->getMessage());
        }
        $class_id = $api_result['listing']['listingsdb_pclass_id'];
        $pclass_api = $this->newPClassApi();
        try {
            $api_result = $pclass_api->read(['class_id' => intval($class_id)]);
        } catch (Exception $e) {
            $misc->logErrorAndDie($e->getMessage());
        }
        $class_name = $api_result['class_name'];
        return '<a href="' . $this->config['baseurl'] . '/index.php?action=searchresults&amp;pclass[]=' . $class_id . '" title="' . $class_name . '">' . $class_name . '</a>';
    }

    /**
     * create create yahoo school link link
     * (Now greatschools.org)
     *
     * @param string $url_only
     *
     * @return  string $display
     */
    public function createYahooSchoolLink(string $url_only = 'no'): string
    {
        global $ORconn, $lang;
        $misc = $this->newMisc();
        $display = '';
        $sql_listingID = intval($_GET['listingID']);

        $city_field = $this->config['map_city'];
        $state_field = $this->config['map_state'];
        $zip_field = $this->config['map_zip'];

        $sql_city_field = $misc->makeDbSafe($city_field);
        $sql = 'SELECT listingsdbelements_field_value, listingsformelements_field_type, listingsformelements_field_caption 
				FROM ' . $this->config['table_prefix'] . 'listingsdbelements, ' . $this->config['table_prefix'] . 'listingsformelements 
				WHERE ((' . $this->config['table_prefix'] . "listingsdbelements.listingsdb_id = $sql_listingID) 
				AND (listingsformelements_field_name = listingsdbelements_field_name) 
				AND (listingsdbelements_field_name = $sql_city_field))";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $city = '';
        while (!$recordSet->EOF) {
            $city = $recordSet->fields('listingsdbelements_field_value');
            $recordSet->MoveNext();
        } // end while
        //Get State

        $sql_state_field = $misc->makeDbSafe($state_field);
        $sql = 'SELECT listingsdbelements_field_value, listingsformelements_field_type, listingsformelements_field_caption 
				FROM ' . $this->config['table_prefix'] . 'listingsdbelements, ' . $this->config['table_prefix'] . 'listingsformelements 
				WHERE ((' . $this->config['table_prefix'] . "listingsdbelements.listingsdb_id = $sql_listingID) 
				AND (listingsformelements_field_name = listingsdbelements_field_name) 
				AND (listingsdbelements_field_name = $sql_state_field))";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $state = '';
        while (!$recordSet->EOF) {
            $state = $recordSet->fields('listingsdbelements_field_value');
            $recordSet->MoveNext();
        } // end while

        //Get Zip
        $sql_zip_field = $misc->makeDbSafe($zip_field);
        $sql = 'SELECT listingsdbelements_field_value, listingsformelements_field_type, listingsformelements_field_caption 
				FROM ' . $this->config['table_prefix'] . 'listingsdbelements, ' . $this->config['table_prefix'] . 'listingsformelements 
				WHERE ((' . $this->config['table_prefix'] . "listingsdbelements.listingsdb_id = $sql_listingID) 
				AND (listingsformelements_field_name = listingsdbelements_field_name) 
				AND (listingsdbelements_field_name = $sql_zip_field))";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $zip = '';
        while (!$recordSet->EOF) {
            $zip = $recordSet->fields('listingsdbelements_field_value');
            $recordSet->MoveNext();
        } // end while
        //Build URL
        if ($city != '' && ($state != '' || $zip != '')) {
            if ($url_only == 'no') {
                $display = '<a href="https://www.greatschools.org/search/search.page?locationType=postal_code&amp;zipCode=' . $zip . '&amp;city=' . $city . '&amp;state=' . $state . '" onclick="window.open(this.href,\'_school\',\'location=0,status=0,scrollbars=1,toolbar=0,menubar=0,resizable=1,noopener,noreferrer\');return false">' . $lang['school_profile'] . '</a>';
            } else {
                $display = 'https://www.greatschools.org/cgi-bin/byaddr/pa?biid=&amp;zip=' . $zip . '&amp;city' . $city . '&amp;stateselect=' . $state;
            }
        }
        return $display;
    }


    /**
     * create bestplaces neighborhood link
     *
     * @param string $url_only
     *
     * @return  string $display
     */
    public function createBestPlacesNeighborhoodLink(string $url_only = 'no'): string
    {
        global $lang;

        $listingsdb_id = intval($_GET['listingID']);
        $display = '';
        $city = $this->getListingSingleValue($this->config['map_city'], $listingsdb_id);
        $zip = $this->getListingSingleValue($this->config['map_zip'], $listingsdb_id);

        if ($zip != '') {
            if ($url_only == 'no') {
                $display = '<a href="https://www.bestplaces.net/search/?q=' . $zip . '" onclick="window.open(this.href,\'_neighborhood\',\'location=0,status=0,scrollbars=1,toolbar=0,menubar=0,resizable=1,noopener,noreferrer\');return false">' . $lang['neighborhood_profile'] . '</a>';
            } else {
                $display = 'https://www.bestplaces.net/search/?q=' . $zip;
            }
        } elseif ($city != '') {
            if ($url_only == 'no') {
                $display = '<a href="https://www.bestplaces.net/search/?q=' . $city . '" onclick="window.open(this.href,\'_neighborhood\',\'location=0,status=0,scrollbars=1,toolbar=0,menubar=0,resizable=1,noopener,noreferrer\');return false">' . $lang['neighborhood_profile'] . '</a>';
            } else {
                $display = 'https://www.bestplaces.net/search/?q=' . $city;
            }
        }
        return $display;
    }

    /**
     * create printer friendly link
     *
     * @param string $url_only
     *
     * @return  string $display
     */
    public function createEmailFriendLink(string $url_only = 'no'): string
    {
        global $lang;

        if (isset($_GET['listingID'])) {
            $listingsdb_id = intval($_GET['listingID']);

            if ($url_only == 'no') {
                return '<a href="' . $this->config['baseurl'] . '/index.php?action=contact_friend&amp;popup=yes&amp;listing_id=' . $listingsdb_id . '" onclick="window.open(this.href,\'_blank\',\'location=0,status=0,scrollbars=1,toolbar=0,menubar=0,width=500,height=520\');return false">' . $lang['email_listing_link'] . '</a>';
            } else {
                return $this->config['baseurl'] . '/index.php?action=contact_friend&amp;popup=yes&amp;listing_id=' . $listingsdb_id;
            }
        }
        return '';
    }

    /**
     * create printer friendly link
     *
     * @param string $url_only
     *
     * @return  string $display
     */
    public function createPrinterFriendlyLink(string $url_only = 'no'): string
    {
        global $lang;
        if (isset($_GET['listingID'])) {
            $listingsdb_id = intval($_GET['listingID']);
            if ($url_only == 'no') {
                $display = '<a href="' . $this->config['baseurl'] . '/index.php?action=listingview&amp;listingID=' . $listingsdb_id . '&amp;printer_friendly=yes" rel="nofollow">' . $lang['printer_version_link'] . '</a>';
            } else {
                $display = $this->config['baseurl'] . '/index.php?action=listingview&amp;listingID=' . $listingsdb_id . '&amp;printer_friendly=yes';
            }
        } else {
            // Save GET
            $guidestring = '';
            foreach ($_GET as $k => $v) {
                if ($v && is_string($k) && $k != 'PHPSESSID' && $k != 'printer_friendly') {
                    if (is_array($v)) {
                        foreach ($v as $vitem) {
                            if (is_string($vitem)) {
                                $guidestring .= '&amp;' . urlencode($k) . '[]=' . urlencode($vitem);
                            }
                        }
                    } else {
                        $guidestring .= '&amp;' . urlencode($k) . '=' . urlencode($v);
                    }
                }
            }
            if ($url_only == 'no') {
                $display = '<a href="' . $this->config['baseurl'] . '/index.php?' . $guidestring . '&amp;printer_friendly=yes" rel="nofollow">' . $lang['printer_version_link'] . '</a>';
            } else {
                $display = $this->config['baseurl'] . '/index.php?' . $guidestring . '&amp;printer_friendly=yes';
            }
        }
        return $display;
    }

    /**
     * create calc link
     *
     * @param string $url_only
     *
     * @return  string $display
     */
    public function createCalcLink(string $url_only = 'no'): string
    {
        global $lang;
        if ($url_only == 'no') {
            if (isset($_GET['listingID'])) {
                $listingsdb_id = intval($_GET['listingID']);
                $display = '<a href="' . $this->config['baseurl'] . '/index.php?action=calculator&amp;popup=yes&amp;price=' . $this->renderSingleListingItem($listingsdb_id, $this->config['price_field'], 'rawvalue') . '" onclick="window.open(this.href,\'_blank\',\'location=0,status=0,scrollbars=1,toolbar=0,menubar=0,width=620,height=560\');return false" rel="nofollow">' . $lang['mortgage_calculator_link'] . '</a>';
            } else {
                $display = '<a href="' . $this->config['baseurl'] . '/index.php?action=calculator&amp;popup=yes" onclick="window.open(this.href,\'_blank\',\'location=0,status=0,scrollbars=1,toolbar=0,menubar=0,width=620,height=560\');return false" rel="nofollow">' . $lang['mortgage_calculator_link'] . '</a>';
            }
        } else {
            if (isset($_GET['listingID'])) {
                $listingsdb_id = intval($_GET['listingID']);
                $display = $this->config['baseurl'] . '/index.php?action=calculator&amp;popup=yes&amp;price=' . $this->renderSingleListingItem($listingsdb_id, $this->config['price_field'], 'rawvalue');
            } else {
                $display = $this->config['baseurl'] . '/index.php?action=calculator&amp;popup=yes';
            }
        }
        return $display;
    }

    /**
     * create add favorite link
     *
     * @param string $url_only
     *
     * @return  string $display
     */
    public function createAddFavoriteLink(string $url_only = 'no'): string
    {
        global $lang, $current_ID;

        if ($current_ID != '') {
            $_GET['listingID'] = intval($current_ID);
        }
        if ($url_only == 'no') {
            $listingsdb_id = intval($_GET['listingID']);
            $display = '<a href="' . $this->config['baseurl'] . '/index.php?action=addtofavorites&amp;listingID=' . $listingsdb_id . '" rel="nofollow">' . $lang['add_favorites_link'] . '</a>';
        } else {
            $listingsdb_id = intval($_GET['listingID']);
            $display = $this->config['baseurl'] . '/index.php?action=addtofavorites&amp;listingID=' . $listingsdb_id;
        }
        return $display;
    }

    /**
     * qr code link
     *
     * @param integer $listingsdb_id
     *
     * @return  string $display
     */
    public function qrCodeLink(int $listingsdb_id): string
    {
        return $this->config['baseurl'] . '/index.php?action=listingqrcode&listing_id=' . $listingsdb_id;
    }

    /**
     * qr code (generate)
     *
     * @param integer $listingsdb_id
     */
    public function qrCode(int $listingsdb_id): void
    {
        $page = $this->newPageUser();
        $writer = new PngWriter();
        $listingURL = $page->magicURIGenerator('listing', (string)$listingsdb_id, true);
        $qrCode = QrCode::create($listingURL)
            ->setEncoding(new Encoding('UTF-8'));
        try {
            $result = $writer->write($qrCode);
            header('Content-Type: ' . $result->getMimeType());
            echo $result->getString();
        } catch (Exception) {
            echo "";
        }
    }

    /**
     * contact agent link
     *
     * @param string $url_only
     *
     * @return  string $display
     */
    public function contactAgentLink(string $url_only = 'no'): string
    {
        global $lang;


        $page = $this->newPageUser();
        $listingsdb_id = intval($_GET['listingID']);
        $url = $page->magicURIGenerator('contact_listing_agent', (string)$listingsdb_id, true);
        if ($url_only == 'no') {
            $display = '<a href="' . $url . '" >' . $lang['contact_agent'] . '</a>';
        } else {
            $display = $url;
        }
        return $display;
    }

    /**
     * edit listing link
     *
     * @param string $url_only
     *
     * @return  string $display
     */
    public function editListingLink(string $url_only = 'no'): string
    {
        global $lang, $current_ID;
        $display = '';
        //Get the listing ID
        if ($current_ID != '') {
            $_GET['listingID'] = $current_ID;
        }
        if (isset($_GET['listingID'])) {
            $listingsdb_id = intval($_GET['listingID']);

            $listingagentid = $this->getListingAgentValue('userdb_id', $listingsdb_id);

            if (isset($_SESSION['userID'])) {
                $userid = $_SESSION['userID'];
                if ((isset($_SESSION['edit_all_listings']) && $_SESSION['edit_all_listings'] == 'yes') || (isset($_SESSION['admin_privs']) && $_SESSION['admin_privs'] == 'yes') || (isset($_SESSION['isAgent']) && $_SESSION['isAgent'] == 'yes' && ($listingagentid == $userid))) {
                    $edit_link = $this->config['baseurl'] . '/admin/index.php?action=edit_listing&amp;edit=' . $listingsdb_id;
                } else {
                    return '';
                }
                if ($url_only == 'yes') {
                    $display = $edit_link;
                } else {
                    $display = '<a href="' . $edit_link . '">' . $lang['edit_listing'] . '</a>';
                }
            }
        }
        return $display;
    }

    /**
     * get featured (status)
     *
     * @param integer $listingsdb_id
     */
    public function getFeatured(int $listing_id, string $raw): string
    {
        $listingsdb_id = $listing_id;
        $featured = $this->getListingSingleValue('listingsdb_featured', $listingsdb_id);
        if ($raw == 'no') {
            if ($featured == 'yes') {
                $featured = 'featured';
            } else {
                $featured = '';
            }
        }
        return $featured;
    }

    /**
     * Get listing single value (replaces several get_user_xx() functions)
     *
     * @param string  $field_name    Field Name (e.g. listingsdb_pclass_id).
     * @param integer $listingsdb_id Listing ID.
     *
     * @return  string $display returns field value or empty string.
     */
    public function getListingSingleValue(string $field_name, int $listingsdb_id): string
    {
        if ($listingsdb_id !== 0 && $field_name !== '') {
            $listing_api = $this->newListingApi();
            try {
                $result = $listing_api->read([
                    'listing_id' => $listingsdb_id,
                    'fields' => [
                        $field_name,
                    ],
                ]);
            } catch (Exception) {
                return '';
            }

            if (isset($result['listing']) && is_scalar($result['listing'][$field_name])) {
                return (string)$result['listing'][$field_name];
            }
        }
        return '';
    }

    /**
     * Get listing agent value (replaces several get_Listing_xx() functions)
     *
     * @param string  $field_name    User Field to get (e.g. userdb_user_first_name).
     * @param integer $listingsdb_id Listing ID.
     *
     * @return  string $display Returns the value of the user field or empty string.
     */
    public function getListingAgentValue(string $field_name, int $listingsdb_id): string
    {
        $listing_api = $this->newListingApi();
        try {
            $result = $listing_api->read(['listing_id' => $listingsdb_id, 'fields' => ['userdb_id']]);
        } catch (Exception) {
            return '';
        }


        $userdb_id = is_numeric($result['listing']['userdb_id']) ? (int)$result['listing']['userdb_id'] : 0;
        //if we just wanted the userdb_id return it now
        if ($field_name == 'userdb_id') {
            return (string)$userdb_id;
        }

        $user_api = $this->newUserApi();
        try {
            $result_data = $user_api->read(['user_id' => $userdb_id, 'resource' => 'agent', 'fields' => [$field_name]]);
        } catch (Exception) {
            return '';
        }
        return (string)$result_data['user'][$field_name];
    }

    /**
     * Listing Hit count
     *
     * @param integer $listingsdb_id Listing ID.
     *
     * @return  integer $hit_count
     */
    public function hitCount(int $listingsdb_id): int
    {
        // hit counter for user listings
        global $ORconn;
        $misc = $this->newMisc();
        $listing_api = $this->newListingApi();
        try {
            $result = $listing_api->read([
                'listing_id' => $listingsdb_id,
                'fields' => [
                    'listingsdb_hit_count',
                    'listingsdb_pclass_id',
                ],
            ]);
        } catch (Exception) {
            return 0;
        }
        $hit_count = is_numeric($result['listing']['listingsdb_hit_count']) ? (int)$result['listing']['listingsdb_hit_count'] : 0;
        $hit_count++;

        // can't use API here, site visitors have no permission to update listing data
        $sql = 'UPDATE ' . $this->config['table_prefix'] . 'listingsdb
            SET 
            listingsdb_hit_count=listingsdb_hit_count+1
            WHERE listingsdb_id=' . $listingsdb_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        return $hit_count;
    }


    /******************* DEPRECATED BELOW *************************************/

    /**
     * Get Title (deprecated)
     *
     * @param integer $listing_id
     * @return  string $listingsdb_title
     * @deprecated
     */
    public function getTitle(int $listingsdb_id): string
    {
        return $this->getListingSingleValue('listingsdb_title', $listingsdb_id);
    }
}
