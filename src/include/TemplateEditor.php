<?php

declare(strict_types=1);

namespace OpenRealty;

use Exception;
use OpenRealty\Api\Commands\FieldsApi;
use OpenRealty\Api\Commands\LogApi;

class TemplateEditor extends BaseClass
{
    /*********************************/
    /* EDIT AGENT AND MEMBER FIELDS */
    /*******************************/

    public function editUserTemplate(string $type): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();

        $page = $this->newPageAdmin();
        $page->loadPage($this->config['admin_template_path'] . '/user_template_editor.html');
        if ($type == 'member') {
            $security = $login->verifyPriv('edit_member_template');
        } elseif ($type == 'agent') {
            $security = $login->verifyPriv('edit_agent_template');
        } else {
            return ('Invalid user type');
        }

        $display = '';

        if ($security === true) {
            $this->deleteUserField($type);

            // Grab the list of fields set to be on the search results page sorted by search_result_rank
            $sql = 'SELECT ' . $type . 'formelements_id, ' . $type . 'formelements_field_name,
						' . $type . 'formelements_required, ' . $type . 'formelements_field_caption, 
						' . $type . 'formelements_rank
						FROM ' . $this->config['table_prefix'] . $type . 'formelements
						ORDER BY ' . $type . 'formelements_rank;';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }

            $all_fields = [];
            $html_results = '';
            $html = $page->getTemplateSection('user_field_block');
            while (!$recordSet->EOF) {
                $new_field_block = $html;
                $all_fields[$recordSet->fields($type . 'formelements_field_name')] = $recordSet->fields($type . 'formelements_field_caption') . ' (' . $recordSet->fields($type . 'formelements_field_name') . ')';
                $fid = $recordSet->fields($type . 'formelements_id');
                $f_rank = $recordSet->fields($type . 'formelements_rank');
                // Get Caption from users selected language
                if (!isset($_SESSION['users_lang'])) {
                    $caption = $recordSet->fields($type . 'formelements_field_caption');
                } else {
                    $field_id = intval($fid);
                    $sql2 = 'SELECT ' . $type . 'formelements_field_caption
								FROM ' . $this->config['lang_table_prefix'] . $type . "formelements
								WHERE $type.'formelements_id = $field_id";
                    $recordSet2 = $ORconn->Execute($sql2);
                    if (is_bool($recordSet2)) {
                        $misc->logErrorAndDie($sql2);
                    }
                    $caption = $recordSet2->fields($type . 'formelements_field_caption');
                }

                if ($recordSet->fields($type . 'formelements_required') == 'Yes') {
                    $new_field_block = $page->cleanupTemplateBlock('required', $new_field_block);
                } else {
                    $new_field_block = $page->removeTemplateBlock('required', $new_field_block);
                }

                $field_name = $recordSet->fields($type . 'formelements_field_name');
                $new_field_block = $page->replaceTagSafe('field_rank', $f_rank, $new_field_block);
                $new_field_block = $page->replaceTagSafe('field_id', $fid, $new_field_block);
                $new_field_block = $page->replaceTagSafe('field_name', $field_name, $new_field_block);
                $new_field_block = $page->replaceTagSafe('field_caption', $caption, $new_field_block);
                $html_results .= $new_field_block;
                $recordSet->MoveNext();
            }

            $page->replaceTemplateSection('user_field_block', $html_results);
            $selected_field = '';
            if (isset($_GET['edit_field']) && is_string($_GET['edit_field'])) {
                $selected_field = $_GET['edit_field'];
            }
            $html = $page->getTemplateSection('user_template_editor_field_edit_block');
            $html = $page->formOptions($all_fields, $selected_field, $html);
            $page->replaceTemplateSection('user_template_editor_field_edit_block', $html);

            $page->replaceTagSafe('user_type', $type);


            $page->replaceTag('application_status_text', '');
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }


    public function ajaxAddUserField(string $type): string
    {
        global $lang;

        $login = $this->newLogin();

        if ($type == 'member') {
            $security = $login->verifyPriv('edit_member_template');
        } elseif ($type == 'agent') {
            $security = $login->verifyPriv('edit_agent_template');
        } else {
            return ('Invalid user type');
        }

        if ($security) {
            $display = $this->addUserTemplateField($type);
        } else {
            $display = '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function addUserTemplateField(string $type): string
    {
        global $lang;


        $page = $this->newPageAdmin();

        $login = $this->newLogin();

        $page->loadPage($this->config['admin_template_path'] . '/user_template_add_field.html');

        if ($type == 'member') {
            $security = $login->verifyPriv('edit_member_template');
        } else {
            $security = $login->verifyPriv('edit_agent_template');
        }

        if ($security === true) {
            $page->replaceTagSafe('user_type', $type);
            $page->replaceTag('application_status_text', '');
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        }
        return '<div class="error_text">' . $lang['access_denied'] . '</div>';
    }

    public function ajaxInsertUserField(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        if (isset($_POST['user_type']) && is_string($_POST['user_type'])) {
            $type = $_POST['user_type'];

            if ($type == 'member') {
                $security = $login->verifyPriv('edit_member_template');
            } else {
                $security = $login->verifyPriv('edit_agent_template');
            }

            if ($security === true) {
                if (
                    !isset($_POST['lang_change'])
                    && isset(
                        $_POST['edit_field'],
                        $_POST['field_type'],
                        $_POST['field_caption'],
                        $_POST['default_text'],
                        $_POST['field_elements'],
                        $_POST['required'],
                        $_POST['tool_tip'],
                        $_POST['rank']
                    )
                    && is_string($_POST['edit_field'])
                    && is_string($_POST['field_type'])
                    && is_string($_POST['field_caption'])
                    && is_string($_POST['default_text'])
                    && is_string($_POST['field_elements'])
                    && is_string($_POST['required'])
                    && is_string($_POST['tool_tip'])
                    && is_numeric($_POST['rank'])
                ) {
                    $field_type = $misc->makeDbSafe($_POST['field_type']);
                    $_POST['edit_field'] = str_replace(' ', '_', $_POST['edit_field']);
                    $field_name = $misc->makeDbSafe($_POST['edit_field']);
                    $field_caption = $misc->makeDbSafe($_POST['field_caption']);
                    $default_text = $misc->makeDbSafe($_POST['default_text']);
                    $field_elements = $misc->makeDbSafe($_POST['field_elements']);
                    $rank = intval($_POST['rank']);
                    $required = $misc->makeDbSafe($_POST['required']);
                    $tool_tip = $misc->makeDbSafe($_POST['tool_tip']);
                    if ($type == 'member') {
                        $sql = 'INSERT INTO ' . $this->config['table_prefix'] . $type . 'formelements
							(' . $type . 'formelements_field_type, ' . $type . 'formelements_field_name, ' . $type . 'formelements_field_caption, ' . $type . 'formelements_default_text, ' . $type . 'formelements_field_elements, ' . $type . 'formelements_rank, ' . $type . 'formelements_required, ' . $type . "formelements_tool_tip)
							VALUES ($field_type,$field_name,$field_caption,$default_text,$field_elements,$rank,$required,$tool_tip)";
                    } else {
                        $sql = 'INSERT INTO ' . $this->config['table_prefix'] . $type . 'formelements
							(' . $type . 'formelements_field_type, ' . $type . 'formelements_field_name, ' . $type . 'formelements_field_caption, ' . $type . 'formelements_default_text, ' . $type . 'formelements_field_elements, ' . $type . 'formelements_rank, ' . $type . 'formelements_required,' . $type . 'formelements_display_priv, ' . $type . "formelements_tool_tip)
							VALUES ($field_type,$field_name,$field_caption,$default_text,$field_elements,$rank,$required,0,$tool_tip)";
                    }
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    header('Content-type: application/json');
                    return json_encode(['error' => '0', 'field_name' => $field_name]) ?: '';
                }
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['access_denied']]) ?: '';
    }

    public function editUserField(string $listing_field_name, string $type): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $page = $this->newPageAdmin();
        if ($type == 'member') {
            $security = $login->verifyPriv('edit_member_template');
        } else {
            $security = $login->verifyPriv('edit_agent_template');
        }

        if ($security === true) {
            $page->loadPage($this->config['admin_template_path'] . '/user_template_edit_field.html');

            $edit_listing_field_name = $misc->makeDbSafe($listing_field_name);
            $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . $type . 'formelements
					WHERE ' . $type . 'formelements_field_name = ' . $edit_listing_field_name;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $id = $recordSet->fields($type . 'formelements_id');
            $field_type = $recordSet->fields($type . 'formelements_field_type');
            $field_name = $recordSet->fields($type . 'formelements_field_name');
            $field_caption = $recordSet->fields($type . 'formelements_field_caption');
            $default_text = $recordSet->fields($type . 'formelements_default_text');
            $field_elements = $recordSet->fields($type . 'formelements_field_elements');
            $rank = $recordSet->fields($type . 'formelements_rank');
            $required = $recordSet->fields($type . 'formelements_required');
            $tool_tip = (string)$recordSet->fields($type . 'formelements_tool_tip');

            if ($type == 'agent') {
                $display_priv = $recordSet->fields('agentformelements_display_priv');
                $page->replaceTagSafe('display_priv', $display_priv);
                $page->page = $page->cleanupTemplateBlock('display_priv', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('display_priv', $page->page);
            }

            $page->replaceTagSafe('field_id', $id);
            $page->replaceTagSafe('user_type', $type);
            $page->replaceTagSafe('field_name', $field_name);
            $page->replaceTagSafe('field_caption', $field_caption);
            $page->replaceTagSafe('field_type', $field_type);
            $page->replaceTagSafe('required', $required);
            $page->replaceTagSafe('required_lower', strtolower($required));
            $page->replaceTagSafe('field_elements', $field_elements);
            $page->replaceTagSafe('default_text', $default_text);

            $page->replaceTagSafe('tool_tip', $tool_tip);
            $page->replaceTagSafe('rank', $rank);


            $page->replaceTag('application_status_text', '');
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
    }

    public function ajaxUpdateUserField(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();

        if (isset($_POST['user_type']) && is_string($_POST['user_type'])) {
            $type = $_POST['user_type'];

            if ($type == 'member') {
                $security = $login->verifyPriv('edit_member_template');
            } else {
                $security = $login->verifyPriv('edit_agent_template');
            }
            if ($security === true) {
                if (
                    !isset($_POST['lang_change'])
                    && isset(
                        $_POST['update_id'],
                        $_POST['edit_field'],
                        $_POST['old_field_name'],
                        $_POST['field_type'],
                        $_POST['field_caption'],
                        $_POST['default_text'],
                        $_POST['field_elements'],
                        $_POST['required'],
                        $_POST['tool_tip'],
                        $_POST['rank'],
                    )
                    && is_numeric($_POST['update_id'])
                    && is_string($_POST['field_type'])
                    && is_string($_POST['edit_field'])
                    && is_string($_POST['old_field_name'])
                    && is_string($_POST['field_caption'])
                    && is_string($_POST['default_text'])
                    && is_string($_POST['field_elements'])
                    && is_string($_POST['required'])
                    && is_string($_POST['tool_tip'])
                    && is_numeric($_POST['rank'])
                ) {
                    $id = $_POST['update_id'];
                    $field_type = $misc->makeDbSafe($_POST['field_type']);
                    $_POST['edit_field'] = str_replace(' ', '_', $_POST['edit_field']);
                    $_POST['old_field_name'] = str_replace(' ', '_', $_POST['old_field_name']);
                    $field_name = $misc->makeDbSafe($_POST['edit_field']);
                    $old_field_name = $misc->makeDbSafe($_POST['old_field_name']);
                    //See if we are updating field name
                    $update_field_name = false;
                    if ($old_field_name != $field_name) {
                        $update_field_name = true;
                    }
                    $field_caption = $misc->makeDbSafe($_POST['field_caption']);
                    $default_text = $misc->makeDbSafe($_POST['default_text']);
                    $field_elements = $misc->makeDbSafe($_POST['field_elements']);
                    $rank = intval($_POST['rank']);
                    $required = $misc->makeDbSafe($_POST['required']);
                    $tool_tip = $misc->makeDbSafe($_POST['tool_tip']);
                    if ($type == 'agent') {
                        if (!isset($_POST['display_priv']) || !is_numeric($_POST['display_priv'])) {
                            header('Content-type: application/json');
                            return json_encode(['error' => '1', 'error_msg' => $lang['access_denied']]) ?: '';
                        }
                        $display_priv = $misc->makeDbSafe($_POST['display_priv']);
                        $sql = 'UPDATE ' . $this->config['table_prefix'] . $type . 'formelements
							SET ' . $type . 'formelements_field_type = ' . $field_type . ', ' . $type . 'formelements_field_name = ' . $field_name . ', ' . $type . 'formelements_field_caption = ' . $field_caption . ', ' . $type . 'formelements_default_text = ' . $default_text . ', ' . $type . 'formelements_field_elements = ' . $field_elements . ', ' . $type . 'formelements_rank = ' . $rank . ', ' . $type . 'formelements_required = ' . $required . ', ' . $type . 'formelements_display_priv  = ' . $display_priv . ', ' . $type . 'formelements_tool_tip  = ' . $tool_tip . '
							WHERE ' . $type . 'formelements_id = ' . $id;
                    } else {
                        $sql = 'UPDATE ' . $this->config['table_prefix'] . $type . 'formelements
							SET ' . $type . 'formelements_field_type = ' . $field_type . ', ' . $type . 'formelements_field_name = ' . $field_name . ', ' . $type . 'formelements_field_caption = ' . $field_caption . ', ' . $type . 'formelements_default_text = ' . $default_text . ', ' . $type . 'formelements_field_elements = ' . $field_elements . ', ' . $type . 'formelements_rank = ' . $rank . ', ' . $type . 'formelements_required = ' . $required . ', ' . $type . 'formelements_tool_tip  = ' . $tool_tip . '
							WHERE ' . $type . 'formelements_id = ' . $id;
                    }
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    if ($update_field_name) {
                        $lang_sql = 'UPDATE  ' . $this->config['table_prefix'] . "userdbelements 
								SET userdbelements_field_name = $field_name
								WHERE userdbelements_field_name = $old_field_name";
                        $lang_recordSet = $ORconn->Execute($lang_sql);
                        if (!$lang_recordSet) {
                            $misc->logErrorAndDie($lang_sql);
                        }
                    }
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }

                    header('Content-type: application/json');
                    return json_encode(['error' => '0', 'field_id' => $id]) ?: '';
                }
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['access_denied']]) ?: '';
    }

    public function ajaxGetIUserFieldInfo(string $type): string
    {
        global $lang;

        $login = $this->newLogin();

        if ($type == 'member') {
            $security = $login->verifyPriv('edit_member_template');
        } elseif ($type == 'agent') {
            $security = $login->verifyPriv('edit_agent_template');
        } else {
            return ('Invalid user type');
        }

        $display = '';

        if ($security === true) {
            if (isset($_GET['edit_field']) && is_string($_GET['edit_field'])) {
                $display .= $this->editUserField($_GET['edit_field'], $type);
            }
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function ajaxSaveUserRank(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();

        if (isset($_POST['user_type']) && is_string($_POST['user_type']) && in_array($_POST['user_type'], ['agent', 'member'])) {
            if ($_POST['user_type'] == 'agent') {
                $rank_field = 'agentformelements_rank';
            } else {
                $rank_field = 'memberformelements_rank';
            }
            $type = $_POST['user_type'];

            $sec_type = 'edit_' . $type . '_template';

            //security check
            $security = $login->verifyPriv($sec_type);
            if ($security === true && isset($_POST['field_name']) && is_array($_POST['field_name'])) {
                if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfTokenAjax($_POST['token'])) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
                }


                $rank = 0;
                foreach ($_POST['field_name'] as $field_name) {
                    //empty locations are skipped
                    if (is_string($field_name) && !empty($field_name)) {
                        $rank = $rank + 1;

                        $sql = 'UPDATE ' . $this->config['table_prefix'] . $type . 'formelements
					SET  ' . $rank_field . " = '" . $rank . "'
					WHERE " . $type . "formelements_field_name = '" . $field_name . "'";
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            $misc->logErrorAndDie($sql);
                        }
                    }
                }
                header('Content-type: application/json');
                return json_encode(['error' => false, 'status_msg' => $lang['admin_template_editor_field_order_set']]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => true, 'error_msg' => $lang['access_denied']]) ?: '';
    }

    public function deleteUserField(string $type): ?string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        if ($type == 'member') {
            $security = $login->verifyPriv('edit_member_template');
        } else {
            $security = $login->verifyPriv('edit_agent_template');
        }
        if ($security) {
            if (isset($_GET['delete_field']) && is_string($_GET['delete_field']) && !isset($_POST['lang_change'])) {
                $field_name = $misc->makeDbSafe($_GET['delete_field']);
                $sql = 'DELETE FROM ' . $this->config['table_prefix'] . $type . 'formelements
						WHERE ' . $type . 'formelements_field_name = ' . $field_name;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                return null;
            }
        }
        return '<div class="error_text">' . $lang['access_denied'] . '</div>';
    }

    /************************/
    /* EDIT LISTING FIELDS */
    /**********************/

    public function editListingTemplate(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        // Verify User is an Admin
        $security = $login->verifyPriv('edit_listing_template');

        //Load the Core Template

        $page = $this->newPageAdmin();
        $page->loadPage($this->config['admin_template_path'] . '/listing_template_editor.html');
        $display = '';
        $display1 = '';

        if ($security) {
            $display1 .= $this->deleteListingField();
            //$display1 .= $this->save_search_setup();
            $display .= $display1;

            //Replace NavBar Field
            $sql = 'SELECT listingsformelements_field_name, listingsformelements_field_caption
            FROM ' . $this->config['table_prefix'] . 'listingsformelements';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $all_fields = [];

            while (!$recordSet->EOF) {
                $all_fields[$recordSet->fields('listingsformelements_field_name')] = $recordSet->fields('listingsformelements_field_caption') . ' (' . $recordSet->fields('listingsformelements_field_name') . ')';
                $recordSet->MoveNext();
            }
            $selected_field = '';
            if (isset($_GET['edit_field']) && is_string($_GET['edit_field'])) {
                $selected_field = $_GET['edit_field'];
            }
            $page->replaceTag('content', $display);

            $html = $page->getTemplateSection('listing_template_editor_field_edit_block');
            $html = $page->formOptions($all_fields, $selected_field, $html);
            $page->replaceTemplateSection('listing_template_editor_field_edit_block', $html);


            $page->replaceTag('application_status_text', '');
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        }
        return '<div class="error_text">' . $lang['access_denied'] . '</div>';
    }

    public function showQuickFieldEdit(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_listing_template');
        if ($security === true) {
            $display = '';

            $dla = explode(',', $this->config['template_listing_sections']);
            $dla[] = '';
            foreach ($dla as $key => $display_loc) {
                $display_loc = trim($display_loc);
                // Grab a list of field_names in the Database to Edit
                $sql = 'SELECT listingsformelements_id, listingsformelements_field_name,
						listingsformelements_required, listingsformelements_searchable,
						listingsformelements_field_caption, listingsformelements_rank
						FROM ' . $this->config['table_prefix'] . "listingsformelements
						WHERE listingsformelements_location = '" . $display_loc . "'
						ORDER BY listingsformelements_rank";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }

                // if the display location was added after "bottom_right", it is custom
                // definitely not the best way to handle this
                if ($display_loc == '') {
                    $display_loc_class = 'center_box';
                    $display .= '<div class="' . $display_loc_class . '">' . $display_loc;
                } else {
                    if (!in_array($display_loc, ['headline', 'top_left', 'top_right', 'center', 'feature1', 'feature2', 'bottom_left', 'bottom_right'])) {
                        $display_loc_class = 'center_box ' . $display_loc;
                    } else {
                        $display_loc_class = $display_loc;
                    }

                    $display .= '<div class="' . $display_loc_class . '">' . $display_loc;
                }

                $display .= '	<ul id="' . $display_loc . '" class="qed_list display_location" title="' . $display_loc . '">';

                while (!$recordSet->EOF) {
                    $fid = $recordSet->fields('listingsformelements_id');
                    $f_rank = $recordSet->fields('listingsformelements_rank');

                    // Get Caption from users selected language
                    if (!isset($_SESSION['users_lang'])) {
                        $caption = $recordSet->fields('listingsformelements_field_caption');
                    } else {
                        $field_id = $misc->makeDbSafe($fid);
                        $sql2 = 'SELECT listingsformelements_field_caption
								FROM ' . $this->config['lang_table_prefix'] . "listingsformelements
								WHERE listingsformelements_id = $field_id";
                        $recordSet2 = $ORconn->Execute($sql2);
                        if (is_bool($recordSet2)) {
                            $misc->logErrorAndDie($sql2);
                        }
                        $caption = htmlspecialchars($recordSet2->fields('listingsformelements_field_caption'));
                    }

                    if ($recordSet->fields('listingsformelements_searchable') == 1) {
                        $searchable = '<span class="src_field">&nbsp;</span>';
                    } else {
                        $searchable = '<span class="fill">&nbsp;</span>';
                    }
                    if ($recordSet->fields('listingsformelements_required') == 'Yes') {
                        $required = '<span class="req_field">&nbsp;</span>';
                    } else {
                        $required = '<span class="fill">&nbsp;</span>';
                    }

                    $field_name = $recordSet->fields('listingsformelements_field_name');

                    $display .= '<li id="' . $field_name . '">';
                    $display .= '	<div class="block">';
                    $display .= '		<a href="" class="edit_field_link" id="rank_' . $f_rank . '" name="' . $field_name . '">' . $caption . '</a>' . $searchable . $required;
                    $display .= '	</div>';
                    $display .= '</li>';
                    $recordSet->MoveNext();
                }

                $display .= '	</ul>';
                $display .= '</div>';

                if ($key == 7) {
                    $display .= '<div class="clear"></div>';
                }
            } //end foreach

            return $display;
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
    }

    public function editListingTemplateSPO(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        // Verify User is an Admin
        $security = $login->verifyPriv('edit_listing_template');
        $display = '';

        if ($security) {
            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/listing_template_editor_spo.html');

            // Grab the list of Searchable fields sorted by search_rank
            $sql = 'SELECT listingsformelements_id, listingsformelements_field_name,
						listingsformelements_required, listingsformelements_searchable,
						listingsformelements_field_caption, listingsformelements_search_rank
						FROM ' . $this->config['table_prefix'] . "listingsformelements
						WHERE listingsformelements_searchable = '1'
		 				ORDER BY listingsformelements_search_rank";
            $recordSet = $ORconn->Execute($sql);

            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $html_results = '';
            $html = $page->getTemplateSection('spo_item_block');
            while (!$recordSet->EOF) {
                $new_field_block = $html;
                $fid = $recordSet->fields('listingsformelements_id');
                $f_rank = $recordSet->fields('listingsformelements_search_rank');

                // Get Caption from users selected language
                if (!isset($_SESSION['users_lang'])) {
                    $caption = $recordSet->fields('listingsformelements_field_caption');
                } else {
                    $field_id = $misc->makeDbSafe($fid);
                    $sql2 = 'SELECT listingsformelements_field_caption
								FROM ' . $this->config['lang_table_prefix'] . "listingsformelements
								WHERE listingsformelements_id = $field_id";
                    $recordSet2 = $ORconn->Execute($sql2);
                    if (is_bool($recordSet2)) {
                        $misc->logErrorAndDie($sql2);
                    }
                    $caption = $recordSet2->fields('listingsformelements_field_caption');
                }

                $field_name = htmlentities($recordSet->fields('listingsformelements_field_name'));

                $new_field_block = str_replace('{field_rank}', $f_rank, $new_field_block);
                $new_field_block = str_replace('{field_id}', $fid, $new_field_block);
                $new_field_block = str_replace('{field_name}', $field_name, $new_field_block);
                $new_field_block = str_replace('{field_caption}', $caption, $new_field_block);

                if ($recordSet->fields('listingsformelements_searchable') == 1) {
                    $new_field_block = $page->cleanupTemplateBlock('searchable', $new_field_block);
                } else {
                    $new_field_block = $page->removeTemplateBlock('searchable', $new_field_block);
                }
                if ($recordSet->fields('listingsformelements_required') == 'Yes') {
                    $new_field_block = $page->cleanupTemplateBlock('required', $new_field_block);
                } else {
                    $new_field_block = $page->removeTemplateBlock('required', $new_field_block);
                }
                $html_results .= $new_field_block;
                $recordSet->MoveNext();
            }
            $page->replaceTemplateSection('spo_item_block', $html_results);
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            $display .= $page->returnPage();
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function editListingTemplateSRO(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        // Verify User is an Admin
        $security = $login->verifyPriv('edit_listing_template');
        $display = '';

        if ($security === true) {
            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/listing_template_editor_sro.html');

            // Grab the list of fields set to be on the search results page sorted by search_result_rank
            $sql = 'SELECT listingsformelements_id, listingsformelements_field_name,
						listingsformelements_required, listingsformelements_searchable,
						listingsformelements_field_caption, listingsformelements_search_result_rank
						FROM ' . $this->config['table_prefix'] . "listingsformelements
						WHERE listingsformelements_display_on_browse = 'Yes'
						ORDER BY listingsformelements_search_result_rank;";
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $html_results = '';
            $html = $page->getTemplateSection('sro_item_block');
            while (!$recordSet->EOF) {
                $new_field_block = $html;
                $fid = $recordSet->fields('listingsformelements_id');
                $f_rank = $recordSet->fields('listingsformelements_search_result_rank');
                // Get Caption from users selected language
                if (!isset($_SESSION['users_lang'])) {
                    $caption = $recordSet->fields('listingsformelements_field_caption');
                } else {
                    $field_id = intval($fid);
                    $sql2 = 'SELECT listingsformelements_field_caption
								FROM ' . $this->config['lang_table_prefix'] . "listingsformelements
								WHERE listingsformelements_id = $field_id";
                    $recordSet2 = $ORconn->Execute($sql2);
                    if (is_bool($recordSet2)) {
                        $misc->logErrorAndDie($sql2);
                    }
                    $caption = $recordSet2->fields('listingsformelements_field_caption');
                }

                $field_name = htmlentities($recordSet->fields('listingsformelements_field_name'));

                $new_field_block = str_replace('{field_rank}', $f_rank, $new_field_block);
                $new_field_block = str_replace('{field_id}', $fid, $new_field_block);
                $new_field_block = str_replace('{field_name}', $field_name, $new_field_block);
                $new_field_block = str_replace('{field_caption}', $caption, $new_field_block);

                if ($recordSet->fields('listingsformelements_searchable') == 1) {
                    $new_field_block = $page->cleanupTemplateBlock('searchable', $new_field_block);
                } else {
                    $new_field_block = $page->removeTemplateBlock('searchable', $new_field_block);
                }
                if ($recordSet->fields('listingsformelements_required') == 'Yes') {
                    $new_field_block = $page->cleanupTemplateBlock('required', $new_field_block);
                } else {
                    $new_field_block = $page->removeTemplateBlock('required', $new_field_block);
                }
                $html_results .= $new_field_block;
                $recordSet->MoveNext();
            }
            $page->replaceTemplateSection('sro_item_block', $html_results);
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            $display .= $page->returnPage();
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function editListingTemplateQED(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        // Verify User is an Admin
        $security = $login->verifyPriv('edit_listing_template');
        $display = '';

        if ($security === true) {
            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/listing_template_editor_qed.html');
            $sections = explode(',', $this->config['template_listing_sections']);
            $sections[] = 'misc';
            foreach ($sections as $section) {
                $section_name = trim($section);
                if ($section_name == 'misc') {
                    $sql_section_name = $misc->makeDbSafe('');
                } else {
                    $sql_section_name = $misc->makeDbSafe($section_name);
                }

                // Grab a list of field_names in the Database to Edit
                $sql = 'SELECT listingsformelements_id, listingsformelements_field_name,
						listingsformelements_required, listingsformelements_searchable,
						listingsformelements_field_caption, listingsformelements_rank
						FROM ' . $this->config['table_prefix'] . "listingsformelements
						WHERE listingsformelements_location = " . $sql_section_name . "
						ORDER BY listingsformelements_rank";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $html_results = '';
                $html = $page->getTemplateSection($section_name . '_item_block');
                while (!$recordSet->EOF) {
                    $new_field_block = $html;
                    $fid = $recordSet->fields('listingsformelements_id');
                    $f_rank = $recordSet->fields('listingsformelements_rank');

                    // Get Caption from users selected language
                    if (!isset($_SESSION['users_lang'])) {
                        $caption = htmlentities($recordSet->fields('listingsformelements_field_caption'));
                    } else {
                        $field_id = $misc->makeDbSafe($fid);
                        $sql2 = 'SELECT listingsformelements_field_caption
								FROM ' . $this->config['lang_table_prefix'] . "listingsformelements
								WHERE listingsformelements_id = $field_id";
                        $recordSet2 = $ORconn->Execute($sql2);
                        if (is_bool($recordSet2)) {
                            $misc->logErrorAndDie($sql2);
                        }
                        $caption = htmlentities($recordSet2->fields('listingsformelements_field_caption'));
                    }
                    $field_name = htmlentities($recordSet->fields('listingsformelements_field_name'));

                    $new_field_block = str_replace('{field_rank}', $f_rank, $new_field_block);
                    $new_field_block = str_replace('{field_id}', $fid, $new_field_block);
                    $new_field_block = str_replace('{field_name}', $field_name, $new_field_block);
                    $new_field_block = str_replace('{field_caption}', $caption, $new_field_block);

                    if ($recordSet->fields('listingsformelements_searchable') == 1) {
                        $new_field_block = $page->cleanupTemplateBlock('searchable', $new_field_block);
                    } else {
                        $new_field_block = $page->removeTemplateBlock('searchable', $new_field_block);
                    }
                    if ($recordSet->fields('listingsformelements_required') == 'Yes') {
                        $new_field_block = $page->cleanupTemplateBlock('required', $new_field_block);
                    } else {
                        $new_field_block = $page->removeTemplateBlock('required', $new_field_block);
                    }
                    $html_results .= $new_field_block;
                    $recordSet->MoveNext();
                }
                $page->replaceTemplateSection($section_name . '_item_block', $html_results);
            }
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            $display .= $page->returnPage();
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function ajaxAddListingField(): string
    {
        global $lang;


        $login = $this->newLogin();
        // Verify User is an Admin
        $security = $login->verifyPriv('edit_listing_template');
        if ($security) {
            $display = $this->addListingTemplateField();
        } else {
            $display = '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function addListingTemplateField(): string
    {
        global $lang, $ORconn;
        $misc = $this->newMisc();
        $login = $this->newLogin();
        $page = $this->newPageAdmin();
        // Verify User has cred
        $security = $login->verifyPriv('edit_listing_template');

        if ($security) {
            $page->loadPage($this->config['admin_template_path'] . '/listing_template_add_field.html');
            $yes_no = [];
            //Define our Yes and No Options
            $yes_no['No'] = $lang['no'];
            $yes_no['Yes'] = $lang['yes'];

            // Define our Field Types
            $field_types = [];
            $field_types['text'] = $lang['text'];
            $field_types['textarea'] = $lang['textarea'];
            $field_types['select'] = $lang['select'];
            $field_types['select-multiple'] = $lang['select-multiple'];
            $field_types['option'] = $lang['option'];
            $field_types['checkbox'] = $lang['checkbox'];
            $field_types['divider'] = $lang['divider'];
            $field_types['price'] = $lang['price'];
            $field_types['url'] = $lang['url'];
            $field_types['email'] = $lang['email'];
            $field_types['number'] = $lang['number'];
            $field_types['decimal'] = $lang['decimal'];
            $field_types['date'] = $lang['date'];
            $field_types['lat'] = $lang['lat'];
            $field_types['long'] = $lang['long'];


            $display_privs = [];
            $display_privs[0] = $lang['display_priv_0'];
            $display_privs[1] = $lang['display_priv_1'];
            $display_privs[2] = $lang['display_priv_2'];
            $display_privs[3] = $lang['display_priv_3'];

            $locations = [];
            $locations[""] = $lang['do_not_display'];
            $sections = explode(',', $this->config['template_listing_sections']);
            foreach ($sections as $section) {
                $locations[$section] = $section;
            }

            $search_types = [];
            $search_types['ptext'] = $lang['ptext_description'];
            $search_types['optionlist'] = $lang['optionlist_description'];
            $search_types['optionlist_or'] = $lang['optionlist_or_description'];
            $search_types['fcheckbox'] = $lang['fcheckbox_description'];
            $search_types['fcheckbox_or'] = $lang['fcheckbox_or_description'];
            $search_types['fpulldown'] = $lang['fpulldown_description'];
            $search_types['select'] = $lang['select_description'];
            $search_types['select_or'] = $lang['select_or_description'];
            $search_types['pulldown'] = $lang['pulldown_description'];
            $search_types['checkbox'] = $lang['checkbox_description'];
            $search_types['checkbox_or'] = $lang['checkbox_or_description'];
            $search_types['option'] = $lang['option_description'];
            $search_types['minmax'] = $lang['minmax_description'];
            $search_types['daterange'] = $lang['daterange_description'];
            $search_types['singledate'] = $lang['singledate_description'];
            $search_types['null_checkbox'] = $lang['null_checkbox_description'];
            $search_types['notnull_checkbox'] = $lang['notnull_checkbox_description'];

            $html = $page->getTemplateSection('required_block');
            $html = $page->formOptions($yes_no, "", $html);
            $page->replaceTemplateSection('required_block', $html);

            $html = $page->getTemplateSection('field_type_block');
            $html = $page->formOptions($field_types, "", $html);
            $page->replaceTemplateSection('field_type_block', $html);

            $html = $page->getTemplateSection('display_priv_block');
            $html = $page->formOptions($display_privs, "", $html);
            $page->replaceTemplateSection('display_priv_block', $html);

            $html = $page->getTemplateSection('location_block');
            $html = $page->formOptions($locations, "", $html);
            $page->replaceTemplateSection('location_block', $html);

            $html = $page->getTemplateSection('search_type_block');
            $html = $page->formOptions($search_types, "", $html);
            $page->replaceTemplateSection('search_type_block', $html);

            $html = $page->getTemplateSection('display_on_browse_block');
            $html = $page->formOptions($yes_no, "", $html);
            $page->replaceTemplateSection('display_on_browse_block', $html);

            // get list of all property clases
            $sql = 'SELECT class_name, class_id
					FROM ' . $this->config['table_prefix'] . 'class
					ORDER BY class_rank';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }

            $classes = [];


            while (!$recordSet->EOF) {
                $class_id = $recordSet->fields('class_id');
                $class_name = $recordSet->fields('class_name');
                $classes[$class_id] = $class_name;
                $recordSet->MoveNext();
            }

            $html = $page->getTemplateSection('pclass_select_block');
            $html = $page->formOptions($classes, "", $html);
            $page->replaceTemplateSection('pclass_select_block', $html);


            $page->replaceTag('application_status_text', '');
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        }
        return '<div class="error_text">' . $lang['access_denied'] . '</div>';
    }

    public function ajaxInsertListingField(): string
    {
        global $lang;


        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_listing_template');
        header('Content-Type: application/json; charset=utf-8');
        if ($security) {
            if (isset($_POST['edit_field']) && !isset($_POST['lang_change'])) {
                if (!is_array($_POST['property_class']) || count($_POST['property_class']) == 0) {
                    return json_encode(['error' => '1', 'error_msg' => $lang['no_pclass_selected']]) ?: '';
                }
                $pclasses = array_map('intval', $_POST['property_class']);
                if (!isset($_POST['searchable'])) {
                    $_POST['searchable'] = false;
                } else {
                    $_POST['searchable'] = true;
                }
                if (!isset($_POST['field_length']) || !is_numeric($_POST['field_length'])) {
                    $_POST['field_length'] = 0;
                }
                if (!isset($_POST['tool_tip']) || !is_string($_POST['tool_tip'])) {
                    $_POST['tool_tip'] = '';
                }
                if ($_POST['required'] == 'Yes') {
                    $_POST['required'] = true;
                } else {
                    $_POST['required'] = false;
                }
                if ($_POST['display_on_browse'] == 'Yes') {
                    $_POST['display_on_browse'] = true;
                } else {
                    $_POST['display_on_browse'] = false;
                }
                $fields_api = $this->newFieldsApi();
                try {
                    return json_encode($fields_api->create([
                        'resource' => 'listing',
                        'class' => $pclasses,
                        'field_type' => is_string($_POST['field_type']) ? $_POST['field_type'] : '',
                        'field_name' => is_string($_POST['edit_field']) ? $_POST['edit_field'] : '',
                        'field_caption' => is_string($_POST['field_caption']) ? $_POST['field_caption'] : '',
                        'default_text' => is_string($_POST['default_text']) ? $_POST['default_text'] : '',
                        'field_elements' => explode('||', is_string($_POST['field_elements']) ? $_POST['field_elements'] : ''),
                        'rank' => is_numeric($_POST['rank']) ? (int)$_POST['rank'] : 0,
                        'search_rank' => is_numeric($_POST['search_rank']) ? (int)$_POST['search_rank'] : 0,
                        'search_result_rank' => is_numeric($_POST['search_result_rank']) ? (int)$_POST['search_result_rank'] : 0,
                        'required' => $_POST['required'],
                        'location' => is_string($_POST['location']) ? $_POST['location'] : '',
                        'display_on_browse' => $_POST['display_on_browse'],
                        'search_step' => is_string($_POST['search_step']) ? $_POST['search_step'] : '',
                        'display_priv' => is_numeric($_POST['display_priv']) ? (int)$_POST['display_priv'] : 0,
                        'field_length' => intval($_POST['field_length']),
                        'tool_tip' => $_POST['tool_tip'],
                        'search_label' => is_string($_POST['search_label']) ? $_POST['search_label'] : '',
                        'search_type' => is_string($_POST['search_type']) ? $_POST['search_type'] : '',
                        'searchable' => $_POST['searchable'],
                    ]));
                } catch (Exception $e) {
                    http_response_code(500);
                    return json_encode(['error' => '1', 'error_msg' => $e->getMessage()]) ?: '';
                }
            }
        }
        return json_encode(['error' => '1', 'error_msg' => $lang['access_denied']]) ?: '';
    }

    public function editListingField(string $edit_listing_field_name): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_listing_template');

        if ($security === true) {
            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/listing_template_edit_field.html');

            $yes_no = [];
            //Define our Yes and No Options
            $yes_no['No'] = $lang['no'];
            $yes_no['Yes'] = $lang['yes'];

            // Define our Field Types
            $field_types = [];
            $field_types['text'] = $lang['text'];
            $field_types['textarea'] = $lang['textarea'];
            $field_types['select'] = $lang['select'];
            $field_types['select-multiple'] = $lang['select-multiple'];
            $field_types['option'] = $lang['option'];
            $field_types['checkbox'] = $lang['checkbox'];
            $field_types['divider'] = $lang['divider'];
            $field_types['price'] = $lang['price'];
            $field_types['url'] = $lang['url'];
            $field_types['email'] = $lang['email'];
            $field_types['number'] = $lang['number'];
            $field_types['decimal'] = $lang['decimal'];
            $field_types['date'] = $lang['date'];
            $field_types['lat'] = $lang['lat'];
            $field_types['long'] = $lang['long'];


            $display_privs = [];
            $display_privs[0] = $lang['display_priv_0'];
            $display_privs[1] = $lang['display_priv_1'];
            $display_privs[2] = $lang['display_priv_2'];
            $display_privs[3] = $lang['display_priv_3'];

            $locations = [];
            $locations[""] = $lang['do_not_display'];
            $sections = explode(',', $this->config['template_listing_sections']);
            foreach ($sections as $section) {
                $locations[$section] = $section;
            }


            $search_types = [];
            $search_types['ptext'] = $lang['ptext_description'];
            $search_types['optionlist'] = $lang['optionlist_description'];
            $search_types['optionlist_or'] = $lang['optionlist_or_description'];
            $search_types['fcheckbox'] = $lang['fcheckbox_description'];
            $search_types['fcheckbox_or'] = $lang['fcheckbox_or_description'];
            $search_types['fpulldown'] = $lang['fpulldown_description'];
            $search_types['select'] = $lang['select_description'];
            $search_types['select_or'] = $lang['select_or_description'];
            $search_types['pulldown'] = $lang['pulldown_description'];
            $search_types['checkbox'] = $lang['checkbox_description'];
            $search_types['checkbox_or'] = $lang['checkbox_or_description'];
            $search_types['option'] = $lang['option_description'];
            $search_types['minmax'] = $lang['minmax_description'];
            $search_types['daterange'] = $lang['daterange_description'];
            $search_types['singledate'] = $lang['singledate_description'];
            $search_types['null_checkbox'] = $lang['null_checkbox_description'];
            $search_types['notnull_checkbox'] = $lang['notnull_checkbox_description'];


            $edit_listing_field_name = $misc->makeDbSafe($edit_listing_field_name);
            $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . "listingsformelements
					WHERE listingsformelements_field_name = $edit_listing_field_name";
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $id = $recordSet->fields('listingsformelements_id');
            $field_type = $recordSet->fields('listingsformelements_field_type');
            $field_name = $recordSet->fields('listingsformelements_field_name');

            // Multi Lingual Support
            if (!isset($_SESSION['users_lang'])) {
                // Hold empty string for translation fields, as we are workgin with teh default lang
                $field_caption = (string)$recordSet->fields('listingsformelements_field_caption');
                $default_text = (string)$recordSet->fields('listingsformelements_default_text');
                $field_elements = (string)$recordSet->fields('listingsformelements_field_elements');
                $search_label = (string)$recordSet->fields('listingsformelements_search_label');
            } else {
                // Store default lang to show for tanslator
                $field_id = intval($recordSet->fields('listingsformelements_id'));
                $lang_sql = 'SELECT listingsformelements_field_caption,listingsformelements_default_text,listingsformelements_field_elements,listingsformelements_search_label
							FROM ' . $this->config['lang_table_prefix'] . "listingsformelements
							WHERE listingsformelements_id = $field_id";
                $lang_recordSet = $ORconn->Execute($lang_sql);
                if (is_bool($lang_recordSet)) {
                    $misc->logErrorAndDie($lang_sql);
                }
                $field_caption = (string)$lang_recordSet->fields('listingsformelements_field_caption');
                $default_text = (string)$lang_recordSet->fields('listingsformelements_default_text');
                $field_elements = (string)$lang_recordSet->fields('listingsformelements_field_elements');
                $search_label = (string)$lang_recordSet->fields('listingsformelements_search_label');
            }

            $rank = (string)$recordSet->fields('listingsformelements_rank');
            $search_rank = (string)$recordSet->fields('listingsformelements_search_rank');
            $search_result_rank = (string)$recordSet->fields('listingsformelements_search_result_rank');
            $required = (string)$recordSet->fields('listingsformelements_required');
            $location = (string)$recordSet->fields('listingsformelements_location');
            $display_on_browse = (string)$recordSet->fields('listingsformelements_display_on_browse');
            $display_priv = (string)$recordSet->fields('listingsformelements_display_priv');
            $search_step = (string)$recordSet->fields('listingsformelements_search_step');
            $searchable = (string)$recordSet->fields('listingsformelements_searchable');
            $search_type = (string)$recordSet->fields('listingsformelements_search_type');
            $field_length = (string)$recordSet->fields('listingsformelements_field_length');
            $tool_tip = (string)$recordSet->fields('listingsformelements_tool_tip');

            $page->replaceTagSafe("field_id", $id);
            $page->replaceTagSafe("field_name", $field_name);
            $page->replaceTagSafe("field_caption", $field_caption);
            $page->replaceTagSafe("field_elements", $field_elements);
            $page->replaceTagSafe("default_text", $default_text);
            $page->replaceTagSafe("rank", $rank);
            $page->replaceTagSafe("search_rank", $search_rank);
            $page->replaceTagSafe("search_result_rank", $search_result_rank);
            $page->replaceTagSafe("search_step", $search_step);
            $page->replaceTagSafe("field_length", $field_length);
            $page->replaceTagSafe("tool_tip", $tool_tip);
            $page->replaceTagSafe("search_label", $search_label);
            //$page->replaceTagSafe("searchable", $searchable);


            if ($searchable > 0) {
                $page->page = $page->cleanupTemplateBlock('searchable', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('searchable', $page->page);
            }

            $html = $page->getTemplateSection('required_block');
            $html = $page->formOptions($yes_no, $required, $html);
            $page->replaceTemplateSection('required_block', $html);

            $html = $page->getTemplateSection('field_type_block');
            $html = $page->formOptions($field_types, $field_type, $html);
            $page->replaceTemplateSection('field_type_block', $html);

            $html = $page->getTemplateSection('display_priv_block');
            $html = $page->formOptions($display_privs, $display_priv, $html);
            $page->replaceTemplateSection('display_priv_block', $html);

            $html = $page->getTemplateSection('location_block');
            $html = $page->formOptions($locations, $location, $html);
            $page->replaceTemplateSection('location_block', $html);

            $html = $page->getTemplateSection('search_type_block');
            $html = $page->formOptions($search_types, $search_type, $html);
            $page->replaceTemplateSection('search_type_block', $html);

            $html = $page->getTemplateSection('display_on_browse_block');
            $html = $page->formOptions($yes_no, $display_on_browse, $html);
            $page->replaceTemplateSection('display_on_browse_block', $html);

            // get list of all property clases
            $sql = 'SELECT class_name, class_id
					FROM ' . $this->config['table_prefix'] . 'class
					ORDER BY class_rank';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }

            $classes = [];
            $selectedClasses = [];

            while (!$recordSet->EOF) {
                $class_id = $recordSet->fields('class_id');
                $class_name = $recordSet->fields('class_name');
                $classes[$class_id] = $class_name;
                // check if this field is part of this class
                $sql = 'SELECT count(class_id) 
						AS exist 
						FROM ' . $this->config['table_prefix_no_lang'] . 'classformelements
						WHERE listingsformelements_id = ' . $id . '
						AND class_id =' . $class_id;
                $recordSet2 = $ORconn->Execute($sql);
                if (is_bool($recordSet2)) {
                    $misc->logErrorAndDie($sql);
                }
                $select = $recordSet2->fields('exist');
                if ($select > 0) {
                    $selectedClasses[] = $class_id;
                }
                $recordSet->MoveNext();
            }

            $html = $page->getTemplateSection('pclass_select_block');
            $html = $page->formOptions($classes, $selectedClasses, $html);
            $page->replaceTemplateSection('pclass_select_block', $html);


            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);

            return $page->returnPage();
        } else {
            return '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
    }

    public function ajaxUpdateListingField(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_listing_template');

        if ($security) {
            if (
                !isset($_POST['lang_change'])
                && isset(
                    $_POST['update_id'],
                    $_POST['edit_field'],
                    $_POST['old_field_name'],
                    $_POST['field_type'],
                    $_POST['field_caption'],
                    $_POST['default_text'],
                    $_POST['field_elements'],
                    $_POST['required'],
                    $_POST['tool_tip'],
                    $_POST['rank'],
                    $_POST['search_type'],
                    $_POST['location'],
                    $_POST['display_on_browse'],
                    $_POST['search_label']
                )
                && is_numeric($_POST['update_id'])
                && is_string($_POST['field_type'])
                && is_string($_POST['edit_field'])
                && is_string($_POST['old_field_name'])
                && is_string($_POST['field_caption'])
                && is_string($_POST['default_text'])
                && is_string($_POST['field_elements'])
                && is_string($_POST['required'])
                && is_string($_POST['tool_tip'])
                && is_numeric($_POST['rank'])
                && is_string($_POST['search_type'])
                && is_string($_POST['location'])
                && is_string($_POST['display_on_browse'])
                && is_string($_POST['search_label'])
            ) {
                $id = intval($_POST['update_id']);
                $_POST['old_field_name'] = str_replace(' ', '_', $_POST['old_field_name']);
                $_POST['edit_field'] = str_replace(' ', '_', $_POST['edit_field']);
                $field_name = $misc->makeDbSafe($_POST['edit_field']);
                $old_field_name = $misc->makeDbSafe($_POST['old_field_name']);
                $required = $misc->makeDbSafe($_POST['required']);
                $update_field_name = false;
                if ($old_field_name != $field_name) {
                    $update_field_name = true;
                }
                $field_type = $misc->makeDbSafe($_POST['field_type']);
                $field_caption = $misc->makeDbSafe($_POST['field_caption']);
                $default_text = $misc->makeDbSafe($_POST['default_text']);
                $field_elements = $misc->makeDbSafe($_POST['field_elements']);
                $rank = intval($_POST['rank']);
                $search_rank = intval($_POST['search_rank']);
                $search_result_rank = intval($_POST['search_result_rank']);
                $location = $misc->makeDbSafe($_POST['location']);
                $display_on_browse = $misc->makeDbSafe($_POST['display_on_browse']);
                $display_priv = intval($_POST['display_priv']);
                $search_step = intval($_POST['search_step']);
                $field_length = intval($_POST['field_length']);
                $tool_tip = $misc->makeDbSafe($_POST['tool_tip']);
                if (isset($_POST['searchable'])) {
                    $searchable = intval($_POST['searchable']);
                } else {
                    $searchable = 0;
                }
                if ($searchable == '1' && $_POST['search_type'] == '') {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['no_search_type']]) ?: '';
                } elseif (!is_array($_POST['property_class']) || count($_POST['property_class']) == 0) {
                    header('Content-type: application/json');
                    return json_encode(['error' => '1', 'error_msg' => $lang['no_property_class_selected']]) ?: '';
                } else {
                    $search_label = $misc->makeDbSafe($_POST['search_label']);
                    $search_type = $misc->makeDbSafe($_POST['search_type']);
                    $sql = 'UPDATE ' . $this->config['table_prefix'] . "listingsformelements
							SET listingsformelements_field_type = $field_type, listingsformelements_field_name = $field_name, listingsformelements_rank = $rank, listingsformelements_search_rank = $search_rank, listingsformelements_search_result_rank = $search_result_rank, listingsformelements_required = $required, listingsformelements_location = $location, listingsformelements_display_on_browse = $display_on_browse, listingsformelements_search_step = $search_step, listingsformelements_searchable = $searchable, listingsformelements_search_type = $search_type, listingsformelements_display_priv = $display_priv, listingsformelements_field_length = $field_length, listingsformelements_tool_tip = $tool_tip
							WHERE listingsformelements_id = $id";
                    $recordSet = $ORconn->Execute($sql);
                    if ($recordSet === false) {
                        $misc->logErrorAndDie($sql);
                    }
                    // Update Current language
                    if (!isset($_SESSION['users_lang'])) {
                        $lang_sql = 'UPDATE  ' . $this->config['table_prefix'] . "listingsformelements SET listingsformelements_field_caption = $field_caption, listingsformelements_default_text = $default_text,listingsformelements_field_elements = $field_elements,listingsformelements_search_label = $search_label
									WHERE listingsformelements_id = $id";
                        $lang_recordSet = $ORconn->Execute($lang_sql);
                        if (!$lang_recordSet) {
                            $misc->logErrorAndDie($lang_sql);
                        }
                    } else {
                        $lang_sql = 'DELETE FROM  ' . $this->config['lang_table_prefix'] . "listingsformelements WHERE listingsformelements_id = $id";
                        $lang_recordSet = $ORconn->Execute($lang_sql);
                        if (!$lang_recordSet) {
                            $misc->logErrorAndDie($lang_sql);
                        }
                        $lang_sql = 'INSERT INTO ' . $this->config['lang_table_prefix'] . "listingsformelements (listingsformelements_id, listingsformelements_field_caption,listingsformelements_default_text,listingsformelements_field_elements,listingsformelements_search_label)
									VALUES ($id, $field_caption,$default_text,$field_elements,$search_label)";
                        $lang_recordSet = $ORconn->Execute($lang_sql);
                        if (!$lang_recordSet) {
                            $misc->logErrorAndDie($lang_sql);
                        }
                    }
                    // Check if field name changed, if it as update all listingsdbelement tables
                    if ($update_field_name) {
                        $lang_sql = 'UPDATE  ' . $this->config['table_prefix'] . "listingsdbelements SET listingsdbelements_field_name = $field_name
									WHERE listingsdbelements_field_name = $old_field_name";
                        $lang_recordSet = $ORconn->Execute($lang_sql);
                        if (!$lang_recordSet) {
                            $misc->logErrorAndDie($lang_sql);
                        }
                    }
                    // Delete from classform elements.
                    $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . 'classformelements WHERE listingsformelements_id = ' . $id;
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    // Insert new selections into class formelements
                    $class_sql = '';
                    foreach ($_POST['property_class'] as $class_id) {
                        $class_id = intval($class_id);
                        if ($class_id > 0) {
                            //Add to Property Class
                            $fields_api = $this->newFieldsApi();
                            ;
                            try {
                                $fields_api->assignClass(['class' => $class_id, 'field_id' => $id]);
                            } catch (Exception $e) {
                                $misc->logErrorAndDie($e->getMessage());
                            }
                            if (!empty($class_sql)) {
                                $class_sql .= ' OR listingsdb_pclass_id = ' . $class_id;
                            } else {
                                $class_sql .= ' listingsdb_pclass_id = ' . $class_id;
                            }
                        }
                    }
                    // Remove fields from any listings that are not in this class.
                    $pclass_list = '';
                    $sql = 'SELECT listingsdb_id FROM ' . $this->config['table_prefix'] . 'listingsdb WHERE ' . $class_sql;
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    while (!$recordSet->EOF) {
                        if (empty($pclass_list)) {
                            $pclass_list .= $recordSet->fields('listingsdb_id');
                        } else {
                            $pclass_list .= ',' . $recordSet->fields('listingsdb_id');
                        }
                        $recordSet->Movenext();
                    }
                    if ($pclass_list == '') {
                        $pclass_list = 0;
                    }
                    $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'listingsdbelements
							WHERE listingsdbelements_field_name = ' . $field_name . '
							AND listingsdb_id NOT IN (' . $pclass_list . ')';
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                    header('Content-type: application/json');
                    return json_encode(['error' => '0', 'field_id' => $id]) ?: '';
                    //$display .= '<center>' . $lang['field_has_been_updated'] . '</center><br />';
                }
            } else {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => $lang['access_denied']]) ?: '';
            }
        }
        header('Content-type: application/json');
        return json_encode(['error' => '1', 'error_msg' => $lang['access_denied']]) ?: '';
    }

    public function ajaxGetListingFieldInfo(): string
    {
        global $lang;


        $login = $this->newLogin();
        // Verify User is an Admin
        $security = $login->verifyPriv('edit_listing_template');

        $display = '';

        if ($security === true) {
            if (isset($_GET['edit_field']) && is_string($_GET['edit_field'])) {
                $display .= $this->editListingField($_GET['edit_field']);
            }
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }

    public function ajaxSaveListingFieldOrder(): string
    {
        global $lang, $ORconn;
        header('Content-type: application/json');
        $misc = $this->newMisc();
        $login = $this->newLogin();
        // Verify User is an Admin
        $security = $login->verifyPriv('edit_listing_template');

        if ($security === true) {
            if (isset($_POST['section']) && is_string($_POST['section']) && isset($_POST['fields']) && is_array($_POST['fields'])) {
                //Verify Section is valid
                $valid_section = explode(',', $this->config["template_listing_sections"]);
                if (!in_array($_POST['section'], $valid_section)) {
                    return json_encode(['error' => true, 'error_msg' => $lang['invalid_template_section']]) ?: '';
                }
                $sql_section = $misc->makeDbSafe($_POST['section']);
                foreach ($_POST['fields'] as $rank => $field_name) {
                    //empty locations are skipped
                    if (is_string($field_name) && !empty($field_name)) {
                        $sql_field_name = $misc->makeDbSafe($field_name);
                        $sql_rank = intval($rank);

                        $sql = 'UPDATE ' . $this->config['table_prefix'] . "listingsformelements
                        SET listingsformelements_location = " . $sql_section . ",
                            listingsformelements_rank = " . $sql_rank . "
                        WHERE listingsformelements_field_name = " . $sql_field_name;
                        $recordSet = $ORconn->Execute($sql);
                        //echo $sql.'<br>';
                        if (is_bool($recordSet)) {
                            $misc->logErrorAndDie($sql);
                        }
                    }
                }
                return json_encode(['error' => false, 'status_msg' => $lang['admin_template_editor_field_order_set']]) ?: '';
            }
        }
        return json_encode(['error' => true, 'error_msg' => $lang['access_denied']]) ?: '';
    }

    /**
     * @param 'spo'|'sro' $order_form
     */
    public function ajaxSaveListingSearchOrder(string $order_form): string
    {
        global $lang, $ORconn;
        header('Content-type: application/json');
        $misc = $this->newMisc();
        $login = $this->newLogin();
        // Verify User is an Admin
        $security = $login->verifyPriv('edit_listing_template');

        if ($security === true) {
            if (isset($_POST['fields']) && is_array($_POST['fields'])) {
                if ($order_form == 'spo') {
                    $rank_field = 'listingsformelements_search_rank';
                } else {
                    $rank_field = 'listingsformelements_search_result_rank';
                }
                foreach ($_POST['fields'] as $search_rank => $field_name) {
                    //empty locations are skipped
                    if (is_string($field_name) && !empty($field_name)) {
                        $sql_field_name = $misc->makeDbSafe($field_name);
                        $sql_rank = intval($search_rank);
                        $sql_rank = $sql_rank + 1;

                        $sql = 'UPDATE ' . $this->config['table_prefix'] . 'listingsformelements
						SET  ' . $rank_field . " = " . $sql_rank . "
						WHERE listingsformelements_field_name = " . $sql_field_name;
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            $misc->logErrorAndDie($sql);
                        }
                    }
                }
                return json_encode(['error' => false, 'status_msg' => $lang['admin_template_editor_field_order_set']]) ?: '';
            }
        }
        return json_encode(['error' => true, 'error_msg' => $lang['access_denied']]) ?: '';
    }

    public function deleteListingField(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_listing_template');

        if ($security) {
            if (isset($_GET['delete_field']) && is_string($_GET['delete_field']) && !isset($_POST['lang_change'])) {
                $field_name = $misc->makeDbSafe($_GET['delete_field']);
                $sql = 'SELECT listingsformelements_id 
						FROM ' . $this->config['table_prefix'] . "listingsformelements
						WHERE listingsformelements_field_name = $field_name";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                // Delete All Translationf for this field.
                $configured_langs = explode(',', $this->config['configured_langs']);
                while (!$recordSet->EOF) {
                    $listingsformelements_id = intval($recordSet->fields('listingsformelements_id'));
                    foreach ($configured_langs as $configured_lang) {
                        $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . $configured_lang . "_listingsformelements
								WHERE listingsformelements_id = $listingsformelements_id";
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            $misc->logErrorAndDie($sql);
                        }
                    }
                    // Remove field from property class.
                    $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . 'classformelements
							WHERE listingsformelements_id = ' . $listingsformelements_id;
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                }
                // Cleanup any listingdbelemts entries from this field.
                foreach ($configured_langs as $configured_lang) {
                    $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . $configured_lang . "_listingsdbelements
							WHERE listingsdbelements_field_name = $field_name";
                    $recordSet = $ORconn->Execute($sql);
                    if (is_bool($recordSet)) {
                        $misc->logErrorAndDie($sql);
                    }
                }
                $log_api = $this->newLogApi();
                try {
                    $log_api->create(['log_api_command' => 'function->delete_listing_field', 'log_message' => 'Deleted Listing Field ' . $field_name]);
                } catch (Exception $e) {
                    die($e->getMessage());
                }
            }
        }
        return '<div class="error_text">' . $lang['access_denied'] . '</div>';
    }
}
