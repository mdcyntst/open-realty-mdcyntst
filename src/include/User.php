<?php

declare(strict_types=1);

namespace OpenRealty;

use OpenRealty\Api\Commands\ListingApi;
use OpenRealty\Api\Commands\UserApi;
use Sabre\VObject;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\Writer\PngWriter;
use Exception;

class User extends BaseClass
{
    /**
     * @param $type
     */
    public function viewUsers(): string
    {
        global $ORconn, $agent_id;

        $misc = $this->newMisc();
        $user_section = '';
        $page = $this->newPageUser();
        $page->loadPage($this->config['template_path'] . '/view_users_default.html');
        $addons = $page->loadAddons();
        $addon_fields = $page->getAddonTemplateFieldList($addons);

        if (!$this->config['show_admin_on_agent_list']) {
            $options = 'userdb_is_agent = \'yes\'';
        } else {
            $options = '(userdb_is_agent = \'yes\' or userdb_is_admin = \'yes\')';
        }
        //Get User Count
        $sql = 'SELECT count(userdb_id) as user_count
		FROM ' . $this->config['table_prefix'] . 'userdb
		WHERE ' . $options . ' and userdb_active = \'yes\' and userdb_rank > 0
		ORDER BY userdb_rank,userdb_user_name';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $num_rows = (int)$recordSet->fields('user_count');

        $sql = 'SELECT userdb_id
				FROM ' . $this->config['table_prefix'] . 'userdb
				WHERE ' . $options . ' and userdb_active = \'yes\'  and userdb_rank > 0
				ORDER BY userdb_rank, userdb_user_name';
        //Handle Pagnation
        if (!isset($_GET['cur_page'])) {
            $_GET['cur_page'] = 0;
        }
        $limit_str = intval($_GET['cur_page']) * $this->config['users_per_page'];

        $next_prev = $misc->nextPrev($num_rows, intval($_GET['cur_page'])); // put in the next/previous stuff
        $recordSet = $ORconn->SelectLimit($sql, $this->config['users_per_page'], $limit_str);

        while (!$recordSet->EOF) {
            $agent_id = $recordSet->fields('userdb_id');
            $user_section .= $page->getTemplateSection('user_block');
            $user_section = $page->replaceUserFieldTags($agent_id, $user_section, 'agent');
            $user_section = $page->parseAddonTags($user_section, $addon_fields);
            $recordSet->MoveNext();
        }

        $page->replaceTemplateSection('user_block', $user_section);
        $page->page = str_replace('{next_prev}', $next_prev, $page->page);
        return $page->page;
    }

    /**
     * View User
     *
     * @return  string $display
     */
    public function viewUser()
    {
        global $lang;

        $misc = $this->newMisc();
        $display = '';
        $userdb_id = intval($_GET['user']);

        if ($userdb_id != '' && $userdb_id > 0) {
            $is_agent = $misc->getAgentStatus($userdb_id);
            $is_admin = $misc->getAdminStatus($userdb_id);

            if (($is_agent === true) || ($is_admin && $this->config['show_listedby_admin']) || ($is_admin && $this->config['show_admin_on_agent_list'])) {
                $page = $this->newPageUser();
                $image_handler = $this->newImageHandler();
                $file_handler = $this->newFileHandler();
                $page->loadPage($this->config['template_path'] . '/' . $this->config['agent_template']);
                //Replace Tags
                $page->replaceUserFieldTags($userdb_id, '', 'user');
                //TODO: Move other user related tags to the core replace_user_field_tag function
                $page->page = str_replace('{user_images_thumbnails}', $image_handler->renderUserImages($userdb_id), $page->page);
                $page->page = str_replace('{user_vcard_link}', $this->vcardAgentLink($userdb_id), $page->page);
                $page->page = str_replace('{user_listings_list}', $this->userListings($userdb_id), $page->page);

                $page->page = preg_replace_callback(
                    '/{user_listings_list_([0-9]*)}/i',
                    fn ($matches) => $this->userListings($userdb_id, (int)$matches[1]),
                    $page->page
                );

                $page->page = str_replace('{user_hit_count}', $this->userhitCount($userdb_id), $page->page);
                $page->page = str_replace('{user_listings_link}', $this->userListingsLink($userdb_id), $page->page);
                $page->page = str_replace('{files_user_horizontal}', $file_handler->renderTemplatedFiles($userdb_id, 'user', 'horizontal'), $page->page);
                $page->page = str_replace('{files_user_vertical}', $file_handler->renderTemplatedFiles($userdb_id, 'user', 'vertical'), $page->page);
                $page->page = str_replace('{user_files_select}', $file_handler->renderFilesSelect($userdb_id, 'user'), $page->page);

                $display = $page->page;
            } else {
                $display = $lang['user_manager_invalid_user_id'];
            }
        }
        return $display;
    }

    /**
     * User Listings
     *
     * @param integer $userdb_id
     *          integer $limit
     *
     * @return  string $display
     */
    public function userListings(int $userdb_id, int $limit = 50): string
    {
        global $lang;

        $misc = $this->newMisc();
        $page = $this->newPageUser();
        $listing_api = $this->newListingApi();
        $display = '<strong>' . $lang['users_other_listings'] . '</strong>';
        try {
            $result = $listing_api->search([
                'parameters' => [
                    'listingsdb_active' => 'yes',
                    'user_ID' => $userdb_id,
                ],
                'sortby' => ['listingsdb_id'],
                'sorttype' => ['ASC'],
                'limit' => $limit,
                'offset' => 0,
            ]);
        } catch (Exception $e) {
            $misc->logErrorAndDie($e->getMessage());
        }

        if ($result['listing_count'] > 0) {
            foreach ($result['listings'] as $listingsdb_id) {
                try {
                    $api_read = $listing_api->read(['listing_id' => $listingsdb_id, 'fields' => ['listingsdb_title']]);
                    $listingsdb_title = $api_read['listing']['listingsdb_title'];
                    $url = $page->magicURIGenerator('listing', (string)$listingsdb_id, true);
                    $display .= '<li> <a href="' . $url . '">' . $listingsdb_title . '</a></li>';
                } catch (Exception) {
                    continue;
                }
            }
        }
        $display .= '</ul>';

        return $display;
    }

    /**
     * User Hit count
     *
     * @param integer $userdb_id
     *
     * @return  string $display
     */
    public function userhitCount(int $userdb_id): string
    {
        // hit counter for user listings
        global $lang, $ORconn;
        $misc = $this->newMisc();
        $display = '';

        $user_api = $this->newUserApi();
        try {
            $result = $user_api->read([
                'user_id' => $userdb_id,
                'resource' => 'agent',
                'fields' => [
                    'userdb_hit_count',
                ],
            ]);
        } catch (Exception $e) {
            $misc->logErrorAndDie($e->getMessage());
        }

        $hit_count = $result['user']['userdb_hit_count'];
        $hit_count = $hit_count + 1;

        // Can't use this API to do update as members will not have access.

        $sql = 'UPDATE ' . $this->config['table_prefix'] . 'userdb
            SET userdb_hit_count=userdb_hit_count+1
            WHERE userdb_id=' . $userdb_id;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }


        $display .= $lang['this_user_has_been_viewed'] . ' <strong>' . $hit_count . '</strong> ' . $lang['times'];

        return $display;
    }

    /**
     * Get user single item (replaces several get_user_xx() functions)
     *
     * @param string $field_name (e.g. userdb_user_first_name)
     *                           integer $userdb_id
     *
     * @return  string $display
     */
    public function getUserSingleItem(string $field_name, int $userdb_id): string
    {
        $misc = $this->newMisc();

        $is_agent = $misc->getAgentStatus($userdb_id);
        $is_admin = $misc->getAdminStatus($userdb_id);

        if ($is_agent === true || $is_admin === true) {
            $resource = 'agent';
        } else {
            $resource = 'member';
        }
        $user_api = $this->newUserApi();
        try {
            $result_data = $user_api->read([
                'user_id' => $userdb_id,
                'resource' => $resource,
                'fields' => [
                    $field_name,
                ],
            ]);
        } catch (Exception) {
            return '';
        }
        return $result_data['user'][$field_name] ?? '';
    }

    /**
     * Get user type
     *
     * @param integer $userdb_id
     *
     * @return  string (admin/agent/member)
     */
    public function getUserType(int $user): string
    {
        $misc = $this->newMisc();

        $userdb_id = $user;
        $is_agent = $misc->getAgentStatus($userdb_id);
        $is_admin = $misc->getAdminStatus($userdb_id);

        if ($is_admin) {
            return 'admin';
        } elseif ($is_agent) {
            return 'agent';
        }
        return 'member';
    }

    /**
     * Contact agent link
     *
     * @param integer $userdb_id
     *
     * @return string $display
     */
    public function contactAgentLink(int $userdb_id): string
    {
        global $lang;
        return '<a href="' . $this->config['baseurl'] . '/index.php?action=contact_agent&amp;agent_id=' . $userdb_id . '">' . $lang['contact_agent'] . '</a>';
    }

    public function renderUserInfo(int $user): string
    {
        global $ORconn;
        $misc = $this->newMisc();
        $login = $this->newLogin();
        $display = '';
        $formDB = $this->determineUserFormType($user);
        $priv_sql = '';

        if ($formDB == 'agentformelements') {
            //Check Users Permissions.
            $display_agent = $login->verifyPriv('Agent');
            $display_member = $login->verifyPriv('Member');
            if ($display_agent) {
                $priv_sql = 'AND ' . $formDB . '_display_priv <= 2 ';
            } elseif ($display_member) {
                $priv_sql = 'AND ' . $formDB . '_display_priv <= 1 ';
            } else {
                $priv_sql = 'AND ' . $formDB . '_display_priv = 0 ';
            }
        }
        $sql = 'SELECT userdbelements_field_value, ' . $formDB . '_field_type, ' . $formDB . '_field_caption
				FROM ' . $this->config['table_prefix'] . 'userdbelements, ' . $this->config['table_prefix'] . $formDB . '
				WHERE ((userdb_id = ' . $user . ')
				AND (userdbelements_field_name = ' . $formDB . '_field_name)) ' . $priv_sql . '
				ORDER BY ' . $formDB . '_rank ASC';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        while (!$recordSet->EOF) {
            $field_value = $recordSet->fields('userdbelements_field_value');
            $field_type = $recordSet->fields($formDB . '_field_type');
            $field_caption = $recordSet->fields($formDB . '_field_caption');
            if ($field_value != '') {
                if ($field_type == 'select-multiple' or $field_type == 'option' or $field_type == 'checkbox') {
                    // handle field types with multiple options
                    $display .= '<br /><strong>' . $field_caption . '</strong>: <br />';
                    $feature_index_list = explode('||', $field_value);
                    foreach ($feature_index_list as $feature_list_item) {
                        $display .= $feature_list_item;
                        $display .= $this->config['feature_list_separator'];
                    }
                } elseif ($field_type == 'price') {
                    $money_amount = $misc->internationalNumFormat($field_value);
                    $display .= '<strong>' . $field_caption . '</strong>: ' . $misc->moneyFormats($money_amount) . '<br />';
                } elseif ($field_type == 'number') {
                    $display .= '<strong>' . $field_caption . '</strong>: ' . $misc->internationalNumFormat($field_value) . '<br />';
                } elseif ($field_type == 'url') {
                    if ($formDB == 'agentformelements') {
                        $display .= '<strong>' . $field_caption . '</strong>: <a href="' . $field_value . '" onclick="window.open(this.href,\'_blank\',\'location=1,resizable=1,status=1,scrollbars=1,toolbar=1,menubar=1,noopener,noreferrer\');return false">' . $field_value . '</a><br />';
                    } else {
                        $display .= '<strong>' . $field_caption . '</strong>: <a href="' . $field_value . '" onclick="window.open(this.href,\'_blank\',\'location=1,resizable=1,status=1,scrollbars=1,toolbar=1,menubar=1,noopener,noreferrer\');return false" rel="nofollow">' . $field_value . '</a><br />';
                    }
                } elseif ($field_type == 'email') {
                    $display .= '<strong>' . $field_caption . '</strong>: <a href="mailto:' . $field_value . '">' . $field_value . '</a><br />';
                } elseif ($field_type == 'date') {
                    if ($this->config['date_format'] == 1) {
                        $format = 'm/d/Y';
                    } elseif ($this->config['date_format'] == 2) {
                        $format = 'Y/d/m';
                    } else {
                        $format = 'd/m/Y';
                    }
                    $field_value = date($format, intval($field_value));
                    $display .= '<strong>' . $field_caption . '</strong>: ' . $field_value . '<br />';
                } else {
                    if ($this->config['add_linefeeds']) {
                        $field_value = nl2br($field_value); //replace returns with <br />
                    } // end if
                    $display .= '<strong>' . $field_caption . '</strong>: ' . $field_value . '<br />';
                } // end else
            } // end if
            $recordSet->MoveNext();
        } // end while
        return $display;
    }

    /**
     * Determine user form type
     *
     * @param integer $userdb_id
     *
     * @return string $formDB
     */
    public function determineUserFormType(int $userdb_id): string
    {
        $misc = $this->newMisc();
        $is_agent = $misc->getAgentStatus($userdb_id);
        $is_admin = $misc->getAdminStatus($userdb_id);
        $formDB = 'memberformelements';
        if ($is_agent === true || $is_admin === true) {
            $formDB = 'agentformelements';
        }
        return $formDB;
    }

    public function renderSingleListingItem(int $userID, string $name, string $display_type = 'both'): string
    {
        // Display_type - Sets what should be returned.
        // both - Displays both the caption and the formated value
        // value - Displays just the formated value
        // rawvalue - Displays just the raw value
        // caption - Displays only the captions
        global $ORconn;
        $misc = $this->newMisc();
        $display = '';
        $formDB = $this->determineUserFormType($userID); //agentformelements, memberformelements
        $name = $misc->makeDbSafe($name);
        $user_type = $this->getUserType($userID);
        $sql = 'SELECT userdbelements_field_value, ' . $formDB . '_id, ' . $formDB . '_field_type, ' . $formDB . '_field_caption
				FROM ' . $this->config['table_prefix'] . 'userdbelements, ' . $this->config['table_prefix'] . $formDB . '
				WHERE ((userdb_id = ' . $userID . ')
				AND (' . $formDB . '_field_name = userdbelements_field_name)
				AND (userdbelements_field_name = ' . $name . '))';

        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        while (!$recordSet->EOF) {
            $field_value = $recordSet->fields('userdbelements_field_value');
            $field_type = $recordSet->fields($formDB . '_field_type');
            $form_elements_id = $recordSet->fields($formDB . '_id');
            if (!isset($_SESSION['users_lang'])) {
                // Hold empty string for translation fields, as we are workgin with teh default lang
                $field_caption = $recordSet->fields($formDB . '_field_caption');
            } else {
                $lang_sql = 'SELECT ' . $formDB . '_field_caption
							FROM ' . $this->config['lang_table_prefix'] . $formDB . '
							WHERE ' . $formDB . '_id = ' . $form_elements_id;
                $lang_recordSet = $ORconn->Execute($lang_sql);
                if (is_bool($lang_recordSet)) {
                    $misc->logErrorAndDie($lang_sql);
                }
                $field_caption = $lang_recordSet->fields($formDB . '_field_caption');
            }

            if (is_scalar($field_value) && $field_value != '') {
                if ($display_type === 'both' || $display_type === 'caption') {
                    $display .= '<span class="field_caption">' . $field_caption . '</span>';
                }
                if ($display_type == 'both') {
                    $display .= ':&nbsp;';
                }
                if ($display_type === 'both' || $display_type === 'value') {
                    if ($field_type == 'select-multiple' or $field_type == 'option' or $field_type == 'checkbox') {
                        // handle field types with multiple options
                        $feature_index_list = explode('||', (string)$field_value);
                        sort($feature_index_list);
                        foreach ($feature_index_list as $feature_list_item) {
                            $display .= '<br />' . $feature_list_item;
                        } // end while
                    } elseif ($field_type == 'price') {
                        $money_amount = $misc->internationalNumFormat((float)$field_value, $this->config['number_decimals_price_fields']);
                        $display .= $misc->moneyFormats($money_amount);
                    } elseif ($field_type == 'number') {
                        $display .= $misc->internationalNumFormat((float)$field_value, $this->config['number_decimals_number_fields']);
                    } elseif ($field_type == 'url') {
                        if ($user_type == 'member') {
                            $display .= '<a href="' . $field_value . '" onclick="window.open(this.href,\'_blank\',\'location=1,resizable=1,status=1,scrollbars=1,toolbar=1,menubar=1,noopener,noreferrer\');return false" rel="nofollow">' . $field_value . '</a>';
                        } else {
                            $display .= '<a href="' . $field_value . '" onclick="window.open(this.href,\'_blank\',\'location=1,resizable=1,status=1,scrollbars=1,toolbar=1,menubar=1,noopener,noreferrer\');return false">' . $field_value . '</a>';
                        }
                    } elseif ($field_type == 'email') {
                        $display .= '<a href="mailto:' . $field_value . '">' . $field_value . '</a>';
                    } elseif ($field_type == 'text' or $field_type == 'textarea') {
                        if ($this->config['add_linefeeds']) {
                            $field_value = nl2br((string)$field_value); //replace returns with <br />
                        } // end if
                        $display .= $field_value;
                    } elseif ($field_type == 'date') {
                        if ($this->config['date_format'] == 1) {
                            $format = 'm/d/Y';
                        } elseif ($this->config['date_format'] == 2) {
                            $format = 'Y/d/m';
                        } else {
                            $format = 'd/m/Y';
                        }
                        /** @var string $field_value */
                        $field_value = date($format, (int)$field_value);
                        $display .= $field_value;
                    } else {
                        $display .= $field_value;
                    } // end else
                }
                if ($display_type === 'rawvalue') {
                    $display .= $field_value;
                }
            } // end if
            $recordSet->MoveNext();
        }
        return $display;
    }

    public function vcardAgentLink(int $user): string
    {
        global $lang;
        return '<a href="index.php?action=create_vcard&amp;user=' . $user . '">' . $lang['vcard_link_text'] . '</a>';
    }

    public function createVcard(int $user): void
    {
        global $ORconn;
        $misc = $this->newMisc();
        // define vcard
        $vcard = new VObject\Component\VCard();

        $first = $this->getUserSingleItem('userdb_user_first_name', $user);
        $last = $this->getUserSingleItem('userdb_user_last_name', $user);
        $additional = '';
        $prefix = '';
        $suffix = '';


        $vcard->add('N', [$last, $first, $additional, $prefix, $suffix]);

        $sql = 'SELECT userdb_emailaddress FROM ' . $this->config['lang_table_prefix'] . 'userdb WHERE userdb_id=' . $user;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $email = $recordSet->fields('userdb_emailaddress');
        $vcard->add('EMAIL', $email);
        $sql = 'SELECT userdbelements_field_name,userdbelements_field_value
                    FROM ' . $this->config['lang_table_prefix'] . 'userdbelements
                    WHERE userdb_id=' . $user;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $address = '';
        $city = '';
        $state = '';
        $zip = '';
        $country = '';

        while (!$recordSet->EOF) {
            if ($recordSet->fields('userdbelements_field_name') == $this->config['vcard_phone']) {
                $phone = $recordSet->fields('userdbelements_field_value');
                $vcard->add('TEL', $phone, ['type' => 'WORK;VOICE']);
            } elseif ($recordSet->fields('userdbelements_field_name') == $this->config['vcard_fax']) {
                $fax = $recordSet->fields('userdbelements_field_value');
                $vcard->add('TEL', $fax, ['type' => 'WORK;FAX']);
            } elseif ($recordSet->fields('userdbelements_field_name') == $this->config['vcard_mobile']) {
                $mobile = $recordSet->fields('userdbelements_field_value');
                $vcard->add('TEL', $mobile, ['type' => 'WORK;CELL']);
            } elseif ($recordSet->fields('userdbelements_field_name') == $this->config['vcard_notes']) {
                $notes = $recordSet->fields('userdbelements_field_value');
                $vcard->add('NOTE', $notes);
            } elseif ($recordSet->fields('userdbelements_field_name') == $this->config['vcard_url']) {
                $url = $recordSet->fields('userdbelements_field_value');
                $vcard->add('URL', $url, ['type' => 'WORK']);
            } elseif ($recordSet->fields('userdbelements_field_name') == $this->config['vcard_address']) {
                $address = $recordSet->fields('userdbelements_field_value');
            } elseif ($recordSet->fields('userdbelements_field_name') == $this->config['vcard_city']) {
                $city = $recordSet->fields('userdbelements_field_value');
            } elseif ($recordSet->fields('userdbelements_field_name') == $this->config['vcard_state']) {
                $state = $recordSet->fields('userdbelements_field_value');
            } elseif ($recordSet->fields('userdbelements_field_name') == $this->config['vcard_zip']) {
                $zip = $recordSet->fields('userdbelements_field_value');
            } elseif ($recordSet->fields('userdbelements_field_name') == $this->config['vcard_country']) {
                $country = $recordSet->fields('userdbelements_field_value');
            }
            $recordSet->MoveNext();
        }
        if ($address != '' || $city != '' || $state != '' || $zip != '' || $country != '') {
            $vcard->add('ADR', ['', '', $address, $city, $state, $zip, $country], ['type' => 'WORK']);
        }

        $output = $vcard->serialize();
        //echo $output;
        $filename = $last . "_" . $first . ".vcf";
        Header('Content-Disposition: attachment; filename=' . $filename);
        Header('Content-Length: ' . strlen($output));
        Header('Connection: close');
        Header('Content-Type: text/x-vCard; name=' . $filename);
    }

    /**
     * user listings link
     *
     * @param integer $userdb_id
     *
     * @return string $display
     */
    public function userListingsLink(int $userdb_id): string
    {
        global $lang;
        return '<a href="' . $this->config['baseurl'] . '/index.php?action=searchresults&amp;user_ID=' . $userdb_id . '">' . $lang['user_listings_link_text'] . '</a>';
    }

    /**
     * QR Code link
     *
     * @param integer $userdb_id
     *
     * @return string $display
     */
    public function qrCodeLink(int $userdb_id): string
    {
        return $this->config['baseurl'] . '/index.php?action=userqrcode&amp;user_id=' . $userdb_id;
    }

    /**
     * QR Code
     *
     * @param integer $userdb_id
     *
     * @return
     */
    public function qrCode(int $userdb_id): string
    {
        $page = $this->newPageUser();
        $writer = new PngWriter();
        $userURL = $page->magicURIGenerator('agent', (string)$userdb_id, true);
        $qrCode = QrCode::create($userURL)
            ->setEncoding(new Encoding('UTF-8'));
        try {
            $result = $writer->write($qrCode);
            header('Content-Type: ' . $result->getMimeType());
            return $result->getString();
        } catch (Exception) {
            return '';
        }
    }


    /**
     * Get user reg info
     *
     * @param integer $userdb_id
     *
     * @return array{user_name: string, first_name: string, last_name:string, emailaddress:string}
     */
    public function getUserRegInfo(int $userdb_id): array
    {
        // grabs the main info for a given user
        global $ORconn;
        $misc = $this->newMisc();
        $user = $userdb_id;
        $sql = 'SELECT userdb_user_name, userdb_user_first_name, userdb_user_last_name, userdb_emailaddress
				FROM ' . $this->config['table_prefix'] . 'userdb
				WHERE (userdb_id = ' . $user . ')';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }

        $reg_info = [];
        /** @var string */
        $reg_info['user_name'] = $recordSet->fields('userdb_user_name');
        /** @var string */
        $reg_info['first_name'] = $recordSet->fields('userdb_user_first_name');
        /** @var string */
        $reg_info['last_name'] = $recordSet->fields('userdb_user_last_name');
        /** @var string */
        $reg_info['emailaddress'] = $recordSet->fields('userdb_emailaddress');

        return $reg_info;
    }


    /******************* DEPRECATED BELOW *************************************/

    /**
     * Get user name (deprecated)
     *
     * @param integer $userdb_id
     *
     * @return string $display
     * @deprecated
     */
    public function getUserName(int $userdb_id): string
    {
        // grabs the main info for a given user
        global $ORconn;
        $misc = $this->newMisc();
        $display = '';
        $sql = 'SELECT userdb_user_name
				FROM ' . $this->config['table_prefix'] . 'userdb
				WHERE (userdb_id = ' . $userdb_id . ')';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        // get main listings data
        $name = '';
        while (!$recordSet->EOF) {
            $name = $recordSet->fields('userdb_user_name');
            $recordSet->MoveNext();
        } // end while
        $display .= $name;
        return $display;
    }

    /**
     * Get user email (deprecated)
     *
     * @param integer $userdb_id
     *
     * @return string $email
     * @deprecated
     */
    public function getUserEmail(int $userdb_id): string
    {
        // grabs the main info for a given user
        $misc = $this->newMisc();

        $is_agent = $misc->getAgentStatus($userdb_id);
        $is_admin = $misc->getAdminStatus($userdb_id);

        if ($is_agent === true || $is_admin === true) {
            $resource = 'agent';
        } else {
            $resource = 'member';
        }

        $user_api = $this->newUserApi();
        try {
            $result_data = $user_api->read([
                'user_id' => $userdb_id,
                'resource' => $resource,
                'fields' => [
                    'userdb_emailaddress',
                ],
            ]);
        } catch (Exception) {
            return '';
        }

        return $result_data['user']['userdb_emailaddress'];
    }
}
