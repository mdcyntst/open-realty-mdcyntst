<?php

declare(strict_types=1);

namespace OpenRealty;

use Abraham\TwitterOAuth\TwitterOAuth;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use Throwable;

/**
 * configurator
 * This class contains all functions related the site configurator.
 */
class Configurator extends BaseClass
{
    /**
     * Array for handling Yes No in forms
     *
     * @var string[]
     */

    private const YES_NO = [0 => 'No', 1 => 'Yes'];
    /**
     * Array for handling ASC / DESC in forms
     *
     * @var string[]
     */
    private const ASC_DESC = ['ASC' => 'ASC', 'DESC' => 'DESC'];

    /**
     * Array for handling captcha selection in forms
     *
     * @var string[]
     */
    private const CAPTCHA_SYSTEMS = ['recaptcha' => 'reCAPTCHA', 'securimage' => 'Local Captcha'];
    /**
     * Array for handling timezone selection
     *
     * @var string[]
     */
    private const TIMEZONES = [
        'Africa/Abidjan' => 'Africa/Abidjan',
        'Africa/Accra' => 'Africa/Accra',
        'Africa/Addis_Ababa' => 'Africa/Addis_Ababa',
        'Africa/Algiers' => 'Africa/Algiers',
        'Africa/Asmara' => 'Africa/Asmara',
        'Africa/Bamako' => 'Africa/Bamako',
        'Africa/Bangui' => 'Africa/Bangui',
        'Africa/Banjul' => 'Africa/Banjul',
        'Africa/Bissau' => 'Africa/Bissau',
        'Africa/Blantyre' => 'Africa/Blantyre',
        'Africa/Brazzaville' => 'Africa/Brazzaville',
        'Africa/Bujumbura' => 'Africa/Bujumbura',
        'Africa/Cairo' => 'Africa/Cairo',
        'Africa/Casablanca' => 'Africa/Casablanca',
        'Africa/Ceuta' => 'Africa/Ceuta',
        'Africa/Conakry' => 'Africa/Conakry',
        'Africa/Dakar' => 'Africa/Dakar',
        'Africa/Dar_es_Salaam' => 'Africa/Dar_es_Salaam',
        'Africa/Djibouti' => 'Africa/Djibouti',
        'Africa/Douala' => 'Africa/Douala',
        'Africa/El_Aaiun' => 'Africa/El_Aaiun',
        'Africa/Freetown' => 'Africa/Freetown',
        'Africa/Gaborone' => 'Africa/Gaborone',
        'Africa/Harare' => 'Africa/Harare',
        'Africa/Johannesburg' => 'Africa/Johannesburg',
        'Africa/Juba' => 'Africa/Juba',
        'Africa/Kampala' => 'Africa/Kampala',
        'Africa/Khartoum' => 'Africa/Khartoum',
        'Africa/Kigali' => 'Africa/Kigali',
        'Africa/Kinshasa' => 'Africa/Kinshasa',
        'Africa/Lagos' => 'Africa/Lagos',
        'Africa/Libreville' => 'Africa/Libreville',
        'Africa/Lome' => 'Africa/Lome',
        'Africa/Luanda' => 'Africa/Luanda',
        'Africa/Lubumbashi' => 'Africa/Lubumbashi',
        'Africa/Lusaka' => 'Africa/Lusaka',
        'Africa/Malabo' => 'Africa/Malabo',
        'Africa/Maputo' => 'Africa/Maputo',
        'Africa/Maseru' => 'Africa/Maseru',
        'Africa/Mbabane' => 'Africa/Mbabane',
        'Africa/Mogadishu' => 'Africa/Mogadishu',
        'Africa/Monrovia' => 'Africa/Monrovia',
        'Africa/Nairobi' => 'Africa/Nairobi',
        'Africa/Ndjamena' => 'Africa/Ndjamena',
        'Africa/Niamey' => 'Africa/Niamey',
        'Africa/Nouakchott' => 'Africa/Nouakchott',
        'Africa/Ouagadougou' => 'Africa/Ouagadougou',
        'Africa/Porto-Novo' => 'Africa/Porto-Novo',
        'Africa/Sao_Tome' => 'Africa/Sao_Tome',
        'Africa/Tripoli' => 'Africa/Tripoli',
        'Africa/Tunis' => 'Africa/Tunis',
        'Africa/Windhoek' => 'Africa/Windhoek',
        'America/Adak' => 'America/Adak',
        'America/Anchorage' => 'America/Anchorage',
        'America/Anguilla' => 'America/Anguilla',
        'America/Antigua' => 'America/Antigua',
        'America/Araguaina' => 'America/Araguaina',
        'America/Argentina/Buenos_Aires' => 'America/Argentina/Buenos_Aires',
        'America/Argentina/Catamarca' => 'America/Argentina/Catamarca',
        'America/Argentina/Cordoba' => 'America/Argentina/Cordoba',
        'America/Argentina/Jujuy' => 'America/Argentina/Jujuy',
        'America/Argentina/La_Rioja' => 'America/Argentina/La_Rioja',
        'America/Argentina/Mendoza' => 'America/Argentina/Mendoza',
        'America/Argentina/Rio_Gallegos' => 'America/Argentina/Rio_Gallegos',
        'America/Argentina/Salta' => 'America/Argentina/Salta',
        'America/Argentina/San_Juan' => 'America/Argentina/San_Juan',
        'America/Argentina/San_Luis' => 'America/Argentina/San_Luis',
        'America/Argentina/Tucuman' => 'America/Argentina/Tucuman',
        'America/Argentina/Ushuaia' => 'America/Argentina/Ushuaia',
        'America/Aruba' => 'America/Aruba',
        'America/Asuncion' => 'America/Asuncion',
        'America/Atikokan' => 'America/Atikokan',
        'America/Bahia' => 'America/Bahia',
        'America/Bahia_Banderas' => 'America/Bahia_Banderas',
        'America/Barbados' => 'America/Barbados',
        'America/Belem' => 'America/Belem',
        'America/Belize' => 'America/Belize',
        'America/Blanc-Sablon' => 'America/Blanc-Sablon',
        'America/Boa_Vista' => 'America/Boa_Vista',
        'America/Bogota' => 'America/Bogota',
        'America/Boise' => 'America/Boise',
        'America/Cambridge_Bay' => 'America/Cambridge_Bay',
        'America/Campo_Grande' => 'America/Campo_Grande',
        'America/Cancun' => 'America/Cancun',
        'America/Caracas' => 'America/Caracas',
        'America/Cayenne' => 'America/Cayenne',
        'America/Cayman' => 'America/Cayman',
        'America/Chicago' => 'America/Chicago',
        'America/Chihuahua' => 'America/Chihuahua',
        'America/Costa_Rica' => 'America/Costa_Rica',
        'America/Creston' => 'America/Creston',
        'America/Cuiaba' => 'America/Cuiaba',
        'America/Curacao' => 'America/Curacao',
        'America/Danmarkshavn' => 'America/Danmarkshavn',
        'America/Dawson' => 'America/Dawson',
        'America/Dawson_Creek' => 'America/Dawson_Creek',
        'America/Denver' => 'America/Denver',
        'America/Detroit' => 'America/Detroit',
        'America/Dominica' => 'America/Dominica',
        'America/Edmonton' => 'America/Edmonton',
        'America/Eirunepe' => 'America/Eirunepe',
        'America/El_Salvador' => 'America/El_Salvador',
        'America/Fort_Nelson' => 'America/Fort_Nelson',
        'America/Fortaleza' => 'America/Fortaleza',
        'America/Glace_Bay' => 'America/Glace_Bay',
        'America/Goose_Bay' => 'America/Goose_Bay',
        'America/Grand_Turk' => 'America/Grand_Turk',
        'America/Grenada' => 'America/Grenada',
        'America/Guadeloupe' => 'America/Guadeloupe',
        'America/Guatemala' => 'America/Guatemala',
        'America/Guayaquil' => 'America/Guayaquil',
        'America/Guyana' => 'America/Guyana',
        'America/Halifax' => 'America/Halifax',
        'America/Havana' => 'America/Havana',
        'America/Hermosillo' => 'America/Hermosillo',
        'America/Indiana/Indianapolis' => 'America/Indiana/Indianapolis',
        'America/Indiana/Knox' => 'America/Indiana/Knox',
        'America/Indiana/Marengo' => 'America/Indiana/Marengo',
        'America/Indiana/Petersburg' => 'America/Indiana/Petersburg',
        'America/Indiana/Tell_City' => 'America/Indiana/Tell_City',
        'America/Indiana/Vevay' => 'America/Indiana/Vevay',
        'America/Indiana/Vincennes' => 'America/Indiana/Vincennes',
        'America/Indiana/Winamac' => 'America/Indiana/Winamac',
        'America/Inuvik' => 'America/Inuvik',
        'America/Iqaluit' => 'America/Iqaluit',
        'America/Jamaica' => 'America/Jamaica',
        'America/Juneau' => 'America/Juneau',
        'America/Kentucky/Louisville' => 'America/Kentucky/Louisville',
        'America/Kentucky/Monticello' => 'America/Kentucky/Monticello',
        'America/Kralendijk' => 'America/Kralendijk',
        'America/La_Paz' => 'America/La_Paz',
        'America/Lima' => 'America/Lima',
        'America/Los_Angeles' => 'America/Los_Angeles',
        'America/Lower_Princes' => 'America/Lower_Princes',
        'America/Maceio' => 'America/Maceio',
        'America/Managua' => 'America/Managua',
        'America/Manaus' => 'America/Manaus',
        'America/Marigot' => 'America/Marigot',
        'America/Martinique' => 'America/Martinique',
        'America/Matamoros' => 'America/Matamoros',
        'America/Mazatlan' => 'America/Mazatlan',
        'America/Menominee' => 'America/Menominee',
        'America/Merida' => 'America/Merida',
        'America/Metlakatla' => 'America/Metlakatla',
        'America/Mexico_City' => 'America/Mexico_City',
        'America/Miquelon' => 'America/Miquelon',
        'America/Moncton' => 'America/Moncton',
        'America/Monterrey' => 'America/Monterrey',
        'America/Montevideo' => 'America/Montevideo',
        'America/Montserrat' => 'America/Montserrat',
        'America/Nassau' => 'America/Nassau',
        'America/New_York' => 'America/New_York',
        'America/Nipigon' => 'America/Nipigon',
        'America/Nome' => 'America/Nome',
        'America/Noronha' => 'America/Noronha',
        'America/North_Dakota/Beulah' => 'America/North_Dakota/Beulah',
        'America/North_Dakota/Center' => 'America/North_Dakota/Center',
        'America/North_Dakota/New_Salem' => 'America/North_Dakota/New_Salem',
        'America/Nuuk' => 'America/Nuuk',
        'America/Ojinaga' => 'America/Ojinaga',
        'America/Panama' => 'America/Panama',
        'America/Pangnirtung' => 'America/Pangnirtung',
        'America/Paramaribo' => 'America/Paramaribo',
        'America/Phoenix' => 'America/Phoenix',
        'America/Port-au-Prince' => 'America/Port-au-Prince',
        'America/Port_of_Spain' => 'America/Port_of_Spain',
        'America/Porto_Velho' => 'America/Porto_Velho',
        'America/Puerto_Rico' => 'America/Puerto_Rico',
        'America/Punta_Arenas' => 'America/Punta_Arenas',
        'America/Rainy_River' => 'America/Rainy_River',
        'America/Rankin_Inlet' => 'America/Rankin_Inlet',
        'America/Recife' => 'America/Recife',
        'America/Regina' => 'America/Regina',
        'America/Resolute' => 'America/Resolute',
        'America/Rio_Branco' => 'America/Rio_Branco',
        'America/Santarem' => 'America/Santarem',
        'America/Santiago' => 'America/Santiago',
        'America/Santo_Domingo' => 'America/Santo_Domingo',
        'America/Sao_Paulo' => 'America/Sao_Paulo',
        'America/Scoresbysund' => 'America/Scoresbysund',
        'America/Sitka' => 'America/Sitka',
        'America/St_Barthelemy' => 'America/St_Barthelemy',
        'America/St_Johns' => 'America/St_Johns',
        'America/St_Kitts' => 'America/St_Kitts',
        'America/St_Lucia' => 'America/St_Lucia',
        'America/St_Thomas' => 'America/St_Thomas',
        'America/St_Vincent' => 'America/St_Vincent',
        'America/Swift_Current' => 'America/Swift_Current',
        'America/Tegucigalpa' => 'America/Tegucigalpa',
        'America/Thule' => 'America/Thule',
        'America/Thunder_Bay' => 'America/Thunder_Bay',
        'America/Tijuana' => 'America/Tijuana',
        'America/Toronto' => 'America/Toronto',
        'America/Tortola' => 'America/Tortola',
        'America/Vancouver' => 'America/Vancouver',
        'America/Whitehorse' => 'America/Whitehorse',
        'America/Winnipeg' => 'America/Winnipeg',
        'America/Yakutat' => 'America/Yakutat',
        'America/Yellowknife' => 'America/Yellowknife',
        'Antarctica/Casey' => 'Antarctica/Casey',
        'Antarctica/Davis' => 'Antarctica/Davis',
        'Antarctica/DumontDUrville' => 'Antarctica/DumontDUrville',
        'Antarctica/Macquarie' => 'Antarctica/Macquarie',
        'Antarctica/Mawson' => 'Antarctica/Mawson',
        'Antarctica/McMurdo' => 'Antarctica/McMurdo',
        'Antarctica/Palmer' => 'Antarctica/Palmer',
        'Antarctica/Rothera' => 'Antarctica/Rothera',
        'Antarctica/Syowa' => 'Antarctica/Syowa',
        'Antarctica/Troll' => 'Antarctica/Troll',
        'Antarctica/Vostok' => 'Antarctica/Vostok',
        'Arctic/Longyearbyen' => 'Arctic/Longyearbyen',
        'Asia/Aden' => 'Asia/Aden',
        'Asia/Almaty' => 'Asia/Almaty',
        'Asia/Amman' => 'Asia/Amman',
        'Asia/Anadyr' => 'Asia/Anadyr',
        'Asia/Aqtau' => 'Asia/Aqtau',
        'Asia/Aqtobe' => 'Asia/Aqtobe',
        'Asia/Ashgabat' => 'Asia/Ashgabat',
        'Asia/Atyrau' => 'Asia/Atyrau',
        'Asia/Baghdad' => 'Asia/Baghdad',
        'Asia/Bahrain' => 'Asia/Bahrain',
        'Asia/Baku' => 'Asia/Baku',
        'Asia/Bangkok' => 'Asia/Bangkok',
        'Asia/Barnaul' => 'Asia/Barnaul',
        'Asia/Beirut' => 'Asia/Beirut',
        'Asia/Bishkek' => 'Asia/Bishkek',
        'Asia/Brunei' => 'Asia/Brunei',
        'Asia/Chita' => 'Asia/Chita',
        'Asia/Choibalsan' => 'Asia/Choibalsan',
        'Asia/Colombo' => 'Asia/Colombo',
        'Asia/Damascus' => 'Asia/Damascus',
        'Asia/Dhaka' => 'Asia/Dhaka',
        'Asia/Dili' => 'Asia/Dili',
        'Asia/Dubai' => 'Asia/Dubai',
        'Asia/Dushanbe' => 'Asia/Dushanbe',
        'Asia/Famagusta' => 'Asia/Famagusta',
        'Asia/Gaza' => 'Asia/Gaza',
        'Asia/Hebron' => 'Asia/Hebron',
        'Asia/Ho_Chi_Minh' => 'Asia/Ho_Chi_Minh',
        'Asia/Hong_Kong' => 'Asia/Hong_Kong',
        'Asia/Hovd' => 'Asia/Hovd',
        'Asia/Irkutsk' => 'Asia/Irkutsk',
        'Asia/Jakarta' => 'Asia/Jakarta',
        'Asia/Jayapura' => 'Asia/Jayapura',
        'Asia/Jerusalem' => 'Asia/Jerusalem',
        'Asia/Kabul' => 'Asia/Kabul',
        'Asia/Kamchatka' => 'Asia/Kamchatka',
        'Asia/Karachi' => 'Asia/Karachi',
        'Asia/Kathmandu' => 'Asia/Kathmandu',
        'Asia/Khandyga' => 'Asia/Khandyga',
        'Asia/Kolkata' => 'Asia/Kolkata',
        'Asia/Krasnoyarsk' => 'Asia/Krasnoyarsk',
        'Asia/Kuala_Lumpur' => 'Asia/Kuala_Lumpur',
        'Asia/Kuching' => 'Asia/Kuching',
        'Asia/Kuwait' => 'Asia/Kuwait',
        'Asia/Macau' => 'Asia/Macau',
        'Asia/Magadan' => 'Asia/Magadan',
        'Asia/Makassar' => 'Asia/Makassar',
        'Asia/Manila' => 'Asia/Manila',
        'Asia/Muscat' => 'Asia/Muscat',
        'Asia/Nicosia' => 'Asia/Nicosia',
        'Asia/Novokuznetsk' => 'Asia/Novokuznetsk',
        'Asia/Novosibirsk' => 'Asia/Novosibirsk',
        'Asia/Omsk' => 'Asia/Omsk',
        'Asia/Oral' => 'Asia/Oral',
        'Asia/Phnom_Penh' => 'Asia/Phnom_Penh',
        'Asia/Pontianak' => 'Asia/Pontianak',
        'Asia/Pyongyang' => 'Asia/Pyongyang',
        'Asia/Qatar' => 'Asia/Qatar',
        'Asia/Qostanay' => 'Asia/Qostanay',
        'Asia/Qyzylorda' => 'Asia/Qyzylorda',
        'Asia/Riyadh' => 'Asia/Riyadh',
        'Asia/Sakhalin' => 'Asia/Sakhalin',
        'Asia/Samarkand' => 'Asia/Samarkand',
        'Asia/Seoul' => 'Asia/Seoul',
        'Asia/Shanghai' => 'Asia/Shanghai',
        'Asia/Singapore' => 'Asia/Singapore',
        'Asia/Srednekolymsk' => 'Asia/Srednekolymsk',
        'Asia/Taipei' => 'Asia/Taipei',
        'Asia/Tashkent' => 'Asia/Tashkent',
        'Asia/Tbilisi' => 'Asia/Tbilisi',
        'Asia/Tehran' => 'Asia/Tehran',
        'Asia/Thimphu' => 'Asia/Thimphu',
        'Asia/Tokyo' => 'Asia/Tokyo',
        'Asia/Tomsk' => 'Asia/Tomsk',
        'Asia/Ulaanbaatar' => 'Asia/Ulaanbaatar',
        'Asia/Urumqi' => 'Asia/Urumqi',
        'Asia/Ust-Nera' => 'Asia/Ust-Nera',
        'Asia/Vientiane' => 'Asia/Vientiane',
        'Asia/Vladivostok' => 'Asia/Vladivostok',
        'Asia/Yakutsk' => 'Asia/Yakutsk',
        'Asia/Yangon' => 'Asia/Yangon',
        'Asia/Yekaterinburg' => 'Asia/Yekaterinburg',
        'Asia/Yerevan' => 'Asia/Yerevan',
        'Atlantic/Azores' => 'Atlantic/Azores',
        'Atlantic/Bermuda' => 'Atlantic/Bermuda',
        'Atlantic/Canary' => 'Atlantic/Canary',
        'Atlantic/Cape_Verde' => 'Atlantic/Cape_Verde',
        'Atlantic/Faroe' => 'Atlantic/Faroe',
        'Atlantic/Madeira' => 'Atlantic/Madeira',
        'Atlantic/Reykjavik' => 'Atlantic/Reykjavik',
        'Atlantic/South_Georgia' => 'Atlantic/South_Georgia',
        'Atlantic/St_Helena' => 'Atlantic/St_Helena',
        'Atlantic/Stanley' => 'Atlantic/Stanley',
        'Australia/Adelaide' => 'Australia/Adelaide',
        'Australia/Brisbane' => 'Australia/Brisbane',
        'Australia/Broken_Hill' => 'Australia/Broken_Hill',
        'Australia/Darwin' => 'Australia/Darwin',
        'Australia/Eucla' => 'Australia/Eucla',
        'Australia/Hobart' => 'Australia/Hobart',
        'Australia/Lindeman' => 'Australia/Lindeman',
        'Australia/Lord_Howe' => 'Australia/Lord_Howe',
        'Australia/Melbourne' => 'Australia/Melbourne',
        'Australia/Perth' => 'Australia/Perth',
        'Australia/Sydney' => 'Australia/Sydney',
        'Europe/Amsterdam' => 'Europe/Amsterdam',
        'Europe/Andorra' => 'Europe/Andorra',
        'Europe/Astrakhan' => 'Europe/Astrakhan',
        'Europe/Athens' => 'Europe/Athens',
        'Europe/Belgrade' => 'Europe/Belgrade',
        'Europe/Berlin' => 'Europe/Berlin',
        'Europe/Bratislava' => 'Europe/Bratislava',
        'Europe/Brussels' => 'Europe/Brussels',
        'Europe/Bucharest' => 'Europe/Bucharest',
        'Europe/Budapest' => 'Europe/Budapest',
        'Europe/Busingen' => 'Europe/Busingen',
        'Europe/Chisinau' => 'Europe/Chisinau',
        'Europe/Copenhagen' => 'Europe/Copenhagen',
        'Europe/Dublin' => 'Europe/Dublin',
        'Europe/Gibraltar' => 'Europe/Gibraltar',
        'Europe/Guernsey' => 'Europe/Guernsey',
        'Europe/Helsinki' => 'Europe/Helsinki',
        'Europe/Isle_of_Man' => 'Europe/Isle_of_Man',
        'Europe/Istanbul' => 'Europe/Istanbul',
        'Europe/Jersey' => 'Europe/Jersey',
        'Europe/Kaliningrad' => 'Europe/Kaliningrad',
        'Europe/Kiev' => 'Europe/Kiev',
        'Europe/Kirov' => 'Europe/Kirov',
        'Europe/Lisbon' => 'Europe/Lisbon',
        'Europe/Ljubljana' => 'Europe/Ljubljana',
        'Europe/London' => 'Europe/London',
        'Europe/Luxembourg' => 'Europe/Luxembourg',
        'Europe/Madrid' => 'Europe/Madrid',
        'Europe/Malta' => 'Europe/Malta',
        'Europe/Mariehamn' => 'Europe/Mariehamn',
        'Europe/Minsk' => 'Europe/Minsk',
        'Europe/Monaco' => 'Europe/Monaco',
        'Europe/Moscow' => 'Europe/Moscow',
        'Europe/Oslo' => 'Europe/Oslo',
        'Europe/Paris' => 'Europe/Paris',
        'Europe/Podgorica' => 'Europe/Podgorica',
        'Europe/Prague' => 'Europe/Prague',
        'Europe/Riga' => 'Europe/Riga',
        'Europe/Rome' => 'Europe/Rome',
        'Europe/Samara' => 'Europe/Samara',
        'Europe/San_Marino' => 'Europe/San_Marino',
        'Europe/Sarajevo' => 'Europe/Sarajevo',
        'Europe/Saratov' => 'Europe/Saratov',
        'Europe/Simferopol' => 'Europe/Simferopol',
        'Europe/Skopje' => 'Europe/Skopje',
        'Europe/Sofia' => 'Europe/Sofia',
        'Europe/Stockholm' => 'Europe/Stockholm',
        'Europe/Tallinn' => 'Europe/Tallinn',
        'Europe/Tirane' => 'Europe/Tirane',
        'Europe/Ulyanovsk' => 'Europe/Ulyanovsk',
        'Europe/Uzhgorod' => 'Europe/Uzhgorod',
        'Europe/Vaduz' => 'Europe/Vaduz',
        'Europe/Vatican' => 'Europe/Vatican',
        'Europe/Vienna' => 'Europe/Vienna',
        'Europe/Vilnius' => 'Europe/Vilnius',
        'Europe/Volgograd' => 'Europe/Volgograd',
        'Europe/Warsaw' => 'Europe/Warsaw',
        'Europe/Zagreb' => 'Europe/Zagreb',
        'Europe/Zaporozhye' => 'Europe/Zaporozhye',
        'Europe/Zurich' => 'Europe/Zurich',
        'Indian/Antananarivo' => 'Indian/Antananarivo',
        'Indian/Chagos' => 'Indian/Chagos',
        'Indian/Christmas' => 'Indian/Christmas',
        'Indian/Cocos' => 'Indian/Cocos',
        'Indian/Comoro' => 'Indian/Comoro',
        'Indian/Kerguelen' => 'Indian/Kerguelen',
        'Indian/Mahe' => 'Indian/Mahe',
        'Indian/Maldives' => 'Indian/Maldives',
        'Indian/Mauritius' => 'Indian/Mauritius',
        'Indian/Mayotte' => 'Indian/Mayotte',
        'Indian/Reunion' => 'Indian/Reunion',
        'Pacific/Apia' => 'Pacific/Apia',
        'Pacific/Auckland' => 'Pacific/Auckland',
        'Pacific/Bougainville' => 'Pacific/Bougainville',
        'Pacific/Chatham' => 'Pacific/Chatham',
        'Pacific/Chuuk' => 'Pacific/Chuuk',
        'Pacific/Easter' => 'Pacific/Easter',
        'Pacific/Efate' => 'Pacific/Efate',
        'Pacific/Enderbury' => 'Pacific/Enderbury',
        'Pacific/Fakaofo' => 'Pacific/Fakaofo',
        'Pacific/Fiji' => 'Pacific/Fiji',
        'Pacific/Funafuti' => 'Pacific/Funafuti',
        'Pacific/Galapagos' => 'Pacific/Galapagos',
        'Pacific/Gambier' => 'Pacific/Gambier',
        'Pacific/Guadalcanal' => 'Pacific/Guadalcanal',
        'Pacific/Guam' => 'Pacific/Guam',
        'Pacific/Honolulu' => 'Pacific/Honolulu',
        'Pacific/Kiritimati' => 'Pacific/Kiritimati',
        'Pacific/Kosrae' => 'Pacific/Kosrae',
        'Pacific/Kwajalein' => 'Pacific/Kwajalein',
        'Pacific/Majuro' => 'Pacific/Majuro',
        'Pacific/Marquesas' => 'Pacific/Marquesas',
        'Pacific/Midway' => 'Pacific/Midway',
        'Pacific/Nauru' => 'Pacific/Nauru',
        'Pacific/Niue' => 'Pacific/Niue',
        'Pacific/Norfolk' => 'Pacific/Norfolk',
        'Pacific/Noumea' => 'Pacific/Noumea',
        'Pacific/Pago_Pago' => 'Pacific/Pago_Pago',
        'Pacific/Palau' => 'Pacific/Palau',
        'Pacific/Pitcairn' => 'Pacific/Pitcairn',
        'Pacific/Pohnpei' => 'Pacific/Pohnpei',
        'Pacific/Port_Moresby' => 'Pacific/Port_Moresby',
        'Pacific/Rarotonga' => 'Pacific/Rarotonga',
        'Pacific/Saipan' => 'Pacific/Saipan',
        'Pacific/Tahiti' => 'Pacific/Tahiti',
        'Pacific/Tarawa' => 'Pacific/Tarawa',
        'Pacific/Tongatapu' => 'Pacific/Tongatapu',
        'Pacific/Wake' => 'Pacific/Wake',
        'Pacific/Wallis' => 'Pacific/Wallis',
        'UTC' => 'UTC',
    ];

    /**
     * This function handles the display and updates for the site configurator.
     *
     * @return string Returns HTML from site_config.html template
     */
    public function showConfigurator(): string
    {
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_site_config');
        $display = '';
        if ($security === true) {
            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/site_config.html');
            $page->replaceTag('view_warnings', $this->viewWarnings());
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            $display .= $page->returnPage();
        }
        return $display;
    }

    /**
     * Builds the HTML for the General Confiugation Section site_config_general.html
     *
     * @return string Return HTML
     */
    public function ajaxConfigureGeneral(): string
    {
        global $ORconn, $lang;

        $login = $this->newLogin();
        $misc = $this->newMisc();

        $security = $login->verifyPriv('edit_site_config');
        $display = '';
        if ($security === true) {
            $page = $this->newPageAdmin();
            //Load TEmplate File
            $page->loadPage($this->config['admin_template_path'] . '/site_config_general.html');
            // New Charset Settings - Current charsets supported by PHP 4.3.0 and up
            $charset = [];
            $charset['ISO-8859-1'] = 'ISO-8859-1';
            $charset['ISO-8859-15'] = 'ISO-8859-15';
            $charset['UTF-8'] = 'UTF-8';
            $charset['cp866'] = 'cp866';
            $charset['cp1251'] = 'cp1251';
            $charset['cp1252'] = 'cp1252';
            $charset['KOI8-R'] = 'KOI8-R';
            $charset['BIG5'] = 'BIG5';
            $charset['GB2312'] = 'GB2312';
            $charset['BIG5-HKSCS'] = 'BIG5-HKSCS';
            $charset['Shift_JIS'] = 'Shift_JIS';
            $charset['EUC-JP'] = 'EUC-JP';
            //Replace Tags
            $page->replaceTag('controlpanel_admin_name', htmlentities($this->config['admin_name'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('basepath', htmlentities($this->config['basepath'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_admin_email', htmlentities($this->config['admin_email'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_site_email', htmlentities($this->config['site_email'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_company_name', htmlentities($this->config['company_name'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_company_location', htmlentities($this->config['company_location'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_company_logo', htmlentities($this->config['company_logo'], ENT_COMPAT, $this->config['charset']));

            $page->replaceTag('baseurl', htmlentities($this->config['baseurl'], ENT_COMPAT, $this->config['charset']));

            $page->replaceTag('controlpanel_seo_default_keywords', htmlentities($this->config['seo_default_keywords'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_seo_default_description', htmlentities($this->config['seo_default_description'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_seo_listing_keywords', htmlentities($this->config['seo_listing_keywords'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_seo_listing_description', htmlentities($this->config['seo_listing_description'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_seo_default_title', htmlentities($this->config['seo_default_title'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_seo_listing_title', htmlentities($this->config['seo_listing_title'], ENT_COMPAT, $this->config['charset']));

            $html = $page->getTemplateSection('automatic_update_block');
            $html = $page->formOptions(self::YES_NO, $this->config['automatic_update_check'] ? '1' : '0', $html);
            $page->replaceTemplateSection('automatic_update_block', $html);

            $html = $page->getTemplateSection('demo_mode_block');
            $html = $page->formOptions(self::YES_NO, $this->config['demo_mode'] ? '1' : '0', $html);
            $page->replaceTemplateSection('demo_mode_block', $html);

            $html = $page->getTemplateSection('maintenance_mode_block');
            $html = $page->formOptions(self::YES_NO, $this->config['maintenance_mode'] ? '1' : '0', $html);
            $page->replaceTemplateSection('maintenance_mode_block', $html);

            $url_type = [];
            $url_type[1] = $lang['url_standard'];
            $url_type[2] = $lang['url_search_friendly'];
            $html = $page->getTemplateSection('url_type_block');
            $html = $page->formOptions($url_type, $this->config['seo_url_seperator'], $html);
            $page->replaceTemplateSection('url_type_block', $html);

            $url_seperator = [];
            $url_seperator['+'] = $lang['url_seperator_default'];
            $url_seperator['-'] = $lang['url_seperator_hyphen'];
            $html = $page->getTemplateSection('url_seperator_block');
            $html = $page->formOptions($url_seperator, $this->config['seo_url_seperator'], $html);
            $page->replaceTemplateSection('url_seperator_block', $html);
            //Default page
            $default_page = [];
            $default_page['blog_index'] = $lang['default_page_blog_index'];
            $default_page['wysiwyg_page'] = $lang['default_page_wysiwyg_page'];
            $html = $page->getTemplateSection('default_page_block');
            $html = $page->formOptions($default_page, $this->config['default_page'], $html);
            $page->replaceTemplateSection('default_page_block', $html);

            $html = $page->getTemplateSection('timezone_block');
            $html = $page->formOptions(self::TIMEZONES, $this->config['timezone'], $html);
            $page->replaceTemplateSection('timezone_block', $html);

            $html = $page->getTemplateSection('charset_block');
            $html = $page->formOptions($charset, $this->config['charset'], $html);
            $page->replaceTemplateSection('charset_block', $html);


            // Mail Settings Start
            $mailsystems = [0 => 'PHP Mail()', 1 => 'SMTP w/ PHPMailer'];
            $html = $page->getTemplateSection('phpmailer_block');
            $html = $page->formOptions($mailsystems, $this->config['phpmailer'] ? '1' : '0', $html);
            $page->replaceTemplateSection('phpmailer_block', $html);

            $page->replaceTag('controlpanel_mailserver', htmlentities($this->config['mailserver'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_mailport', (string)$this->config['mailport']);
            $page->replaceTag('controlpanel_mailuser', htmlentities($this->config['mailuser'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_mailpass', htmlentities($this->config['mailpass'], ENT_COMPAT, $this->config['charset']));

            //Mail Settings End

            $html = $page->getTemplateSection('wysiwyg_show_edit_block');
            $html = $page->formOptions(self::YES_NO, $this->config['wysiwyg_show_edit'] ? '1' : '0', $html);
            $page->replaceTemplateSection('wysiwyg_show_edit_block', $html);


            $html = $page->getTemplateSection('mbstring_enabled_block');
            $html = $page->formOptions(self::YES_NO, $this->config['controlpanel_mbstring_enabled'] ? '1' : '0', $html);
            $page->replaceTemplateSection('mbstring_enabled_block', $html);

            $html = $page->getTemplateSection('add_linefeeds_block');
            $html = $page->formOptions(self::YES_NO, $this->config['add_linefeeds'] ? '1' : '0', $html);
            $page->replaceTemplateSection('add_linefeeds_block', $html);

            $html = $page->getTemplateSection('strip_html_block');
            $html = $page->formOptions(self::YES_NO, $this->config['strip_html'] ? '1' : '0', $html);
            $page->replaceTemplateSection('strip_html_block', $html);

            $html = $page->getTemplateSection('allow_template_change_block');
            $html = $page->formOptions(self::YES_NO, $this->config['allow_template_change'] ? '1' : '0', $html);
            $page->replaceTemplateSection('allow_template_change_block', $html);

            $html = $page->getTemplateSection('allow_language_change_block');
            $html = $page->formOptions(self::YES_NO, $this->config['allow_language_change'] ? '1' : '0', $html);
            $page->replaceTemplateSection('allow_language_change_block', $html);

            $html = $page->getTemplateSection('captcha_system_block');
            $html = $page->formOptions(self::CAPTCHA_SYSTEMS, $this->config['captcha_system'], $html);
            $page->replaceTemplateSection('captcha_system_block', $html);

            $page->replaceTag('controlpanel_allowed_html_tags', htmlentities($this->config['allowed_html_tags'], ENT_COMPAT, $this->config['charset']));

            //Show reCaptcha stuff
            $sql = 'SELECT controlpanel_recaptcha_sitekey, controlpanel_recaptcha_secretkey 
				FROM ' . $this->config['table_prefix_no_lang'] . 'controlpanel';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }

            $recaptchasecretkey = (string)$recordSet->fields('controlpanel_recaptcha_secretkey');
            $recaptchasitekey = (string)$recordSet->fields('controlpanel_recaptcha_sitekey');

            $page->replaceTag('controlpanel_recaptcha_sitekey', $recaptchasitekey);
            $page->replaceTag('controlpanel_recaptcha_secretkey', $recaptchasecretkey);

            $page->replaceTag('controlpanel_google_client_id', htmlentities($this->config['google_client_id'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_google_client_secret', htmlentities($this->config['google_client_secret'], ENT_COMPAT, $this->config['charset']));

            $page->replacePermissionTags();
            $page->replaceLangTemplateTags();
            $page->autoReplaceTags('', true);
            $display .= $page->returnPage();
        }

        return $display;
    }

    /**
     * Builds the HTML for the SEO Confiugation Section site_config_seo.html
     *
     * @return string Return HTML
     */
    public function ajaxConfigureSeo(): string
    {
        global $lang;

        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_site_config');
        $display = '';
        if ($security === true) {
            $page = $this->newPageAdmin();
            //Load TEmplate File
            $page->loadPage($this->config['admin_template_path'] . '/site_config_seo.html');
            //Replace Tags
            //Deal with
            $page->replaceTag('baseurl', htmlentities($this->config['baseurl'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_seo_default_keywords', htmlentities($this->config['seo_default_keywords'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_seo_default_description', htmlentities($this->config['seo_default_description'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_seo_listing_keywords', htmlentities($this->config['seo_listing_keywords'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_seo_listing_description', htmlentities($this->config['seo_listing_description'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_seo_default_title', htmlentities($this->config['seo_default_title'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_seo_listing_title', htmlentities($this->config['seo_listing_title'], ENT_COMPAT, $this->config['charset']));

            $url_type = [];
            $url_type[1] = $lang['url_standard'];
            $url_type[2] = $lang['url_search_friendly'];
            $html = $page->getTemplateSection('url_type_block');
            $html = $page->formOptions($url_type, (string)$this->config['url_style'], $html);
            $page->replaceTemplateSection('url_type_block', $html);
            $url_seperator = [];
            $url_seperator['+'] = $lang['url_seperator_default'];
            $url_seperator['-'] = $lang['url_seperator_hyphen'];
            $html = $page->getTemplateSection('url_seperator_block');
            $html = $page->formOptions($url_seperator, $this->config['seo_url_seperator'], $html);
            $page->replaceTemplateSection('url_seperator_block', $html);
            //Default page
            $default_page = [];
            $default_page['blog_index'] = $lang['default_page_blog_index'];
            $default_page['wysiwyg_page'] = $lang['default_page_wysiwyg_page'];
            $html = $page->getTemplateSection('default_page_block');
            $html = $page->formOptions($default_page, $this->config['default_page'], $html);
            $page->replaceTemplateSection('default_page_block', $html);

            $page->replacePermissionTags();
            $page->replaceLangTemplateTags();
            $page->autoReplaceTags('', true);
            $display .= $page->returnPage();
        }
        return $display;
    }

    /**
     * Builds the HTML for the SEO Links Section site_config_seo_links.html
     *
     * @return string Return HTML
     */
    public function ajaxConfigureSeoLinks(): string
    {
        global $ORconn;

        $login = $this->newLogin();
        $misc = $this->newMisc();
        $security = $login->verifyPriv('edit_site_config');
        $display = '';
        if ($security === true) {
            $page = $this->newPageAdmin();
            //Load TEmplate File
            $page->loadPage($this->config['admin_template_path'] . '/site_config_seo_links.html');
            //Replace Tags

            //Deal with SEO URL Structures
            $sql = 'SELECT action,slug,uri FROM ' . $this->config['table_prefix_no_lang'] . 'seouri';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            while (!$recordSet->EOF) {
                $action = (string)$recordSet->fields('action');
                $slug = (string)$recordSet->fields('slug');
                $uri = (string)$recordSet->fields('uri');
                $page->replaceTag($action . '_slug', htmlentities($slug, ENT_COMPAT, $this->config['charset']));
                $page->replaceTag($action . '_uri', htmlentities($uri, ENT_COMPAT, $this->config['charset']));

                $recordSet->MoveNext();
            }
            $page->replaceTag('baseurl', htmlentities($this->config['baseurl'], ENT_COMPAT, $this->config['charset']));

            $page->replacePermissionTags();
            $page->replaceLangTemplateTags();
            $page->autoReplaceTags('', true);
            $display .= $page->returnPage();
        }
        return $display;
    }

    /**
     * Builds the HTML for the Uploads Section site_config_uploads.html
     *
     * @return string Return HTML
     */
    public function ajaxConfigureUploads(): string
    {
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_site_config');
        $display = '';
        if ($security === true) {
            $page = $this->newPageAdmin();
            //Load TEmplate File
            $page->loadPage($this->config['admin_template_path'] . '/site_config_uploads.html');
            //Load Jscript
            $thumbnail_prog = [];
            $thumbnail_prog['gd'] = 'GD Libs';
            $thumbnail_prog['imagemagick'] = 'ImageMagick';
            $resize_opts = [];
            $resize_opts['width'] = 'Width';
            $resize_opts['height'] = 'Height';
            $resize_opts['bestfit'] = 'Best Fit';
            $resize_opts['both'] = 'Both';
            $mainimage_opts = [];
            $mainimage_opts['width'] = 'Width';
            $mainimage_opts['height'] = 'Height';
            $mainimage_opts['both'] = 'Both';
            $filedisplay = [];
            $filedisplay['filename'] = 'Filename';
            $filedisplay['caption'] = 'Caption';
            $filedisplay['both'] = 'Both';

            $page->replaceTag('controlpanel_allowed_upload_extensions', htmlentities($this->config['allowed_upload_extensions'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_thumbnail_width', htmlentities((string)$this->config['thumbnail_width'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_thumbnail_height', htmlentities((string)$this->config['thumbnail_height'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_user_jpeg_quality', htmlentities((string)$this->config['user_jpeg_quality'], ENT_COMPAT, $this->config['charset']));
            $html = $page->getTemplateSection('user_resize_thumb_by_block');
            $html = $page->formOptions($resize_opts, $this->config['user_resize_thumb_by'], $html);
            $page->replaceTemplateSection('user_resize_thumb_by_block', $html);
            $html = $page->getTemplateSection('user_resize_img_block');
            $html = $page->formOptions(self::YES_NO, $this->config['user_resize_img'] ? '1' : '0', $html);
            $page->replaceTemplateSection('user_resize_img_block', $html);
            $html = $page->getTemplateSection('user_resize_by_block');
            $html = $page->formOptions($resize_opts, $this->config['user_resize_by'], $html);
            $page->replaceTemplateSection('user_resize_by_block', $html);
            $page->replaceTag('controlpanel_user_thumbnail_width', htmlentities((string)$this->config['user_thumbnail_width'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_user_thumbnail_height', htmlentities((string)$this->config['user_thumbnail_height'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_path_to_imagemagick', htmlentities($this->config['path_to_imagemagick'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_jpeg_quality', htmlentities((string)$this->config['jpeg_quality'], ENT_COMPAT, $this->config['charset']));
            $html = $page->getTemplateSection('make_thumbnail_block');
            $html = $page->formOptions(self::YES_NO, $this->config['make_thumbnail'] ? '1' : '0', $html);
            $page->replaceTemplateSection('make_thumbnail_block', $html);
            $html = $page->getTemplateSection('resize_thumb_by_block');
            $html = $page->formOptions($resize_opts, $this->config['resize_thumb_by'], $html);
            $page->replaceTemplateSection('resize_thumb_by_block', $html);
            $html = $page->getTemplateSection('thumbnail_prog_block');
            $html = $page->formOptions($thumbnail_prog, $this->config['thumbnail_prog'], $html);
            $page->replaceTemplateSection('thumbnail_prog_block', $html);
            $html = $page->getTemplateSection('resize_img_block');
            $html = $page->formOptions(self::YES_NO, $this->config['resize_img'] ? '1' : '0', $html);
            $page->replaceTemplateSection('resize_img_block', $html);
            $html = $page->getTemplateSection('resize_by_block');
            $html = $page->formOptions($resize_opts, $this->config['resize_by'], $html);
            $page->replaceTemplateSection('resize_by_block', $html);
            $html = $page->getTemplateSection('show_no_photo_block');
            $html = $page->formOptions(self::YES_NO, $this->config['show_no_photo'] ? '1' : '0', $html);
            $page->replaceTemplateSection('show_no_photo_block', $html);
            $html = $page->getTemplateSection('show_agent_no_photo_block');
            $html = $page->formOptions(self::YES_NO, $this->config['show_agent_no_photo'] ? '1' : '0', $html);
            $page->replaceTemplateSection('show_agent_no_photo_block', $html);
            $page->replaceTag('controlpanel_max_listings_uploads', htmlentities((string)$this->config['max_listings_uploads'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_max_listings_upload_size', htmlentities((string)$this->config['max_listings_upload_size'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_max_listings_upload_width', htmlentities((string)$this->config['max_listings_upload_width'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_max_listings_upload_height', htmlentities((string)$this->config['max_listings_upload_height'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_max_user_uploads', htmlentities((string)$this->config['max_user_uploads'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_max_user_upload_size', htmlentities((string)$this->config['max_user_upload_size'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_max_user_upload_width', htmlentities((string)$this->config['max_user_upload_width'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_max_user_upload_height', htmlentities((string)$this->config['max_user_upload_height'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_max_vtour_uploads', htmlentities((string)$this->config['max_vtour_uploads'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_max_vtour_upload_size', htmlentities((string)$this->config['max_vtour_upload_size'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_max_vtour_upload_width', htmlentities((string)$this->config['max_vtour_upload_width'], ENT_COMPAT, $this->config['charset']));

            $html = $page->getTemplateSection('main_image_display_by_block');
            $html = $page->formOptions($mainimage_opts, $this->config['main_image_display_by'], $html);
            $page->replaceTemplateSection('main_image_display_by_block', $html);
            $page->replaceTag('controlpanel_main_image_width', htmlentities((string)$this->config['main_image_width'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_main_image_height', htmlentities((string)$this->config['main_image_height'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_number_columns', htmlentities((string)$this->config['number_columns'], ENT_COMPAT, $this->config['charset']));

            $page->replaceTag('controlpanel_allowed_file_upload_extensions', htmlentities($this->config['allowed_file_upload_extensions'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_max_listings_file_uploads', htmlentities((string)$this->config['max_listings_file_uploads'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_max_listings_file_upload_size', htmlentities((string)$this->config['max_listings_file_upload_size'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_max_users_file_uploads', htmlentities((string)$this->config['max_users_file_uploads'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_max_users_file_upload_size', htmlentities((string)$this->config['max_users_file_upload_size'], ENT_COMPAT, $this->config['charset']));

            $html = $page->getTemplateSection('show_file_icon_block');
            $html = $page->formOptions(self::YES_NO, $this->config['show_file_icon'] ? "1" : "0", $html);
            $page->replaceTemplateSection('show_file_icon_block', $html);

            $html = $page->getTemplateSection('file_display_option_block');
            $html = $page->formOptions($filedisplay, $this->config['file_display_option'], $html);
            $page->replaceTemplateSection('file_display_option_block', $html);

            $html = $page->getTemplateSection('show_file_size_block');
            $html = $page->formOptions(self::YES_NO, $this->config['file_display_size'] ? '1' : '0', $html);
            $page->replaceTemplateSection('show_file_size_block', $html);

            $page->replaceTag('controlpanel_icon_image_width', htmlentities((string)$this->config['file_icon_width'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_icon_image_height', htmlentities((string)$this->config['file_icon_height'], ENT_COMPAT, $this->config['charset']));

            $page->replaceTag('controlpanel_vtour_width', htmlentities((string)$this->config['vtour_width'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_vtour_height', htmlentities((string)$this->config['vtour_height'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_vt_popup_width', htmlentities((string)$this->config['vt_popup_width'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_vt_popup_height', htmlentities((string)$this->config['vt_popup_height'], ENT_COMPAT, $this->config['charset']));

            $page->replaceTag('controlpanel_listingimages_slideshow_group_thumb', htmlentities((string)$this->config['listingimages_slideshow_group_thumb'], ENT_COMPAT, $this->config['charset']));
            $page->replacePermissionTags();
            $page->replaceLangTemplateTags();
            $page->autoReplaceTags('', true);
            $display .= $page->returnPage();
        }
        return $display;
    }

    /**
     * Builds the HTML for the Template Section site_config_templates.html
     *
     * @return string Return HTML
     */
    public function ajaxConfigureTemplates(): string
    {
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_site_config');
        $display = '';
        if ($security === true) {
            $page = $this->newPageAdmin();
            //Load TEmplate File
            $page->loadPage($this->config['admin_template_path'] . '/site_config_templates.html');

            // Get Site and mobile Template List
            $options = [];
            $directories = glob($this->config['basepath'] . '/template/*', GLOB_ONLYDIR);
            foreach ($directories as $file) {
                $file = basename($file);
                if ($file == 'default') {
                    continue;
                }
                $options[$file] = $file;
            }
            $html = $page->getTemplateSection('template_block');
            $html = $page->formOptions($options, $this->config['template'], $html);
            $page->replaceTemplateSection('template_block', $html);
            $html = $page->getTemplateSection('mobile_template_block');
            $html = $page->formOptions($options, $this->config['mobile_template'], $html);
            $page->replaceTemplateSection('mobile_template_block', $html);

            // Get Admin Template List
            $options = [];
            $directories = glob($this->config['basepath'] . '/admin/template/*', GLOB_ONLYDIR);
            foreach ($directories as $file) {
                $file = basename($file);
                if ($file == 'default') {
                    continue;
                }
                $options[$file] = $file;
            }
            $html = $page->getTemplateSection('admin_template_block');
            $html = $page->formOptions($options, $this->config['admin_template'], $html);
            $page->replaceTemplateSection('admin_template_block', $html);

            // Get Listing Template List
            $options = [];
            //Get default templates
            $files = glob($this->config['basepath'] . '/template/default/*');
            foreach ($files as $file) {
                $file = basename($file);
                if (str_starts_with($file, 'listing_detail')) {
                    $options[$file] = substr($file, 15, -5);
                }
            }
            //Get Custom listing detail Templates
            $files = glob($this->config['basepath'] . '/template/' . $this->config['template'] . '/*');
            foreach ($files as $file) {
                $file = basename($file);
                if (str_starts_with($file, 'listing_detail') && !str_starts_with($file, 'listing_detail_pclass')) {
                    $options[$file] = substr($file, 15, -5);
                }
            }
            $html = $page->getTemplateSection('listing_template_block');
            $html = $page->formOptions($options, $this->config['listing_template'], $html);
            $page->replaceTemplateSection('listing_template_block', $html);
            $page->replaceTag('controlpanel_template_listing_sections', htmlentities($this->config['template_listing_sections'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_template_lead_sections', htmlentities($this->config['template_lead_sections'], ENT_COMPAT, $this->config['charset']));

            // Get Search Result Template List
            $options = [];
            $files = glob($this->config['basepath'] . '/template/default/*');
            foreach ($files as $file) {
                $file = basename($file);
                if (str_starts_with($file, 'search_result')) {
                    $options[$file] = substr($file, 14, -5);
                }
            }
            //Get Custom search result Templates
            $files = glob($this->config['basepath'] . '/template/' . $this->config['template'] . '/*');
            foreach ($files as $file) {
                $file = basename($file);
                if (str_starts_with($file, 'search_result')) {
                    $options[$file] = substr($file, 14, -5);
                }
            }
            $html = $page->getTemplateSection('search_result_template_block');
            $html = $page->formOptions($options, $this->config['search_result_template'], $html);
            $page->replaceTemplateSection('search_result_template_block', $html);

            // Get View Agent Template List
            $options = [];
            $files = glob($this->config['basepath'] . '/template/default/*');
            foreach ($files as $file) {
                $file = basename($file);
                if (str_starts_with($file, 'view_user_')) {
                    $options[$file] = substr($file, 10, -5);
                }
            }
            //Get Custom View Agent Templates
            $files = glob($this->config['basepath'] . '/template/' . $this->config['template'] . '/*');
            foreach ($files as $file) {
                $file = basename($file);
                if (str_starts_with($file, 'view_user_')) {
                    $options[$file] = substr($file, 10, -5);
                }
            }
            $html = $page->getTemplateSection('agent_template_block');
            $html = $page->formOptions($options, $this->config['agent_template'], $html);
            $page->replaceTemplateSection('agent_template_block', $html);

            // Get VTour Template List
            $options = [];
            //Get default templates
            if ($handle = opendir($this->config['basepath'] . '/template/default/')) {
                while (false !== ($file = readdir($handle))) {
                    if ($file != '.' && $file != '..' && $file != 'CVS' && $file != '.svn') {
                        if (!is_dir($this->config['basepath'] . '/template/default/' . $file)) {
                            if (str_starts_with($file, 'vtour_')) {
                                $options[$file] = substr($file, 6, -5);
                            }
                        }
                    }
                }
                closedir($handle);
            }
            //Get Custom Vtour Templates
            if ($handle = opendir($this->config['basepath'] . '/template/' . $this->config['template'])) {
                while (false !== ($file = readdir($handle))) {
                    if ($file != '.' && $file != '..' && $file != 'CVS' && $file != '.svn') {
                        if (!is_dir($this->config['basepath'] . '/template/' . $this->config['template'] . '/' . $file)) {
                            if (str_starts_with($file, 'vtour_')) {
                                $options[$file] = substr($file, 6, -5);
                            }
                        }
                    }
                }
                closedir($handle);
            }
            $html = $page->getTemplateSection('vtour_template_block');
            $html = $page->formOptions($options, $this->config['vtour_template'], $html);
            $page->replaceTemplateSection('vtour_template_block', $html);

            // Get Notify New Listing Template List
            $options = [];
            //Get default templates
            if ($handle = opendir($this->config['basepath'] . '/template/default/')) {
                while (false !== ($file = readdir($handle))) {
                    if ($file != '.' && $file != '..' && $file != 'CVS' && $file != '.svn') {
                        if (!is_dir($this->config['basepath'] . '/template/default/' . $file)) {
                            if (str_starts_with($file, 'notify_listings_')) {
                                $options[$file] = substr($file, 16, -5);
                            }
                        }
                    }
                }
                closedir($handle);
            }
            //Get Custom Notify New Listing Templates
            if ($handle = opendir($this->config['basepath'] . '/template/' . $this->config['template'])) {
                while (false !== ($file = readdir($handle))) {
                    if ($file != '.' && $file != '..' && $file != 'CVS' && $file != '.svn') {
                        if (!is_dir($this->config['basepath'] . '/template/' . $this->config['template'] . '/' . $file)) {
                            if (str_starts_with($file, 'notify_listings_')) {
                                $options[$file] = substr($file, 16, -5);
                            }
                        }
                    }
                }
                closedir($handle);
            }
            $html = $page->getTemplateSection('notify_listings_template_block');
            $html = $page->formOptions($options, $this->config['notify_listings_template'], $html);
            $page->replaceTemplateSection('notify_listings_template_block', $html);

            // Get contact Agent Template List
            $options = [];
            //Get default templates
            if ($handle = opendir($this->config['basepath'] . '/template/default/')) {
                while (false !== ($file = readdir($handle))) {
                    if ($file != '.' && $file != '..' && $file != 'CVS' && $file != '.svn') {
                        if (!is_dir($this->config['basepath'] . '/template/default/' . $file)) {
                            if (str_starts_with($file, 'contact_agent_')) {
                                $options[$file] = substr($file, 14, -5);
                            }
                        }
                    }
                }
                closedir($handle);
            }
            //Get Custom contact Agent Template  Templates
            if ($handle = opendir($this->config['basepath'] . '/template/' . $this->config['template'])) {
                while (false !== ($file = readdir($handle))) {
                    if ($file != '.' && $file != '..' && $file != 'CVS' && $file != '.svn') {
                        if (!is_dir($this->config['basepath'] . '/template/' . $this->config['template'] . '/' . $file)) {
                            if (str_starts_with($file, 'contact_agent_')) {
                                $options[$file] = substr($file, 14, -5);
                            }
                        }
                    }
                }
                closedir($handle);
            }
            $html = $page->getTemplateSection('contact_template_block');
            $html = $page->formOptions($options, $this->config['contact_template'], $html);
            $page->replaceTemplateSection('contact_template_block', $html);

            $page->replacePermissionTags();
            $page->replaceLangTemplateTags();
            $page->autoReplaceTags('', true);
            $display .= $page->returnPage();
        }
        return $display;
    }

    /**
     * Builds the HTML for the Listing Section site_config_listings.html
     *
     * @return string Return HTML
     */
    public function ajaxConfigureListings(): string
    {
        global $ORconn, $lang;


        $login = $this->newLogin();
        $misc = $this->newMisc();
        $security = $login->verifyPriv('edit_site_config');
        $display = '';
        if ($security === true) {
            $page = $this->newPageAdmin();
            //Load TEmplate File
            $page->loadPage($this->config['admin_template_path'] . '/site_config_listings.html');
            // Listing Template Field Names for Map Field Selection
            $sql = 'SELECT listingsformelements_field_name, listingsformelements_field_caption FROM ' . $this->config['table_prefix'] . 'listingsformelements';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $listing_field_name_options = [];
            while (!$recordSet->EOF) {
                $field_name = (string)$recordSet->fields('listingsformelements_field_name');
                $listing_field_name_options[$field_name] = $field_name . ' (' . $recordSet->fields('listingsformelements_field_caption') . ')';
                $recordSet->MoveNext();
            }
            // Listing Template Field Names for Search Field Selection
            $sql = 'SELECT listingsformelements_field_name, listingsformelements_field_caption FROM ' . $this->config['table_prefix'] . "listingsformelements WHERE listingsformelements_display_on_browse = 'Yes'";
            $recordSet = $ORconn->Execute($sql);
            $search_field_sortby_options = [];
            $search_field_sortby_options['random'] = $lang['random'];
            $search_field_sortby_options['listingsdb_id'] = $lang['id_caption'];
            $search_field_sortby_options['listingsdb_title'] = $lang['title'];
            $search_field_sortby_options['listingsdb_featured'] = $lang['featured'];
            $search_field_sortby_options['listingsdb_last_modified'] = $lang['last_modified'];
            $search_field_special_sortby_options = [];
            $search_field_special_sortby_options['none'] = $lang['none'];
            $search_field_special_sortby_options['listingsdb_featured'] = $lang['featured'];
            $search_field_special_sortby_options['listingsdb_id'] = $lang['id_caption'];
            $search_field_special_sortby_options['listingsdb_title'] = $lang['title'];
            $search_field_special_sortby_options['listingsdb_last_modified'] = $lang['last_modified'];
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            while (!$recordSet->EOF) {
                $field_name = (string)$recordSet->fields('listingsformelements_field_name');
                $search_field_sortby_options[$field_name] = $field_name . ' (' . $recordSet->fields('listingsformelements_field_caption') . ')';
                $search_field_special_sortby_options[$field_name] = $field_name . ' (' . $recordSet->fields('listingsformelements_field_caption') . ')';
                $recordSet->MoveNext();
            }
            $number_format = [];
            $number_format[1] = '1,000.00';
            $number_format[2] = '1.000,00';
            $number_format[3] = '1 000.00';
            $number_format[4] = '1 000,00';
            $number_format[5] = '1\'000,00';
            $number_format[6] = '1-000 00';
            $money_format = [];
            $money_format[1] = htmlentities($this->config['money_sign'], ENT_COMPAT, $this->config['charset']) . '1';
            $money_format[2] = '1' . htmlentities($this->config['money_sign'], ENT_COMPAT, $this->config['charset']);
            $money_format[3] = htmlentities($this->config['money_sign'], ENT_COMPAT, $this->config['charset']) . ' 1';
            $date_format = [];
            $date_format[1] = 'mm/dd/yyyy';
            $date_format[2] = 'yyyy/dd/mm';
            $date_format[3] = 'dd/mm/yyyy';
            //Map Types
            // New Global Maps
            $map_types = [];
            $map_types['global_mapquest'] = $lang['global_mapquest'];
            $map_types['global_multimap'] = $lang['global_multimap'];
            // Map Options
            $map_types['mapquest_AD'] = $lang['mapquest_AD'];
            $map_types['mapquest_AE'] = $lang['mapquest_AE'];
            $map_types['mapquest_AF'] = $lang['mapquest_AF'];
            $map_types['mapquest_AG'] = $lang['mapquest_AG'];
            $map_types['mapquest_AI'] = $lang['mapquest_AI'];
            $map_types['mapquest_AL'] = $lang['mapquest_AL'];
            $map_types['mapquest_AM'] = $lang['mapquest_AM'];
            $map_types['mapquest_AN'] = $lang['mapquest_AN'];
            $map_types['mapquest_AO'] = $lang['mapquest_AO'];
            $map_types['mapquest_AR'] = $lang['mapquest_AR'];
            $map_types['mapquest_AS'] = $lang['mapquest_AS'];
            $map_types['mapquest_AT'] = $lang['mapquest_AT'];
            $map_types['mapquest_AU'] = $lang['mapquest_AU'];
            $map_types['mapquest_AW'] = $lang['mapquest_AW'];
            $map_types['mapquest_AZ'] = $lang['mapquest_AZ'];
            $map_types['mapquest_BA'] = $lang['mapquest_BA'];
            $map_types['mapquest_BB'] = $lang['mapquest_BB'];
            $map_types['mapquest_BD'] = $lang['mapquest_BD'];
            $map_types['mapquest_BE'] = $lang['mapquest_BE'];
            $map_types['mapquest_BF'] = $lang['mapquest_BF'];
            $map_types['mapquest_BG'] = $lang['mapquest_BG'];
            $map_types['mapquest_BH'] = $lang['mapquest_BH'];
            $map_types['mapquest_BI'] = $lang['mapquest_BI'];
            $map_types['mapquest_BJ'] = $lang['mapquest_BJ'];
            $map_types['mapquest_BM'] = $lang['mapquest_BM'];
            $map_types['mapquest_BN'] = $lang['mapquest_BN'];
            $map_types['mapquest_BO'] = $lang['mapquest_BO'];
            $map_types['mapquest_BR'] = $lang['mapquest_BR'];
            $map_types['mapquest_BS'] = $lang['mapquest_BS'];
            $map_types['mapquest_BT'] = $lang['mapquest_BT'];
            $map_types['mapquest_BV'] = $lang['mapquest_BV'];
            $map_types['mapquest_BW'] = $lang['mapquest_BW'];
            $map_types['mapquest_BY'] = $lang['mapquest_BY'];
            $map_types['mapquest_BZ'] = $lang['mapquest_BZ'];
            $map_types['mapquest_CA'] = $lang['mapquest_CA'];
            $map_types['mapquest_CC'] = $lang['mapquest_CC'];
            $map_types['mapquest_CD'] = $lang['mapquest_CD'];
            $map_types['mapquest_CF'] = $lang['mapquest_CF'];
            $map_types['mapquest_CG'] = $lang['mapquest_CG'];
            $map_types['mapquest_CH'] = $lang['mapquest_CH'];
            $map_types['mapquest_CI'] = $lang['mapquest_CI'];
            $map_types['mapquest_CK'] = $lang['mapquest_CK'];
            $map_types['mapquest_CL'] = $lang['mapquest_CL'];
            $map_types['mapquest_CM'] = $lang['mapquest_CM'];
            $map_types['mapquest_CN'] = $lang['mapquest_CN'];
            $map_types['mapquest_CO'] = $lang['mapquest_CO'];
            $map_types['mapquest_CR'] = $lang['mapquest_CR'];
            $map_types['mapquest_CS'] = $lang['mapquest_CS'];
            $map_types['mapquest_CU'] = $lang['mapquest_CU'];
            $map_types['mapquest_CV'] = $lang['mapquest_CV'];
            $map_types['mapquest_CX'] = $lang['mapquest_CX'];
            $map_types['mapquest_CY'] = $lang['mapquest_CY'];
            $map_types['mapquest_CZ'] = $lang['mapquest_CZ'];
            $map_types['mapquest_DE'] = $lang['mapquest_DE'];
            $map_types['mapquest_DJ'] = $lang['mapquest_DJ'];
            $map_types['mapquest_DK'] = $lang['mapquest_DK'];
            $map_types['mapquest_DM'] = $lang['mapquest_DM'];
            $map_types['mapquest_DO'] = $lang['mapquest_DO'];
            $map_types['mapquest_DZ'] = $lang['mapquest_DZ'];
            $map_types['mapquest_EC'] = $lang['mapquest_EC'];
            $map_types['mapquest_EE'] = $lang['mapquest_EE'];
            $map_types['mapquest_EG'] = $lang['mapquest_EG'];
            $map_types['mapquest_EH'] = $lang['mapquest_EH'];
            $map_types['mapquest_ER'] = $lang['mapquest_ER'];
            $map_types['mapquest_ES'] = $lang['mapquest_ES'];
            $map_types['mapquest_ET'] = $lang['mapquest_ET'];
            $map_types['mapquest_FI'] = $lang['mapquest_FI'];
            $map_types['mapquest_FJ'] = $lang['mapquest_FJ'];
            $map_types['mapquest_FK'] = $lang['mapquest_FK'];
            $map_types['mapquest_FM'] = $lang['mapquest_FM'];
            $map_types['mapquest_FO'] = $lang['mapquest_FO'];
            $map_types['mapquest_FR'] = $lang['mapquest_FR'];
            $map_types['multimap_FR'] = $lang['multimap_FR'];
            $map_types['mapquest_GA'] = $lang['mapquest_GA'];
            $map_types['mapquest_GB'] = $lang['mapquest_GB'];
            $map_types['mapquest_GD'] = $lang['mapquest_GD'];
            $map_types['mapquest_GE'] = $lang['mapquest_GE'];
            $map_types['mapquest_GF'] = $lang['mapquest_GF'];
            $map_types['mapquest_GH'] = $lang['mapquest_GH'];
            $map_types['mapquest_GI'] = $lang['mapquest_GI'];
            $map_types['mapquest_GL'] = $lang['mapquest_GL'];
            $map_types['mapquest_GM'] = $lang['mapquest_GM'];
            $map_types['mapquest_GN'] = $lang['mapquest_GN'];
            $map_types['mapquest_GP'] = $lang['mapquest_GP'];
            $map_types['mapquest_GQ'] = $lang['mapquest_GQ'];
            $map_types['mapquest_GR'] = $lang['mapquest_GR'];
            $map_types['mapquest_GS'] = $lang['mapquest_GS'];
            $map_types['mapquest_GT'] = $lang['mapquest_GT'];
            $map_types['mapquest_GU'] = $lang['mapquest_GU'];
            $map_types['mapquest_GW'] = $lang['mapquest_GW'];
            $map_types['mapquest_GY'] = $lang['mapquest_GY'];
            $map_types['mapquest_GZ'] = $lang['mapquest_GZ'];
            $map_types['mapquest_HK'] = $lang['mapquest_HK'];
            $map_types['mapquest_HM'] = $lang['mapquest_HM'];
            $map_types['mapquest_HN'] = $lang['mapquest_HN'];
            $map_types['mapquest_HR'] = $lang['mapquest_HR'];
            $map_types['mapquest_HT'] = $lang['mapquest_HT'];
            $map_types['mapquest_HU'] = $lang['mapquest_HU'];
            $map_types['mapquest_ID'] = $lang['mapquest_ID'];
            $map_types['mapquest_IE'] = $lang['mapquest_IE'];
            $map_types['mapquest_IL'] = $lang['mapquest_IL'];
            $map_types['mapquest_IN'] = $lang['mapquest_IN'];
            $map_types['mapquest_IO'] = $lang['mapquest_IO'];
            $map_types['mapquest_IQ'] = $lang['mapquest_IQ'];
            $map_types['mapquest_IR'] = $lang['mapquest_IR'];
            $map_types['mapquest_IS'] = $lang['mapquest_IS'];
            $map_types['mapquest_IT'] = $lang['mapquest_IT'];
            $map_types['mapquest_JM'] = $lang['mapquest_JM'];
            $map_types['mapquest_JO'] = $lang['mapquest_JO'];
            $map_types['mapquest_JP'] = $lang['mapquest_JP'];
            $map_types['mapquest_KE'] = $lang['mapquest_KE'];
            $map_types['mapquest_KG'] = $lang['mapquest_KG'];
            $map_types['mapquest_KH'] = $lang['mapquest_KH'];
            $map_types['mapquest_KI'] = $lang['mapquest_KI'];
            $map_types['mapquest_KM'] = $lang['mapquest_KM'];
            $map_types['mapquest_KN'] = $lang['mapquest_KN'];
            $map_types['mapquest_KP'] = $lang['mapquest_KP'];
            $map_types['mapquest_KR'] = $lang['mapquest_KR'];
            $map_types['mapquest_KW'] = $lang['mapquest_KW'];
            $map_types['mapquest_KY'] = $lang['mapquest_KY'];
            $map_types['mapquest_KZ'] = $lang['mapquest_KZ'];
            $map_types['mapquest_LA'] = $lang['mapquest_LA'];
            $map_types['mapquest_LB'] = $lang['mapquest_LB'];
            $map_types['mapquest_LC'] = $lang['mapquest_LC'];
            $map_types['mapquest_LI'] = $lang['mapquest_LI'];
            $map_types['mapquest_LK'] = $lang['mapquest_LK'];
            $map_types['mapquest_LR'] = $lang['mapquest_LR'];
            $map_types['mapquest_LS'] = $lang['mapquest_LS'];
            $map_types['mapquest_LT'] = $lang['mapquest_LT'];
            $map_types['mapquest_LU'] = $lang['mapquest_LU'];
            $map_types['mapquest_LV'] = $lang['mapquest_LV'];
            $map_types['mapquest_LY'] = $lang['mapquest_LY'];
            $map_types['mapquest_MA'] = $lang['mapquest_MA'];
            $map_types['mapquest_MC'] = $lang['mapquest_MC'];
            $map_types['mapquest_MD'] = $lang['mapquest_MD'];
            $map_types['mapquest_MG'] = $lang['mapquest_MG'];
            $map_types['mapquest_MH'] = $lang['mapquest_MH'];
            $map_types['mapquest_MK'] = $lang['mapquest_MK'];
            $map_types['mapquest_ML'] = $lang['mapquest_ML'];
            $map_types['mapquest_MM'] = $lang['mapquest_MM'];
            $map_types['mapquest_MN'] = $lang['mapquest_MN'];
            $map_types['mapquest_MO'] = $lang['mapquest_MO'];
            $map_types['mapquest_MP'] = $lang['mapquest_MP'];
            $map_types['mapquest_MQ'] = $lang['mapquest_MQ'];
            $map_types['mapquest_MR'] = $lang['mapquest_MR'];
            $map_types['mapquest_MS'] = $lang['mapquest_MS'];
            $map_types['mapquest_MT'] = $lang['mapquest_MT'];
            $map_types['mapquest_MU'] = $lang['mapquest_MU'];
            $map_types['mapquest_MV'] = $lang['mapquest_MV'];
            $map_types['mapquest_MW'] = $lang['mapquest_MW'];
            $map_types['mapquest_MX'] = $lang['mapquest_MX'];
            $map_types['mapquest_MY'] = $lang['mapquest_MY'];
            $map_types['mapquest_MZ'] = $lang['mapquest_MZ'];
            $map_types['mapquest_NA'] = $lang['mapquest_NA'];
            $map_types['mapquest_NC'] = $lang['mapquest_NC'];
            $map_types['mapquest_NE'] = $lang['mapquest_NE'];
            $map_types['mapquest_NF'] = $lang['mapquest_NF'];
            $map_types['mapquest_NG'] = $lang['mapquest_NG'];
            $map_types['mapquest_NI'] = $lang['mapquest_NI'];
            $map_types['mapquest_NL'] = $lang['mapquest_NL'];
            $map_types['mapquest_NO'] = $lang['mapquest_NO'];
            $map_types['mapquest_NP'] = $lang['mapquest_NP'];
            $map_types['mapquest_NR'] = $lang['mapquest_NR'];
            $map_types['mapquest_NU'] = $lang['mapquest_NU'];
            $map_types['mapquest_NZ'] = $lang['mapquest_NZ'];
            $map_types['mapquest_OM'] = $lang['mapquest_OM'];
            $map_types['mapquest_PA'] = $lang['mapquest_PA'];
            $map_types['mapquest_PE'] = $lang['mapquest_PE'];
            $map_types['mapquest_PF'] = $lang['mapquest_PF'];
            $map_types['mapquest_PG'] = $lang['mapquest_PG'];
            $map_types['mapquest_PH'] = $lang['mapquest_PH'];
            $map_types['mapquest_PK'] = $lang['mapquest_PK'];
            $map_types['mapquest_PL'] = $lang['mapquest_PL'];
            $map_types['mapquest_PM'] = $lang['mapquest_PM'];
            $map_types['mapquest_PN'] = $lang['mapquest_PN'];
            $map_types['mapquest_PR'] = $lang['mapquest_PR'];
            $map_types['mapquest_PS'] = $lang['mapquest_PS'];
            $map_types['mapquest_PT'] = $lang['mapquest_PT'];
            $map_types['mapquest_PW'] = $lang['mapquest_PW'];
            $map_types['mapquest_PY'] = $lang['mapquest_PY'];
            $map_types['mapquest_QA'] = $lang['mapquest_QA'];
            $map_types['mapquest_RE'] = $lang['mapquest_RE'];
            $map_types['mapquest_RO'] = $lang['mapquest_RO'];
            $map_types['mapquest_RU'] = $lang['mapquest_RU'];
            $map_types['mapquest_RW'] = $lang['mapquest_RW'];
            $map_types['mapquest_SA'] = $lang['mapquest_SA'];
            $map_types['mapquest_SB'] = $lang['mapquest_SB'];
            $map_types['mapquest_SC'] = $lang['mapquest_SC'];
            $map_types['mapquest_SD'] = $lang['mapquest_SD'];
            $map_types['mapquest_SE'] = $lang['mapquest_SE'];
            $map_types['mapquest_SG'] = $lang['mapquest_SG'];
            $map_types['mapquest_SH'] = $lang['mapquest_SH'];
            $map_types['mapquest_SI'] = $lang['mapquest_SI'];
            $map_types['mapquest_SJ'] = $lang['mapquest_SJ'];
            $map_types['mapquest_SK'] = $lang['mapquest_SK'];
            $map_types['mapquest_SL'] = $lang['mapquest_SL'];
            $map_types['mapquest_SM'] = $lang['mapquest_SM'];
            $map_types['mapquest_SN'] = $lang['mapquest_SN'];
            $map_types['mapquest_SO'] = $lang['mapquest_SO'];
            $map_types['mapquest_SR'] = $lang['mapquest_SR'];
            $map_types['mapquest_ST'] = $lang['mapquest_ST'];
            $map_types['mapquest_SV'] = $lang['mapquest_SV'];
            $map_types['mapquest_SY'] = $lang['mapquest_SY'];
            $map_types['mapquest_SZ'] = $lang['mapquest_SZ'];
            $map_types['mapquest_TC'] = $lang['mapquest_TC'];
            $map_types['mapquest_TD'] = $lang['mapquest_TD'];
            $map_types['mapquest_TF'] = $lang['mapquest_TF'];
            $map_types['mapquest_TG'] = $lang['mapquest_TG'];
            $map_types['mapquest_TH'] = $lang['mapquest_TH'];
            $map_types['mapquest_TJ'] = $lang['mapquest_TJ'];
            $map_types['mapquest_TK'] = $lang['mapquest_TK'];
            $map_types['mapquest_TM'] = $lang['mapquest_TM'];
            $map_types['mapquest_TN'] = $lang['mapquest_TN'];
            $map_types['mapquest_TO'] = $lang['mapquest_TO'];
            $map_types['mapquest_TP'] = $lang['mapquest_TP'];
            $map_types['mapquest_TR'] = $lang['mapquest_TR'];
            $map_types['mapquest_TT'] = $lang['mapquest_TT'];
            $map_types['mapquest_TV'] = $lang['mapquest_TV'];
            $map_types['mapquest_TW'] = $lang['mapquest_TW'];
            $map_types['mapquest_TZ'] = $lang['mapquest_TZ'];
            $map_types['mapquest_UA'] = $lang['mapquest_UA'];
            $map_types['mapquest_UG'] = $lang['mapquest_UG'];
            $map_types['multimap_GB'] = $lang['multimap_uk'];
            $map_types['google_us'] = $lang['google_us'];
            $map_types['mapquest_US'] = $lang['mapquest_US'];
            $map_types['yahoo_us'] = $lang['yahoo_us'];
            $map_types['mapquest_UY'] = $lang['mapquest_UY'];
            $map_types['mapquest_UZ'] = $lang['mapquest_UZ'];
            $map_types['mapquest_VA'] = $lang['mapquest_VA'];
            $map_types['mapquest_VC'] = $lang['mapquest_VC'];
            $map_types['mapquest_VE'] = $lang['mapquest_VE'];
            $map_types['mapquest_VG'] = $lang['mapquest_VG'];
            $map_types['mapquest_VI'] = $lang['mapquest_VI'];
            $map_types['mapquest_VN'] = $lang['mapquest_VN'];
            $map_types['mapquest_VU'] = $lang['mapquest_VU'];
            $map_types['mapquest_WF'] = $lang['mapquest_WF'];
            $map_types['mapquest_WS'] = $lang['mapquest_WS'];
            $map_types['mapquest_YE'] = $lang['mapquest_YE'];
            $map_types['mapquest_YT'] = $lang['mapquest_YT'];
            $map_types['mapquest_ZA'] = $lang['mapquest_ZA'];
            $map_types['mapquest_ZM'] = $lang['mapquest_ZM'];
            $map_types['mapquest_ZW'] = $lang['mapquest_ZW'];
            $html = $page->getTemplateSection('number_format_style_block');
            $html = $page->formOptions($number_format, (string)$this->config['number_format_style'], $html);
            $page->replaceTemplateSection('number_format_style_block', $html);

            $page->replaceTag('controlpanel_number_decimals_number_fields', htmlentities((string)$this->config['number_decimals_number_fields'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_number_decimals_price_fields', htmlentities((string)$this->config['number_decimals_price_fields'], ENT_COMPAT, $this->config['charset']));

            $html = $page->getTemplateSection('force_decimals_block');
            $html = $page->formOptions(self::YES_NO, $this->config['force_decimals'] ? '1' : '0', $html);
            $page->replaceTemplateSection('force_decimals_block', $html);

            $html = $page->getTemplateSection('money_format_block');
            $html = $page->formOptions($money_format, (string)$this->config['money_format'], $html);
            $page->replaceTemplateSection('money_format_block', $html);

            $page->replaceTag('controlpanel_money_sign', htmlentities($this->config['money_sign'], ENT_COMPAT, $this->config['charset']));

            $html = $page->getTemplateSection('date_format_block');
            $html = $page->formOptions($date_format, (string)$this->config['date_format'], $html);
            $page->replaceTemplateSection('date_format_block', $html);

            $html = $page->getTemplateSection('zero_price_block');
            $html = $page->formOptions(self::YES_NO, $this->config['zero_price'] ? '1' : '0', $html);
            $page->replaceTemplateSection('zero_price_block', $html);

            $html = $page->getTemplateSection('price_field_block');
            $html = $page->formOptions($listing_field_name_options, $this->config['price_field'], $html);
            $page->replaceTemplateSection('price_field_block', $html);

            $page->replaceTag('controlpanel_num_featured_listings', htmlentities((string)$this->config['num_featured_listings'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_num_popular_listings', htmlentities((string)$this->config['num_popular_listings'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_num_random_listings', htmlentities((string)$this->config['num_random_listings'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_num_latest_listings', htmlentities((string)$this->config['num_latest_listings'], ENT_COMPAT, $this->config['charset']));

            $html = $page->getTemplateSection('use_expiration_block');
            $html = $page->formOptions(self::YES_NO, $this->config['use_expiration'] ? '1' : '0', $html);
            $page->replaceTemplateSection('use_expiration_block', $html);

            $page->replaceTag('controlpanel_days_until_listings_expire', htmlentities((string)$this->config['days_until_listings_expire'], ENT_COMPAT, $this->config['charset']));

            $html = $page->getTemplateSection('moderate_listings_block');
            $html = $page->formOptions(self::YES_NO, $this->config['moderate_listings'] ? '1' : '0', $html);
            $page->replaceTemplateSection('moderate_listings_block', $html);

            $html = $page->getTemplateSection('export_listings_block');
            $html = $page->formOptions(self::YES_NO, $this->config['export_listings'] ? '1' : '0', $html);
            $page->replaceTemplateSection('export_listings_block', $html);

            $html = $page->getTemplateSection('show_listedby_admin_block');
            $html = $page->formOptions(self::YES_NO, $this->config['show_listedby_admin'] ? '1' : '0', $html);
            $page->replaceTemplateSection('show_listedby_admin_block', $html);

            $html = $page->getTemplateSection('show_next_prev_listing_page_block');
            $html = $page->formOptions(self::YES_NO, $this->config['show_next_prev_listing_page'] ? '1' : '0', $html);
            $page->replaceTemplateSection('show_next_prev_listing_page_block', $html);

            $html = $page->getTemplateSection('show_notes_field_block');
            $html = $page->formOptions(self::YES_NO, $this->config['show_notes_field'] ? '1' : '0', $html);
            $page->replaceTemplateSection('show_notes_field_block', $html);

            $page->replaceTag('controlpanel_feature_list_separator', htmlentities($this->config['feature_list_separator'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_admin_listing_per_page', htmlentities((string)$this->config['admin_listing_per_page'], ENT_COMPAT, $this->config['charset']));

            $page->replaceTag('controlpanel_search_step_max', htmlentities((string)$this->config['search_step_max'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_listings_per_page', htmlentities((string)$this->config['listings_per_page'], ENT_COMPAT, $this->config['charset']));

            $html = $page->getTemplateSection('search_sortby_block');
            $html = $page->formOptions($search_field_sortby_options, $this->config['sortby'], $html);
            $page->replaceTemplateSection('search_sortby_block', $html);

            $html = $page->getTemplateSection('search_sorttype_block');
            $html = $page->formOptions(self::ASC_DESC, $this->config['sorttype'], $html);
            $page->replaceTemplateSection('search_sorttype_block', $html);

            $html = $page->getTemplateSection('special_search_sortby_block');
            $html = $page->formOptions($search_field_special_sortby_options, $this->config['special_sortby'], $html);
            $page->replaceTemplateSection('special_search_sortby_block', $html);

            $html = $page->getTemplateSection('special_search_sorttype_block');
            $html = $page->formOptions(self::ASC_DESC, $this->config['special_sorttype'], $html);
            $page->replaceTemplateSection('special_search_sorttype_block', $html);

            $html = $page->getTemplateSection('configured_show_count_block');
            $html = $page->formOptions(self::YES_NO, $this->config['configured_show_count'] ? '1' : '0', $html);
            $page->replaceTemplateSection('configured_show_count_block', $html);

            $page->replaceTag('controlpanel_max_search_results', htmlentities((string)$this->config['max_search_results'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_search_list_separator', htmlentities($this->config['search_list_separator'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_textarea_short_chars', htmlentities((string)$this->config['textarea_short_chars'], ENT_COMPAT, $this->config['charset']));

            $html = $page->getTemplateSection('map_type_block');
            $html = $page->formOptions($map_types, $this->config['map_type'], $html);
            $page->replaceTemplateSection('map_type_block', $html);

            $html = $page->getTemplateSection('map_address_block');
            $html = $page->formOptions($listing_field_name_options, $this->config['map_address'], $html);
            $page->replaceTemplateSection('map_address_block', $html);

            $html = $page->getTemplateSection('map_address2_block');
            $html = $page->formOptions($listing_field_name_options, $this->config['map_address2'], $html);
            $page->replaceTemplateSection('map_address2_block', $html);

            $html = $page->getTemplateSection('map_address3_block');
            $html = $page->formOptions($listing_field_name_options, $this->config['map_address3'], $html);
            $page->replaceTemplateSection('map_address3_block', $html);

            $html = $page->getTemplateSection('map_address4_block');
            $html = $page->formOptions($listing_field_name_options, $this->config['map_address4'], $html);
            $page->replaceTemplateSection('map_address4_block', $html);

            $html = $page->getTemplateSection('map_city_block');
            $html = $page->formOptions($listing_field_name_options, $this->config['map_city'], $html);
            $page->replaceTemplateSection('map_city_block', $html);

            $html = $page->getTemplateSection('map_state_block');
            $html = $page->formOptions($listing_field_name_options, $this->config['map_state'], $html);
            $page->replaceTemplateSection('map_state_block', $html);

            $html = $page->getTemplateSection('map_zip_block');
            $html = $page->formOptions($listing_field_name_options, $this->config['map_zip'], $html);
            $page->replaceTemplateSection('map_zip_block', $html);

            $html = $page->getTemplateSection('map_country_block');
            $html = $page->formOptions($listing_field_name_options, $this->config['map_country'], $html);
            $page->replaceTemplateSection('map_country_block', $html);

            $html = $page->getTemplateSection('map_latitude_block');
            $html = $page->formOptions($listing_field_name_options, $this->config['map_latitude'], $html);
            $page->replaceTemplateSection('map_latitude_block', $html);

            $html = $page->getTemplateSection('map_longitude_block');
            $html = $page->formOptions($listing_field_name_options, $this->config['map_longitude'], $html);
            $page->replaceTemplateSection('map_longitude_block', $html);

            $page->replacePermissionTags();
            $page->replaceLangTemplateTags();
            $page->autoReplaceTags('', true);
            $display .= $page->returnPage();
        }

        return $display;
    }

    /**
     * Builds the HTML for the Users Section site_config_users.html
     *
     * @return string Return HTML
     */
    public function ajaxConfigureUsers(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_site_config');
        $display = '';
        if ($security === true) {
            $page = $this->newPageAdmin();
            //Load TEmplate File
            $page->loadPage($this->config['admin_template_path'] . '/site_config_users.html');
            // Agent Template Field Names for Vcard Selection
            $sql = 'SELECT agentformelements_field_name, agentformelements_field_caption FROM ' . $this->config['table_prefix'] . 'agentformelements';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $agent_field_name_options = [];
            while (!$recordSet->EOF) {
                $field_name = (string)$recordSet->fields('agentformelements_field_name');
                $agent_field_name_options[$field_name] = $field_name . ' (' . $recordSet->fields('agentformelements_field_caption') . ')';
                $recordSet->MoveNext();
            }

            $html = $page->getTemplateSection('use_signup_image_verification_block');
            $html = $page->formOptions(self::YES_NO, $this->config['use_signup_image_verification'] ? '1' : '0', $html);
            $page->replaceTemplateSection('use_signup_image_verification_block', $html);

            $html = $page->getTemplateSection('require_email_verification_block');
            $html = $page->formOptions(self::YES_NO, $this->config['require_email_verification'] ? '1' : '0', $html);
            $page->replaceTemplateSection('require_email_verification_block', $html);

            $html = $page->getTemplateSection('moderate_members_block');
            $html = $page->formOptions(self::YES_NO, $this->config['moderate_members'] ? '1' : '0', $html);
            $page->replaceTemplateSection('moderate_members_block', $html);

            $html = $page->getTemplateSection('allow_member_signup_block');
            $html = $page->formOptions(self::YES_NO, $this->config['allow_member_signup'] ? '1' : '0', $html);
            $page->replaceTemplateSection('allow_member_signup_block', $html);

            //Agent Permissions
            $html = $page->getTemplateSection('moderate_agents_block');
            $html = $page->formOptions(self::YES_NO, $this->config['moderate_agents'] ? '1' : '0', $html);
            $page->replaceTemplateSection('moderate_agents_block', $html);

            $html = $page->getTemplateSection('allow_agent_signup_block');
            $html = $page->formOptions(self::YES_NO, $this->config['allow_agent_signup'] ? '1' : '0', $html);
            $page->replaceTemplateSection('allow_agent_signup_block', $html);

            $html = $page->getTemplateSection('agent_default_active_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_active'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_active_block', $html);

            $html = $page->getTemplateSection('agent_default_admin_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_admin'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_admin_block', $html);

            $html = $page->getTemplateSection('agent_default_edit_all_users_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_edit_all_users'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_edit_all_users_block', $html);

            $html = $page->getTemplateSection('agent_default_edit_all_listings_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_edit_all_listings'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_edit_all_listings_block', $html);

            $html = $page->getTemplateSection('agent_default_feature_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_feature'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_feature_block', $html);

            $html = $page->getTemplateSection('agent_default_moderate_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_moderate'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_moderate_block', $html);

            $html = $page->getTemplateSection('agent_default_logview_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_logview'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_logview_block', $html);

            $html = $page->getTemplateSection('agent_default_edit_site_config_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_edit_site_config'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_edit_site_config_block', $html);

            $html = $page->getTemplateSection('agent_default_edit_member_template_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_edit_member_template'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_edit_member_template_block', $html);

            $html = $page->getTemplateSection('agent_default_edit_agent_template_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_edit_agent_template'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_edit_agent_template_block', $html);

            $html = $page->getTemplateSection('agent_default_edit_listing_template_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_edit_listing_template'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_edit_listing_template_block', $html);

            $html = $page->getTemplateSection('agent_default_can_export_listings_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_can_export_listings'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_can_export_listings_block', $html);

            $html = $page->getTemplateSection('agent_default_canchangeexpirations_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_canchangeexpirations'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_canchangeexpirations_block', $html);

            $html = $page->getTemplateSection('agent_default_editpages_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_editpages'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_editpages_block', $html);

            $html = $page->getTemplateSection('agent_default_havevtours_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_havevtours'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_havevtours_block', $html);

            $html = $page->getTemplateSection('agent_default_havefiles_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_havefiles'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_havefiles_block', $html);

            $html = $page->getTemplateSection('agent_default_canManageAddons_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_canManageAddons'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_canManageAddons_block', $html);

            $blog_type = [1 => $lang['blog_perm_subscriber'], 2 => $lang['blog_perm_contributor'], 3 => $lang['blog_perm_author'], 4 => $lang['blog_perm_editor']];
            $html = $page->getTemplateSection('agent_default_blogUserType_block');
            $html = $page->formOptions($blog_type, (string)$this->config['agent_default_blogUserType'], $html);
            $page->replaceTemplateSection('agent_default_blogUserType_block', $html);

            $html = $page->getTemplateSection('agent_default_edit_all_leads_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_edit_all_leads'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_edit_all_leads_block', $html);
            //$this->config["agent_default_edit_lead_template"]


            $html = $page->getTemplateSection('agent_default_edit_lead_template_block');
            $html = $page->formOptions(self::YES_NO, $this->config['agent_default_edit_lead_template'] ? '1' : '0', $html);
            $page->replaceTemplateSection('agent_default_edit_lead_template_block', $html);

            $page->replaceTag('controlpanel_agent_default_num_listings', htmlentities((string)$this->config['agent_default_num_listings'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_agent_default_num_featuredlistings', htmlentities((string)$this->config['agent_default_num_featuredlistings'], ENT_COMPAT, $this->config['charset']));

            $page->replaceTag('controlpanel_users_per_page', htmlentities((string)$this->config['users_per_page'], ENT_COMPAT, $this->config['charset']));

            $html = $page->getTemplateSection('show_admin_on_agent_list_block');
            $html = $page->formOptions(self::YES_NO, $this->config['show_admin_on_agent_list'] ? '1' : '0', $html);
            $page->replaceTemplateSection('show_admin_on_agent_list_block', $html);

            //Agent VCard
            $html = $page->getTemplateSection('vcard_phone_block');
            $html = $page->formOptions($agent_field_name_options, $this->config['vcard_phone'], $html);
            $page->replaceTemplateSection('vcard_phone_block', $html);

            $html = $page->getTemplateSection('vcard_fax_block');
            $html = $page->formOptions($agent_field_name_options, $this->config['vcard_fax'], $html);
            $page->replaceTemplateSection('vcard_fax_block', $html);

            $html = $page->getTemplateSection('vcard_mobile_block');
            $html = $page->formOptions($agent_field_name_options, $this->config['vcard_mobile'], $html);
            $page->replaceTemplateSection('vcard_mobile_block', $html);

            $html = $page->getTemplateSection('vcard_address_block');
            $html = $page->formOptions($agent_field_name_options, $this->config['vcard_address'], $html);
            $page->replaceTemplateSection('vcard_address_block', $html);

            $html = $page->getTemplateSection('vcard_city_block');
            $html = $page->formOptions($agent_field_name_options, $this->config['vcard_city'], $html);
            $page->replaceTemplateSection('vcard_city_block', $html);

            $html = $page->getTemplateSection('vcard_state_block');
            $html = $page->formOptions($agent_field_name_options, $this->config['vcard_state'], $html);
            $page->replaceTemplateSection('vcard_state_block', $html);

            $html = $page->getTemplateSection('vcard_zip_block');
            $html = $page->formOptions($agent_field_name_options, $this->config['vcard_zip'], $html);
            $page->replaceTemplateSection('vcard_zip_block', $html);

            $html = $page->getTemplateSection('vcard_country_block');
            $html = $page->formOptions($agent_field_name_options, $this->config['vcard_country'], $html);
            $page->replaceTemplateSection('vcard_country_block', $html);

            $html = $page->getTemplateSection('vcard_notes_block');
            $html = $page->formOptions($agent_field_name_options, $this->config['vcard_notes'], $html);
            $page->replaceTemplateSection('vcard_notes_block', $html);

            $html = $page->getTemplateSection('vcard_url_block');
            $html = $page->formOptions($agent_field_name_options, $this->config['vcard_url'], $html);
            $page->replaceTemplateSection('vcard_url_block', $html);

            //Banned Settings
            $page->replaceTag('controlpanel_banned_domains_signup', htmlentities($this->config['banned_domains_signup'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_banned_ips_signup', htmlentities($this->config['banned_ips_signup'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_banned_ips_site', htmlentities($this->config['banned_ips_site'], ENT_COMPAT, $this->config['charset']));

            $page->replacePermissionTags();
            $page->replaceLangTemplateTags();
            $page->autoReplaceTags('', true);
            $display .= $page->returnPage();
        }

        return $display;
    }

    /**
     * Builds the HTML for the Social Section site_config_social.html
     *
     * @return string Return HTML
     */
    public function ajaxConfigureSocial(): string
    {
        global $ORconn, $lang;


        $login = $this->newLogin();
        $misc = $this->newMisc();
        $security = $login->verifyPriv('edit_site_config');
        $display = '';
        if ($security === true) {
            $page = $this->newPageAdmin();
            //Load TEmplate File
            $page->loadPage($this->config['admin_template_path'] . '/site_config_social.html');
            //Load Jscript
            $html = $page->getTemplateSection('email_notification_of_new_users_block');
            $html = $page->formOptions(self::YES_NO, $this->config['email_notification_of_new_users'] ? '1' : '0', $html);
            $page->replaceTemplateSection('email_notification_of_new_users_block', $html);

            $html = $page->getTemplateSection('email_notification_of_new_listings_block');
            $html = $page->formOptions(self::YES_NO, $this->config['email_notification_of_new_listings'] ? '1' : '0', $html);
            $page->replaceTemplateSection('email_notification_of_new_listings_block', $html);

            $html = $page->getTemplateSection('email_users_notification_of_new_listings_block');
            $html = $page->formOptions(self::YES_NO, $this->config['email_users_notification_of_new_listings'] ? '1' : '0', $html);
            $page->replaceTemplateSection('email_users_notification_of_new_listings_block', $html);

            $html = $page->getTemplateSection('email_information_to_new_users_block');
            $html = $page->formOptions(self::YES_NO, $this->config['email_information_to_new_users'] ? '1' : '0', $html);
            $page->replaceTemplateSection('email_information_to_new_users_block', $html);

            $html = $page->getTemplateSection('disable_referrer_check_block');
            $html = $page->formOptions(self::YES_NO, $this->config['disable_referrer_check'] ? '1' : '0', $html);
            $page->replaceTemplateSection('disable_referrer_check_block', $html);

            $html = $page->getTemplateSection('include_senders_ip_block');
            $html = $page->formOptions(self::YES_NO, $this->config['include_senders_ip'] ? '1' : '0', $html);
            $page->replaceTemplateSection('include_senders_ip_block', $html);

            $page->replaceTag('controlpanel_rss_title_featured', htmlentities($this->config['rss_title_featured'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_rss_desc_featured', htmlentities($this->config['rss_desc_featured'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_rss_listingdesc_featured', htmlentities($this->config['rss_listingdesc_featured'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_rss_limit_featured', htmlentities((string)$this->config['rss_limit_featured'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_rss_title_lastmodified', htmlentities($this->config['rss_title_lastmodified'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_rss_desc_lastmodified', htmlentities($this->config['rss_desc_lastmodified'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_rss_listingdesc_lastmodified', htmlentities($this->config['rss_listingdesc_lastmodified'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_rss_limit_lastmodified', htmlentities((string)$this->config['rss_limit_lastmodified'], ENT_COMPAT, $this->config['charset']));

            $page->replaceTag('controlpanel_rss_title_latestlisting', htmlentities($this->config['rss_title_latestlisting'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_rss_desc_latestlisting', htmlentities($this->config['rss_desc_latestlisting'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_rss_listingdesc_latestlisting', htmlentities($this->config['rss_listingdesc_latestlisting'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_rss_limit_latestlisting', htmlentities((string)$this->config['rss_limit_latestlisting'], ENT_COMPAT, $this->config['charset']));

            $page->replaceTag('controlpanel_rss_title_blogposts', htmlentities($this->config['rss_title_blogposts'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_rss_desc_blogposts', htmlentities($this->config['rss_desc_blogposts'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_rss_title_blogcomments', htmlentities($this->config['rss_title_blogcomments'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_rss_desc_blogcomments', htmlentities($this->config['rss_desc_blogcomments'], ENT_COMPAT, $this->config['charset']));

            $page->replaceTag('baseurl', htmlentities($this->config['baseurl'], ENT_COMPAT, $this->config['charset']));

            $page->replaceTag('controlpanel_twitter_consumer_secret', htmlentities($this->config['twitter_consumer_secret'], ENT_COMPAT, $this->config['charset']));
            $page->replaceTag('controlpanel_twitter_consumer_key', htmlentities($this->config['twitter_consumer_key'], ENT_COMPAT, $this->config['charset']));

            if ($this->config['twitter_auth'] == '' && $this->config['twitter_consumer_secret'] != '' && $this->config['twitter_consumer_key'] !== '') {
                $exception_message = '';
                $url = '';
                $ORconnection = new TwitterOAuth($this->config['twitter_consumer_key'], $this->config['twitter_consumer_secret']);
                $token = [];
                try {
                    $request_token = $ORconnection->oauth('oauth/request_token', ['oauth_callback' => $this->config['baseurl'] . '/admin/index.php?action=twitterback']);
                    /* Save temporary credentials to session. */
                    $_SESSION['oauth_token'] = $token = (string)$request_token['oauth_token'];
                    $_SESSION['oauth_token_secret'] = (string)$request_token['oauth_token_secret'];
                } catch (Throwable $t) {
                    $exception_message = $t->getMessage() . "\n";
                    // Executed only in PHP 7, will not match in PHP 5
                }

                /* If last connection failed don't display authorization link. */
                switch ($ORconnection->getLastHttpCode()) {
                    case 200:
                        /* Build authorize URL and redirect user to Twitter. */
                        $url = $ORconnection->url('oauth/authorize', ['oauth_token' => $token]);
                        //$url = $ORconnection->url("oauth/token", array("oauth_token" => $token));
                        //$url = $ORconnection->getAuthorizeURL($token);
                        break;
                    case 415:
                        // Callback not approved.s
                        break;
                }
                if ($url != '') {
                    $page->replaceTag('twitter_auth', '<a href="' . $url . '" class="btn btn-primary">' . $lang['connect_to_twitter'] . '</a>');
                } else {
                    $page->replaceTag('twitter_auth', 'Could not connect to Twitter. Refresh the page or try again later.' . $exception_message);
                }
            } elseif ($this->config['twitter_auth'] != '') {
                $page->replaceTag('twitter_auth', '<a href="#" id="disconnect_twitter" class="btn btn-danger">' . $lang['disconnect_from_twitter'] . '</a>');
            } else {
                $page->replaceTag('twitter_auth', '');
            }
            $html = $page->getTemplateSection('twitter_new_listings_block');
            $html = $page->formOptions(self::YES_NO, $this->config['twitter_new_listings'] ? '1' : '0', $html);
            $page->replaceTemplateSection('twitter_new_listings_block', $html);

            $html = $page->getTemplateSection('twitter_update_listings_block');
            $html = $page->formOptions(self::YES_NO, $this->config['twitter_update_listings'] ? '1' : '0', $html);
            $page->replaceTemplateSection('twitter_update_listings_block', $html);

            $html = $page->getTemplateSection('twitter_new_blog_block');
            $html = $page->formOptions(self::YES_NO, $this->config['twitter_new_blog'] ? '1' : '0', $html);
            $page->replaceTemplateSection('twitter_new_blog_block', $html);

            $html = $page->getTemplateSection('twitter_new_listing_photo_block');
            $html = $page->formOptions(self::YES_NO, $this->config['twitter_listing_photo'] ? '1' : '0', $html);
            $page->replaceTemplateSection('twitter_new_listing_photo_block', $html);

            $sql = 'SELECT userdb_id, userdb_user_first_name, userdb_user_last_name, userdb_is_admin 
					FROM ' . $this->config['table_prefix'] . "userdb 
					WHERE userdb_is_agent = 'yes' 
					OR userdb_is_admin = 'yes' 
					ORDER BY userdb_is_admin 
					DESC,userdb_user_last_name,userdb_user_first_name";
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $all_agents = [];
            while (!$recordSet->EOF) {
                // strip slashes so input appears correctly
                $agent_ID = (int)$recordSet->fields('userdb_id');
                $agent_first_name = (string)$recordSet->fields('userdb_user_first_name');
                $agent_last_name = (string)$recordSet->fields('userdb_user_last_name');
                $userdb_is_admin = (string)$recordSet->fields('userdb_is_admin');
                $all_agents[$agent_ID] = $agent_last_name . ', ' . $agent_first_name;
                if ($userdb_is_admin == 'yes') {
                    $all_agents[$agent_ID] .= ' *';
                }

                $recordSet->MoveNext();
            }
            $current_floor_agent = explode(',', (string)$this->config['floor_agent']);
            $html = $page->getTemplateSection('floor_agent_block');
            $html = $page->formOptions($all_agents, $current_floor_agent, $html);
            $page->replaceTemplateSection('floor_agent_block', $html);

            $page->replacePermissionTags();
            $page->replaceLangTemplateTags();
            $page->autoReplaceTags('', true);
            $display .= $page->returnPage();
        }

        return $display;
    }

    /**
     * Parses the post data and updates site config.
     *
     * @return string Json Response
     */
    public function ajaxUpdateSiteConfig(): string
    {
        global $ORconn, $lang;
        header('Content-type: application/json');

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_site_config');
        if ($security === true) {
            if (!isset($_POST['token']) || is_string($_POST['token']) && !$misc->validateCsrfTokenAjax($_POST['token'])) {
                header('Content-type: application/json');
                return json_encode(['error' => '1', 'error_msg' => $lang['invalid_csrf_token']]) ?: '';
            }
            $sql = 'UPDATE ' . $this->config['table_prefix_no_lang'] . 'controlpanel SET ';
            $sqlRss = 'UPDATE ' . $this->config['table_prefix_no_lang'] . 'controlpanel_rss SET ';
            $sqlTemplate = 'UPDATE ' . $this->config['table_prefix_no_lang'] . 'controlpanel_template SET ';
            $sqlSeo = 'UPDATE ' . $this->config['table_prefix_no_lang'] . 'controlpanel_seo SET ';
            $sql_part_controlpanl = '';
            $sql_part_rss = '';
            $sql_part_seo = '';
            $sql_part_template = '';
            foreach ($_POST as $field => $value) {
                $field = (string)$field;
                if ($field == "token") {
                    continue;
                }
                if (str_starts_with($field, 'controlpanel_rss')) {
                    $sql_part = $sql_part_rss;
                } elseif (str_starts_with($field, 'controlpanel_seo')) {
                    $sql_part = $sql_part_seo;
                } elseif (str_starts_with($field, 'controlpanel_template')) {
                    $sql_part = $sql_part_template;
                } else {
                    $sql_part = $sql_part_controlpanl;
                }
                $sql_field = $ORconn->addQ($field);
                if (is_array($value)) {
                    $value2 = '';
                    foreach ($value as $f) {
                        if (is_scalar($f)) {
                            if ($value2 == '') {
                                $value2 = "$f";
                            } else {
                                $value2 .= ",$f";
                            }
                        }
                    }
                    $value2 = $ORconn->qStr($value2);
                    if (empty($sql_part)) {
                        $sql_part = "`$sql_field` = $value2";
                    } else {
                        $sql_part .= " , `$sql_field` = $value2";
                    }
                } else {
                    $value = $ORconn->qStr($value);
                    if (empty($sql_part)) {
                        $sql_part = "`$sql_field` = $value";
                    } else {
                        $sql_part .= " , `$sql_field` = $value";
                    }
                }
                if (str_starts_with($field, 'controlpanel_rss')) {
                    $sql_part_rss = $sql_part;
                } elseif (str_starts_with($field, 'controlpanel_seo')) {
                    $sql_part_seo = $sql_part;
                } elseif (str_starts_with($field, 'controlpanel_template')) {
                    $sql_part_template = $sql_part;
                } else {
                    $sql_part_controlpanl = $sql_part;
                }
            }
            if ($sql_part_controlpanl != '') {
                $sql .= $sql_part_controlpanl;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logError($sql);
                    return json_encode(['error' => '1', 'error_msg' => $sql]) ?: '';
                }
            }
            if ($sql_part_rss != '') {
                $sqlRss .= $sql_part_rss;
                $recordSet = $ORconn->Execute($sqlRss);
                if (is_bool($recordSet)) {
                    $misc->logError($sqlRss);
                    return json_encode(['error' => '1', 'error_msg' => $sqlRss]) ?: '';
                }
            }
            if ($sql_part_seo != '') {
                $sqlSeo .= $sql_part_seo;
                $recordSet = $ORconn->Execute($sqlSeo);
                if (is_bool($recordSet)) {
                    $misc->logError($sqlSeo);
                    return json_encode(['error' => '1', 'error_msg' => $sqlSeo]) ?: '';
                }
            }
            if ($sql_part_template != '') {
                $sqlTemplate .= $sql_part_template;
                $recordSet = $ORconn->Execute($sqlTemplate);
                if (is_bool($recordSet)) {
                    $misc->logError($sqlTemplate);
                    return json_encode(['error' => '1', 'error_msg' => $sqlTemplate]) ?: '';
                }
            }
            if (isset($_POST['controlpanel_template'])) {
                unset($_SESSION['template']);
            }
            return json_encode(['error' => '0', 'status' => $lang['configuration_saved']]) ?: '';
        }
        return json_encode(['error' => true, 'error_msg' => 'permission_denied']) ?: '';
    }

    /**
     * Parses the post data and updates seo uris.
     *
     * @return string Json Response
     */
    public function ajaxUpdateSeoUris(): string
    {
        global $ORconn, $lang;

        $login = $this->newLogin();
        $misc = $this->newMisc();
        $security = $login->verifyPriv('edit_site_config');
        if ($security === true) {
            foreach ($_POST as $field => $value) {
                $fieldname = strval($field);
                if (is_scalar($value)) {
                    if (str_contains($fieldname, '_slug')) {
                        $slug = $misc->makeDbSafe($value);
                        $action = str_replace('_slug', '', $fieldname);
                        $action = $misc->makeDbSafe($action);
                        $sql = 'UPDATE ' . $this->config['table_prefix_no_lang'] . 'seouri SET slug = ' . $slug . ' WHERE action = ' . $action;
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            $misc->logError($sql);
                            return json_encode(['error' => '1', 'error_msg' => $sql]) ?: '';
                        }
                    } elseif (str_contains($fieldname, '_uri')) {
                        $uri = $misc->makeDbSafe($value);
                        $action = str_replace('_uri', '', $fieldname);
                        $action = $misc->makeDbSafe($action);
                        $sql = 'UPDATE ' . $this->config['table_prefix_no_lang'] . 'seouri SET uri = ' . $uri . ' WHERE action = ' . $action;
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            $misc->logError($sql);
                            return json_encode(['error' => '1', 'error_msg' => $sql]) ?: '';
                        }
                    }
                }
            }
            return json_encode(['error' => '0', 'status' => $lang['configuration_saved']]) ?: '';
        }
        return json_encode(['error' => true, 'error_msg' => 'permission_denied']) ?: '';
    }

    /**
     * Test the SMTP Settings and tries to validate the connection.phpcs:ignore
     * Squiz.Commenting.FunctionCommentThrowTag.Missing.
     *
     * @return string|null
     * @throws \Exception We really do not throw excpetions, there is a bug in validation.
     *                    https://github.com/squizlabs/PHP_CodeSniffer/issues/3685.
     */
    public function ajaxSmtpTest(): ?string
    {
        $login = $this->newLogin();
        $security = $login->verifyPriv('edit_site_config');
        $mailserver = '';
        $mailuser = '';
        $mailpass = '';
        $mailport = 0;
        if ($security === true) {
            if (!empty($_POST['controlpanel_mailserver']) && is_string($_POST['controlpanel_mailserver'])) {
                $mailserver = $_POST['controlpanel_mailserver'];
            }
            if (!empty($_POST['controlpanel_mailport']) && is_string($_POST['controlpanel_mailport'])) {
                $mailport = intval($_POST['controlpanel_mailport']);
            }
            if (!empty($_POST['controlpanel_mailuser']) && is_string($_POST['controlpanel_mailuser'])) {
                $mailuser = $_POST['controlpanel_mailuser'];
            }
            if (!empty($_POST['controlpanel_mailpass']) && is_string($_POST['controlpanel_mailpass'])) {
                $mailpass = $_POST['controlpanel_mailpass'];
            }

            //Create a new SMTP instance
            $smtp = new SMTP();
            //$smtp->IsSMTP(); // telling the class to use SMTP

            //Enable connection-level debug output
            //$smtp->do_debug = SMTP::DEBUG_CONNECTION;
            try {
                //Connect to an SMTP server
                if (!$smtp->connect($mailserver, $mailport)) {
                    throw new Exception('Connect failed');
                }
                //Say hello
                if (!$smtp->hello(gethostname())) {
                    throw new Exception('EHLO failed: ' . $smtp->getError()['error']);
                }
                //Get the list of ESMTP services the server offers
                $e = $smtp->getServerExtList();
                //If server can do TLS encryption, use it
                if (is_array($e) && array_key_exists('STARTTLS', $e)) {
                    $tlsok = $smtp->startTLS();
                    if (!$tlsok) {
                        throw new Exception('Failed to start encryption: ' . $smtp->getError()['error']);
                    }
                    //Repeat EHLO after STARTTLS
                    if (!$smtp->hello(gethostname())) {
                        throw new Exception('EHLO (2) failed: ' . $smtp->getError()['error']);
                    }
                    //Get new capabilities list, which will usually now include AUTH if it didn't before
                    $e = $smtp->getServerExtList();
                }
                //If server supports authentication, do it (even if no encryption)
                if (is_array($e) && array_key_exists('AUTH', $e)) {
                    if ($smtp->authenticate($mailuser, $mailpass)) {
                        return 'Connection successful!';
                    } else {
                        throw new Exception('Authentication failed: ' . $smtp->getError()['error']);
                    }
                }
            } catch (Exception $e) {
                return 'SMTP error: ' . $e->getMessage() . "\n";
            }
            //Whatever happened, close the connection.
            $smtp->quit();

            //return json_encode(array('error' => "0",'status' => $lang['configuration_saved'])) ?: '';
        }
        return null;
    }

    /**
     * Export Active Users From UserDB
     * Builds a CSV of Email, First Name, Last Name
     *
     * @return string Returns the CSV File or a Error String
     */
    public function ajaxExportEmails(): string
    {
        global $ORconn, $lang;

        $login = $this->newLogin();
        $misc = $this->newMisc();
        $security = $login->verifyPriv('edit_site_config');

        if ($security === true) {
            if (!empty($_POST['email_export_type'])) {
                $export_type = $_POST['email_export_type'];

                switch ($export_type) {
                    case 'all':
                        $whereclause = "	WHERE userdb_active ='yes'";
                        $user_type = $lang['all'];
                        break;
                    case 'admins':
                        $whereclause = "	WHERE userdb_is_admin = 'yes'";
                        $user_type = $lang['user_manager_admins'];
                        break;
                    case 'agents':
                        $whereclause = "	WHERE userdb_is_agent = 'yes'";
                        $user_type = $lang['user_manager_agents'];
                        break;
                    case 'members':
                        $whereclause = "	WHERE userdb_is_agent = 'no' AND userdb_id <> 1";
                        $user_type = $lang['user_manager_members'];
                        break;
                    default:
                        return 'Error: Required export type is invalid';
                }

                $sql = 'SELECT userdb_emailaddress, userdb_user_first_name, userdb_user_last_name 
						FROM ' . $this->config['table_prefix'] . 'userdb 
						' . $whereclause . " 
						AND userdb_active = 'yes'";
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }

                $headings = ['userdb_emailaddress', 'userdb_user_first_name', 'userdb_user_last_name'];
                $fh = fopen('php://output', 'w');
                //fputs($fh, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
                fputcsv($fh, $headings);

                while (!$recordSet->EOF) {
                    fputcsv(
                        $fh,
                        [
                            (string)$recordSet->fields('userdb_emailaddress'),
                            (string)$recordSet->fields('userdb_user_first_name'),
                            (string)$recordSet->fields('userdb_user_last_name'),
                        ]
                    );
                    $recordSet->MoveNext();
                }

                fclose($fh);
                $csv = ob_get_clean();

                header('Content-Type: text/csv');
                header("Content-Disposition: attachment; filename=\"$user_type.csv\";");

                return ($csv);
            } else {
                return 'Error: Required export type missing';
            }
        } else {
            return 'Unauthorized access';
        }
    }

    /**
     * Summary of viewWarnings
     *
     * @return string
     */
    public function viewWarnings(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $counter = 0;
        $display = '';
        $warnings = '';
        if (ini_get('safe_mode')) {
            $warnings .= '<div class="warnings_message">' . $lang['warnings_safe_mode'] . '</div>';
            $counter++;
        }
        // CHECK MBString
        if (!in_array('mbstring', get_loaded_extensions()) && $this->config['controlpanel_mbstring_enabled']) {
            $warnings .= '<div class="warnings_message">' . $lang['warnings_mb_convert_encoding'] . '</div>';
            $counter++;
        }
        if (!in_array('curl', get_loaded_extensions())) {
            $warnings .= '<div class="warnings_message">' . $lang['curl_not_enabled'] . '</div>';
            $counter++;
        }
        if (!in_array('zip', get_loaded_extensions())) {
            $warnings .= '<div class="warnings_message">' . $lang['warnings_php_zip'] . '</div>';
            $counter++;
        }
        if (!in_array('gd', get_loaded_extensions())) {
            $warnings .= '<div class="warnings_message">' . $lang['warnings_php_gd'] . '</div>';
        }
        $gdinfo = gd_info();
        if (!isset($gdinfo['FreeType Support']) || !$gdinfo['FreeType Support']) {
            $warnings .= '<div class="warnings_message">' . $lang['warnings_php_freetype'] . '</div>';
        }
        // CHECK OpenSSL
        if (!in_array('openssl', get_loaded_extensions())) {
            $warnings .= '<div class="warnings_message">' . $lang['warnings_openssl'] . '</div>';
            $counter++;
        }

        // CHECK mod_rewrite AND htaccess file
        if (defined('APACHE_GET_MODULES')) {
            if (!in_array('mod_rewrite', apache_get_modules())) {
                $warnings .= '<div class="warnings_message">' . $lang['warnings_mod_rewrite'] . '</div>';
                $counter++;
            }
            if (!file_exists($this->config['basepath'] . '/.htaccess')) {
                $warnings .= '<div class="warnings_message">' . $lang['warnings_htaccess'] . '</div>';
                $counter++;
            }
        }

        // CHECK default password for userdb_user_name = admin
        $sql = 'SELECT userdb_user_password FROM ' . $this->config['table_prefix'] . 'userdb WHERE userdb_user_name = "admin"';
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $default_admin_password = (string)$recordSet->fields('userdb_user_password');
        if (password_verify('password', $default_admin_password)) {
            $warnings .= '<div class="warnings_message">' . $lang['warnings_admin_password'] . '</div>';
            $counter++;
        }
        // END CHECKING
        if ($counter > 0) {
            $display .= '<div id="warnings">' . $warnings . '</div>';
        }
        return $display;
    }
}
