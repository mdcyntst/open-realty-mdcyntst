<?php

declare(strict_types=1);

namespace OpenRealty\Api;

use PDO;
use PDOException;

class Database
{
    private string $db_type;
    private string $db_user;
    private string $db_password;
    private string $db_database;
    private string $db_server;

    private PDO $dbh;

    public function __construct()
    {
        //Load Database Config from common.ini.php
        $settings = parse_ini_file(dirname(__file__) . '/common.ini.php', false, INI_SCANNER_TYPED);
        if (is_bool($settings)) {
            die('Failed to Parse common.ini.php');
        }
        $this->db_type = $settings['db_type'];
        $this->db_server = $settings['db_server'];
        $this->db_database = $settings['db_name'];
        $this->db_user = $settings['db_user'];
        $this->db_password = $settings['db_password'];
        $this->dbh = $this->connectToDatabase();
    }

    private function connectToDatabase(): PDO
    {
        try {
            $dbh = new PDO($this->db_type . ':host=' . $this->db_server . ';dbname=' . $this->db_database, $this->db_user, $this->db_password);
            $dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            return $dbh;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function getDatabaseHandler(): PDO
    {
        return $this->dbh;
    }
}
