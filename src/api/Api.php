<?php

declare(strict_types=1);

/**
 * This File Contains the main API Framework
 *
 * @author    Ryan C. Bonham <ryan@open-realty.org>
 * @copyright 2021 Ryan C. Bonham
 * @link      http://www.open-realty.com Open-Realty
 * @license   https://opensource.org/licenses/MIT MIT
 */

namespace OpenRealty\Api;

use OpenRealty\Api\Commands\SettingsApi;
use PDO;

define('BETA_API', false);

/**
 * This is the base API, all other API functions simply extend this class.
 *
 **/
class Api
{
    protected PDO $dbh;

    protected array $config = [];

    /**
     * Constructor loads settings.
     */
    public function __construct()
    {
        $this->loadSettings();
    }

    /**
     * This Creates the $db connection for the API and also creates the $settings and $lang variables.
     *
     * @access private
     */
    private function loadSettings(): void
    {
        //Load Config Look first for OR's common.php file then fall back to a common.php in this folder for remote usage
        $commonFile = dirname(__FILE__) . '/../include/common.php';
        if (file_exists($commonFile)) {
            include_once $commonFile;
        }

        $apiDB = new Database();
        $this->dbh = $apiDB->getDatabaseHandler();
        $settingApi = new SettingsApi($this->dbh);
        $this->config = $settingApi->read(['is_mobile' => false])['config'];

        /*
        // Load the default API Language File
        if(file_exists(dirname(__FILE__).'/../lang/'.$this->config['lang'].'/api.inc.php')){
            include_once dirname(__FILE__).'/../lang/'.$this->config['lang'].'/api.inc.php';
        }
        */
        //TODO API Language Need to be seperated.
        /**
         * @var string $lang
         */
        $lang_name = $this->config['lang'];
        if (file_exists(dirname(__FILE__) . '/../include/language/' . $lang_name . '/lang.inc.php')) {
            /**
             * @psalm-suppress UnresolvableInclude
             */
            include_once dirname(__FILE__) . '/../include/language/' . $lang_name . '/lang.inc.php';
        }
    }
}
