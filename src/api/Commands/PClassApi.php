<?php

/**
 * This File Contains the Property Class API Commands
 *
 * @package    Open-Realty
 * @subpackage API
 * @author     Ryan C. Bonham
 * @copyright  2021
 * @link       http://www.open-realty.com Open-Realty
 */

declare(strict_types=1);

namespace OpenRealty\Api\Commands;

use Exception;
use OpenRealty\Login;
use PDO;

/**
 * This is the pclass API, it contains all api calls for setting and retrieving property class data.
 *
 **/
class PClassApi extends BaseCommand
{
    /**
     * This function creates a new property class.
     *
     * @param array{
     *          class_name: string,
     *          class_rank: integer,
     *          field_id?: integer[]
     *        } $data Array of innformation used to create a property class.
     * @return array{class_id: integer}
     * @throws Exception Throws Exception which includes an error message when any Fatal error occurs.
     */
    public function create(array $data): array
    {
        $login = new Login($this->dbh, $this->config);
        $login_status = $login->verifyPriv('Agent');
        if ($login_status !== true) {
            throw new Exception('Login Failure');
        }

        //Check that required settings were passed
        if (!isset($data['class_name'])) {
            throw new Exception('class_name: correct_parameter_not_passed');
        }
        $class_name = $data['class_name'];
        if (!isset($data['class_rank'])) {
            throw new Exception('class_rank: correct_parameter_not_passed');
        }
        $class_rank = $data['class_rank'];

        $security = $login->verifyPriv('edit_property_classes');
        if ($security !== true) {
            throw new Exception('permission_denied');
        }
        //Check for duplicate class names
        $sql = "SELECT 1 FROM {$this->config['table_prefix']}class WHERE class_name = :class_name";
        try {
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindValue(':class_name', $class_name);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->pclass->create', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $class_count = (int)$stmt->fetchColumn();
        if ($class_count > 0) {
            throw new Exception('Class Already Exists: ' . $class_name);
        }

        $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'class (class_name,class_rank) VALUES (:class_name,:class_rank)';
        try {
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindValue(':class_name', $class_name);
            $stmt->bindValue(':class_rank', $class_rank, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $new_class_id = $this->dbh->lastInsertId();
        if (is_bool($new_class_id)) {
            throw new Exception('Failed Getting Last Insert ID');
        }
        $new_class_id = (int)$new_class_id;

        if (isset($data['field_id'])) {
            $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . 'classformelements (class_id,listingsformelements_id) VALUES (:class_id,:field_id)';
            try {
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            foreach ($data['field_id'] as $fid) {
                $fid = intval($fid);
                try {
                    $stmt->bindValue(':class_id', $new_class_id, PDO::PARAM_INT);
                    $stmt->bindValue(':field_id', $fid, PDO::PARAM_INT);
                    $stmt->execute();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }
            }
        }
        return ['class_id' => $new_class_id];
    }


    /**
     * This function reads information on single property class
     *
     * @param array{class_id: integer} $data
     * @return array{class_id: integer, class_name: string, class_rank: integer}
     * @throws Exception
     */
    public function read(array $data): array
    {
        //Check that required settings were passed
        if (!isset($data['class_id'])) {
            throw new Exception('class_id: correct_parameter_not_passed');
        }

        $sql = 'SELECT class_name, class_rank
				FROM ' . $this->config['table_prefix'] . 'class
				WHERE class_id = :class_id';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->bindValue(':class_id', $data['class_id'], PDO::PARAM_INT);

            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $recordSet = $stmt->fetchAll();
        $class_count = count($recordSet);
        if ($class_count === 1) {
            $class_name = (string)$recordSet[0]['class_name'];
            $class_rank = (int)$recordSet[0]['class_rank'];
            return ['class_id' => $data['class_id'], 'class_name' => $class_name, 'class_rank' => $class_rank];
        }
        throw new Exception('Class Not Found');
    }

    /**
     * This function reads information on all property class.
     *
     * @return array{metadata: array<int, array{'name': string, 'rank': int}>}
     * @throws Exception Throws Execpetion, with error message on all fatal errors.
     */
    public function metadata(): array
    {
        $sql = 'SELECT class_id, class_name, class_rank
				FROM ' . $this->config['table_prefix'] . 'class ORDER BY class_rank';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $recordSet = $stmt->fetchAll();
        $class_metadata = [];
        foreach ($recordSet as $record) {
            $cid = (int)$record['class_id'];
            $class_metadata[$cid] = [
                'name' => (string)$record['class_name'],
                'rank' => (int)$record['class_rank'],
            ];
        }
        return ['metadata' => $class_metadata];
    }
}
