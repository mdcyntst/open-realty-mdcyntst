<?php

declare(strict_types=1);

namespace OpenRealty\Api\Commands;

use CurlHandle;
use Exception;
use OpenRealty\Hooks;
use OpenRealty\Login;
use PDO;

/**
 * Media API used to manage images, files, and virtual tours.
 */
class MediaApi extends BaseCommand
{
    /**
     * Summary of MEDIA_TYPES
     *
     * @psalm-var string[]
     */
    public const MEDIA_TYPES = ['listingsimages', 'listingsfiles', 'listingsvtours', 'userimages', 'usersfiles'];

    /**
     * @param string $url URL to fetch.
     *
     * @return string|boolean Retruns string contents or False on error.
     * @access private
     */
    private function getUrl(string $url): bool|string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        $data = curl_exec($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);
        if ($header['http_code'] != '200') {
            return false;
        }
        return $data;
    }

    /**
     * @param string       $input_file_name File Name of Photo to create thumbnail of.
     * @param string       $input_file_path Path to photo to create thumbnail of.
     * @param string       $media_type      Media Type.
     * @psalm-param string $media_type      Media Type.
     *
     * @access private
     */
    private function makeThumbnailGD(string $input_file_name, string $input_file_path, string $media_type): string
    {
        // makes a thumbnail using the GD library
        /**
         * @var integer $quality
         */
        $quality = $this->config['jpeg_quality']; // jpeg quality -- set in common.php)
        $output_path = $input_file_path;

        if ($media_type == 'listingsimages' || $media_type == 'listingsvtours') {
            /**
             * @var integer $max_width
             */
            $max_width = $this->config['thumbnail_width'];
            /**
             * @var integer $max_height
             */
            $max_height = $this->config['thumbnail_height'];
            $resize_by = $this->config['resize_thumb_by'];
        } else {
            /**
             * @var integer $max_width
             */
            $max_width = $this->config['user_thumbnail_width'];
            /**
             * @var integer $max_height
             */
            $max_height = $this->config['user_thumbnail_height'];
            $resize_by = $this->config['user_resize_thumb_by'];
        }
        // Specify your file details
        $current_file = $input_file_path . '/' . $input_file_name;

        // Get the current info on the file
        $imagedata = getimagesize($current_file);
        if (is_bool($imagedata)) {
            return $input_file_name;
        }
        /**
         * @var integer $imagewidth
         */
        $imagewidth = $imagedata[0];
        /**
         * @var integer $imageheight
         */
        $imageheight = $imagedata[1];
        /**
         * @var integer $imagetype
         */
        $imagetype = $imagedata[2];
        $new_img_width = 0;
        $new_img_height = 0;
        if ($resize_by == 'width') {
            $shrinkage = $imagewidth / $max_width;
            $new_img_width = $max_width;
            $new_img_height = intval(round($imageheight / $shrinkage));
        } elseif ($resize_by == 'height') {
            $shrinkage = $imageheight / $max_height;
            $new_img_height = $max_height;
            $new_img_width = intval(round($imagewidth / $shrinkage));
        } elseif ($resize_by == 'both') {
            $new_img_width = $max_width;
            $new_img_height = $max_height;
        } elseif ($resize_by == 'bestfit') {
            $shrinkage_width = $imagewidth / $max_width;
            $shrinkage_height = $imageheight / $max_height;
            $shrinkage = max($shrinkage_width, $shrinkage_height);
            $new_img_height = intval(round($imageheight / $shrinkage));
            $new_img_width = intval(round($imagewidth / $shrinkage));
        }
        // type definitions
        // 1 = GIF, 2 = JPG, 3 = PNG, 4 = SWF, 5 = PSD, 6 = BMP
        // 7 = TIFF(intel byte order), 8 = TIFF(motorola byte order)
        // 9 = JPC, 10 = JP2, 11 = JPX
        $thumb_name = $input_file_name; //by default
        // the GD library, which this uses, can only resize GIF, JPG and PNG
        if ($imagetype == 1) {
            // it's a GIF
            // see if GIF support is enabled
            if (imagetypes() & IMG_GIF) {
                $src_img = imagecreatefromgif($current_file);
                $dst_img = imageCreateTrueColor($new_img_width, $new_img_height);
                // copy the original image info into the new image with new dimensions
                // checking to see which function is available
                ImageCopyResampled($dst_img, $src_img, 0, 0, 0, 0, $new_img_width, $new_img_height, $imagewidth, $imageheight);
                $thumb_name = 'thumb_' . "$input_file_name";
                imagegif($dst_img, "$output_path/$thumb_name");
                @chmod("$output_path/$thumb_name", 0777);
                imagedestroy($src_img);
                imagedestroy($dst_img);
            } // end if GIF support is enabled
        } elseif ($imagetype == 2) {
            // it's a JPG
            $src_img = imagecreatefromjpeg($current_file);
            $dst_img = imageCreateTrueColor($new_img_width, $new_img_height);
            // copy the original image info into the new image with new dimensions
            ImageCopyResampled($dst_img, $src_img, 0, 0, 0, 0, $new_img_width, $new_img_height, $imagewidth, $imageheight);
            $thumb_name = 'thumb_' . "$input_file_name";
            imagejpeg($dst_img, "$output_path/$thumb_name", $quality);
            @chmod("$output_path/$thumb_name", 0777);
            imagedestroy($src_img);
            imagedestroy($dst_img);
        } elseif ($imagetype == 3) {
            // it's a PNG
            $src_img = imagecreatefrompng($current_file);
            $dst_img = imageCreateTrueColor($new_img_width, $new_img_height);
            ImageCopyResampled($dst_img, $src_img, 0, 0, 0, 0, $new_img_width, $new_img_height, $imagewidth, $imageheight);
            $thumb_name = 'thumb_' . "$input_file_name";
            imagepng($dst_img, "$output_path/$thumb_name");
            @chmod("$output_path/$thumb_name", 0777);
            imagedestroy($src_img);
            imagedestroy($dst_img);
        } // end if $imagetype == 3
        return $thumb_name;
    }

    /**
     * @param string $input_file_name
     * @param string $input_file_path
     * @param string $media_type
     *
     * @return string Returns Image File Name
     * @access private
     */
    private function resizeImageGD(string $input_file_name, string $input_file_path, string $media_type): string
    {
        // resizes image using the GD library
        /**
         * @var integer $quality
         */
        $quality = $this->config['jpeg_quality']; // jpeg quality -- set in common.php
        // Specify your file details
        $current_file = $input_file_path . '/' . $input_file_name;

        if ($media_type == 'listingsimages') {
            /**
             * @var integer $max_width
             */
            $max_width = $this->config['max_listings_upload_width'];
            /**
             * @var integer $max_height
             */
            $max_height = $this->config['max_listings_upload_height'];
            $resize_by = $this->config['resize_by'];
        } else {
            /**
             * @var integer $max_width
             */
            $max_width = $this->config['max_user_upload_width'];
            /**
             * @var integer $max_height
             */
            $max_height = $this->config['max_user_upload_height'];
            $resize_by = $this->config['user_resize_by'];
        }

        // Get the current info on the file
        $imagedata = getimagesize($current_file);

        if (is_bool($imagedata)) {
            return $input_file_name;
        }
        /**
         * @var integer $imagewidth
         */
        $imagewidth = $imagedata[0];
        /**
         * @var integer $imageheight
         */
        $imageheight = $imagedata[1];
        /**
         * @var integer $imagetype
         */
        $imagetype = $imagedata[2];
        $new_img_width = 0;
        $new_img_height = 0;

        // if this is a .jpg see if it has an orientation if so, and it's not normal,
        // swap the width/height values
        if ($imagetype == 2) {
            $exif = exif_read_data($current_file);

            if (!empty($exif['Orientation']) && $exif['Orientation'] != 1) {
                /**
                 * @var integer $imagewidth
                 */
                $imagewidth = $imagedata[1];
                /**
                 * @var integer $imageheight
                 */
                $imageheight = $imagedata[0];
                /**
                 * @var integer $imagetype
                 */
            }
        }

        if ($resize_by == 'width') {
            $shrinkage = $imagewidth / $max_width;
            $new_img_width = $max_width;
            $new_img_height = intval(round($imageheight / $shrinkage));
        } elseif ($resize_by == 'height') {
            $shrinkage = $imageheight / $max_height;
            $new_img_height = $max_height;
            $new_img_width = intval(round($imagewidth / $shrinkage));
        } elseif ($resize_by == 'both') {
            $new_img_width = $max_width;
            $new_img_height = $max_height;
        } elseif ($resize_by == 'bestfit') {
            $shrinkage_width = $imagewidth / $max_width;
            $shrinkage_height = $imageheight / $max_height;
            $shrinkage = max($shrinkage_width, $shrinkage_height);
            $new_img_height = intval(round($imageheight / $shrinkage));
            $new_img_width = intval(round($imagewidth / $shrinkage));
        }
        // type definitions
        // 1 = GIF, 2 = JPG, 3 = PNG, 4 = SWF, 5 = PSD, 6 = BMP
        // 7 = TIFF(intel byte order), 8 = TIFF(motorola byte order)
        // 9 = JPC, 10 = JP2, 11 = JPX
        $img_name = $input_file_name; //by default
        // the GD library, which this uses, can only resize GIF, JPG and PNG
        if ($imagetype == 1) {
            // it's a GIF
            // see if GIF support is enabled
            if (imagetypes() & IMG_GIF) {
                $src_img = imagecreatefromgif($current_file);
                $dst_img = imageCreateTrueColor($new_img_width, $new_img_height);
                // copy the original image info into the new image with new dimensions
                ImageCopyResampled($dst_img, $src_img, 0, 0, 0, 0, $new_img_width, $new_img_height, $imagewidth, $imageheight);
                imagegif($dst_img, "$input_file_path/$img_name");
                imagedestroy($src_img);
                imagedestroy($dst_img);
            } //end if GIF support is enabled
        } elseif ($imagetype == 2) {
            // it's a JPG
            $src_img = imagecreatefromjpeg($current_file);

            //if the .jpg has a EXIF orientation set, rotate it back to normal
            if (!empty($exif['Orientation'])) {
                switch ($exif['Orientation']) {
                    case 3:
                        $src_img = imagerotate($src_img, 180, 0);
                        break;
                    case 6:
                        $src_img = imagerotate($src_img, -90, 0);
                        break;
                    case 8:
                        $src_img = imagerotate($src_img, 90, 0);
                        break;
                }
            }

            $dst_img = imageCreateTrueColor($new_img_width, $new_img_height);
            // copy the original image info into the new image with new dimensions
            // checking to see which function is available
            ImageCopyResampled($dst_img, $src_img, 0, 0, 0, 0, $new_img_width, $new_img_height, $imagewidth, $imageheight);
            $img_name = "$input_file_name";
            imagejpeg($dst_img, "$input_file_path/$img_name", $quality);
            imagedestroy($src_img);
            imagedestroy($dst_img);
        } elseif ($imagetype == 3) {
            // it's a PNG
            $src_img = imagecreatefrompng($current_file);
            $dst_img = imageCreateTrueColor($new_img_width, $new_img_height);
            ImageCopyResampled($dst_img, $src_img, 0, 0, 0, 0, $new_img_width, $new_img_height, $imagewidth, $imageheight);
            $img_name = "$input_file_name";
            imagepng($dst_img, "$input_file_path/$img_name");
            imagedestroy($src_img);
            imagedestroy($dst_img);
        } // end if $imagetype == 3
        return $img_name;
    }

    /**
     * @param string $input_file_name
     * @param string $input_file_path
     * @param string $media_type
     *
     * @return string
     * @access private
     */
    private function resizeImageImageMagick(string $input_file_name, string $input_file_path, string $media_type): string
    {
        // resizes image using ImageMagick
        // Specify your file details
        $current_file = $input_file_path . '/' . $input_file_name;
        if ($media_type == 'listingsimages') {
            /**
             * @var integer $max_width
             */
            $max_width = $this->config['max_listings_upload_width'];
            /**
             * @var integer $max_height
             */
            $max_height = $this->config['max_listings_upload_height'];
            $resize_by = $this->config['resize_by'];
        } else {
            /**
             * @var integer $max_width
             */
            $max_width = $this->config['max_user_upload_width'];
            /**
             * @var integer $max_height
             */
            $max_height = $this->config['max_user_upload_height'];
            $resize_by = $this->config['user_resize_by'];
        }
        // Get the current info on the file
        $imagedata = getimagesize($current_file);
        if (is_bool($imagedata)) {
            return $input_file_name;
        }
        /**
         * @var integer $imagewidth
         */
        $imagewidth = $imagedata[0];
        /**
         * @var integer $imageheight
         */
        $imageheight = $imagedata[1];

        $new_img_width = 0;
        $new_img_height = 0;
        if ($resize_by == 'width') {
            $shrinkage = $imagewidth / $max_width;
            $new_img_width = $max_width;
            $new_img_height = intval(round($imageheight / $shrinkage));
        } elseif ($resize_by == 'height') {
            $shrinkage = $imageheight / $max_height;
            $new_img_height = $max_height;
            $new_img_width = intval(round($imagewidth / $shrinkage));
        } elseif ($resize_by == 'both') {
            $new_img_width = $max_width;
            $new_img_height = $max_height;
        } elseif ($resize_by == 'bestfit') {
            $shrinkage_width = $imagewidth / $max_width;
            $shrinkage_height = $imageheight / $max_height;
            $shrinkage = max($shrinkage_width, $shrinkage_height);
            $new_img_height = intval(round($imageheight / $shrinkage));
            $new_img_width = intval(round($imagewidth / $shrinkage));
        }
        // $image_base = explode('.', $current_file);
        // This part gets the new thumbnail name
        // $image_basename = $image_base[0];
        // $image_ext = $image_base[1];
        $path = $this->config['path_to_imagemagick'];
        $debug_path = '"' . $path . '" -geometry ' . $new_img_width . 'x' . $new_img_height . ' "' . $current_file . '" "' . $current_file . '"';
        // Convert the file
        exec($debug_path);
        return $input_file_name;
    }

    /**
     * @param string $input_file_name
     * @param string $input_file_path
     * @param string $media_type
     *
     * @return string
     * @access private
     */
    private function makeThumbImageMagick(string $input_file_name, string $input_file_path, string $media_type): string
    {
        // makes a thumbnail using ImageMagick
        // Specify your file details
        $current_file = $input_file_path . '/' . $input_file_name;
        if ($media_type == 'listingsimages' || $media_type == 'listingsvtours') {
            /**
             * @var integer $max_width
             */
            $max_width = $this->config['thumbnail_width'];
            /**
             * @var integer $max_height
             */
            $max_height = $this->config['thumbnail_height'];
            $resize_by = $this->config['resize_thumb_by'];
        } else {
            /**
             * @var integer $max_width
             */
            $max_width = $this->config['user_thumbnail_width'];
            /**
             * @var integer $max_height
             */
            $max_height = $this->config['user_thumbnail_height'];
            $resize_by = $this->config['user_resize_thumb_by'];
        }

        // Get the current info on the file
        $imagedata = getimagesize($current_file);
        if (is_bool($imagedata)) {
            return $input_file_name;
        }
        /**
         * @var integer $imagewidth
         */
        $imagewidth = $imagedata[0];
        /**
         * @var integer $imageheight
         */
        $imageheight = $imagedata[1];

        $new_img_width = 0;
        $new_img_height = 0;

        if ($resize_by == 'width') {
            $shrinkage = $imagewidth / $max_width;
            $new_img_width = $max_width;
            $new_img_height = round($imageheight / $shrinkage);
        } elseif ($resize_by == 'height') {
            $shrinkage = $imageheight / $max_height;
            $new_img_height = $max_height;
            $new_img_width = round($imagewidth / $shrinkage);
        } elseif ($resize_by == 'both') {
            $new_img_width = $max_width;
            $new_img_height = $max_height;
        } elseif ($resize_by == 'bestfit') {
            $shrinkage_width = $imagewidth / $max_width;
            $shrinkage_height = $imageheight / $max_height;
            $shrinkage = max($shrinkage_width, $shrinkage_height);
            $new_img_height = round($imageheight / $shrinkage);
            $new_img_width = round($imagewidth / $shrinkage);
        }

        // $image_base = explode('.', $current_file);
        // This part gets the new thumbnail name
        // $image_basename = $image_base[0];
        // $image_ext = $image_base[1];
        $thumb_name = $input_file_path . '/thumb_' . $input_file_name;
        $thumb_name2 = 'thumb_' . $input_file_name;
        $path = $this->config['path_to_imagemagick'];
        // Convert the file
        $debug_path = '"' . $path . '" -geometry ' . $new_img_width . 'x' . $new_img_height . ' "' . $current_file . '" "' . $thumb_name . '"';
        $debug = exec($debug_path);
        if ($debug === false) {
            return $input_file_name;
        }
        @chmod("$input_file_path/$thumb_name", 0777);
        return $thumb_name2;
    }

    /**
     * Summary of mediaPermissionCheck
     *
     * Returns the owner ID if the user has permission to modify the media. Returns false if the user doesn't have
     * permisison.
     *
     * @param string  $media_type
     * @param integer $media_id
     * @param integer $parent_id
     *
     * @return false|integer
     * @throws \Exception
     */
    private function mediaPermissionCheck(string $media_type, int $media_id = 0, int $parent_id = 0): int|false
    {
        $login = new Login($this->dbh, $this->config);
        //Make sure Media Type is valid
        if (!in_array($media_type, self::MEDIA_TYPES)) {
            return false;
        }
        $parent_field = str_starts_with($media_type, 'listing') ? 'listingsdb_id' : 'userdb_id';
        if ($media_id != 0 && $parent_id == 0) {
            $sql = 'SELECT ' . $parent_field . ' 
							FROM ' . $this->config['table_prefix'] . "$media_type 
							WHERE " . $media_type . "_id = :media_id";
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->bindValue(':media_id', $media_id, PDO::PARAM_INT);
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->media->mediaPermissionCheck', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $parent_id = (int)$stmt->fetchColumn();
        }
        switch ($media_type) {
            case 'listingsimages':
                $listing_api = new ListingApi($this->dbh, $this->config);
                $data = ['listing_id' => $parent_id, 'fields' => ['userdb_id']];
                $result = $listing_api->read($data);
                $listing_agent_id = 0;
                if (is_integer($result['listing']['userdb_id'])) {
                    $listing_agent_id = intval($result['listing']['userdb_id']);
                }
                //Make sure we can Edit this lisitng
                if (isset($_SESSION['userID']) && $_SESSION['userID'] !== $listing_agent_id) {
                    $security = $login->verifyPriv('edit_all_listings');
                    if ($security === true) {
                        return $listing_agent_id;
                    }
                } elseif (isset($_SESSION['userID']) && $_SESSION['userID'] === $listing_agent_id) {
                    return $listing_agent_id;
                }
                return false;
            case 'userimages':
                if (isset($_SESSION['userID']) && $_SESSION['userID'] !== $parent_id) {
                    $security = $login->verifyPriv('edit_all_users');
                    if ($security === true) {
                        return $parent_id;
                    }
                } elseif (isset($_SESSION['userID']) && $_SESSION['userID'] === $parent_id) {
                    return $parent_id;
                }
                return false;
            case 'listingsvtours':
            case 'listingsfiles':
                $listing_api = new ListingApi($this->dbh, $this->config);
                $data = ['listing_id' => $parent_id, 'fields' => ['userdb_id']];
                $result = $listing_api->read($data);
                $listing_agent_id = 0;
                if (is_integer($result['listing']['userdb_id'])) {
                    $listing_agent_id = $result['listing']['userdb_id'];
                }
                //Make sure we can Edit this lisitng
                if (isset($_SESSION['userID']) && $_SESSION['userID'] !== $listing_agent_id) {
                    if ($_SESSION['edit_all_listings'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                        return $listing_agent_id;
                    }
                } elseif (isset($_SESSION['userID']) && $_SESSION['userID'] === $listing_agent_id) {
                    return $listing_agent_id;
                }
                return false;
            case 'usersfiles':
                //Make sure we can Edit this lisitng
                if (isset($_SESSION['userID']) && $_SESSION['userID'] !== $parent_id) {
                    if ($_SESSION['edit_all_users'] == 'yes' || $_SESSION['admin_privs'] == 'yes') {
                        return $parent_id;
                    }
                } elseif (isset($_SESSION['userID']) && $_SESSION['userID'] === $parent_id) {
                    return $parent_id;
                }
                return false;
        }
        return false;
    }

    /**
     * @param string $media_type
     * @param int    $media_parent_id
     * @return int
     * @throws \Exception
     */
    private function getMediaCount(string $media_type, int $media_parent_id): int
    {
        if (!in_array($media_type, self::MEDIA_TYPES)) {
            throw new Exception('correct_parameter_not_passed');
        }
        $parent_field = str_starts_with($media_type, 'listing') ? 'listingsdb_id' : 'userdb_id';
        $sql = 'SELECT count(' . $parent_field . ') as num_images 
							FROM ' . $this->config['table_prefix'] . $media_type . " 
							WHERE ' . $parent_field . ' = :parent_id";
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->bindValue(':parent_id', $media_parent_id, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->media->getMediaCount', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        return (int)$stmt->fetchColumn();
    }

    /**
     * @param string $media_type
     * @param int    $media_parent_id
     * @return int
     * @throws \Exception
     */
    private function getMediaMaxRank(string $media_type, int $media_parent_id): int
    {
        if (!in_array($media_type, self::MEDIA_TYPES)) {
            throw new Exception('correct_parameter_not_passed');
        }
        $parent_field = str_starts_with($media_type, 'listing') ? 'listingsdb_id' : 'userdb_id';
        $sql = 'SELECT MAX(' . $media_type . '_rank) AS max_rank 
										FROM ' . $this->config['table_prefix'] . "$media_type
										WHERE '.$parent_field.' = :parent_id";
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->bindValue(':parent_id', $media_parent_id, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->media->getMediaCount', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        return (int)$stmt->fetchColumn();
    }

    /**
     * @param string $media_type
     * @param int    $media_parent_id
     * @param int    $owner
     * @param string $file_name
     * @param string $thumb_name
     * @param int    $rank
     * @param string $caption
     * @param string $description
     * @param string $active
     * @return void
     * @throws \Exception
     */
    private function writeMediaToDatabase(
        string $media_type,
        int    $media_parent_id,
        int    $owner,
        string $file_name,
        string $thumb_name,
        int    $rank,
        string $caption = '',
        string $description = '',
        string $active = 'yes'
    ) {
        if (!in_array($media_type, self::MEDIA_TYPES)) {
            throw new Exception('correct_parameter_not_passed');
        }
        $parent_field = str_starts_with($media_type, 'listing') ? 'listingsdb_id' : 'userdb_id';
        $owner_param = str_starts_with($media_type, 'listing') ? ':owner,' : '';
        $owner_sql = str_starts_with($media_type, 'listing') ? 'userdb_id,' : '';
        $thumb_param = !str_ends_with($media_type, 'files') ? ':thumb_name,' : '';
        $thumb_sql = !str_ends_with($media_type, 'files') ? "{$media_type}_thumb_file_name," : '';
        $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "$media_type (
                                        $parent_field,
                                        $owner_sql 
										{$media_type}_file_name, 
										$thumb_sql
										{$media_type}_rank,
										{$media_type}_caption,
										{$media_type}_description,
										{$media_type}_active
									) 
									VALUES (:parent_id, $owner_param :file_name, $thumb_param :rank, :caption,:description, :active)";
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
        } catch (Exception $e) {
            throw new Exception('Prepairing Statement Failed: ' . $e->getMessage());
        }

        try {
            if (!empty($owner_sql)) {
                $stmt->bindValue(':owner', $owner, PDO::PARAM_INT);
            }
            $stmt->bindValue(':parent_id', $media_parent_id, PDO::PARAM_INT);
            $stmt->bindValue(':rank', $rank, PDO::PARAM_INT);
            $stmt->bindValue(':file_name', $file_name);
            if (!empty($thumb_sql)) {
                $stmt->bindValue(':thumb_name', $thumb_name);
            }
            $stmt->bindValue(':caption', $caption);
            $stmt->bindValue(':description', $description);
            $stmt->bindValue(':active', $active);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->media->getMediaCount', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
    }
    /**
     * This function create a new property class.
     *
     * @param array $data Data array should contain the following elements.
     *                    <ul>
     *                    <li>$data['media_parent_id'] - This should be either the User ID or Listing ID that the media
     *                    is associated with.</li>
     *                    <li>$data['media_type'] - Type of Media
     *                    ('listingsimages','listingsfiles','listingsvtours','userimages','usersfiles')</li>
     *
     *      <li>$data['media_data'] - This should be array like follows, with the key being the FILENAME for the media
     *      file, example  "greenhouse.jpg".</li>
     *      <li>$data['media_data']['FILENAME']['caption'] = 'You Caption goes here"  - OPTIONAL used to set the media
     *      caption.</li>
     *      <li>$data['media_data']['FILENAME']['description'] = 'You description goes here"  - OPTIONAL used to set
     *      the media description.</li>
     *      <li>$data['media_data']['FILENAME']['data'] = BINARY / ULR  - This should be either the BINARY data for
     *      this media or a URL to the media.</li>
     *      <li>$data['media_data']['FILENAME']['remote'] = TRUE /FALSE  - OPTIONAL If set to true the media will only
     *      be linked to and not downloaded. Only applies with the data command above contains a URL</li>
     *      <li>$data['media_data']['FILENAME']['rank'] - OPTIONAL INT- Ordering rank of the media. If left empty the
     *      media will be the next avaliable rank (last). </li>
     *  </ul>
     *
     * @return array
     */
    /**
     * Summary of create
     *
     * @param array{
     *            media_parent_id: integer,
     *            media_type: string,
     *            media_data: array<
     *                           string, array{
     *                                      caption?: string,
     *                                      description?: string,
     *                                      data: string,
     *                                      remote?: boolean,
     *                                      rank?: integer
     *                                   }
     *                        >
     *        } $data
     * @return array
     * @throws Exception
     */
    public function create(array $data): array
    {
        global $lang;

        $login = new Login($this->dbh, $this->config);
        $login_status = $login->verifyPriv('Agent');
        if ($login_status !== true) {
            throw new Exception('Login Failure');
        }
        //Check that required settings were passed
        if (!isset($data['media_type']) || !in_array($data['media_type'], self::MEDIA_TYPES)) {
            throw new Exception('correct_parameter_not_passed');
        }
        $media_type = $data['media_type'];

        if (!isset($data['media_data'])) {
            throw new Exception('correct_parameter_not_passed');
        }

        $media_data = $data['media_data'];

        if (!isset($data['media_parent_id'])) {
            throw new Exception('media_parent_id: correct_parameter_not_passed');
        }
        $media_parent_id = intval($data['media_parent_id']);
        $owner = $this->mediaPermissionCheck($media_type, 0, $media_parent_id);
        $resize_by = '';
        if (is_integer($owner)) {
            $allow_resize = false;
            switch ($media_type) {
                case 'listingsimages':
                    $num_files = $this->getMediaCount($media_type, $media_parent_id);
                    $max_listings_uploads = $this->config['max_listings_uploads'];
                    $avaliable_files = $max_listings_uploads - $num_files;
                    $max_file_size = $this->config['max_listings_upload_size'];
                    $max_width = $this->config['max_listings_upload_width'];
                    $max_height = $this->config['max_listings_upload_height'];
                    $resize_by = $this->config['resize_by'];
                    if ($this->config['resize_img']) {
                        $allow_resize = true;
                    }
                    $allowed_extensions = $this->config['allowed_upload_extensions'];
                    break;
                case 'userimages':
                    $num_files = $this->getMediaCount($media_type, $media_parent_id);
                    $max_user_uploads = $this->config['max_user_uploads'];
                    $avaliable_files = $max_user_uploads - $num_files;
                    $max_file_size = $this->config['max_user_upload_size'];
                    $max_width = $this->config['max_user_upload_width'];
                    $max_height = $this->config['max_user_upload_height'];
                    $allowed_extensions = $this->config['allowed_upload_extensions'];
                    $resize_by = $this->config['user_resize_by'];
                    if ($this->config['user_resize_img']) {
                        $allow_resize = true;
                    }
                    break;
                case 'listingsvtours':
                    $num_files = $this->getMediaCount($media_type, $media_parent_id);
                    $vmax_vtour_uploads = $this->config['max_vtour_uploads'];
                    $avaliable_files = $vmax_vtour_uploads - $num_files;
                    $max_file_size = $this->config['max_vtour_upload_size'];
                    $max_width = $this->config['max_vtour_upload_width'];
                    $max_height = 0;
                    $allowed_extensions = $this->config['allowed_upload_extensions'];
                    break;
                case 'listingsfiles':
                    $num_files = $this->getMediaCount($media_type, $media_parent_id);
                    $max_listings_file_uploads = $this->config['max_listings_file_uploads'];
                    $avaliable_files = $max_listings_file_uploads - $num_files;
                    $max_file_size = $this->config['max_listings_file_upload_size'];
                    $max_width = 0;
                    $max_height = 0;
                    $allowed_extensions = $this->config['allowed_file_upload_extensions'];
                    break;
                case 'usersfiles':
                    $num_files = $this->getMediaCount($media_type, $media_parent_id);
                    $max_users_file_uploads = $this->config['max_users_file_uploads'];
                    $avaliable_files = $max_users_file_uploads - $num_files;
                    $max_file_size = $this->config['max_users_file_upload_size'];
                    $max_width = 0;
                    $max_height = 0;
                    $allowed_extensions = $this->config['allowed_file_upload_extensions'];
                    break;
                default:
                    throw new Exception('correct_parameter_not_passed');
            }
            //Get Multiple URLS at the same time to speed up curl requests..
            //print_r($media_data);
            $image_import_count = 0;
            $master = curl_multi_init();
            $curl_arr = [];
            foreach ($media_data as $media_name => $media_info) {
                if ($image_import_count == $avaliable_files) {
                    continue;
                }
                if ((str_starts_with($media_info['data'], 'http://') || str_starts_with($media_info['data'], 'https://') || str_starts_with($media_info['data'], '//')) && (!isset($media_info['remote']) || !$media_info['remote'])) {
                    $curl_arr[$media_name] = curl_init();
                    curl_setopt($curl_arr[$media_name], CURLOPT_URL, $media_info['data']);
                    curl_setopt($curl_arr[$media_name], CURLOPT_ENCODING, 'gzip');
                    curl_setopt($curl_arr[$media_name], CURLOPT_TIMEOUT, 20);
                    curl_setopt($curl_arr[$media_name], CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl_arr[$media_name], CURLOPT_SSL_VERIFYPEER, 1);
                    curl_setopt($curl_arr[$media_name], CURLOPT_SSL_VERIFYHOST, 2);
                    curl_multi_add_handle($master, $curl_arr[$media_name]);
                }
                $image_import_count++;
            }
            do {
                curl_multi_exec($master, $running);
            } while ($running > 0);
            foreach ($curl_arr as $media_name => $curlobj) {
                if ($curlobj instanceof CurlHandle) {
                    $media_data[$media_name]['data'] = curl_multi_getcontent($curlobj);
                    curl_multi_remove_handle($master, $curlobj);
                }
            }
            curl_multi_close($master);
            //Keep Track of number of files added.
            $image_import_count = 0;
            $media_response = [];
            $media_error = [];
            //print_r($media_data);
            $imagewidth = 0;
            $imageheight = 0;
            foreach ($media_data as $media_name => $media_info) {
                //Set Error to True by default
                $media_error[$media_name] = true;
                //Make sure we are not out of allowed media
                if ($image_import_count == $avaliable_files) {
                    $media_response[$media_name] = $lang['media_limit_exceeded'];
                    continue;
                }
                $save_file = false;

                if (!isset($media_info['data'])) {
                    //Skip Bad Media Object
                    $media_response[$media_name] = $lang['media_bad_object'];
                } else {
                    //Figure out if this is an URL or Binary Data
                    if (!str_starts_with($media_info['data'], 'http://') && !str_starts_with($media_info['data'], 'https://') && !str_starts_with($media_info['data'], '//')) {
                        //We have Binary Data
                        $filename = $media_name;
                        $save_file = true;
                    } else {
                        //We have a URL check to see if we are importing or linking to this URL.
                        if (isset($media_info['remote']) && $media_info['remote']) {
                            $filename = $media_info['data'];
                        } else {
                            $save_file = true;
                            //Determine filename from the URL
                            $filename = $media_name;
                        }
                    }
                    //Ok we have the file name, check to see if it is already uploaded.
                    $save_name = $filename;
                    if ($media_type == 'listingsimages') {
                        $sql = 'SELECT 1 FROM ' . $this->config['table_prefix'] . "listingsimages WHERE listingsimages_file_name = :filename";
                    } elseif ($media_type == 'listingsvtours') {
                        $sql = 'SELECT 1 FROM ' . $this->config['table_prefix'] . "listingsvtours WHERE listingsvtours_file_name = :filename";
                    } elseif ($media_type == 'userimages') {
                        $sql = 'SELECT 1 FROM ' . $this->config['table_prefix'] . "userimages WHERE userimages_file_name = :filename";
                    } elseif ($media_type == 'listingsfiles') {
                        $sql = 'SELECT 1 FROM ' . $this->config['table_prefix'] . "listingsfiles WHERE listingsfiles_file_name = :filename";
                    } else {
                        $sql = 'SELECT 1 FROM ' . $this->config['table_prefix'] . "usersfiles WHERE usersfiles_file_name = :filename";
                    }
                    $stmt = $this->dbh->prepare($sql);
                    if (is_bool($stmt)) {
                        throw new Exception('Prepairing Statement Failed');
                    }
                    try {
                        $stmt->bindValue(':filename', $filename);
                        $stmt->execute();
                    } catch (Exception $e) {
                        $error = $e->getMessage();
                        $log_api = new LogApi($this->dbh, $this->config);
                        $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                        throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                    }
                    $num = (int)$stmt->fetchColumn();
                    if ($num > 0) {
                        $media_response[$media_name] = $lang['media_object_exists'];
                        continue;
                    }
                    $temp_file = false;
                    if ($save_file) {
                        //This is a saftey in case the multi curl get failed..
                        if (str_starts_with($media_info['data'], 'http://') || str_starts_with($media_info['data'], 'https://') || str_starts_with($media_info['data'], '//')) {
                            $file_data = $this->getUrl($media_info['data']);
                            if (is_bool($file_data)) {
                                $media_response[$media_name] = $lang['media_object_invalid_url'];
                                continue;
                            }
                        } else {
                            $file_data = $media_info['data'];
                        }

                        $temp_file = tempnam(sys_get_temp_dir(), $filename);
                        if (is_bool($temp_file)) {
                            unset($temp_file);
                            $media_response[$media_name] = $lang['media_object_unable_to_create_temp_file'];
                            continue;
                        }
                        $fp = fopen($temp_file, 'wb');
                        fwrite($fp, $file_data);
                        fclose($fp);


                        $extension = strtolower(substr(strrchr($filename, '.'), 1));
                        $filesize = filesize($temp_file);
                        // check file extensions
                        if (!in_array($extension, explode(',', $allowed_extensions))) {
                            $media_response[$media_name] = $lang['upload_invalid_extension'];
                            unlink($temp_file);
                            continue;
                        }
                        // check size

                        if ($max_file_size != 0 && $filesize > $max_file_size) {
                            $media_response[$media_name] = $lang['upload_too_large'];
                            unlink($temp_file);
                            continue;
                        }
                        //Test Image Height & Width Restrictions
                        // check width & height
                        if ($max_width !== 0 && $max_height !== 0) {
                            $imagedata = GetImageSize($temp_file);
                            if (is_bool($imagedata)) {
                                $media_response[$media_name] = $lang['media_object_invalid_image'];
                                continue;
                            }
                            /**
                             * @var integer $imagewidth
                             */
                            $imagewidth = $imagedata[0];
                            /**
                             * @var integer $imageheight
                             */
                            $imageheight = $imagedata[1];
                            if ($allow_resize) {
                                $shrinkage = 1;
                                // Figure out what the sizes are going to be AFTER resizing the images to know if we should allow the upload or not
                                if ($resize_by == 'width') {
                                    if ($imagewidth > $max_width) {
                                        $shrinkage = $imagewidth / $max_width;
                                    }
                                    $new_img_height = round($imageheight / $shrinkage);
                                    if ($new_img_height > $max_height) {
                                        $media_response[$media_name] = $lang['upload_too_high'];
                                        unlink($temp_file);
                                        continue;
                                    }
                                } elseif ($resize_by == 'height') {
                                    if ($imageheight > $max_height) {
                                        $shrinkage = $imageheight / $max_height;
                                    }
                                    $new_img_width = round($imagewidth / $shrinkage);
                                    if ($new_img_width > $max_width) {
                                        $media_response[$media_name] = $lang['upload_too_wide'];
                                        unlink($temp_file);
                                        continue;
                                    }
                                } elseif ($resize_by == 'both') {
                                } elseif ($resize_by == 'bestfit') {
                                }
                            } else {
                                if ($max_width != 0 && $imagewidth > $max_width) {
                                    $media_response[$media_name] = $lang['upload_too_wide'];
                                    unlink($temp_file);
                                    continue;
                                }
                                if ($max_height != 0) {
                                    if ($imageheight > $max_height) {
                                        $media_response[$media_name] = $lang['upload_too_high'];
                                        unlink($temp_file);
                                        continue;
                                    }
                                }
                            }
                        }
                    }

                    switch ($media_type) {
                        case 'listingsimages':
                            $thumb_name = $save_name; // by default -- no difference... unless...
                            if ($save_file && is_string($temp_file)) {
                                if (!@rename($temp_file, $this->config['listings_upload_path'] . '/' . $filename)) {
                                    if (copy($temp_file, $this->config['listings_upload_path'] . '/' . $filename)) {
                                        unlink($temp_file);
                                    } else {
                                        unlink($temp_file);
                                        $media_response[$media_name] = $lang['media_object_rename_failed'] . ' ' . $temp_file . ' -> ' . $this->config['listings_upload_path'] . '/' . $filename;
                                        continue 2;
                                    }
                                }
                                if ($allow_resize && ($imagewidth > $max_width || $imageheight > $max_height)) {
                                    // if the option to resize the images on upload is activated...
                                    if ($this->config['thumbnail_prog'] == 'imagemagick') {
                                        $this->resizeImageImageMagick($filename, $this->config['listings_upload_path'], $media_type);
                                    } else {
                                        $this->resizeImageGD($filename, $this->config['listings_upload_path'], $media_type);
                                    }
                                } // end if $this->config[resize_img] === "1"
                                if ($this->config['make_thumbnail']) {
                                    // if the option to make a thumbnail is activated...
                                    if ($this->config['thumbnail_prog'] == 'imagemagick') {
                                        $thumb_name = $this->makeThumbImageMagick($filename, $this->config['listings_upload_path'], $media_type);
                                    } else {
                                        $thumb_name = $this->makeThumbnailGD($filename, $this->config['listings_upload_path'], $media_type);
                                    }
                                } // end if $this->config[make_thumbnail] === "1"
                            }
                            // Get Max Image Rank
                            if (!isset($media_info['rank'])) {
                                $rank = $this->getMediaMaxRank($media_type, $media_parent_id);
                                $rank++;
                            } else {
                                $rank = intval($media_info['rank']);
                            }
                            //Deal with Caption and Description.
                            $caption = $media_info['caption'] ?? '';
                            $description = $media_info['description'] ?? '';
                            $active = 'yes';
                            $this->writeMediaToDatabase($media_type, $media_parent_id, $owner, $save_name, $thumb_name, $rank, $caption, $description, $active);


                            $media_response[$media_name] = "$lang[log_uploaded_listing_image] $filename";
                            $media_error[$media_name] = false;
                            @chmod($this->config['listings_upload_path'] . "/$filename", 0777);
                            break;
                        case 'userimages':
                            $thumb_name = $save_name; // by default -- no difference... unless...
                            if ($save_file && is_string($temp_file)) {
                                if (!@rename($temp_file, $this->config['user_upload_path'] . '/' . $filename)) {
                                    if (copy($temp_file, $this->config['user_upload_path'] . '/' . $filename)) {
                                        unlink($temp_file);
                                    } else {
                                        unlink($temp_file);
                                        $media_response[$media_name] = $lang['media_object_rename_failed'] . ' ' . $temp_file . ' -> ' . $this->config['user_upload_path'] . '/' . $filename;
                                        continue 2;
                                    }
                                }
                                if ($allow_resize && ($imagewidth > $max_width || $imageheight > $max_height)) {
                                    // if the option to resize the images on upload is activated...
                                    if ($this->config['thumbnail_prog'] == 'imagemagick') {
                                        $this->resizeImageImageMagick($filename, $this->config['user_upload_path'], $media_type);
                                    } else {
                                        $this->resizeImageGD($filename, $this->config['user_upload_path'], $media_type);
                                    }
                                } // end if $this->config[resize_img] === "1"
                                if ($this->config['make_thumbnail']) {
                                    if ($this->config['thumbnail_prog'] == 'imagemagick') {
                                        $thumb_name = $this->makeThumbImageMagick($filename, $this->config['user_upload_path'], $media_type);
                                    } else {
                                        $thumb_name = $this->makeThumbnailGD($filename, $this->config['user_upload_path'], $media_type);
                                    }
                                } // end if $this->config[make_thumbnail] === "1"
                            }
                            // Get Max Image Rank
                            if (!isset($media_info['rank'])) {
                                $rank = $this->getMediaMaxRank($media_type, $media_parent_id);
                                $rank++;
                            } else {
                                $rank = intval($media_info['rank']);
                            }
                            //Deal with Caption and Description.
                            $caption = $media_info['caption'] ?? '';
                            $description = $media_info['description'] ?? '';
                            $active = 'yes';
                            $this->writeMediaToDatabase($media_type, $media_parent_id, 0, $save_name, $thumb_name, $rank, $caption, $description, $active);

                            $media_response[$media_name] = "$lang[log_uploaded_user_image] $filename";
                            $media_error[$media_name] = false;
                            @chmod($this->config['user_upload_path'] . "/$filename", 0777);
                            break;
                        case 'listingsfiles':
                            $uploadpath = $this->config['listings_file_upload_path'] . '/' . $media_parent_id;
                            @mkdir($uploadpath);
                            if ($save_file && is_string($temp_file)) {
                                if (!@rename($temp_file, $uploadpath . '/' . $filename)) {
                                    if (copy($temp_file, $uploadpath . '/' . $filename)) {
                                        unlink($temp_file);
                                    } else {
                                        unlink($temp_file);
                                        $media_response[$media_name] = $lang['media_object_rename_failed'] . ' ' . $temp_file . ' -> ' . $uploadpath . '/' . $filename;
                                        continue 2;
                                    }
                                }
                            }
                            // Get Max Files Rank
                            if (!isset($media_info['rank'])) {
                                $rank = $this->getMediaMaxRank($media_type, $media_parent_id);
                                $rank++;
                            } else {
                                $rank = intval($media_info['rank']);
                            }
                            //Deal with Caption and Description.
                            $caption = $media_info['caption'] ?? '';
                            $description = $media_info['description'] ?? '';
                            $active = 'yes';
                            $this->writeMediaToDatabase($media_type, $media_parent_id, $owner, $save_name, '', $rank, $caption, $description, $active);


                            $media_response[$media_name] = "$uploadpath $filename";
                            $media_error[$media_name] = false;
                            @chmod("$uploadpath/$filename", 0777);
                            break;
                        case 'usersfiles':
                            $uploadpath = $this->config['users_file_upload_path'] . '/' . $media_parent_id;
                            @mkdir($uploadpath);
                            if ($save_file && is_string($temp_file)) {
                                if (!@rename($temp_file, $uploadpath . '/' . $filename)) {
                                    if (copy($temp_file, $uploadpath . '/' . $filename)) {
                                        unlink($temp_file);
                                    } else {
                                        unlink($temp_file);
                                        $media_response[$media_name] = $lang['media_object_rename_failed'] . ' ' . $temp_file . ' -> ' . $uploadpath . '/' . $filename;
                                        continue 2;
                                    }
                                }
                            }
                            // Get Max Image Rank
                            if (!isset($media_info['rank'])) {
                                $rank = $this->getMediaMaxRank($media_type, $media_parent_id);
                                $rank++;
                            } else {
                                $rank = intval($media_info['rank']);
                            }
                            //Deal with Caption and Description.
                            $caption = $media_info['caption'] ?? '';
                            $description = $media_info['description'] ?? '';
                            $active = 'yes';
                            $this->writeMediaToDatabase($media_type, $media_parent_id, 0, $save_name, '', $rank, $caption, $description, $active);

                            $media_response[$media_name] = "$uploadpath $filename";
                            $media_error[$media_name] = false;
                            @chmod("$uploadpath/$filename", 0777);
                            break;
                        case 'listingsvtours':
                            $thumb_name = $save_name; // by default -- no difference... unless...
                            if ($save_file && is_string($temp_file)) {
                                if (!@rename($temp_file, $this->config['vtour_upload_path'] . '/' . $filename)) {
                                    if (copy($temp_file, $this->config['vtour_upload_path'] . '/' . $filename)) {
                                        unlink($temp_file);
                                    } else {
                                        unlink($temp_file);
                                        $media_response[$media_name] = $lang['media_object_rename_failed'] . ' ' . $temp_file . ' -> ' . $this->config['vtour_upload_path'] . '/' . $filename;
                                        continue 2;
                                    }
                                }
                            }
                            // Get Max Image Rank
                            if (!isset($media_info['rank'])) {
                                $rank = $this->getMediaMaxRank($media_type, $media_parent_id);
                                $rank++;
                            } else {
                                $rank = intval($media_info['rank']);
                            }
                            //Deal with Caption and Description.
                            $caption = $media_info['caption'] ?? '';
                            $description = $media_info['description'] ?? '';
                            $active = 'yes';
                            $this->writeMediaToDatabase($media_type, $media_parent_id, 0, $save_name, $thumb_name, $rank, $caption, $description, $active);


                            $media_response[$media_name] = "$lang[log_uploaded_listing_image] $filename";
                            $media_error[$media_name] = false;
                            @chmod($this->config['vtour_upload_path'] . "/$filename", 0777);
                            break;
                    }

                    $mediainfo = [];
                    $mediainfo['file_name'] = $filename;
                    $mediainfo['media_type'] = $media_type;
                    $mediainfo['media_parent_id'] = $media_parent_id;

                    //Call afterMediaCreate hoook
                    $hooks = new Hooks($this->dbh, $this->config);
                    $hooks->load('afterMediaCreate', $mediainfo);

                    $image_import_count++;
                }
            }
            return ['media_response' => $media_response, 'media_error' => $media_error];
        } else {
            throw new Exception('permission_denied');
        }
    }

    /**
     * This function reads media information (get's photos, files, etc). (Avaliable in 3.0.13)
     * userimages $media_type added 1/07/2017
     *
     * @param array $data Data array should contain the following elements.
     *                    <ul>
     *                    <li>$data['media_parent_id'] - This should be either the User ID or Listing ID that the media
     *                    is associated with.</li>
     *                    <li>$data['media_type'] - Type of Media
     *                    ('listingsimages','listingsfiles','listingsvtours','userimages','usersfiles') (Only
     *                    listingsimages and userimages is avaliable currently)</li>
     *                    <li>$data['media_output'] - DATA/URL. DATA will return the raw binary image, instead of the
     *                    URL. You may not get all images if using DATA mode, as remote images will not be
     *                    returned</li>
     *                    </ul>
     *
     * @return array
     */

    /**
     * This function reads media information (get's photos, files, etc). (Avaliable in 3.0.13)
     *
     * @param array{
     *           media_parent_id: integer,
     *           media_type: string,
     *           media_output: 'DATA' | 'URL',
     *           media_limit?: integer
     *           } $data
     * @return array{
     *                  media_object: list<array{
     *                       media_id: integer,
     *                       media_rank: integer,
     *                       caption: string,
     *                       description: string,
     *                       thumb_file_name?: string,
     *                       file_name: string,
     *                       file_size?: int,
     *                       remote?: bool,
     *                       file_width?: integer,
     *                       file_height?: integer,
     *                       thumb_width?: integer,
     *                       thumb_height?: integer,
     *                       thumb_file_size?: int,
     *                       thumb_file_src?: string,
     *                       file_src?: string
     *               }>,
     *                  media_count: integer
     *              }
     * @throws Exception
     */
    public function read(array $data): array
    {
        $media_output_types = ['DATA', 'URL'];
        $login = new Login($this->dbh, $this->config);
        $agent_status = $login->verifyPriv('Agent');

        //Check that required settings were passed
        if (!isset($data['media_type']) || !in_array($data['media_type'], self::MEDIA_TYPES)) {
            //throw new \Exception('correct_parameter_not_passed - media_type');
            throw new Exception('correct_parameter_not_passed - media_type');
        }
        $media_type = $data['media_type'];
        if (!isset($data['media_output']) || !in_array($data['media_output'], $media_output_types)) {
            throw new Exception('correct_parameter_not_passed - media_output');
        }
        $media_output = $data['media_output'];
        if (!isset($data['media_parent_id'])) {
            throw new Exception('media_parent_id: correct_parameter_not_passed - media_parent_id');
        }
        $media_parent_id = $data['media_parent_id'];

        $LIMITstr = '';
        //optional
        $dbIntParam = [];
        if (isset($data['media_limit']) && $data['media_limit'] != 0) {
            $LIMITstr = "LIMIT :limit";
            $dbIntParam[':limit'] = $data['media_limit'];
        }
        $dbIntParam[':parent_id'] = $media_parent_id;

        $media_object = [];
        $media_count = 0;
        switch ($media_type) {
            case 'listingsimages':
                if ($agent_status) {
                    $sql = 'SELECT listingsimages_id, listingsimages_caption, listingsimages_description, listingsimages_file_name, listingsimages_thumb_file_name,listingsimages_rank 
							FROM ' . $this->config['table_prefix'] . "listingsimages 
							WHERE (listingsdb_id = :parent_id) ORDER BY listingsimages_rank
							$LIMITstr";
                } else {
                    $sql = 'SELECT listingsimages_id, listingsimages_caption, listingsimages_description, listingsimages_file_name, listingsimages_thumb_file_name,listingsimages_rank 
							FROM ' . $this->config['table_prefix'] . 'listingsimages 
							LEFT JOIN ' . $this->config['table_prefix'] . 'listingsdb 
							ON ' . $this->config['table_prefix'] . 'listingsimages.listingsdb_id = ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id 
							WHERE ' . $this->config['table_prefix'] . "listingsimages.listingsdb_id = :parent_id AND listingsdb_active = 'yes'  
							ORDER BY listingsimages_rank
							$LIMITstr";
                }

                try {
                    $stmt = $this->dbh->prepare($sql);
                    if (is_bool($stmt)) {
                        throw new Exception('Prepairing Statement Failed');
                    }
                    foreach ($dbIntParam as $p => $v) {
                        $stmt->bindValue($p, $v, PDO::PARAM_INT);
                    }
                    $stmt->execute();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }
                $recordSet = $stmt->fetchAll();
                $media_count = count($recordSet);
                foreach ($recordSet as $record) {
                    $this_media = [];
                    $this_media['media_id'] = (int)$record['listingsimages_id'];
                    $this_media['media_rank'] = (int)$record['listingsimages_rank'];
                    $this_media['caption'] = (string)$record['listingsimages_caption'];
                    $this_media['description'] = (string)$record['listingsimages_description'];
                    $this_media['thumb_file_name'] = (string)$record['listingsimages_thumb_file_name'];
                    $this_media['file_name'] = (string)$record['listingsimages_file_name'];

                    //Deal with Remote Images
                    if (str_starts_with($this_media['thumb_file_name'], 'http://') || str_starts_with($this_media['thumb_file_name'], 'https://') || str_starts_with($this_media['thumb_file_name'], '//')) {
                        if ($media_output == 'URL') {
                            $this_media['thumb_file_src'] = (string)$record['listingsimages_thumb_file_name'];
                            $this_media['file_src'] = (string)$record['listingsimages_file_name'];
                            $this_media['remote'] = true;
                        }
                    } else {
                        if (file_exists($this->config['listings_upload_path'] . '/' . $this_media['thumb_file_name'])) {
                            $thumb_imagedata = GetImageSize($this->config['listings_upload_path'] . '/' . $this_media['thumb_file_name']);
                            $thumb_imagewidth = 0;
                            $thumb_imageheight = 0;
                            if (!is_bool($thumb_imagedata)) {
                                /** @var integer */
                                $thumb_imagewidth = $thumb_imagedata[0];
                                /** @var integer */
                                $thumb_imageheight = $thumb_imagedata[1];
                            }
                            $this_media['thumb_width'] = $thumb_imagewidth;
                            $this_media['thumb_height'] = $thumb_imageheight;
                            if ($media_output == 'URL') {
                                $this_media['thumb_file_src'] = $this->config['listings_view_images_path'] . '/' . $this_media['thumb_file_name'];
                            } else {
                                $src = file_get_contents($this->config['listings_upload_path'] . '/' . $this_media['thumb_file_name']);
                                if (is_string($src)) {
                                    $this_media['thumb_file_src'] = $src;
                                }
                            }
                        }
                        if (file_exists($this->config['listings_upload_path'] . '/' . $this_media['file_name'])) {
                            $imagedata = GetImageSize($this->config['listings_upload_path'] . '/' . $this_media['file_name']);
                            $imagewidth = 0;
                            $imageheight = 0;
                            if (!is_bool($imagedata)) {
                                /** @var integer */
                                $imagewidth = $imagedata[0];
                                /** @var integer */
                                $imageheight = $imagedata[1];
                            }
                            $this_media['file_width'] = $imagewidth;
                            $this_media['file_height'] = $imageheight;
                            if ($media_output == 'URL') {
                                $this_media['file_src'] = $this->config['listings_view_images_path'] . '/' . $this_media['file_name'];
                            } else {
                                $src = file_get_contents($this->config['listings_upload_path'] . '/' . $this_media['file_name']);
                                if (is_string($src)) {
                                    $this_media['file_src'] = $src;
                                }
                            }
                        }
                        $this_media['remote'] = false;
                    }
                    $media_object[] = $this_media;
                } // end while
                break;

            case 'listingsfiles':
                if ($agent_status) {
                    $sql = 'SELECT listingsfiles_id, listingsfiles_caption, listingsfiles_description, listingsfiles_file_name, listingsfiles_rank 
							FROM ' . $this->config['table_prefix'] . "listingsfiles 
							WHERE (listingsdb_id = :parent_id) ORDER BY listingsfiles_rank
							$LIMITstr";
                } else {
                    $sql = 'SELECT listingsfiles_id, listingsfiles_caption, listingsfiles_description, listingsfiles_file_name, listingsfiles_rank 
							FROM ' . $this->config['table_prefix'] . 'listingsfiles 
							LEFT JOIN ' . $this->config['table_prefix'] . 'listingsdb 
							ON ' . $this->config['table_prefix'] . 'listingsfiles.listingsdb_id = ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id 
							WHERE ' . $this->config['table_prefix'] . "listingsfiles.listingsdb_id = :parent_id AND listingsdb_active = 'yes'  
							ORDER BY listingsfiles_rank
							$LIMITstr";
                }

                try {
                    $stmt = $this->dbh->prepare($sql);
                    if (is_bool($stmt)) {
                        throw new Exception('Prepairing Statement Failed');
                    }
                    foreach ($dbIntParam as $p => $v) {
                        $stmt->bindValue($p, $v, PDO::PARAM_INT);
                    }
                    $stmt->execute();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }
                $recordSet = $stmt->fetchAll();
                $media_count = count($recordSet);
                foreach ($recordSet as $record) {
                    $this_media = [];
                    $this_media['media_id'] = (int)$record['listingsfiles_id'];
                    $this_media['media_rank'] = (int)$record['listingsfiles_rank'];
                    $this_media['caption'] = (string)$record['listingsfiles_caption'];
                    $this_media['description'] = (string)$record['listingsfiles_description'];
                    $this_media['file_name'] = (string)$record['listingsfiles_file_name'];

                    //Deal with Remote files
                    if (str_starts_with($this_media['file_name'], 'http://') || str_starts_with($this_media['file_name'], 'https://') || str_starts_with($this_media['file_name'], '//')) {
                        if ($media_output == 'URL') {
                            $this_media['file_src'] = (string)$record['listingsfiles_file_name'];
                        }
                    }
                    if ($media_output == 'URL') {
                        $this_media['file_src'] = $this->config['listings_view_file_path'] . '/' . $media_parent_id . '/' . $this_media['file_name'];
                        if (file_exists($this_media['file_src'])) {
                            $size = filesize($this_media['file_src']);
                            if (is_int($size)) {
                                $this_media['file_size'] = $size;
                            }
                        }
                    } else {
                        $src = file_get_contents($this->config['listings_file_upload_path'] . '/' . $this_media['file_name']);
                        if (is_string($src)) {
                            $this_media['thumb_file_src'] = $src;
                        }
                    }

                    $media_object[] = $this_media;
                } // end while
                break;

            case 'userimages':
                if ($agent_status) {
                    $sql = 'SELECT userimages_id, userimages_caption, userimages_description, userimages_file_name, userimages_thumb_file_name,userimages_rank 
							FROM ' . $this->config['table_prefix'] . "userimages 
							WHERE (userdb_id = :parent_id) ORDER BY userimages_rank
							$LIMITstr";
                } else {
                    $sql = 'SELECT userimages_id, userimages_caption, userimages_description, userimages_file_name, userimages_thumb_file_name,userimages_rank 
							FROM ' . $this->config['table_prefix'] . 'userimages 
							LEFT JOIN ' . $this->config['table_prefix'] . 'userdb 
							ON ' . $this->config['table_prefix'] . 'userimages.userdb_id = ' . $this->config['table_prefix'] . 'userdb.userdb_id 
							WHERE ' . $this->config['table_prefix'] . "userimages.userdb_id = :parent_id AND userdb_active = 'yes'  
							ORDER BY userimages_rank
							$LIMITstr";
                }

                try {
                    $stmt = $this->dbh->prepare($sql);
                    if (is_bool($stmt)) {
                        throw new Exception('Prepairing Statement Failed');
                    }
                    foreach ($dbIntParam as $p => $v) {
                        $stmt->bindValue($p, $v, PDO::PARAM_INT);
                    }
                    $stmt->execute();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }
                $recordSet = $stmt->fetchAll();
                $media_count = count($recordSet);
                foreach ($recordSet as $record) {
                    $this_media = [];
                    $this_media['media_id'] = (int)$record['userimages_id'];
                    $this_media['media_rank'] = (int)$record['userimages_rank'];
                    $this_media['caption'] = (string)$record['userimages_caption'];
                    $this_media['description'] = (string)$record['userimages_description'];
                    $this_media['thumb_file_name'] = (string)$record['userimages_thumb_file_name'];
                    $this_media['file_name'] = (string)$record['userimages_file_name'];

                    //Deal with Remote Images
                    if (str_starts_with($this_media['thumb_file_name'], 'http://') || str_starts_with($this_media['thumb_file_name'], 'https://') || str_starts_with($this_media['thumb_file_name'], '//')) {
                        if ($media_output == 'URL') {
                            $this_media['thumb_file_src'] = (string)$record['userimages_thumb_file_name'];
                            $this_media['file_src'] = (string)$record['userimages_file_name'];
                        }
                    } else {
                        if (file_exists($this->config['user_upload_path'] . '/' . $this_media['thumb_file_name'])) {
                            $thumb_imagedata = GetImageSize($this->config['user_upload_path'] . '/' . $this_media['thumb_file_name']);
                            $thumb_imagewidth = 0;
                            $thumb_imageheight = 0;
                            if (!is_bool($thumb_imagedata)) {
                                /** @var integer */
                                $thumb_imagewidth = $thumb_imagedata[0];
                                /** @var integer */
                                $thumb_imageheight = $thumb_imagedata[1];
                            }
                            $this_media['thumb_width'] = $thumb_imagewidth;
                            $this_media['thumb_height'] = $thumb_imageheight;
                            if ($media_output == 'URL') {
                                $this_media['thumb_file_src'] = $this->config['user_view_images_path'] . '/' . $this_media['thumb_file_name'];
                                if (file_exists($this_media['thumb_file_src'])) {
                                    $size = filesize($this_media['thumb_file_src']);
                                    if (is_int($size)) {
                                        $this_media['thumb_file_size'] = $size;
                                    }
                                }
                            } else {
                                $src = file_get_contents($this->config['user_upload_path'] . '/' . $this_media['thumb_file_name']);
                                if (is_string($src)) {
                                    $this_media['thumb_file_src'] = $src;
                                }
                            }
                        }
                        if (file_exists($this->config['user_upload_path'] . '/' . $this_media['file_name'])) {
                            $imagedata = GetImageSize($this->config['user_upload_path'] . '/' . $this_media['file_name']);
                            $imagewidth = 0;
                            $imageheight = 0;
                            if (!is_bool($imagedata)) {
                                /** @var integer */
                                $imagewidth = $imagedata[0];
                                /** @var integer */
                                $imageheight = $imagedata[1];
                            }
                            $this_media['file_width'] = $imagewidth;
                            $this_media['file_height'] = $imageheight;
                            if ($media_output == 'URL') {
                                $this_media['file_src'] = $this->config['user_view_images_path'] . '/' . $this_media['file_name'];
                                if (file_exists($this_media['file_src'])) {
                                    $size = filesize($this_media['file_src']);
                                    if (is_int($size)) {
                                        $this_media['file_size'] = $size;
                                    }
                                }
                            } else {
                                $src = file_get_contents($this->config['user_upload_path'] . '/' . $this_media['file_name']);
                                if (is_string($src)) {
                                    $this_media['file_src'] = $src;
                                }
                            }
                        }
                    }
                    $media_object[] = $this_media;
                } // end while
                break;

            case 'usersfiles':
                if ($agent_status) {
                    $sql = 'SELECT usersfiles_id, usersfiles_caption, usersfiles_description, usersfiles_file_name, usersfiles_rank 
							FROM ' . $this->config['table_prefix'] . "usersfiles 
							WHERE (userdb_id = :parent_id) ORDER BY usersfiles_rank
							$LIMITstr";
                } else {
                    $sql = 'SELECT usersfiles_id, usersfiles_caption, usersfiles_description, usersfiles_file_name, usersfiles_rank 
							FROM ' . $this->config['table_prefix'] . 'usersfiles 
							LEFT JOIN ' . $this->config['table_prefix'] . 'listingsdb 
							ON ' . $this->config['table_prefix'] . 'usersfiles.userdb_id = ' . $this->config['table_prefix'] . 'listingsdb.userdb_id 
							WHERE ' . $this->config['table_prefix'] . "usersfiles.userdb_id = :parent_id) AND userdb_active = 'yes'  
							ORDER BY usersfiles_rank
							$LIMITstr";
                }

                try {
                    $stmt = $this->dbh->prepare($sql);
                    if (is_bool($stmt)) {
                        throw new Exception('Prepairing Statement Failed');
                    }
                    foreach ($dbIntParam as $p => $v) {
                        $stmt->bindValue($p, $v, PDO::PARAM_INT);
                    }
                    $stmt->execute();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }
                $recordSet = $stmt->fetchAll();
                $media_count = count($recordSet);
                foreach ($recordSet as $record) {
                    $this_media = [];
                    $this_media['media_id'] = (int)$record['usersfiles_id'];
                    $this_media['media_rank'] = (int)$record['usersfiles_rank'];
                    $this_media['caption'] = (string)$record['usersfiles_caption'];
                    $this_media['description'] = (string)$record['usersfiles_description'];
                    $this_media['file_name'] = (string)$record['usersfiles_file_name'];


                    //Deal with Remote files
                    if (str_starts_with($this_media['file_name'], 'http://') || str_starts_with($this_media['file_name'], 'https://') || str_starts_with($this_media['file_name'], '//')) {
                        if ($media_output == 'URL') {
                            $this_media['file_src'] = (string)$record['usersfiles_file_name'];
                        }
                    }
                    if ($media_output == 'URL') {
                        $this_media['file_src'] = $this->config['users_view_file_path'] . '/' . $media_parent_id . '/' . $this_media['file_name'];
                        if (file_exists($this_media['file_src'])) {
                            $size = filesize($this_media['file_src']);
                            if (is_int($size)) {
                                $this_media['file_size'] = $size;
                            }
                        }
                    } else {
                        $src = file_get_contents($this->config['users_file_upload_path'] . '/' . $this_media['file_name']);
                        if (is_string($src)) {
                            $this_media['file_src'] = $src;
                        }
                    }
                    $media_object[] = $this_media;
                } // end while
                break;
        }
        return ['media_object' => $media_object, 'media_count' => $media_count];
    }

    /**
     * @param array{
     *      media_type: string,
     *      media_parent_id: integer,
     *      media_object_id: integer | '*',
     *      disable_logs?: boolean
     *      } $data
     *
     * @return array
     * @throws Exception
     */
    public function delete(array $data): array
    {
        global $lang;
        $login = new Login($this->dbh, $this->config);
        //Check that required settings were passed
        if (!isset($data['media_type']) || !in_array($data['media_type'], self::MEDIA_TYPES)) {
            throw new Exception('correct_parameter_not_passed');
        }
        if (!isset($data['media_parent_id'])) {
            throw new Exception('media_parent_id: correct_parameter_not_passed');
        }
        if (!isset($data['media_object_id'])) {
            throw new Exception('media_object_id: correct_parameter_not_passed');
        }
        $folderpresent = false;
        $owner = $this->mediaPermissionCheck($data['media_type'], 0, $data['media_parent_id']);
        if (is_integer($owner)) {
            switch ($data['media_type']) {
                case 'listingsimages':
                    $has_thumb = true;
                    $path = $this->config['listings_upload_path'];
                    $foriegnkey = 'listingsdb_id';
                    break;
                case 'userimages':
                    $has_thumb = true;
                    $path = $this->config['user_upload_path'];
                    $foriegnkey = 'userdb_id';
                    break;
                case 'listingsvtours':
                    $has_thumb = true;
                    $path = $this->config['vtour_upload_path'];
                    $foriegnkey = 'listingsdb_id';
                    break;
                case 'listingsfiles':
                    $has_thumb = false;
                    $path = $this->config['listings_file_upload_path'] . '/' . $data['media_parent_id'];
                    $foriegnkey = 'listingsdb_id';
                    break;
                case 'usersfiles':
                    $has_thumb = false;
                    $path = $this->config['users_file_upload_path'] . '/' . $data['media_parent_id'];
                    $foriegnkey = 'userdb_id';
                    break;
                default:
                    throw new Exception('correct_parameter_not_passed');
            }
            $dbIntParam = [];
            $dbIntParam[':parent_id'] = $data['media_parent_id'];
            if ($has_thumb) {
                if ($data['media_object_id'] == '*') {
                    $sql = 'SELECT ' . $data['media_type'] . '_file_name, ' . $data['media_type'] . '_thumb_file_name 
							FROM ' . $this->config['table_prefix'] . $data['media_type'] . " 
							WHERE $foriegnkey = :parent_id";
                } else {
                    $sql = 'SELECT ' . $data['media_type'] . '_file_name, ' . $data['media_type'] . '_thumb_file_name 
							FROM ' . $this->config['table_prefix'] . $data['media_type'] . " 
							WHERE $foriegnkey = :parent_id 
							AND " . $data['media_type'] . "_id = :media_id";
                    $dbIntParam[':media_id'] = $data['media_object_id'];
                }
            } else {
                if ($data['media_object_id'] == '*') {
                    $sql = 'SELECT ' . $data['media_type'] . '_file_name 
							FROM ' . $this->config['table_prefix'] . $data['media_type'] . " 
							WHERE $foriegnkey = :parent_id";
                    //we just deleted everything, so no need to keep the associated /data['media_parent_id']/ folder.
                    $folderpresent = true;
                } else {
                    $sql = 'SELECT ' . $data['media_type'] . '_file_name 
							FROM ' . $this->config['table_prefix'] . $data['media_type'] . " 
							WHERE $foriegnkey = :parent_id AND " . $data['media_type'] . "_id = :media_id";
                    $dbIntParam[':media_id'] = $data['media_object_id'];
                }
            }

            try {
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
                foreach ($dbIntParam as $p => $v) {
                    $stmt->bindValue($p, $v, PDO::PARAM_INT);
                }
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }

            $recordSet = $stmt->fetchAll();
            $mediainfo = [];
            //setup the vars we need to send to the hook
            $mediainfo['media_type'] = $data['media_type'];
            $mediainfo['media_parent_id'] = $data['media_parent_id'];

            foreach ($recordSet as $record) {
                $file_name = (string)$record[$data['media_type'] . '_file_name'];
                $thumb_file_name = '';
                if ($has_thumb) {
                    $thumb_file_name = (string)$record[$data['media_type'] . '_thumb_file_name'];
                }

                $mediainfo['file_name'] = $file_name;

                //call the before_delete hook
                $hooks = new Hooks($this->dbh, $this->config);
                $hooks->load('beforeMediaCreate', $mediainfo);

                //Delete Full Photo
                if (!str_starts_with($file_name, 'https://') && !str_starts_with($file_name, 'http://') && !str_starts_with($file_name, '//')) {
                    try {
                        unlink($path . '/' . $file_name);
                    } catch (Exception $e) {
                        $log_api = new LogApi($this->dbh, $this->config);
                        $log_api->create(['log_api_command' => 'api->media->delete', 'log_message' => 'missing file: ' . $file_name . ' ' . $e]);
                    }
                }

                // Delete Thumbnail
                if ($has_thumb && (!str_starts_with($thumb_file_name, 'http://') && !str_starts_with($thumb_file_name, 'https://') && !str_starts_with($thumb_file_name, '//'))) {
                    try {
                        if ($file_name != $thumb_file_name) {
                            unlink($path . '/' . $thumb_file_name);
                        }
                    } catch (Exception $e) {
                        $log_api = new LogApi($this->dbh, $this->config);
                        $log_api->create(['log_api_command' => 'api->media->delete', 'log_message' => 'missing file: ' . $thumb_file_name . ' ' . $e]);
                    }
                }
            } // end while
            //nuke the /media_parent_id/ folder if there was one
            if ($folderpresent && count($recordSet) > 0) {
                if (file_exists($path) === true) {
                    rmdir($path);
                }
            }
            $dbIntParam = [];
            $dbIntParam[':parent_id'] = $data['media_parent_id'];
            // delete from the db
            if ($data['media_object_id'] == '*') {
                $sql = 'DELETE FROM ' . $this->config['table_prefix'] . $data['media_type'] . " 
						WHERE $foriegnkey = :parent_id";
            } else {
                $sql = 'DELETE FROM ' . $this->config['table_prefix'] . $data['media_type'] . " 
						WHERE $foriegnkey = :parent_id AND " . $data['media_type'] . "_id = :media_id";
                $dbIntParam[':media_id'] = $data['media_object_id'];
            }
            try {
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
                foreach ($dbIntParam as $p => $v) {
                    $stmt->bindValue($p, $v, PDO::PARAM_INT);
                }
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }

            $admin_status = $login->verifyPriv('Admin');
            if (!$admin_status || !isset($data['disable_logs']) || !$data['disable_logs']) {
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->media->delete', 'log_message' => $lang['log_deleted_listing_image'] . ' ' . $data['media_type'] . ' - ' . $data['media_parent_id'] . ' - ' . $data['media_object_id']]);
            }

            $hooks = new Hooks($this->dbh, $this->config);
            $hooks->load('afterMediaDelete', $mediainfo);

            if ($data['media_object_id'] == '*') {
                $itemtype = 'ALL';
            } else {
                $itemtype = $data['media_object_id'];
            }

            return [
                'status_msg' => 'Deleted ' . $itemtype,
                'media_parent_id' => $data['media_parent_id'],
                'media_type' => $data['media_type'],
            ];
        } else {
            throw new Exception('permission denied: not media owner (API) - ' . $data['media_type'] . ' ' . $data['media_parent_id']);
        }
    }
}
