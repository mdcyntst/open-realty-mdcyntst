<?php

/**
 * This File Contains the Log API Commands
 *
 * @package    Open-Realty
 * @subpackage API
 * @author     Ryan C. Bonham
 * @copyright  2010
 * @link       http://www.open-realty.com Open-Realty
 */

declare(strict_types=1);

namespace OpenRealty\Api\Commands;

use Exception;
use OpenRealty\Login;
use PDO;

/**
 * This is the menu API, it contains all api calls for creating, modifying, and reading menus.
 *
 * @package    Open-Realty
 * @subpackage API
 **/
class MenuApi extends BaseCommand
{
    /**
     * This API Command provides a list of the avaliable menus.
     *
     * @param array{} $data Expects an array containing the following array keys.
     *
     * @returns array{
     *     menus: array<int, string>
     *     }
     * @throws \Exception
     */
    public function metadata(array $data): array
    {
        $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . 'menu ORDER BY menu_name';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $menus = [];
        $recordSet = $stmt->fetchAll();
        foreach ($recordSet as $record) {
            $menus[$record['menu_id']] = $record['menu_name'];
        }
        return ['menus' => $menus];
    }

    /**
     * @param array{
     *  menu_name : string
     *            } $data
     * @return array{
     *     menu_id: integer,
     *     menu_name: string
     * }
     * @throws \Exception
     */
    public function create(array $data): array
    {
        $login = new Login($this->dbh, $this->config);
        $security = $login->verifyPriv('editpages');
        if ($security !== true) {
            throw new Exception('permission_denied');
        }

        if (!isset($data['menu_name']) || trim($data['menu_name']) == '') {
            throw new Exception('menu_name: correct_parameter_not_passed');
        }
        $menu_name = trim($data['menu_name']);

        $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'menu (menu_name) VALUES (:menu_name)';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->bindValue(':menu_name', $menu_name);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }

        $menu_id = $this->dbh->lastInsertId();
        if (is_bool($menu_id)) {
            throw new Exception('Failed Getting Last Insert ID');
        }
        $menu_id = (int)$menu_id;
        $log_api = new LogApi($this->dbh, $this->config);
        $log_api->create(['log_api_command' => 'api->menu->create', 'log_message' => 'Menu Created: ' . $menu_name . '(' . $menu_id . ')']);
        return ['menu_id' => $menu_id, 'menu_name' => $menu_name];
    }

    /**
     * @param array{
     * menu_id: integer
     * } $data
     *
     * @return array{
     * menu_id: integer
     * }
     * @throws \Exception
     */
    public function delete(array $data): array
    {
        $login = new Login($this->dbh, $this->config);
        $security = $login->verifyPriv('editpages');
        if ($security !== true) {
            throw new Exception('permission_denied');
        }

        if (!isset($data['menu_id'])) {
            throw new Exception('menu_id: correct_parameter_not_passed');
        }


        $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'menu_items WHERE menu_id = :menu_id';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->bindValue(':menu_id', intval($data['menu_id']), PDO::PARAM_INT);

            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }

        $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'menu WHERE menu_id = :menu_id';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->bindValue(':menu_id', intval($data['menu_id']), PDO::PARAM_INT);

            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $log_api = new LogApi($this->dbh, $this->config);
        $log_api->create(['log_api_command' => 'api->menu->delete', 'log_message' => 'Menu Deleted: ' . $data['menu_id']]);
        return ['menu_id' => $data['menu_id']];
    }

    /**
     * Reads Menu items and groups by parent_id. Does not filter out based on visibility, that needs to be done client
     * side at the moment.
     *
     * @param array{
     *  menu_id: integer
     * } $data
     *
     * @return array{
     *     menu: array<int, array<int, array{
     *     item_id: integer,
     *     item_name: string,
     *     item_type: integer,
     *     item_value: string,
     *     item_target: string,
     *     item_class: string,
     *     visible_guest: boolean,
     *     visible_member: boolean,
     *     visible_agent: boolean,
     *     visible_admin: boolean
     *     }>>
     *     }
     * @throws \Exception
     */
    public function read(array $data): array
    {
        if (!isset($data['menu_id'])) {
            throw new Exception('menu_id: correct_parameter_not_passed');
        }
        $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . 'menu_items  WHERE menu_id = :menu_id ORDER BY parent_id, item_order';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }

            $stmt->bindValue(':menu_id', $data['menu_id'], PDO::PARAM_INT);

            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $recordSet = $stmt->fetchAll();
        $menu = [];
        $x = 1;
        foreach ($recordSet as $record) {
            $menu[(int)$record['parent_id']][$x] = [
                'item_id' => (int)$record['item_id'],
                'item_name' => (string)$record['item_name'],
                'item_type' => (int)$record['item_type'],
                'item_value' => (string)$record['item_value'],
                'item_target' => (string)$record['item_target'],
                'item_class' => (string)$record['item_class'],
                'visible_guest' => (bool)$record['visible_guest'],
                'visible_member' => (bool)$record['visible_member'],
                'visible_agent' => (bool)$record['visible_agent'],
                'visible_admin' => (bool)$record['visible_admin'],
            ];
            $x++;
        }
        return ['menu' => $menu];
    }

    /**
     * @param array{
     * item_id: integer
     * } $data
     *
     * @return array{
     *     menu_item: array{
     *     item_id: integer,
     *     parent_id: integer,
     *     menu_id: integer,
     *     item_name: string,
     *     item_order: integer,
     *     item_type: integer,
     *     item_value: string,
     *     item_target: string,
     *     item_class: string,
     *     visible_guest: boolean,
     *     visible_member: boolean,
     *     visible_agent: boolean,
     *     visible_admin: boolean
     *    }}
     * @throws \Exception
     */
    public function itemDetails(array $data): array
    {
        if (!isset($data['item_id'])) {
            throw new Exception('item_id: correct_parameter_not_passed');
        }
        $sql = 'SELECT * FROM ' . $this->config['table_prefix'] . 'menu_items WHERE item_id = :item_id';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }

            $stmt->bindValue(':item_id', $data['item_id'], PDO::PARAM_INT);

            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $recordSet = $stmt->fetchAll();
        $item = [];
        if (count($recordSet) == 1) {
            $item['item_id'] = (int)$recordSet[0]['item_id'];
            $item['parent_id'] = (int)$recordSet[0]['parent_id'];
            $item['menu_id'] = (int)$recordSet[0]['menu_id'];
            $item['item_name'] = (string)$recordSet[0]['item_name'];
            $item['item_order'] = (int)$recordSet[0]['item_order'];
            $item['item_type'] = (int)$recordSet[0]['item_type'];
            $item['item_value'] = (string)$recordSet[0]['item_value'];
            $item['item_target'] = (string)$recordSet[0]['item_target'];
            $item['item_class'] = (string)$recordSet[0]['item_class'];
            $item['visible_guest'] = (bool)$recordSet[0]['visible_guest'];
            $item['visible_member'] = (bool)$recordSet[0]['visible_member'];
            $item['visible_agent'] = (bool)$recordSet[0]['visible_agent'];
            $item['visible_admin'] = (bool)$recordSet[0]['visible_admin'];
        } else {
            throw new Exception('Unexpected Error, item not found');
        }
        return ['menu_item' => $item];
    }

    /**
     * @param array{
     *     menu_id: integer,
     *     menu_items: array<int, array{
     *     order: integer,
     *     parent: integer
     *     }>
     * } $data
     * @return array{ menu_id: integer}
     * @throws \Exception
     */
    public function setMenuOrder(array $data): array
    {
        $login = new Login($this->dbh, $this->config);
        $security = $login->verifyPriv('editpages');
        if ($security !== true) {
            throw new Exception('permission_denied');
        }

        if (!isset($data['menu_id'])) {
            throw new Exception('item_id: correct_parameter_not_passed');
        }
        if (!isset($data['menu_items'])) {
            throw new Exception('menu_items: correct_parameter_not_passed');
        }
        //Make Sure this is a valid menu
        $sql = 'SELECT 1 FROM ' . $this->config['table_prefix'] . 'menu WHERE menu_id = :menu_id';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->bindValue(':menu_id', $data['menu_id'], PDO::PARAM_INT);

            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $menu_count = (int)$stmt->fetchColumn();
        if ($menu_count == 0) {
            throw new Exception('Invalid Menu ID');
        }

        $sql = 'UPDATE ' . $this->config['table_prefix'] . 'menu_items SET item_order = :order_id, parent_id = :parent_id WHERE menu_id = :menu_id AND item_id = :item_id';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        foreach ($data['menu_items'] as $item_id => $item_array) {
            try {
                $stmt->bindValue(':order_id', $item_array['order'], PDO::PARAM_INT);
                $stmt->bindValue(':parent_id', $item_array['parent'], PDO::PARAM_INT);
                $stmt->bindValue(':menu_id', $data['menu_id'], PDO::PARAM_INT);
                $stmt->bindValue(':item_id', $item_id, PDO::PARAM_INT);

                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
        }
        return ['menu_id' => $data['menu_id']];
    }

    /**
     * @param array{
     * item_id: integer
     * } $data
     *
     * @return array{
     * item_id: integer
     * }
     * @throws \Exception
     */
    public function deleteMenuItem(array $data): array
    {
        $login = new Login($this->dbh, $this->config);
        $security = $login->verifyPriv('editpages');
        if ($security !== true) {
            throw new Exception('permission_denied');
        }

        if (!isset($data['item_id'])) {
            throw new Exception('item_name: correct_parameter_not_passed');
        }
        if ($data['item_id'] == 0) {
            throw new Exception('item_id can not be zero.');
        }
        $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'menu_items WHERE item_id = :item_id OR parent_id = :parent_id';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->bindValue(':item_id', $data['item_id'], PDO::PARAM_INT);
            $stmt->bindValue(':parent_id', $data['item_id'], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $log_api = new LogApi($this->dbh, $this->config);
        $log_api->create(['log_api_command' => 'api->menu->delete_menu_item', 'log_message' => 'Menu Item Deleted: ' . $data['item_id']]);
        return ['item_id' => $data['item_id']];
    }

    /**
     * @param array{
     *  item_name: string,
     *  menu_id: integer,
     * parent_id: integer,
     * } $data
     * @return array
     * @throws \Exception
     */
    public function addMenuItem(array $data): array
    {
        $login = new Login($this->dbh, $this->config);
        $security = $login->verifyPriv('editpages');
        if ($security !== true) {
            throw new Exception('permission_denied');
        }

        if (empty($data['item_name'])) {
            throw new Exception('item_name: correct_parameter_not_passed');
        }
        if (!isset($data['menu_id'])) {
            throw new Exception('menu_id: correct_parameter_not_passed');
        }
        if (!isset($data['parent_id'])) {
            throw new Exception('parent_id: correct_parameter_not_passed');
        }
        //Get Highest Item Order
        $sql = 'SELECT max(item_order) as max_order FROM ' . $this->config['table_prefix'] . 'menu_items WHERE menu_id = :menu_id AND parent_id = :parent_id';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->bindValue(':menu_id', $data['menu_id'], PDO::PARAM_INT);
            $stmt->bindValue(':parent_id', $data['parent_id'], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $max_order = (int)$stmt->fetchColumn();
        $max_order++;
        $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'menu_items (menu_id,item_name,parent_id,item_order) VALUES
			(:menu_id,:item_name,:parent_id,:max_order)';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->bindValue(':menu_id', $data['menu_id'], PDO::PARAM_INT);
            $stmt->bindValue(':parent_id', $data['parent_id'], PDO::PARAM_INT);
            $stmt->bindValue(':max_order', $max_order, PDO::PARAM_INT);
            $stmt->bindValue(':item_name', $data['item_name']);

            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $item_id = $this->dbh->lastInsertId();
        if (is_bool($item_id)) {
            throw new Exception('Failed Getting Last Insert ID');
        }
        return ['item_id' => (int)$item_id, 'parent_id' => intval($data['parent_id']), 'item_name' => $data['item_name']];
    }

    /**
     * @param array{
     *  item_id: integer,
     *  item_name: string,
     *  item_type: integer,
     *  item_value?: string,
     *  item_target?: string,
     *  item_class: string,
     *     visible_guest: boolean,
     *      visible_member: boolean,
     *     visible_agent: boolean,
     *     visible_admin: boolean
     * } $data
     *
     * @return array{item_id: integer}
     * @throws \Exception
     */
    public function saveMenuItem(array $data): array
    {
        $login = new Login($this->dbh, $this->config);
        $security = $login->verifyPriv('editpages');
        if ($security !== true) {
            throw new Exception('permission_denied');
        }
        if (!isset($data['item_id'])) {
            throw new Exception('item_id: correct_parameter_not_passed');
        }
        if (!isset($data['item_name'])) {
            throw new Exception('item_name: correct_parameter_not_passed');
        }
        if (!isset($data['item_type'])) {
            throw new Exception('item_type: correct_parameter_not_passed');
        }
        if (!isset($data['item_value'])) {
            $data['item_value'] = '';
        }
        if (empty($data['item_target'])) {
            $data['item_target'] = '_self';
        } elseif (!in_array($data['item_target'], ['_self', '_blank', '_parent', '_top'])) {
            throw new Exception('item_target: correct_parameter_not_passed');
        }
        if (!isset($data['item_class'])) {
            throw new Exception('item_class: correct_parameter_not_passed');
        }
        if (!isset($data['visible_guest'])) {
            throw new Exception('visible_guest: correct_parameter_not_passed');
        }
        if (!isset($data['visible_member'])) {
            throw new Exception('visible_member: correct_parameter_not_passed');
        }
        if (!isset($data['visible_agent'])) {
            throw new Exception('visible_agent: correct_parameter_not_passed');
        }
        if (!isset($data['visible_admin'])) {
            throw new Exception('visible_admin: correct_parameter_not_passed');
        }
        $sql = 'UPDATE ' . $this->config['table_prefix'] . 'menu_items
			SET item_type = :item_type,
			visible_guest = :visible_guest,
			visible_member = :visible_member,
			visible_agent = :visible_agent,
			visible_admin = :visible_admin,
			item_name = :item_name,
			item_value = :item_value,
			item_target = :item_target,
			item_class = :item_class
			WHERE item_id = :item_id';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->bindValue(':item_type', $data['item_type'], PDO::PARAM_INT);
            $stmt->bindValue(':visible_guest', $data['visible_guest'], PDO::PARAM_INT);
            $stmt->bindValue(':visible_member', $data['visible_member'], PDO::PARAM_INT);
            $stmt->bindValue(':visible_agent', $data['visible_agent'], PDO::PARAM_INT);
            $stmt->bindValue(':visible_admin', $data['visible_admin'], PDO::PARAM_INT);
            $stmt->bindValue(':item_id', $data['item_id'], PDO::PARAM_INT);
            $stmt->bindValue(':item_name', $data['item_name']);
            $stmt->bindValue(':item_value', $data['item_value']);
            $stmt->bindValue(':item_target', $data['item_target']);
            $stmt->bindValue(':item_class', $data['item_class']);

            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        return ['item_id' => $data['item_id']];
    }
}
