<?php

declare(strict_types=1);

/**
 * This File Contains the Twitter API Commands
 *
 * @package    Open-Realty
 * @subpackage API
 * @author     Ryan C. Bonham
 * @copyright  2010
 * @link       http://www.open-realty.com Open-Realty
 */

namespace OpenRealty\Api\Commands;

use Exception;
use OpenRealty\Api\Api;
use Abraham\TwitterOAuth\TwitterOAuth;
use OpenRealty\Login;

/**
 * This is the Twitter API, it contains all api calls for posting to twitter.
 *
 **/
class TwitterApi extends BaseCommand
{
    /**
     * This API Command posts a message to twitter.
     *
     * @param array{
     *          message: string,
     *          media?: string,
     *          media_remote?: boolean
     *         } $data Information To Post To Twitter.
     *
     * @return array{
     *          message: string
     *              }
     * @throws Exception All Fatal errors will throw an Exception, which include an error message.
     */
    public function post(array $data): array
    {
        $login = new Login($this->dbh, $this->config);
        $login_status = $login->verifyPriv('Agent');
        if ($login_status !== true) {
            throw new Exception('Login Failure');
        }

        //Check that required settings were passed
        if (empty($data['message'])) {
            throw new Exception('message: correct_parameter_not_passed');
        }
        if (strlen($data['message']) > 280) {
            throw new Exception('message: longer then 280 characters');
        }

        /**
         * @psalm-suppress MixedAssignment
         */
        $access_token = unserialize($this->config['twitter_auth']);

        if (!is_array($access_token) || !isset($access_token['oauth_token']) || !is_string($access_token['oauth_token']) || !isset($access_token['oauth_token_secret']) || !is_string($access_token['oauth_token_secret'])) {
            throw new Exception('Twitter Auth Token Not Valid');
        }

        $twitterOAuth = new TwitterOAuth($this->config['twitter_consumer_key'], $this->config['twitter_consumer_secret'], $access_token['oauth_token'], $access_token['oauth_token_secret']);
        $twitterOAuth->get('account/verify_credentials');

        //if media set
        if (isset($data['media']) && $data['media'] != '') {
            if (isset($data['media_remote']) && $data['media_remote'] === true) {
                /**
                 * @var  object $media_file
                 */
                $media_file = $twitterOAuth->upload('media/upload', ['media' => $data['media']]);
            } else {
                /**
                 * @var  object $media_file
                 */
                $media_file = $twitterOAuth->upload('media/upload', ['media' => $this->config['listings_upload_path'] . '/' . $data['media']]);
            }
            $parameters = [
                'text' => $data['message'],
                'media' => [
                    'media_ids' => [$media_file->media_id_string],
                ]];
        } else {
            $parameters = [
                'text' => $data['message'],
            ];
        }

        //post our tweet and get response
        $twitterOAuth->setApiVersion('2');
        $twitterOAuth->post('tweets', $parameters, true);
        $http_code = $twitterOAuth->getLastHttpCode();
        /**
         * @var  object $body
         */
        $body = ($twitterOAuth->getLastBody());
        switch ($http_code) {
            case 200:
            case 201:
                //success
                return ['message' => $data['message']];
            case 304:
                throw new Exception('304 Not Modified');
            case 403:
                /**
                 * @var array $errors
                 */
                $error_message = 'Error message not found';
                if (property_exists($body, 'errors')) {
                    if (isset($body->errors[0]->message)) {
                        $error_message = (string)$body->errors[0]->message;
                    }
                }
                if (property_exists($body, 'detail')) {
                    $error_message = (string)$body->detail;
                }
                throw new Exception('403 Twitter post Failed: ' . $error_message);
            default:
                throw new Exception('Twitter post Failed');
        }
    }
}
