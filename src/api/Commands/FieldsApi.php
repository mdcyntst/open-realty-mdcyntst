<?php

/**
 * This File Contains the Listing Field API Commands
 *
 * @package    Open-Realty
 * @subpackage API
 * @author     Ryan C. Bonham
 * @copyright  2010
 * @link       http://www.open-realty.com Open-Realty
 */

declare(strict_types=1);

namespace OpenRealty\Api\Commands;

use Exception;
use OpenRealty\Login;
use PDO;

/**
 * This is the listings Field API, it contains all api calls for creating and retrieving listing field data.
 *
 */
class FieldsApi extends BaseCommand
{
    private array $field_types = ['text', 'textarea', 'select', 'select-multiple', 'option', 'checkbox', 'divider', 'price', 'url', 'email', 'number', 'decimal', 'date', 'lat', 'long'];
    private array $search_type = ['ptext', 'optionlist', 'optionlist_or', 'fcheckbox', 'fcheckbox_or', 'fpulldown', 'select', 'select_or', 'pulldown', 'checkbox', 'checkbox_or', 'option', 'minmax', 'daterange', 'singledate', 'null_checkbox', 'notnull_checkbox'];
    /*
    * 0 = 'All Visitors';
    *   $lang['display_priv_1'] = 'Members and Agents';
    *   $lang['display_priv_2'] = 'Agents Only';
    *   3 = Admin Only
    */
    private array $display_priv = [0, 1, 2, 3];

    /**
     * Get field metadata(information)
     *
     * @param array{
     *              resource: string,
     *              class?:integer[],
     *              field_id?: integer,
     *              searchable_only?: boolean,
     *              browseable_only?: boolean
     *          } $data expects an array containing the following array keys.
     *
     *  <ul>
     *      <li>$data['resource'] - Text - What resource do you want to get fields for. Allowed Options are
     *      "listing".</li>
     *      <li>$data['class'] - Array - Optional array of property class IDs. Only fields assigned to these IDs will
     *      be returned.</li>
     *      <li>$data['field_id'] - Integer - Optional Field ID. If set only the metadata for this field is
     *      returned..</li>
     *      <li>$data['searchable_only'] - Boolean - Optional - If Set only searchable fields are returned.</li>
     *  </ul>
     *
     * @return array{
     *     fields: array<int,array{
     *     default_text?: string,
     *     display_on_browse?: string,
     *     display_priv?: int,
     *     field_caption?: string,
     *     field_elements?: non-empty-list<string>,
     *     field_id: int,
     *     field_length?: int,
     *     field_name?: string,
     *     field_type?: string,
     *     location?: string,
     *     rank?: int,
     *     required?: string,
     *     search_label?:string,
     *     search_rank?: int,
     *     search_result_rank?: int,
     *     search_step?: string,
     *     search_type?: string,
     *     searchable?: bool,
     *     tool_tip?: string
     *      }>
     *     }
     * @throws \Exception
     */
    public function metadata(array $data): array
    {
        $login = new Login($this->dbh, $this->config);
        //Check that required settings were passed

        if (!isset($data['resource']) || $data['resource'] != 'listing' && $data['resource'] != 'agent' && $data['resource'] != 'member' && $data['resource'] != 'feedback') {
            throw new Exception('resource: correct_parameter_not_passed');
        }
        if (!isset($data['searchable_only'])) {
            $data['searchable_only'] = false;
        }
        if (!isset($data['browseable_only'])) {
            $data['browseable_only'] = false;
        }

        $fields = [];

        $dbParamaters = [];
        if ($data['resource'] == 'listing') {
            $sql = 'SELECT  * FROM ' . $this->config['table_prefix'] . 'listingsformelements';
            $sql_where = [];
            $order_by = 'ORDER BY listingsformelements_rank';
            if (isset($data['field_id'])) {
                $dbParamaters[':field_id'] = $data['field_id'];
                $sql_where[] = 'listingsformelements_id = :field_id';
            }
            if (!empty($data['class'])) {
                $sql .= ' as lfe LEFT JOIN ' . $this->config['table_prefix_no_lang'] . 'classformelements  as cfe
					ON lfe.listingsformelements_id = cfe.listingsformelements_id';

                $preparedIds = array_map(function ($v) {
                    return ":class_id$v";
                }, $data['class']);

                $dbParamaters = array_merge(array_combine($preparedIds, $data['class']), $dbParamaters);
                $sql_where[] = 'class_id IN (' . implode(',', $preparedIds) . ')';
            }
            if ($data['searchable_only']) {
                $sql_where[] = 'listingsformelements_searchable = 1';
                $order_by = 'ORDER BY listingsformelements_search_rank';
            }
            if ($data['browseable_only']) {
                $sql_where[] = 'listingsformelements_display_on_browse = \'Yes\'';
                $order_by = 'ORDER BY listingsformelements_search_result_rank';
            }
            //Show only fields that are visible to the user
            $display_status = $login->verifyPriv('Admin');
            if (!$display_status) {
                $display_status = $login->verifyPriv('Agent');
                if ($display_status) {
                    $sql_where[] = '(listingsformelements_display_priv = 0 OR listingsformelements_display_priv = 1 OR listingsformelements_display_priv = 2)';
                } else {
                    $display_status = $login->verifyPriv('Member');
                    if ($display_status) {
                        $sql_where[] = '(listingsformelements_display_priv = 0 OR listingsformelements_display_priv = 1)';
                    } else {
                        $sql_where[] = 'listingsformelements_display_priv = 0';
                    }
                }
            } else {
                $sql_where[] = '(listingsformelements_display_priv = 0 OR listingsformelements_display_priv = 1 OR listingsformelements_display_priv = 2 OR listingsformelements_display_priv = 3)';
            }
            $sql .= ' WHERE ' . implode(' AND ', $sql_where) . ' ' . $order_by;


            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->execute($dbParamaters);
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api-->fields->metadata', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }

            $recordSet = $stmt->fetchAll();

            foreach ($recordSet as $row) {
                $listingsformelements_id = (int)$row['listingsformelements_id'];
                $fields[$listingsformelements_id]['field_id'] = $listingsformelements_id;
                $fields[$listingsformelements_id]['field_type'] = (string)$row['listingsformelements_field_type'];
                $fields[$listingsformelements_id]['field_name'] = (string)$row['listingsformelements_field_name'];
                $fields[$listingsformelements_id]['field_caption'] = (string)$row['listingsformelements_field_caption'];
                $fields[$listingsformelements_id]['default_text'] = (string)$row['listingsformelements_default_text'];
                $fields[$listingsformelements_id]['field_elements'] = explode('||', (string)$row['listingsformelements_field_elements']);
                $fields[$listingsformelements_id]['rank'] = (int)$row['listingsformelements_rank'];
                $fields[$listingsformelements_id]['search_rank'] = (int)$row['listingsformelements_search_rank'];
                $fields[$listingsformelements_id]['search_result_rank'] = (int)$row['listingsformelements_search_result_rank'];
                $fields[$listingsformelements_id]['required'] = (string)$row['listingsformelements_required'];
                $fields[$listingsformelements_id]['location'] = (string)$row['listingsformelements_location'];
                $fields[$listingsformelements_id]['display_on_browse'] = (string)$row['listingsformelements_display_on_browse'];
                $fields[$listingsformelements_id]['searchable'] = (bool)$row['listingsformelements_searchable'];
                $fields[$listingsformelements_id]['search_type'] = (string)$row['listingsformelements_search_type'];
                $fields[$listingsformelements_id]['search_label'] = (string)$row['listingsformelements_search_label'];
                $fields[$listingsformelements_id]['search_step'] = (string)$row['listingsformelements_search_step'];
                $fields[$listingsformelements_id]['display_priv'] = (int)$row['listingsformelements_display_priv'];
                $fields[$listingsformelements_id]['field_length'] = (int)$row['listingsformelements_field_length'];
                $fields[$listingsformelements_id]['tool_tip'] = (string)$row['listingsformelements_tool_tip'];
            }
        }
        if ($data['resource'] == 'agent') {
            $sql = 'SELECT  * FROM ' . $this->config['table_prefix'] . 'agentformelements';
            $sql_where = [];
            $order_by = 'ORDER BY agentformelements_rank';
            if (isset($data['field_id'])) {
                $dbParamaters[':field_id'] = $data['field_id'];
                $sql_where[] = 'agentformelements_id = :field_id';
            }

            //Show only fields that are visible to the user
            $display_status = $login->verifyPriv('Admin');
            if (!$display_status) {
                $display_status = $login->verifyPriv('Agent');
                if ($display_status) {
                    $sql_where[] = '(agentformelements_display_priv = 0 OR agentformelements_display_priv = 1 OR agentformelements_display_priv = 2)';
                } else {
                    $display_status = $login->verifyPriv('Member');
                    if ($display_status) {
                        $sql_where[] = '(agentformelements_display_priv = 0 OR agentformelements_display_priv = 1)';
                    } else {
                        $sql_where[] = 'agentformelements_display_priv = 0';
                    }
                }
            } else {
                $sql_where[] = '(agentformelements_display_priv = 0 OR agentformelements_display_priv = 1 OR agentformelements_display_priv = 2 OR agentformelements_display_priv = 3)';
            }
            $sql .= ' WHERE ' . implode(' AND ', $sql_where) . ' ' . $order_by;

            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->execute($dbParamaters);
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $recordSet = $stmt->fetchAll();
            $fields = [];
            foreach ($recordSet as $row) {
                $agentformelements_id = (int)$row['agentformelements_id'];
                $fields[$agentformelements_id]['field_id'] = $agentformelements_id;
                $fields[$agentformelements_id]['field_type'] = (string)$row['agentformelements_field_type'];
                $fields[$agentformelements_id]['field_name'] = (string)$row['agentformelements_field_name'];
                $fields[$agentformelements_id]['field_caption'] = (string)$row['agentformelements_field_caption'];
                $fields[$agentformelements_id]['default_text'] = (string)$row['agentformelements_default_text'];
                $fields[$agentformelements_id]['field_elements'] = explode('||', (string)$row['agentformelements_field_elements']);
                $fields[$agentformelements_id]['rank'] = (int)$row['agentformelements_rank'];
                $fields[$agentformelements_id]['required'] = (string)$row['agentformelements_required'];
                $fields[$agentformelements_id]['display_priv'] = (int)$row['agentformelements_display_priv'];
                $fields[$agentformelements_id]['tool_tip'] = (string)$row['agentformelements_tool_tip'];
            }
        }
        if ($data['resource'] == 'member') {
            $sql = 'SELECT  * FROM ' . $this->config['table_prefix'] . 'memberformelements ';
            $sql_where = [];
            $order_by = 'ORDER BY memberformelements_rank';

            if (isset($data['field_id'])) {
                $dbParamaters[':field_id'] = $data['field_id'];
                $sql_where[] = 'memberformelements_id = :field_id';
            }

            if (!empty($sql_where)) {
                $sql .= ' WHERE ' . implode(' AND ', $sql_where) . ' ' . $order_by;
            } else {
                $sql .= $order_by;
            }

            //$sql .= ' WHERE '.implode(' AND ',$sql_where).' '.$order_by;

            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->execute($dbParamaters);
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api-->fields->metadata', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }

            $recordSet = $stmt->fetchAll();
            $fields = [];
            foreach ($recordSet as $row) {
                $memberformelements_id = (int)$row['memberformelements_id'];
                $fields[$memberformelements_id]['field_id'] = $memberformelements_id;
                $fields[$memberformelements_id]['field_type'] = (string)$row['memberformelements_field_type'];
                $fields[$memberformelements_id]['field_name'] = (string)$row['memberformelements_field_name'];
                $fields[$memberformelements_id]['field_caption'] = (string)$row['memberformelements_field_caption'];
                $fields[$memberformelements_id]['default_text'] = (string)$row['memberformelements_default_text'];
                $fields[$memberformelements_id]['field_elements'] = explode('||', $row['memberformelements_field_elements']);
                $fields[$memberformelements_id]['rank'] = (int)$row['memberformelements_rank'];
                $fields[$memberformelements_id]['required'] = (string)$row['memberformelements_required'];
                $fields[$memberformelements_id]['tool_tip'] = (string)$row['memberformelements_tool_tip'];
            }
        }
        if ($data['resource'] == 'feedback') {
            $sql = 'SELECT  * FROM ' . $this->config['table_prefix'] . 'feedbackformelements ';
            $sql_where = [];
            $order_by = 'ORDER BY feedbackformelements_rank';
            if (isset($data['field_id'])) {
                $dbParamaters[':field_id'] = $data['field_id'];
                $sql_where[] = 'feedbackformelements_id = :field_id';
            }

            $sql .= ' WHERE ' . implode(' AND ', $sql_where) . ' ' . $order_by;

            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->execute($dbParamaters);
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $recordSet = $stmt->fetchAll();
            $fields = [];
            foreach ($recordSet as $row) {
                $feedbackformelements_id = (int)$row['feedbackformelements_id'];
                $fields[$feedbackformelements_id]['field_id'] = $feedbackformelements_id;
                $fields[$feedbackformelements_id]['field_type'] = (string)$row['feedbackformelements_field_type'];
                $fields[$feedbackformelements_id]['field_name'] = (string)$row['feedbackformelements_field_name'];
                $fields[$feedbackformelements_id]['field_caption'] = (string)$row['feedbackformelements_field_caption'];
                $fields[$feedbackformelements_id]['default_text'] = (string)$row['feedbackformelements_default_text'];
                $fields[$feedbackformelements_id]['field_elements'] = explode('||', $row['feedbackformelements_field_elements']);
                $fields[$feedbackformelements_id]['rank'] = (int)$row['feedbackformelements_rank'];
                $fields[$feedbackformelements_id]['required'] = (string)$row['feedbackformelements_required'];
                $fields[$feedbackformelements_id]['location'] = (string)$row['feedbackformelements_location'];
                $fields[$feedbackformelements_id]['tool_tip'] = (string)$row['feedbackformelements_tool_tip'];
            }
        }

        return ['fields' => $fields];
    }

    /**
     * @param array $data {
     *                    field_name: string,
     *                    field_type: string,
     *                    pclass: int[]
     *                    }
     * @return array {
     *                    field_values: string[],
     *                    field_counts: array<string,int>
     *                    }
     * @throws \Exception
     */
    public function values(array $data): array
    {
        //TODO Add ability to limit fields to member only fields
        //$login = new Login($this->dbh, $this->config);
        //Check that required settings were passed
        if (!isset($data['field_name'])) {
            throw new Exception('field_name: correct_parameter_not_passed');
        }
        if (!isset($data['field_type'])) {
            throw new Exception('field_type: correct_parameter_not_passed');
        }
        if (isset($data['pclass']) && !is_array($data['pclass'])) {
            throw new Exception('pclass: correct_parameter_not_passed');
        }
        $class_sql = '';
        $class_sql_array = [];
        $dbParameters = [];
        if (!is_null($data['pclass'])) {
            foreach ($data['pclass'] as $class_id) {
                if ($class_id > 0) {
                    $dbParameters[':class_id'] = $class_id;
                    $class_sql_array[] = $this->config['table_prefix'] . 'listingsdb.listingsdb_pclass_id = :class_id';
                }
            }
        }
        if (!empty($class_sql_array)) {
            $class_sql .= ' AND (' . implode(' OR ', $class_sql_array) . ')';
        }
        $dbParameters[':field_name'] = $data['field_name'];
        $sortby = match ($data['field_type']) {
            'decimal' => 'ORDER BY listingsdbelements_field_value+0 ASC',
            'number' => 'ORDER BY CAST(listingsdbelements_field_value as signed) ASC',
            default => 'ORDER BY listingsdbelements_field_value ASC',
        };

        if ($this->config['configured_show_count']) {
            $sql = 'SELECT listingsdbelements_field_value, count(listingsdbelements_field_value)
					AS num_type
					FROM ' . $this->config['table_prefix'] . 'listingsdbelements, ' . $this->config['table_prefix'] . 'listingsdb
					WHERE listingsdbelements_field_name = :field_name
					AND listingsdb_active = \'yes\' AND listingsdbelements_field_value <> \'\'
					AND ' . $this->config['table_prefix'] . 'listingsdbelements.listingsdb_id = ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id ' . $class_sql;
        } else {
            $sql = 'SELECT listingsdbelements_field_value
					FROM ' . $this->config['table_prefix'] . 'listingsdbelements, ' . $this->config['table_prefix'] . 'listingsdb
					WHERE listingsdbelements_field_name = :field_name
					AND listingsdb_active = \'yes\' AND listingsdbelements_field_value <> \'\'
					AND ' . $this->config['table_prefix'] . 'listingsdbelements.listingsdb_id = ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id ' . $class_sql;
        }

        if ($this->config['use_expiration']) {
            $dbParameters[':expiration'] = date("Y-m-d H:i:s", time());
            $sql .= ' AND listingsdb_expiration > :expiration';
        }

        $sql .= ' GROUP BY ' . $this->config['table_prefix'] . 'listingsdbelements.listingsdbelements_field_value ' . $sortby;
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->execute($dbParameters);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        //echo $sql;
        $field_values = [];
        $field_counts = [];
        $recordSet = $stmt->fetchAll();
        foreach ($recordSet as $record) {
            $field_values[] = (string)$record['listingsdbelements_field_value'];
            if ($this->config['configured_show_count']) {
                $field_counts[(string)$record['listingsdbelements_field_value']] = (int)$record['num_type'];
            }
        }
        return ['field_values' => $field_values, 'field_counts' => $field_counts];
    }

    /**
     * API command to insert a listing field into the class.
     *
     * @param array{ resource: string,
     *               class: int[],
     *                field_type: string,
     *               field_name: string,
     *               field_caption: string,
     *               default_text: string,
     *               field_elements: string[],
     *     rank: integer,
     *     search_rank: integer,
     *     search_result_rank: integer,
     *     required: boolean,
     *     location: string,
     *     display_on_browse: boolean,
     *     search_step:string,
     *     display_priv: integer,
     *     field_length: integer,
     *     tool_tip: string,
     *     search_label: string,
     *     search_type: string,
     *     searchable: boolean
     *     } $data Expects an array containing the following array keys.
     *
     *  <ul>
     *      <li>$data['resource'] - Text - What type of resource are you creating a field for. Allowed Options are
     *      "listing".</li>
     *      <li>$data['class'] - Array - Array of property class ID that you want to insert an listing field into.</li>
     *      <li>$data['field_type'] - Text - Type of field to create. Valid options are
     *      'text','textarea','select','select-multiple','option','checkbox','divider','price','url','email','number','decimal','date','lat','long'
     *      </li>
     *      <li>$data['field_name'] - Text - Name of field to create</li>
     *      <li>$data['field_caption'] - Text - Caption to display for field.</li>
     *      <li>$data['field_elements'] - Array - Array of options if this is a select type of field.</li>
     *      <li>$data['rank'] - Numberic - Order of Field.</li>
     *      <li>$data['search_rank'] - Numberic - Order of Field on Search pages.</li>
     *      <li>$data['search_result_rank'] - Numberic - Order of field on search results page.</li>
     *      <li>$data['required'] - Boolean - Field is Reuired TRUE/FALSE</li>
     *      <li>$data['location'] - Text - Template Location to display field at.</li>
     *      <li>$data['display_on_browse'] - Boolean - Should this field be displayed on the search results page?.</li>
     *      <li>$data['search_step'] - Value to step search values by for min/max fields</li>
     *      <li>$data['display_priv'] - Numberic - 0 = Show Field to All Visitors, 1 = members &amp; Agents, 2 = Agents
     *      Only, 3 = Admin Only</li>
     *      <li>data['field_length'] - Numberic - Maximum Length of Field</li>
     *      <li>$data['tool_tip'] - Text -Tooltip to show</li>
     *      <li>$data['search_label'] - Text - Label to show user when searching this field</li>
     *      <li>$data['search_type'] - Text - Allowed Values
     *      'ptext','optionlist','optionlist_or','fcheckbox','fcheckbox_or','fpulldown','select','select_or','pulldown','checkbox','checkbox_or','option','minmax','daterange','singledate','null_checkbox','notnull_checkbox'</li>
     *      <li>$data[$searchable'] - Boolean - Is the field searchable?</li>
     *  </ul>
     *
     * @return array{field_id: int}
     * @throws Exception
     */

    public function create(array $data): array
    {
        $login = new Login($this->dbh, $this->config);
        $login_status = $login->verifyPriv('Agent');
        if ($login_status !== true) {
            throw new Exception('Login Failure');
        }
        //Check that required settings were passed

        if (!isset($data['resource']) || $data['resource'] != 'listing') {
            throw new Exception('resource: correct_parameter_not_passed');
        }
        if (!isset($data['class'])) {
            throw new Exception('class: correct_parameter_not_passed');
        }
        if (!isset($data['field_type']) || !in_array($data['field_type'], $this->field_types)) {
            throw new Exception('field_type: correct_parameter_not_passed');
        }
        if (!isset($data['field_name']) || trim($data['field_name']) == '') {
            throw new Exception('field_name: correct_parameter_not_passed');
        }
        if (!isset($data['field_caption'])) {
            throw new Exception('field_caption: correct_parameter_not_passed');
        }
        if (!isset($data['default_text'])) {
            throw new Exception('default_text: correct_parameter_not_passed');
        }
        if (!isset($data['field_elements'])) {
            throw new Exception('field_elements: correct_parameter_not_passed');
        }
        if (!isset($data['rank'])) {
            throw new Exception('rank: correct_parameter_not_passed');
        }
        if (!isset($data['search_rank'])) {
            throw new Exception('search_rank: correct_parameter_not_passed');
        }
        if (!isset($data['search_result_rank'])) {
            throw new Exception('search_result_rank: correct_parameter_not_passed');
        }
        if (!isset($data['required'])) {
            throw new Exception('required: correct_parameter_not_passed');
        }

        if (!isset($data['location'])) {
            throw new Exception('location: correct_parameter_not_passed');
        }
        if (!isset($data['display_on_browse'])) {
            throw new Exception('display_on_browse: correct_parameter_not_passed');
        }
        if (!isset($data['search_step'])) {
            throw new Exception('search_step: correct_parameter_not_passed');
        }
        if (!isset($data['display_priv']) || !in_array($data['display_priv'], $this->display_priv)) {
            throw new Exception('display_priv: correct_parameter_not_passed');
        }
        if (!isset($data['field_length'])) {
            throw new Exception('field_length: correct_parameter_not_passed');
        }
        if (!isset($data['tool_tip'])) {
            throw new Exception('tool_tip: correct_parameter_not_passed');
        }
        if (!isset($data['search_label'])) {
            throw new Exception('search_label: correct_parameter_not_passed');
        }
        if (!isset($data['search_type']) || ($data['search_type'] != '' && !in_array($data['search_type'], $this->search_type))) {
            throw new Exception('search_type: correct_parameter_not_passed');
        }
        if (!isset($data['searchable'])) {
            throw new Exception('searchable: correct_parameter_not_passed');
        }

        $security = $login->verifyPriv('edit_listing_template');
        if ($security !== true) {
            throw new Exception('permission_denied');
        }

        $field_name = preg_replace("/[^A-Za-z0-9_]/", '', $data['field_name']);

        //Check for duplicate field names
        $sql = 'SELECT listingsformelements_field_name FROM ' . $this->config['table_prefix'] . 'listingsformelements 
                WHERE listingsformelements_field_name = :field_name';
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->execute([':field_name' => $field_name]);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $recordSet = $stmt->fetchAll();
        if (count($recordSet) > 0) {
            throw new Exception('Field Already Exists: ' . $field_name);
        }
        $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "listingsformelements
			(listingsformelements_field_type, listingsformelements_field_name, listingsformelements_field_caption, listingsformelements_default_text,
			listingsformelements_field_elements, listingsformelements_rank, listingsformelements_search_rank, listingsformelements_search_result_rank,
			listingsformelements_required, listingsformelements_location, listingsformelements_display_on_browse, listingsformelements_search_step,
			listingsformelements_searchable, listingsformelements_search_label, listingsformelements_search_type,listingsformelements_display_priv,
			listingsformelements_field_length, listingsformelements_tool_tip) VALUES
			(:field_type,:field_name,:field_caption,:default_text,:field_elements,:rank,:search_rank,:search_result_rank,:required,
			:location,:display_on_browse,:search_step,:searchable,:search_label,:search_type,:display_priv, :field_length, :tool_tip)";
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->bindParam(':field_type', $data['field_type']);
            $stmt->bindParam(':field_name', $field_name);
            $stmt->bindParam(':field_caption', $data['field_caption']);
            $stmt->bindParam(':default_text', $data['default_text']);
            $field_elements = implode('||', $data['field_elements']);
            $stmt->bindParam(':field_elements', $field_elements);
            $stmt->bindParam(':rank', $data['rank'], PDO::PARAM_INT);
            $stmt->bindParam(':search_rank', $data['search_rank'], PDO::PARAM_INT);
            $stmt->bindParam(':search_result_rank', $data['search_result_rank'], PDO::PARAM_INT);


            $required = 'No';
            if ($data['required']) {
                $required = 'Yes';
            }
            $stmt->bindParam(':required', $required);
            $stmt->bindParam(':location', $data['location']);
            $display_on_browse = 'No';
            if ($data['display_on_browse']) {
                $display_on_browse = 'Yes';
            }
            $stmt->bindParam(':display_on_browse', $display_on_browse);
            $stmt->bindParam(':searchable', $data['searchable'], PDO::PARAM_INT);
            $stmt->bindParam(':tool_tip', $data['tool_tip']);
            $stmt->bindParam(':search_step', $data['search_step']);
            $stmt->bindParam(':search_type', $data['search_type']);
            $stmt->bindParam(':search_label', $data['search_label']);
            $stmt->bindParam(':display_priv', $data['display_priv'], PDO::PARAM_INT);
            $stmt->bindParam(':field_length', $data['field_length'], PDO::PARAM_INT);

            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }

        $listingsformelements_id = $this->dbh->lastInsertId();
        if (is_bool($listingsformelements_id)) {
            throw new Exception('Failed Getting Last Insert ID');
        }

        // Add Listing Field to property class

        foreach ($data['class'] as $class_id) {
            $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . 'classformelements
								(class_id,listingsformelements_id)
								VALUES (:class_id,:listingsformelements_id)';
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->bindParam('class_id', $class_id, PDO::PARAM_INT);
            $stmt->bindParam('listingsformelements_id', $listingsformelements_id, PDO::PARAM_INT);
            try {
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $this->addFieldsToClass($class_id, $field_name);
        }

        return ['field_id' => (int)$listingsformelements_id];
    }

    /**
     * @param int    $class
     * @param string $field_name
     * @return void
     * @throws \Exception
     */
    private function addFieldsToClass(int $class, string $field_name): void
    {
        $login = new Login($this->dbh, $this->config);
        $login_status = $login->verifyPriv('Agent');
        if ($login_status !== true) {
            throw new Exception('Login Failure');
        }
        //Get List of Listings in Class that already contain the field.
        $sql = 'SELECT listingsdb_id FROM ' . $this->config['table_prefix'] . 'listingsdbelements 
        WHERE listingsdbelements_field_name = :field_name 
        AND listingsdb_id IN (SELECT  listingsdb_id FROM ' . $this->config['table_prefix'] . 'listingsdb WHERE listingsdb_pclass_id = :class_id)';

        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->execute([':class_id' => $class, ':field_name' => $field_name]);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->fields->addFieldsToClass', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $skip_listings = [];
        $recordSet = $stmt->fetchAll();
        foreach ($recordSet as $record) {
            $skip_listings[] = $record['listingsdb_id'];
        }
        //Get All Listings in Class
        $sql = 'SELECT listingsdb_id FROM ' . $this->config['table_prefix'] . 'listingsdb WHERE listingsdb_pclass_id = :class_id';
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->execute([':class_id' => $class]);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->fields->addFieldsToClass', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $all_listings = [];
        $recordSet = $stmt->fetchAll();
        foreach ($recordSet as $record) {
            $all_listings[] = $record['listingsdb_id'];
        }
        $do_listings = array_diff($all_listings, $skip_listings);

        $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'listingsdbelements
					(listingsdbelements_field_name, listingsdb_id,userdb_id,listingsdbelements_field_value)
					VALUES (:field_name,:listing_id,:user_id,\'\')';
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        foreach ($do_listings as $listing_id) {
            try {
                $stmt->execute([':field_name' => $field_name, ':user_id' => $_SESSION['userID'], ':listing_id' => $listing_id]);
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->fields->addFieldsToClass', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
        }
    }

    /**
     * Simple API Command to add an existing field to a new property class.
     *
     * @param array{
     *                  class: int,
     *                  field_id: int
     *             } $data Expects an array containing the following array keys.
     *                     <ul>
     *                     <li>$data['class'] - Number - Class ID that we are assigned field to</li>
     *                     <li>$data['field_id'] - Number - Field ID to assign</li>
     *                     </ul>
     *
     * @return array{field_id: int, class_id: int}
     * @throws Exception
     */
    public function assignClass(array $data): array
    {
        $login = new Login($this->dbh, $this->config);
        $login_status = $login->verifyPriv('Agent');
        if ($login_status !== true) {
            throw new Exception('Login Failure');
        }
        if (!isset($data['class'])) {
            throw new Exception('class: correct_parameter_not_passed');
        }
        if (!isset($data['field_id'])) {
            throw new Exception('field_id: correct_parameter_not_passed');
        }


        //Make sure this is a valid field
        $sql = 'SELECT listingsformelements_field_name FROM ' . $this->config['table_prefix'] . 'listingsformelements WHERE listingsformelements_id = :field_id';
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->execute([':field_id' => $data['field_id']]);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->fields->assignClass', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $recordSet = $stmt->fetchAll();
        if (count($recordSet) !== 1) {
            throw new Exception('Invalid Field');
        }
        $field_name = (string)$recordSet[0]['listingsformelements_field_name'];
        //Make sure this is a valid class
        $sql = 'SELECT class_id FROM ' . $this->config['table_prefix'] . 'class WHERE class_id =:class_id';
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->execute([':class_id' => $data['class']]);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->fields->assignClass', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $recordSet = $stmt->fetchAll();
        if (count($recordSet) !== 1) {
            throw new Exception('Invalid Class');
        }

        //Make sure Field is not in property class
        $sql = 'SELECT class_id FROM ' . $this->config['table_prefix_no_lang'] . 'classformelements WHERE class_id = :class_id AND listingsformelements_id = :field_id';
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->execute([':class_id' => $data['class'], ':field_id' => $data['field_id']]);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->fields->assignClass', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $recordSet = $stmt->fetchAll();
        if (count($recordSet) !== 1) {
            $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . 'classformelements
								(class_id,listingsformelements_id)
								VALUES (:class_id,:field_id)';
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->execute([':class_id' => $data['class'], ':field_id' => $data['field_id']]);
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->fields->assignClass', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
        }
        //Make sure field exists on all listings in class.
        $this->addFieldsToClass($data['class'], $field_name);
        return ['field_id' => $data['field_id'], 'class_id' => $data['class']];
    }
}
