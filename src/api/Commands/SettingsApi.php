<?php

declare(strict_types=1);

namespace OpenRealty\Api\Commands;

use PDO;

class SettingsApi
{
    private PDO $dbh;

    public function __construct(PDO $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param array{
     *             is_mobile: boolean,
     *  } $data
     *
     * @return array{
     *     config: array{
     *          table_prefix_no_lang: string,
     *          version: string,
     *          basepath: string,
     *          baseurl: string,
     *          admin_name: string,
     *          admin_email: string,
     *          site_email: string,
     *          company_name: string,
     *          company_location: string,
     *          company_logo: string,
     *          automatic_update_check: boolean,
     *          url_style: integer,
     *          seo_default_keywords: string,
     *          seo_default_description: string,
     *          seo_listing_keywords: string,
     *          seo_listing_description: string,
     *          seo_default_title: string,
     *          seo_listing_title: string,
     *          seo_url_seperator: string,
     *          template: string,
     *          full_template: string,
     *          admin_template: string,
     *          listing_template: string,
     *          agent_template: string,
     *          template_listing_sections: string,
     *          search_result_template: string,
     *          vtour_template: string,
     *          lang: string,
     *          listings_per_page: integer,
     *          search_step_max: integer,
     *          max_search_results: integer,
     *          add_linefeeds: boolean,
     *          strip_html: boolean,
     *          allowed_html_tags: string,
     *          money_sign: string,
     *          show_no_photo: boolean,
     *          number_format_style: integer,
     *          number_decimals_number_fields: integer,
     *          number_decimals_price_fields: integer,
     *          money_format: integer,
     *          date_format: integer,
     *          date_to_timestamp: '%m/%d/%Y'|'%Y/%d/%m'|'%d/%m/%Y',
     *          date_format_long: 'mm/dd/yyyy'|'yyyy/dd/mm'|'dd/mm/yyyy',
     *          date_format_timestamp: 'm/d/Y'|'Y/d/m'|'d/m/Y',
     *          max_listings_uploads: integer,
     *          max_listings_upload_size: integer,
     *          max_listings_upload_width: integer,
     *          max_listings_upload_height: integer,
     *          max_user_uploads: integer,
     *          max_user_upload_size: integer,
     *          max_user_upload_width: integer,
     *          max_user_upload_height: integer,
     *          max_vtour_uploads: integer,
     *          max_vtour_upload_size: integer,
     *          max_vtour_upload_width: integer,
     *          allowed_upload_extensions: string,
     *          make_thumbnail: boolean,
     *          thumbnail_width: integer,
     *          thumbnail_prog: string,
     *          path_to_imagemagick: string,
     *          resize_img: boolean,
     *          jpeg_quality: integer,
     *          use_expiration: boolean,
     *          days_until_listings_expire: integer,
     *          allow_member_signup: boolean,
     *          allow_agent_signup: boolean,
     *          agent_default_active: boolean,
     *          agent_default_admin: boolean,
     *          agent_default_feature: boolean,
     *          agent_default_moderate: boolean,
     *          agent_default_logview: boolean,
     *          agent_default_edit_site_config: boolean,
     *          agent_default_edit_member_template: boolean,
     *          agent_default_edit_agent_template: boolean,
     *          agent_default_edit_listing_template: boolean,
     *          agent_default_canchangeexpirations: boolean,
     *          agent_default_editpages: boolean,
     *          agent_default_havevtours: boolean,
     *          agent_default_num_listings: integer,
     *          agent_default_num_featuredlistings: integer,
     *          agent_default_can_export_listings: boolean,
     *          agent_default_edit_all_users: boolean,
     *          agent_default_edit_all_listings: boolean,
     *          agent_default_edit_property_classes: boolean,
     *          moderate_agents: boolean,
     *          moderate_members: boolean,
     *          moderate_listings: boolean,
     *          export_listings: boolean,
     *          email_notification_of_new_users: boolean,
     *          email_information_to_new_users: boolean,
     *          email_notification_of_new_listings: boolean,
     *          configured_langs: string,
     *          configured_show_count: boolean,
     *          sortby: string,
     *          sorttype: string,
     *          special_sortby: string,
     *          special_sorttype: string,
     *          email_users_notification_of_new_listings: boolean,
     *          num_featured_listings: integer,
     *          map_type: string,
     *          map_address: string,
     *          map_address2: string,
     *          map_address3: string,
     *          map_address4: string,
     *          map_city: string,
     *          map_state: string,
     *          map_zip: string,
     *          map_country: string,
     *          map_latitude: string,
     *          map_longitude: string,
     *          show_listedby_admin: boolean,
     *          show_next_prev_listing_page: boolean,
     *          show_notes_field: boolean,
     *          vtour_width: integer,
     *          vtour_height: integer,
     *          vt_popup_width: integer,
     *          vt_popup_height: integer,
     *          zero_price: boolean,
     *          vcard_phone: string,
     *          vcard_fax: string,
     *          vcard_mobile: string,
     *          vcard_address: string,
     *          vcard_city: string,
     *          vcard_state: string,
     *          vcard_zip: string,
     *          vcard_country: string,
     *          vcard_url: string,
     *          vcard_notes: string,
     *          demo_mode: boolean,
     *          feature_list_separator: string,
     *          search_list_separator: string,
     *          rss_title_featured: string,
     *           rss_desc_featured: string,
     *           rss_listingdesc_featured: string,
     *           rss_title_lastmodified: string,
     *           rss_desc_lastmodified: string,
     *           rss_listingdesc_lastmodified: string,
     *           rss_limit_lastmodified: integer,
     *           rss_title_latestlisting: string,
     *           rss_desc_latestlisting: string,
     *           rss_listingdesc_latestlisting: string,
     *           rss_limit_latestlisting: integer,
     *           resize_by: string,
     *           resize_thumb_by: string,
     *           thumbnail_height: integer,
     *           charset: string,
     *           wysiwyg_show_edit: boolean,
     *           textarea_short_chars: integer,
     *           main_image_display_by: string,
     *           main_image_width: integer,
     *           main_image_height: integer,
     *           number_columns: integer,
     *           rss_limit_featured: integer,
     *           force_decimals: boolean,
     *           max_listings_file_uploads: integer,
     *           max_listings_file_upload_size: integer,
     *           allowed_file_upload_extensions: string,
     *           file_icon_width: integer,
     *           file_icon_height: integer,
     *           show_file_icon: boolean,
     *           file_display_option: string,
     *           file_display_size: boolean,
     *           include_senders_ip: boolean,
     *           agent_default_havefiles: boolean,
     *           agent_default_haveuserfiles: boolean,
     *           max_users_file_uploads: integer,
     *           max_users_file_upload_size: integer,
     *           disable_referrer_check: boolean,
     *           price_field: string,
     *           users_per_page: integer,
     *           show_admin_on_agent_list: boolean,
     *           use_signup_image_verification: boolean,
     *           controlpanel_mbstring_enabled: boolean,
     *           require_email_verification: boolean,
     *           blog_requires_moderation: boolean,
     *           maintenance_mode: boolean,
     *           notify_listings_template: string,
     *           twitter_consumer_secret: string,
     *           twitter_consumer_key: string,
     *           twitter_auth: string,
     *           twitter_new_listings: boolean,
     *           twitter_update_listings: boolean,
     *           twitter_new_blog: boolean,
     *           twitter_listing_photo: boolean,
     *           pingback_services: string,
     *           blogs_per_page: integer,
     *           send_url_pingbacks: boolean,
     *           send_service_pingbacks: boolean,
     *           timezone: string,
     *           default_page: string,
     *           blog_pingback_urls: string,
     *           banned_domains_signup: string,
     *           banned_ips_signup: string,
     *           banned_ips_site: string,
     *           rss_title_blogposts: string,
     *           rss_desc_blogposts: string,
     *           rss_title_blogcomments: string,
     *           rss_desc_blogcomments: string,
     *           phpmailer: boolean,
     *           mailserver: string,
     *           mailport: integer,
     *           mailuser: string,
     *           mailpass: string,
     *           agent_default_canManageAddons: boolean,
     *           agent_default_blogUserType: integer,
     *           agent_default_edit_all_leads: boolean,
     *           agent_default_edit_lead_template: boolean,
     *           enable_tracking: boolean,
     *           enable_tracking_crawlers: boolean,
     *           show_agent_no_photo: boolean,
     *           template_lead_sections: string,
     *           allow_template_change: boolean,
     *           allow_language_change: boolean,
     *           listingimages_slideshow_group_thumb: integer,
     *           admin_listing_per_page: integer,
     *           mobile_template: string,
     *           captcha_system: string,
     *           floor_agent: integer,
     *           contact_template: string,
     *           user_jpeg_quality: integer,
     *           user_resize_img: boolean,
     *           user_resize_by: string,
     *           user_resize_thumb_by: string,
     *           user_thumbnail_width: integer,
     *           user_thumbnail_height: integer,
     *           num_popular_listings: integer,
     *           num_random_listings: integer,
     *           num_latest_listings: integer,
     *           google_client_id: string,
     *           google_client_secret: string,
     *           lang_table_prefix: string,
     *           table_prefix: string,
     *           path_to_thumbnailer: string,
     *           template_path: string,
     *           template_url: string,
     *           admin_template_path: string,
     *           admin_template_url: string,
     *           listings_upload_path: string,
     *           listings_view_images_path: string,
     *           user_upload_path: string,
     *           user_view_images_path: string,
     *           vtour_upload_path: string,
     *           vtour_view_images_path: string,
     *           file_icons_path: string,
     *           listings_view_file_icons_path: string,
     *           listings_file_upload_path: string,
     *           listings_view_file_path: string,
     *           users_file_upload_path: string,
     *           users_view_file_path: string,
     *           allow_pingbacks: boolean,
     *           uri_parts: array<
     *              string, array{
     *                  'slug': string,
     *                  'uri': string
     *                  }
     *           >
     *          }
     *     }
     */
    public function read(array $data): array
    {
        if (!array_key_exists('is_mobile', $data)) {
            $data['is_mobile'] = false;
        }
        /** We need to read the table prefix from our common.ini file */
        $settings = parse_ini_file(dirname(__file__, 2) . '/common.ini.php', false, INI_SCANNER_TYPED);
        if (is_bool($settings)) {
            die('Failed to Parse common.ini.php');
        }

        $config = [];

        $config["table_prefix_no_lang"] = (string)$settings['table_prefix'];

        $dbValues = $this->dbh->query("SELECT * FROM " . $config["table_prefix_no_lang"] . "controlpanel")->fetch();

        try {
            $dbValuesRss = $this->dbh->query("SELECT * FROM " . $config["table_prefix_no_lang"] . "controlpanel_rss")->fetch();
        } catch (\PDOException $e) {
            if ($e->getCode() == '42S02') {
                $dbValuesRss = [];
            } else {
                throw $e;
            }
        }

        try {
            $dbValuesSeo = $this->dbh->query("SELECT * FROM " . $config["table_prefix_no_lang"] . "controlpanel_seo")->fetch();
        } catch (\PDOException $e) {
            if ($e->getCode() == '42S02') {
                $dbValuesSeo = [];
            } else {
                throw $e;
            }
        }
        try {
            $dbValuesTemplate = $this->dbh->query("SELECT * FROM " . $config["table_prefix_no_lang"] . "controlpanel_template")->fetch();
        } catch (\PDOException $e) {
            if ($e->getCode() == '42S02') {
                $dbValuesTemplate = [];
            } else {
                throw $e;
            }
        }

        // Loop throught Control Panel and save to Array
        $config["version"] = isset($dbValues["controlpanel_version"]) ? (string)$dbValues["controlpanel_version"] : '';
        $config["basepath"] = isset($dbValues["controlpanel_basepath"]) ? (string)$dbValues["controlpanel_basepath"] : '';
        $config["baseurl"] = isset($dbValues["controlpanel_baseurl"]) ? (string)$dbValues["controlpanel_baseurl"] : '';
        $config["admin_name"] = isset($dbValues["controlpanel_admin_name"]) ? (string)$dbValues["controlpanel_admin_name"] : '';
        $config["admin_email"] = isset($dbValues["controlpanel_admin_email"]) ? (string)$dbValues["controlpanel_admin_email"] : '';
        $config["site_email"] = isset($dbValues["controlpanel_site_email"]) ? (string)$dbValues["controlpanel_site_email"] : '';
        $config["company_name"] = isset($dbValues["controlpanel_company_name"]) ? (string)$dbValues["controlpanel_company_name"] : '';
        $config["company_location"] = isset($dbValues["controlpanel_company_location"]) ? (string)$dbValues["controlpanel_company_location"] : '';
        $config["company_logo"] = isset($dbValues["controlpanel_company_logo"]) ? (string)$dbValues["controlpanel_company_logo"] : '';
        $config["automatic_update_check"] = isset($dbValues["controlpanel_automatic_update_check"]) ? (bool)$dbValues["controlpanel_automatic_update_check"] : false;
        $config["url_style"] = isset($dbValues["controlpanel_url_style"]) ? (int)$dbValues["controlpanel_url_style"] : 0;
        $config["seo_default_keywords"] = isset($dbValuesSeo["controlpanel_seo_default_keywords"]) ? (string)$dbValuesSeo["controlpanel_seo_default_keywords"] : '';
        $config["seo_default_description"] = isset($dbValuesSeo["controlpanel_seo_default_description"]) ? (string)$dbValuesSeo["controlpanel_seo_default_description"] : '';
        $config["seo_listing_keywords"] = isset($dbValuesSeo["controlpanel_seo_listing_keywords"]) ? (string)$dbValuesSeo["controlpanel_seo_listing_keywords"] : '';
        $config["seo_listing_description"] = isset($dbValuesSeo["controlpanel_seo_listing_description"]) ? (string)$dbValuesSeo["controlpanel_seo_listing_description"] : '';
        $config["seo_default_title"] = isset($dbValuesSeo["controlpanel_seo_default_title"]) ? (string)$dbValuesSeo["controlpanel_seo_default_title"] : '';
        $config["seo_listing_title"] = isset($dbValuesSeo["controlpanel_seo_listing_title"]) ? (string)$dbValuesSeo["controlpanel_seo_listing_title"] : '';
        $config["seo_url_seperator"] = isset($dbValuesSeo["controlpanel_seo_url_seperator"]) ? (string)$dbValuesSeo["controlpanel_seo_url_seperator"] : '';
        if (isset($_SESSION["template"]) && is_string($_SESSION["template"])) {
            $config["template"] = $_SESSION["template"];
        } else {
            $config["template"] = isset($dbValuesTemplate["controlpanel_template_frontend"]) ? (string)$dbValuesTemplate["controlpanel_template_frontend"] : '';
        }
        $config["full_template"] = isset($dbValuesTemplate["controlpanel_template_frontend"]) ? (string)$dbValuesTemplate["controlpanel_template_frontend"] : '';
        $config["admin_template"] = isset($dbValuesTemplate["controlpanel_template_admin_frontend"]) ? (string)$dbValuesTemplate["controlpanel_template_admin_frontend"] : '';
        $config["listing_template"] = isset($dbValuesTemplate["controlpanel_template_listing"]) ? (string)$dbValuesTemplate["controlpanel_template_listing"] : '';
        $date_format = null;
        $date_format_timestamp = null;
        $date_to_timestamp = null;
        $config["agent_template"] = isset($dbValuesTemplate["controlpanel_template_agent"]) ? (string)$dbValuesTemplate["controlpanel_template_agent"] : '';
        $config["template_listing_sections"] = isset($dbValuesTemplate["controlpanel_template_listing_sections"]) ? (string)$dbValuesTemplate["controlpanel_template_listing_sections"] : '';
        $config["search_result_template"] = isset($dbValuesTemplate["controlpanel_template_search_result"]) ? (string)$dbValuesTemplate["controlpanel_template_search_result"] : '';
        $config["vtour_template"] = isset($dbValuesTemplate["controlpanel_template_vtour"]) ? (string)$dbValuesTemplate["controlpanel_template_vtour"] : '';
        $config["lang"] = isset($dbValues["controlpanel_lang"]) ? (string)$dbValues["controlpanel_lang"] : '';
        $config["listings_per_page"] = isset($dbValues["controlpanel_listings_per_page"]) ? (int)$dbValues["controlpanel_listings_per_page"] : 0;
        $config["search_step_max"] = isset($dbValues["controlpanel_search_step_max"]) ? (int)$dbValues["controlpanel_search_step_max"] : 0;
        $config["max_search_results"] = isset($dbValues["controlpanel_max_search_results"]) ? (int)$dbValues["controlpanel_max_search_results"] : 0;
        $config["add_linefeeds"] = isset($dbValues["controlpanel_add_linefeeds"]) ? (bool)$dbValues["controlpanel_add_linefeeds"] : false;
        $config["strip_html"] = isset($dbValues["controlpanel_strip_html"]) ? (bool)$dbValues["controlpanel_strip_html"] : false;
        $config["allowed_html_tags"] = isset($dbValues["controlpanel_allowed_html_tags"]) ? (string)$dbValues["controlpanel_allowed_html_tags"] : '';
        $config["money_sign"] = isset($dbValues["controlpanel_money_sign"]) ? (string)$dbValues["controlpanel_money_sign"] : '';
        $config["show_no_photo"] = isset($dbValues["controlpanel_show_no_photo"]) ? (bool)$dbValues["controlpanel_show_no_photo"] : false;
        $config["number_format_style"] = isset($dbValues["controlpanel_number_format_style"]) ? (int)$dbValues["controlpanel_number_format_style"] : 0;
        $config["number_decimals_number_fields"] = isset($dbValues["controlpanel_number_decimals_number_fields"]) ? (int)$dbValues["controlpanel_number_decimals_number_fields"] : 0;
        $config["number_decimals_price_fields"] = isset($dbValues["controlpanel_number_decimals_price_fields"]) ? (int)$dbValues["controlpanel_number_decimals_price_fields"] : 0;
        $config["money_format"] = isset($dbValues["controlpanel_money_format"]) ? (int)$dbValues["controlpanel_money_format"] : 0;
        $config["date_format"] = isset($dbValues["controlpanel_date_format"]) ? (int)$dbValues["controlpanel_date_format"] : 0;
        $date_format[1] = "mm/dd/yyyy";
        $date_format[2] = "yyyy/dd/mm";
        $date_format[3] = "dd/mm/yyyy";
        $date_format_timestamp[1] = "m/d/Y";
        $date_format_timestamp[2] = "Y/d/m";
        $date_format_timestamp[3] = "d/m/Y";
        $date_to_timestamp[1] = "%m/%d/%Y";
        $date_to_timestamp[2] = "%Y/%d/%m";
        $date_to_timestamp[3] = "%d/%m/%Y";
        /** @var "%m/%d/%Y" | "%Y/%d/%m" | "%d/%m/%Y" */
        $config["date_to_timestamp"] = $date_to_timestamp[$config["date_format"]];
        /** @var "mm/dd/yyyy" | "yyyy/dd/mm" | "dd/mm/yyyy" */
        $config["date_format_long"] = $date_format[$config["date_format"]];
        /** @var "m/d/Y" | "Y/d/m" | "d/m/Y" */
        $config["date_format_timestamp"] = $date_format_timestamp[$config["date_format"]];
        $config["max_listings_uploads"] = isset($dbValues["controlpanel_max_listings_uploads"]) ? (int)$dbValues["controlpanel_max_listings_uploads"] : 0;
        $config["max_listings_upload_size"] = isset($dbValues["controlpanel_max_listings_upload_size"]) ? (int)$dbValues["controlpanel_max_listings_upload_size"] : 0;
        $config["max_listings_upload_width"] = isset($dbValues["controlpanel_max_listings_upload_width"]) ? (int)$dbValues["controlpanel_max_listings_upload_width"] : 0;
        $config["max_listings_upload_height"] = isset($dbValues["controlpanel_max_listings_upload_height"]) ? (int)$dbValues["controlpanel_max_listings_upload_height"] : 0;
        $config["max_user_uploads"] = isset($dbValues["controlpanel_max_user_uploads"]) ? (int)$dbValues["controlpanel_max_user_uploads"] : 0;
        $config["max_user_upload_size"] = isset($dbValues["controlpanel_max_user_upload_size"]) ? (int)$dbValues["controlpanel_max_user_upload_size"] : 0;
        $config["max_user_upload_width"] = isset($dbValues["controlpanel_max_user_upload_width"]) ? (int)$dbValues["controlpanel_max_user_upload_width"] : 0;
        $config["max_user_upload_height"] = isset($dbValues["controlpanel_max_user_upload_height"]) ? (int)$dbValues["controlpanel_max_user_upload_height"] : 0;
        $config["max_vtour_uploads"] = isset($dbValues["controlpanel_max_vtour_uploads"]) ? (int)$dbValues["controlpanel_max_vtour_uploads"] : 0;
        $config["max_vtour_upload_size"] = isset($dbValues["controlpanel_max_vtour_upload_size"]) ? (int)$dbValues["controlpanel_max_vtour_upload_size"] : 0;
        $config["max_vtour_upload_width"] = isset($dbValues["controlpanel_max_vtour_upload_width"]) ? (int)$dbValues["controlpanel_max_vtour_upload_width"] : 0;
        $config["allowed_upload_extensions"] = isset($dbValues["controlpanel_allowed_upload_extensions"]) ? (string)$dbValues["controlpanel_allowed_upload_extensions"] : '';
        $config["make_thumbnail"] = isset($dbValues["controlpanel_make_thumbnail"]) ? (bool)$dbValues["controlpanel_make_thumbnail"] : false;
        $config["thumbnail_width"] = isset($dbValues["controlpanel_thumbnail_width"]) ? (int)$dbValues["controlpanel_thumbnail_width"] : 0;
        $config["thumbnail_prog"] = isset($dbValues["controlpanel_thumbnail_prog"]) ? (string)$dbValues["controlpanel_thumbnail_prog"] : '';
        $config["path_to_imagemagick"] = isset($dbValues["controlpanel_path_to_imagemagick"]) ? (string)$dbValues["controlpanel_path_to_imagemagick"] : '';
        $config["resize_img"] = isset($dbValues["controlpanel_resize_img"]) ? (bool)$dbValues["controlpanel_resize_img"] : false;
        $config["jpeg_quality"] = isset($dbValues["controlpanel_jpeg_quality"]) ? (int)$dbValues["controlpanel_jpeg_quality"] : 0;
        $config["use_expiration"] = isset($dbValues["controlpanel_use_expiration"]) ? (bool)$dbValues["controlpanel_use_expiration"] : false;
        $config["days_until_listings_expire"] = isset($dbValues["controlpanel_days_until_listings_expire"]) ? (int)$dbValues["controlpanel_days_until_listings_expire"] : 0;
        $config["allow_member_signup"] = isset($dbValues["controlpanel_allow_member_signup"]) ? (bool)$dbValues["controlpanel_allow_member_signup"] : false;
        $config["allow_agent_signup"] = isset($dbValues["controlpanel_allow_agent_signup"]) ? (bool)$dbValues["controlpanel_allow_agent_signup"] : false;
        $config["agent_default_active"] = isset($dbValues["controlpanel_agent_default_active"]) ? (bool)$dbValues["controlpanel_agent_default_active"] : false;
        $config["agent_default_admin"] = isset($dbValues["controlpanel_agent_default_admin"]) ? (bool)$dbValues["controlpanel_agent_default_admin"] : false;
        $config["agent_default_feature"] = isset($dbValues["controlpanel_agent_default_feature"]) ? (bool)$dbValues["controlpanel_agent_default_feature"] : false;
        $config["agent_default_moderate"] = isset($dbValues["controlpanel_agent_default_moderate"]) ? (bool)$dbValues["controlpanel_agent_default_moderate"] : false;
        $config["agent_default_logview"] = isset($dbValues["controlpanel_agent_default_logview"]) ? (bool)$dbValues["controlpanel_agent_default_logview"] : false;
        $config["agent_default_edit_site_config"] = isset($dbValues["controlpanel_agent_default_edit_site_config"]) ? (bool)$dbValues["controlpanel_agent_default_edit_site_config"] : false;
        $config["agent_default_edit_member_template"] = isset($dbValues["controlpanel_agent_default_edit_member_template"]) ? (bool)$dbValues["controlpanel_agent_default_edit_member_template"] : false;
        $config["agent_default_edit_agent_template"] = isset($dbValues["controlpanel_agent_default_edit_agent_template"]) ? (bool)$dbValues["controlpanel_agent_default_edit_agent_template"] : false;
        $config["agent_default_edit_listing_template"] = isset($dbValues["controlpanel_agent_default_edit_listing_template"]) ? (bool)$dbValues["controlpanel_agent_default_edit_listing_template"] : false;
        $config["agent_default_canchangeexpirations"] = isset($dbValues["controlpanel_agent_default_canchangeexpirations"]) ? (bool)$dbValues["controlpanel_agent_default_canchangeexpirations"] : false;
        $config["agent_default_editpages"] = isset($dbValues["controlpanel_agent_default_editpages"]) ? (bool)$dbValues["controlpanel_agent_default_editpages"] : false;
        $config["agent_default_havevtours"] = isset($dbValues["controlpanel_agent_default_havevtours"]) ? (bool)$dbValues["controlpanel_agent_default_havevtours"] : false;
        $config["agent_default_num_listings"] = isset($dbValues["controlpanel_agent_default_num_listings"]) ? (int)$dbValues["controlpanel_agent_default_num_listings"] : 0;
        $config["agent_default_num_featuredlistings"] = isset($dbValues["controlpanel_agent_default_num_featuredlistings"]) ? (int)$dbValues["controlpanel_agent_default_num_featuredlistings"] : 0;
        $config["agent_default_can_export_listings"] = isset($dbValues["controlpanel_agent_default_can_export_listings"]) ? (bool)$dbValues["controlpanel_agent_default_can_export_listings"] : false;
        $config["agent_default_edit_all_users"] = isset($dbValues["controlpanel_agent_default_edit_all_users"]) ? (bool)$dbValues["controlpanel_agent_default_edit_all_users"] : false;
        $config["agent_default_edit_all_listings"] = isset($dbValues["controlpanel_agent_default_edit_all_listings"]) ? (bool)$dbValues["controlpanel_agent_default_edit_all_listings"] : false;
        $config["agent_default_edit_property_classes"] = isset($dbValues["controlpanel_agent_default_edit_property_classes"]) ? (bool)$dbValues["controlpanel_agent_default_edit_property_classes"] : false;
        $config["moderate_agents"] = isset($dbValues["controlpanel_moderate_agents"]) ? (bool)$dbValues["controlpanel_moderate_agents"] : false;
        $config["moderate_members"] = isset($dbValues["controlpanel_moderate_members"]) ? (bool)$dbValues["controlpanel_moderate_members"] : false;
        $config["moderate_listings"] = isset($dbValues["controlpanel_moderate_listings"]) ? (bool)$dbValues["controlpanel_moderate_listings"] : false;
        $config["export_listings"] = isset($dbValues["controlpanel_export_listings"]) ? (bool)$dbValues["controlpanel_export_listings"] : false;
        $config["email_notification_of_new_users"] = isset($dbValues["controlpanel_email_notification_of_new_users"]) ? (bool)$dbValues["controlpanel_email_notification_of_new_users"] : false;
        $config["email_information_to_new_users"] = isset($dbValues["controlpanel_email_information_to_new_users"]) ? (bool)$dbValues["controlpanel_email_information_to_new_users"] : false;
        $config["email_notification_of_new_listings"] = isset($dbValues["controlpanel_email_notification_of_new_listings"]) ? (bool)$dbValues["controlpanel_email_notification_of_new_listings"] : false;
        $config["configured_langs"] = isset($dbValues["controlpanel_configured_langs"]) ? (string)$dbValues["controlpanel_configured_langs"] : '';
        $config["configured_show_count"] = isset($dbValues["controlpanel_configured_show_count"]) ? (bool)$dbValues["controlpanel_configured_show_count"] : false;
        $config["sortby"] = isset($dbValues["controlpanel_search_sortby"]) ? (string)$dbValues["controlpanel_search_sortby"] : '';
        $config["sorttype"] = isset($dbValues["controlpanel_search_sorttype"]) ? (string)$dbValues["controlpanel_search_sorttype"] : '';
        $config["special_sortby"] = isset($dbValues["controlpanel_special_search_sortby"]) ? (string)$dbValues["controlpanel_special_search_sortby"] : '';
        $config["special_sorttype"] = isset($dbValues["controlpanel_special_search_sorttype"]) ? (string)$dbValues["controlpanel_special_search_sorttype"] : '';
        $config["email_users_notification_of_new_listings"] = isset($dbValues["controlpanel_email_users_notification_of_new_listings"]) ? (bool)$dbValues["controlpanel_email_users_notification_of_new_listings"] : false;
        $config["num_featured_listings"] = isset($dbValues["controlpanel_num_featured_listings"]) ? (int)$dbValues["controlpanel_num_featured_listings"] : 0;
        $config["map_type"] = isset($dbValues["controlpanel_map_type"]) ? (string)$dbValues["controlpanel_map_type"] : '';
        $config["map_address"] = isset($dbValues["controlpanel_map_address"]) ? (string)$dbValues["controlpanel_map_address"] : '';
        $config["map_address2"] = isset($dbValues["controlpanel_map_address2"]) ? (string)$dbValues["controlpanel_map_address2"] : '';
        $config["map_address3"] = isset($dbValues["controlpanel_map_address3"]) ? (string)$dbValues["controlpanel_map_address3"] : '';
        $config["map_address4"] = isset($dbValues["controlpanel_map_address4"]) ? (string)$dbValues["controlpanel_map_address4"] : '';
        $config["map_city"] = isset($dbValues["controlpanel_map_city"]) ? (string)$dbValues["controlpanel_map_city"] : '';
        $config["map_state"] = isset($dbValues["controlpanel_map_state"]) ? (string)$dbValues["controlpanel_map_state"] : '';
        $config["map_zip"] = isset($dbValues["controlpanel_map_zip"]) ? (string)$dbValues["controlpanel_map_zip"] : '';
        $config["map_country"] = isset($dbValues["controlpanel_map_country"]) ? (string)$dbValues["controlpanel_map_country"] : '';
        $config["map_latitude"] = isset($dbValues["controlpanel_map_latitude"]) ? (string)$dbValues["controlpanel_map_latitude"] : '';
        $config["map_longitude"] = isset($dbValues["controlpanel_map_longitude"]) ? (string)$dbValues["controlpanel_map_longitude"] : '';
        $config["show_listedby_admin"] = isset($dbValues["controlpanel_show_listedby_admin"]) ? (bool)$dbValues["controlpanel_show_listedby_admin"] : false;
        $config["show_next_prev_listing_page"] = isset($dbValues["controlpanel_show_next_prev_listing_page"]) ? (bool)$dbValues["controlpanel_show_next_prev_listing_page"] : false;
        $config["show_notes_field"] = isset($dbValues["controlpanel_show_notes_field"]) ? (bool)$dbValues["controlpanel_show_notes_field"] : false;
        $config["vtour_width"] = isset($dbValues["controlpanel_vtour_width"]) ? (int)$dbValues["controlpanel_vtour_width"] : 0;
        $config["vtour_height"] = isset($dbValues["controlpanel_vtour_height"]) ? (int)$dbValues["controlpanel_vtour_height"] : 0;
        $config["vt_popup_width"] = isset($dbValues["controlpanel_vt_popup_width"]) ? (int)$dbValues["controlpanel_vt_popup_width"] : 0;
        $config["vt_popup_height"] = isset($dbValues["controlpanel_vt_popup_height"]) ? (int)$dbValues["controlpanel_vt_popup_height"] : 0;
        $config["zero_price"] = isset($dbValues["controlpanel_zero_price"]) ? (bool)$dbValues["controlpanel_zero_price"] : false;
        $config["vcard_phone"] = isset($dbValues["controlpanel_vcard_phone"]) ? (string)$dbValues["controlpanel_vcard_phone"] : '';
        $config["vcard_fax"] = isset($dbValues["controlpanel_vcard_fax"]) ? (string)$dbValues["controlpanel_vcard_fax"] : '';
        $config["vcard_mobile"] = isset($dbValues["controlpanel_vcard_mobile"]) ? (string)$dbValues["controlpanel_vcard_mobile"] : '';
        $config["vcard_address"] = isset($dbValues["controlpanel_vcard_address"]) ? (string)$dbValues["controlpanel_vcard_address"] : '';
        $config["vcard_city"] = isset($dbValues["controlpanel_vcard_city"]) ? (string)$dbValues["controlpanel_vcard_city"] : '';
        $config["vcard_state"] = isset($dbValues["controlpanel_vcard_state"]) ? (string)$dbValues["controlpanel_vcard_state"] : '';
        $config["vcard_zip"] = isset($dbValues["controlpanel_vcard_zip"]) ? (string)$dbValues["controlpanel_vcard_zip"] : '';
        $config["vcard_country"] = isset($dbValues["controlpanel_vcard_country"]) ? (string)$dbValues["controlpanel_vcard_country"] : '';
        $config["vcard_url"] = isset($dbValues["controlpanel_vcard_url"]) ? (string)$dbValues["controlpanel_vcard_url"] : '';
        $config["vcard_notes"] = isset($dbValues["controlpanel_vcard_notes"]) ? (string)$dbValues["controlpanel_vcard_notes"] : '';
        $config["demo_mode"] = isset($dbValues["controlpanel_demo_mode"]) ? (bool)$dbValues["controlpanel_demo_mode"] : false;
        $config["feature_list_separator"] = isset($dbValues["controlpanel_feature_list_separator"]) ? (string)$dbValues["controlpanel_feature_list_separator"] : '';
        $config["search_list_separator"] = isset($dbValues["controlpanel_search_list_separator"]) ? (string)$dbValues["controlpanel_search_list_separator"] : '';
        $config["rss_title_featured"] = isset($dbValuesRss["controlpanel_rss_title_featured"]) ? (string)$dbValuesRss["controlpanel_rss_title_featured"] : '';
        $config["rss_desc_featured"] = isset($dbValuesRss["controlpanel_rss_desc_featured"]) ? (string)$dbValuesRss["controlpanel_rss_desc_featured"] : '';
        $config["rss_listingdesc_featured"] = isset($dbValuesRss["controlpanel_rss_listingdesc_featured"]) ? (string)$dbValuesRss["controlpanel_rss_listingdesc_featured"] : '';
        $config["rss_title_lastmodified"] = isset($dbValuesRss["controlpanel_rss_title_lastmodified"]) ? (string)$dbValuesRss["controlpanel_rss_title_lastmodified"] : '';
        $config["rss_desc_lastmodified"] = isset($dbValuesRss["controlpanel_rss_desc_lastmodified"]) ? (string)$dbValuesRss["controlpanel_rss_desc_lastmodified"] : '';
        $config["rss_listingdesc_lastmodified"] = isset($dbValuesRss["controlpanel_rss_listingdesc_lastmodified"]) ? (string)$dbValuesRss["controlpanel_rss_listingdesc_lastmodified"] : '';
        $config["rss_limit_lastmodified"] = isset($dbValuesRss["controlpanel_rss_limit_lastmodified"]) ? (int)$dbValuesRss["controlpanel_rss_limit_lastmodified"] : 0;
        $config["rss_title_latestlisting"] = isset($dbValuesRss["controlpanel_rss_title_latestlisting"]) ? (string)$dbValuesRss["controlpanel_rss_title_latestlisting"] : '';
        $config["rss_desc_latestlisting"] = isset($dbValuesRss["controlpanel_rss_desc_latestlisting"]) ? (string)$dbValuesRss["controlpanel_rss_desc_latestlisting"] : '';
        $config["rss_listingdesc_latestlisting"] = isset($dbValuesRss["controlpanel_rss_listingdesc_latestlisting"]) ? (string)$dbValuesRss["controlpanel_rss_listingdesc_latestlisting"] : '';
        $config["rss_limit_latestlisting"] = isset($dbValuesRss["controlpanel_rss_limit_latestlisting"]) ? (int)$dbValuesRss["controlpanel_rss_limit_latestlisting"] : 0;
        $config["resize_by"] = isset($dbValues["controlpanel_resize_by"]) ? (string)$dbValues["controlpanel_resize_by"] : '';
        $config["resize_thumb_by"] = isset($dbValues["controlpanel_resize_thumb_by"]) ? (string)$dbValues["controlpanel_resize_thumb_by"] : '';
        $config["thumbnail_height"] = isset($dbValues["controlpanel_thumbnail_height"]) ? (int)$dbValues["controlpanel_thumbnail_height"] : 0;
        $config["charset"] = isset($dbValues["controlpanel_charset"]) ? (string)$dbValues["controlpanel_charset"] : '';
        $config["wysiwyg_show_edit"] = isset($dbValues["controlpanel_wysiwyg_show_edit"]) ? (bool)$dbValues["controlpanel_wysiwyg_show_edit"] : false;
        $config["textarea_short_chars"] = isset($dbValues["controlpanel_textarea_short_chars"]) ? (int)$dbValues["controlpanel_textarea_short_chars"] : 0;
        $config["main_image_display_by"] = isset($dbValues["controlpanel_main_image_display_by"]) ? (string)$dbValues["controlpanel_main_image_display_by"] : '';
        $config["main_image_width"] = isset($dbValues["controlpanel_main_image_width"]) ? (int)$dbValues["controlpanel_main_image_width"] : 0;
        $config["main_image_height"] = isset($dbValues["controlpanel_main_image_height"]) ? (int)$dbValues["controlpanel_main_image_height"] : 0;
        $config["number_columns"] = isset($dbValues["controlpanel_number_columns"]) ? (int)$dbValues["controlpanel_number_columns"] : 0;
        $config["rss_limit_featured"] = isset($dbValuesRss["controlpanel_rss_limit_featured"]) ? (int)$dbValuesRss["controlpanel_rss_limit_featured"] : 0;
        $config["force_decimals"] = isset($dbValues["controlpanel_force_decimals"]) ? (bool)$dbValues["controlpanel_force_decimals"] : false;
        $config["max_listings_file_uploads"] = isset($dbValues["controlpanel_max_listings_file_uploads"]) ? (int)$dbValues["controlpanel_max_listings_file_uploads"] : 0;
        $config["max_listings_file_upload_size"] = isset($dbValues["controlpanel_max_listings_file_upload_size"]) ? (int)$dbValues["controlpanel_max_listings_file_upload_size"] : 0;
        $config["allowed_file_upload_extensions"] = isset($dbValues["controlpanel_allowed_file_upload_extensions"]) ? (string)$dbValues["controlpanel_allowed_file_upload_extensions"] : '';
        $config["file_icon_width"] = isset($dbValues["controlpanel_icon_image_width"]) ? (int)$dbValues["controlpanel_icon_image_width"] : 0;
        $config["file_icon_height"] = isset($dbValues["controlpanel_icon_image_height"]) ? (int)$dbValues["controlpanel_icon_image_height"] : 0;
        $config["show_file_icon"] = isset($dbValues["controlpanel_show_file_icon"]) ? (bool)$dbValues["controlpanel_show_file_icon"] : false;
        $config["file_display_option"] = isset($dbValues["controlpanel_file_display_option"]) ? (string)$dbValues["controlpanel_file_display_option"] : '';
        $config["file_display_size"] = isset($dbValues["controlpanel_show_file_size"]) ? (bool)$dbValues["controlpanel_show_file_size"] : false;
        $config["include_senders_ip"] = isset($dbValues["controlpanel_include_senders_ip"]) ? (bool)$dbValues["controlpanel_include_senders_ip"] : false;
        $config["agent_default_havefiles"] = isset($dbValues["controlpanel_agent_default_havefiles"]) ? (bool)$dbValues["controlpanel_agent_default_havefiles"] : false;
        $config["agent_default_haveuserfiles"] = isset($dbValues["controlpanel_agent_default_haveuserfiles"]) ? (bool)$dbValues["controlpanel_agent_default_haveuserfiles"] : false;
        $config["max_users_file_uploads"] = isset($dbValues["controlpanel_max_users_file_uploads"]) ? (int)$dbValues["controlpanel_max_users_file_uploads"] : 0;
        $config["max_users_file_upload_size"] = isset($dbValues["controlpanel_max_users_file_upload_size"]) ? (int)$dbValues["controlpanel_max_users_file_upload_size"] : 0;
        $config["disable_referrer_check"] = isset($dbValues["controlpanel_disable_referrer_check"]) ? (bool)$dbValues["controlpanel_disable_referrer_check"] : false;
        $config["price_field"] = isset($dbValues["controlpanel_price_field"]) ? (string)$dbValues["controlpanel_price_field"] : '';
        $config["users_per_page"] = isset($dbValues["controlpanel_users_per_page"]) ? (int)$dbValues["controlpanel_users_per_page"] : 0;
        $config["show_admin_on_agent_list"] = isset($dbValues["controlpanel_show_admin_on_agent_list"]) ? (bool)$dbValues["controlpanel_show_admin_on_agent_list"] : false;
        $config["use_signup_image_verification"] = isset($dbValues["controlpanel_use_signup_image_verification"]) ? (bool)$dbValues["controlpanel_use_signup_image_verification"] : false;
        $config["controlpanel_mbstring_enabled"] = isset($dbValues["controlpanel_mbstring_enabled"]) ? (bool)$dbValues["controlpanel_mbstring_enabled"] : false;
        $config["require_email_verification"] = isset($dbValues["controlpanel_require_email_verification"]) ? (bool)$dbValues["controlpanel_require_email_verification"] : false;
        $config["blog_requires_moderation"] = isset($dbValues["controlpanel_blog_requires_moderation"]) ? (bool)$dbValues["controlpanel_blog_requires_moderation"] : false;
        $config["maintenance_mode"] = isset($dbValues["controlpanel_maintenance_mode"]) ? (bool)$dbValues["controlpanel_maintenance_mode"] : false;
        $config["notify_listings_template"] = isset($dbValuesTemplate["controlpanel_template_notify_listings"]) ? (string)$dbValuesTemplate["controlpanel_template_notify_listings"] : '';
        $config["twitter_consumer_secret"] = isset($dbValues["controlpanel_twitter_consumer_secret"]) ? (string)$dbValues["controlpanel_twitter_consumer_secret"] : '';
        $config["twitter_consumer_key"] = isset($dbValues["controlpanel_twitter_consumer_key"]) ? (string)$dbValues["controlpanel_twitter_consumer_key"] : '';
        $config["twitter_auth"] = isset($dbValues["controlpanel_twitter_auth"]) ? (string)$dbValues["controlpanel_twitter_auth"] : '';
        $config["twitter_new_listings"] = isset($dbValues["controlpanel_twitter_new_listings"]) ? (bool)$dbValues["controlpanel_twitter_new_listings"] : false;
        $config["twitter_update_listings"] = isset($dbValues["controlpanel_twitter_update_listings"]) ? (bool)$dbValues["controlpanel_twitter_update_listings"] : false;
        $config["twitter_new_blog"] = isset($dbValues["controlpanel_twitter_new_blog"]) ? (bool)$dbValues["controlpanel_twitter_new_blog"] : false;
        $config["twitter_listing_photo"] = isset($dbValues["controlpanel_twitter_listing_photo"]) ? (bool)$dbValues["controlpanel_twitter_listing_photo"] : false;
        $config["pingback_services"] = isset($dbValues["controlpanel_blog_pingback_urls"]) ? (string)$dbValues["controlpanel_blog_pingback_urls"] : '';
        $config["blogs_per_page"] = isset($dbValues["controlpanel_blogs_per_page"]) ? (int)$dbValues["controlpanel_blogs_per_page"] : 0;
        $config["allow_pingbacks"] = isset($dbValues["controlpanel_allow_pingbacks"]) ? (bool)$dbValues["controlpanel_allow_pingbacks"] : false;
        $config["send_url_pingbacks"] = isset($dbValues["controlpanel_send_url_pingbacks"]) ? (bool)$dbValues["controlpanel_send_url_pingbacks"] : false;
        $config["send_service_pingbacks"] = isset($dbValues["controlpanel_send_service_pingbacks"]) ? (bool)$dbValues["controlpanel_send_service_pingbacks"] : false;
        $config["timezone"] = isset($dbValues["controlpanel_timezone"]) ? (string)$dbValues["controlpanel_timezone"] : 'UTC';
        $config["default_page"] = isset($dbValues["controlpanel_default_page"]) ? (string)$dbValues["controlpanel_default_page"] : '';
        $config["blog_pingback_urls"] = isset($dbValues["controlpanel_blog_pingback_urls"]) ? (string)$dbValues["controlpanel_blog_pingback_urls"] : '';
        $config["banned_domains_signup"] = isset($dbValues["controlpanel_banned_domains_signup"]) ? (string)$dbValues["controlpanel_banned_domains_signup"] : '';
        $config["banned_ips_signup"] = isset($dbValues["controlpanel_banned_ips_signup"]) ? (string)$dbValues["controlpanel_banned_ips_signup"] : '';
        $config["banned_ips_site"] = isset($dbValues["controlpanel_banned_ips_site"]) ? (string)$dbValues["controlpanel_banned_ips_site"] : '';
        $config["rss_title_blogposts"] = isset($dbValuesRss["controlpanel_rss_title_blogposts"]) ? (string)$dbValuesRss["controlpanel_rss_title_blogposts"] : '';
        $config["rss_desc_blogposts"] = isset($dbValuesRss["controlpanel_rss_desc_blogposts"]) ? (string)$dbValuesRss["controlpanel_rss_desc_blogposts"] : '';
        $config["rss_title_blogcomments"] = isset($dbValuesRss["controlpanel_rss_title_blogcomments"]) ? (string)$dbValuesRss["controlpanel_rss_title_blogcomments"] : '';
        $config["rss_desc_blogcomments"] = isset($dbValuesRss["controlpanel_rss_desc_blogcomments"]) ? (string)$dbValuesRss["controlpanel_rss_desc_blogcomments"] : '';
        $config["phpmailer"] = isset($dbValues["controlpanel_phpmailer"]) ? (bool)$dbValues["controlpanel_phpmailer"] : false;
        $config["mailserver"] = isset($dbValues["controlpanel_mailserver"]) ? (string)$dbValues["controlpanel_mailserver"] : '';
        $config["mailport"] = isset($dbValues["controlpanel_mailport"]) ? (int)$dbValues["controlpanel_mailport"] : 0;
        $config["mailuser"] = isset($dbValues["controlpanel_mailuser"]) ? (string)$dbValues["controlpanel_mailuser"] : '';
        $config["mailpass"] = isset($dbValues["controlpanel_mailpass"]) ? (string)$dbValues["controlpanel_mailpass"] : '';
        $config["agent_default_canManageAddons"] = isset($dbValues["controlpanel_agent_default_canManageAddons"]) ? (bool)$dbValues["controlpanel_agent_default_canManageAddons"] : false;
        $config["agent_default_blogUserType"] = isset($dbValues["controlpanel_agent_default_blogUserType"]) ? (int)$dbValues["controlpanel_agent_default_blogUserType"] : 0;
        $config["agent_default_edit_all_leads"] = isset($dbValues["controlpanel_agent_default_edit_all_leads"]) ? (bool)$dbValues["controlpanel_agent_default_edit_all_leads"] : false;
        $config["agent_default_edit_lead_template"] = isset($dbValues["controlpanel_agent_default_edit_lead_template"]) ? (bool)$dbValues["controlpanel_agent_default_edit_lead_template"] : false;
        $config["enable_tracking"] = isset($dbValues["controlpanel_enable_tracking"]) ? (bool)$dbValues["controlpanel_enable_tracking"] : false;
        $config["enable_tracking_crawlers"] = isset($dbValues["controlpanel_enable_tracking_crawlers"]) ? (bool)$dbValues["controlpanel_enable_tracking_crawlers"] : false;
        $config["show_agent_no_photo"] = isset($dbValues["controlpanel_show_agent_no_photo"]) ? (bool)$dbValues["controlpanel_show_agent_no_photo"] : false;
        $config["template_lead_sections"] = isset($dbValuesTemplate["controlpanel_template_lead_sections"]) ? (string)$dbValuesTemplate["controlpanel_template_lead_sections"] : '';
        $config["allow_template_change"] = isset($dbValuesTemplate["controlpanel_template_allow_template_change"]) ? (bool)$dbValuesTemplate["controlpanel_template_allow_template_change"] : false;
        $config["allow_language_change"] = isset($dbValues["controlpanel_allow_language_change"]) ? (bool)$dbValues["controlpanel_allow_language_change"] : false;
        $config["listingimages_slideshow_group_thumb"] = isset($dbValues["controlpanel_listingimages_slideshow_group_thumb"]) ? (int)$dbValues["controlpanel_listingimages_slideshow_group_thumb"] : 0;
        $config["admin_listing_per_page"] = isset($dbValues["controlpanel_admin_listing_per_page"]) ? (int)$dbValues["controlpanel_admin_listing_per_page"] : 0;
        $config["mobile_template"] = isset($dbValuesTemplate["controlpanel_template_mobile"]) ? (string)$dbValuesTemplate["controlpanel_template_mobile"] : '';
        $config["captcha_system"] = isset($dbValues["controlpanel_captcha_system"]) ? (string)$dbValues["controlpanel_captcha_system"] : '';
        $config["floor_agent"] = isset($dbValues["controlpanel_floor_agent"]) ? (int)$dbValues["controlpanel_floor_agent"] : 0;
        $config["contact_template"] = isset($dbValuesTemplate["controlpanel_template_contact"]) ? (string)$dbValuesTemplate["controlpanel_template_contact"] : '';
        $config["user_jpeg_quality"] = isset($dbValues["controlpanel_user_jpeg_quality"]) ? (int)$dbValues["controlpanel_user_jpeg_quality"] : 0;
        $config["user_resize_img"] = isset($dbValues["controlpanel_user_resize_img"]) ? (bool)$dbValues["controlpanel_user_resize_img"] : false;
        $config["user_resize_by"] = isset($dbValues["controlpanel_user_resize_by"]) ? (string)$dbValues["controlpanel_user_resize_by"] : '';
        $config["user_resize_thumb_by"] = isset($dbValues["controlpanel_user_resize_thumb_by"]) ? (string)$dbValues["controlpanel_user_resize_thumb_by"] : '';
        $config["user_thumbnail_width"] = isset($dbValues["controlpanel_user_thumbnail_width"]) ? (int)$dbValues["controlpanel_user_thumbnail_width"] : 0;
        $config["user_thumbnail_height"] = isset($dbValues["controlpanel_user_thumbnail_height"]) ? (int)$dbValues["controlpanel_user_thumbnail_height"] : 0;
        $config["num_popular_listings"] = isset($dbValues["controlpanel_num_popular_listings"]) ? (int)$dbValues["controlpanel_num_popular_listings"] : 0;
        $config["num_random_listings"] = isset($dbValues["controlpanel_num_random_listings"]) ? (int)$dbValues["controlpanel_num_random_listings"] : 0;
        $config["num_latest_listings"] = isset($dbValues["controlpanel_num_latest_listings"]) ? (int)$dbValues["controlpanel_num_latest_listings"] : 0;
        $config["google_client_id"] = isset($dbValues["controlpanel_google_client_id"]) ? (string)$dbValues["controlpanel_google_client_id"] : '';
        $config["google_client_secret"] = isset($dbValues["controlpanel_google_client_secret"]) ? (string)$dbValues["controlpanel_google_client_secret"] : '';

        if (!empty($config["timezone"])) {
            date_default_timezone_set($config["timezone"]);
        } else {
            date_default_timezone_set('UTC');
        }
        //Determine which table to use based on language
        $config["table_prefix"] = "default_$config[lang]_";
        if (!isset($_SESSION["users_lang"])) {
            $config["lang_table_prefix"] = "default_$config[lang]_";
        } else {
            $config["lang_table_prefix"] = "default_$_SESSION[users_lang]_";
        }
        ///////////////////////////////////////////////////
        // Path Settings
        // These Paths are set based on setting in the control panel
        $config["path_to_thumbnailer"] = $config["basepath"] . "/include/thumbnail" . $config["thumbnail_prog"] . ".php"; // path to the thumnailing tool
        if ($data['is_mobile'] && !isset($_SESSION["template"])) {
            $config["template"] = $config["mobile_template"];
            $config["template_path"] = $config["basepath"] . "/template/" . $config["mobile_template"]; // leave off the trailing slashes
            $config["template_url"] = $config["baseurl"] . "/template/" . $config["mobile_template"]; // leave off the trailing slashes
        } else {
            $config["template_path"] = $config["basepath"] . "/template/" . $config["template"]; // leave off the trailing slashes
            $config["template_url"] = $config["baseurl"] . "/template/" . $config["template"]; // leave off the trailing slashes
        }
        $config["admin_template_path"] = $config["basepath"] . "/admin/template/" . $config["admin_template"]; // leave off the trailing slashes
        $config["admin_template_url"] = $config["baseurl"] . "/admin/template/" . $config["admin_template"]; // leave off the trailing slashes
        ///////////////////////////////////////////////////
        // MISCELLENEOUS SETTINGS
        // you shouldn"t have to mess with these things unless you rename a folder, etc...
        $config["listings_upload_path"] = $config["basepath"] . "/images/listing_photos";
        $config["listings_view_images_path"] = $config["baseurl"] . "/images/listing_photos";
        $config["user_upload_path"] = $config["basepath"] . "/images/user_photos";
        $config["user_view_images_path"] = $config["baseurl"] . "/images/user_photos";
        $config["vtour_upload_path"] = $config["basepath"] . "/images/vtour_photos";
        $config["vtour_view_images_path"] = $config["baseurl"] . "/images/vtour_photos";
        $config["file_icons_path"] = $config["basepath"] . "/files";
        $config["listings_view_file_icons_path"] = $config["baseurl"] . "/files";
        $config["listings_file_upload_path"] = $config["basepath"] . "/files/listings";
        $config["listings_view_file_path"] = $config["baseurl"] . "/files/listings";
        $config["users_file_upload_path"] = $config["basepath"] . "/files/users";
        $config["users_view_file_path"] = $config["baseurl"] . "/files/users";
        $config["uri_parts"] = [];
        $sth = $this->dbh->prepare("SELECT slug,uri,action FROM " . $config["table_prefix_no_lang"] . "seouri");
        $sth->execute();
        $result = $sth->fetchAll();
        foreach ($result as $data) {
            $config["uri_parts"][(string)$data["action"]]["slug"] = (string)$data["slug"];
            $config["uri_parts"][(string)$data["action"]]["uri"] = (string)$data["uri"];
        }

        return ['config' => $config];
    }
}
