<?php

declare(strict_types=1);

/**
 * This File Contains the Log API Commands
 *
 * @package    Open-Realty
 * @subpackage API
 * @author     Ryan C. Bonham
 * @copyright  2010
 * @link       http://www.open-realty.com Open-Realty
 */

namespace OpenRealty\Api\Commands;

use Exception;
use OpenRealty\Api\Api;
use OpenRealty\Misc;
use PDO;

/**
 * This is the log API, it contains all api calls for creating and retrieving activity log data.
 *
 * @package    Open-Realty
 * @subpackage API
 **/
class LogApi extends BaseCommand
{
    /**
     * This API Command creates an entry in the activity log.
     *
     * @param array{
     *      log_api_command: string,
     *      log_message:string,
     *      } $data Data array of the message to log.
     *
     *  <ul>
     *   <li>$data['log_api_command'] - API Command you are running when the error occured</li>
     *   <li>$data['log_message'] - Log Message to insert</li>
     *  </ul>
     *
     * @returns array{status_msg: 'Log Generated'}
     * @throws Exception
     */
    public function create(array $data): array
    {
        if (!isset($data['log_api_command'])) {
            throw new Exception('correct_parameter_not_passed');
        }
        if (!isset($data['log_message'])) {
            throw new Exception('correct_parameter_not_passed');
        }

        $sql_log_message = $data['log_api_command'] . ' : ' . $data['log_message'];

        if (isset($_SESSION['userID'])) {
            $id = intval($_SESSION['userID']);
        } else {
            $id = 0;
        }

        $sql_remote_addr = $_SERVER['REMOTE_ADDR'] ?? '';

        $stmt = $this->dbh->prepare('INSERT INTO ' . $this->config['table_prefix'] . 'activitylog (activitylog_log_date, userdb_id, activitylog_action, activitylog_ip_address) VALUES (:date, :user_id, :action, :ip)');
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->execute([':date' => date("Y-m-d H:i:s", time()), ':user_id' => $id, ':action' => $sql_log_message, ':ip' => $sql_remote_addr]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        return ['status_msg' => 'Log Generated'];
    }
}
