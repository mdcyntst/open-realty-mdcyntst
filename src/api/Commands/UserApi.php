<?php

declare(strict_types=1);

namespace OpenRealty\Api\Commands;

use Exception;
use OpenRealty\Hooks;
use OpenRealty\Login;
use OpenRealty\Misc;
use PDO;

/**
 * This is the User  API, it contains all api calls for creating and retrieving user data.
 *
 **/
class UserApi extends BaseCommand
{
    //password field removed from list.
    protected array $OR_INT_FIELDS = ['userdb_id', 'userdb_user_name', 'userdb_emailaddress', 'userdb_user_first_name', 'userdb_user_last_name', 'userdb_comments', 'userdb_is_admin', 'userdb_can_edit_site_config', 'userdb_can_edit_member_template', 'userdb_can_edit_agent_template', 'userdb_can_edit_listing_template', 'userdb_creation_date', 'userdb_can_feature_listings', 'userdb_can_view_logs', 'userdb_last_modified', 'userdb_hit_count', 'userdb_can_moderate', 'userdb_can_edit_pages', 'userdb_can_have_vtours', 'userdb_is_agent', 'userdb_active', 'userdb_limit_listings', 'userdb_can_edit_expiration', 'userdb_can_export_listings', 'userdb_can_edit_all_users', 'userdb_can_edit_all_listings', 'userdb_can_edit_property_classes', 'userdb_can_have_files', 'userdb_can_have_user_files', 'userdb_blog_user_type', 'userdb_rank', 'userdb_featuredlistinglimit', 'userdb_email_verified', 'userdb_can_manage_addons', 'userdb_can_edit_all_leads', 'userdb_can_edit_lead_template', 'userdb_send_notifications_to_floor'];

    /**
     * This API Command searches the users
     *
     * @param array{
     *     resource: string,
     *     parameters: array<string, mixed>,
     *     sortby?: string[],
     *     sorttype?: string[],
     *     offset?: integer,
     *     limit?: integer,
     *     count_only?: boolean
     *  } $data $data expects an array containing the following array keys.
     *
     *  <ul>
     *      <li>$data['parameters'] - This is a REQUIRED array of the fields and the values we are searching for.</li>
     *      <li>$data['sortby'] - This is an optional array of fields to sort by.</li>
     *      <li>$data['sorttype'] - This is an optional array of sort types (ASC/DESC) to sort the sortby fields
     *      by.</li>
     *      <li>$data['offset'] - This is an optional integer of the number of users to offset the search by. To use
     *      offset you must set a limit.</li>
     *      <li>$data['limit'] - This is an optional integer of the number of users to limit the search by. 0 or unset
     *      will return all users.</li>
     *      <li>$data['count_only'] - This is an optional integer flag 1/0, where 1 returns a record count only,
     *      defaults to 0 if not set. Usefull if doing limit/offset search for pagenation to get the inital full record
     *      count..</li>
     *  </ul>
     *
     * @return array{
     *     user_count: integer,
     *     users: integer[],
     *     info: array{
     *          process_time: string,
     *          query_time: string,
     *          total_time: string
     *     },
     *      sortby: string[],
     *      sorttype: string[],
     *     limit: integer,
     *     resource: string
     * }  Array retruned will contain the following paramaters.
     *
     *  [user_count] = Number of records found, if using a limit this is only the number of records that match your
     *  current limit/offset results.
     *  [users] = Array of user_ids.
     *  [info] = The info array contains benchmark information on the search, including process_time, query_time, and
     *  total_time
     *  [sortby] = Contains an array of fields that were used to sort the search results. Note if you are doing a
     *  CountONly search the sort is not actually used as this would just slow down the query.
     *  [sorttype] = Contains an array of the sorttype (ASC/DESC) used on the sortby fields.
     *  ['limit'] - INT - The numeric limit being imposed on the results.
     *  ['resource'] - TEXT - The resource the search was made against, 'agent' or 'member'
     *
     * @throws \Exception
     **/
    public function search(array $data): array
    {
        $misc = new Misc($this->dbh, $this->config);
        $start_time = $misc->getmicrotime();
        $login = new Login($this->dbh, $this->config);
        //Check that required settings were passed
        if (!isset($data['resource']) || $data['resource'] != 'agent' && $data['resource'] != 'member') {
            throw new Exception('resource: correct_parameter_not_passed');
        }
        if (!isset($data['parameters'])) {
            throw new Exception('parameters: correct_parameter_not_passed');
        }

        if (!isset($data['limit'])) {
            $data['limit'] = 0;
        }

        if (!isset($data['count_only'])) {
            $data['count_only'] = false;
        }
        $searchresultSQL = '';
        // Set Default Search Options
        $imageonly = false;

        $tablelist = [];

        $login_status = $login->verifyPriv('edit_all_users');
        $string_where_clause = '';
        $string_where_clause_nosort = '';
        if ($login_status !== true || !isset($data['parameters']['userdb_active'])) {
            //If we are not an agent only show active agents, or if user did not specify show only actives by default.
            $data['parameters']['userdb_active'] = 'yes';
        }

        //check to see if the admin is set to be shown on the Agent list
        $display_admin = false;
        if ($data['resource'] == 'agent') {
            $display_admin = $this->config['show_admin_on_agent_list'];
            if ($display_admin) {
                $string_where_clause .= '(' . $this->config['table_prefix'] . 'userdb.userdb_is_agent = \'yes\')';
            } else {
                $string_where_clause .= '(' . $this->config['table_prefix'] . 'userdb.userdb_is_agent = \'yes\' OR ' . $this->config['table_prefix'] . 'userdb.userdb_is_admin = \'yes\')';
            }
        } else {
            $string_where_clause .= '(' . $this->config['table_prefix'] . 'userdb.userdb_is_agent = \'no\' AND ' . $this->config['table_prefix'] . 'userdb.userdb_is_admin = \'no\')';
        }


        $admin_not_agent = false;
        if (isset($_SESSION['userID']) && $_SESSION['userID'] !== '') {
            //check to see if the present user is an Admin but not an Agent
            $is_admin = $misc->getAdminStatus($_SESSION['userID']);
            $is_agent = $misc->getAgentStatus($_SESSION['userID']);
            if ($is_agent === false && $is_admin === true && $_SESSION['userID'] != 1) {
                $admin_not_agent = true;
            }
        }
        //Get Fields

        $fields_api = new FieldsApi($this->dbh, $this->config);
        $field_list = $fields_api->metadata(['resource' => $data['resource']]);

        //echo '<pre>'.print_r($field_list,TRUE).'</pre>';
        //Verify Listing Fields passed via API exist in the class that the listing is being inserted into.
        $allowed_fields_type = [];
        foreach ($field_list['fields'] as $field) {
            $field_id = $field['field_id'];
            $field_name = $field['field_name'] ?? '';
            $field_elements = $field['field_elements'] ?? [];
            $ftype = $field['field_type'] ?? '';
            //Make sure field does not have same name as a core field for saftey
            if (!in_array($field_name, $this->OR_INT_FIELDS) && $field_name != '') {
                $allowed_fields_type[$field_name] = $ftype;
            }
        }

        $dbIntParam = [];
        $dbStrParam = [];
        //Loop through search paramaters
        foreach ($data['parameters'] as $k => $v) {
            if (empty($v)) {
                continue;
            }
            //Search users By Agent
            if ($k == 'userdb_active') {
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                if ($v == 'no') {
                    $string_where_clause .= '(' . $this->config['table_prefix'] . 'userdb.userdb_active = \'no\')';
                    $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'userdb.userdb_active = \'no\')';
                } elseif ($v == 'any') {
                    $string_where_clause .= '(' . $this->config['table_prefix'] . 'userdb.userdb_active = \'yes\' or ' . $this->config['table_prefix'] . 'userdb.userdb_active = \'no\')';
                    $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'userdb.userdb_active = \'yes\' or ' . $this->config['table_prefix'] . 'userdb.userdb_active = \'no\')';
                } else {
                    $string_where_clause .= '(' . $this->config['table_prefix'] . 'userdb.userdb_active = \'yes\')';
                    $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'userdb.userdb_active = \'yes\')';
                }
            } elseif ($k == 'userdb_id') {
                $userdb_id = explode(',', (string)$v);
                $i = 0;
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
                foreach ($userdb_id as $id) {
                    $id = intval($id);
                    $dbIntParam[':userdb_id' . $i] = $id;
                    if ($i == 0) {
                        $searchresultSQL .= '((' . $this->config['table_prefix'] . 'userdb.userdb_id = :userdb_id' . $i . ')';
                    } else {
                        $searchresultSQL .= ' OR (' . $this->config['table_prefix'] . 'userdb.userdb_id = :userdb_id' . $i . ')';
                    }
                    $i++;
                }
                $searchresultSQL .= ')';
            } elseif ($k == 'imagesOnly') {
                // Grab only users with images if that is what we need.
                if ($v == 'yes') {
                    $imageonly = true;
                }
            } elseif ($k == 'userdb_user_name' || $k == 'userdb_emailaddress') {
                $safe_v = '%' . $v . '%';
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                $dbStrParam[':' . $k] = $safe_v;
                $string_where_clause .= '(' . $this->config['table_prefix'] . 'userdb.' . $k . ' LIKE :' . $k . ')';
                $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'userdb.' . $k . ' LIKE :' . $k . ')';
            } elseif ($k == 'userdb_is_admin' || $k == 'userdb_is_agent') {
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                $dbStrParam[':' . $k] = $v;
                if ($admin_not_agent === true && !$display_admin) {
                    $string_where_clause .= '(' . $this->config['table_prefix'] . 'userdb.' . $k . ' = :' . $k . ')';
                    $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'userdb.' . $k . ' = :' . $k . ')';
                } elseif ($display_admin == 1 || (isset($_SESSION['userID']) && $_SESSION['userID'] == 1)) {
                    $string_where_clause .= '(' . $this->config['table_prefix'] . 'userdb.' . $k . ' = :' . $k . ' OR ' . $this->config['table_prefix'] . 'userdb.userdb_id = \'1\')';
                    $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'userdb.' . $k . ' = :' . $k . ' OR ' . $this->config['table_prefix'] . 'userdb.userdb_id = \'1\')';
                } else {
                    $string_where_clause .= '(' . $this->config['table_prefix'] . 'userdb.' . $k . ' = :' . $k . ')';
                    $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'userdb.' . $k . ' = :' . $k . ')';
                }
            } elseif ($k == 'user_last_modified_equal') {
                if (is_scalar($v)) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $dbStrParam[':' . $k] = date("Y-m-d H:i:s", (int)$v);
                    $searchresultSQL .= ' userdb_last_modified = :' . $k;
                }
            } elseif ($k == 'user_last_modified_greater') {
                if (is_scalar($v)) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $dbStrParam[':' . $k] = date("Y-m-d H:i:s", (int)$v);
                    $searchresultSQL .= ' userdb_last_modified > :' . $k;
                }
            } elseif ($k == 'user_last_modified_less') {
                if (is_scalar($v)) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $dbStrParam[':' . $k] = date("Y-m-d H:i:s", (int)$v);
                    $searchresultSQL .= ' userdb_last_modified < :' . $k;
                }
            } elseif ($k == 'userdb_creation_date_equal') {
                //$v = intval($v);
                if ($v > 0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $dbStrParam[':' . $k] = date("Y-m-d", (int)$v);
                    $searchresultSQL .= ' userdb_creation_date = :' . $k;
                }
            } elseif ($k == 'userdb_creation_date_greater') {
                if ($v > 0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $dbStrParam[':' . $k] = date("Y-m-d", (int)$v);
                    $searchresultSQL .= ' userdb_creation_date > :' . $k;
                }
            } elseif ($k == 'userdb_creation_date_less') {
                //$v = intval($v);
                if ($v > 0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $dbStrParam[':' . $k] = date("Y-m-d", (int)$v);
                    $searchresultSQL .= ' userdb_creation_date < :' . $k;
                }
            } elseif ($k == 'userdb_creation_date_equal_days') {
                $v = intval($v);
                if ($v > 0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $time = mktime(0, 0, 0, (int)date('m'), (int)date('d') - $v, (int)date('Y'));
                    $dbStrParam[':' . $k] = date("Y-m-d", $time);
                    $searchresultSQL .= ' userdb_creation_date = :' . $k;
                }
            } elseif ($k == 'userdb_creation_date_greater_days') {
                $v = intval($v);
                if ($v > 0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $time = mktime(0, 0, 0, (int)date('m'), (int)date('d') - $v, (int)date('Y'));
                    $dbStrParam[':' . $k] = date("Y-m-d", $time);
                    $searchresultSQL .= ' userdb_creation_date > :' . $k;
                }
            } elseif ($k == 'userdb_creation_date_less_days') {
                $v = intval($v);
                if ($v > 0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $time = mktime(0, 0, 0, (int)date('m'), (int)date('d') - $v, (int)date('Y'));
                    $dbStrParam[':' . $k] = date("Y-m-d", $time);
                    $searchresultSQL .= ' userdb_creation_date < :' . $k;
                }
            } elseif (
                $v != '' && $k != 'cur_page' && $k != 'action' && $k != 'PHPSESSID'
                && $k != 'sortby' && $k != 'sorttype' && $k != 'printer_friendly' && $k != 'template' && $k != 'x' && $k != 'y' && $k != 'userdb_expiration_greater'
                && $k != 'userdb_expiration_less' && $k != 'popup'
            ) {
                if (!is_array($v)) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }

                    //Handle NULL/NOTNULL Searches
                    if (str_ends_with($k, '-NULL') && $v == '1') {
                        $subk = substr($k, 0, -5);
                        if (array_key_exists($subk, $allowed_fields_type)) {
                            $searchresultSQL .= "(`$subk`.userdbelements_field_name = '$subk' AND (`$subk`.userdbelements_field_value IS NULL OR `$subk`.userdbelements_field_value = ''))";
                            if (!in_array($subk, $tablelist)) {
                                $tablelist[] = $subk;
                            }
                        }
                    } elseif (str_ends_with($k, '-NOTNULL') && $v == '1') {
                        $subk = substr($k, 0, -8);
                        if (array_key_exists($subk, $allowed_fields_type)) {
                            $searchresultSQL .= "(`$subk`.userdbelements_field_name = '$subk' AND (`$subk`.userdbelements_field_value IS NOT NULL  AND `$subk`.userdbelements_field_value <> ''))";
                            if (!in_array($subk, $tablelist)) {
                                $tablelist[] = $subk;
                            }
                        }
                    } elseif (str_ends_with($k, '-max')) {
                        $subk = substr($k, 0, -4);
                        if (array_key_exists($subk, $allowed_fields_type)) {
                            $field_type = $allowed_fields_type[$subk];
                            $dbStrParam[':' . $k] = $v;
                            if ($field_type == 'lat' || $field_type == 'long') {
                                $searchresultSQL .= "(`$subk`.userdbelements_field_name = $subk AND CAST(`$subk`.userdbelements_field_value as DECIMAL(13,7)) <= :$k)";
                            } elseif ($field_type == 'decimal') {
                                $searchresultSQL .= "(`$subk`.userdbelements_field_name = $subk AND CAST(`$subk`.userdbelements_field_value as DECIMAL(64,6)) <= :$k)";
                            } else {
                                $searchresultSQL .= "(`$subk`.userdbelements_field_name = $subk AND CAST(`$subk`.userdbelements_field_value as signed) <= :$k)";
                            }
                            if (!in_array($subk, $tablelist)) {
                                $tablelist[] = $subk;
                            }
                        }
                    } elseif (str_ends_with($k, '-min')) {
                        $subk = substr($k, 0, -4);
                        if (array_key_exists($subk, $allowed_fields_type)) {
                            $field_type = $allowed_fields_type[$subk];
                            $dbStrParam[':' . $k] = $v;


                            if ($field_type == 'lat' || $field_type == 'long') {
                                $searchresultSQL .= "(`$subk`.userdbelements_field_name = $subk AND CAST(`$subk`.userdbelements_field_value as DECIMAL(13,7)) >= :$k)";
                            } elseif ($field_type == 'decimal') {
                                $searchresultSQL .= "(`$subk`.userdbelements_field_name = $subk AND CAST(`$subk`.userdbelements_field_value as DECIMAL(64,6)) >= :$k)";
                            } else {
                                $searchresultSQL .= "(`$subk`.userdbelements_field_name = $subk AND CAST(`$subk`.userdbelements_field_value as signed) >= :$k)";
                            }

                            if (!in_array($subk, $tablelist)) {
                                $tablelist[] = $subk;
                            }
                        }
                    } elseif (str_ends_with($k, '-maxdate')) {
                        $subk = substr($k, 0, -8);
                        if (array_key_exists($subk, $allowed_fields_type)) {
                            $dbStrParam[':' . $k] = $this->convertDate($v);
                            $searchresultSQL .= "(`$subk`.userdbelements_field_name = '$subk' AND `$subk`.userdbelements_field_value <= :$k)";
                            if (!in_array($subk, $tablelist)) {
                                $tablelist[] = $subk;
                            }
                        }
                    } elseif (str_ends_with($k, '-mindate')) {
                        $subk = substr($k, 0, -8);
                        if (array_key_exists($subk, $allowed_fields_type)) {
                            $dbStrParam[':' . $k] = $this->convertDate($v);
                            $searchresultSQL .= "(`$subk`.userdbelements_field_name = '$subk' AND `$subk`.userdbelements_field_value >= :$k)";
                            if (!in_array($subk, $tablelist)) {
                                $tablelist[] = $subk;
                            }
                        }
                    } elseif (str_ends_with($k, '-date')) {
                        $subk = substr($k, 0, -5);
                        if (array_key_exists($subk, $allowed_fields_type)) {
                            $dbStrParam[':' . $k] = $this->convertDate($v);
                            $searchresultSQL .= "(`$subk`.userdbelements_field_name = '$subk' AND `$subk`.userdbelements_field_value = :$k)";
                            if (!in_array($subk, $tablelist)) {
                                $tablelist[] = $subk;
                            }
                        }
                    } elseif ($k == 'searchtext') {
                        $dbStrParam[':searchtext1'] = "%$v%";
                        $dbStrParam[':searchtext2'] = "%$v%";
                        $searchresultSQL .= "((`$k`.userdbelements_field_value like :searchtext1) OR (userdb_user_name like :searchtext2))";
                        $tablelist[] = $k;
                    } else {
                        if (array_key_exists($k, $allowed_fields_type)) {
                            $dbStrParam[':' . $k] = "$v";
                            $searchresultSQL .= "(`$k`.userdbelements_field_name = '$k' AND `$k`.userdbelements_field_value = :$k)";
                            $tablelist[] = $k;
                        }
                    }
                } else {
                    // Make Sure Array is not empty
                    $use = false;
                    $comma_separated = implode(' ', $v);
                    if (trim($comma_separated) != '') {
                        $use = true;
                        if ($searchresultSQL != '') {
                            $searchresultSQL .= ' AND ';
                        }
                    }
                    if ($use === true) {
                        if (str_ends_with($k, '_or')) {
                            $sub_k = substr($k, 0, strlen($k) - 3);
                            if (array_key_exists($sub_k, $allowed_fields_type)) {
                                $searchresultSQL .= "(`$sub_k`.userdbelements_field_name = '$k' AND (";
                                $vitem_count = 0;
                                foreach ($v as $vitem) {
                                    if ($vitem != '') {
                                        $dbStrParam[':' . $k . $vitem_count] = "%$vitem%";
                                        if ($vitem_count != 0) {
                                            $searchresultSQL .= " OR `$sub_k`.userdbelements_field_value LIKE :" . $k . $vitem_count;
                                        } else {
                                            $searchresultSQL .= " `$sub_k`.userdbelements_field_value LIKE :" . $k . $vitem_count;
                                        }
                                        $vitem_count++;
                                    }
                                }
                                $searchresultSQL .= '))';
                                $tablelist[] = $sub_k;
                            }
                        } else {
                            if (array_key_exists($k, $allowed_fields_type)) {
                                $searchresultSQL .= "(`$k`.userdbelements_field_name = '$k' AND (";
                                $vitem_count = 0;
                                foreach ($v as $vitem) {
                                    $dbStrParam[':' . $k . $vitem_count] = "%$vitem%";
                                    if ($vitem != '') {
                                        if ($vitem_count != 0) {
                                            $searchresultSQL .= " AND `$k`.userdbelements_field_value LIKE :" . $k . $vitem_count;
                                        } else {
                                            $searchresultSQL .= " `$k`.userdbelements_field_value LIKE :" . $k . $vitem_count;
                                        }
                                        $vitem_count++;
                                    }
                                }
                                $searchresultSQL .= '))';
                                $tablelist[] = $k;
                            }
                        }
                    }
                }
            }
        }

        // Handle Sorting
        // sort the users
        // this is the main SQL that grabs the users
        // basic sort by title..
        $group_order_text = '';
        $sortby_array = [];
        $sorttype_array = [];
        //Set array
        if (isset($data['sortby']) && !empty($data['sortby'])) {
            $sortby_array = $data['sortby'];
        }
        if (isset($data['sorttype']) && !empty($data['sorttype'])) {
            $sorttype_array = $data['sorttype'];
        }
        $sort_text = '';
        $order_text = '';
        $tablelist_nosort = $tablelist;
        $select_fields = [];
        $sort_count = count($sortby_array);
        for ($x = 0; $x < $sort_count; $x++) {
            if (!isset($sorttype_array[$x])) {
                $sorttype_array[$x] = '';
            } elseif ($sorttype_array[$x] != 'ASC' && $sorttype_array[$x] != 'DESC') {
                $sorttype_array[$x] = '';
            }
            if ($sortby_array[$x] == 'userdb_id') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY userdb_id ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',userdb_id ' . $sorttype_array[$x];
                }
            } elseif ($sortby_array[$x] == 'userdb_user_name') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY userdb_user_name ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',userdb_user_name ' . $sorttype_array[$x];
                }
                $select_fields[] = 'userdb_user_name';
            } elseif ($sortby_array[$x] == 'userdb_hit_count') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY userdb_hit_count ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',userdb_hit_count ' . $sorttype_array[$x];
                }
                $select_fields[] = 'userdb_hit_count';
            } elseif ($sortby_array[$x] == 'random') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY rand() ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',rand() ' . $sorttype_array[$x];
                }
            } elseif ($sortby_array[$x] == 'userdb_last_modified') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY userdb_last_modified ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',userdb_last_modified ' . $sorttype_array[$x];
                }
                $select_fields[] = 'userdb_last_modified';
            } elseif ($sortby_array[$x] == 'userdb_rank') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY userdb_rank ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',userdb_rank ' . $sorttype_array[$x];
                }
                $select_fields[] = 'userdb_rank';
            } else {
                $sort_by_field = $sortby_array[$x];
                $tablelist[] = 'sort' . $x;
                $select_fields[] = 'sort' . $x . '.userdbelements_field_value';
                if (array_key_exists($sort_by_field, $allowed_fields_type)) {
                    $field_type = $allowed_fields_type[$sort_by_field];
                    if ($field_type == 'price' || $field_type == 'number') {
                        $sort_text .= 'AND (sort' . $x . '.userdbelements_field_name = \'' . $sort_by_field . '\') ';
                        if ($order_text == '') {
                            $order_text .= ' ORDER BY CAST(sort' . $x . '.userdbelements_field_value as signed) ' . $sorttype_array[$x];
                            $group_order_text .= ',sort' . $x . '.userdbelements_field_value';
                        } else {
                            $order_text .= ',CAST(sort' . $x . '.userdbelements_field_value as signed) ' . $sorttype_array[$x];
                            $group_order_text .= ',sort' . $x . '.userdbelements_field_value';
                        }
                    } elseif ($field_type == 'decimal') {
                        $sort_text .= 'AND (sort' . $x . '.userdbelements_field_name = \'' . $sort_by_field . '\') ';
                        if ($order_text == '') {
                            $order_text .= ' ORDER BY CAST(sort' . $x . '.userdbelements_field_value as decimal(64,6)) ' . $sorttype_array[$x];
                            $group_order_text .= ',sort' . $x . '.userdbelements_field_value';
                        } else {
                            $order_text .= ',CAST(sort' . $x . '.userdbelements_field_value as signed) ' . $sorttype_array[$x];
                            $group_order_text .= ',sort' . $x . '.userdbelements_field_value';
                        }
                    } else {
                        $sort_text .= 'AND (sort' . $x . '.userdbelements_field_name = \'' . $sort_by_field . '\') ';
                        if ($order_text == '') {
                            $order_text .= ' ORDER BY sort' . $x . '.userdbelements_field_value ' . $sorttype_array[$x];
                        } else {
                            $order_text .= ', sort' . $x . '.userdbelements_field_value ' . $sorttype_array[$x];
                        }
                        $group_order_text .= ',sort' . $x . '.userdbelements_field_value';
                    }
                }
            }
        }
        $group_order_text = $group_order_text . ' ' . $order_text;

        $select_fields_text = '';
        if (count($select_fields) > 0) {
            $select_fields_text = ',' . join(",", $select_fields);
        }

        if ($imageonly) {
            $order_text = 'GROUP BY ' . $this->config['table_prefix'] . 'userdb.userdb_id, ' . $this->config['table_prefix'] . 'userdb.userdb_user_name ' . $group_order_text;
        }
        //        if ($DEBUG_SQL) {
        //            echo '<strong>Sort Type SQL:</strong> ' . $sql_sort_type . '<br />';
        //            echo '<strong>Sort Text:</strong> ' . $sort_text . '<br />';
        //            echo '<strong>Order Text:</strong> ' . $order_text . '<br />';
        //        }

        //$guidestring_with_sort = $guidestring_with_sort . $guidestring;
        // End of Sort
        $arrayLength = count($tablelist);
        //        if ($DEBUG_SQL) {
        //            echo '<strong>Table List Array Length:</strong> ' . $arrayLength . '<br />';
        //        }
        $string_table_list = '';
        for ($i = 0; $i < $arrayLength; $i++) {
            $string_table_list .= ' ,' . $this->config['table_prefix'] . 'userdbelements `' . $tablelist[$i] . '`';
        }
        $arrayLength = count($tablelist_nosort);
        $string_table_list_no_sort = '';
        for ($i = 0; $i < $arrayLength; $i++) {
            $string_table_list_no_sort .= ' ,' . $this->config['table_prefix'] . 'userdbelements `' . $tablelist[$i] . '`';
        }

        $arrayLength = count($tablelist);
        for ($i = 0; $i < $arrayLength; $i++) {
            if ($string_where_clause != '') {
                $string_where_clause .= ' AND ';
            }
            $string_where_clause .= ' (' . $this->config['table_prefix'] . 'userdb.userdb_id = `' . $tablelist[$i] . '`.userdb_id)';
        }
        $arrayLength = count($tablelist_nosort);
        for ($i = 0; $i < $arrayLength; $i++) {
            if ($string_where_clause_nosort != '') {
                $string_where_clause_nosort .= ' AND ';
            }
            $string_where_clause_nosort .= ' (' . $this->config['table_prefix'] . 'userdb.userdb_id = `' . $tablelist[$i] . '`.userdb_id)';
        }

        if ($imageonly) {
            $searchSQL = 'SELECT distinct(' . $this->config['table_prefix'] . 'userdb.userdb_id), ' . $this->config['table_prefix'] . 'userdb.userdb_id,
						' . $this->config['table_prefix'] . 'userdb.userdb_user_name ' . $select_fields_text . 'FROM ' . $this->config['table_prefix'] . 'userdb,
						' . $this->config['table_prefix'] . 'userimages ' . $string_table_list . '
						WHERE ' . $string_where_clause . '
						AND (' . $this->config['table_prefix'] . 'userimages.userdb_id = ' . $this->config['table_prefix'] . 'userdb.userdb_id)';

            $searchSQLCount = 'SELECT COUNT(distinct(' . $this->config['table_prefix'] . 'userdb.userdb_id)) as total_users ' . $select_fields_text . '
						FROM ' . $this->config['table_prefix'] . 'userdb, ' . $this->config['table_prefix'] . 'userimages ' . $string_table_list_no_sort . '
						WHERE ' . $string_where_clause_nosort . '
						AND (' . $this->config['table_prefix'] . 'userimages.userdb_id = ' . $this->config['table_prefix'] . 'userdb.userdb_id)';
        } else {
            $searchSQL = 'SELECT distinct(' . $this->config['table_prefix'] . 'userdb.userdb_id) ' . $select_fields_text . '
						FROM ' . $this->config['table_prefix'] . 'userdb ' . $string_table_list . '
						WHERE ' . $string_where_clause;
            $searchSQLCount = 'SELECT COUNT(distinct(' . $this->config['table_prefix'] . 'userdb.userdb_id)) as total_users 
						FROM ' . $this->config['table_prefix'] . 'userdb ' . $string_table_list_no_sort . '
						WHERE ' . $string_where_clause_nosort;
        }
        if ($searchresultSQL != '') {
            $searchSQL .= ' AND ' . $searchresultSQL;
            $searchSQLCount .= ' AND ' . $searchresultSQL;
        }

        $sql = $searchSQL . ' ' . $sort_text . ' ' . $order_text;
        if ($data['count_only']) {
            $sql = $searchSQLCount;
        }
        //$searchSQLCount = $searchSQLCount;
        // We now have a complete SQL Query. Now grab the results
        $process_time = $misc->getmicrotime();
        //echo 'Limit: '.$limit .'<br>';
        //echo 'Offset: '.$offset;

        if ($data['limit'] > 0 && isset($data['offset'])) {
            $sql = $sql . ' LIMIT :offset, :limit';
            $dbIntParam[':offset'] = $data['offset'];
            $dbIntParam[':limit'] = $data['limit'];
        }
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            foreach ($dbIntParam as $p => $v) {
                $stmt->bindValue($p, $v, PDO::PARAM_INT);
            }
            foreach ($dbStrParam as $p => $v) {
                $stmt->bindValue($p, $v);
            }
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }

        $query_time = $misc->getmicrotime();
        $query_time = $query_time - $process_time;
        $process_time = $process_time - $start_time;

        $users_found = [];

        if ($data['count_only']) {
            $user_count = (int)$stmt->fetchColumn();
        } else {
            $recordSet = $stmt->fetchAll();
            $user_count = count($recordSet);
            foreach ($recordSet as $record) {
                $users_found[] = (int)$record['userdb_id'];
            }
        }
        $total_time = $misc->getmicrotime();
        $total_time = $total_time - $start_time;
        $info = [];
        $info['process_time'] = sprintf('%.3f', $process_time);
        $info['query_time'] = sprintf('%.3f', $query_time);
        $info['total_time'] = sprintf('%.3f', $total_time);
        return [
            'user_count' => $user_count,
            'users' => $users_found,
            'info' => $info,
            'sortby' => $sortby_array,
            'sorttype' => $sorttype_array,
            'limit' => $data['limit'],
            'resource' => $data['resource'],
        ];
    }

    /**
     * This API Command creates users.
     *
     * @param array{
     *          user_details: array{
     *              user_name: string,
     *              user_first_name: string,
     *              user_last_name: string,
     *              emailaddress: string,
     *              user_password: string,
     *              active: string,
     *              is_admin: string,
     *              is_agent: string,
     *              rank?: integer,
     *          },
     *          user_fields?: array<string, mixed>
     *          } $data $data expects an array containing the following array keys.
     *
     *
     * @return array{user_id: integer}
     * @throws Exception
     */
    public function create(array $data): array
    {
        global $lang;
        $login = new Login($this->dbh, $this->config);
        $login_status = $login->verifyPriv('edit_all_users');
        if ($login_status !== true && $this->config['demo_mode'] != 1) {
            throw new Exception('Login Failure or Demo mode');
        }

        if (!isset($data['user_details'])) {
            throw new Exception('user_details: correct_parameter_not_passed');
        }

        if (empty($data['user_details']['user_name'])) {
            throw new Exception('$data[user_details][user_name]: correct_parameter_not_passed');
        }
        if (empty($data['user_details']['user_first_name'])) {
            throw new Exception('$data[user_details][user_first_name]: correct_parameter_not_passed');
        }
        if (empty($data['user_details']['user_last_name'])) {
            throw new Exception('$data[user_details][user_last_name]: correct_parameter_not_passed');
        }
        if (empty($data['user_details']['emailaddress'])) {
            throw new Exception('$data[user_details][emailaddress]: correct_parameter_not_passed');
        }
        if (empty($data['user_details']['user_password'])) {
            throw new Exception('$data[user_details][user_password]: correct_parameter_not_passed');
        }
        if (!isset($data['user_details']['active']) || $data['user_details']['active'] != 'yes' && $data['user_details']['active'] != 'no') {
            $data['user_details']['active'] = 'no';
        }
        if (!isset($data['user_details']['is_admin']) || $data['user_details']['is_admin'] != 'yes' && $data['user_details']['is_admin'] != 'no') {
            $data['user_details']['is_admin'] = 'no';
        }
        if (!isset($data['user_details']['is_agent']) || $data['user_details']['is_agent'] != 'yes') {
            $data['user_details']['is_agent'] = 'no';
            $resource = 'member';
        } else {
            $resource = 'agent';
        }

        //get a list of agent or member user fields based on $resource
        $fields_api = new FieldsApi($this->dbh, $this->config);
        $result = $fields_api->metadata([
            'resource' => $resource,
        ]);
        // if there are no user fields in the request
        // check the $result array to see if any are required.
        if (empty($data['user_fields'])) {
            $column_names = array_column($result['fields'], 'required');
            if (in_array('Yes', $column_names)) {
                throw new Exception('user_fields: Fields set as required are missing');
            }
        }

        //Ok we this far now we bulid the user.
        $errors = '';

        // first, make sure the user name isn't in use
        $user_api = new UserApi($this->dbh, $this->config);
        $result = $user_api->search([
            'parameters' => [
                'userdb_user_name' => $data['user_details']['user_name'],
                'userdb_active' => 'any',
            ],
            'resource' => 'agent',
            'count_only' => false,
        ]);
        $num = $result['user_count'];

        if ($num >= 1) {
            $errors .= $lang['user_creation_username_taken'] . '<br />';
        }
        // second, make sure the user email isn't in use
        $result = $user_api->search([
            'parameters' => [
                'userdb_emailaddress' => $data['user_details']['emailaddress'],
                'userdb_active' => 'any',
            ],
            'resource' => 'agent',
            'count_only' => true,
        ]);
        $num2 = $result['user_count'];

        if ($num2 >= 1) {
            $errors .= $lang['email_address_already_registered'] . '<br />';
        } // end if
        if (!empty($errors)) {
            throw new Exception($errors);
        }

        $sql = "INSERT INTO " . $this->config['table_prefix'] . "userdb (
                    userdb_user_name, 
                    userdb_user_password,
                    userdb_user_first_name,
                    userdb_user_last_name,
                    userdb_emailAddress,
                    userdb_creation_date,
                    userdb_last_modified,
                    userdb_active,
                    userdb_is_agent,
                    userdb_is_admin,
                    userdb_can_edit_member_template,
                    userdb_can_edit_agent_template,
                    userdb_can_edit_listing_template,
                    userdb_can_edit_lead_template,
                    userdb_can_feature_listings,
                    userdb_can_view_logs,
                    userdb_can_moderate,
                    userdb_can_edit_pages,
                    userdb_can_have_vtours,
                    userdb_can_have_files,
                    userdb_can_have_user_files,
                    userdb_limit_listings,
                    userdb_comments,
                    userdb_hit_count,
                    userdb_can_edit_expiration,
                    userdb_can_export_listings,
                    userdb_can_edit_all_users,
                    userdb_can_edit_all_listings,
                    userdb_can_edit_all_leads,
                    userdb_can_edit_site_config,
                    userdb_can_edit_property_classes,
                    userdb_can_manage_addons,
                    userdb_rank,
                    userdb_featuredlistinglimit,
                    userdb_email_verified,
                    userdb_blog_user_type
                ) 
                VALUES(
                    :sql_user_name,
                    :hash_user_pass,
                    :sql_user_first_name,
                    :sql_user_last_name,
                    :sql_user_email,
                    :userdb_creation_date,
                    :userdb_last_modified,
                    :sql_active,
                    :sql_isAgent,
                    :sql_isAdmin,
                    :sql_canEditMemberTemplate,
                    :sql_canEditAgentTemplate,
                    :sql_canEditListingTemplate,
                    :sql_canEditLeadTemplate,
                    :sql_canFeatureListings,
                    :sql_canViewLogs,
                    :sql_canModerate,
                    :sql_canPages,
                    :sql_canVtour,
                    :sql_canFiles,
                    :sql_canUserFiles,
                    :sql_limitListings,
                    '',
                    0,
                    :sql_canEditListingExpiration,
                    :sql_canExportListings,
                    :sql_canEditAllUsers,
                    :sql_canEditAllListings,
                    :sql_canEditAllLeads,
                    :sql_canEditSiteConfig,
                    :sql_canEditPropertyClasses,
                    :sql_canManageAddons,
                    :sql_rank,
                    :sql_limitFeaturedListings,
                    'yes',
                    :sql_blogUserType)";
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }

        $stmt->bindValue(':sql_user_name', $data['user_details']['user_name']);
        $stmt->bindValue(':sql_user_first_name', $data['user_details']['user_first_name']);
        $stmt->bindValue(':sql_user_last_name', $data['user_details']['user_last_name']);
        $stmt->bindValue(':sql_user_email', $data['user_details']['emailaddress']);
        $hash_user_pass = password_hash($data['user_details']['user_password'], PASSWORD_DEFAULT);
        $stmt->bindValue(':hash_user_pass', $hash_user_pass);
        $stmt->bindValue(':sql_active', $data['user_details']['active']);
        $stmt->bindValue(':sql_isAgent', $data['user_details']['is_agent']);
        $stmt->bindValue(':sql_isAdmin', $data['user_details']['is_admin']);

        if ($data['user_details']['is_admin'] == 'yes') {
            $resource = 'agent';
            //set this again because we don;t want an admin who is not an also Agent.
            $stmt->bindValue(':sql_isAgent', 'yes');

            $stmt->bindValue(':sql_limitFeaturedListings', -1, PDO::PARAM_INT);
            if (isset($data['user_details']['rank'])) {
                $stmt->bindValue(':sql_rank', intval($data['user_details']['rank']), PDO::PARAM_INT);
            } else {
                $stmt->bindValue(':sql_rank', 1, PDO::PARAM_INT);
            }
            $stmt->bindValue(':sql_limitListings', -1, PDO::PARAM_INT);
            $stmt->bindValue(':sql_canEditSiteConfig', 'no');
            $stmt->bindValue(':sql_canEditMemberTemplate', 'no');
            $stmt->bindValue(':sql_canEditAgentTemplate', 'no');
            $stmt->bindValue(':sql_canEditListingTemplate', 'no');
            $stmt->bindValue(':sql_canEditLeadTemplate', 'no');
            $stmt->bindValue(':sql_canFeatureListings', 'no');
            $stmt->bindValue(':sql_canViewLogs', 'no');
            $stmt->bindValue(':sql_canModerate', 'no');
            $stmt->bindValue(':sql_canPages', 'no');
            $stmt->bindValue(':sql_canVtour', 'no');
            $stmt->bindValue(':sql_canFiles', 'no');
            $stmt->bindValue(':sql_canUserFiles', 'no');
            $stmt->bindValue(':sql_canExportListings', 'no');
            $stmt->bindValue(':sql_canEditListingExpiration', 'no');
            $stmt->bindValue(':sql_canEditAllListings', 'no');
            $stmt->bindValue(':sql_canEditAllLeads', 'no');
            $stmt->bindValue(':sql_canEditAllUsers', 'no');
            $stmt->bindValue(':sql_canEditPropertyClasses', 'no');
            $stmt->bindValue(':sql_canManageAddons', 'no');
            $stmt->bindValue(':sql_blogUserType', 4, PDO::PARAM_INT);
        } elseif ($data['user_details']['is_agent'] == 'yes') {
            $resource = 'agent';
            if ($this->config['agent_default_edit_site_config']) {
                $stmt->bindValue(':sql_canEditSiteConfig', 'yes');
            } else {
                $stmt->bindValue(':sql_canEditSiteConfig', 'no');
            }
            if ($this->config['agent_default_edit_member_template']) {
                $stmt->bindValue(':sql_canEditMemberTemplate', 'yes');
            } else {
                $stmt->bindValue(':sql_canEditMemberTemplate', 'no');
            }
            if ($this->config['agent_default_edit_agent_template']) {
                $stmt->bindValue(':sql_canEditAgentTemplate', 'yes');
            } else {
                $stmt->bindValue(':sql_canEditAgentTemplate', 'no');
            }

            if ($this->config['agent_default_edit_listing_template']) {
                $stmt->bindValue(':sql_canEditListingTemplate', 'yes');
            } else {
                $stmt->bindValue(':sql_canEditListingTemplate', 'no');
            }
            if ($this->config['agent_default_edit_lead_template']) {
                $stmt->bindValue(':sql_canEditLeadTemplate', 'yes');
            } else {
                $stmt->bindValue(':sql_canEditLeadTemplate', 'no');
            }

            if ($this->config['agent_default_feature']) {
                $stmt->bindValue(':sql_canFeatureListings', 'yes');
            } else {
                $stmt->bindValue(':sql_canFeatureListings', 'no');
            }
            if ($this->config['agent_default_logview']) {
                $stmt->bindValue(':sql_canViewLogs', 'yes');
            } else {
                $stmt->bindValue(':sql_canViewLogs', 'no');
            }
            if ($this->config['agent_default_moderate']) {
                $stmt->bindValue(':sql_canModerate', 'yes');
            } else {
                $stmt->bindValue(':sql_canModerate', 'no');
            }
            if ($this->config['agent_default_editpages']) {
                $stmt->bindValue(':sql_canPages', 'yes');
            } else {
                $stmt->bindValue(':sql_canPages', 'no');
            }
            if ($this->config['agent_default_havevtours']) {
                $stmt->bindValue(':sql_canVtour', 'yes');
            } else {
                $stmt->bindValue(':sql_canVtour', 'no');
            }
            if ($this->config['agent_default_havefiles']) {
                $stmt->bindValue(':sql_canFiles', 'yes');
            } else {
                $stmt->bindValue(':sql_canFiles', 'no');
            }
            if ($this->config['agent_default_haveuserfiles']) {
                $stmt->bindValue(':sql_canUserFiles', 'yes');
            } else {
                $stmt->bindValue(':sql_canUserFiles', 'no');
            }
            $stmt->bindValue(':sql_limitListings', $this->config['agent_default_num_listings'], PDO::PARAM_INT);
            $stmt->bindValue(':sql_limitFeaturedListings', $this->config['agent_default_num_featuredlistings'], PDO::PARAM_INT);

            if ($this->config['agent_default_can_export_listings']) {
                $stmt->bindValue(':sql_canExportListings', 'yes');
            } else {
                $stmt->bindValue(':sql_canExportListings', 'no');
            }
            if ($this->config['agent_default_canchangeexpirations']) {
                $stmt->bindValue(':sql_canEditListingExpiration', 'yes');
            } else {
                $stmt->bindValue(':sql_canEditListingExpiration', 'no');
            }
            if ($this->config['agent_default_edit_all_listings']) {
                $stmt->bindValue(':sql_canEditAllListings', 'yes');
            } else {
                $stmt->bindValue(':sql_canEditAllListings', 'no');
            }
            if ($this->config['agent_default_edit_all_leads']) {
                $stmt->bindValue(':sql_canEditAllLeads', 'yes');
            } else {
                $stmt->bindValue(':sql_canEditAllLeads', 'no');
            }
            if ($this->config['agent_default_edit_all_users']) {
                $stmt->bindValue(':sql_canEditAllUsers', 'yes');
            } else {
                $stmt->bindValue(':sql_canEditAllUsers', 'no');
            }

            if ($this->config['agent_default_edit_property_classes']) {
                $stmt->bindValue(':sql_canEditPropertyClasses', 'yes');
            } else {
                $stmt->bindValue(':sql_canEditPropertyClasses', 'no');
            }

            if ($this->config['agent_default_canManageAddons']) {
                $stmt->bindValue(':sql_canManageAddons', 'yes');
            } else {
                $stmt->bindValue(':sql_canManageAddons', 'no');
            }
            if (isset($data['user_details']['rank'])) {
                $stmt->bindValue(':sql_rank', intval($data['user_details']['rank']), PDO::PARAM_INT);
            } else {
                //TODO: improve this to set to last rank position.
                $stmt->bindValue(':sql_rank', 5, PDO::PARAM_INT);
            }
            $stmt->bindValue(':sql_blogUserType', $this->config['agent_default_blogUserType'], PDO::PARAM_INT);
        } else {
            $resource = 'member';
            $stmt->bindValue(':sql_canEditSiteConfig', 'no');
            $stmt->bindValue(':sql_canEditMemberTemplate', 'no');
            $stmt->bindValue(':sql_canEditAgentTemplate', 'no');
            $stmt->bindValue(':sql_canEditListingTemplate', 'no');
            $stmt->bindValue(':sql_canEditLeadTemplate', 'no');
            $stmt->bindValue(':sql_canFeatureListings', 'no');
            $stmt->bindValue(':sql_canViewLogs', 'no');
            $stmt->bindValue(':sql_canModerate', 'no');
            $stmt->bindValue(':sql_canPages', 'no');
            $stmt->bindValue(':sql_canVtour', 'no');
            $stmt->bindValue(':sql_canFiles', 'no');
            $stmt->bindValue(':sql_canUserFiles', 'no');
            $stmt->bindValue(':sql_canExportListings', 'no');
            $stmt->bindValue(':sql_canEditListingExpiration', 'no');
            $stmt->bindValue(':sql_canEditAllListings', 'no');
            $stmt->bindValue(':sql_canEditAllLeads', 'no');
            $stmt->bindValue(':sql_canEditAllUsers', 'no');
            $stmt->bindValue(':sql_limitListings', 0, PDO::PARAM_INT);
            $stmt->bindValue(':sql_limitFeaturedListings', 0, PDO::PARAM_INT);
            $stmt->bindValue(':sql_rank', 0, PDO::PARAM_INT);
            $stmt->bindValue(':sql_canEditPropertyClasses', 'no');
            $stmt->bindValue(':sql_canManageAddons', 'no');
            $stmt->bindValue(':sql_blogUserType', 1, PDO::PARAM_INT);
        }
        // create the account
        $stmt->bindValue(':userdb_creation_date', date("Y-m-d", time()));
        $stmt->bindValue(':userdb_last_modified', date("Y-m-d H:i:s", time()));
        try {
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }

        $new_user_id = $this->dbh->lastInsertId();
        if (is_bool($new_user_id)) {
            throw new Exception('Failed Getting Last Insert ID');
        }
        $new_user_id = (int)$new_user_id;

        // Insert custom user fields
        if (isset($data['user_fields'])) {
            $sql = 'SELECT ' . $resource . 'formelements_default_text, 
                            ' . $resource . 'formelements_field_name, 
                            ' . $resource . 'formelements_id, 
                            ' . $resource . 'formelements_field_type, 
                            ' . $resource . 'formelements_field_elements,
                            ' . $resource . 'formelements_required
                    FROM  ' . $this->config['table_prefix'] . $resource . 'formelements';
            try {
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $recordSet = $stmt->fetchAll();
            //Verify user fields passed via API exist
            $insert_user_fields = [];
            foreach ($recordSet as $record) {
                $name = (string)$record[$resource . 'formelements_field_name'];
                $data_type = (string)$record[$resource . 'formelements_field_type'];
                $data_elements = (string)$record[$resource . 'formelements_field_elements'];
                $default_text = (string)$record[$resource . 'formelements_default_text'];
                if (array_key_exists($name, $data['user_fields'])) {
                    switch ($data_type) {
                        case 'number':
                            if ($data['user_fields'][$name] === '') {
                                $insert_user_fields[$name] = null;
                            } else {
                                if (is_numeric($data['user_fields'][$name])) {
                                    $insert_user_fields[$name] = $data['user_fields'][$name];
                                } else {
                                    $price = str_replace(',', '', $data['user_fields'][$name]);
                                    $insert_user_fields[$name] = intval($price);
                                }
                            }
                            break;
                        case 'decimal':
                        case 'price':
                            if (is_string($data['user_fields'][$name])) {
                                if ($data['user_fields'][$name] === '') {
                                    $insert_user_fields[$name] = null;
                                } else {
                                    $price = str_replace(',', '', $data['user_fields'][$name]);
                                    $insert_user_fields[$name] = (float)$price;
                                }
                            }
                            break;
                        case 'date':
                            if (is_string($data['user_fields'][$name])) {
                                if ($data['user_fields'][$name] === '') {
                                    $insert_user_fields[$name] = null;
                                } else {
                                    $insert_user_fields[$name] = $this->convertDate($data['user_fields'][$name]);
                                }
                            }
                            break;
                        case 'select':
                        case 'select-multiple':
                        case 'option':
                        case 'checkbox':
                            //This is a lookup field. Make sure values passed are allowed by the system.
                            //Get Array of allowed data elements
                            $data_elements_array = explode('||', $data_elements);
                            //Get array of passed data eleements
                            if (!is_array($data['user_fields'][$name])) {
                                $t_value = $data['user_fields'][$name];
                                unset($data['user_fields'][$name]);
                                $data['user_fields'][$name][] = $t_value;
                            }
                            $good_elements = [];
                            foreach ($data['user_fields'][$name] as $fvalue) {
                                if (in_array($fvalue, $data_elements_array) && !in_array($fvalue, $good_elements)) {
                                    $good_elements[] = $fvalue;
                                }
                            }
                            $insert_user_fields[$name] = $good_elements;
                            break;
                        default:
                            $insert_user_fields[$name] = $data['user_fields'][$name];
                            break;
                    }
                } else {
                    if ($default_text != '') {
                        $insert_user_fields[$name] = $default_text;
                    } else {
                        $insert_user_fields[$name] = '';
                    }
                }
            }
            $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'userdbelements (userdbelements_field_name, userdbelements_field_value, userdb_id) VALUES ';
            $sql2 = [];
            $x = 0;
            $dbIntParam = [];
            $dbStrParam = [];
            foreach ($insert_user_fields as $name => $value) {
                $dbStrParam[":sql_name$x"] = $name;
                if (is_array($value)) {
                    $dbStrParam[":sql_value$x"] = implode('||', $value);
                } else {
                    $dbStrParam[":sql_value$x"] = $value ?? '';
                }
                $dbIntParam[":new_user_id$x"] = $new_user_id;
                $sql2[] = "(:sql_name$x, :sql_value$x, :new_user_id$x)";
                $x++;
            }
            if (count($sql2) > 0) {
                $sql .= implode(',', $sql2);
                try {
                    $stmt = $this->dbh->prepare($sql);
                    if (is_bool($stmt)) {
                        throw new Exception('Prepairing Statement Failed');
                    }
                    foreach ($dbIntParam as $p => $v) {
                        $stmt->bindValue($p, $v, PDO::PARAM_INT);
                    }
                    foreach ($dbStrParam as $p => $v) {
                        $stmt->bindValue($p, $v);
                    }
                    $stmt->execute();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }
            }
        }

        //call the new User hook
        $hooks = new Hooks($this->dbh, $this->config);
        $hooks->load('afterUserSignup', $new_user_id);

        return ['user_id' => $new_user_id];
    }

    /**
     * This API Command reads user info
     *
     * @param array{
     *          user_id: integer,
     *          resource: string,
     *          fields?: string[],
     *  } $data expects an array containing the following array keys.
     *
     *  <ul>
     *      <li>$data['userdb_id'] - This is the user ID that we are updating.</li>
     *      <li>$data['resource'] - This is the resource you want to get fields for. Allowed Options are:
     *      'agent' or 'member'</li>
     *      <li>$data['fields'] - This is an optional array of fields to retrieve, if left empty or not
     *      passed all fields will be retrieved.</li>
     *  </ul>
     *
     * @return array
     *
     * @throws \Exception
     **/
    public function read(array $data): array
    {
        $login = new Login($this->dbh, $this->config);
        $login_is_agent = $login->verifyPriv('edit_all_users');

        if (!isset($data['user_id'])) {
            throw new Exception('user_id: correct_parameter_not_passed');
        }

        if (!isset($data['resource']) || $data['resource'] != 'agent' && $data['resource'] != 'member') {
            throw new Exception('resource: correct_parameter_not_passed');
        }
        //This will hold our user data
        $user_data = [];
        //If no fields were passed make an empty array to save checking for if !isset later
        if (!isset($data['fields'])) {
            $data['fields'] = [];
        }
        //
        $sql = 'SELECT 1 
				FROM ' . $this->config['table_prefix'] . 'userdb 
				WHERE userdb_id = :user_id';
        //TODO: Move this active check up higher to ensure user is active no matter if we ask for one field or all fields
        if (!$login_is_agent) {
            $sql .= ' AND userdb_active = \'yes\'';
        }
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }

            $stmt->bindValue(':user_id', $data['user_id'], PDO::PARAM_INT);

            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $user_count = (int)$stmt->fetchColumn();
        if ($user_count == 0) {
            throw new Exception('User does not exist or you do not have permission');
        }
        //Get Base user information
        if (empty($data['fields'])) {
            $sql = 'SELECT ' . implode(',', $this->OR_INT_FIELDS) . ' 
					FROM ' . $this->config['table_prefix'] . 'userdb 
					WHERE userdb_id = :user_id';

            try {
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }

                $stmt->bindValue(':user_id', $data['user_id'], PDO::PARAM_INT);

                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $recordSet = $stmt->fetchAll();
            if (count($recordSet) === 1) {
                $user_data['userdb_id'] = (int)$recordSet[0]['userdb_id'];
                $user_data['userdb_user_name'] = (string)$recordSet[0]['userdb_user_name'];
                $user_data['userdb_emailaddress'] = (string)$recordSet[0]['userdb_emailaddress'];
                $user_data['userdb_user_first_name'] = (string)$recordSet[0]['userdb_user_first_name'];
                $user_data['userdb_user_last_name'] = (string)$recordSet[0]['userdb_user_last_name'];
                $user_data['userdb_comments'] = (string)$recordSet[0]['userdb_comments'];
                $user_data['userdb_is_admin'] = (string)$recordSet[0]['userdb_is_admin'];
                $user_data['userdb_can_edit_site_config'] = (string)$recordSet[0]['userdb_can_edit_site_config'];
                $user_data['userdb_can_edit_member_template'] = (string)$recordSet[0]['userdb_can_edit_member_template'];
                $user_data['userdb_can_edit_agent_template'] = (string)$recordSet[0]['userdb_can_edit_agent_template'];
                $user_data['userdb_can_edit_listing_template'] = (string)$recordSet[0]['userdb_can_edit_listing_template'];
                $user_data['userdb_creation_date'] = (string)$recordSet[0]['userdb_creation_date'];
                $user_data['userdb_can_feature_listings'] = (string)$recordSet[0]['userdb_can_feature_listings'];
                $user_data['userdb_can_view_logs'] = (string)$recordSet[0]['userdb_can_view_logs'];
                $user_data['userdb_last_modified'] = (string)$recordSet[0]['userdb_last_modified'];
                $user_data['userdb_hit_count'] = (int)$recordSet[0]['userdb_hit_count'];
                $user_data['userdb_can_moderate'] = (string)$recordSet[0]['userdb_can_moderate'];
                $user_data['userdb_can_edit_pages'] = (string)$recordSet[0]['userdb_can_edit_pages'];
                $user_data['userdb_can_have_vtours'] = (string)$recordSet[0]['userdb_can_have_vtours'];
                $user_data['userdb_is_agent'] = (string)$recordSet[0]['userdb_is_agent'];
                $user_data['userdb_active'] = (string)$recordSet[0]['userdb_active'];
                $user_data['userdb_limit_listings'] = (int)$recordSet[0]['userdb_limit_listings'];
                $user_data['userdb_can_edit_expiration'] = (string)$recordSet[0]['userdb_can_edit_expiration'];
                $user_data['userdb_can_export_listings'] = (string)$recordSet[0]['userdb_can_export_listings'];
                $user_data['userdb_can_edit_all_users'] = (string)$recordSet[0]['userdb_can_edit_all_users'];
                $user_data['userdb_can_edit_all_listings'] = (string)$recordSet[0]['userdb_can_edit_all_listings'];
                $user_data['userdb_can_edit_property_classes'] = (string)$recordSet[0]['userdb_can_edit_property_classes'];
                $user_data['userdb_can_have_files'] = (string)$recordSet[0]['userdb_can_have_files'];
                $user_data['userdb_can_have_user_files'] = (string)$recordSet[0]['userdb_can_have_user_files'];
                $user_data['userdb_blog_user_type'] = (int)$recordSet[0]['userdb_blog_user_type'];
                $user_data['userdb_rank'] = (int)$recordSet[0]['userdb_rank'];
                $user_data['userdb_featuredlistinglimit'] = (int)$recordSet[0]['userdb_featuredlistinglimit'];
                $user_data['userdb_email_verified'] = (string)$recordSet[0]['userdb_email_verified'];
                $user_data['userdb_can_manage_addons'] = (string)$recordSet[0]['userdb_can_manage_addons'];
                $user_data['userdb_can_edit_all_leads'] = (string)$recordSet[0]['userdb_can_edit_all_leads'];
                $user_data['userdb_can_edit_lead_template'] = (string)$recordSet[0]['userdb_can_edit_lead_template'];
                $user_data['userdb_send_notifications_to_floor'] = (int)$recordSet[0]['userdb_send_notifications_to_floor'];
            }
            $fields_api = new FieldsApi($this->dbh, $this->config);
            $field_list = $fields_api->metadata(['resource' => $data['resource']]);
            //echo '<pre>'.print_r($field_list,true).'</pre>';

            $allowed_fields = [];
            $allowed_fields_values = [];
            $allowed_fields_type = [];
            foreach ($field_list['fields'] as $field) {
                $field_id = $field['field_id'];
                $field_name = $field['field_name'] ?? '';
                $field_elements = $field['field_elements'] ?? [];
                $ftype = $field['field_type'] ?? '';
                //Make sure field does not have same name as a core field for saftey
                if (!in_array($field_name, $this->OR_INT_FIELDS) && $field_name != '') {
                    $allowed_fields[$field_id] = $field_name;
                    if ($ftype == 'select' || $ftype == 'select-multiple' || $ftype == 'option' || $ftype == 'checkbox') {
                        $allowed_fields_values[$field_name] = $field_elements;
                        $allowed_fields_type[$field_name] = $ftype;
                    }
                }
            }
            //Get The fields
            $sql = 'SELECT userdbelements_field_name, userdbelements_field_value 
					FROM ' . $this->config['table_prefix'] . 'userdbelements 
					WHERE userdb_id = :user_id';
            try {
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }

                $stmt->bindValue(':user_id', $data['user_id'], PDO::PARAM_INT);

                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $recordSet = $stmt->fetchAll();
            foreach ($recordSet as $record) {
                $field_name = (string)$record['userdbelements_field_name'];
                $field_value = (string)$record['userdbelements_field_value'];
                if (!array_key_exists($field_name, $user_data)) {
                    if (in_array($field_name, $allowed_fields)) {
                        //See if this is a lookup
                        if (isset($allowed_fields_values[$field_name]) && isset($allowed_fields_type[$field_name])) {
                            if ($allowed_fields_type[$field_name] == 'select' || $allowed_fields_type[$field_name] == 'option') {
                                if (in_array($field_value, $allowed_fields_values[$field_name])) {
                                    $user_data[$field_name] = $field_value;
                                } else {
                                    $user_data[$field_name] = '';
                                }
                            } else {
                                $field_values = explode('||', $field_value);
                                $real_values = array_intersect($allowed_fields_values[$field_name], $field_values);
                                $user_data[$field_name] = $real_values;
                            }
                        } else {
                            $user_data[$field_name] = $field_value;
                        }
                    }
                }
            }
        } else {
            $core_fields = array_intersect($this->OR_INT_FIELDS, $data['fields']);
            $noncore_fields = array_diff($data['fields'], $this->OR_INT_FIELDS);
            if (!empty($core_fields)) {
                $sql = 'SELECT ' . implode(',', $core_fields) . ' 
						FROM ' . $this->config['table_prefix'] . 'userdb 
						WHERE userdb_id = :user_id';

                try {
                    $stmt = $this->dbh->prepare($sql);
                    if (is_bool($stmt)) {
                        throw new Exception('Prepairing Statement Failed');
                    }

                    $stmt->bindValue(':user_id', $data['user_id'], PDO::PARAM_INT);

                    $stmt->execute();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }
                $recordSet = $stmt->fetchAll();

                if (count($recordSet) == 1) {
                    if (in_array('userdb_id', $data['fields'])) {
                        $user_data['userdb_id'] = (int)$recordSet[0]['userdb_id'];
                    }
                    if (in_array('userdb_user_name', $data['fields'])) {
                        $user_data['userdb_user_name'] = (string)$recordSet[0]['userdb_user_name'];
                    }
                    if (in_array('userdb_emailaddress', $data['fields'])) {
                        $user_data['userdb_emailaddress'] = (string)$recordSet[0]['userdb_emailaddress'];
                    }
                    if (in_array('userdb_user_first_name', $data['fields'])) {
                        $user_data['userdb_user_first_name'] = (string)$recordSet[0]['userdb_user_first_name'];
                    }
                    if (in_array('userdb_user_last_name', $data['fields'])) {
                        $user_data['userdb_user_last_name'] = (string)$recordSet[0]['userdb_user_last_name'];
                    }
                    if (in_array('userdb_comments', $data['fields'])) {
                        $user_data['userdb_comments'] = (string)$recordSet[0]['userdb_comments'];
                    }
                    if (in_array('userdb_is_admin', $data['fields'])) {
                        $user_data['userdb_is_admin'] = (string)$recordSet[0]['userdb_is_admin'];
                    }
                    if (in_array('userdb_can_edit_site_config', $data['fields'])) {
                        $user_data['userdb_can_edit_site_config'] = (string)$recordSet[0]['userdb_can_edit_site_config'];
                    }
                    if (in_array('userdb_can_edit_member_template', $data['fields'])) {
                        $user_data['userdb_can_edit_member_template'] = (string)$recordSet[0]['userdb_can_edit_member_template'];
                    }
                    if (in_array('userdb_can_edit_agent_template', $data['fields'])) {
                        $user_data['userdb_can_edit_agent_template'] = (string)$recordSet[0]['userdb_can_edit_agent_template'];
                    }
                    if (in_array('userdb_can_edit_listing_template', $data['fields'])) {
                        $user_data['userdb_can_edit_listing_template'] = (string)$recordSet[0]['userdb_can_edit_listing_template'];
                    }
                    if (in_array('userdb_creation_date', $data['fields'])) {
                        $user_data['userdb_creation_date'] = (string)$recordSet[0]['userdb_creation_date'];
                    }
                    if (in_array('userdb_can_feature_listings', $data['fields'])) {
                        $user_data['userdb_can_feature_listings'] = (string)$recordSet[0]['userdb_can_feature_listings'];
                    }
                    if (in_array('userdb_can_view_logs', $data['fields'])) {
                        $user_data['userdb_can_view_logs'] = (string)$recordSet[0]['userdb_can_view_logs'];
                    }
                    if (in_array('userdb_last_modified', $data['fields'])) {
                        $user_data['userdb_last_modified'] = (string)$recordSet[0]['userdb_last_modified'];
                    }
                    if (in_array('userdb_hit_count', $data['fields'])) {
                        $user_data['userdb_hit_count'] = (int)$recordSet[0]['userdb_hit_count'];
                    }
                    if (in_array('userdb_can_moderate', $data['fields'])) {
                        $user_data['userdb_can_moderate'] = (string)$recordSet[0]['userdb_can_moderate'];
                    }
                    if (in_array('userdb_can_edit_pages', $data['fields'])) {
                        $user_data['userdb_can_edit_pages'] = (string)$recordSet[0]['userdb_can_edit_pages'];
                    }
                    if (in_array('userdb_can_have_vtours', $data['fields'])) {
                        $user_data['userdb_can_have_vtours'] = (string)$recordSet[0]['userdb_can_have_vtours'];
                    }
                    if (in_array('userdb_is_agent', $data['fields'])) {
                        $user_data['userdb_is_agent'] = (string)$recordSet[0]['userdb_is_agent'];
                    }
                    if (in_array('userdb_active', $data['fields'])) {
                        $user_data['userdb_active'] = (string)$recordSet[0]['userdb_active'];
                    }
                    if (in_array('userdb_limit_listings', $data['fields'])) {
                        $user_data['userdb_limit_listings'] = (int)$recordSet[0]['userdb_limit_listings'];
                    }
                    if (in_array('userdb_can_edit_expiration', $data['fields'])) {
                        $user_data['userdb_can_edit_expiration'] = (string)$recordSet[0]['userdb_can_edit_expiration'];
                    }
                    if (in_array('userdb_can_export_listings', $data['fields'])) {
                        $user_data['userdb_can_export_listings'] = (string)$recordSet[0]['userdb_can_export_listings'];
                    }
                    if (in_array('userdb_can_edit_all_users', $data['fields'])) {
                        $user_data['userdb_can_edit_all_users'] = (string)$recordSet[0]['userdb_can_edit_all_users'];
                    }
                    if (in_array('userdb_can_edit_all_listings', $data['fields'])) {
                        $user_data['userdb_can_edit_all_listings'] = (string)$recordSet[0]['userdb_can_edit_all_listings'];
                    }
                    if (in_array('userdb_can_edit_property_classes', $data['fields'])) {
                        $user_data['userdb_can_edit_property_classes'] = (string)$recordSet[0]['userdb_can_edit_property_classes'];
                    }
                    if (in_array('userdb_can_have_files', $data['fields'])) {
                        $user_data['userdb_can_have_files'] = (string)$recordSet[0]['userdb_can_have_files'];
                    }
                    if (in_array('userdb_can_have_user_files', $data['fields'])) {
                        $user_data['userdb_can_have_user_files'] = (string)$recordSet[0]['userdb_can_have_user_files'];
                    }
                    if (in_array('userdb_blog_user_type', $data['fields'])) {
                        $user_data['userdb_blog_user_type'] = (int)$recordSet[0]['userdb_blog_user_type'];
                    }
                    if (in_array('userdb_rank', $data['fields'])) {
                        $user_data['userdb_rank'] = (int)$recordSet[0]['userdb_rank'];
                    }
                    if (in_array('userdb_featuredlistinglimit', $data['fields'])) {
                        $user_data['userdb_featuredlistinglimit'] = (int)$recordSet[0]['userdb_featuredlistinglimit'];
                    }
                    if (in_array('userdb_email_verified', $data['fields'])) {
                        $user_data['userdb_email_verified'] = (string)$recordSet[0]['userdb_email_verified'];
                    }
                    if (in_array('userdb_can_manage_addons', $data['fields'])) {
                        $user_data['userdb_can_manage_addons'] = (string)$recordSet[0]['userdb_can_manage_addons'];
                    }
                    if (in_array('userdb_can_edit_all_leads', $data['fields'])) {
                        $user_data['userdb_can_edit_all_leads'] = (string)$recordSet[0]['userdb_can_edit_all_leads'];
                    }
                    if (in_array('userdb_can_edit_lead_template', $data['fields'])) {
                        $user_data['userdb_can_edit_lead_template'] = (string)$recordSet[0]['userdb_can_edit_lead_template'];
                    }
                    if (in_array('userdb_send_notifications_to_floor', $data['fields'])) {
                        $user_data['userdb_send_notifications_to_floor'] = (int)$recordSet[0]['userdb_send_notifications_to_floor'];
                    }
                }
            }

            if (!empty($noncore_fields)) {
                $fields_api = new FieldsApi($this->dbh, $this->config);
                $field_list = $fields_api->metadata(['resource' => $data['resource']]);

                $allowed_fields = [];
                $allowed_fields_values = [];
                $allowed_fields_type = [];

                foreach ($field_list['fields'] as $field) {
                    $field_id = $field['field_id'];
                    $field_name = $field['field_name'] ?? '';
                    $field_elements = $field['field_elements'] ?? [];
                    $ftype = $field['field_type'] ?? '';
                    //Make sure field does not have same name as a core field for saftey
                    if (in_array($field_name, $noncore_fields) && $field_name != '') {
                        $allowed_fields[$field_id] = $field_name;
                        $allowed_fields_type[$field_name] = $ftype;
                        if ($ftype == 'select' || $ftype == 'select-multiple' || $ftype == 'option' || $ftype == 'checkbox') {
                            $allowed_fields_values[$field_name] = $field_elements;
                        }
                    }
                }

                //Get The fields
                $sql = 'SELECT userdbelements_field_name, userdbelements_field_value 
						FROM ' . $this->config['table_prefix'] . 'userdbelements 
						WHERE userdb_id = :user_id';
                try {
                    $stmt = $this->dbh->prepare($sql);
                    if (is_bool($stmt)) {
                        throw new Exception('Prepairing Statement Failed');
                    }

                    $stmt->bindValue(':user_id', $data['user_id'], PDO::PARAM_INT);

                    $stmt->execute();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }
                $recordSet = $stmt->fetchAll();
                foreach ($recordSet as $record) {
                    $field_name = $record['userdbelements_field_name'];
                    $field_value = $record['userdbelements_field_value'];
                    if (!array_key_exists($field_name, $user_data)) {
                        if (in_array($field_name, $allowed_fields)) {
                            //See if this is a lookup
                            if (isset($allowed_fields_values[$field_name]) && isset($allowed_fields_type[$field_name])) {
                                if ($allowed_fields_type[$field_name] == 'select' || $allowed_fields_type[$field_name] == 'option') {
                                    if (in_array($field_value, $allowed_fields_values[$field_name])) {
                                        $user_data[$field_name] = $field_value;
                                    } else {
                                        $user_data[$field_name] = '';
                                    }
                                } else {
                                    $field_values = explode('||', $field_value);
                                    $real_values = array_intersect($allowed_fields_values[$field_name], $field_values);
                                    $user_data[$field_name] = $real_values;
                                }
                            } else {
                                $user_data[$field_name] = $field_value;
                            }
                        }
                    }
                }
            }
        }
        return ['user' => $user_data];
    }

    /**
     * This API Command updates users.
     *
     * @param array{
     *          user_id: int,
     *          user_details: array{
     *                  active?: boolean,
     *                  user_first_name?: non-empty-string,
     *                  user_last_name?: non-empty-string,
     *                  emailaddress?: non-empty-string,
     *                  user_password?: non-empty-string,
     *                  hit_count?: integer,
     *                  can_edit_site_config?: boolean,
     *     can_edit_member_template?: boolean,
     *     can_edit_agent_template?: boolean,
     *     can_edit_listing_template?: boolean,
     *     can_feature_listings?: boolean,
     *     can_view_logs?: boolean,
     *     can_moderate?: boolean,
     *     can_edit_pages?: boolean,
     *     can_have_vtours?: boolean,
     *     can_edit_expiration?: boolean,
     *     can_export_listings?: boolean,
     *     can_edit_all_users?: boolean,
     *     can_edit_all_listings?: boolean,
     *     can_edit_property_classes?: boolean,
     *     can_have_files?: boolean,
     *     can_have_user_files?: boolean,
     *     can_manage_addons?: boolean,
     *     can_edit_all_leads?: boolean,
     *     can_edit_lead_template?: boolean,
     *     comments?: string,
     *     limit_listings?: integer,
     *      blog_user_type?: integer,
     *     rank?: integer,
     *     featuredlistinglimit?: integer,
     *     email_verified?: boolean,
     *     send_notifications_to_floor?: boolean,
     *              },
     *              user_fields?: array<string, mixed>
     *              } $data Expects an array containing the following array keys.
     *
     *   <ul>
     *      <li>$data['userdb_id'] - This is the user ID that we are updating.</li>
     *      <li>$data['user_details'] - This should be an array containg the following three
     *      settings.</li>
     *      <li>$data['user_details']['active'] -Set if this a active users, only set if you need to
     *      change. true/false</li>
     *      <li>$data['user_fields'] - This should be an array of the actual user data. The array keys
     *      should be the field name and the array values should be the field values. Only valid fields
     *      will be used, other data will be dropped.
     *      <code>$data['user_fields'] =array('phone' => '555-555-1212','fax'=>'555-555-1313');
     *  </ul>
     *
     * @return array{
     *     userdb_id: integer
     *  }
     * @throws \Exception
     *
     */
    public function update(array $data): array
    {
        global $lang;

        $misc = new Misc($this->dbh, $this->config);
        $login = new Login($this->dbh, $this->config);
        //setup array that will contain our SQL fields for update
        $dbStrParams = [];
        $dbIntParams = [];

        //Check that required settings were passed
        if (!isset($data['user_id'])) {
            throw new Exception('user_id: correct_parameter_not_passed');
        }

        $userdb_id = intval($data['user_id']);

        //get user status (resource) of user
        $is_admin = $misc->getAdminStatus($userdb_id);
        $is_agent = $misc->getAgentStatus($userdb_id);
        $data['resource'] = 'member';
        if ($is_agent === true || $is_admin === true) {
            $data['resource'] = 'agent';
        }

        //does this user exist?
        $user_api = new UserApi($this->dbh, $this->config);
        $resultc = $user_api->search([
            'parameters' => [
                'userdb_id' => $userdb_id,
                'userdb_active' => 'any',
            ],
            'resource' => $data['resource'],
        ]);
        if ($resultc['user_count'] == 0) {
            throw new Exception('user_id: ' . $userdb_id . ' not present');
        }

        $editing_self = false;
        //check permissions.
        if ($_SESSION['userID'] === $data['user_id']) {
            $editing_self = true;
        }
        $edit_all_users_permission = $login->verifyPriv('edit_all_users');
        if (!$edit_all_users_permission && $_SESSION['admin_privs'] != 'yes' && !$editing_self) {
            throw new Exception('Permission denied');
        }


        //these can't be set empty, and have lowest restrictions
        if (isset($data['user_details']['user_first_name']) && empty($data['user_details']['user_first_name'])) {
            throw new Exception('user_details[user_first_name]: cannot be empty');
        } elseif (isset($data['user_details']['user_first_name'])) {
            $dbStrParams['userdb_user_first_name'] = $data['user_details']['user_first_name'];
        }

        if (isset($data['user_details']['user_last_name']) && empty($data['user_details']['user_last_name'])) {
            throw new Exception('user_details[user_last_name]: cannot be empty');
        } elseif (isset($data['user_details']['user_last_name'])) {
            $dbStrParams['userdb_user_last_name'] = $data['user_details']['user_last_name'];
        }
        if (isset($data['user_details']['emailaddress']) && !filter_var($data['user_details']['emailaddress'], FILTER_VALIDATE_EMAIL)) {
            throw new Exception('user_details[emailaddress]: not a valid address');
        } elseif (isset($data['user_details']['emailaddress'])) {
            //make sure this address does not already exist
            $user_api = new UserApi($this->dbh, $this->config);
            $result = $user_api->search([
                'parameters' => [
                    'userdb_emailaddress' => $data['user_details']['emailaddress'],
                    'userdb_active' => 'any',
                ],
                'resource' => $data['resource'],
                'count_only' => true,
            ]);
            $num = $result['user_count'];
            if ($num >= 1) {
                throw new Exception('user_details[emailaddress]: address already exists');
            } else {
                $dbStrParams['userdb_emailaddress'] = $data['user_details']['emailaddress'];
            }
        }

        if (isset($data['user_details']['user_password']) && empty($data['user_details']['user_password'])) {
            throw new Exception('user_details[user_password]: cannot be empty');
        } elseif (isset($data['user_details']['user_password'])) {
            $dbStrParams['userdb_user_password'] = password_hash($data['user_details']['user_password'], PASSWORD_DEFAULT);
        }

        //Admin or edit_all_users permissions required
        if (($edit_all_users_permission === true || $_SESSION['admin_privs'] == 'yes')) {
            if (isset($data['user_details']['active'])) {
                if ($data['user_details']['active'] === true) {
                    $dbStrParams['userdb_active'] = 'yes';
                } else {
                    $dbStrParams['userdb_active'] = 'no';
                }
            }
            //See if the user is currently active.
            $user_api = new UserApi($this->dbh, $this->config);
            $api_result = $user_api->read(['user_id' => $userdb_id, 'fields' => ['userdb_active'], 'resource' => $data['resource']]);
            $oldstatus = $api_result['user']['userdb_active'];

            if (isset($data['user_details']['hit_count'])) {
                $dbIntParams['userdb_hit_count'] = intval($data['user_details']['hit_count']);
            }

            //check and set values only if an Agent account.
            if ($is_agent === true) {
                //
                //boolean permission fields. These can only be set by an Admin or an Agent with edit_all_users permissions
                if (isset($data['user_details']['can_edit_site_config'])) {
                    if ($data['user_details']['can_edit_site_config']) {
                        $dbStrParams['userdb_can_edit_site_config'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_edit_site_config'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_edit_member_template'])) {
                    if ($data['user_details']['can_edit_member_template']) {
                        $dbStrParams['userdb_can_edit_member_template'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_edit_member_template'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_edit_agent_template'])) {
                    if ($data['user_details']['can_edit_agent_template']) {
                        $dbStrParams['userdb_can_edit_agent_template'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_edit_agent_template'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_edit_listing_template'])) {
                    if ($data['user_details']['can_edit_listing_template']) {
                        $dbStrParams['userdb_can_edit_listing_template'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_edit_listing_template'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_feature_listings'])) {
                    if ($data['user_details']['can_feature_listings']) {
                        $dbStrParams['userdb_can_feature_listings'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_feature_listings'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_view_logs'])) {
                    if ($data['user_details']['can_view_logs']) {
                        $dbStrParams['userdb_can_view_logs'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_view_logs'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_moderate'])) {
                    if ($data['user_details']['can_moderate']) {
                        $dbStrParams['can_moderate'] = 'yes';
                    } else {
                        $dbStrParams['can_moderate'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_edit_pages'])) {
                    if ($data['user_details']['can_edit_pages']) {
                        $dbStrParams['userdb_can_edit_pages'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_edit_pages'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_have_vtours'])) {
                    if ($data['user_details']['can_have_vtours']) {
                        $dbStrParams['userdb_can_have_vtours'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_have_vtours'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_edit_expiration'])) {
                    if ($data['user_details']['can_edit_expiration']) {
                        $dbStrParams['userdb_can_edit_expiration'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_edit_expiration'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_export_listings'])) {
                    if ($data['user_details']['can_export_listings']) {
                        $dbStrParams['userdb_can_export_listings'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_export_listings'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_edit_all_users'])) {
                    if ($data['user_details']['can_edit_all_users']) {
                        $dbStrParams['userdb_can_edit_all_users'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_edit_all_users'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_edit_all_listings'])) {
                    if ($data['user_details']['can_edit_all_listings']) {
                        $dbStrParams['userdb_can_edit_all_listings'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_edit_all_listings'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_edit_property_classes'])) {
                    if ($data['user_details']['can_edit_property_classes']) {
                        $dbStrParams['userdb_can_edit_property_classes'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_edit_property_classes'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_have_files'])) {
                    if ($data['user_details']['can_have_files']) {
                        $dbStrParams['userdb_can_have_files'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_have_files'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_have_user_files'])) {
                    if ($data['user_details']['can_have_user_files']) {
                        $dbStrParams['userdb_can_have_user_files'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_have_user_files'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_manage_addons'])) {
                    if ($data['user_details']['can_manage_addons']) {
                        $dbStrParams['userdb_can_manage_addons'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_manage_addons'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_edit_all_leads'])) {
                    if ($data['user_details']['can_edit_all_leads']) {
                        $dbStrParams['userdb_can_edit_all_leads'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_edit_all_leads'] = 'no';
                    }
                }

                if (isset($data['user_details']['can_edit_lead_template'])) {
                    if ($data['user_details']['can_edit_lead_template']) {
                        $dbStrParams['userdb_can_edit_lead_template'] = 'yes';
                    } else {
                        $dbStrParams['userdb_can_edit_lead_template'] = 'no';
                    }
                }

                //
                //misc settings. These can also only be set by an Admin or an Agent with edit_all_users permissions
                if (isset($data['user_details']['comments']) && empty($data['user_details']['comments'])) {
                    throw new Exception('user_details[comments]: correct_parameter_not_passed');
                } elseif (isset($data['user_details']['comments'])) {
                    $dbStrParams['userdb_comments'] = $data['user_details']['comments'];
                }

                if (isset($data['user_details']['limit_listings'])) {
                    $dbIntParams['userdb_limit_listings'] = intval($data['user_details']['limit_listings']);
                }

                if (isset($data['user_details']['blog_user_type'])) {
                    $dbIntParams['userdb_blog_user_type'] = intval($data['user_details']['blog_user_type']);
                }

                if (isset($data['user_details']['rank'])) {
                    $dbIntParams['userdb_rank'] = intval($data['user_details']['rank']);
                }

                if (isset($data['user_details']['featuredlistinglimit'])) {
                    $dbIntParams['userdb_featuredlistinglimit'] = intval($data['user_details']['featuredlistinglimit']);
                }

                if (isset($data['user_details']['email_verified'])) {
                    if ($data['user_details']['email_verified']) {
                        $dbStrParams['userdb_email_verified'] = 'yes';
                    } else {
                        $dbStrParams['userdb_email_verified'] = 'no';
                    }
                }

                if (isset($data['user_details']['send_notifications_to_floor'])) {
                    if ($data['user_details']['send_notifications_to_floor'] === true) {
                        $dbIntParams['userdb_send_notifications_to_floor'] = 1;
                    } else {
                        $dbIntParams['userdb_send_notifications_to_floor'] = 0;
                    }
                }
            }


            $dbStrParams['userdb_last_modified'] = date("Y-m-d H:i:s", time());

            $sql_a = [];
            foreach ($dbStrParams as $field => $value) {
                $sql_a[] = $field . ' = :' . $field;
            }
            foreach ($dbIntParams as $field => $value) {
                $sql_a[] = $field . ' = :' . $field;
            }

            $sql = 'UPDATE ' . $this->config['table_prefix'] . 'userdb 
					SET ' . implode(',', $sql_a) . ' 
					WHERE userdb_id = :userdb_id';
            try {
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
                foreach ($dbIntParams as $p => $v) {
                    $stmt->bindValue(':' . $p, $v, PDO::PARAM_INT);
                }
                $stmt->bindValue(':userdb_id', $userdb_id, PDO::PARAM_INT);
                foreach ($dbStrParams as $p => $v) {
                    $stmt->bindValue(':' . $p, $v);
                }
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }

            //Deal with user fields
            if (isset($data['user_fields'])) {
                //Get List of user fields
                $sql = 'SELECT ' . $data['resource'] . 'formelements_field_name, ' . $data['resource'] . 'formelements_id,' . $data['resource'] . 'formelements_field_type, ' . $data['resource'] . 'formelements_field_elements
							FROM  ' . $this->config['table_prefix'] . $data['resource'] . 'formelements';

                try {
                    $stmt = $this->dbh->prepare($sql);
                    if (is_bool($stmt)) {
                        throw new Exception('Prepairing Statement Failed');
                    }
                    $stmt->execute();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }
                $recordSet = $stmt->fetchAll();
                //Verify user fields passed via API exist in the applicable formelements table.
                $insert_user_fields = [];
                foreach ($recordSet as $record) {
                    $name = (string)$record[$data['resource'] . 'formelements_field_name'];
                    if (array_key_exists($name, $data['user_fields'])) {
                        $data_type = (string)$record[$data['resource'] . 'formelements_field_type'];
                        $data_elements = (string)$record[$data['resource'] . 'formelements_field_elements'];
                        switch ($data_type) {
                            case 'number':
                                if ($data['user_fields'][$name] === '') {
                                    $insert_user_fields[$name] = null;
                                } else {
                                    if (is_numeric($data['user_fields'][$name])) {
                                        $insert_user_fields[$name] = $data['user_fields'][$name];
                                    } else {
                                        $price = str_replace(',', '', $data['user_fields'][$name]);
                                        $insert_user_fields[$name] = intval($price);
                                    }
                                }
                                break;
                            case 'decimal':
                            case 'price':
                                if (is_string($data['user_fields'][$name])) {
                                    if ($data['user_fields'][$name] === '') {
                                        $insert_user_fields[$name] = null;
                                    } else {
                                        $price = str_replace(',', '', $data['user_fields'][$name]);
                                        $insert_user_fields[$name] = (float)$price;
                                    }
                                }
                                break;
                            case 'date':
                                if (is_string($data['user_fields'][$name])) {
                                    if ($data['user_fields'][$name] === '') {
                                        $insert_user_fields[$name] = null;
                                    } else {
                                        $insert_user_fields[$name] = $this->convertDate($data['user_fields'][$name]);
                                    }
                                }
                                break;
                            case 'select':
                            case 'select-multiple':
                            case 'option':
                            case 'checkbox':
                                //This is a lookup field. Make sure values passed are allowed by the system.
                                //Get Array of allowed data elements
                                $data_elements_array = explode('||', $data_elements);
                                //echo '<pre> Data Elements: '.print_r($data_elements_array,true).'</pre>';
                                //Get array of passed data eleements
                                if (!is_array($data['user_fields'][$name])) {
                                    $t_value = $data['user_fields'][$name];
                                    unset($data['user_fields'][$name]);
                                    $data['user_fields'][$name][] = $t_value;
                                }
                                //echo '<pre> Field Elements: '.print_r($data['user_fields'][$name],true).'</pre>';
                                $good_elements = [];
                                foreach ($data['user_fields'][$name] as $fvalue) {
                                    if (in_array($fvalue, $data_elements_array) && !in_array($fvalue, $good_elements)) {
                                        $good_elements[] = $fvalue;
                                    }
                                }
                                //echo '<pre> Good Elements: '.print_r($good_elements,true).'</pre>';
                                $insert_user_fields[$name] = $good_elements;
                                break;
                            default:
                                $insert_user_fields[$name] = $data['user_fields'][$name];
                                break;
                        }
                    }
                }

                $sql = 'UPDATE ' . $this->config['table_prefix'] . "userdbelements 
							SET userdbelements_field_value = :field_value 
							WHERE userdbelements_field_name = :field_name 
							AND userdb_id = :userdb_id";
                try {
                    $stmt = $this->dbh->prepare($sql);
                    if (is_bool($stmt)) {
                        throw new Exception('Prepairing Statement Failed');
                    }
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }
                foreach ($insert_user_fields as $name => $value) {
                    $stmt->bindValue(':userdb_id', $userdb_id, PDO::PARAM_INT);
                    $stmt->bindValue(':field_name', $name);

                    if (is_array($value)) {
                        $sql_value = implode('||', $value);
                    } else {
                        $sql_value = $value;
                    }
                    $stmt->bindValue(':field_value', $sql_value);

                    try {
                        $stmt->execute();
                    } catch (Exception $e) {
                        $error = $e->getMessage();
                        $log_api = new LogApi($this->dbh, $this->config);
                        $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                        throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                    }
                }
            }
            //Deal with Hooks and social sites
            if (isset($data['user_details']['active']) && $data['user_details']['active'] && $oldstatus == 'no') {
                $hooks = new Hooks($this->dbh, $this->config);
                $hooks->load('after_activated_user', $userdb_id);
            }


            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create([

                'log_api_command' => 'api->user->update',
                'log_message' => $lang['log_updated_user'] . ' ' . $userdb_id . ' by ' . $_SESSION['username'],
            ]);

            //call the changed user change hook
            $hooks = new Hooks($this->dbh, $this->config);
            $hooks->load('afterUserUpdate', $userdb_id);
            return ['userdb_id' => $userdb_id];
        } //end permission check
        throw new Exception($lang['user_manager_permission_denied']);
    }

    /**
     * This API Command deletes users.
     *
     * @param array{
     *      user_id: integer
     *  } $data expects an array containing the following array keys.
     *
     *  <ul>
     *      <li>$data['user_id'] - Number - User ID to delete</li>
     *  <ul>
     *
     * @returns array{user_id: integer}
     * @throws \Exception
     */
    public function delete(array $data): array
    {
        global $lang;

        $misc = new Misc($this->dbh, $this->config);
        $login = new Login($this->dbh, $this->config);

        $login_status = $login->verifyPriv('edit_all_users');
        if ($login_status !== true) {
            throw new Exception('Login Failure');
        }

        //Check that required settings were passed
        if (!isset($data['user_id'])) {
            throw new Exception('user_id: ' . $lang['user_manager_invalid_user_id']);
        }

        // Set Variable to hold errors
        $errors = '';

        if ($this->config['demo_mode'] && $_SESSION['admin_privs'] != 'yes') {
            throw new Exception($lang['demo_mode'] . ' - ' . $lang['user_manager_permission_denied']);
        }

        // if this is the admin account, forget it
        if ($data['user_id'] === 1) {
            $errors .= $lang['admin_delete_warning'];
            throw new Exception($errors);
        }

        $is_admin = $misc->getAdminStatus($data['user_id']);

        // Admins can delete any user but Admin. Anyone can delete their own information as this is needed for updates.
        if (($_SESSION['admin_privs'] == 'yes' || $_SESSION['edit_all_users'] == 'yes') && $data['user_id'] != '') {
            $delete_id = $data['user_id'];
        } elseif (($_SESSION['admin_privs'] == 'yes' && $data['user_id'] == '') || ($_SESSION['userID'] == $data['user_id'])) {
            $delete_id = (int)$_SESSION['userID'];
        } else {
            //return $lang['user_manager_permission_denied'];
            throw new Exception($lang['user_manager_permission_denied'] . '1');
        }
        if ($is_admin && $_SESSION['admin_privs'] == 'no') {
            //return $lang['user_manager_permission_denied'];
            throw new Exception($lang['user_manager_permission_denied'] . '2');
        }

        //delete all Agent photos
        $media_api = new MediaApi($this->dbh, $this->config);
        $media_api->delete([
            'media_type' => 'userimages',
            'media_parent_id' => $delete_id,
            'media_object_id' => '*',
        ]);

        //delete all Agent Files
        $media_api->delete([
            'media_type' => 'usersfiles',
            'media_parent_id' => $delete_id,
            'media_object_id' => '*',
        ]);

        //get a list of this agent's listing ID#s
        $listing_api = new ListingApi($this->dbh, $this->config);
        $result = $listing_api->search([
            'parameters' => [
                'user_ID' => $delete_id,
                'userdb_active' => 'any',
            ],
        ]);

        $num_rows = $result['listing_count'];

        if ($num_rows > 0) {
            //delete all associated listings
            foreach ($result['listings'] as $listing_id) {
                $listing_api = new ListingApi($this->dbh, $this->config);
                try {
                    $listing_api->delete(['listing_id' => $listing_id]);
                } catch (Exception $e) {
                    $errors .= $e->getMessage();
                }
            }
        }
        if ($errors != '') {
            throw new Exception($errors);
        }

        // delete all the favorites associated with a user
        $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'userfavoritelistings 
            WHERE userdb_id = :delete_id';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->bindValue(':delete_id', $delete_id, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        // delete all the saved searches associated with a user
        $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'usersavedsearches 
                WHERE userdb_id = :delete_id';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->bindValue(':delete_id', $delete_id, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }

        // delete all the elements associated with the user
        $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'userdbelements 
				WHERE userdb_id = :delete_id';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->bindValue(':delete_id', $delete_id, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        // delete the user
        $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'userdb 
        WHERE userdb_id = :delete_id';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->bindValue(':delete_id', $delete_id, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }

        // ta da! we're done...
        $log_api = new LogApi($this->dbh, $this->config);
        $log_api->create([

            'log_api_command' => 'api->users->delete',
            'log_message' => $lang['log_deleted_user'] . ' ' . $data['user_id'] . ' by ' . $_SESSION['username'],
        ]);

        $hooks = new Hooks($this->dbh, $this->config);
        $hooks->load('afterUserDelete', $delete_id);

        //success
        return ['user_id' => $data['user_id']];
    }

    /**
     * Updates the user fields (userdbelements) info
     *
     * @param int   $user_id
     * @param array $formdata
     *
     * @return bool
     * @throws \Exception
     */
    private function updateUserData(int $user_id, array $formdata): bool
    {
        $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'userdbelements 
				WHERE userdb_id = :user_id';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->bindValue(':user_id', $user_id, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }


        $user_api = new UserApi($this->dbh, $this->config);
        try {
            $result = $user_api->read([
                'user_id' => $user_id,
                'resource' => 'agent',
                'fields' => ['userdb_is_admin', 'userdb_is_agent'],
            ]);
        } catch (Exception) {
            return false;
        }
        if ((isset($result['user']['userdb_is_admin']) && $result['user']['userdb_is_admin'] === 'yes') || (isset($result['user']['userdb_is_agent']) && $result['user']['userdb_is_agent'] === 'yes')) {
            $db_to_use = 'agent';
        } else {
            $db_to_use = 'member';
        }

        $sql = 'SELECT ' . $db_to_use . 'formelements_field_type 
					FROM ' . $this->config['table_prefix'] . $db_to_use . 'formelements 
					WHERE ' . $db_to_use . "formelements_field_name = :field_name";
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $sqlInsert = 'INSERT INTO ' . $this->config['table_prefix'] . 'userdbelements (userdbelements_field_name, userdbelements_field_value, userdb_id) 
							VALUES (:field_name, :field_value, :user_id)';
        try {
            $stmtInsert = $this->dbh->prepare($sqlInsert);
            if (is_bool($stmtInsert)) {
                throw new Exception('Prepairing Statement Failed');
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sqlInsert]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sqlInsert);
        }
        foreach ($formdata as $ElementIndexValue => $ElementContents) {
            try {
                $stmt->bindValue(':field_name', $ElementIndexValue);
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $recordSet = $stmt->fetchAll();
            $field_count = count($recordSet);
            if ($field_count == 1) {
                $field_type = $recordSet[0][$db_to_use . 'formelements_field_type'];
                // first, ignore all the stuff that's been taken care of above
                if ($ElementIndexValue == 'user_user_name' || $ElementIndexValue == 'edit_user_pass' || $ElementIndexValue == 'edit_user_pass2' || $ElementIndexValue == 'user_email' || $ElementIndexValue == 'PHPSESSID' || $ElementIndexValue == 'edit' || $ElementIndexValue == 'edit_isAdmin' || $ElementIndexValue == 'edit_active' || $ElementIndexValue == 'edit_isAgent' || $ElementIndexValue == 'edit_limitListings' || $ElementIndexValue == 'edit_canEditSiteConfig' || $ElementIndexValue == 'edit_canMemberTemplate' || $ElementIndexValue == 'edit_canAgentTemplate' || $ElementIndexValue == 'edit_canListingTemplate' || $ElementIndexValue == 'edit_canViewLogs' || $ElementIndexValue == 'edit_canModerate' || $ElementIndexValue == 'edit_canFeatureListings' || $ElementIndexValue == 'edit_canPages' || $ElementIndexValue == 'edit_canVtour' || $ElementIndexValue == 'edit_canFiles' || $ElementIndexValue == 'edit_canUserFiles') {
                    // do nothing
                    continue;
                } else {
                    $stmtInsert->bindValue(':field_name', $ElementIndexValue);
                    $stmtInsert->bindValue(':user_id', $user_id, PDO::PARAM_INT);
                    if (is_array($ElementContents)) {
                        $feature_insert = implode('||', $ElementContents);
                        $stmtInsert->bindValue(':field_value', $feature_insert);
                    } else {
                        $value = $ElementContents;
                        if ($field_type == 'date' && $ElementContents != '') {
                            $value = $this->convertDate((string)$ElementContents);
                        }
                        $stmtInsert->bindValue(':field_value', $value);
                    }
                    try {
                        $stmtInsert->execute();
                    } catch (Exception $e) {
                        $error = $e->getMessage();
                        $log_api = new LogApi($this->dbh, $this->config);
                        $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sqlInsert]);
                        throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sqlInsert);
                    }
                } // end else
            }
        } // end while
        //success
        return true;
    }

    /**
     * @param string $date
     * @param string $format
     * @return int|false
     */
    private function convertDate(string $date, string $format = '%Y-%m-%d'): int|false
    {
        //Supported formats
        //%Y - year as a decimal number including the century
        //%m - month as a decimal number (range 01 to 12)
        //%d - day of the month as a decimal number (range 01 to 31)
        //%H - hour as a decimal number using a 24-hour clock (range 00 to 23)
        //%M - minute as a decimal number
        // Builds up date pattern from the given $format, keeping delimiters in place.
        if (!preg_match_all('/%([YmdHMp])([^%])*/', $format, $formatTokens, PREG_SET_ORDER)) {
            //return 'BAD FORMAT';
            return false;
        }
        $datePattern = '';
        foreach ($formatTokens as $formatToken) {
            if (isset($formatToken[2])) {
                $delimiter = preg_quote($formatToken[2], '/');
            } else {
                $delimiter = '';
            }

            $datePattern .= '(.*)' . $delimiter;
        }
        // Splits up the given $date
        if (!preg_match('/' . $datePattern . '/', $date, $dateTokens)) {
            //return 'BAD SPLIT';
            return false;
        }
        $dateSegments = [];
        $formatTokenCount = count($formatTokens);
        for ($i = 0; $i < $formatTokenCount; $i++) {
            $dateSegments[$formatTokens[$i][1]] = $dateTokens[$i + 1];
        }
        // Reformats the given $date into US English date format, suitable for strtotime()
        if ($dateSegments['Y'] && $dateSegments['m'] && $dateSegments['d']) {
            $dateReformated = $dateSegments['Y'] . '-' . $dateSegments['m'] . '-' . $dateSegments['d'];
        } else {
            return false;
        }
        if (isset($dateSegments['H']) && isset($dateSegments['M'])) {
            $dateReformated .= ' ' . $dateSegments['H'] . ':' . $dateSegments['M'];
        }

        return strtotime($dateReformated);
    }
}
