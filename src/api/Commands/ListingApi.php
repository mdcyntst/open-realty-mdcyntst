<?php

/**
 * This File Contains the Listing API Commands
 *
 * @package    Open-Realty
 * @subpackage API
 * @author     Ryan C. Bonham
 * @copyright  2010
 * @link       http://www.open-realty.com Open-Realty
 */

declare(strict_types=1);

namespace OpenRealty\Api\Commands;

use Exception;
use OpenRealty\Hooks;
use OpenRealty\ListingPages;
use OpenRealty\Login;
use OpenRealty\Misc;
use OpenRealty\PageUser;
use PDO;

/**
 * This is the listings API, it contains all api calls for creating and retrieving listing data.
 *
 * @package    Open-Realty
 * @subpackage API
 **/
class ListingApi extends BaseCommand
{
    protected array $OR_INT_FIELDS = ['listingsdb_id', 'listingsdb_pclass_id', 'userdb_id', 'listingsdb_title', 'listingsdb_expiration', 'listingsdb_notes', 'listingsdb_creation_date', 'listingsdb_last_modified', 'listingsdb_hit_count', 'listingsdb_featured', 'listingsdb_active', 'listingsdb_mlsexport', 'listing_seotitle'];

    /**
     * This API Command searches the listings
     *
     * @param array{
     *     parameters: array<array-key, mixed>,
     *     sortby?: string[],
     *     sorttype?: string[],
     *     offset?: integer,
     *     limit?: integer,
     *     count_only?: boolean
     *  } $data $data expects an array containing the following array keys.
     *
     * <ul>
     *      <li>$data['parameters'] - This is a REQUIRED array of the fields and the values we are
     *          searching for.</li>
     *      <li>$data['sortby'] - This is an optional array of fields to sort by.</li>
     *      <li>$data['sorttype'] - This is an optional array of sort types (ASC/DESC) to sort the
     *          sortby fields by.</li>
     *      <li>$data['offset'] - This is an optional integer of the number of listings to offset the
     *          search by. To use offset you must set a limit.</li>
     *      <li>$data['limit'] - This is an optional integer of the number of listings to limit the
     *          search by. 0 or unset will return all listings.</li>
     *      <li>$data['count_only'] - This is an optional integer flag 1/0, where 1 returns a record
     *          count only, defaults to 0 if not set. Usefull if doing limit/offset search for pagenation to
     *          get the inital full record count..</li>
     *  </ul>
     *
     * @return array{
     *     listing_count: integer,
     *     listings: integer[],
     *     info: array{
     *          process_time: string,
     *          query_time: string,
     *          total_time: string
     *     },
     *      sortby: string[],
     *      sorttype: string[],
     *     limit: integer
     * }  Array retruned will contain the following paramaters.
     *
     *  [listing_count] = Number of records found, if using a limit this is only the number of records that match your
     *  current limit/offset results.
     *  [listings] = Array of listing IDs.
     *  [info] = The info array contains benchmark information on the search, including process_time, query_time, and
     *  total_time
     *  [sortby] = Contains an array of fields that were used to sort the search results. Note if you are doing a
     *  CountONly search the sort is not actually used as this would just slow down the query.
     *  [sorttype] = Contains an array of the sorttype (ASC/DESC) used on the sortby fields.
     *  [limit] = Limit that was applied to the search.
     *
     * @throws \Exception
     **/
    public function search(array $data): array
    {
        $misc = new Misc($this->dbh, $this->config);

        $start_time = $misc->getmicrotime();
        $login = new Login($this->dbh, $this->config);

        //Check that required settings were passed
        if (!isset($data['parameters'])) {
            throw new Exception('parameters: correct_parameter_not_passed');
        }
        if (!isset($data['count_only'])) {
            $data['count_only'] = false;
        }
        if (!isset($data['limit'])) {
            $data['limit'] = 0;
        }
        $searchresultSQL = '';
        // Set Default Search Options
        $imageonly = false;
        $vtoursonly = false;
        $tablelist = [];
        $postalcode_dist_lat = '';
        $postalcode_dist_long = '';
        $postalcode_dist_dist = '';
        $latlong_dist_lat = '';
        $latlong_dist_long = '';
        $latlong_dist_dist = '';
        $city_dist_lat = '';
        $city_dist_long = '';
        $city_dist_dist = '';
        $login_status = $login->verifyPriv('Agent');
        $string_where_clause = '';
        $string_where_clause_nosort = '';
        if ($login_status !== true || !isset($data['parameters']['listingsdb_active'])) {
            //If we are not an agent only show active listings, or if user did not specify show only actives by default.
            $data['parameters']['listingsdb_active'] = 'yes';
        }
        if ($login_status !== true && $this->config['use_expiration']) {
            $data['parameters']['listingsdb_expiration_greater'] = time();
            unset($data['parameters']['listingsdb_expiration_less']);
        }
        //Get List of Listing Fields

        $fieldsApi = new FieldsApi($this->dbh, $this->config);
        $results = $fieldsApi->metadata(['resource' => 'listing']);
        $listingFieldTypes = [];
        foreach ($results['fields'] as $f) {
            if (!empty($f['field_name']) && !empty($f['field_type'])) {
                $listingFieldTypes[$f['field_name']] = $f['field_type'];
            }
        }
        //Loop through search paramaters
        $dbIntParams = [];
        $dbStrParams = [];
        foreach ($data['parameters'] as $k => $v) {
            //Search Listings By Agent
            if ($k == 'user_ID') {
                if ($v != '' && $v != 'Any Agent') {
                    if (is_array($v)) {
                        $sstring = '';
                        $i = 0;
                        foreach ($v as $u) {
                            if (empty($sstring)) {
                                $sstring .= $this->config['table_prefix'] . 'listingsduserdb_idb.userdb_id = :u' . $i;
                            } else {
                                $sstring .= ' OR ' . $this->config['table_prefix'] . 'listingsdb.userdb_id = :u' . $i;
                            }
                            $dbIntParams[':u' . $i] = (int)$u;
                            $i++;
                        }
                        if ($searchresultSQL != '') {
                            $searchresultSQL .= ' AND ';
                        }
                        $searchresultSQL .= '(' . $sstring . ')';
                    } elseif (is_numeric($v)) {
                        if ($searchresultSQL != '') {
                            $searchresultSQL .= ' AND ';
                        }
                        $dbIntParams[':u'] = (int)$v;
                        $searchresultSQL .= '(' . $this->config['table_prefix'] . 'listingsdb.userdb_id = :u)';
                    }
                }
            } elseif ($k == 'listingsdb_active') {
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                if ($v == 'no') {
                    $string_where_clause .= '(' . $this->config['table_prefix'] . 'listingsdb.listingsdb_active = \'no\')';
                    $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'listingsdb.listingsdb_active = \'no\')';
                } elseif ($v == 'any') {
                    $string_where_clause .= '(' . $this->config['table_prefix'] . 'listingsdb.listingsdb_active = \'yes\' or ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_active = \'no\')';
                    $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'listingsdb.listingsdb_active = \'yes\' or ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_active = \'no\')';
                } else {
                    $string_where_clause .= '(' . $this->config['table_prefix'] . 'listingsdb.listingsdb_active = \'yes\')';
                    $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'listingsdb.listingsdb_active = \'yes\')';
                }
            } elseif ($k == 'listingsdb_expiration_greater' && is_numeric($v)) {
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                $expiration = date("Y-m-d H:i:s", (int)$v);
                $dbStrParams[':listingsdb_expiration_greater'] = $expiration;
                $string_where_clause .= ' (listingsdb_expiration > :listingsdb_expiration_greater)';
                $string_where_clause_nosort .= ' (listingsdb_expiration > :listingsdb_expiration_greater)';
            } elseif ($k == 'listingsdb_expiration_less' && is_numeric($v)) {
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                $expiration = date("Y-m-d H:i:s", (int)$v);
                $dbStrParams[':listingsdb_expiration_less'] = $expiration;
                $string_where_clause .= ' (listingsdb_expiration < :listingsdb_expiration_less)';
                $string_where_clause_nosort .= ' (listingsdb_expiration < :listingsdb_expiration_less)';
            } elseif ($k == 'featuredOnly') {
                if ($v == 'yes') {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $searchresultSQL = $searchresultSQL . '(' . $this->config['table_prefix'] . 'listingsdb.listingsdb_featured = \'yes\')';
                }
            } elseif ($k == 'pclass') {
                $class_sql = '';
                if (is_array($v)) {
                    $i = 0;
                    foreach ($v as $class) {
                        // Ignore non numberic values
                        if (is_numeric($class)) {
                            if (!empty($class_sql)) {
                                $class_sql .= ' OR ';
                            }
                            $dbIntParams[':pclass' . $i] = (int)$class;
                            $class_sql .= $this->config['table_prefix'] . 'listingsdb.listingsdb_pclass_id = :pclass' . $i;
                        }
                        $i++;
                    }
                    if (!empty($class_sql)) {
                        if ($searchresultSQL != '') {
                            $searchresultSQL .= ' AND ';
                        }
                        $searchresultSQL .= '(' . $class_sql . ')';
                        //$searchresultSQL = $searchresultSQL . '(' . $class_sql . ') AND ' . $this->config['table_prefix_no_lang'] . 'classlistingsdb.listingsdb_id = ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id';
                        //$tablelist_fullname[] = $this->config['table_prefix_no_lang'] . 'classlistingsdb';
                    }
                }
            } elseif ($k == 'listing_id') {
                $listing_id = explode(',', $v);
                $i = 0;
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
                foreach ($listing_id as $id) {
                    if ($i == 0) {
                        $searchresultSQL .= '((' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id = :listing_id' . $i . ')';
                    } else {
                        $searchresultSQL .= ' OR (' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id = :listing_id' . $i . ')';
                    }
                    $dbIntParams[':listing_id' . $i] = (int)$id;
                    $i++;
                }
                $searchresultSQL .= ')';
            } elseif ($k == 'imagesOnly') {
                // Grab only listings with images if that is what we need.
                if ($v == 'yes') {
                    $imageonly = true;
                }
            } elseif ($k == 'vtoursOnly') {
                // Grab only listings with images if that is what we need.
                if ($v == 'yes') {
                    $vtoursonly = true;
                }
            } elseif ($k == 'listingsdb_title') {
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                $dbStrParams[':listingsdb_title'] = "%$v%";
                $string_where_clause .= '(' . $this->config['table_prefix'] . 'listingsdb.listingsdb_title LIKE :listingsdb_title)';
                $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'listingsdb.listingsdb_title LIKE :listingsdb_title)';
            } elseif ($k == 'listing_last_modified_equal' && is_numeric($v)) {
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
                $dateTime = date("Y-m-d H:i:s", (int)$v);
                $dbStrParams[':listing_last_modified_equal'] = $dateTime;
                $searchresultSQL .= ' listingsdb_last_modified = :listing_last_modified_equal';
            } elseif ($k == 'listing_last_modified_greater' && is_numeric($v)) {
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
                $dateTime = date("Y-m-d H:i:s", (int)$v);
                $dbStrParams[':listing_last_modified_greater'] = $dateTime;
                $searchresultSQL .= ' listingsdb_last_modified > :listing_last_modified_greater';
            } elseif ($k == 'listing_last_modified_less' && is_numeric($v)) {
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
                $dateTime = date("Y-m-d H:i:s", (int)$v);
                $dbStrParams[':listing_last_modified_less'] = $dateTime;
                $searchresultSQL .= ' listingsdb_last_modified < :listing_last_modified_less';
            } elseif ($k == 'listingsdb_creation_date_equal' && is_numeric($v)) {
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
                $dateTime = date("Y-m-d", (int)$v);
                $dbStrParams[':listingsdb_creation_date_equal'] = $dateTime;
                $searchresultSQL .= ' listingsdb_creation_date = :listingsdb_creation_date_equal';
            } elseif ($k == 'listingsdb_creation_date_greater' && is_numeric($v)) {
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
                $dateTime = date("Y-m-d", (int)$v);
                $dbStrParams[':listingsdb_creation_date_greater'] = $dateTime;
                $searchresultSQL .= ' listingsdb_creation_date > :listingsdb_creation_date_greater';
            } elseif ($k == 'listingsdb_creation_date_less' && is_numeric($v)) {
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
                $dateTime = date("Y-m-d", (int)$v);
                $dbStrParams[':listingsdb_creation_date_less'] = $dateTime;
                $searchresultSQL .= ' listingsdb_creation_date < :listingsdb_creation_date_less';
            } elseif ($k == 'listingsdb_creation_date_equal_days' && is_numeric($v)) {
                $v = intval($v);
                if ($v > 0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $time = mktime(0, 0, 0, (int)date('m'), (int)date('d') - $v, (int)date('Y'));
                    $dateTime = date("Y-m-d H:i:s", $time);
                    $dbStrParams[':listingsdb_creation_date_equal_days'] = $dateTime;
                    $searchresultSQL .= ' listingsdb_creation_date = :listingsdb_creation_date_equal_days';
                }
            } elseif ($k == 'listingsdb_creation_date_greater_days' && is_numeric($v)) {
                $v = intval($v);
                if ($v > 0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $time = mktime(0, 0, 0, (int)date('m'), (int)date('d') - $v, (int)date('Y'));
                    $dateTime = date("Y-m-d H:i:s", $time);
                    $dbStrParams[':listingsdb_creation_date_greater_days'] = $dateTime;
                    $searchresultSQL .= ' listingsdb_creation_date > :listingsdb_creation_date_greater_days';
                }
            } elseif ($k == 'listingsdb_creation_date_less_days' && is_numeric($v)) {
                $v = intval($v);
                if ($v > 0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $time = mktime(0, 0, 0, (int)date('m'), (int)date('d') - $v, (int)date('Y'));
                    $dateTime = date("Y-m-d H:i:s", $time);
                    $dbStrParams[':listingsdb_creation_date_less_days'] = $dateTime;
                    $searchresultSQL .= ' listingsdb_creation_date < :listingsdb_creation_date_less_days';
                }
            } elseif ($k == 'latlong_dist_lat' || $k == 'latlong_dist_long' || $k == 'latlong_dist_dist' && $v != '') {
                switch ($k) {
                    case 'latlong_dist_lat':
                        $latlong_dist_lat = (float)$v;
                        break;
                    case 'latlong_dist_long':
                        $latlong_dist_long = (float)$v;
                        break;
                    case 'latlong_dist_dist':
                        $latlong_dist_dist = (int)$v;
                        break;
                }
            } elseif ($k == 'postalcode_dist_code' && $v != '') {
                $sql = 'SELECT zipdist_latitude, zipdist_longitude 
						FROM ' . $this->config['table_prefix_no_lang'] . 'zipdist 
						WHERE zipdist_zipcode = :postalcode';

                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
                try {
                    $stmt->execute([':postalcode' => $v]);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }
                $record = $stmt->fetch();
                $postalcode_dist_lat = floatval($record['zipdist_latitude']);
                $postalcode_dist_long = floatval($record['zipdist_longitude']);
            } elseif ($k == 'postalcode_dist_dist' && $v != '') {
                $postalcode_dist_dist = floatval($v);
            } elseif ($k == 'city_dist_code' && $v != '') {
                $sql = 'SELECT zipdist_latitude, zipdist_longitude FROM ' . $this->config['table_prefix_no_lang'] . 'zipdist WHERE zipdist_cityname = :city';
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
                try {
                    $stmt->execute([':city' => $v]);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }
                $record = $stmt->fetch();
                $city_dist_lat = floatval($record['zipdist_latitude']);
                $city_dist_long = floatval($record['zipdist_longitude']);
            } elseif ($k == 'city_dist_dist' && $v != '') {
                $city_dist_dist = $v;
            } elseif (
                $v != '' && $k != 'listingID' && $k != 'postalcode_dist_code' && $k != 'postalcode_dist_dist' && $k != 'city_dist_code' && $k != 'city_dist_dist'
                && $k != 'latlong_dist_dist' && $k != 'cur_page' && $k != 'action' && $k != 'PHPSESSID'
                && $k != 'sortby' && $k != 'sorttype' && $k != 'printer_friendly' && $k != 'template' && $k != 'x' && $k != 'y' && $k != 'popup'
            ) {
                if (!is_array($v)) {
                    //Handle NULL/NOTNULL Searches
                    if (str_ends_with($k, '-NULL') && $v == '1') {
                        $subk = substr($k, 0, -5);
                        if (in_array($subk, $listingFieldTypes)) {
                            if ($searchresultSQL != '') {
                                $searchresultSQL .= ' AND ';
                            }
                            $searchresultSQL .= "(`$subk`.listingsdbelements_field_name = '$subk' AND (`$subk`.listingsdbelements_field_value IS NULL OR `$subk`.listingsdbelements_field_value = ''))";
                            if (!in_array($subk, $tablelist)) {
                                $tablelist[] = $subk;
                            }
                        }
                    } elseif (str_ends_with($k, '-NOTNULL') && $v == '1') {
                        $subk = substr($k, 0, -8);
                        if (in_array($subk, $listingFieldTypes)) {
                            if ($searchresultSQL != '') {
                                $searchresultSQL .= ' AND ';
                            }
                            $searchresultSQL .= "(`$subk`.listingsdbelements_field_name = '$subk' AND (`$subk`.listingsdbelements_field_value IS NOT NULL  AND `$subk`.listingsdbelements_field_value <> ''))";
                            if (!in_array($subk, $tablelist)) {
                                $tablelist[] = $subk;
                            }
                        }
                    } elseif (str_ends_with($k, '-max')) {
                        // Handle Min/Max Searches
                        $subk = substr($k, 0, -4);
                        $k = str_replace('-', '_', $k);
                        if (array_key_exists($subk, $listingFieldTypes)) {
                            if ($searchresultSQL != '') {
                                $searchresultSQL .= ' AND ';
                            }
                            $dbStrParams[':field_' . $k] = $subk;
                            $dbStrParams[':' . $k] = $v;
                            $field_type = $listingFieldTypes[$subk];

                            if ($field_type == 'lat' || $field_type == 'long') {
                                $searchresultSQL .= "(`$subk`.listingsdbelements_field_name = :field_" . $k . " AND CAST(`$subk`.listingsdbelements_field_value as DECIMAL(13,7)) <= :" . $k . ")";
                            } elseif ($field_type == 'decimal') {
                                $searchresultSQL .= "(`$subk`.listingsdbelements_field_name = :field_" . $k . " AND CAST(`$subk`.listingsdbelements_field_value as DECIMAL(64,6)) <= :" . $k . ")";
                            } else {
                                $searchresultSQL .= "(`$subk`.listingsdbelements_field_name = :field_" . $k . " AND CAST(`$subk`.listingsdbelements_field_value as signed) <= :" . $k . ")";
                            }

                            if (!in_array($subk, $tablelist)) {
                                $tablelist[] = $subk;
                            }
                        }
                    } elseif (str_ends_with($k, '-min')) {
                        $subk = substr($k, 0, -4);
                        $k = str_replace('-', '_', $k);
                        if (array_key_exists($subk, $listingFieldTypes)) {
                            if ($searchresultSQL != '') {
                                $searchresultSQL .= ' AND ';
                            }
                            $dbStrParams[':field_' . $k] = $subk;
                            $dbStrParams[':' . $k] = $v;
                            $field_type = $listingFieldTypes[$subk];

                            if ($field_type == 'lat' || $field_type == 'long') {
                                $searchresultSQL .= "(`$subk`.listingsdbelements_field_name = :field_" . $k . " AND CAST(`$subk`.listingsdbelements_field_value as DECIMAL(13,7)) >= :" . $k . ")";
                            } elseif ($field_type == 'decimal') {
                                $searchresultSQL .= "(`$subk`.listingsdbelements_field_name = :field_" . $k . " AND CAST(`$subk`.listingsdbelements_field_value as DECIMAL(64,6)) >= :" . $k . ")";
                            } else {
                                $searchresultSQL .= "(`$subk`.listingsdbelements_field_name = :field_" . $k . " AND CAST(`$subk`.listingsdbelements_field_value as signed) >= :" . $k . ")";
                            }
                            if (!in_array($subk, $tablelist)) {
                                $tablelist[] = $subk;
                            }
                        }
                    } elseif (str_ends_with($k, '-maxdate')) {
                        $subk = substr($k, 0, -8);
                        $k = str_replace('-', '_', $k);
                        if (array_key_exists($subk, $listingFieldTypes)) {
                            if ($searchresultSQL != '') {
                                $searchresultSQL .= ' AND ';
                            }
                            $date = $this->convertDate($v);
                            $dbStrParams[':field_' . $k] = $subk;
                            $dbStrParams[':' . $k] = $date;
                            $searchresultSQL .= "(`$subk`.listingsdbelements_field_name = :field_" . $k . " AND `$subk`.listingsdbelements_field_value <= :" . $k . ")";
                            if (!in_array($subk, $tablelist)) {
                                $tablelist[] = $subk;
                            }
                        }
                    } elseif (str_ends_with($k, '-mindate')) {
                        $subk = substr($k, 0, -8);
                        $k = str_replace('-', '_', $k);
                        if (array_key_exists($subk, $listingFieldTypes)) {
                            if ($searchresultSQL != '') {
                                $searchresultSQL .= ' AND ';
                            }
                            $date = $this->convertDate($v);
                            $dbStrParams[':field_' . $k] = $subk;
                            $dbStrParams[':' . $k] = $date;
                            $searchresultSQL .= "(`$subk`.listingsdbelements_field_name = :field_" . $k . " AND `$subk`.listingsdbelements_field_value >= :" . $k . ")";
                            if (!in_array($subk, $tablelist)) {
                                $tablelist[] = $subk;
                            }
                        }
                    } elseif (str_ends_with($k, '-date')) {
                        $subk = substr($k, 0, -8);
                        $k = str_replace('-', '_', $k);
                        if (array_key_exists($subk, $listingFieldTypes)) {
                            if ($searchresultSQL != '') {
                                $searchresultSQL .= ' AND ';
                            }
                            $date = $this->convertDate($v);
                            $dbStrParams[':field_' . $k] = $subk;
                            $dbStrParams[':' . $k] = $date;
                            $searchresultSQL .= "(`$subk`.listingsdbelements_field_name = :field_" . $k . " AND `$subk`.listingsdbelements_field_value = :" . $k . ")";
                            if (!in_array($subk, $tablelist)) {
                                $tablelist[] = $subk;
                            }
                        }
                    } elseif ($k == 'searchtext') {
                        if ($searchresultSQL != '') {
                            $searchresultSQL .= ' AND ';
                        }
                        $dbStrParams[':searchtext_field'] = "%$v%";
                        $dbStrParams[':searchtext_title'] = "%$v%";
                        $searchresultSQL .= "((`$k`.listingsdbelements_field_value like :searchtext_field) OR (listingsdb_title like :searchtext_title))";
                        $tablelist[] = $k;
                    } else {
                        $safe_k = str_replace('-', '_', (string)$k);
                        if (array_key_exists($k, $listingFieldTypes)) {
                            if ($searchresultSQL != '') {
                                $searchresultSQL .= ' AND ';
                            }
                            $dbStrParams[':field_' . $safe_k] = $k;
                            $dbStrParams[':' . $safe_k] = $v;
                            $searchresultSQL .= "(`$k`.listingsdbelements_field_name = :field_$safe_k AND `$k`.listingsdbelements_field_value = :$safe_k)";
                            $tablelist[] = $k;
                        }
                    }
                } else {
                    // Make Sure Array is not empty
                    if (!empty($v)) {
                        if (str_ends_with($k, '_or')) {
                            $subk = substr($k, 0, -3);
                            if (array_key_exists($subk, $listingFieldTypes)) {
                                if ($searchresultSQL != '') {
                                    $searchresultSQL .= ' AND ';
                                }
                                $dbStrParams[':field_' . $k] = $subk;
                                $dbStrParams[':' . $k] = $v;
                                $searchresultSQL .= "(`$subk`.listingsdbelements_field_name = :field_' . $k AND (";
                                $vitem_count = 0;
                                foreach ($v as $vitem) {
                                    if ($vitem != '') {
                                        $dbStrParams[':' . $k . $vitem_count] = "%$vitem%";
                                        if ($vitem_count != 0) {
                                            $searchresultSQL .= " OR `$subk`.listingsdbelements_field_value LIKE :$k$vitem_count";
                                        } else {
                                            $searchresultSQL .= " `$subk`.listingsdbelements_field_value LIKE :$k$vitem_count";
                                        }
                                        $vitem_count++;
                                    }
                                }
                                $searchresultSQL .= '))';
                                $tablelist[] = $subk;
                            }
                        } else {
                            if (array_key_exists($k, $listingFieldTypes)) {
                                if ($searchresultSQL != '') {
                                    $searchresultSQL .= ' AND ';
                                }
                                $dbStrParams[':field_' . $k] = $k;

                                $searchresultSQL .= "(`$k`.listingsdbelements_field_name = :field_$k AND (";
                                $vitem_count = 0;
                                foreach ($v as $vitem) {
                                    if ($vitem != '') {
                                        $dbStrParams[':' . $k . $vitem_count] = "%$vitem%";
                                        if ($vitem_count != 0) {
                                            $searchresultSQL .= ") AND (`$k`.listingsdbelements_field_value LIKE :$k$vitem_count";
                                        } else {
                                            $searchresultSQL .= "`$k`.listingsdbelements_field_value LIKE :$k$vitem_count";
                                        }
                                        $vitem_count++;
                                    }
                                }
                                $searchresultSQL .= '))';
                                $tablelist[] = $k;
                            }
                        }
                    }
                }
            }
        }
        if ($postalcode_dist_lat != '' && $postalcode_dist_long != '' && $postalcode_dist_dist != '') {
            $sql = 'SELECT zipdist_zipcode
					FROM ' . $this->config['table_prefix_no_lang'] . 'zipdist
					WHERE (POW((69.1*(zipdist_longitude-"' . $postalcode_dist_long . '")*cos(' . $postalcode_dist_lat . '/57.3)),"2")+POW((69.1*(zipdist_latitude-"' . $postalcode_dist_lat . '")),"2"))<(' . $postalcode_dist_dist . '*' . $postalcode_dist_dist . ') ';
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $recordSet = $stmt->fetchAll();
            $zipcodes = [];
            foreach ($recordSet as $record) {
                $zipcodes[] = $record['zipdist_zipcode'];
            }
            $pc_field_name = $this->config['map_zip'];
            // Build Search Query
            // Make Sure Array is not empty
            $use = false;
            $comma_separated = implode(' ', $zipcodes);
            if (trim($comma_separated) != '') {
                $use = true;
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
            }
            if ($use === true) {
                $searchresultSQL .= "(`$pc_field_name`.listingsdbelements_field_name = '$pc_field_name' AND (";
                $vitem_count = 0;
                foreach ($zipcodes as $vitem) {
                    $dbStrParams[':' . $pc_field_name . $vitem_count] = $vitem;
                    if ($vitem != '') {
                        if ($vitem_count != 0) {
                            $searchresultSQL .= " OR `$pc_field_name`.listingsdbelements_field_value = :$pc_field_name$vitem_count";
                        } else {
                            $searchresultSQL .= " `$pc_field_name`.listingsdbelements_field_value = :$pc_field_name$vitem_count";
                        }
                        $vitem_count++;
                    }
                }
                $searchresultSQL .= '))';
                $tablelist[] = $pc_field_name;
            }
        }
        if ($city_dist_lat != '' && $city_dist_long != '' && $city_dist_dist != '') {
            $sql = "SELECT zipdist_zipcode FROM " . $this->config['table_prefix_no_lang'] . "zipdist WHERE (POW((69.1*(zipdist_longitude-\"$city_dist_long\")*cos($city_dist_lat/57.3)),\"2\")+POW((69.1*(zipdist_latitude-\"$city_dist_lat\")),\"2\"))<($city_dist_dist*$city_dist_dist) ";
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $recordSet = $stmt->fetchAll();
            $zipcodes = [];
            foreach ($recordSet as $record) {
                $zipcodes[] = $record['zipdist_zipcode'];
            }
            $pc_field_name = $this->config['map_zip'];
            // Build Search Query
            // Make Sure Array is not empty
            $use = false;
            $comma_separated = implode(' ', $zipcodes);
            if (trim($comma_separated) != '') {
                $use = true;
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
            }
            if ($use === true) {
                $searchresultSQL .= "(`$pc_field_name`.listingsdbelements_field_name = '$pc_field_name' AND (";
                $vitem_count = 0;
                foreach ($zipcodes as $vitem) {
                    $dbStrParams[':' . $pc_field_name . $vitem_count] = $vitem;
                    if ($vitem != '') {
                        if ($vitem_count != 0) {
                            $searchresultSQL .= " OR `$pc_field_name`.listingsdbelements_field_value = :$pc_field_name$vitem_count";
                        } else {
                            $searchresultSQL .= " `$pc_field_name`.listingsdbelements_field_value = :$pc_field_name$vitem_count";
                        }
                        $vitem_count++;
                    }
                }
                $searchresultSQL .= '))';
                $tablelist[] = $pc_field_name;
            }
        }
        //Lat Long Distance
        if ($latlong_dist_lat != '' && $latlong_dist_long != '' && $latlong_dist_dist != '') {
            /*
             max_lon = lon1 + arcsin(sin(D/R)/cos(lat1))
             min_lon = lon1 - arcsin(sin(D/R)/cos(lat1))
             max_lat = lat1 + (180/pi)(D/R)
             min_lat = lat1 - (180/pi)(D/R)
             $max_long = $latlong_dist_long + asin(sin($latlong_dist_dist/3956)/cos($latlong_dist_lat));
             $min_long = $latlong_dist_long - asin(sin($latlong_dist_dist/3956)/cos($latlong_dist_lat));
             $max_lat = $latlong_dist_lat + (180/pi())*($latlong_dist_dist/3956);
             $min_lat = $latlong_dist_lat - (180/pi())*($latlong_dist_dist/3956);
             /*
             Latitude:
             Apparently a degree of latitude expressed in miles does
             vary slighty by latitude

             (http://www.ncgia.ucsb.edu/education/curricula/giscc/units/u014/tables/table01.html)
             but for our purposes, I suggest we use 1 degree latitude

             = 69 miles.



             Longitude:
             This is more tricky one since it varies by latitude
             (http://www.ncgia.ucsb.edu/education/curricula/giscc/units/u014/tables/table02.html).
             The

             simplest formula seems to be:
             1 degree longitude expressed in miles = cos (latitude) *
             69.17 miles
             */
            //Get Correct Milage for ong based on lat.
            $cos_long = 69.17;
            if ($latlong_dist_lat >= 10) {
                $cos_long = 68.13;
            }
            if ($latlong_dist_lat >= 20) {
                $cos_long = 65.03;
            }
            if ($latlong_dist_lat >= 30) {
                $cos_long = 59.95;
            }
            if ($latlong_dist_lat >= 40) {
                $cos_long = 53.06;
            }
            if ($latlong_dist_lat >= 50) {
                $cos_long = 44.55;
            }
            if ($latlong_dist_lat >= 60) {
                $cos_long = 34.67;
            }
            if ($latlong_dist_lat >= 70) {
                $cos_long = 23.73;
            }
            if ($latlong_dist_lat >= 80) {
                $cos_long = 12.05;
            }
            if ($latlong_dist_lat >= 90) {
                $cos_long = 0;
            }
            $max_long = $latlong_dist_long + $latlong_dist_dist / (cos(deg2rad($latlong_dist_lat)) * $cos_long);
            $min_long = $latlong_dist_long - $latlong_dist_dist / (cos(deg2rad($latlong_dist_lat)) * $cos_long);
            $max_lat = $latlong_dist_lat + $latlong_dist_dist / 69;
            $min_lat = $latlong_dist_lat - $latlong_dist_dist / 69;
            //
            if ($max_lat < $min_lat) {
                $max_lat2 = $min_lat;
                $min_lat = $max_lat;
                $max_lat = $max_lat2;
            }
            if ($max_long < $min_long) {
                $max_long2 = $min_long;
                $min_long = $max_long;
                $max_long = $max_long2;
            }
            $lat_field = array_search('lat', $listingFieldTypes, true);
            $long_field = array_search('long', $listingFieldTypes, true);
            if ($lat_field != '' && $long_field != '') {
                $tablelist[] = $lat_field;
                $tablelist[] = $long_field;
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
                $searchresultSQL .= "(`$lat_field`.listingsdbelements_field_name = '$lat_field' AND `$lat_field`.listingsdbelements_field_value+0 <= '$max_lat')";
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
                $searchresultSQL .= "(`$lat_field`.listingsdbelements_field_name = '$lat_field' AND `$lat_field`.listingsdbelements_field_value+0 >= '$min_lat')";
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
                $searchresultSQL .= "(`$long_field`.listingsdbelements_field_name = '$long_field' AND `$long_field`.listingsdbelements_field_value+0 <= '$max_long')";
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
                $searchresultSQL .= "(`$long_field`.listingsdbelements_field_name = '$long_field' AND `$long_field`.listingsdbelements_field_value+0 >= '$min_long')";
            }
        }
        // Handle Sorting
        // sort the listings
        // this is the main SQL that grabs the listings
        // basic sort by title..
        $group_order_text = '';
        $sortby_array = [];
        $sorttype_array = [];
        //Set array
        if (!empty($data['sortby'])) {
            $sortby_array = $data['sortby'];
        }
        if (!empty($data['sorttype'])) {
            $sorttype_array = $data['sorttype'];
        }
        $sort_text = '';
        $order_text = '';
        $tablelist_nosort = $tablelist;
        $sort_count = count($sortby_array);
        $select_fields = [];
        for ($x = 0; $x < $sort_count; $x++) {
            if (!isset($sorttype_array[$x])) {
                $sorttype_array[$x] = '';
            } elseif ($sorttype_array[$x] != 'ASC' && $sorttype_array[$x] != 'DESC') {
                $sorttype_array[$x] = '';
            }
            if ($sortby_array[$x] == 'listingsdb_id') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY listingsdb_id ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',listingsdb_id ' . $sorttype_array[$x];
                }
            } elseif ($sortby_array[$x] == 'listingsdb_title') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY listingsdb_title ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',listingsdb_title ' . $sorttype_array[$x];
                }
                $select_fields[] = 'listingsdb_title';
            } elseif ($sortby_array[$x] == 'listingsdb_hit_count') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY listingsdb_hit_count ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',listingsdb_hit_count ' . $sorttype_array[$x];
                }
                $select_fields[] = 'listingsdb_hit_count';
            } elseif ($sortby_array[$x] == 'random') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY rand() ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',rand() ' . $sorttype_array[$x];
                }
            } elseif ($sortby_array[$x] == 'listingsdb_featured') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY listingsdb_featured ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',listingsdb_featured ' . $sorttype_array[$x];
                }
                $select_fields[] = 'listingsdb_featured';
            } elseif ($sortby_array[$x] == 'listingsdb_last_modified') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY listingsdb_last_modified ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',listingsdb_last_modified ' . $sorttype_array[$x];
                }
                $select_fields[] = 'listingsdb_last_modified';
            } elseif ($sortby_array[$x] == 'pclass') {
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
                //$searchresultSQL .=  $this->config['table_prefix_no_lang'] . 'classlistingsdb.listingsdb_id = ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id AND '. $this->config['table_prefix_no_lang'] . 'classlistingsdb.class_id = '.$this->config['table_prefix'].'class.class_id ';
                //$tablelist_fullname[] = $this->config['table_prefix_no_lang'] . 'classlistingsdb';
                //$tablelist_fullname[] = $this->config['table_prefix'].'class';
                if ($order_text == '') {
                    $order_text .= 'ORDER BY ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_pclass_id ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',' . $this->config['table_prefix'] . 'listingsdb.listingsdb_pclass_id ' . $sorttype_array[$x];
                }
                $select_fields[] = $this->config['table_prefix'] . 'listingsdb.listingsdb_pclass_id';
            } else {
                $sort_by_field = $sortby_array[$x];
                if (array_key_exists($sort_by_field, $listingFieldTypes)) {
                    $field_type = $listingFieldTypes[$sort_by_field];
                    $tablelist[] = 'sort' . $x;
                    $select_fields[] = 'sort' . $x . '.listingsdbelements_field_value';
                    if ($field_type == 'price' || $field_type == 'number') {
                        $sort_text .= 'AND (sort' . $x . '.listingsdbelements_field_name = \'' . $sort_by_field . '\') ';

                        if ($order_text == '') {
                            $order_text .= ' ORDER BY CAST(sort' . $x . '.listingsdbelements_field_value as signed) ' . $sorttype_array[$x];
                            $group_order_text .= ',sort' . $x . '.listingsdbelements_field_value';
                        } else {
                            $order_text .= ',CAST(sort' . $x . '.listingsdbelements_field_value as signed) ' . $sorttype_array[$x];
                            $group_order_text .= ',sort' . $x . '.listingsdbelements_field_value';
                        }
                    } elseif ($field_type == 'lat' || $field_type == 'long') {
                        $sort_text .= 'AND (sort' . $x . '.listingsdbelements_field_name = \'' . $sort_by_field . '\') ';

                        if ($order_text == '') {
                            $order_text .= ' ORDER BY CAST(sort' . $x . '.listingsdbelements_field_value as decimal(13,7)) ' . $sorttype_array[$x];
                            $group_order_text .= ',sort' . $x . '.listingsdbelements_field_value';
                        } else {
                            $order_text .= ',CAST(sort' . $x . '.listingsdbelements_field_value as signed) ' . $sorttype_array[$x];
                            $group_order_text .= ',sort' . $x . '.listingsdbelements_field_value';
                        }
                    } elseif ($field_type == 'decimal') {
                        $sort_text .= 'AND (sort' . $x . '.listingsdbelements_field_name = \'' . $sort_by_field . '\') ';
                        if ($order_text == '') {
                            $order_text .= ' ORDER BY CAST(sort' . $x . '.listingsdbelements_field_value as decimal(64,6)) ' . $sorttype_array[$x];
                            $group_order_text .= ',sort' . $x . '.listingsdbelements_field_value';
                        } else {
                            $order_text .= ',CAST(sort' . $x . '.listingsdbelements_field_value as signed) ' . $sorttype_array[$x];
                            $group_order_text .= ',sort' . $x . '.listingsdbelements_field_value';
                        }
                    } else {
                        $sort_text .= 'AND (sort' . $x . '.listingsdbelements_field_name = \'' . $sort_by_field . '\') ';
                        if ($order_text == '') {
                            $order_text .= ' ORDER BY sort' . $x . '.listingsdbelements_field_value ' . $sorttype_array[$x];
                        } else {
                            $order_text .= ', sort' . $x . '.listingsdbelements_field_value ' . $sorttype_array[$x];
                        }
                        $group_order_text .= ',sort' . $x . '.listingsdbelements_field_value';
                    }
                }
            }
        }
        $group_order_text = $group_order_text . ' ' . $order_text;
        $select_fields_text = '';
        if (count($select_fields) > 0) {
            $select_fields_text = ',' . join(",", $select_fields);
        }

        if ($imageonly || $vtoursonly) {
            $order_text = 'GROUP BY ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id, ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_title ' . $group_order_text;
        }
        //        if ($DEBUG_SQL) {
        //            echo '<strong>Sort Type SQL:</strong> ' . $sql_sort_type . '<br />';
        //            echo '<strong>Sort Text:</strong> ' . $sort_text . '<br />';
        //            echo '<strong>Order Text:</strong> ' . $order_text . '<br />';
        //        }

        //$guidestring_with_sort = $guidestring_with_sort . $guidestring;
        // End of Sort
        $arrayLength = count($tablelist);

        $string_table_list = '';
        for ($i = 0; $i < $arrayLength; $i++) {
            $string_table_list .= ' ,' . $this->config['table_prefix'] . 'listingsdbelements `' . $tablelist[$i] . '`';
        }
        $arrayLength = count($tablelist_nosort);
        $string_table_list_no_sort = '';
        for ($i = 0; $i < $arrayLength; $i++) {
            $string_table_list_no_sort .= ' ,' . $this->config['table_prefix'] . 'listingsdbelements `' . $tablelist[$i] . '`';
        }

        $arrayLength = count($tablelist);
        for ($i = 0; $i < $arrayLength; $i++) {
            if ($string_where_clause != '') {
                $string_where_clause .= ' AND ';
            }
            $string_where_clause .= ' (' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id = `' . $tablelist[$i] . '`.listingsdb_id)';
        }
        $arrayLength = count($tablelist_nosort);
        for ($i = 0; $i < $arrayLength; $i++) {
            if ($string_where_clause_nosort != '') {
                $string_where_clause_nosort .= ' AND ';
            }
            $string_where_clause_nosort .= ' (' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id = `' . $tablelist[$i] . '`.listingsdb_id)';
        }

        if ($imageonly) {
            $searchSQL = 'SELECT distinct(' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id), ' . $this->config['table_prefix'] . 'listingsdb.userdb_id,
						' . $this->config['table_prefix'] . 'listingsdb.listingsdb_title ' . $select_fields_text . ' FROM ' . $this->config['table_prefix'] . 'listingsdb,
						' . $this->config['table_prefix'] . 'listingsimages ' . $string_table_list . '
						WHERE ' . $string_where_clause . '
						AND (' . $this->config['table_prefix'] . 'listingsimages.listingsdb_id = ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id)';

            $searchSQLCount = 'SELECT COUNT(distinct(' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id)) as total_listings 
						FROM ' . $this->config['table_prefix'] . 'listingsdb, ' . $this->config['table_prefix'] . 'listingsimages ' . $string_table_list_no_sort . '
						WHERE ' . $string_where_clause_nosort . '
						AND (' . $this->config['table_prefix'] . 'listingsimages.listingsdb_id = ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id)';
        } elseif ($vtoursonly) {
            $searchSQL = 'SELECT distinct(' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id), ' . $this->config['table_prefix'] . 'listingsdb.userdb_id,
						' . $this->config['table_prefix'] . 'listingsdb.listingsdb_title ' . $select_fields_text . '
						FROM ' . $this->config['table_prefix'] . 'listingsdb, ' . $this->config['table_prefix'] . 'listingsvtours ' . $string_table_list . '
						WHERE ' . $string_where_clause . '
						AND (' . $this->config['table_prefix'] . 'listingsvtours.listingsdb_id = ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id)';

            $searchSQLCount = 'SELECT COUNT(distinct(' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id)) as total_listings 
						FROM ' . $this->config['table_prefix'] . 'listingsdb, ' . $this->config['table_prefix'] . 'listingsvtours ' . $string_table_list_no_sort . '
						WHERE ' . $string_where_clause_nosort . '
						AND (' . $this->config['table_prefix'] . 'listingsvtours.listingsdb_id = ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id) ';
        } else {
            $searchSQL = 'SELECT distinct(' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id) ' . $select_fields_text . '
						FROM ' . $this->config['table_prefix'] . 'listingsdb ' . $string_table_list . '
						WHERE ' . $string_where_clause;
            $searchSQLCount = 'SELECT COUNT(distinct(' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id)) as total_listings 
						FROM ' . $this->config['table_prefix'] . 'listingsdb ' . $string_table_list_no_sort . ' 
						WHERE ' . $string_where_clause_nosort;
        }
        if ($searchresultSQL != '') {
            $searchSQL .= ' AND ' . $searchresultSQL;
            $searchSQLCount .= ' AND ' . $searchresultSQL;
        }

        $sql = $searchSQL . ' ' . $sort_text . ' ' . $order_text;
        if ($data['count_only']) {
            $sql = $searchSQLCount;
        }
        //$searchSQLCount = $searchSQLCount;
        // We now have a complete SQL Query. Now grab the results
        $process_time = $misc->getmicrotime();
        if (isset($data['offset']) && $data['limit'] > 0) {
            $sql = $sql . ' LIMIT :offset, :limit';
            $dbIntParams[':offset'] = $data['offset'];
            $dbIntParams[':limit'] = $data['limit'];
        }
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->listing->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        try {
            foreach ($dbStrParams as $x => $v) {
                $stmt->bindValue($x, $v);
            }
            foreach ($dbIntParams as $x => $v) {
                $stmt->bindValue($x, $v, PDO::PARAM_INT);
            }
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->listing->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $recordSet = [];
        $listing_count = 0;
        if ($data['count_only']) {
            $listing_count = (int)$stmt->fetchColumn();
        } else {
            $recordSet = $stmt->fetchAll();
        }
        $query_time = $misc->getmicrotime();
        $query_time = $query_time - $process_time;
        $listings_found = [];
        if (!$data['count_only']) {
            $listing_count = count($recordSet);
            foreach ($recordSet as $record) {
                $listings_found[] = (int)$record['listingsdb_id'];
            }
        }
        $process_time = $process_time - $start_time;
        $total_time = $misc->getmicrotime();
        $total_time = $total_time - $start_time;
        $info = [];
        $info['process_time'] = sprintf('%.3f', $process_time);
        $info['query_time'] = sprintf('%.3f', $query_time);
        $info['total_time'] = sprintf('%.3f', $total_time);
        return ['listing_count' => $listing_count, 'listings' => $listings_found, 'info' => $info, 'sortby' => $sortby_array, 'sorttype' => $sorttype_array, 'limit' => $data['limit']];
    }

    /**
     * @param string $date
     * @param string $format
     * @return int|false
     */
    private function convertDate(string $date, string $format = '%Y-%m-%d'): int|false
    {
        //Supported formats
        //%Y - year as a decimal number including the century
        //%m - month as a decimal number (range 01 to 12)
        //%d - day of the month as a decimal number (range 01 to 31)
        //%H - hour as a decimal number using a 24-hour clock (range 00 to 23)
        //%M - minute as a decimal number
        // Builds up date pattern from the given $format, keeping delimiters in place.
        //echo 'Date: "'.$date.'"';
        //echo 'Format "'.$format.'"';
        if (!preg_match_all('/%([YmdHMp])([^%])*/', $format, $formatTokens, PREG_SET_ORDER)) {
            //return 'BAD FORMAT';
            return false;
        }
        $datePattern = '';
        //echo '<pre>'.print_r($formatTokens,TRUE).'</pre>';
        foreach ($formatTokens as $formatToken) {
            if (isset($formatToken[2])) {
                $delimiter = preg_quote($formatToken[2], '/');
            } else {
                $delimiter = '';
            }

            $datePattern .= '(.*)' . $delimiter;
        }
        // Splits up the given $date
        if (!preg_match('/' . $datePattern . '/', $date, $dateTokens)) {
            //return 'BAD SPLIT';
            return false;
        }
        $dateSegments = [];
        $formatTokenCount = count($formatTokens);
        for ($i = 0; $i < $formatTokenCount; $i++) {
            $dateSegments[$formatTokens[$i][1]] = $dateTokens[$i + 1];
        }
        // Reformats the given $date into US English date format, suitable for strtotime()
        if ($dateSegments['Y'] && $dateSegments['m'] && $dateSegments['d']) {
            $dateReformated = $dateSegments['Y'] . '-' . $dateSegments['m'] . '-' . $dateSegments['d'];
        } else {
            //return 'BAD DATE';
            return false;
        }
        if (isset($dateSegments['H']) && isset($dateSegments['M'])) {
            $dateReformated .= ' ' . $dateSegments['H'] . ':' . $dateSegments['M'];
        }

        return strtotime($dateReformated);
    }

    /**
     * @param array{
     *          function: string,
     *          field_name:string,
     *          pclass?: int[],
     *          format?: bool
     * } $data
     * @return array|float[]|int[]
     * @throws \Exception
     */
    public function getStatistics(array $data): array
    {
        $misc = new Misc($this->dbh, $this->config);
        $class_sql = '';
        $dbIntParams = [];
        if (isset($data['pclass'])) {
            $i = 0;
            foreach ($data['pclass'] as $class_id) {
                if ($class_id > 0) {
                    $dbIntParams[':pclass' . $i] = $class_id;
                    $class_sql .= ' AND ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_pclass_id = :pclass' . $i;
                }
                $i++;
            }
        }
        if (!isset($data['function']) || !in_array($data['function'], ['max', 'min', 'avg', 'median'])) {
            throw new Exception('function: correct_parameter_not_passed');
        }
        if (!isset($data['field_name'])) {
            throw new Exception('field_name: correct_parameter_not_passed');
        }
        $numformat = false;
        if (isset($data['format'])) {
            $numformat = $data['format'];
        }
        //Get Field List
        $fieldsApi = new FieldsApi($this->dbh, $this->config);
        $results = $fieldsApi->metadata(['resource' => 'listing']);
        $listingFieldTypes = [];
        foreach ($results['fields'] as $f) {
            if (!empty($f['field_name']) && !empty($f['field_type'])) {
                $listingFieldTypes[$f['field_name']] = $f['field_type'];
            }
        }
        $fieldName = $data['field_name'];
        $field_type = null;
        if (array_key_exists($fieldName, $listingFieldTypes)) {
            $field_type = $listingFieldTypes[$fieldName];
        }
        if ($field_type == 'decimal') {
            $field_sql = 'listingsdbelements_field_value+0';
        } else {
            $field_sql = 'CAST(listingsdbelements_field_value as signed)';
        }
        if (!is_null($field_type)) {
            $field_list = $this->config['table_prefix'] . 'listingsdbelements, ' . $this->config['table_prefix'] . 'listingsdb WHERE
			' . $this->config['table_prefix'] . 'listingsdbelements.listingsdb_id = ' . $this->config['table_prefix'] . 'listingsdb.listingsdb_id';
            switch ($data['function']) {
                case 'max':
                case 'min':
                    $sql = 'SELECT max(' . $field_sql . ') as max, min(' . $field_sql . ') as min
							FROM ' . $field_list . '
							AND listingsdbelements_field_name = :field_name ' . $class_sql;
                    $stmt = $this->dbh->prepare($sql);
                    if (is_bool($stmt)) {
                        throw new Exception('Prepairing Statement Failed');
                    }
                    try {
                        foreach ($dbIntParams as $x => $v) {
                            $stmt->bindValue($x, $v, PDO::PARAM_INT);
                        }
                        $stmt->bindValue(':field_name', $fieldName);
                        $stmt->execute();
                    } catch (Exception $e) {
                        $error = $e->getMessage();
                        $log_api = new LogApi($this->dbh, $this->config);
                        $log_api->create(['log_api_command' => 'api->listing->getStatistics', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                        throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                    }
                    $record = $stmt->fetch();
                    $max = (float)$record['max'];
                    $min = (float)$record['min'];
                    if ($numformat) {
                        if ($field_type == 'price') {
                            $max = $misc->moneyFormats($misc->internationalNumFormat($max, $this->config['number_decimals_price_fields']));
                            $min = $misc->moneyFormats($misc->internationalNumFormat($min, $this->config['number_decimals_price_fields']));
                        } elseif ($field_type == 'number') {
                            $max = $misc->internationalNumFormat($max, $this->config['number_decimals_number_fields']);
                            $min = $misc->internationalNumFormat($min, $this->config['number_decimals_number_fields']);
                        }
                    }
                    return ['min' => $min, 'max' => $max];

                case 'avg':
                    $sql = 'SELECT AVG(' . $field_sql . ') as avg
							FROM ' . $field_list . '
							AND listingsdbelements_field_name = :field_name' . $class_sql;
                    $stmt = $this->dbh->prepare($sql);
                    if (is_bool($stmt)) {
                        throw new Exception('Prepairing Statement Failed');
                    }
                    try {
                        foreach ($dbIntParams as $x => $v) {
                            $stmt->bindValue($x, $v, PDO::PARAM_INT);
                        }
                        $stmt->bindValue(':field_name', $fieldName);
                        $stmt->execute();
                    } catch (Exception $e) {
                        $error = $e->getMessage();
                        $log_api = new LogApi($this->dbh, $this->config);
                        $log_api->create(['log_api_command' => 'api->listing->getStatistics', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                        throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                    }
                    $record = $stmt->fetch();
                    $avg = (float)$record['avg'];
                    if ($numformat) {
                        if ($field_type == 'price') {
                            $avg = $misc->moneyFormats($misc->internationalNumFormat($avg, $this->config['number_decimals_price_fields']));
                        } elseif ($field_type == 'number') {
                            $avg = $misc->internationalNumFormat($avg, $this->config['number_decimals_number_fields']);
                        }
                    }
                    return ['avg' => $avg];
                case 'median':
                    $sql = 'SELECT ' . $field_sql . ' as fvalue
							FROM ' . $field_list . '
							AND listingsdbelements_field_name = :field_name' . $class_sql . ' ORDER BY fvalue DESC';
                    $stmt = $this->dbh->prepare($sql);
                    if (is_bool($stmt)) {
                        throw new Exception('Prepairing Statement Failed');
                    }
                    try {
                        foreach ($dbIntParams as $x => $v) {
                            $stmt->bindValue($x, $v, PDO::PARAM_INT);
                        }
                        $stmt->bindValue(':field_name', $fieldName);
                        $stmt->execute();
                    } catch (Exception $e) {
                        $error = $e->getMessage();
                        $log_api = new LogApi($this->dbh, $this->config);
                        $log_api->create(['log_api_command' => 'api->listing->getStatistics', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                        throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                    }
                    $recordSet = $stmt->fetchAll();
                    $varray = [];
                    foreach ($recordSet as $record) {
                        $varray[] = (float)$record['fvalue'];
                    }
                    if (empty($varray)) {
                        $median = 0;
                    } else {
                        $count = count($varray);
                        //Check if value array has even number of elements.
                        if ($count % 2 == 0) {
                            $topHalf = $count / 2;
                            $bottomHalf = $topHalf--;
                            // Get index values
                            $first_half = $varray[(int)$topHalf];
                            $second_half = $varray[(int)$bottomHalf];
                            $median = ($first_half + $second_half) / 2;
                        } else {
                            $middle = $count / 2;
                            $median = $varray[(int)$middle];
                        }
                    }

                    if ($numformat) {
                        if ($field_type == 'price') {
                            $median = $misc->moneyFormats($misc->internationalNumFormat($median, $this->config['number_decimals_price_fields']));
                        } elseif ($field_type == 'number') {
                            $median = $misc->internationalNumFormat($median, $this->config['number_decimals_number_fields']);
                        }
                    }

                    return ['median' => $median];
            }
            throw new Exception('function: correct_parameter_not_passed');
        }
        throw new Exception('field not found');
    }

    /**
     * This API Command creates listings.
     *
     * @param array{
     *     class_id: integer,
     *     listing_details: array{
     *                    title: string,
     *                    seotitle?: string,
     *                    notes?: string,
     *                    featured: boolean,
     *                    active: boolean
     *                    },
     *     listing_agents: int[],
     *     listing_fields?:array<string, mixed>,
     *     listing_media?: array,
     *     disable_logs?: bool
     *     } $data Expects an array containing the following array keys.
     *
     *                    <ul>
     *                    <li>$data['class_id'] - This should be an integer of the class_id that this listing is
     *                    assigned to.</li>
     *                    <li>$data['listing_details'] - This should be an array containg the following three
     *                    settings.</li>
     *                    <li>$data['listing_details']['title'] - Required. This is the title for the listing.</li>
     *                    <li>$data['listing_details']['seotitle'] - Optional. If not set the API will create a seo
     *                    friendly title based on the title supplied. The API will ensure teh seotitle is unique, so
     *                    the seotitle you supply will be modified if needed..</li>
     *                    <li>$data['listing_details']['notes'] - Options - notes about this listing, only visible to
     *                    admin and agents.</li>
     *                    <li>$data['listing_details']['featured'] - Required Boolean - Is this a featured listings.
     *                    TRUE/FALSE</li>
     *                    <li>$data['listing_details']['active'] - - Required Boolean - Is this a active listings.
     *                    TRUE/FALSE</li>
     *                    <li>$data['listing_agents'] - This should be an array of up to agent ids. This sets the
     *                    listing agent ID, the primary listing agent ID must be key 0.
     *                    <code>$data['listing_agents'][0]=5; //This lising belongs to agent 5. All other keys are
     *                    currently ignored.</code></li>
     *                    <li>$data['listing_fields'] - This should be an array of the actual listing data. The array
     *                    keys should be the field name and the array values should be the field values. Only valid
     *                    fields will be used, other data will be dropped.
     *                    <code>$data['listing_fields'] =array('mls_id' => 126,'address'=>'126 E Buttler Ave');  //
     *                    This example defines a field value of 126 for a field called mls_id and a value of "126 E
     *                    Buttler Ave" for the address field.</code></li>
     *                    <li>$data['listing_media'] - Currently not used and MUST be an empty array.</li>
     *                    </ul>
     *
     * @return array{listing_id:int}
     * @throws \Exception
     */
    public function create(array $data): array
    {
        global $lang;

        $misc = new Misc($this->dbh, $this->config);
        $login = new Login($this->dbh, $this->config);
        $login_status = $login->verifyPriv('Agent');
        if ($login_status !== true) {
            throw new Exception('Login Failure');
        }

        //Check that required settings were passed
        if (!isset($data['class_id'])) {
            throw new Exception('class_id: correct_parameter_not_passed');
        }
        if (!isset($data['listing_details'])) {
            throw new Exception('listing_details: correct_parameter_not_passed');
        }
        if (empty($data['listing_details']['title'])) {
            throw new Exception('$listing_details[title]: correct_parameter_not_passed');
        }
        if (!isset($data['listing_details']['featured'])) {
            throw new Exception('$data[listing_details][featured]: correct_parameter_not_passed');
        }
        if (!isset($data['listing_details']['active'])) {
            throw new Exception('$data[listing_details][active]: correct_parameter_not_passed');
        }

        if (!isset($data['listing_agents']) || !isset($data['listing_agents'][0])) {
            throw new Exception('listing_agents: correct_parameter_not_passed');
        }
        if (!isset($data['listing_fields'])) {
            throw new Exception('listing_fields: correct_parameter_not_passed');
        }

        //Strip Tags from Titles
        $data['listing_details']['title'] = strip_tags($data['listing_details']['title']);

        if (isset($data['listing_details']['seotitle'])) {
            $data['listing_details']['seotitle'] = strip_tags($data['listing_details']['seotitle']);
        }

        //Ok we have all the needed variables so now we bulid the listings.
        $data['listing_details']['creation_date'] = time();
        $data['listing_details']['last_modified'] = time();
        $data['listing_details']['expiration'] = time() + ($this->config['days_until_listings_expire'] * 86400);

        if (!isset($data['listing_details']['seotitle'])) {
            //Make SEO Title
            if (!$this->config['controlpanel_mbstring_enabled']) {
                // MBSTRING NOT ENABLED
                $data['listing_details']['seotitle'] = strtolower($data['listing_details']['title']);
            } else {
                $data['listing_details']['seotitle'] = mb_convert_case($data['listing_details']['title'], MB_CASE_LOWER, $this->config['charset']);
            }
            $data['listing_details']['seotitle'] = trim($data['listing_details']['seotitle']);
            $data['listing_details']['seotitle'] = preg_replace('/[\~`!@#\$%^*\(\)\+=\"\':;\[\]\{\}|\\\?\<\>,\.\/]/', '', $data['listing_details']['seotitle']);
            $data['listing_details']['seotitle'] = str_replace(' ', $this->config['seo_url_seperator'], $data['listing_details']['seotitle']);
            $data['listing_details']['seotitle'] = preg_replace('/[\-]+/', '-', $data['listing_details']['seotitle']);
        }
        //Verify Permissions
        // Check Number of Listings User has
        $listing_agent = intval($data['listing_agents'][0]);
        //Verify we have permissions to add a listing
        if ($_SESSION['userID'] == $listing_agent) {
            $security = $login->verifyPriv('Agent');
            if ($security !== true) {
                throw new Exception('Permission Denied (agent)');
            }
        } else {
            //Check tha we can add listing for other users
            $security = $login->verifyPriv('edit_all_listings');
            if ($security !== true) {
                throw new Exception('Permission Denied (edit_all_listings)');
            }
        }

        $listing_count = $this->getListingCountByAgentId($listing_agent);

        $userApi = new UserApi($this->dbh, $this->config);
        $result = $userApi->read([
            'user_id' => $listing_agent,
            'resource' => 'agent',
            'fields' => ['userdb_limit_listings'],
        ]);
        $listing_limit = $result['user']['userdb_limit_listings'] ?? 0;

        //Ok Decide if user can have more listings
        if (($listing_count >= $listing_limit) && ($listing_limit != '-1')) {
            throw new Exception('Listing Limit Reached');
        } else {
            $class_id = intval($data['class_id']);
            if ($data['listing_details']['featured']) {
                $featured = 'yes';
            } else {
                $featured = 'no';
            }
            if ($data['listing_details']['active']) {
                $active = 'yes';
            } else {
                $active = 'no';
            }

            //INSERT LISTING DETAILS
            $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'listingsdb
				(listingsdb_title,listing_seotitle,listingsdb_expiration,listingsdb_notes,listingsdb_hit_count,
				listingsdb_featured,listingsdb_active,listingsdb_creation_date,listingsdb_last_modified,userdb_id,listingsdb_pclass_id,listingsdb_mlsexport)
				VALUES (:title,:seotitle,:expiration,:notes,0,
				:featured,:active,:creation_date,:last_modified,:listing_agent,:class_id,:mlsexport);';
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->bindValue(':title', $data['listing_details']['title']);
                $stmt->bindValue(':seotitle', $data['listing_details']['seotitle']);
                $stmt->bindValue(':notes', $data['listing_details']['notes'] ?? '');
                $stmt->bindValue(':expiration', date("Y-m-d H:i:s", $data['listing_details']['expiration']));
                $stmt->bindValue(':featured', $featured);
                $stmt->bindValue(':active', $active);
                $stmt->bindValue(':mlsexport', 'no');
                $stmt->bindValue(':creation_date', date("Y-m-d", $data['listing_details']['creation_date']));
                $stmt->bindValue(':last_modified', date("Y-m-d H:i:s", $data['listing_details']['last_modified']));
                $stmt->bindValue(':listing_agent', $listing_agent, PDO::PARAM_INT);
                $stmt->bindValue(':class_id', $class_id, PDO::PARAM_INT);
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }


            $listing_id = $this->dbh->lastInsertId();
            if (is_bool($listing_id)) {
                throw new Exception('Failed Getting Last Insert ID');
            }
            $listing_id = (int)$listing_id;

            //Make sure title is unique
            $sql = 'SELECT count(listingsdb_id) 
					FROM ' . $this->config['table_prefix'] . 'listingsdb 
					WHERE listing_seotitle = :seotitle';
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->bindValue(':seotitle', $data['listing_details']['seotitle']);
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->listing->create', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $listing_count = (int)$stmt->fetchColumn();
            if ($listing_count > 1) {
                $data['listing_details']['seotitle'] = $data['listing_details']['seotitle'] . '-' . $listing_id;
                $sql = 'UPDATE ' . $this->config['table_prefix'] . 'listingsdb 
						SET listing_seotitle = :seotitle 
						WHERE listingsdb_id = :listing_id';
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
                try {
                    $stmt->bindValue(':seotitle', $data['listing_details']['seotitle']);
                    $stmt->bindValue(':listing_id', $listing_id, PDO::PARAM_INT);
                    $stmt->execute();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }
            }


            $fields_api = new FieldsApi($this->dbh, $this->config);
            $field_list = $fields_api->metadata(['resource' => 'listing', 'class' => [$class_id]]);
            //echo '<pre>'.print_r($field_list,TRUE).'</pre>';
            //Verify Listing Fields passed via API exist in the class that the listing is being inserted into.
            $insert_listing_fields = [];
            foreach ($field_list['fields'] as $field) {
                $field_id = $field['field_id'];
                $name = $field['field_name'] ?? '';
                $data_elements = $field['field_elements'] ?? [];
                $data_type = $field['field_type'] ?? '';
                $default_text = $field['default_text'] ?? '';
                //Make sure field does not have same name as a core field for saftey
                if (array_key_exists($name, $data['listing_fields'])) {
                    switch ($data_type) {
                        case 'number':
                            if ($data['listing_fields'][$name] === '') {
                                $insert_listing_fields[$name] = null;
                            } else {
                                if (is_numeric($data['listing_fields'][$name])) {
                                    $insert_listing_fields[$name] = $data['listing_fields'][$name];
                                } else {
                                    $price = str_replace(',', '', $data['listing_fields'][$name]);
                                    $insert_listing_fields[$name] = intval($price);
                                }
                            }
                            break;
                        case 'decimal':
                        case 'price':
                            if ($data['listing_fields'][$name] === '' || !is_scalar($data['listing_fields'][$name])) {
                                $insert_listing_fields[$name] = null;
                            } else {
                                $price = str_replace(',', '', (string)$data['listing_fields'][$name]);
                                $insert_listing_fields[$name] = (float)$price;
                            }
                            break;
                        case 'date':
                            if (!is_string($data['listing_fields'][$name]) || empty($data['listing_fields'][$name])) {
                                $insert_listing_fields[$name] = null;
                            } else {
                                $insert_listing_fields[$name] = $this->convertDate($data['listing_fields'][$name]);
                            }
                            break;
                        case 'select':
                        case 'select-multiple':
                        case 'option':
                        case 'checkbox':
                            //This is a lookup field. Make sure values passed are allowed by the system.
                            //Get array of passed data eleements
                            if (!is_array($data['listing_fields'][$name])) {
                                $t_value = $data['listing_fields'][$name];
                                unset($data['listing_fields'][$name]);
                                $data['listing_fields'][$name][] = $t_value;
                            }
                            $good_elements = [];
                            foreach ($data['listing_fields'][$name] as $fvalue) {
                                if (in_array($fvalue, $data_elements) && !in_array($fvalue, $good_elements)) {
                                    $good_elements[] = $fvalue;
                                }
                            }
                            $insert_listing_fields[$name] = $good_elements;
                            break;
                        default:
                            $insert_listing_fields[$name] = $data['listing_fields'][$name];
                            break;
                    }
                } else {
                    if ($default_text != '') {
                        $insert_listing_fields[$name] = $default_text;
                    } else {
                        $insert_listing_fields[$name] = '';
                    }
                }
            }


            $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'listingsdbelements (listingsdbelements_field_name, listingsdbelements_field_value, listingsdb_id, userdb_id) VALUES ';
            $dbIntParams = [];
            $dbStrParams = [];
            $sql2 = [];
            $i = 0;
            foreach ($insert_listing_fields as $name => $value) {
                $sql_value = '';
                if (is_array($value)) {
                    $sql_value = implode('||', $value);
                } elseif (is_scalar($value)) {
                    $sql_value = (string)$value;
                }
                $dbStrParams[':name' . $i] = $name;
                $dbStrParams[':value' . $i] = $sql_value;
                $dbIntParams[':listing_id' . $i] = $listing_id;
                $dbIntParams[':listing_agent' . $i] = $listing_agent;
                $sql2[] = "(:name$i, :value$i, :listing_id$i, :listing_agent$i)";
                $i++;
            }
            if (count($sql2) > 0) {
                $sql .= implode(',', $sql2);
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
                try {
                    foreach ($dbStrParams as $k => $v) {
                        $stmt->bindValue($k, $v);
                    }
                    foreach ($dbIntParams as $k => $v) {
                        $stmt->bindValue($k, $v, PDO::PARAM_INT);
                    }
                    $stmt->execute();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql . 'Params: ' . json_encode($dbStrParams));
                }
            }

            //Call afterListingCreate hoook
            $hooks = new Hooks($this->dbh, $this->config);
            $hooks->load('afterListingCreate', $listing_id);

            //Deal with Hooks and social sites
            if ($data['listing_details']['active']) {
                $hooks->load('afterListingActivated', $listing_id);
            }

            // Only used when creation is done via the API. The add listing
            // feature in OR creates an inactive listing and used the the update
            // method to send the tweet when the status is changed from inactive
            // to active
            if ($this->config['twitter_new_listings'] && $data['listing_details']['active']) {
                if ($this->config['twitter_listing_photo']) {
                    $media_api = new MediaApi($this->dbh, $this->config);
                    $result = $media_api->read([
                        'media_type' => 'listingsimages',
                        'media_parent_id' => $listing_id,
                        'media_output' => 'URL',
                    ]);

                    if ($result['media_count'] > 0) {
                        $media = $result['media_object'][0]['file_name'];

                        $media_remote = $result['media_object'][0]['remote'] ?? false;
                        $twitter_url = ' ' . $this->config['baseurl'] . '/l/' . $listing_id;
                        $twitter_title = $data['listing_details']['title'];
                        if (strlen($twitter_url) + strlen($twitter_title) > 140) {
                            $twitter_title = substr($twitter_title, 0, 137 - strlen($twitter_url)) . '...';
                        }
                        $twitter_post = $twitter_title . $twitter_url;
                        $twitter_api = new TwitterApi($this->dbh, $this->config);
                        $twitter_api->post(['message' => $twitter_post, 'media' => $media, 'media_remote' => $media_remote]);
                    }
                }
            }

            //Email Notification
            if ($this->config['email_notification_of_new_listings']) {
                $page = new PageUser($this->dbh, $this->config);
                $listing_pages = new ListingPages($this->dbh, $this->config);
                $page->loadPage($this->config['admin_template_path'] . '/email/new_listing_notification.html');
                $remote_ip = $_SERVER['REMOTE_ADDR'] ?? '';
                $timestamp = date('F j, Y, g:i:s a');
                $page->replaceTag('user_ip', $remote_ip);
                $page->replaceTag('notification_time', $timestamp);
                $page->replaceListingFieldTags($listing_id);
                $page->replaceLangTemplateTags();
                $page->replaceTags(['company_logo', 'baseurl', 'template_url']);
                $subject = $page->getTemplateSection('subject_block');
                $page->page = $page->removeTemplateBlock('subject', $page->page);
                $page->autoReplaceTags();
                $message = $page->returnPage();
                if (isset($this->config['site_email']) && $this->config['site_email'] != '') {
                    $sender_email = $this->config['site_email'];
                } else {
                    $sender_email = $this->config['admin_email'];
                }
                $listing_agent_email = $listing_pages->getListingAgentValue('userdb_emailaddress', $listing_id);
                $listing_agent_name = $listing_pages->getListingAgentValue('userdb_user_last_name', $listing_id) . ', ' . $listing_pages->getListingAgentValue('userdb_user_first_name', $listing_id);
                $misc->sendEmail($this->config['admin_name'], $sender_email, $this->config['admin_email'], $message, $subject, true, false, $listing_agent_name, $listing_agent_email);
            } // end if
            // ta da! we're done...
            $admin_status = $login->verifyPriv('Admin');
            if (!$admin_status || !isset($data['disable_logs']) || !$data['disable_logs']) {
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->listing->create', 'log_message' => $lang['log_created_listing'] . ' ' . $listing_id . ' by ' . $_SESSION['username']]);
            }
            return ['listing_id' => $listing_id];
        }
    }

    /**
     * This API Command reads a listing
     *
     * @param array{
     *     listing_id: int,
     *     fields: string[]
     * } $data $data expects an array containing the following array keys.
     *
     * <ul>
     *  <li>$data['listing_id'] - This is the Listing ID that we are updating.</li>
     *  <li>$data['fields'] - This is an optional array of fields to retrieve, if left empty or not
     *  passed all fields will be retrieved.</li>
     *</ul>
     *
     * @return array{listing: array<string, array<int<0, max>, string>|int|string>}
     * @throws \Exception
     **/
    public function read(array $data): array
    {
        $login = new Login($this->dbh, $this->config);
        $login_is_agent = $login->verifyPriv('Agent');

        //Check that required settings were passed
        if (!isset($data['listing_id'])) {
            throw new Exception('listing_id: correct_parameter_not_passed');
        }

        //This will hold our listing data
        $listing_data = [];
        //If no fields were passed make an empty array to save checking for if !isset later
        if (!isset($data['fields'])) {
            $data['fields'] = [];
        }
        //
        $sql = 'SELECT 1 FROM ' . $this->config['table_prefix'] . 'listingsdb WHERE listingsdb_id = :listing_id';
        //TODO: Move this active check up higher to ensure listing is active no matter if we ask for one field or all fields on a listing
        $dbStrParams = [];
        if (!$login_is_agent) {
            $sql .= ' AND listingsdb_active = :active';
            $dbStrParams[':active'] = 'yes';
            if ($this->config['use_expiration']) {
                $sql .= ' AND listingsdb_expiration > :expiration';
                $dbStrParams[':expiration'] = date("Y-m-d", time());
            }
        }
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            foreach ($dbStrParams as $k => $v) {
                $stmt->bindValue($k, $v);
            }
            $stmt->bindValue(':listing_id', $data['listing_id'], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }

        if ((int)$stmt->fetchColumn() == 0) {
            throw new Exception('Listing does Not exist or you do not have permission');
        }

        //Get Base Listing Information
        if (empty($data['fields'])) {
            $sql = 'SELECT ' . implode(',', $this->OR_INT_FIELDS) . ' FROM ' . $this->config['table_prefix'] . 'listingsdb WHERE listingsdb_id = :listing_id';

            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->bindValue(':listing_id', $data['listing_id'], PDO::PARAM_INT);
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }

            $recordSet = $stmt->fetchAll();
            if (count($recordSet) != 1) {
                throw new Exception('Listing does Not exist or you do not have permission');
            }
            // 'listingsdb_id', 'listingsdb_pclass_id', 'userdb_id', 'listingsdb_title', 'listingsdb_expiration', 'listingsdb_notes', 'listingsdb_creation_date', 'listingsdb_last_modified', 'listingsdb_hit_count', 'listingsdb_featured', 'listingsdb_active', 'listingsdb_mlsexport', 'listing_seotitle'
            $listing_data['listingsdb_id'] = (int)$recordSet[0]['listingsdb_id'];
            $listing_data['listingsdb_pclass_id'] = (int)$recordSet[0]['listingsdb_pclass_id'];
            $listing_data['userdb_id'] = (int)$recordSet[0]['userdb_id'];
            $listing_data['listingsdb_title'] = (string)$recordSet[0]['listingsdb_title'];
            $listing_data['listingsdb_expiration'] = (string)$recordSet[0]['listingsdb_expiration'];
            $listing_data['listingsdb_notes'] = (string)$recordSet[0]['listingsdb_notes'];
            $listing_data['listingsdb_creation_date'] = (string)$recordSet[0]['listingsdb_creation_date'];
            $listing_data['listingsdb_last_modified'] = (string)$recordSet[0]['listingsdb_last_modified'];
            $listing_data['listingsdb_hit_count'] = (int)$recordSet[0]['listingsdb_hit_count'];
            $listing_data['listingsdb_featured'] = (string)$recordSet[0]['listingsdb_featured'];
            $listing_data['listingsdb_active'] = (string)$recordSet[0]['listingsdb_active'];
            $listing_data['listingsdb_mlsexport'] = (string)$recordSet[0]['listingsdb_mlsexport'];
            $listing_data['listing_seotitle'] = (string)$recordSet[0]['listing_seotitle'];

            $pclass_list = [$listing_data['listingsdb_pclass_id']];
            $fields_api = new FieldsApi($this->dbh, $this->config);
            $field_list = $fields_api->metadata(['resource' => 'listing', 'class' => $pclass_list]);
            //echo '<pre>'.print_r($field_list,TRUE).'</pre>';
            //Verify Listing Fields passed via API exist in the class that the listing is being inserted into.
            $allowed_fields = [];
            $allowed_fields_values = [];
            $allowed_fields_type = [];
            foreach ($field_list['fields'] as $field) {
                $field_id = $field['field_id'];
                $field_name = $field['field_name'] ?? '';
                $field_elements = $field['field_elements'] ?? [];
                $ftype = $field['field_type'] ?? '';
                //Make sure field does not have same name as a core field for saftey
                if (!in_array($field_name, $this->OR_INT_FIELDS) && $field_name != '') {
                    $allowed_fields[$field_id] = $field_name;
                    $allowed_fields_type[$field_name] = $ftype;
                    if ($ftype == 'select' || $ftype == 'select-multiple' || $ftype == 'option' || $ftype == 'checkbox') {
                        $allowed_fields_values[$field_name] = $field_elements;
                    }
                }
            }
            //Get The fields
            $sql = 'SELECT listingsdbelements_field_name, listingsdbelements_field_value FROM ' . $this->config['table_prefix'] . 'listingsdbelements WHERE listingsdb_id = :listing_id';
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->bindValue(':listing_id', $data['listing_id'], PDO::PARAM_INT);
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }

            $recordSet = $stmt->fetchAll();
            foreach ($recordSet as $record) {
                $field_name = (string)$record['listingsdbelements_field_name'];
                $field_value = (string)$record['listingsdbelements_field_value'];

                if (in_array($field_name, $allowed_fields)) {
                    //See if this is a lookup
                    if (isset($allowed_fields_type[$field_name]) && isset($allowed_fields_values[$field_name])) {
                        if ($allowed_fields_type[$field_name] == 'select' || $allowed_fields_type[$field_name] == 'option') {
                            if (in_array($field_value, $allowed_fields_values[$field_name])) {
                                $listing_data[$field_name] = $field_value;
                            } else {
                                $listing_data[$field_name] = '';
                            }
                        } else {
                            $field_values = explode('||', $field_value);
                            $real_values = array_intersect($allowed_fields_values[$field_name], $field_values);
                            $listing_data[$field_name] = $real_values;
                        }
                    } else {
                        $listing_data[$field_name] = $field_value;
                    }
                }
            }
        } else {
            $core_fields = array_intersect($this->OR_INT_FIELDS, $data['fields']);
            $noncore_fields = array_diff($data['fields'], $this->OR_INT_FIELDS);
            if (!empty($core_fields)) {
                $sql = 'SELECT ' . implode(',', $core_fields) . ' FROM ' . $this->config['table_prefix'] . 'listingsdb WHERE listingsdb_id = :listing_id';

                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
                try {
                    $stmt->bindValue(':listing_id', $data['listing_id'], PDO::PARAM_INT);
                    $stmt->execute();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }

                $recordSet = $stmt->fetchAll();
                if (count($recordSet) != 1) {
                    throw new Exception('Listing does Not exist or you do not have permission');
                }
                if (in_array('listingsdb_id', $core_fields)) {
                    $listing_data['listingsdb_id'] = (int)$recordSet[0]['listingsdb_id'];
                }
                if (in_array('listingsdb_pclass_id', $core_fields)) {
                    $listing_data['listingsdb_pclass_id'] = (int)$recordSet[0]['listingsdb_pclass_id'];
                }
                if (in_array('userdb_id', $core_fields)) {
                    $listing_data['userdb_id'] = (int)$recordSet[0]['userdb_id'];
                }
                if (in_array('listingsdb_title', $core_fields)) {
                    $listing_data['listingsdb_title'] = (string)$recordSet[0]['listingsdb_title'];
                }
                if (in_array('listingsdb_expiration', $core_fields)) {
                    $listing_data['listingsdb_expiration'] = (string)$recordSet[0]['listingsdb_expiration'];
                }
                if (in_array('listingsdb_notes', $core_fields)) {
                    $listing_data['listingsdb_notes'] = (string)$recordSet[0]['listingsdb_notes'];
                }
                if (in_array('listingsdb_creation_date', $core_fields)) {
                    $listing_data['listingsdb_creation_date'] = (string)$recordSet[0]['listingsdb_creation_date'];
                }
                if (in_array('listingsdb_last_modified', $core_fields)) {
                    $listing_data['listingsdb_last_modified'] = (string)$recordSet[0]['listingsdb_last_modified'];
                }
                if (in_array('listingsdb_hit_count', $core_fields)) {
                    $listing_data['listingsdb_hit_count'] = (int)$recordSet[0]['listingsdb_hit_count'];
                }
                if (in_array('listingsdb_featured', $core_fields)) {
                    $listing_data['listingsdb_featured'] = (string)$recordSet[0]['listingsdb_featured'];
                }
                if (in_array('listingsdb_active', $core_fields)) {
                    $listing_data['listingsdb_active'] = (string)$recordSet[0]['listingsdb_active'];
                }
                if (in_array('listingsdb_mlsexport', $core_fields)) {
                    $listing_data['listingsdb_mlsexport'] = (string)$recordSet[0]['listingsdb_mlsexport'];
                }
                if (in_array('listing_seotitle', $core_fields)) {
                    $listing_data['listing_seotitle'] = (string)$recordSet[0]['listing_seotitle'];
                }
            }
            //Ok we have the core fields, figure out what property class this listing is in.
            if (!empty($noncore_fields)) {
                $sql = 'SELECT listingsdb_pclass_id FROM ' . $this->config['table_prefix'] . 'listingsdb WHERE listingsdb_id = :listing_id';
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
                try {
                    $stmt->bindValue(':listing_id', $data['listing_id'], PDO::PARAM_INT);
                    $stmt->execute();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }

                $pclass_list = [(int)$stmt->fetchColumn()];
                $fields_api = new FieldsApi($this->dbh, $this->config);
                $field_list = $fields_api->metadata(['resource' => 'listing', 'class' => $pclass_list]);
                //Verify Listing Fields passed via API exist in the class that the listing is being inserted into.
                $allowed_fields = [];
                $allowed_fields_values = [];
                $allowed_fields_type = [];
                foreach ($field_list['fields'] as $field) {
                    $field_id = $field['field_id'];
                    $field_name = $field['field_name'] ?? '';
                    $field_elements = $field['field_elements'] ?? [];
                    $ftype = $field['field_type'] ?? '';
                    //Make sure field does not have same name as a core field for saftey
                    if (in_array($field_name, $noncore_fields) && $field_name != '') {
                        $allowed_fields[$field_id] = $field_name;
                        $allowed_fields_type[$field_name] = $ftype;
                        if ($ftype == 'select' || $ftype == 'select-multiple' || $ftype == 'option' || $ftype == 'checkbox') {
                            $allowed_fields_values[$field_name] = $field_elements;
                        }
                    }
                }
                //Get The fields
                $sql = 'SELECT listingsdbelements_field_name, listingsdbelements_field_value FROM ' . $this->config['table_prefix'] . 'listingsdbelements WHERE listingsdb_id = :listing_id';
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
                try {
                    $stmt->bindValue(':listing_id', $data['listing_id'], PDO::PARAM_INT);
                    $stmt->execute();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }

                $recordSet = $stmt->fetchAll();
                foreach ($recordSet as $record) {
                    $field_name = (string)$record['listingsdbelements_field_name'];
                    $field_value = (string)$record['listingsdbelements_field_value'];
                    if (in_array($field_name, $allowed_fields)) {
                        //See if this is a lookup
                        if (array_key_exists($field_name, $allowed_fields_type) && isset($allowed_fields_values[$field_name])) {
                            if ($allowed_fields_type[$field_name] == 'select' || $allowed_fields_type[$field_name] == 'option') {
                                if (in_array($field_value, $allowed_fields_values[$field_name])) {
                                    $listing_data[$field_name] = $field_value;
                                } else {
                                    $listing_data[$field_name] = '';
                                }
                            } else {
                                $field_values = explode('||', $field_value);
                                $real_values = array_intersect($allowed_fields_values[$field_name], $field_values);
                                $listing_data[$field_name] = $real_values;
                            }
                        } else {
                            $listing_data[$field_name] = $field_value;
                        }
                    }
                }
            }
        }
        return ['listing' => $listing_data];
    }

    /**
     * This API Command updates listings.
     *
     * @param array{listing_id: integer,
     *              class_id?: integer,
     *              listing_agents?: int[],
     *              listing_details?: array{title?: string,
     *                                      seotitle?: string,
     *                                      notes?: string,
     *                                      featured?: boolean,
     *                                      active?: boolean,
     *                                      expiration?: string,
     *                                      hit_count?: integer
     *              },
     *              listing_fields?: array,
     *              disable_logs?: bool,
     *              listing_media?: array,
     *          } $data Expects an array containing the following array keys.
     *
     * <ul>
     *      <li>$data['listing_id'] - This is the Listing ID that we are updating.</li>
     *      <li>$data['class_id'] - This should be an integer of the class_id that this listing is
     *          currently assigned to.</li>
     *      <li>$data['listing_details'] - This should be an array containg the following three
     *          settings.</li>
     *      <li>$data['listing_details']['title'] - This is the listing title. Set only if you want to
     *          change the existing title.</li>
     *      <li>$data['listing_details']['seotitle'] - This is the listing title. Set only if you want to
     *          change the existing title, or set to AUTO to have the system generate a new SEO title based
     *          on the title you have updated.</li>
     *      <li>$data['listing_details']['featured'] - Set if this a featured listings, only set if you
     *          need to change. TRUE/FALSE</li>
     *      <li>$data['listing_details']['active'] -Set if this a active listings, only set if you need
     *          to change. TRUE/FALSE</li>
     *      <li>$data['listing_agents'] - This should be an array of up to agent ids. This sets the
     *      listing agent ID, the primary listing agent ID must be key 0.
     *          <code>$data['listing_agents'][0]=5; //This lising belongs to agent 5. All other keys are
     *          currently ignored.</code>
     *      <li>$data['listing_fields'] - This should be an array of the actual listing data. The array
     *          keys should be the field name and the array values should be the field values. Only valid
     *          fields will be used, other data will be dropped.
     *          <code>$data['listing_fields'] =array('mls_id' => 126,'address'=>'126 E Buttler Ave');  //
     *          This defines a field value of 126 for a field called mls_id and a value of "126 E Buttler
     *          Ave" for the address field.</code></li>
     *  </ul>
     *
     * @return array{listing_id: integer}
     * @throws \Exception
     *
     */
    public function update(array $data): array
    {
        global $lang;
        $login = new Login($this->dbh, $this->config);
        $login_status = $login->verifyPriv('Agent');
        if ($login_status !== true) {
            throw new Exception('Login Failure');
        }

        //Check that required settings were passed
        if (!isset($data['listing_id'])) {
            throw new Exception('listing_id: correct_parameter_not_passed');
        }
        if (isset($data['listing_fields']) && (!isset($data['class_id']))) {
            throw new Exception('class_id: correct_parameter_not_passed');
        }
        if (isset($data['listing_details']['title']) && empty($data['listing_details']['title'])) {
            throw new Exception('$data[listing_details][title]: correct_parameter_not_passed');
        }

        if (isset($data['listing_agents']) && count($data['listing_agents']) < 1) {
            throw new Exception('listing_agents: correct_parameter_not_passed');
        }
        if (isset($data['listing_fields']) && !is_array($data['listing_fields'])) {
            throw new Exception('listing_fields: correct_parameter_not_passed');
        }

        //Strip Tags from Titles
        if (isset($data['listing_details']['title'])) {
            $data['listing_details']['title'] = strip_tags($data['listing_details']['title']);
        }
        if (isset($data['listing_details']['seotitle'])) {
            $data['listing_details']['seotitle'] = strip_tags($data['listing_details']['seotitle']);
        }

        //See if the listing is currently active.
        $listing_api = new ListingApi($this->dbh, $this->config);
        $api_result = $listing_api->read(['listing_id' => $data['listing_id'], 'fields' => ['listingsdb_active']]);
        $oldstatus = $api_result['listing']['listingsdb_active'];
        //Ok we have all the needed variables so now we build the listings.
        $data['listing_details']['last_modified'] = time();
        if (isset($data['listing_details']['title']) && isset($data['listing_details']['seotitle']) && $data['listing_details']['seotitle'] == 'AUTO') {
            //Make SEO Title
            if (!$this->config['controlpanel_mbstring_enabled']) {
                // MBSTRING NOT ENABLED
                $data['listing_details']['seotitle'] = strtolower($data['listing_details']['title']);
            } else {
                $data['listing_details']['seotitle'] = mb_convert_case($data['listing_details']['title'], MB_CASE_LOWER, $this->config['charset']);
            }
            $data['listing_details']['seotitle'] = trim($data['listing_details']['seotitle']);
            $data['listing_details']['seotitle'] = preg_replace('/[\~`!@#\$%^*\(\)\+=\"\':;\[\]\{\}|\\\?\<\>,\.\/]/', '', $data['listing_details']['seotitle']);
            $data['listing_details']['seotitle'] = str_replace(' ', $this->config['seo_url_seperator'], $data['listing_details']['seotitle']);
            $data['listing_details']['seotitle'] = preg_replace('/[\-]+/', '-', $data['listing_details']['seotitle']);
        }
        $listing_id = intval($data['listing_id']);
        //Do Permission Checks
        $sql = 'SELECT userdb_id FROM ' . $this->config['table_prefix'] . 'listingsdb
				WHERE listingsdb_id = :listing_id';
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->bindValue(':listing_id', $data['listing_id'], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        // get main listings data
        $listing_agent = (int)$stmt->fetchColumn();
        //Verify we have permissions to add a listing
        if ($_SESSION['userID'] == $listing_agent) {
            $security = $login->verifyPriv('Agent');
            if ($security !== true) {
                throw new Exception('Permission Denied (agent)');
            }
        } else {
            //Check tha we can add listing for other users
            $security = $login->verifyPriv('edit_all_listings');
            if ($security !== true) {
                throw new Exception('Permission Denied (edit_all_listings)');
            }
        }
        $dbIntParams = [];
        $dbStrParams = [];
        $can_edit_all_listings = $login->verifyPriv('edit_all_listings');
        if ($can_edit_all_listings) {
            if (isset($data['listing_agents']) && $data['listing_agents'][0] > 0) {
                $dbIntParams['userdb_id'] = intval($data['listing_agents'][0]);
            }
        } else {
            $dbIntParams['userdb_id'] = $listing_agent;
        }

        if (isset($data['listing_details']['title'])) {
            $dbStrParams['listingsdb_title'] = $data['listing_details']['title'];
        }

        if (isset($data['listing_details']['notes'])) {
            $dbStrParams['listingsdb_notes'] = $data['listing_details']['notes'];
        }

        if (isset($data['listing_details']['featured'])) {
            if ($data['listing_details']['featured']) {
                $dbStrParams['listingsdb_featured'] = 'yes';
            } else {
                $dbStrParams['listingsdb_featured'] = 'no';
            }
        }
        if (isset($data['listing_details']['active'])) {
            if ($data['listing_details']['active']) {
                $dbStrParams['listingsdb_active'] = 'yes';
            } else {
                $dbStrParams['listingsdb_active'] = 'no';
            }
        }

        if (isset($data['listing_details']['hit_count'])) {
            $dbIntParams['listingsdb_hit_count'] = intval($data['listing_details']['hit_count']);
        }

        $exp_status = $login->verifyPriv('CanEditExpiration');
        if ($exp_status) {
            if (isset($data['listing_details']['expiration'])) {
                $expiration = $this->convertDate($data['listing_details']['expiration']);
                if (!is_bool($expiration)) {
                    $dbStrParams['listingsdb_expiration'] = date("Y-m-d", $expiration);
                }
            }
        }
        $dbStrParams['listingsdb_last_modified'] = date("Y-m-d H:i:s", $data['listing_details']['last_modified']);


        $sql_a = [];
        foreach ($dbIntParams as $field => $value) {
            $sql_a[] = $field . ' = :' . $field;
        }
        foreach ($dbStrParams as $field => $value) {
            $sql_a[] = $field . ' = :' . $field;
        }


        $sql = 'UPDATE ' . $this->config['table_prefix'] . 'listingsdb SET ' . implode(',', $sql_a) . ' WHERE listingsdb_id = :listing_id';
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->bindValue(':listing_id', $data['listing_id'], PDO::PARAM_INT);
            foreach ($dbIntParams as $field => $value) {
                $stmt->bindValue(':' . $field, $value, PDO::PARAM_INT);
            }
            foreach ($dbStrParams as $field => $value) {
                $stmt->bindValue(':' . $field, $value);
            }
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }

        if (isset($data['listing_details']['seotitle'])) {
            //Make sure title is unique
            $sql = 'SELECT count(listingsdb_id) 
					FROM ' . $this->config['table_prefix'] . 'listingsdb 
					WHERE listing_seotitle = :seotitle';
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->bindValue(':seotitle', $data['listing_details']['seotitle']);
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $listing_count = (int)$stmt->fetchColumn();
            if ($listing_count > 1) {
                $data['listing_details']['seotitle'] = $data['listing_details']['seotitle'] . '-' . $data['listing_id'];
                $sql = 'UPDATE ' . $this->config['table_prefix'] . 'listingsdb 
						SET listing_seotitle = :seotitle 
						WHERE listingsdb_id = :listing_id';
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
                try {
                    $stmt->bindValue(':seotitle', $data['listing_details']['seotitle']);
                    $stmt->bindValue(':listing_id', $data['listing_id'], PDO::PARAM_INT);
                    $stmt->execute();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }
            }
        }

        //Deal with Listing Fields
        if (isset($data['listing_fields']) && isset($data['class_id'])) {
            $class_id = intval($data['class_id']);
            //Get List of Fields for this property class

            $fields_api = new FieldsApi($this->dbh, $this->config);
            $field_list = $fields_api->metadata(['resource' => 'listing', 'class' => [$class_id]]);
            //echo '<pre>'.print_r($field_list,TRUE).'</pre>';
            //Verify Listing Fields passed via API exist in the class that the listing is being inserted into.
            $insert_listing_fields = [];
            foreach ($field_list['fields'] as $field) {
                $field_id = $field['field_id'];
                $name = $field['field_name'] ?? '';
                $data_elements = $field['field_elements'] ?? [];
                $data_type = $field['field_type'] ?? '';
                $default_text = $field['default_text'] ?? '';
                if (array_key_exists($name, $data['listing_fields'])) {
                    switch ($data_type) {
                        case 'number':
                            if ($data['listing_fields'][$name] === '') {
                                $insert_listing_fields[$name] = null;
                            } else {
                                if (is_numeric($data['listing_fields'][$name])) {
                                    $insert_listing_fields[$name] = $data['listing_fields'][$name];
                                } else {
                                    $price = str_replace(',', '', $data['listing_fields'][$name]);
                                    $insert_listing_fields[$name] = intval($price);
                                }
                            }
                            break;
                        case 'decimal':
                        case 'price':
                            if (!is_scalar($data['listing_fields'][$name]) || empty($data['listing_fields'][$name])) {
                                $insert_listing_fields[$name] = null;
                            } else {
                                $price = str_replace(',', '', (string)$data['listing_fields'][$name]);
                                $insert_listing_fields[$name] = (float)$price;
                            }
                            break;
                        case 'date':
                            if (!is_string($data['listing_fields'][$name]) || empty($data['listing_fields'][$name])) {
                                $insert_listing_fields[$name] = null;
                            } else {
                                $insert_listing_fields[$name] = $this->convertDate($data['listing_fields'][$name]);
                            }
                            break;
                        case 'select':
                        case 'select-multiple':
                        case 'option':
                        case 'checkbox':
                            //This is a lookup field. Make sure values passed are allowed by the system.
                            //Get Array of allowed data elements
                            //Get array of passed data eleements
                            if (!is_array($data['listing_fields'][$name])) {
                                $t_value = $data['listing_fields'][$name];
                                unset($data['listing_fields'][$name]);
                                $data['listing_fields'][$name][] = $t_value;
                            }
                            //echo '<pre> Field Elements: '.print_r($listing_fields[$name],TRUE).'</pre>';
                            $good_elements = [];
                            foreach ($data['listing_fields'][$name] as $fvalue) {
                                if (in_array($fvalue, $data_elements) && !in_array($fvalue, $good_elements)) {
                                    $good_elements[] = $fvalue;
                                }
                            }
                            //echo '<pre> Good Elements: '.print_r($good_elements,TRUE).'</pre>';
                            $insert_listing_fields[$name] = $good_elements;
                            break;
                        default:
                            $insert_listing_fields[$name] = $data['listing_fields'][$name];
                            break;
                    }
                }
            }

            $sql = 'UPDATE ' . $this->config['table_prefix'] . "listingsdbelements 
						SET listingsdbelements_field_value = :field_value 
						WHERE listingsdbelements_field_name = :field_name AND listingsdb_id = :listing_id";
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            foreach ($insert_listing_fields as $name => $value) {
                $stmt->bindValue(':listing_id', $data['listing_id'], PDO::PARAM_INT);
                if (is_array($value)) {
                    $sql_value = implode('||', $value);
                } else {
                    $sql_value = $value ?? '';
                }
                $stmt->bindValue(':field_name', $name);
                $stmt->bindValue(':field_value', $sql_value);
                try {
                    $stmt->execute();
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }
            }
        }
        //Deal with Hooks and social sites
        if (isset($data['listing_details']['active']) && $data['listing_details']['active'] && $oldstatus == 'no') {
            $hooks = new Hooks($this->dbh, $this->config);
            $hooks->load('afterListingActivated', $listing_id);
        }
        $listing_api = new ListingApi($this->dbh, $this->config);
        $seoresult = $listing_api->read(['listing_id' => $listing_id, 'fields' => ['listingsdb_title']]);
        $twitter_title = is_scalar($seoresult['listing']['listingsdb_title']) ? (string)$seoresult['listing']['listingsdb_title'] : '';

        $media = '';
        $media_remote = false;
        if ($this->config['twitter_listing_photo']) {
            $media_api = new MediaApi($this->dbh, $this->config);
            $result = $media_api->read([
                'media_type' => 'listingsimages',
                'media_parent_id' => $listing_id,
                'media_output' => 'URL',
            ]);

            if ($result['media_count'] > 0) {
                $media = $result['media_object'][0]['file_name'];
                $media_remote = $result['media_object'][0]['remote'] ?? false;
            }
        }

        // The add listing feature in OR creates an inactive listing via the
        // create method and uses the update method to send the tweet when the
        // status is changed from inactive to active.
        if (isset($data['listing_details']['active']) && $data['listing_details']['active'] && $oldstatus == 'no') {
            //treat this as being a new listing
            if ($this->config['twitter_new_listings']) {
                $twitter_url = ' ' . $this->config['baseurl'] . '/l/' . $listing_id;
                if (strlen($twitter_url) + strlen($twitter_title) > 140) {
                    $twitter_title = substr($twitter_title, 0, 137 - strlen($twitter_url)) . '...';
                }
                $twitter_post = $twitter_title . $twitter_url;
                $twitter_api = new TwitterApi($this->dbh, $this->config);
                $twitter_api->post(['message' => $twitter_post, 'media' => $media, 'media_remote' => $media_remote]);
            }
        } elseif (isset($data['listing_details']['active']) && $data['listing_details']['active']) {
            //listing was updated and tweet updates is set.
            if ($this->config['twitter_update_listings']) {
                $twitter_url = ' ' . $this->config['baseurl'] . '/l/' . $listing_id;
                if (strlen($twitter_url) + strlen($twitter_title) > 140) {
                    $twitter_title = substr($twitter_title, 0, 137 - strlen($twitter_url)) . '...';
                }
                $twitter_post = $twitter_title . $twitter_url;
                $twitter_api = new TwitterApi($this->dbh, $this->config);
                $twitter_api->post(['message' => $twitter_post, 'media' => $media, 'media_remote' => $media_remote]);
            }
        }
        // ta da! we're done...
        $admin_status = $login->verifyPriv('Admin');
        if (!$admin_status || !isset($data['disable_logs']) || !$data['disable_logs']) {
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->listing->update', 'log_message' => $lang['log_updated_listing'] . ' ' . $listing_id . ' by ' . $_SESSION['username']]);
        }
        //call the changed listing change hook
        $hooks = new Hooks($this->dbh, $this->config);
        $hooks->load('afterListingUpdate', $listing_id);
        return ['listing_id' => $listing_id];
    }

    /**
     * This API Command deletes listings.
     *
     * @param array{listing_id:integer} $data Expects an array containing the following array keys.
     *
     * <ul>
     *      <li>$data['listing_id'] - Number - Listing ID to delete</li>
     *  <ul>
     *
     * @return array{listing_id:integer}
     * @throws \Exception
     *
     */
    public function delete(array $data): array
    {
        global $lang;
        $login = new Login($this->dbh, $this->config);
        $login_status = $login->verifyPriv('Agent');
        if ($login_status !== true) {
            throw new Exception('Login Failure');
        }
        //Check that required settings were passed
        if (!isset($data['listing_id'])) {
            throw new Exception('listing_id: correct_parameter_not_passed');
        }

        //Check permissions
        $sql = 'SELECT userdb_id
				FROM ' . $this->config['table_prefix'] . 'listingsdb
				WHERE listingsdb_id = :listing_id';
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->bindValue(':listing_id', $data['listing_id'], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }

        // get main listings data
        $listing_agent = (int)$stmt->fetchColumn();
        //Verify we have permissions to add a listing
        if ($_SESSION['userID'] == $listing_agent) {
            $security = $login->verifyPriv('Agent');
            if ($security !== true) {
                throw new Exception('Permission Denied (agent)');
            }
        } else {
            //Check tha we can add listing for other users
            $security = $login->verifyPriv('edit_all_listings');
            if ($security !== true) {
                throw new Exception('Permission Denied (edit_all_listings)');
            }
        }

        //call the deleted listing plugin function
        $hooks = new Hooks($this->dbh, $this->config);
        $hooks->load('beforeListingDelete', $data['listing_id']);
        // now get all the images associated with an listing
        $media_api = new MediaApi($this->dbh, $this->config);
        $media_api->delete(['media_type' => 'listingsimages', 'media_parent_id' => $data['listing_id'], 'media_object_id' => '*', 'disable_logs' => true]);

        // now get all the vtours associated with an listing
        $media_api = new MediaApi($this->dbh, $this->config);
        $media_api->delete(['media_type' => 'listingsvtours', 'media_parent_id' => $data['listing_id'], 'media_object_id' => '*', 'disable_logs' => true]);

        // now get all the vtours associated with an listing
        $media_api = new MediaApi($this->dbh, $this->config);
        $media_api->delete(['media_type' => 'listingsfiles', 'media_parent_id' => $data['listing_id'], 'media_object_id' => '*', 'disable_logs' => true]);

        // delete a listing
        $configured_langs = explode(',', $this->config['configured_langs']);
        foreach ($configured_langs as $configured_lang) {
            $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . $configured_lang . "_listingsdb WHERE listingsdb_id = :listing_id";
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->bindValue(':listing_id', $data['listing_id'], PDO::PARAM_INT);
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . $configured_lang . "_listingsdbelements WHERE listingsdb_id = :listing_id";

            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->bindValue(':listing_id', $data['listing_id'], PDO::PARAM_INT);
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
        }

        // Delete from favorites
        $sql = 'DELETE FROM ' . $this->config['table_prefix'] . "userfavoritelistings WHERE listingsdb_id = :listing_id";
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->bindValue(':listing_id', $data['listing_id'], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        // ta da! we're done...
        $log_api = new LogApi($this->dbh, $this->config);
        $log_api->create(['log_api_command' => 'api->listing->delete', 'log_message' => $lang['log_deleted_listing'] . ' ' . $data['listing_id'] . ' by ' . $_SESSION['username']]);
        //call the deleted listing plugin function
        $hooks = new Hooks($this->dbh, $this->config);
        $hooks->load('afterListingDelete', $data['listing_id']);
        return ['listing_id' => $data['listing_id']];
    }

    /**
     * @param int $agentId
     * @return int
     * @throws \Exception
     */
    private function getListingCountByAgentId(int $agentId): int
    {
        $sql = 'SELECT count(listingsdb_id) as listing_count FROM ' . $this->config['table_prefix'] . 'listingsdb WHERE userdb_id = :agentID';
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->bindValue(':agentID', $agentId, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        return (int)$stmt->fetchColumn();
    }
}
