<?php

declare(strict_types=1);

namespace OpenRealty\Addons\TestAddon;

use Exception;
use OpenRealty\BaseClass;
use OpenRealty\Login;
use OpenRealty\Misc;

/**
 * TestAddpnm Addon For Testing Open-Realty
 *
 *
 * Version 3.1
 *  - Open-Realty 3.7+ ssupport
 *
 * Version 3.0  - 12/10/2021
 *  - Open source license
 *
 *
 * Version 2.2
 * - released at Dec,15/2010
 * - fixed $login::loginCheck calls
 * - improved the code and tested with Open-Realty® v.3.0.12
 *
 */
class Addon extends BaseClass
{
    private string $addonName = 'TestAddon';

    /**
     * installAddon()
     * This function should create and update any necessary database tables.
     * Developers can store version information for the add-on in the "addons" table
     * and use it to determine if an install or upgrade is needed.
     *
     * @return
     */
    public function installAddon(): bool
    {
        global $ORconn;
        $login = new Login($this->dbh, $this->config);
        $security = $login->loginCheck('Admin', true);
        if ($security !== true) {
            // DO NOTHING, NOT ADMIN LOGGED
        } else {
            $current_version = '3.0';
            $misc = new Misc($this->dbh, $this->config);
            $sql = 'SELECT addons_version FROM ' . $this->config['table_prefix_no_lang'] . 'addons WHERE addons_name = "' . $this->addonName . '"';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                die('Error: ' . $sql);
            }
            $addon_version = trim((string)$recordSet->fields('addons_version'));
            if ($addon_version == '') {
                $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . 'addons (addons_version, addons_name) VALUES ("' . $current_version . '", "' . $this->addonName . '")';
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    die('Error: ' . $sql);
                }
                $misc->logAction(strtoupper($this->addonName) . ' add-on v.' . $current_version . ' installed.');
                return true;
            } elseif ($addon_version != $current_version) {
                switch ($addon_version) {
                    case '1':
                    case '2':
                    case '2.1':
                    case '2.2':
                    case '2.3':
                    default:
                        $sql = 'UPDATE ' . $this->config['table_prefix_no_lang'] . 'addons SET addons_version = "' . $current_version . '" WHERE addons_name = "' . $this->addonName . '"';
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            die('Error: ' . $sql);
                        }
                        $misc->logAction(strtoupper($this->addonName) . ' add-on updated to v.' . $current_version);
                        break;
                }
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * showAdminIcons()
     * This function should return an array of the html links that should be shown
     * on the administrative page.
     *
     * @return string[] should return each link that shoudl be shown in the admin
     * section. Should return a string if it is a single link, or an array for
     * multiple links.
     */
    public function showAdminIcons(): array
    {
        return ['<a href="index.php?action=addon_' . $this->addonName . '_admin" title="' . $this->addonName . '">' . $this->addonName . '</a>'];
    }

    /**
     * loadTemplate()
     * This should return an array with all the template tags for Open-Realty's
     * template engine to parse.
     *
     * @return array List of template_fields
     */
    public function loadTemplate(): array
    {
        return array('addon_' . $this->addonName . '_link');
    }

    /**
     * runActionUserTemplate()
     * This function handles user $_GET[] actions related to the add-on. Function
     * must be named using this method: addon_addonname_description.
     *
     * @return string Should return all contents that should be displayed when an
     * add-on specific $_GET['action'] is called.
     */
    public function runActionUserTemplate()
    {
        switch ($_GET['action']) {
            case 'addon_' . $this->addonName . '_showpage1':
                $data = $this->displayAddonPage();
                break;
            default:
                $data = '';
                break;
        }
        return $data;
    }

    /**
     * runActionAdminTemplate()
     * This function handles administrative $_GET[] actions related to the add-on.
     * Function must be named using this method: addon_addonname_description.
     *
     * @return string Should return all contents that should be displayed when a
     * add-on specific $_GET['action'] is called.
     */
    public function runActionAdminTemplate()
    {
        switch ($_GET['action']) {
            case 'addon_' . $this->addonName . '_admin':
                $data = $this->displayAdminPage();
                break;
            default:
                $data = '';
                break;
        }
        return $data;
    }

    /**
     * runTemplateUserFields()
     * This function handles all the replacement of {template_tags} with the actual
     * content. All tags setup here must also be added to teh load_template function
     * in order for open-realty to parse them.
     *
     * @param string $tag
     * @return string Should return all contents that should be displayed when an
     * add-on specific $tag is called.
     */
    public function runTemplateUserFields(string $tag = '')
    {
        switch ($tag) {
            case 'addon_' . $this->addonName . '_link':
                $data = $this->displayAddonLink();
                break;
            default:
                $data = '';
                break;
        }
        return $data;
    }

    /**
     * addonManagerHelp()
     * This function provides a link to the documentation for the add-on, provides a
     * list of the add-on's template tags and a list of the add-on's action URL's.
     *
     * @return array should return an array of the following values: TEMPLATE TAGS,
     * ACTION URLS, DOCUMENTATION URL - The template tags and action urls are to be
     * arrays where the array key is the template tag/action url and the value is
     * the description of the template tag/action url.
     */
    public function addonManagerHelp()
    {
        $template_tags = array();
        $action_urls = array();
        $doc_url = 'http://wiki.open-realty.org/Addon_Documentation';
        $template_tags['addon_' . $this->addonName . '_link'] = 'Main Template File // Printer Friendly - This tag will display the link to the ' . $this->addonName . ' Add-on Page.';
        $action_urls['addon_' . $this->addonName . '_showpage1'] = 'Public Site – This action will display the Add-on\'s demo page.';
        $action_urls['addon_' . $this->addonName . '_admin'] = 'Admin Site – This action will display the Add-on\'s admin page.';
        return array($template_tags, $action_urls, $doc_url);
    }

    /**
     * uninstallTables()
     * This function runs the SQL commands to delete any tables that were added
     * during the installation of the add-on.
     *
     * @return boolean Should return true to indicate the add-on's tables were removed
     * properly.
     */
    public function uninstallTables(): bool
    {
        global $ORconn;
        $login = new Login($this->dbh, $this->config);
        $security = $login->loginCheck('Admin', true);
        if ($security) {
            $misc = new Misc($this->dbh, $this->config);
            $sql_uninstall = [];
            $sql_uninstall[] = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . 'addons WHERE addons_name = "' . $this->addonName . '"';
            $error = false;
            foreach ($sql_uninstall as $elementContents) {
                $recordSet = $ORconn->Execute($elementContents);
                if (is_bool($recordSet)) {
                    echo '<p style="font-weight:bold; color:red;">ERROR - ' . $elementContents . '</p>';
                    $error = true;
                }
            }
            if (!$error) {
                $misc->logAction($this->addonName . ' add-on successfully deleted.');
                return true;
            }
        }
        return false;
    }

    /**
     * This function handles the add-on's $_GET[] actions for calls to the ajax.php file from the backend of
     * Open-Realty. Each get action should have the function to be called defined. The action must be named using this
     * method: addon_name_description. Here is an example of proper code for this function:
     */
    public function runActionAjax(): string
    {
        switch ($_GET['action']) {
            case 'addon_' . $this->addonName . '_get_admin_ajax':
                $data = $this->getAdminAjax();
                break;
            default:
                $data = '';
                break;
        } // End switch ($_GET['action'])
        return $data;
    }

    /**
     * This function handles the add-on's $_GET[] actions for calls to the ajax.php file from the frontend of
     * Open-Realty. Each get action should have the function to be called defined. The action must be named using this
     * method: addon_name_description. Here is an example of proper code for this function:
     */
    public function runUserActionAjax(): string
    {
        switch ($_GET['action']) {
            case 'addon_' . $this->addonName . '_get_user_ajax':
                $data = $this->getUserAjax();
                break;
            default:
                $data = '';
                break;
        } // End switch ($_GET['action'])
        return $data;
    }

// Add-on Specific Function
    public function displayAddonLink(): string
    {
        return '<a href="index.php?action=addon_' . $this->addonName . '_showpage1" title="' . $this->addonName . ' Test">' . $this->addonName . ' Link</a>';
    }

// Add-on Specific Function
    public function displayAddonPage(): string
    {
        $mediaApi = $this->newMediaApi();
        try {
            $data = $mediaApi->read(array('media_type' => 'listingsimages', 'media_parent_id' => 1, 'media_output' => 'URL'));
        } catch (Exception $e) {
            die($e->getMessage());
        }
        return $data['media_object'][0]['caption'];
    }

// Add-on Specific Function
    public function displayAdminPage(): string
    {
        $display = 'This is an Add-on page (replaced by the add-on tag {addon_' . $this->addonName . '_admin}).';
        //Do a simple API Call to get the number of listings in this site.
        $ListingApi = $this->newListingApi();
        try {
            $api_result = $ListingApi->search(array('parameters' => array(), 'limit' => 0, 'offset' => 0, 'count_only' => true));
        } catch (Exception $e) {
            die($e->getMessage());
        }

        $num_listing = $api_result['listing_count'];
        $display .= '<br /> This site has ' . $num_listing . ' listings';

        //No lets add some code to the page to do an admin side ajax call.
        $display .= '<br /><span class="ajaxmessage"></span><script type="text/javascript">
	$(document).ready(function() {
		$.get("{baseurl}/admin/ajax.php?action=addon_' . $this->addonName . '_get_admin_ajax",
			function(data){
				if(!data.error){
					status_msg(data.message);
				}else{
					status_error("Our Ajax call failed");
				}
			},
			"json");
	});
	</script>';

        return $display;
    }

//Add-on Specific Function
    public function getAdminAjax(): string
    {
        //Seem I am using json in this example, i am setting the content-type header to json.
        header('Content-type: application/json');
        $array = array('error' => false, 'message' => 'This is our ajax response for our backend');
        return json_encode($array);
    }

//Add-on Specific Function
    public function getUserajax(): string
    {
        header('Content-type: application/json');
        //Seem I am using json in this example, i am setting the content-type header to json.
        $array = array('error' => false, 'message' => 'This is our ajax response for our frontend');
        return json_encode($array);
    }
}