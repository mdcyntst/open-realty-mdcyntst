<?php

namespace Tests\Integration;

use OpenRealty\Api\Commands\PageApi;
use Tests\Support\IntegrationTester;

/**
 * Integration Test for the Media API
 */
class PageTest extends TestSetup
{
    /**
     * @var IntegrationTester
     */
    protected $tester;

    // phpcs:ignore
    protected function _before(): void
    {
        parent::_before();
    }

    // phpcs:ignore
    protected function _after(): void
    {
        parent::_after();
    }

    /**
     * Test Creating a page as a guest user, ensure we fail.
     *
     * @return void
     */
    public function testPageApiCreateGuest()
    {
        $pageApi = new PageApi($this->dbh, $this->config);

        $this->expectExceptionMessage('Login Failure/no permission');
        $pageApi->create([
            'pagesmain_title' => 'testPageApiCreateGuest',
            'pagesmain_full' => '<b>testPageApiCreateGuest</b><br /><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
            'pagesmain_published' => 0,
            'pagesmain_description' => '',
            'pagesmain_keywords' => '',
        ]);
        $this->tester->dontSeeInDatabase('default_en_pagesmain', ['pagesmain_title' => 'testPageApiCreateGuest']);
    }

    public function testPageApiCreateAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $pageApi = new PageApi($this->dbh, $this->config);
        $results = $pageApi->create([
            'pagesmain_title' => 'testPageApiCreateGuest',
            'pagesmain_full' => '<b>testPageApiCreateGuest</b><br /><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
            'pagesmain_published' => 0,
            'pagesmain_description' => '',
            'pagesmain_keywords' => '',
        ]);
        $this->assertArrayHasKey('pagesmain_id', $results);
        $this->tester->seeInDatabase('default_en_pagesmain', ['pagesmain_title' => 'testPageApiCreateGuest']);
    }

    public function testPageApiDeleteGuest()
    {
        $pageApi = new PageApi($this->dbh, $this->config);
        $this->tester->seeInDatabase('default_en_pagesmain', ['pagesmain_id' => 2]);
        $this->expectExceptionMessage('Login Failure/no permission');
        $pageApi->delete([
            'pagesmain_id' => 2,
        ]);
        $this->tester->seeInDatabase('default_en_pagesmain', ['pagesmain_id' => 2]);
    }

    public function testPageApiDeleteAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $pageApi = new PageApi($this->dbh, $this->config);
        $this->tester->seeInDatabase('default_en_pagesmain', ['pagesmain_id' => 2]);
        $result = $pageApi->delete([
            'pagesmain_id' => 2,
        ]);
        $this->assertEquals(['pagesmain_id' => 2], $result);
        $this->tester->dontSeeInDatabase('default_en_pagesmain', ['pagesmain_id' => 2]);
    }

    public function testPageApiDeletePage1Admin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $pageApi = new PageApi($this->dbh, $this->config);
        $this->tester->seeInDatabase('default_en_pagesmain', ['pagesmain_id' => 1]);
        $this->expectExceptionMessage('You cannot delete Page ID 1');
        $result = $pageApi->delete([
            'pagesmain_id' => 1,
        ]);
        $this->assertEquals(['pagesmain_id' => 2], $result);
        $this->tester->dontSeeInDatabase('default_en_pagesmain', ['pagesmain_id' => 1]);
    }

    public function testPageApiUpdateGuest()
    {
        $pageApi = new PageApi($this->dbh, $this->config);
        $this->tester->seeInDatabase('default_en_pagesmain', ['pagesmain_id' => 2, 'pagesmain_title' => 'Contact Us']);
        $this->expectExceptionMessage('Login Failure/no permission');
        $pageApi->update([
            'pagesmain_id' => 2,
            'pagesmain_title' => 'testPageApiUpdateGuest',
        ]);
        $this->tester->seeInDatabase('default_en_pagesmain', ['pagesmain_id' => 2, 'pagesmain_title' => 'Contact Us']);
    }

    public function testPageApiUpdateAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $pageApi = new PageApi($this->dbh, $this->config);
        $this->tester->seeInDatabase('default_en_pagesmain', ['pagesmain_id' => 2, 'pagesmain_title' => 'Contact Us']);
        $pageApi->update([
            'pagesmain_id' => 2,
            'pagesmain_title' => 'testPageApiUpdateGuest',
        ]);
        $this->tester->seeInDatabase('default_en_pagesmain', ['pagesmain_id' => 2, 'pagesmain_title' => 'testPageApiUpdateGuest']);
    }

    public function testPageApiUpdateSeoTitleAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $pageApi = new PageApi($this->dbh, $this->config);
        $this->tester->seeInDatabase('default_en_pagesmain', ['pagesmain_id' => 2, 'pagesmain_title' => 'Contact Us', 'page_seotitle' => 'contact_us']);
        $pageApi->update([
            'pagesmain_id' => 2,
            'pagesmain_title' => 'testPageApiUpdateGuest',
            'page_seotitle' => 'contact_us_now',
        ]);
        $this->tester->seeInDatabase('default_en_pagesmain', ['pagesmain_id' => 2, 'pagesmain_title' => 'testPageApiUpdateGuest', 'page_seotitle' => 'contact_us_now']);
    }

    public function testPageApiUpdateDuplicateTitleAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $pageApi = new PageApi($this->dbh, $this->config);
        $this->tester->seeInDatabase('default_en_pagesmain', ['pagesmain_id' => 2, 'pagesmain_title' => 'Contact Us']);
        $this->tester->seeInDatabase('default_en_pagesmain', ['pagesmain_id' => 3, 'pagesmain_title' => 'About Us']);
        $this->expectExceptionMessage('Your page Title must be unique!');
        $pageApi->update([
            'pagesmain_id' => 2,
            'pagesmain_title' => 'About Us',
        ]);
        $this->tester->seeInDatabase('default_en_pagesmain', ['pagesmain_id' => 2, 'pagesmain_title' => 'Contact Us']);
    }
}
