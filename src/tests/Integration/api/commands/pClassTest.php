<?php

namespace Tests\Integration;

use OpenRealty\Api\Commands\PClassApi;
use Tests\Support\IntegrationTester;

/**
 * Integration Test for the Media API
 */
class PClassTest extends TestSetup
{
    /**
     * @var IntegrationTester
     */
    protected $tester;

    // phpcs:ignore
    protected function _before(): void
    {
        parent::_before();
    }

    // phpcs:ignore
    protected function _after(): void
    {
        parent::_after();
    }

    /**
     * Test creating a user photo from local file.
     *
     * @return void
     */
    public function testPClassApiMetadata()
    {
        $pClassApi = new PClassApi($this->dbh, $this->config);
        $results = $pClassApi->metadata([]);
        codecept_debug($results);
        $this->assertEquals([
            1 => [
                "name" => "Home",
                "rank" => 1,
            ],
            2 => [
                "name" => "Land",
                "rank" => 2,
            ],
            3 => [
                "name" => "Farms",
                "rank" => 3,
            ],
            4 => [
                "name" => "Commercial",
                "rank" => 4,
            ],
            5 => [
                "name" => "Rental",
                "rank" => 5,
            ],
        ], $results['metadata']);
    }

    public function testPClassApiRead()
    {
        $pClassApi = new PClassApi($this->dbh, $this->config);
        $results = $pClassApi->read(['class_id' => 2]);
        codecept_debug($results);
        $this->assertEquals([
            'class_id' => 2,
            "class_name" => "Land",
            "class_rank" => 2,
        ], $results);
    }

    public function testPClassApiCreateGuest()
    {
        $pClassApi = new PClassApi($this->dbh, $this->config);
        $this->expectExceptionMessage("Login Failure");
        $results = $pClassApi->create(['class_name' => 'testPClassApiCreateGuest', 'class_rank' => 6]);
        codecept_debug($results);
    }

    public function testPClassApiCreateAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $pClassApi = new PClassApi($this->dbh, $this->config);
        $results = $pClassApi->create(['class_name' => 'testPClassApiCreateGuest', 'class_rank' => 6]);
        codecept_debug($results);
        $this->assertEquals(6, $results['class_id']);
        $this->tester->seeInDatabase('default_en_class', ['class_id' => 6, 'class_name' => 'testPClassApiCreateGuest', 'class_rank' => 6]);
    }
}
