<?php

namespace Tests\Integration;

use OpenRealty\Api\Commands\FieldsApi;
use Tests\Support\IntegrationTester;

/**
 * Test the Listing API
 */
class FieldsTest extends TestSetup
{
    /**
     * @var IntegrationTester
     */
    protected $tester;

    /**
     * Test the Blog Search API respects the count_only parameter being true
     *
     * @return void
     */
    public function testFieldsMetadataListingSingleField(): void
    {
        $fieldsApi = new FieldsApi($this->dbh, $this->config);
        $results = $fieldsApi->metadata(['field_id' => 1, 'resource' => 'listing']);
        $this->assertEquals(1, count($results['fields']));
        $this->assertEquals([1 => [
            "field_id" => 1,
            "field_type" => "text",
            "field_name" => "city",
            "field_caption" => "City",
            "default_text" => "",
            "field_elements" => [0 => ""],
            "rank" => 2,
            "search_rank" => 1,
            "search_result_rank" => 1,
            "required" => "Yes",
            "location" => "top_left",
            "display_on_browse" => "Yes",
            "searchable" => false,
            "search_type" => "",
            "search_label" => "",
            "search_step" => "0",
            "display_priv" => 0,
            "field_length" => 0,
            "tool_tip" => "",
        ]], $results['fields']);
    }

    public function testFieldsMetadataListingMultipleFields(): void
    {
        $fieldsApi = new FieldsApi($this->dbh, $this->config);
        $results = $fieldsApi->metadata(['resource' => 'listing']);
        $this->assertEquals(22, count($results['fields']));
    }

    public function testFieldsMetadataListingMultipleFieldsBrowseableOnly(): void
    {
        $fieldsApi = new FieldsApi($this->dbh, $this->config);
        $results = $fieldsApi->metadata(['resource' => 'listing', 'browseable_only' => true]);
        $this->assertEquals(8, count($results['fields']));
    }

    public function testFieldsMetadataListingMultipleFieldsSearchableOnly(): void
    {
        $fieldsApi = new FieldsApi($this->dbh, $this->config);
        $results = $fieldsApi->metadata(['resource' => 'listing', 'searchable_only' => true]);
        $this->assertEquals(2, count($results['fields']));
    }

    public function testFieldsValuesCity(): void
    {
        $fieldsApi = new FieldsApi($this->dbh, $this->config);
        $results = $fieldsApi->values(['field_name' => 'city', 'field_type' => 'text', 'pclass' => [1]]);
        codecept_debug($results);
        $this->assertEquals(1, count($results['field_values']));
        $this->assertEquals(1, count($results['field_counts']));
        $this->assertEquals("Washington", $results['field_values'][0]);
        $this->assertEquals(3, $results['field_counts']["Washington"]);
    }

    public function testFieldsValuesPrice(): void
    {
        $fieldsApi = new FieldsApi($this->dbh, $this->config);
        $results = $fieldsApi->values(['field_name' => 'price', 'field_type' => 'number', 'pclass' => [1]]);
        codecept_debug($results);
        $this->assertEquals(3, count($results['field_values']));
        $this->assertEquals([
            0 => "1187710",
            1 => "2500000",
            2 => "133000000",
        ], $results['field_values']);
        $this->assertEquals(3, count($results['field_counts']));
        $this->assertEquals([
            '1187710' => 1,
            '2500000' => 1,
            '133000000' => 1,
        ], $results['field_counts']);
    }

    public function testFieldsCreateListingText(): void
    {
        $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $fieldsApi = new FieldsApi($this->dbh, $this->config);
        $newField = [
            'resource' => 'listing',
            'class' => [1],
            'field_type' => 'text',
            'field_name' => 'TestField',
            'field_caption' => 'Test Field',
            'default_text' => 'Enter Somethinga',
            'field_elements' => [],
            'rank' => 23,
            'search_rank' => 5,
            'search_result_rank' => 5,
            'required' => true,
            'location' => 'top_left',
            'display_on_browse' => false,
            'search_step' => '',
            'display_priv' => 0,
            'field_length' => 25,
            'tool_tip' => 'Enter Text Up To 25 Chars',
            'search_label' => 'Search for a test string',
            'search_type' => 'ptext',
            'searchable' => true,
        ];
        $results = $fieldsApi->create($newField);
        codecept_debug($results);
        $this->assertArrayHasKey('field_id', $results);
        $this->tester->seeInDatabase('default_en_listingsformelements', ['listingsformelements_id' => $results['field_id']]);
    }

    public function testFieldsCreateListingTextGuest(): void
    {
        $fieldsApi = new FieldsApi($this->dbh, $this->config);
        $newField = [
            'resource' => 'listing',
            'class' => [1],
            'field_type' => 'text',
            'field_name' => 'TestField',
            'field_caption' => 'Test Field',
            'default_text' => 'Enter Somethinga',
            'field_elements' => [],
            'rank' => 23,
            'search_rank' => 5,
            'search_result_rank' => 5,
            'required' => true,
            'location' => 'top_left',
            'display_on_browse' => false,
            'search_step' => '',
            'display_priv' => 0,
            'field_length' => 25,
            'tool_tip' => 'Enter Text Up To 25 Chars',
            'search_label' => 'Search for a test string',
            'search_type' => 'ptext',
            'searchable' => true,
        ];
        $this->expectExceptionMessage('Login Failure');
        $results = $fieldsApi->create($newField);
    }

    public function testFieldsAssignClassGuest(): void
    {
        $fieldsApi = new FieldsApi($this->dbh, $this->config);
        $assignment = [
            'class' => 1,
            'field_id' => 22,
        ];
        $this->expectExceptionMessage('Login Failure');
        $fieldsApi->assignClass($assignment);
    }

    public function testFieldsAssignClassInvalidClass(): void
    {
        $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $fieldsApi = new FieldsApi($this->dbh, $this->config);
        $assignment = [
            'class' => 10,
            'field_id' => 22,
        ];
        $this->expectExceptionMessage('Invalid Class');
        $fieldsApi->assignClass($assignment);
    }

    public function testFieldsAssignClass(): void
    {
        $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        //Add New Class For Test
        $class_id = $this->tester->haveInDatabase('default_en_class', ['class_name' => 'Test Class', 'class_rank' => 6]);
        $this->tester->dontSeeInDatabase('default_classformelements', ['listingsformelements_id' => 1, 'class_id' => $class_id]);
        $fieldsApi = new FieldsApi($this->dbh, $this->config);
        $assignment = [
            'class' => $class_id,
            'field_id' => 1,
        ];
        $fieldsApi->assignClass($assignment);
        $this->tester->seeInDatabase('default_classformelements', ['listingsformelements_id' => 1, 'class_id' => $class_id]);
    }

    /**
     * Test the Blog Search API respects the count_only parameter being true
     *
     * @return void
     */
    public function testFieldsMetadataAgentSingleField(): void
    {
        $fieldsApi = new FieldsApi($this->dbh, $this->config);
        $results = $fieldsApi->metadata(['field_id' => 4, 'resource' => 'agent']);
        codecept_debug($results);
        $this->assertEquals(1, count($results['fields']));
        $this->assertEquals([4 => [
            "field_id" => 4,
            "field_type" => "text",
            "field_name" => "phone",
            "field_caption" => "Phone",
            "default_text" => "",
            "field_elements" => [0 => ""],
            "rank" => 1,
            "required" => "No",
            "display_priv" => 0,
            "tool_tip" => "",
        ]], $results['fields']);
    }

    public function testFieldsMetadataMemberSingleField(): void
    {
        $fieldsApi = new FieldsApi($this->dbh, $this->config);
        $results = $fieldsApi->metadata(['field_id' => 4, 'resource' => 'member']);
        codecept_debug($results);
        $this->assertEquals(1, count($results['fields']));
        $this->assertEquals([4 => [
            "field_id" => 4,
            "field_type" => "text",
            "field_name" => "phone",
            "field_caption" => "Phone",
            "default_text" => "",
            "field_elements" => [0 => ""],
            "rank" => 1,
            "required" => "No",
            "tool_tip" => "",
        ]], $results['fields']);
    }

    public function testFieldsMetadataFeedbackSingleField(): void
    {
        $fieldsApi = new FieldsApi($this->dbh, $this->config);
        $results = $fieldsApi->metadata(['field_id' => 1, 'resource' => 'feedback']);
        codecept_debug($results);
        $this->assertEquals(1, count($results['fields']));
        $this->assertEquals([1 => [
            "field_id" => 1,
            "field_type" => "select",
            "field_name" => "source",
            "field_caption" => "How did you hear about us?",
            "default_text" => "",
            "field_elements" => [
                0 => "Referral",
                1 => "Phone Book",
                2 => "TV ad",
                3 => "Radio ad",
                4 => "Newspaper",
                5 => "Search Engine",
            ],
            "rank" => 1,
            "required" => "No",
            "location" => "center",
            "tool_tip" => "",
        ]], $results['fields']);
    }
}
