<?php

namespace Tests\Integration;

use OpenRealty\Api\Commands\UserApi;
use Tests\Support\IntegrationTester;
use OpenRealty\Api\Commands\ListingApi;
use Codeception\Example;

/**
 * Test the Listing API
 */
class ListingTest extends TestSetup
{
    /**
     * @var IntegrationTester
     */
    protected $tester;

    // phpcs:ignore
    protected function _before()
    {
        parent::_before();
    }

    // phpcs:ignore
    protected function _after()
    {
        parent::_after();
    }

    /**
     * Test the Listing Search API respects the count_only parameter being true
     *
     * @return void
     */
    public function testListingSearchCountOnly()
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $data = ['parameters' => [], 'limit' => 0, 'offset' => 0, 'count_only' => 1];
        $result = $listing_api->search($data);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('listing_count', $result);
        $this->assertArrayHasKey('listings', $result);
        $this->assertEquals(4, $result['listing_count']);
        $this->assertEmpty($result['listings']);
    }


    /**
     * Test the Listing Search API respects the count_only parameter being true
     *
     * @param array $sortby
     * @return void
     * @example [["listingsdb_hit_count"]]
     * @example [["listingsdb_title"]]
     * @example [["listingsdb_id"]]
     * @example [["random"]]
     * @example [["listingsdb_featured"]]
     * @example [["listingsdb_last_modified"]]
     * @example [["pclass"]]
     * @example [["price"]]
     * This test sorting when we pass an invalid sort option "featured"
     * @example [["featured", "price"]]
     * @example [["listingsdb_featured", "price"]]
     */
    public function testListingSearchSortBy($sortby)
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $data = ['parameters' => [], 'limit' => 0, 'offset' => 0, 'count_only' => 0, 'sortby' => $sortby];
        $result = $listing_api->search($data);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('listing_count', $result);
        $this->assertArrayHasKey('listings', $result);
        $this->assertEquals(4, $result['listing_count']);
    }

    /**
     * Test the Listing Search API and search for prices in a range (min/max)
     *
     * @return void
     */
    public function testListingSearchPriceMinMax()
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $data = ['parameters' => ['price-min' => '1187000', 'price-max' => '119818700'], 'limit' => 0, 'offset' => 0, 'count_only' => 1];
        $result = $listing_api->search($data);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('listing_count', $result);
        $this->assertArrayHasKey('listings', $result);
        $this->assertEquals(3, $result['listing_count']);
        $this->assertEmpty($result['listings']);
    }

    /**
     * Test the Listing Search API and search for text
     *
     * @return void
     */
    public function testListingSearchTextSearch()
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $data = ['parameters' => ['searchtext' => 'white'], 'limit' => 0, 'offset' => 0, 'count_only' => 1];
        $result = $listing_api->search($data);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('listing_count', $result);
        $this->assertArrayHasKey('listings', $result);
        $this->assertEquals(1, $result['listing_count']);
        $this->assertEmpty($result['listings']);
    }

    /**
     * Test the Listing Search API respects the count_only parameter being false and returns the listing IDs instead.
     *
     * @return void
     */
    public function testListingSearchReturnsListingIds()
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $data = ['parameters' => [], 'limit' => 0, 'offset' => 0, 'count_only' => 0];
        $result = $listing_api->search($data);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('listing_count', $result);
        $this->assertArrayHasKey('listings', $result);
        $this->assertEquals(4, $result['listing_count']);
        $this->assertEqualsCanonicalizing([1, 2, 3, 4], $result['listings']);
    }

    /**
     * Test the the Listing Search API respects the limit parameter
     *
     * @return void
     */
    public function testListingSearchLimit()
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $data = ['parameters' => [], 'limit' => 2, 'offset' => 0, 'count_only' => 0];
        $result = $listing_api->search($data);

        $this->assertIsArray($result);
        $this->assertArrayHasKey('listing_count', $result);
        $this->assertArrayHasKey('listings', $result);
        $this->assertEquals(2, $result['listing_count']);
        $this->assertEqualsCanonicalizing([1, 2], $result['listings']);
    }

    /**
     * Test Listing Search API, Offset requires a limit, if we do not set both we should get all listings.
     *
     * @return void
     */
    public function testListingSearchOffsetWithoutLimit()
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $data = ['parameters' => [], 'limit' => 0, 'offset' => 1, 'count_only' => 0];
        $result = $listing_api->search($data);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('listing_count', $result);
        $this->assertArrayHasKey('listings', $result);
        $this->assertEquals(4, $result['listing_count']);
        $this->assertEqualsCanonicalizing([1, 2, 3, 4], $result['listings']);
    }

    /**
     * Test Listing Search API correctly offsets the listings when we pass a limit & offset
     *
     * @return void
     */
    public function testListingSearchOffsetWithLimit()
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $data = ['parameters' => [], 'limit' => 2, 'offset' => 1, 'count_only' => 0];
        $result = $listing_api->search($data);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('listing_count', $result);
        $this->assertArrayHasKey('listings', $result);
        $this->assertEquals(2, $result['listing_count']);
        $this->assertEqualsCanonicalizing([2, 3], $result['listings']);
    }

    /**
     * Test the Listing Read API allows reading internal fields, in this case the listingsdb_pclass_id
     *
     * @return void
     */
    public function testListingReadInternalFields()
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $data = ['listing_id' => 1, 'fields' => ['listingsdb_id', 'listingsdb_pclass_id']];
        $result = $listing_api->read($data);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('listing', $result);
        $this->assertArrayHasKey('listingsdb_id', $result['listing']);
        $this->assertArrayHasKey('listingsdb_pclass_id', $result['listing']);
        $this->assertEquals(1, $result['listing']['listingsdb_id']);
        $this->assertEquals(1, $result['listing']['listingsdb_pclass_id']);
    }

    /**
     * Test the Listing Read API allows reading custom fields, in this case the price
     *
     * @return void
     */
    public function testListingReadCustomFields()
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $data = ['listing_id' => 1, 'fields' => ['listingsdb_id', 'price']];
        $result = $listing_api->read($data);

        $this->assertIsArray($result);
        $this->assertArrayHasKey('listing', $result);
        $this->assertArrayHasKey('listingsdb_id', $result['listing']);
        $this->assertArrayHasKey('price', $result['listing']);
        $this->assertEquals(1, $result['listing']['listingsdb_id']);
        $this->assertEquals(2500000, $result['listing']['price']);
    }

    /**
     * Test the Listing Read API allows reading custom fields, in this case the price
     *
     * @return void
     */
    public function testListingReadFieldsAll()
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $data = ['listing_id' => 1, 'fields' => []];
        $result = $listing_api->read($data);
        codecept_debug($result);
        $this->assertArrayHasKey('listing', $result);
        $this->assertEquals([
            "listingsdb_id" => 1,
            "listingsdb_pclass_id" => 1,
            "userdb_id" => 1,
            "listingsdb_title" => "White House",
            "listingsdb_expiration" => "2013-01-01",
            "listingsdb_notes" => "This is an example listing of the white hosue!",
            "listingsdb_creation_date" => "2013-01-01",
            "listingsdb_last_modified" => "2012-01-01 22:39:58",
            "listingsdb_hit_count" => 17,
            "listingsdb_featured" => "yes",
            "listingsdb_active" => "yes",
            "listingsdb_mlsexport" => "no",
            "listing_seotitle" => "white_house",
            "home_features" => [
                0 => "Balcony",
                1 => "Patio/Deck",
                2 => "Waterfront",
                3 => "Dishwasher",
                4 => "Disposal",
                5 => "Gas Range",
                6 => "Microwave",
                7 => "Washer/Dryer",
                8 => "Carpeted Floors",
                9 => "Hardwood Floors",
                10 => "Air Conditioning",
                11 => "Alarm",
                12 => "Cable/Satellite TV",
                13 => "Fireplace",
                14 => "Wheelchair Access",
            ],
            "community_features" => [
                0 => "Fitness Center",
                1 => "Golf Course",
                2 => "Pool",
                3 => "Spa/Jacuzzi",
                4 => "Sports Complex",
                5 => "Tennis Courts",
                6 => "Bike Paths",
                7 => "Boating",
                8 => "Courtyard",
                9 => "Playground/Park",
                10 => "Association Fee",
                11 => "Clubhouse",
                12 => "Controlled Access",
                13 => "Public Transportation",
            ],
            "mls" => "13013",
            "status" => "Active",
            "prop_tax" => "0",
            "garage_size" => "40 car",
            "lot_size" => "20 Acres",
            "sq_feet" => "35000",
            "floors" => "6",
            "year_built" => "1800",
            "baths" => "35",
            "beds" => "10",
            "full_desc" => "Exclusive to this site! For two hundred years, the White House has stood as a symbol of the Presidency, the United States government, and the American people.",
            "price" => "2500000",
            "neighborhood" => "Capitol",
            "zip" => "20500",
            "state" => "DC",
            "city" => "Washington",
            "address" => "1600 Pennsylvania Avenue",
            "country" => "USA",
            "latitude" => "38.897969",
            "longitude" => "-77.036605",
        ], $result['listing']);
    }


    /**
     * Test that we can update a listing custom field, price, with Listing Update and then read the change back with
     * the Listing Read API.
     *
     * @return void
     */
    public function testListingUpdateCustomFields()
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $data = ['listing_id' => 1, 'class_id' => 1, 'listing_fields' => ['price' => 500]];
        $result = $listing_api->update($data);

        $this->assertIsArray($result);

        $data = ['listing_id' => 1, 'fields' => ['listingsdb_id', 'price']];
        $result = $listing_api->read($data);

        $this->assertIsArray($result);
        $this->assertArrayHasKey('listing', $result);
        $this->assertArrayHasKey('listingsdb_id', $result['listing']);
        $this->assertArrayHasKey('price', $result['listing']);
        $this->assertEquals(1, $result['listing']['listingsdb_id']);
        $this->assertEquals(500, $result['listing']['price']);
    }

    /**
     * Test that trying to delete a listing that you are not the listing agent of fails.
     *
     * @return void
     */
    public function testListingDeleteOtherAgentListing()
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $user_api = new UserApi($this->dbh, $this->config);
        //Create Test Agent
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $data = ['user_details' => ['user_name' => 'agent1', 'user_first_name' => 'Test', 'user_last_name' => 'Agent', 'emailaddress' => 'agent1@integrationtest.open-realty.org', 'user_password' => 'password', 'active' => 'yes', 'is_agent' => 'yes']];
        $result = $user_api->create($data);
        $this->assertIsArray($result);
        @session_destroy();
        $_SESSION = [];

        $data = ['listing_id' => 1, 'fields' => ['listingsdb_id', 'price']];
        $login = $this->tester->loginAgent($this->dbh, $this->config, 'agent1', 'password');

        $this->expectExceptionMessage('Permission Denied (edit_all_listings)');
        $listing_api->delete($data);
    }

    /**
     * Test that anonymous API users can not delete listings
     *
     * @return void
     */
    public function testListingDeleteListingAnonymous()
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $data = ['listing_id' => 1];

        $this->expectExceptionMessage('Login Failure');
        $listing_api->delete($data);
    }

    /**
     * Test that you can delete a listing you are the listing agent for.
     *
     * @return void
     */
    public function testListingDeleteMyListing()
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $data = ['listing_id' => 1];
        $result = $listing_api->delete($data);
        $this->assertIsArray($result);

        $this->assertEquals(1, $result['listing_id']);
        $this->tester->dontSeeFileFound('1_china_room.jpg', $this->config['basepath'] . '/images/listing_photos/');
        $this->tester->dontSeeFileFound('1_dining_room.jpg', $this->config['basepath'] . '/images/listing_photos/');
        $this->tester->dontSeeFileFound('1_green_room.jpg', $this->config['basepath'] . '/images/listing_photos/');
        $this->tester->dontSeeFileFound('1_vermeil_room.jpg', $this->config['basepath'] . '/images/listing_photos/');
        $this->tester->dontSeeFileFound('1_white-house.jpg', $this->config['basepath'] . '/images/listing_photos/');
        $this->tester->dontSeeFileFound('thumb_1_china_room.jpg', $this->config['basepath'] . '/images/listing_photos/');
        $this->tester->dontSeeFileFound('thumb_1_dining_room.jpg', $this->config['basepath'] . '/images/listing_photos/');
        $this->tester->dontSeeFileFound('thumb_1_green_room.jpg', $this->config['basepath'] . '/images/listing_photos/');
        $this->tester->dontSeeFileFound('thumb_1_vermeil_room.jpg', $this->config['basepath'] . '/images/listing_photos/');
        $this->tester->dontSeeFileFound('thumb_1_white-house.jpg', $this->config['basepath'] . '/images/listing_photos/');
    }

    public function testHookAfterListingDelete()
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        //Make sure Hook Results do not exist already
        @unlink($this->config['basepath'] . '/afterListingDelete.txt');
        //Copy in Hook From test assets
        copy($this->config['basepath'] . '/tests/_assets/test_data/hooks/TestingHooks.php', $this->config['basepath'] . '/hooks/TestingHooks.php');
        $data = ['listing_id' => 1];
        $result = $listing_api->delete($data);
        $this->assertIsArray($result);
        unlink($this->config['basepath'] . '/hooks/TestingHooks.php');
        $this->tester->seeFileFound('afterListingDelete.txt', $this->config['basepath']);
        unlink($this->config['basepath'] . '/afterListingDelete.txt');
    }

    /**
     * @param mixed $example
     * @example [["avg", false, 34468855.0]]
     * @example [["median", false, 1843855.0]]
     * @example [["min", false, 1187710.0]]
     * @example [["max", false, 133000000.0]]
     */
    public function testListingsStatisticsPrice(array $example): void
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $result = $listing_api->getStatistics([
            'function' => $example[0],
            'field_name' => 'price',
            'format' => $example[1],
        ]);
        codecept_debug($result);
        $this->assertEquals($example[2], $result[$example[0]]);
    }

    public function testListingsCreateAnonymous(): void
    {
        $listing_api = new ListingApi($this->dbh, $this->config);
        $this->expectExceptionMessage('Login Failure');
        $listing_api->create([
            'class_id' => 1,
            'listing_details' => [
                'title' => 'Listing testListingsCreateAnonymous',
                'seotitle' > 'testListingsCreateAnonymous',
                'notes' => 'I am a test Listing',
                'featured' => false,
                'active' => false,
            ],
            'listing_agents' => [1],
            'listing_fields' => [],
            'listing_media' => [],
            'disable_logs' => false,
        ]);
    }

    public function testListingsCreateAdmin(): void
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $listing_api = new ListingApi($this->dbh, $this->config);
        $result = $listing_api->create([
            'class_id' => 1,
            'listing_details' => [
                'title' => 'Listing testListingsCreateAnonymous',
                'seotitle' > 'testListingsCreateAnonymous',
                'notes' => 'I am a test Listing',
                'featured' => false,
                'active' => false,
            ],
            'listing_agents' => [1],
            'listing_fields' => [],
            'listing_media' => [],
            'disable_logs' => false,
        ]);
        codecept_debug($result);
        $this->assertEquals(['listing_id' => 5], $result);
    }

    public function testListingsCreateAdminPrice(): void
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $listing_api = new ListingApi($this->dbh, $this->config);
        $result = $listing_api->create([
            'class_id' => 1,
            'listing_details' => [
                'title' => 'Listing testListingsCreateAnonymous',
                'seotitle' > 'testListingsCreateAnonymous',
                'notes' => 'I am a test Listing',
                'featured' => false,
                'active' => false,
            ],
            'listing_agents' => [1],
            'listing_fields' => ['price' => 500000],
            'listing_media' => [],
            'disable_logs' => false,
        ]);
        codecept_debug($result);
        $this->assertEquals(['listing_id' => 5], $result);
        $this->tester->seeInDatabase('default_en_listingsdbelements', [
            'listingsdbelements_field_name' => 'price',
            'listingsdbelements_field_value' => 500000,
            'listingsdb_id' => 5,
            'userdb_id' => 1,
        ]);
    }
}
