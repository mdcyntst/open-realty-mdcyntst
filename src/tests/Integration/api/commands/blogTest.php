<?php

namespace Tests\Integration;

use OpenRealty\Api\Commands\BlogApi;
use Tests\Support\IntegrationTester;
use OpenRealty\Api\Commands\ListingApi;

/**
 * Test the Listing API
 */
class BlogTest extends TestSetup
{
    /**
     * @var IntegrationTester
     */
    protected $tester;

    /**
     * Test the Blog Search API respects the count_only parameter being true
     *
     * @return void
     */
    public function testBlogSearchCountOnly(): void
    {
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Test 1', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 1, 'blogmain_full_autosave' => '']);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Test 2', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 1, 'blogmain_full_autosave' => '']);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Draft Test 3', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 0, 'blogmain_full_autosave' => '']);
        $blogAPi = new BlogApi($this->dbh, $this->config);
        $results = $blogAPi->search(['count_only' => true]);
        $this->assertEquals(2, $results['blog_count']);
    }

    /**
     * Test the Blog Search API respects the count_only parameter being false
     *
     * @return void
     */
    public function testBlogSearchCountFalse(): void
    {
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Test 1', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 1, 'blogmain_full_autosave' => '']);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Test 2', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 1, 'blogmain_full_autosave' => '']);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Draft Test 3', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 0, 'blogmain_full_autosave' => '']);
        $blogAPi = new BlogApi($this->dbh, $this->config);
        $results = $blogAPi->search(['count_only' => false]);
        $this->assertEquals(2, $results['blog_count']);
        $this->assertIsArray($results['blogs']);
        $this->assertEquals(2, count($results['blogs']));
    }


    /**
     *
     *
     * @param array $sortby
     *
     * @return void
     * @throws \Exception
     * @example [["blogmain_id"]]
     * @example [["userdb_id"]]
     * @example [["blogmain_title"]]
     * @example [["blogmain_date"]]
     * @example [["blogmain_published"]]
     * @example [["random"]]
     * @example [["badfield"]]
     * @example [["badfield","blogmain_date"]]
     */
    public function testBlogApiSearchSorting($sortby)
    {
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Test 1', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 1, 'blogmain_full_autosave' => '']);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Test 2', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 1, 'blogmain_full_autosave' => '']);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Draft Test 3', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 0, 'blogmain_full_autosave' => '']);
        $blogAPi = new BlogApi($this->dbh, $this->config);
        $results = $blogAPi->search(['sortby' => $sortby]);
        $this->assertEquals(2, $results['blog_count']);
        $this->assertIsArray($results['blogs']);
        $this->assertEquals(2, count($results['blogs']));
    }

    /**
     * Test the Blog Search API respects the blogmain_published parameter being any
     *
     * @return void
     */
    public function testBlogSearchPublishedAny(): void
    {
        $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Test 1', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 1, 'blogmain_full_autosave' => '']);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Test 2', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 1, 'blogmain_full_autosave' => '']);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Draft Test 3', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 0, 'blogmain_full_autosave' => '']);
        $blogAPi = new BlogApi($this->dbh, $this->config);
        $results = $blogAPi->search(['parameters' => ['blogmain_published' => 'any']]);
        $this->assertEquals(3, $results['blog_count']);
        $this->assertIsArray($results['blogs']);
        $this->assertEquals(3, count($results['blogs']));
    }

    /**
     * Test the Blog Search API respects the blogmain_published parameter being 0
     *
     * @return void
     */
    public function testBlogSearchPublishedDraft(): void
    {
        $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Test 1', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 1, 'blogmain_full_autosave' => '']);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Test 2', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 1, 'blogmain_full_autosave' => '']);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Draft Test 3', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 0, 'blogmain_full_autosave' => '']);
        $blogAPi = new BlogApi($this->dbh, $this->config);
        $results = $blogAPi->search(['parameters' => ['blogmain_published' => 0]]);
        $this->assertEquals(1, $results['blog_count']);
        $this->assertIsArray($results['blogs']);
        $this->assertEquals(1, count($results['blogs']));
    }

    /**
     * Test the Blog Read For Drafy By Admin
     *
     * @return void
     */
    public function testBlogSearchReadDraftAdmin(): void
    {
        $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Test 1', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 1, 'blogmain_full_autosave' => '']);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Test 2', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 1, 'blogmain_full_autosave' => '']);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Draft Test 3', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 0, 'blogmain_full_autosave' => '']);
        $blogAPi = new BlogApi($this->dbh, $this->config);
        $results = $blogAPi->read(['blog_id' => 3]);
        $this->assertEquals('Draft Test 3', $results['blog']['blogmain_title'] ?? '');
    }

    /**
     * Test the Blog Read For Drafy By Admin
     *
     * @return void
     */
    public function testBlogSearchReadDraftGuest(): void
    {
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Test 1', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 1, 'blogmain_full_autosave' => '']);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Test 2', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 1, 'blogmain_full_autosave' => '']);
        $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Draft Test 3', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 0, 'blogmain_full_autosave' => '']);
        $blogAPi = new BlogApi($this->dbh, $this->config);
        $this->expectExceptionMessage('Unexpected number of records returned.');
        $results = $blogAPi->read(['blog_id' => 3]);
    }

    /**
     * Test the Blog Read For Drafy By Admin
     *
     * @return void
     */
    public function testBlogDelete(): void
    {
        $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $id = $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Test 1', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 1, 'blogmain_full_autosave' => '']);
        $blogAPi = new BlogApi($this->dbh, $this->config);
        $this->tester->seeInDatabase('default_en_blogmain', ['blogmain_id' => $id]);
        $results = $blogAPi->delete(['blog_id' => $id]);
        $this->tester->dontSeeInDatabase('default_en_blogmain', ['blogmain_id' => $id]);
    }

    /**
     * Test the Blog Read For Drafy By Admin
     *
     * @return void
     */
    public function testBlogDeleteGuest(): void
    {
        $id = $this->tester->haveInDatabase('default_en_blogmain', ['userdb_id' => 1, 'blogmain_title' => 'Test 1', 'blogmain_date' => time(), 'blogmain_full' => '', 'blogmain_description' => '', 'blogmain_keywords' => '', 'blogmain_published' => 1, 'blogmain_full_autosave' => '']);
        $blogAPi = new BlogApi($this->dbh, $this->config);
        $this->tester->seeInDatabase('default_en_blogmain', ['blogmain_id' => $id]);
        $this->expectExceptionMessage('Login Failure');
        $results = $blogAPi->delete(['blog_id' => $id]);
        $this->tester->seeInDatabase('default_en_blogmain', ['blogmain_id' => $id]);
    }
}
