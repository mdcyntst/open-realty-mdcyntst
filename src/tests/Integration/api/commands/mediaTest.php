<?php

namespace Tests\Integration;

use OpenRealty\Api\Commands\MediaApi;
use Tests\Support\IntegrationTester;
use OpenRealty\Api\Api;

/**
 * Integration Test for the Media API
 */
class MediaTest extends TestSetup
{
    /**
     * @var IntegrationTester
     */
    protected $tester;

    // phpcs:ignore
    protected function _before()
    {
        parent::_before();
    }

    // phpcs:ignore
    protected function _after()
    {
        parent::_after();
    }

    /**
     * Test creating a user photo from local file.
     *
     * @return void
     */
    public function testMediaApiCreateUserPhoto()
    {
        $media_api = new MediaApi($this->dbh, $this->config);
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $data = ['media_parent_id' => 1,
            'media_type' => 'userimages',
            'media_data' => [
                'sincerely-media-l6ysDz2m6nM-unsplash.jpg' => [
                    'caption' => 'Test Caption',
                    'description' => 'Test Description',
                    'data' => file_get_contents(dirname(__FILE__) . '/../../../_assets/test_data/images/users/sincerely-media-l6ysDz2m6nM-unsplash.jpg'),
                ],],];
        $result = $media_api->create($data);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('media_error', $result);
        $this->assertArrayHasKey('sincerely-media-l6ysDz2m6nM-unsplash.jpg', $result['media_error']);
        $this->assertFalse($result['media_error']['sincerely-media-l6ysDz2m6nM-unsplash.jpg']);
        $this->tester->seeInDatabase('default_en_userimages', ['userdb_id' => '1', 'userimages_description' => 'Test Description', 'userimages_caption' => 'Test Caption', 'userimages_active' => 'yes', 'userimages_file_name' => 'sincerely-media-l6ysDz2m6nM-unsplash.jpg', 'userimages_thumb_file_name' => 'thumb_sincerely-media-l6ysDz2m6nM-unsplash.jpg']);
        $this->tester->seeFileFound('sincerely-media-l6ysDz2m6nM-unsplash.jpg', $this->config['basepath'] . '/images/user_photos/');
        $this->tester->seeFileFound('thumb_sincerely-media-l6ysDz2m6nM-unsplash.jpg', $this->config['basepath'] . '/images/user_photos/');
    }

    public function testMediaApiCreateUserPhotoGuest()
    {
        $media_api = new MediaApi($this->dbh, $this->config);
        $data = ['media_parent_id' => 1,
            'media_type' => 'userimages',
            'media_data' => [
                'sincerely-media-l6ysDz2m6nM-unsplash.jpg' => [
                    'caption' => 'Test Caption',
                    'description' => 'Test Description',
                    'data' => file_get_contents(dirname(__FILE__) . '/../../../_assets/test_data/images/users/sincerely-media-l6ysDz2m6nM-unsplash.jpg'),
                ],],];
        $this->expectExceptionMessage('Login Failure');
        $result = $media_api->create($data);
    }

    /**
     * Test creating a user photo from a remote file and storing it locally in open-realty.
     *
     * @return void
     */
    public function testMediaApiCreateUserRemotePhotoDownload()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $media_api = new MediaApi($this->dbh, $this->config);
        $data = ['media_parent_id' => 1,
            'media_type' => 'userimages',
            'media_data' => [
                'michael-dam-mEZ3PoFGs_k-unsplash.jpg' => [
                    'caption' => 'Test Caption',
                    'description' => 'Test Description',
                    'data' => $this->config['baseurl'] . '/tests/_assets/test_data/images/users/michael-dam-mEZ3PoFGs_k-unsplash.jpg',
                    'remote' => false,
                ],],];
        $result = $media_api->create($data);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('media_error', $result);
        $this->assertArrayHasKey('michael-dam-mEZ3PoFGs_k-unsplash.jpg', $result['media_error']);
        $this->assertFalse($result['media_error']['michael-dam-mEZ3PoFGs_k-unsplash.jpg']);
        $this->tester->seeInDatabase('default_en_userimages', ['userdb_id' => '1', 'userimages_description' => 'Test Description', 'userimages_caption' => 'Test Caption', 'userimages_active' => 'yes', 'userimages_file_name' => 'michael-dam-mEZ3PoFGs_k-unsplash.jpg', 'userimages_thumb_file_name' => 'thumb_michael-dam-mEZ3PoFGs_k-unsplash.jpg']);
        $this->tester->seeFileFound('michael-dam-mEZ3PoFGs_k-unsplash.jpg', $this->config['basepath'] . '/images/user_photos/');
        $this->tester->seeFileFound('thumb_michael-dam-mEZ3PoFGs_k-unsplash.jpg', $this->config['basepath'] . '/images/user_photos/');
    }

    /**
     * Test creating a user photo from a remote file and storing it locally in open-realty that is to large and should
     * return an error.
     *
     * @return void
     */
    public function testMediaApiCreateUserRemotePhotoDownloadToLarge()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $media_api = new MediaApi($this->dbh, $this->config);
        $data = ['media_parent_id' => 1,
            'media_type' => 'userimages',
            'media_data' => [
                'michael-dam-mEZ3PoFGs_k-unsplash_tolarge.jpg' => [
                    'caption' => 'Test Caption',
                    'description' => 'Test Description',
                    'data' => $this->config['baseurl'] . '/tests/_assets/test_data/images/users/michael-dam-mEZ3PoFGs_k-unsplash_tolarge.jpg',
                    'remote' => false,
                ],],];
        $result = $media_api->create($data);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('media_error', $result);
        $this->assertArrayHasKey('michael-dam-mEZ3PoFGs_k-unsplash_tolarge.jpg', $result['media_error']);
        $this->assertTrue($result['media_error']['michael-dam-mEZ3PoFGs_k-unsplash_tolarge.jpg']);

        $this->assertArrayHasKey('media_response', $result);
        $this->assertArrayHasKey('michael-dam-mEZ3PoFGs_k-unsplash_tolarge.jpg', $result['media_response']);
        $this->assertEquals('The file is too large', $result['media_response']['michael-dam-mEZ3PoFGs_k-unsplash_tolarge.jpg']);
        $this->tester->dontSeeInDatabase('default_en_userimages', ['userdb_id' => '1', 'userimages_description' => 'Test Description', 'userimages_caption' => 'Test Caption', 'userimages_active' => 'yes', 'userimages_file_name' => 'michael-dam-mEZ3PoFGs_k-unsplash_tolarge.jpg', 'userimages_thumb_file_name' => 'thumb_michael-dam-mEZ3PoFGs_k-unsplash_tolarge.jpg']);
        $this->tester->dontSeeFileFound('michael-dam-mEZ3PoFGs_k-unsplash_tolarge.jpg', $this->config['basepath'] . '/images/user_photos/');
        $this->tester->dontSeeFileFound('thumb_michael-dam-mEZ3PoFGs_k-unsplash_tolarge.jpg', $this->config['basepath'] . '/images/user_photos/');
    }

    /**
     * Test creating a user photo from a remote file and linking to the remote URL in open-realty.
     *
     * @return void
     */
    public function testMediaApiCreateUserRemotePhotoLink()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $media_api = new MediaApi($this->dbh, $this->config);
        $data = ['media_parent_id' => 1,
            'media_type' => 'userimages',
            'media_data' => [
                'michael-dam-mEZ3PoFGs_k-unsplash.jpg' => [
                    'caption' => 'Test Caption',
                    'description' => 'Test Description',
                    'data' => $this->config['baseurl'] . '/tests/_assets/test_data/images/users/michael-dam-mEZ3PoFGs_k-unsplash.jpg',
                    'remote' => true,
                ],],];
        $result = $media_api->create($data);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('media_error', $result);
        $this->assertArrayHasKey('michael-dam-mEZ3PoFGs_k-unsplash.jpg', $result['media_error']);
        $this->assertFalse($result['media_error']['michael-dam-mEZ3PoFGs_k-unsplash.jpg']);
        $this->tester->seeInDatabase('default_en_userimages', ['userdb_id' => '1', 'userimages_description' => 'Test Description', 'userimages_caption' => 'Test Caption', 'userimages_active' => 'yes', 'userimages_file_name' => $this->config['baseurl'] . '/tests/_assets/test_data/images/users/michael-dam-mEZ3PoFGs_k-unsplash.jpg', 'userimages_thumb_file_name' => $this->config['baseurl'] . '/tests/_assets/test_data/images/users/michael-dam-mEZ3PoFGs_k-unsplash.jpg']);
        $this->tester->dontSeeFileFound('michael-dam-mEZ3PoFGs_k-unsplash.jpg', $this->config['basepath'] . '/images/user_photos/');
        $this->tester->dontSeeFileFound('thumb_michael-dam-mEZ3PoFGs_k-unsplash.jpg', $this->config['basepath'] . '/images/user_photos/');
    }

    public function testMediaApiReadActiveListingPhotoUrlGuest(): void
    {
        $media_api = new MediaApi($this->dbh, $this->config);
        $data = ['media_parent_id' => 1,
            'media_type' => 'listingsimages',
            'media_output' => 'URL',
        ];
        $result = $media_api->read($data);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('media_object', $result);
        $this->assertArrayHasKey('media_count', $result);
        $this->assertEquals(5, $result['media_count']);
        $this->assertEquals([
            'media_id' => 1,
            'media_rank' => 1,
            'caption' => 'View From the Lawn',
            'description' => 'This property has six floors, 132 rooms, 35 bathrooms, 147 windows, 412 doors, 12 chimneys, 8 staircases, and 3 elevators.',
            'thumb_file_name' => 'thumb_1_white-house.jpg',
            'file_name' => '1_white-house.jpg',
            'thumb_width' => 100,
            'thumb_height' => 75,
            'thumb_file_src' => 'http://web.local/images/listing_photos/thumb_1_white-house.jpg',
            'file_width' => 640,
            'file_height' => 480,
            'file_src' => 'http://web.local/images/listing_photos/1_white-house.jpg',
            'remote' => false,
        ], $result['media_object'][0]);
    }

    public function testMediaApiReadActiveListingPhotoDataGuest(): void
    {
        $media_api = new MediaApi($this->dbh, $this->config);
        $data = ['media_parent_id' => 1,
            'media_type' => 'listingsimages',
            'media_output' => 'DATA',
        ];
        $result = $media_api->read($data);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('media_object', $result);
        $this->assertArrayHasKey('media_count', $result);
        $this->assertEquals(5, $result['media_count']);
        $this->assertInstanceOf(\GdImage::class, imagecreatefromstring($result['media_object'][0]['thumb_file_src']));
        $this->assertInstanceOf(\GdImage::class, imagecreatefromstring($result['media_object'][0]['file_src']));
    }

    public function testMediaApiReadActiveListingPhotoAdmin(): void
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $media_api = new MediaApi($this->dbh, $this->config);
        $data = ['media_parent_id' => 1,
            'media_type' => 'listingsimages',
            'media_output' => 'URL',
        ];
        $result = $media_api->read($data);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('media_object', $result);
        $this->assertArrayHasKey('media_count', $result);
        $this->assertEquals(5, $result['media_count']);
    }

    public function testMediaApiDeleteSingleListingPhotoDataGuest(): void
    {
        $media_api = new MediaApi($this->dbh, $this->config);
        $data = ['media_parent_id' => 1,
            'media_type' => 'listingsimages',
            'media_object_id' => '1',
        ];
        $this->expectExceptionMessage('permission denied: not media owner (API) - listingsimages 1');
        $result = $media_api->delete($data);
    }

    public function testMediaApiDeleteSingleListingPhotoDataAdmin(): void
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $media_api = new MediaApi($this->dbh, $this->config);
        $data = ['media_parent_id' => 1,
            'media_type' => 'listingsimages',
            'media_object_id' => '1',
        ];
        $result = $media_api->delete($data);
        codecept_debug($result);
        $this->assertEquals([
            "status_msg" => "Deleted 1",
            "media_parent_id" => 1,
            "media_type" => "listingsimages",
        ], $result);
    }
}
