<?php

namespace Tests\Integration;

use Tests\Support\IntegrationTester;
use OpenRealty\Api\Commands\LogApi;

/**
 * Test the Listing API
 */
class LogTest extends TestSetup
{
    /**
     * @var IntegrationTester
     */
    protected $tester;

    /**
     * Test the Blog Search API respects the count_only parameter being true
     *
     * @return void
     */
    public function testCreateLog(): void
    {
        $logApi = new LogApi($this->dbh, $this->config);
        $uuid = uniqid();
        $results = $logApi->create([
            'log_api_command' => 'test->testCreateLog',
            'log_message' => 'I am test ' . $uuid . '!',
        ]);
        $this->assertEquals('Log Generated', $results['status_msg']);
        $this->tester->seeInDatabase('default_en_activitylog', ['userdb_id' => 0, 'activitylog_action' => 'test->testCreateLog : I am test ' . $uuid . '!']);
    }

    public function testCreateLogAsAdmin(): void
    {
        $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $logApi = new LogApi($this->dbh, $this->config);
        $uuid = uniqid();
        $results = $logApi->create([
            'log_api_command' => 'test->testCreateLog',
            'log_message' => 'I am test ' . $uuid . '!',
        ]);
        $this->assertEquals('Log Generated', $results['status_msg']);
        $this->tester->seeInDatabase('default_en_activitylog', ['userdb_id' => 1, 'activitylog_action' => 'test->testCreateLog : I am test ' . $uuid . '!']);
    }
}
