<?php

namespace Tests\Integration;

use OpenRealty\Api\Commands\MenuApi;
use Tests\Support\IntegrationTester;

/**
 * Integration Test for the Media API
 */
class MenuTest extends TestSetup
{
    /**
     * @var IntegrationTester
     */
    protected $tester;

    // phpcs:ignore
    protected function _before(): void
    {
        parent::_before();
    }

    // phpcs:ignore
    protected function _after(): void
    {
        parent::_after();
    }

    /**
     * Test creating a user photo from local file.
     *
     * @return void
     */
    public function testMenuApiMetadata()
    {
        $menuApi = new MenuApi($this->dbh, $this->config);
        $results = $menuApi->metadata([]);
        codecept_debug($results);
        $this->assertEquals([
            2 => "horizontal",
            1 => "vertical",
        ], $results['menus']);
    }

    public function testMenuApiCreateGuest()
    {
        $menuApi = new MenuApi($this->dbh, $this->config);
        $this->expectExceptionMessage('permission_denied');
        $this->tester->dontSeeInDatabase('default_en_menu', ['menu_name' => 'testMenuApiCreateGuest']);
        $results = $menuApi->create(['menu_name' => 'testMenuApiCreateGuest']);
        codecept_debug($results);
        $this->tester->dontSeeInDatabase('default_en_menu', ['menu_name' => 'testMenuApiCreateGuest']);
    }

    public function testMenuApiCreateAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $menuApi = new MenuApi($this->dbh, $this->config);
        $this->tester->dontSeeInDatabase('default_en_menu', ['menu_name' => 'testMenuApiCreateAdmin']);
        $results = $menuApi->create(['menu_name' => 'testMenuApiCreateAdmin']);
        codecept_debug($results);
        $this->tester->seeInDatabase('default_en_menu', ['menu_name' => 'testMenuApiCreateAdmin']);
    }

    public function testMenuApiDeleteGuest()
    {
        $menuApi = new MenuApi($this->dbh, $this->config);
        $this->expectExceptionMessage('permission_denied');
        $this->tester->seeInDatabase('default_en_menu', ['menu_id' => 1]);
        $this->tester->seeInDatabase('default_en_menu_items', ['menu_id' => 1]);
        $results = $menuApi->delete(['menu_id' => 1]);
        codecept_debug($results);
        $this->tester->seeInDatabase('default_en_menu', ['menu_id' => 1]);
        $this->tester->seeInDatabase('default_en_menu_items', ['menu_id' => 1]);
    }

    public function testMenuApiDeleteAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $menuApi = new MenuApi($this->dbh, $this->config);
        $this->tester->seeInDatabase('default_en_menu', ['menu_id' => 1]);
        $this->tester->seeInDatabase('default_en_menu_items', ['menu_id' => 1]);
        $results = $menuApi->delete(['menu_id' => 1]);
        $this->assertEquals(1, $results['menu_id']);
        codecept_debug($results);
        $this->tester->dontSeeInDatabase('default_en_menu', ['menu_id' => 1]);
        $this->tester->dontSeeInDatabase('default_en_menu_items', ['menu_id' => 1]);
    }

    public function testMenuApiReadGuest()
    {
        $menuApi = new MenuApi($this->dbh, $this->config);
        $results = $menuApi->read(['menu_id' => 1]);
        codecept_debug($results);
        $this->assertEquals(5, count($results['menu']));
    }

    public function testMenuApiItemDetailsGuest()
    {
        $menuApi = new MenuApi($this->dbh, $this->config);
        $results = $menuApi->itemDetails(['item_id' => 83]);
        codecept_debug($results);
        $this->assertEquals([
            'item_id' => 83,
            'parent_id' => 0,
            'menu_id' => 1,
            'item_name' => 'Edit profile',
            'item_order' => 7,
            'item_type' => 1,
            'item_value' => 'url_edit_profile',
            'item_target' => '_self',
            'item_class' => '',
            'visible_guest' => false,
            'visible_member' => true,
            'visible_agent' => false,
            'visible_admin' => false,
        ], $results['menu_item']);
    }

    public function testMenuApiSetMenuOrderGuest()
    {
        $menuApi = new MenuApi($this->dbh, $this->config);
        $menu_items = [];
        $menu_items[83] = [
            'order' => 9,
            'parent' => 0,
        ];
        $this->tester->seeInDatabase('default_en_menu', ['menu_id' => 1]);
        $this->tester->seeInDatabase('default_en_menu_items', [
            'menu_id' => 1,
            'item_id' => 83,
            'parent_id' => 0,
            'item_order' => 7,
        ]);
        $this->expectExceptionMessage('permission_denied');
        $results = $menuApi->setMenuOrder(['menu_id' => 1, 'menu_items' => $menu_items]);
        codecept_debug($results);
        $this->tester->seeInDatabase('default_en_menu', ['menu_id' => 1]);
        $this->tester->seeInDatabase('default_en_menu_items', [
            'menu_id' => 1,
            'item_id' => 83,
            'parent_id' => 0,
            'item_order' => 7,
        ]);
    }

    public function testMenuApiSetMenuOrderAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $menuApi = new MenuApi($this->dbh, $this->config);
        $menu_items = [];
        $menu_items[83] = [
            'order' => 9,
            'parent' => 0,
        ];
        $this->tester->seeInDatabase('default_en_menu', ['menu_id' => 1]);
        $this->tester->seeInDatabase('default_en_menu_items', [
            'menu_id' => 1,
            'item_id' => 83,
            'parent_id' => 0,
            'item_order' => 7,
        ]);

        $results = $menuApi->setMenuOrder(['menu_id' => 1, 'menu_items' => $menu_items]);
        codecept_debug($results);
        $this->assertEquals([
            "menu_id" => 1,
        ], $results);
        $this->tester->seeInDatabase('default_en_menu', ['menu_id' => 1]);
        $this->tester->seeInDatabase('default_en_menu_items', [
            'menu_id' => 1,
            'item_id' => 83,
            'parent_id' => 0,
            'item_order' => 9,
        ]);
    }

    public function testMenuApiDeleteMenuItemAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $menuApi = new MenuApi($this->dbh, $this->config);
        $this->tester->seeInDatabase('default_en_menu_items', ['item_id' => 83]);
        $results = $menuApi->deleteMenuItem(['item_id' => 83]);
        codecept_debug($results);
        $this->assertEquals([
            "item_id" => 83,
        ], $results);
        $this->tester->dontSeeInDatabase('default_en_menu_items', ['item_id' => 83]);
    }

    public function testMenuApiDeleteMenuItemGuest()
    {
        $menuApi = new MenuApi($this->dbh, $this->config);
        $this->tester->seeInDatabase('default_en_menu_items', ['item_id' => 83]);
        $this->expectExceptionMessage('permission_denied');
        $results = $menuApi->deleteMenuItem(['item_id' => 83]);
        $this->tester->seeInDatabase('default_en_menu_items', ['item_id' => 83]);
        codecept_debug($results);
    }

    public function testMenuApiAddMenuItemGuest()
    {
        $menuApi = new MenuApi($this->dbh, $this->config);
        $this->expectExceptionMessage('permission_denied');
        $results = $menuApi->addMenuItem(['menu_id' => 1, 'parent_id' => 0, 'item_name' => 'testMenuApiAddMenuItemGuest']);
        $this->tester->dontSeeInDatabase('default_en_menu_items', [
            'menu_id' => 1,
            'parent_id' => 0,
            'item_name' => 'testMenuApiAddMenuItemGuest',
        ]);
    }

    public function testMenuApiAddMenuItemAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $menuApi = new MenuApi($this->dbh, $this->config);
        $results = $menuApi->addMenuItem(['menu_id' => 1, 'parent_id' => 0, 'item_name' => 'testMenuApiAddMenuItemGuest']);
        codecept_debug($results);
        $new_item = $results['item_id'];
        $this->tester->seeInDatabase('default_en_menu_items', ['item_id' => $new_item,
            'menu_id' => 1,
            'parent_id' => 0,
            'item_name' => 'testMenuApiAddMenuItemGuest',
        ]);
    }

    public function testMenuApiSaveMenuItemGuest()
    {
        $menuApi = new MenuApi($this->dbh, $this->config);
        $this->expectExceptionMessage('permission_denied');
        $results = $menuApi->saveMenuItem([
            'item_id' => 83,
            'item_name' => 'Edit Profile',
            'item_type' => 1,
            'item_value' => 'url_edit_profile',
            'item_target' => '_self',
            'item_class' => 'bah',
            'visible_guest' => false,
            'visible_member' => true,
            'visible_agent' => true,
            'visible_admin' => true,
        ]);
        $this->tester->dontSeeInDatabase('default_en_menu_items', [
            'item_id' => 83,
            'item_name' => 'Edit Profile',
            'item_class' => 'bah',
            'visible_agent' => 1,
            'visible_admin' => 1,
        ]);
    }

    public function testMenuApiSaveMenuItemAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $menuApi = new MenuApi($this->dbh, $this->config);
        $results = $menuApi->saveMenuItem([
            'item_id' => 83,
            'item_name' => 'Edit Profile',
            'item_type' => 1,
            'item_value' => 'url_edit_profile',
            'item_target' => '_self',
            'item_class' => 'bah',
            'visible_guest' => false,
            'visible_member' => true,
            'visible_agent' => true,
            'visible_admin' => true,
        ]);
        $this->tester->seeInDatabase('default_en_menu_items', [
            'item_id' => 83,
            'item_name' => 'Edit Profile',
            'item_class' => 'bah',
            'visible_agent' => 1,
            'visible_admin' => 1,
        ]);
    }
}
