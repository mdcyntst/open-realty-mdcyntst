<?php

namespace Tests\Integration;

use OpenRealty\Api\Commands\MediaApi;
use OpenRealty\Api\Commands\UserApi;
use Tests\Support\IntegrationTester;
use OpenRealty\Api\Api;

/**
 * Integration Test for the User API
 */
class UserTest extends TestSetup
{
    /**
     * @var IntegrationTester
     */
    protected $tester;

    // phpcs:ignore
    protected function _before()
    {
        parent::_before();
    }

    // phpcs:ignore
    protected function _after()
    {
        parent::_after();
    }

    /**
     * Test creating an agent with the User Create API
     *
     * @return void
     */
    public function testUserApiCreateAgent()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $data = ['user_details' => ['user_name' => 'agent1', 'user_first_name' => 'Test', 'user_last_name' => 'Agent', 'emailaddress' => 'agent1@integrationtest.open-realty.org', 'user_password' => 'password', 'active' => 'yes', 'is_agent' => 'yes']];
        $user_api = new UserApi($this->dbh, $this->config);
        $result = $user_api->create($data);
        $this->assertIsArray($result);
        $this->tester->seeInDatabase('default_en_userdb', ['userdb_user_name' => 'agent1', 'userdb_emailaddress' => 'agent1@integrationtest.open-realty.org', 'userdb_is_admin' => 'no', 'userdb_is_agent' => 'yes', 'userdb_active' => 'yes']);
    }

    public function testUserApiUpdateUserNameByAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $data = ['user_id' => 1, 'user_details' => ['user_first_name' => 'Joe', 'user_last_name' => 'Smith']];
        $user_api = new UserApi($this->dbh, $this->config);
        $result = $user_api->update($data);
        $this->assertEquals(1, $result['userdb_id']);
        $this->tester->seeInDatabase('default_en_userdb', ['userdb_user_first_name' => 'Joe', 'userdb_user_last_name' => 'Smith', 'userdb_id' => 1]);
    }

    public function testUserApiUpdateCustomAgentFieldNameByAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $this->tester->seeInDatabase('default_en_userdbelements', ['userdbelements_field_name' => 'info', 'userdbelements_field_value' => 'I am the system administrator!', 'userdb_id' => 1]);
        $data = ['user_id' => 1, 'user_fields' => ['info' => 'I am just a test agent.']];
        $user_api = new UserApi($this->dbh, $this->config);
        $result = $user_api->update($data);
        $this->assertEquals(1, $result['userdb_id']);
        $this->tester->dontSeeInDatabase('default_en_userdbelements', ['userdbelements_field_name' => 'info', 'userdbelements_field_value' => 'I am the system administrator!', 'userdb_id' => 1]);
        $this->tester->seeInDatabase('default_en_userdbelements', ['userdbelements_field_name' => 'info', 'userdbelements_field_value' => 'I am just a test agent.', 'userdb_id' => 1]);
    }

    protected function testUserApiCreateAgentImage()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $data = ['media_parent_id' => 2,
            'media_type' => 'userimages',
            'media_data' => [
                'sincerely-media-l6ysDz2m6nM-unsplash.jpg' => [
                    'caption' => 'Test Caption',
                    'description' => 'Test Description',
                    'data' => file_get_contents(dirname(__FILE__) . '/../../../_assets/test_data/images/users/sincerely-media-l6ysDz2m6nM-unsplash.jpg'),
                ],],];
        $user_api = new MediaApi($this->dbh, $this->config);
        $result = $user_api->create($data);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('media_error', $result);
        $this->assertArrayHasKey('sincerely-media-l6ysDz2m6nM-unsplash.jpg', $result['media_error']);
    }

    /**
     * Test deleting a user from the database.
     */
    public function testUserApiDeleteUser()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $this->testUserApiCreateAgent();
        $data = ['user_id' => 2];
        $user_api = new UserApi($this->dbh, $this->config);
        $result = $user_api->delete($data);
        $this->assertIsArray($result);
        $this->tester->dontSeeInDatabase('default_en_userdb', ['userdb_user_name' => 'agent1', 'userdb_emailaddress' => 'agent1@integrationtest.open-realty.org', 'userdb_is_admin' => 'no', 'userdb_is_agent' => 'yes', 'userdb_active' => 'yes']);
    }

    /**
     * Test deleting a user from the database.
     */
    public function testUserApiDeleteUserWithImage()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $this->testUserApiCreateAgent();
        $this->testUserApiCreateAgentImage();
        $data = ['user_id' => 2];
        $user_api = new UserApi($this->dbh, $this->config);
        $result = $user_api->delete($data);
        $this->assertIsArray($result);
        $this->tester->dontSeeInDatabase('default_en_userdb', ['userdb_user_name' => 'agent1', 'userdb_emailaddress' => 'agent1@integrationtest.open-realty.org', 'userdb_is_admin' => 'no', 'userdb_is_agent' => 'yes', 'userdb_active' => 'yes']);
        $this->tester->dontSeeInDatabase('default_en_userimages', ['userdb_id' => '2', 'userimages_description' => 'Test Description', 'userimages_caption' => 'Test Caption', 'userimages_active' => 'yes', 'userimages_file_name' => 'sincerely-media-l6ysDz2m6nM-unsplash.jpg', 'userimages_thumb_file_name' => 'thumb_sincerely-media-l6ysDz2m6nM-unsplash.jpg']);
        $this->tester->dontSeeFileFound('sincerely-media-l6ysDz2m6nM-unsplash.jpg', $this->config['basepath'] . '/images/user_photos/');
        $this->tester->dontSeeFileFound('thumb_sincerely-media-l6ysDz2m6nM-unsplash.jpg', $this->config['basepath'] . '/images/user_photos/');
    }

    public function testUserApiSearchAgentsAsAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $user_api = new UserApi($this->dbh, $this->config);
        $result = $user_api->search(['resource' => 'agent', 'parameters' => []]);
        codecept_debug($result);
        $this->assertEquals(1, $result['user_count']);
        $this->assertEquals([0 => 1], $result['users']);
        $this->assertEquals("agent", $result['resource']);
    }

    public function testUserApiSearchMembersAsAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $user_api = new UserApi($this->dbh, $this->config);
        $result = $user_api->search(['resource' => 'member', 'parameters' => []]);
        codecept_debug($result);
        $this->assertEquals(0, $result['user_count']);
        $this->assertEquals([], $result['users']);
        $this->assertEquals("member", $result['resource']);
    }

    /**
     *
     *
     * @param array $sortby
     *
     * @return void
     * @throws \Exception
     * @example [["userdb_id"]]
     * @example [["userdb_user_name"]]
     * @example [["userdb_hit_count"]]
     * @example [["random"]]
     * @example [["userdb_last_modified"]]
     * @example [["userdb_rank"]]
     * @example [["phone"]]
     * @example [["badfield", "phone"]]
     * @example [["userdb_user_name","phone"]]
     */
    public function testUserApiSearchMembersAsAdminSorting($sortby)
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $user_api = new UserApi($this->dbh, $this->config);
        $result = $user_api->search(['resource' => 'member', 'parameters' => [], 'sortby' => $sortby]);
        codecept_debug($result);
        $this->assertEquals(0, $result['user_count']);
        $this->assertEquals([], $result['users']);
        $this->assertEquals("member", $result['resource']);
    }

    public function testUserApiReadAgentsAsAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $user_api = new UserApi($this->dbh, $this->config);
        $result = $user_api->read(['resource' => 'agent', 'user_id' => 1]);
        codecept_debug($result);
        $this->assertEquals([
            "userdb_id" => 1,
            "userdb_user_name" => "admin",
            "userdb_emailaddress" => "changeme@default.com",
            "userdb_user_first_name" => "Default",
            "userdb_user_last_name" => "Admin",
            "userdb_comments" => "",
            "userdb_is_admin" => "yes",
            "userdb_can_edit_site_config" => "yes",
            "userdb_can_edit_member_template" => "yes",
            "userdb_can_edit_agent_template" => "yes",
            "userdb_can_edit_listing_template" => "yes",
            "userdb_creation_date" => "2002-07-01",
            "userdb_can_feature_listings" => "yes",
            "userdb_can_view_logs" => "yes",
            "userdb_last_modified" => "2002-07-01 22:38:50",
            "userdb_hit_count" => 1,
            "userdb_can_moderate" => "yes",
            "userdb_can_edit_pages" => "yes",
            "userdb_can_have_vtours" => "yes",
            "userdb_is_agent" => "no",
            "userdb_active" => "yes",
            "userdb_limit_listings" => -1,
            "userdb_can_edit_expiration" => "yes",
            "userdb_can_export_listings" => "yes",
            "userdb_can_edit_all_users" => "yes",
            "userdb_can_edit_all_listings" => "yes",
            "userdb_can_edit_property_classes" => "yes",
            "userdb_can_have_files" => "yes",
            "userdb_can_have_user_files" => "yes",
            "userdb_blog_user_type" => 4,
            "userdb_rank" => 1,
            "userdb_featuredlistinglimit" => -1,
            "userdb_email_verified" => "yes",
            "userdb_can_manage_addons" => "yes",
            "userdb_can_edit_all_leads" => "yes",
            "userdb_can_edit_lead_template" => "yes",
            "userdb_send_notifications_to_floor" => 0,
            "phone" => "215.850.0710",
            "mobile" => "215.850.0710",
            "fax" => "702.995.6591",
            "homepage" => "http://www.open-realty.org",
            "info" => "I am the system administrator!",
        ], $result['user']);
    }

    public function testUserApiReadAgentsSingleFieldEmailAsAdmin()
    {
        $login = $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $this->assertTrue($login);
        $user_api = new UserApi($this->dbh, $this->config);
        $result = $user_api->read(['resource' => 'agent', 'user_id' => 1, 'fields' => ['userdb_emailaddress']]);
        codecept_debug($result);
        $this->assertEquals(["userdb_emailaddress" => "changeme@default.com"], $result['user']);
    }
}
