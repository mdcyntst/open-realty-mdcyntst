<?php

namespace Tests\Integration;

use Tests\Support\IntegrationTester;
use OpenRealty\PageUser;
use OpenRealty\PageAdmin;
use OpenRealty\GeneralAdmin;
use OpenRealty\PageAdminAjax;
use OpenRealty\PageUserAjax;

/**
 * Test the Listing API
 */
class AddonsTest extends TestSetup
{
    /**
     * @var IntegrationTester
     */
    protected $tester;

    protected function _before()
    {
        parent::_before();
        mkdir($this->config['basepath'] . '/addons/TestAddon/');
        copy($this->config['basepath'] . '/tests/_assets/test_data/addons/TestAddon/Addon.php', $this->config['basepath'] . '/addons/TestAddon/Addon.php');
    }

    protected function _after()
    {
        unlink($this->config['basepath'] . '/addons/TestAddon/Addon.php');
        rmdir($this->config['basepath'] . '/addons/TestAddon/');
        parent::_after();
    }

    /**
     * Test the Blog Search API respects the count_only parameter being true
     *
     * @return void
     */
//    public function testCreateLog(): void
//    {
//        $logApi = new LogApi($this->dbh, $this->config);
//        $uuid = uniqid();
//        $results = $logApi->create([
//            'log_api_command' => 'test->testCreateLog',
//            'log_message' => 'I am test ' . $uuid . '!',
//        ]);
//        $this->assertEquals('Log Generated', $results['status_msg']);
//        $this->tester->seeInDatabase('default_en_activitylog', ['userdb_id' => 0, 'activitylog_action' => 'test->testCreateLog : I am test ' . $uuid . '!']);
//    }

    public function testDisplayAddonsAdmin(): void
    {
        //displayAddons
        $this->tester->loginAgent($this->dbh, $this->config, $this->int_user, $this->int_pass);
        $generalAdmin = new GeneralAdmin($this->dbh, $this->config);
        $link = $generalAdmin->displayAddons('TestAddon');
        $this->assertIsArray($link);
        $this->assertEquals('<a href="index.php?action=addon_TestAddon_admin" title="TestAddon">TestAddon</a>', $link[0]);
        $this->tester->seeInDatabase('default_addons', ['addons_version' => '3.0', 'addons_name' => 'TestAddon']);
    }

    public function testReplaceUserAction(): void
    {
        $page = new PageUser($this->dbh, $this->config);
        $_GET['action'] = 'addon_TestAddon_showpage1';
        $result = $page->replaceUserAction();
        $this->assertEquals('View From the Lawn', $result);
    }


    //runActionAdminTemplate
    public function testRunActionAdminTemplate(): void
    {
        $page = new PageAdmin($this->dbh, $this->config);
        $_GET['action'] = 'addon_TestAddon_admin';
        $result = $page->replaceAdminActions();
        $this->assertStringContainsString('This is an Add-on page (replaced by the add-on tag {addon_TestAddon_admin})', $result);
    }

    public function testReplaceTags(): void
    {
        $page = new PageUser($this->dbh, $this->config);
        $_GET['action'] = 'addon_TestAddon_showpage1';
        $page->page = '{addon_TestAddon_link}';
        $page->replaceTags(['addon_TestAddon_link']);
        codecept_debug($page->page);
        $this->assertEquals('<a href="index.php?action=addon_TestAddon_showpage1" title="TestAddon Test">TestAddon Link</a>', $page->page);
    }

    public function testAdminReplaceTags(): void
    {
        $page = new PageAdmin($this->dbh, $this->config);
        $_GET['action'] = 'addon_TestAddon_showpage1';
        $page->page = '{addon_TestAddon_link}';
        $page->replaceTags(['addon_TestAddon_link']);
        codecept_debug($page->page);
        $this->assertEquals('<a href="index.php?action=addon_TestAddon_showpage1" title="TestAddon Test">TestAddon Link</a>', $page->page);
    }

    public function testAdminRunActionAjax(): void
    {
        $page = new PageAdminAjax($this->dbh, $this->config);
        $_GET['action'] = 'addon_TestAddon_get_admin_ajax';
        $result = json_decode($page->callAjax(), true);
        $this->assertEquals('This is our ajax response for our backend', $result['message']);
    }

    public function testRunActionAjax(): void
    {
        $page = new PageUserAjax($this->dbh, $this->config);
        $_GET['action'] = 'addon_TestAddon_get_user_ajax';
        $result = json_decode($page->callAjax(), true);
        $this->assertEquals('This is our ajax response for our frontend', $result['message']);
    }
}
