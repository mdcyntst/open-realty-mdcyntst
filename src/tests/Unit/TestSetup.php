<?php

namespace Tests\Unit;

use Codeception\Test\Unit;
use Mockery;
use PDO;
use ReflectionClass;
use Tests\Support\UnitTester;

/**
 * Undocumented class
 */
abstract class TestSetup extends Unit
{
    /**
     * @var UnitTester
     */
    protected UnitTester $tester;

    protected PDO $dbh;

    protected array $config = [];

    private function setupConfig(): void
    {
        $this->config['baseurl'] = 'http://web.local';
        $this->config['basepath'] = dirname(__FILE__, 3);
        $this->config['strip_html'] = 0;
        $this->config['table_prefix_no_lang'] = 'default_';
        $this->config['table_prefix'] = 'default_en_';
        $this->config['charset'] = 'UTF-8';
    }

    private function cleanupAll(): void
    {
        unset($config);
        $_COOKIE = [];
    }

    // phpcs:ignore
    protected function _before()
    {
        global $ORconn;
        @session_start();
        session_reset();
        $ORconn = Mockery::mock();
        $this->dbh = Mockery::mock('\PDO');
        $this->setupConfig();
        parent::_before();
    }

    // phpcs:ignore
    protected function _after()
    {
        $this->cleanupAll();
        parent::_after();
    }

    /**
     * /Reflector to run private function
     *
     * @param mixed  $object     - A Class Object that contains a private function we need to test
     * @param string $methodName - Name of the private function we need to test
     * @param array  $parameters - Paramaters to pass to the private function
     * @return void
     */
    public function invokeMethod(&$object, string $methodName, array $parameters = []): mixed
    {
        $reflection = new ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
