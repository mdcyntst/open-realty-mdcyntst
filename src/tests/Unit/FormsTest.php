<?php

//declare(strict_types=1);

namespace Tests\Unit;

use DOMDocument;
use Mockery;
use Tests\Support\UnitTester;
use OpenRealty\Forms;

class FormsTest extends TestSetup
{
    protected UnitTester $tester;

    // phpcs:ignore
    protected function _before()
    {
        parent::_before();
    }

    // phpcs:ignore
    protected function _after()
    {
        parent::_after();
    }

    /**
     * Test the renderFormElement function to render a latitude field.
     *
     * @return void
     */
    public function testRenderLatitudeWithLengthAndToolTip()
    {
        $forms = new Forms($this->dbh, $this->config);

        $result = $forms->renderFormElement('lat', 'test_field', '66.57', 'Our Latitude', '', 'no', '', '9', 'Enter your latitude.');
        $expected = '<div class="input-group input-group-static mb-2">
                <label for="test_field">Our Latitude</label>
                <a data-bs-placement="top" data-bs-toggle="tooltip" href="#" style="margin-left:5px !important" title="Enter your latitude.">
                <i class="fa-solid fa-circle-info"/>
                </a>
                <input
                  type="text"
                  name="test_field"
                  id="test_field"
                  value="66.57"
                  class="form-control"
                  maxlength="9" /></div>';
        $this->assertXmlStringEqualsXmlString('<test>' . $expected . '</test>', '<test>' . $result . '</test>');
    }

    /**
     * Test the renderFormElement function to render a checkbox field.
     *
     * @return void
     */
    public function testRenderCheckboxWithToolTip()
    {
        $forms = new Forms($this->dbh, $this->config);

        $result = $forms->renderFormElement('checkbox', 'test_field', 'pool', 'Amenities', '', 'no', 'solar panels||pool||hot tub||landscaping', '', 'Your New Homes Amenities');
        $expectedDoc = new DOMDocument();

        $resultDoc = new DOMDocument();

        $resultDoc->loadHTML($result);
        $resultDoc->preserveWhiteSpace = false;
        $expected = '<input name="test_field[]" type="hidden" value=""/><div class="input-group input-group-static mb-2">
                <label for="test_field[]">Amenities</label>
                <a data-bs-placement="top" data-bs-toggle="tooltip" href="#" style="margin-left:5px !important" title="Your New Homes Amenities">
                    <i class="fa-solid fa-circle-info"/>
                </a>
                </div><div class="form-check">
                <input class="form-check-input" id="test_field1" name="test_field[]" type="checkbox" value="hot tub"/>
                <label class="custom-control-label" for="test_field1">hot tub</label>
                </div><div class="form-check">
                <input class="form-check-input" id="test_field2" name="test_field[]" type="checkbox" value="landscaping"/>
                <label class="custom-control-label" for="test_field2">landscaping</label>
                </div><div class="form-check">
                <input checked="checked" class="form-check-input" id="test_field3" name="test_field[]" type="checkbox" value="pool"/>
                <label class="custom-control-label" for="test_field3">pool</label>
                </div><div class="form-check">
                <input class="form-check-input" id="test_field4" name="test_field[]" type="checkbox" value="solar panels"/>
                <label class="custom-control-label" for="test_field4">solar panels</label>
                </div>';
        $expectedDoc->loadHTML($expected);
        $expectedDoc->preserveWhiteSpace = false;
        $this->assertXmlStringEqualsXmlString($expectedDoc->saveXML(), $resultDoc->saveXML());
        //$this > self::assertEquals($expectedDoc->saveHTML(), $resultDoc->saveHTML());
    }

    public function testRenderCheckboxWithToolTipMultiSelect()
    {
        $forms = new Forms($this->dbh, $this->config);

        $result = $forms->renderFormElement('checkbox', 'test_field', ['pool', 'landscaping'], 'Amenities', '', 'no', 'solar panels||pool||hot tub||landscaping', '', 'Your New Homes Amenities');
        $expectedDoc = new DOMDocument();

        $resultDoc = new DOMDocument();

        $resultDoc->loadHTML($result);
        $resultDoc->preserveWhiteSpace = false;
        $expected = '<input name="test_field[]" type="hidden" value=""/><div class="input-group input-group-static mb-2">
                <label for="test_field[]">Amenities</label>
                <a data-bs-placement="top" data-bs-toggle="tooltip" href="#" style="margin-left:5px !important" title="Your New Homes Amenities">
                    <i class="fa-solid fa-circle-info"/>
                </a>
                </div><div class="form-check">
                <input class="form-check-input" id="test_field1" name="test_field[]" type="checkbox" value="hot tub"/>
                <label class="custom-control-label" for="test_field1">hot tub</label>
                </div><div class="form-check">
                <input checked="checked" class="form-check-input" id="test_field2" name="test_field[]" type="checkbox" value="landscaping"/>
                <label class="custom-control-label" for="test_field2">landscaping</label>
                </div><div class="form-check">
                <input checked="checked" class="form-check-input" id="test_field3" name="test_field[]" type="checkbox" value="pool"/>
                <label class="custom-control-label" for="test_field3">pool</label>
                </div><div class="form-check">
                <input class="form-check-input" id="test_field4" name="test_field[]" type="checkbox" value="solar panels"/>
                <label class="custom-control-label" for="test_field4">solar panels</label>
                </div>';
        $expectedDoc->loadHTML($expected);
        $expectedDoc->preserveWhiteSpace = false;
        $this->assertXmlStringEqualsXmlString($expectedDoc->saveXML(), $resultDoc->saveXML());
        //$this > self::assertEquals($expectedDoc->saveHTML(), $resultDoc->saveHTML());
    }

    public function testRenderSelectMultipleWithToolTip()
    {
        $forms = new Forms($this->dbh, $this->config);

        $result = $forms->renderFormElement('select-multiple', 'test_field', 'landscaping', 'Amenities', '', 'no', 'solar panels||pool||hot tub||landscaping', '', 'Your New Homes Amenities');
        $expectedDoc = new DOMDocument();

        $resultDoc = new DOMDocument();

        $resultDoc->loadHTML($result);
        $resultDoc->preserveWhiteSpace = false;
        $expected = '<div class="input-group input-group-static">
            <label class="ms-0" for="test_field">Amenities</label>
            <a data-bs-placement="top" data-bs-toggle="tooltip" href="#" style="margin-left:5px !important" title="Your New Homes Amenities">
              <i class="fa-solid fa-circle-info"/>
            </a>
            <select class="form-control" id="test_field" multiple="multiple" name="test_field">
            <option value="hot tub">hot tub</option>
            <option selected="selected" value="landscaping">landscaping</option>
            <option value="pool">pool</option>
            <option value="solar panels">solar panels</option>
            </select>
            </div>
';
        $expectedDoc->loadHTML($expected);
        $expectedDoc->preserveWhiteSpace = false;
        $this->assertXmlStringEqualsXmlString($expectedDoc->saveXML(), $resultDoc->saveXML());
        //$this > self::assertEquals($expectedDoc->saveHTML(), $resultDoc->saveHTML());
    }

    public function testRenderOptionWithToolTip()
    {
        $forms = new Forms($this->dbh, $this->config);

        $result = $forms->renderFormElement('option', 'test_field', ['landscaping', 'solar panels'], 'Amenities', '', 'no', 'solar panels||pool||hot tub||landscaping', '', 'Your New Homes Amenities');
        $expectedDoc = new DOMDocument();

        $resultDoc = new DOMDocument();

        $resultDoc->loadHTML($result);
        $resultDoc->preserveWhiteSpace = false;
        $expected = '<input name="test_field[]" type="hidden" value=""/>
            <div class="input-group input-group-static mb-2">
              <label>Amenities</label>
              <a data-bs-placement="top" data-bs-toggle="tooltip" href="#" style="margin-left:5px !important" title="Your New Homes Amenities">
                <i class="fa-solid fa-circle-info"/>
              </a>
            </div>
            <div class="form-check">
              <input class="form-check-input" id="test_field1" name="test_field" type="radio" value="hot tub"/>
              <label class="custom-control-label" for="test_field1">hot tub</label>
            </div>
            <div class="form-check">
              <input checked="checked" class="form-check-input" id="test_field2" name="test_field" type="radio" value="landscaping"/>
              <label class="custom-control-label" for="test_field2">landscaping</label>
            </div>
            <div class="form-check">
              <input class="form-check-input" id="test_field3" name="test_field" type="radio" value="pool"/>
              <label class="custom-control-label" for="test_field3">pool</label>
            </div>
            <div class="form-check">
              <input checked="checked" class="form-check-input" id="test_field4" name="test_field" type="radio" value="solar panels"/>
              <label class="custom-control-label" for="test_field4">solar panels</label>
            </div>
';
        $expectedDoc->loadHTML($expected);
        $expectedDoc->preserveWhiteSpace = false;
        $this->assertXmlStringEqualsXmlString($expectedDoc->saveXML(), $resultDoc->saveXML());
        //$this > self::assertEquals($expectedDoc->saveHTML(), $resultDoc->saveHTML());
    }
}
