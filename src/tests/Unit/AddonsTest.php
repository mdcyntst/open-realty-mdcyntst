<?php

namespace Tests\Unit;

use OpenRealty\AddonManager;
use Tests\Support\UnitTester;
use Mockery;

/**
 *  Unit Test for the addons
 */
class AddonsTest extends TestSetup
{
    protected UnitTester $tester;

    public function testAddonUserAction(): void
    {
        $addon = Mockery::mock('overload:\OpenRealty\Addons\MockAddon\Addon');
        $addon->shouldReceive('runActionUserTemplate')->andReturn('viewed');
        $page = Mockery::mock('\OpenRealty\PageUser', [$this->dbh, $this->config])->makePartial();
        $_GET['action'] = 'addon_MockAddon_view';
        $result = $page->replaceUserAction();
        $this->assertEquals('viewed', $result);
    }

    public function testAddonAdminAction(): void
    {
        $addon = Mockery::mock('overload:\OpenRealty\Addons\MockAddon\Addon');
        $addon->shouldReceive('runActionAdminTemplate')->andReturn('viewed');
        //Setup Mock of Login
        $login = Mockery::mock("\OpenRealty\Login");
        $login->shouldReceive('loginCheck')->andReturn(true);
        $page = Mockery::mock('\OpenRealty\PageAdmin', [$this->dbh, $this->config])->makePartial();
        $page->shouldReceive("newLogin")->andReturn($login);
        $_GET['action'] = 'addon_MockAddon_view';
        $result = $page->replaceAdminActions();
        $this->assertEquals('viewed', $result);
    }

    public function testAddonAdminReplaceTags(): void
    {
        $addon = Mockery::mock('overload:\OpenRealty\Addons\MockAddon\Addon');
        $addon->shouldReceive('runTemplateUserFields')->with('addon_MockAddon_tag1')->andReturn('t1');
        $addon->shouldReceive('runTemplateUserFields')->with('addon_MockAddon_tag2')->andReturn('t2');
        //Setup Mock of Login
        $login = Mockery::mock("\OpenRealty\Login");
        $login->shouldReceive('loginCheck')->andReturn(true);
        $page = Mockery::mock('\OpenRealty\PageAdmin', [$this->dbh, $this->config])->makePartial();
        $page->shouldReceive("newLogin")->andReturn($login);
        $page->page = '{addon_MockAddon_tag1}:{addon_MockAddon_tag2}';
        $page->replaceTags(['addon_MockAddon_tag1', 'addon_MockAddon_tag2']);
        $this->assertEquals('t1:t2', $page->page);
    }

    public function testAddonUserReplaceTags(): void
    {
        $addon = Mockery::mock('overload:\OpenRealty\Addons\MockAddon\Addon');
        $addon->shouldReceive('runTemplateUserFields')->with('addon_MockAddon_tag1')->andReturn('t1');
        $addon->shouldReceive('runTemplateUserFields')->with('addon_MockAddon_tag2')->andReturn('t2');
        $page = Mockery::mock('\OpenRealty\PageUser', [$this->dbh, $this->config])->makePartial();
        $page->page = '{addon_MockAddon_tag2}:{addon_MockAddon_tag1}';
        $page->replaceTags(['addon_MockAddon_tag1', 'addon_MockAddon_tag2']);
        $this->assertEquals('t2:t1', $page->page);
    }

    public function testAddonUserActionAjax(): void
    {
        $addon = Mockery::mock('overload:\OpenRealty\Addons\MockAddon\Addon');
        $addon->shouldReceive('runUserActionAjax')->andReturn('viewed');
        $page = Mockery::mock('\OpenRealty\PageUserAjax', [$this->dbh, $this->config])->makePartial();
        $_GET['action'] = 'addon_MockAddon_view';
        $result = $page->callAjax();
        $this->assertEquals('viewed', $result);
    }

    public function testAddonAdminActionAjax(): void
    {
        $addon = Mockery::mock('overload:\OpenRealty\Addons\MockAddon\Addon');
        $addon->shouldReceive('runActionAjax')->andReturn('viewed');
        //Setup Mock of Login
        $login = Mockery::mock("\OpenRealty\Login");
        $login->shouldReceive('loginCheck')->andReturn(true);
        $page = Mockery::mock('\OpenRealty\PageAdminAjax', [$this->dbh, $this->config])->makePartial();
        $page->shouldReceive("newLogin")->andReturn($login);
        $_GET['action'] = 'addon_MockAddon_view';
        $result = $page->callAjax();
        $this->assertEquals('viewed', $result);
    }
}
