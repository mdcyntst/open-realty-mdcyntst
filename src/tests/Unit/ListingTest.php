<?php

namespace Tests\Unit;

use Mockery;
use Tests\Support\UnitTester;
use OpenRealty\ListingPages;

class ListingTest extends TestSetup
{
    protected UnitTester $tester;

    // phpcs:ignore
    protected function _before()
    {
        parent::_before();
    }

    // phpcs:ignore
    protected function _after()
    {
        parent::_after();
    }

    /**
     * Test the pclass_link function
     *
     * @return void
     */
    public function testPclassLink()
    {
        $api_result = [];
        //Mock the Listing__read api and return pclass ID 2
        $api_result['listing']['listingsdb_pclass_id'] = 2;


        $listing_api = Mockery::mock("\OpenRealty\Api\Commands\ListingApi");
        // check if our mock is called
        $listing_api->shouldReceive("read", ['listing_id' => 1, 'fields' => ['listingsdb_pclass_id']])->andReturns($api_result);

        $api_result = [];
        $api_result['class_name'] = "Test Class";

        $pclass_api = Mockery::mock("\OpenRealty\Api\Commands\PClassApi");
        // check if our mock is called
        $pclass_api->shouldReceive("read", ['class_id' => 2])->andReturns($api_result);

        $lp = Mockery::mock("\OpenRealty\ListingPages", [$this->dbh, $this->config])->makePartial();
        $lp->shouldReceive("newListingApi")->andReturn($listing_api);
        $lp->shouldReceive("newPClassApi")->andReturn($pclass_api);

        // //Run the function
        $link = $lp->pClassLink(1);
        //Set our expected URL base on our mock api calls.
        $expected_url = '<a href="http://web.local/index.php?action=searchresults&amp;pclass[]=2" title="Test Class">Test Class</a>';
        $this->assertEquals($expected_url, $link);
    }
}
