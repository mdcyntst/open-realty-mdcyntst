<?php

namespace Tests\Unit;

use Mockery;
use Tests\Support\UnitTester;

// Include the login class we need to test
use OpenRealty\Login;

/**
 * Unit Test for the Login Class in src/include/login.inc.php
 */
class LoginTest extends TestSetup
{
    /**
     * @var UnitTester
     */
    protected UnitTester $tester;

    // phpcs:ignore
    protected function _before()
    {
        parent::_before();
    }

    // phpcs:ignore
    protected function _after()
    {
        parent::_after();
    }

    /**
     * Tests that the clear_rememberme_cookie will function without a cookie already existing, and will set an empty
     * cookie.
     *
     * @return void
     */
    public function testClearRememberMeCookieWithNoCookie()
    {
        $misc = Mockery::mock('\OpenRealty\Misc', [$this->dbh, $this->config])->makePartial();
        $login = Mockery::mock('\OpenRealty\Login', [$this->dbh, $this->config])->makePartial();
        $login->shouldReceive("newMisc")->andReturn($misc);
        $misc->expects()->setCookie('user_id', '', Mockery::type('int'), '/', 'web.local', false, true);
        $misc->expects()->setCookie('cookie_validator', '', Mockery::type('int'), '/', 'web.local', false, true);
        $misc->expects()->setCookie('cookie_selector', '', Mockery::type('int'), '/', 'web.local', false, true);

        $login->clearRememberMeCookie();
    }

    /**
     * Tests that clear_rememberme_cookie clears the cookie's selector from the auth_tokens table and sets and empty
     * cookie
     *
     * @return void
     */
    public function testClearRememberMe()
    {
        global $ORconn;
        $misc = Mockery::mock('\OpenRealty\Misc', [$this->dbh, $this->config])->makePartial();
        $login = Mockery::mock('\OpenRealty\Login', [$this->dbh, $this->config])->makePartial();
        $login->shouldReceive("newMisc")->andReturn($misc);

        $misc->allows()->makeDbSafe('bah')->andReturns('"bah"');
        $misc->allows()->makeDbSafe(1)->andReturns(1);
        //Make sure we delete the cookie from the auth_tokens db table.
        $ORconn->expects()->Execute(
            'DELETE FROM default_auth_tokens
            WHERE userdb_id = 1 AND  selector = "bah"'
        )->andReturns(null);
        //Make sure we clear cookies
        $misc->expects()->setCookie('user_id', '', Mockery::type('int'), '/', 'web.local', false, true);
        $misc->expects()->setCookie('cookie_validator', '', Mockery::type('int'), '/', 'web.local', false, true);
        $misc->expects()->setCookie('cookie_selector', '', Mockery::type('int'), '/', 'web.local', false, true);

        $_COOKIE['cookie_selector'] = 'bah';
        $_COOKIE['user_id'] = 1;
        $login->clearRememberMeCookie();
    }
}
