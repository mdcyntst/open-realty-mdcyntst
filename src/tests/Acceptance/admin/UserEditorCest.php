<?php

namespace Tests\Acceptance;

use Tests\Support\AcceptanceTester;

/**
 * Acceptance Test for the User Editor's Media Widget
 */
class UserEditorCest extends TestSetup
{
    // phpcs:ignore
    public function _before(AcceptanceTester $I)
    {
        parent::_before($I);
    }

    // phpcs:ignore
    public function _after(AcceptanceTester $I)
    {
        parent::_after($I);
    }

    /**
     * Check that the add user Dialog Open when add_user_link is clicked
     *
     * @param AcceptanceTester $I
     * @return void
     */
    public function addUserDialogOpens(AcceptanceTester $I)
    {
        $I->adminLogin('admin', 'password');
        $I->amOnPage('http://localhost:8100/admin/index.php?action=user_manager');
        $I->waitForJS("return $.active == 0;", 60); // Wait for all ajax calls to complete
        $I->click("#add_user_link");
        $I->waitForElementVisible('#customizeModalTitle', 30); // secs
        $I->See("Password (Again)");
    }
}
