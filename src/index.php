<?php

declare(strict_types=1);

use OpenRealty\Loader;

require_once dirname(__FILE__) . "/autoloader.php";
$loader = new Loader();
