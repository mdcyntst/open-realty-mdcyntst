<?php

$autoLoader = require dirname(__FILE__) . "/vendor/autoload.php";
$autoLoader->setPsr4('OpenRealty\\', dirname(__FILE__) . "/include/");
$autoLoader->setPsr4('OpenRealty\\Install\\', dirname(__FILE__) . "/install");
$autoLoader->setPsr4('OpenRealty\\Addons\\', dirname(__FILE__) . "/addons");
$autoLoader->setPsr4('OpenRealty\\Hooks\\', dirname(__FILE__) . "/hooks");
$autoLoader->setPsr4('OpenRealty\\Api\\', dirname(__FILE__) . "/api");
